#ifndef sfi_trans_Ylm_h
#define sfi_trans_Ylm_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Transform.h"
#include "full_system.h"
#include <complex>
#include <cmath>
#include "ALP.h"
#include "fourier.h"
#include "legendre.h"

namespace sfi {
  namespace bases {
    /** @return Index in array for given l and m, no negative m */
    constexpr inline size_t ylm_index(unsigned l, unsigned m) { return size_t((l*(l+1))/2 + m); }

    /** @return Number of parameters needed for a series of degree l */
    constexpr inline size_t ylm_nparams(unsigned l) { return ylm_index(l,l) + 1; }

    /**
     * Perform Yl1m1*Yl2m2
     *
     * @param buffer Values for Ylm
     *
     * Reference:
     * > Dong and Lemus, "The Overlap Integral of Three Associated Legendre Polynomials",
     *   Applied Mathematics Letters 15 (2002) 541-546
     */
    std::complex<double> ylm_multiply(int l1, int m1, int l2, int m2, std::complex<double>* buff);

    /**
     * Multiply two Ylm and store the result in a table indexed by L
     *
     * Reference:
     * > Dong and Lemus, "The Overlap Integral of Three Associated Legendre Polynomials",
     *   Applied Mathematics Letters 15 (2002) 541-546
     *
     * @return If the computation went well
     */
    bool ylm_param_multiply(int l1, int m1, int l2, int m2, Vector<std::complex<double>>&res, const std::complex<double>& p);

    /**
     * @class sfi::bases::Ylm
     *
     * Transform using spherical harmonics (Y_{lm}).
     * The method compute computes Ylm for all l<=n and 0<=m<=l
     * and stores them in a n(n+3)/2 sized array.
     * Only positive m:s need to be stored, the imaginary parts cancel
     * upon evaluation, and the real part is multiplied by 2.
     *
     * The first index refers to l and cos(theta), the second to m and phi.
     *
     * Note: The theta variable needs to be transformed using a variable
     * transformation in order to get correc spherical harmonics.
     */
    struct Ylm : public BasisImpl<true, false> {
      static const std::string& name () {
        static std::string s_name = "ylm";
        return s_name;
      }

      static constexpr inline bool zero_integral(int _k) { return _k > 0; }

      static inline double integral(int /*_k*/) { return 2.0; }

      struct Constants : ALP::Constants {
        inline double operator()(unsigned l, unsigned m) const { return m_norms[ylm_index(l, m)]; }
        inline const double* operator[](unsigned l) const { return &m_norms[ylm_index(l, 0)]; }
        static const Constants& instance();
        inline double norm(unsigned l, int m) const {
          return (m >= 0) ? m_norms[ylm_index(l, m)] : m_neg_norms[ylm_index(l, -m)];
        }
      protected:
        Constants();
        double m_norms[ylm_nparams(g_alp_max_L)];
        double m_neg_norms[ylm_nparams(g_alp_max_L)];
      };

      static inline const Constants& constants() { return Constants::instance();  }

      static constexpr inline double norm(unsigned) { return 1.0; }

      static inline double norm(unsigned l, unsigned m) { return constants()(l, m); }

      static inline const double* norm_data(unsigned l) { return constants()[l]; }

      // TODO: Move to fourier.h, add norm for [0]
      template<class T, CompType ctype>
      static void fourier_compute(T __x, unsigned n, std::complex<T>* res) {
        const T xtr((ctype == Basis::Project) ? -transform_phi(__x) : transform_phi(__x));
        T _x(cos(xtr));
        res[0] = std::complex<T>(1.0, 0.0);
        std::complex<T> p1(_x, sin(xtr));
        res[1] = p1;
        if(n == 1) return;
        std::complex<T> p0(1.0, 0.0), p(0.0, 0.0);
        _x *= 2.0;
        for (unsigned i=2; i <= n; ++i) {
          p.real(std::fma(_x, p1.real(), - p0.real()));
          p.imag(std::fma(_x, p1.imag(), - p0.imag()));
          p0 = p1;
          p1 = p;
          res[i] = p;
        }
      }
      /**
       * Compute Ylm(x, phi)
       *
       * Starts by computing P_{ii} as:
       * P_{ii} = (2i-1)sqrt(1-x*x)P_{i-1i-1} -> p0
       * and
       * P_{ii-1} = (2i-1)*x*P_{i-1i-1} -> p1
       *
       * Then P_{im} = -((m+1)* x * p1/sqrt(1-x*x) + p0)/((i+m+1)*(i-m))
       */
      template<class T, CompType ctype>
      static void compute(const T _x0, const T __y, unsigned n, std::complex<T>* res) {
        // polar region
        if(fabs(_x0) > (1-1e-8)) {
          // clear
          const unsigned N(mempos(n, n)+1);
          for(unsigned i=0; i<N; ++i) res[i] = 0.0;
          for(unsigned i=0; i<=n; ++i) *(res + mempos(i, 0)) = constants().polar(i);
          return;
        }

        // fourier factors
        std::complex<T>* f = res + mempos(n, n) + 1;
        fourier_compute<T, ctype>(__y, n, f);

        // Note factor of 2 needed in the computation below
        const T _cost(_x0), _sint(sqrt(1 - _x0*_x0)), _cott(2.0*_cost/_sint);

        res[0] = norm(0, 0);
        T y0(0.0), y1(0.0), y(0.0);
        T p0(-_sint);

        res[1] = norm(1,0)*_cost;
        res[2] = norm(1,1)*p0*f[1];

        std::complex<T>* pos = 0;
        const double *nor = 0; // normalization
        const double *den = 0; // precomputed denominators
        for (unsigned i=2; i <= n; ++i) {
          pos = res + mempos(i, 0);
          nor = norm_data(i);
          den = constants().denom(i);

          y0 = (2*i - 1)*p0;
          y1 = _cost*y0;
          y0 = -_sint*y0;
          p0 = y0;

          pos[i] = nor[i]*y0*f[i];
          pos[i-1] = nor[i-1]*y1*f[i-1];

          for(int m=(i-2); m >= 0; --m) {
            y = den[m]*(double(m+1)*_cott * y1 + y0);
            pos[m] = nor[m]*y*f[m];
            y0 = y1;
            y1 = y;
          }
        }
      }

      static constexpr inline size_t mempos(unsigned l, unsigned m) { return ylm_index(l, m); }

      /** Size of base function buffers, make sure it's even */
      static constexpr inline size_t buffer_size(size_t deg) { return (deg+4)*(deg+1)/2; }

      static constexpr double half_pi = 0.5*M_PI;

      /** Map -1 -> 1 to 0 -> pi */
      template<class _T>
      static constexpr inline _T transform_theta(_T x) { return (x + 1.0)*half_pi; }
      /** Map -1 -> 1 to 0 -> 2pi */
      template<class _T>
      static constexpr inline _T transform_phi(_T y) { return (y+1.0)*M_PI; }
    };
  }

  template<> struct _t_coefficient_type<bases::Ylm> : tvalued<std::complex<double>> {};
  template<> struct _t_is_basis<bases::Ylm> : ttrue {};

  /**** YLM  ****/

  struct EigenYlm {};
  template<> struct _t_is_eigentype<EigenYlm> : ttrue {};

  template<unsigned _Dims, unsigned _Deg>
  struct _eigen_npars<_Dims, _Deg, EigenYlm> {
    static inline constexpr size_t value() { return powc(bases::ylm_index(_Deg, _Deg)+1, _Dims/2); }
  };

  template<unsigned _Dims>
  struct _eigen_npars<_Dims, 0U, EigenYlm> {
    static inline size_t value(unsigned _deg) { return powi(bases::ylm_index(_deg, _deg)+1, _Dims/2); }
  };

  template<unsigned _Dims, unsigned _Deg, class _IdxType>
  struct EigenIndices<_Dims, _Deg, EigenYlm, _IdxType> : public EigenIndicesBase<_Dims, _Deg, EigenYlm, _IdxType> {
    using This = EigenIndices<_Dims, _Deg, EigenYlm, _IdxType>;
    using Base = EigenIndicesBase<_Dims, _Deg, EigenYlm, _IdxType>;
    using Data = EigenVectorsData<_Dims, _Deg, EigenYlm>;

    EigenIndices(const Data& da):Base(da) { }

    EigenIndices(EigenIndices&& o):Base(std::forward<EigenIndices>(o)) {}

    EigenIndices(const EigenIndices& o):Base(o) {}

    inline bool next() {
      unsigned i=0;
      while((i < This::dims()) && (++this->m_indices[i+1] > this->m_indices[i])) {
        this->m_indices[i+1] = 0;
        if(++this->m_indices[i] <= This::degree()) break;
        this->m_indices[i] = 0;
        i += 2;
      }
      return i < _Dims;
    }

    inline uint64_t index() const {
      uint64_t idx = 0;
      for(int i=This::dims()-2; i >= 0; i -= 2) {
        idx *= bases::ylm_nparams(This::degree());
        idx += bases::ylm_index(this->m_indices[i], this->m_indices[i+1]);
      }
      return idx;
    }

    inline This& operator=(const This& o) { Base::set(o); return *this; }
  };

  template<unsigned _Dims, unsigned _Deg>
  struct YlmEigenSystemBase : StaticEigenSystem<tuint<_Dims>, tuint<_Deg>, EigenYlm, bases::Ylm, tuint<maxc(_Dims/2,1U)>> {
    using Base = StaticEigenSystem<tuint<_Dims>, tuint<_Deg>, EigenYlm, bases::Ylm, tuint<maxc(_Dims/2,1U)>>;
    using Eigenv = t_EigenVectors<Base>;
    using Var = double;
    using Coeff = t_CoefficienctType<bases::Ylm>;

    YlmEigenSystemBase(const Eigenv&):Base() {}

    YlmEigenSystemBase():Base() {}

    static inline void reset_eval(t_EvalData<Base>& ed) {
      for(unsigned d = 0; d<Base::indices(); ++d) {
        ed.ptr[d] = ed.data[d].data();
        ed.ptr_end[d] = ed.ptr[d] + degree() + 1;
      }
    }

    template<Basis::CompType _ttype>
    static inline void precompute(const Var* x, t_EvalData<Base>& ed) { precompute<_ttype>(x, ed[0]); }

    template<Basis::CompType _ttype, class _Var = Var, class _Coeff = Coeff>
    static inline void precompute(const Eigenv&, const _Var* x, _Coeff* res) {
      precompute<_ttype, _Var, _Coeff>(x, res);
    }

    template<Basis::CompType _ttype, class _Var = Var, class _Coeff = Coeff>
    static inline void precompute(const _Var* x, _Coeff* res) {
      for(unsigned i = 0; i<Base::indices(); ++i) {
        bases::Ylm::template compute<_Var, _ttype>(x[2*i], x[2*i+1], _Deg, res);
        res += bases::Ylm::buffer_size(_Deg);
      }
    }

    template<Basis::CompType _ttype, class _Var = Var, class _Coeff = Coeff>
    static inline void precompute(const Eigenv&, unsigned /*d*/, const _Var& /*x*/, _Coeff* /*res*/) { }

    static inline constexpr size_t total_buffer_size(const Eigenv&) {
      return Base::indices()*bases::Ylm::buffer_size(_Deg);
    }

    static constexpr inline size_t degree() { return bases::ylm_index(_Deg, _Deg); }
  };

  template<unsigned _Dims>
  struct YlmEigenSystemBase<_Dims, 0U> : DynamicEigenSystem<tuint<_Dims>, EigenYlm, bases::Ylm, tuint<maxc(_Dims/2,1U)>> {
    using Base = DynamicEigenSystem<tuint<_Dims>, EigenYlm, bases::Ylm, tuint<maxc(_Dims/2,1U)>>;
    using Eigenv = t_EigenVectors<Base>;
    using Var = double;
    using EvalData = t_EvalData<Base>;
    using Coeff = t_CoefficienctType<bases::Ylm>;

    YlmEigenSystemBase(const Eigenv& ev):Base(ev) {}

    YlmEigenSystemBase():Base() {}

    inline void reset_eval(EvalData& ed) const {
      for(unsigned d = 0; d<Base::indices(); ++d) {
        ed.ptr[d] = ed.ptr0[d];
        ed.ptr_end[d] = ed.ptr0[d] + degree() + 1;
      }
    }

    template<Basis::CompType _ttype>
    inline void precompute(const Var* x, EvalData& ed) const { precompute<_ttype>(Base::eigenvectors(), x, ed[0]); }

    template<Basis::CompType _ttype, class _Var = Var, class _Coeff = Coeff>
    static inline void precompute(const Eigenv& ev, const _Var* x, _Coeff* res) {
      for(unsigned i = 0; i<Base::indices(); ++i) {
        bases::Ylm::template compute<_Var, _ttype>(x[2*i], x[2*i+1], ev.degree(), res);
        res += bases::Ylm::buffer_size(ev.degree());
      }
    }

    template<Basis::CompType _ttype, class _Var = Var, class _Coeff = Coeff>
    static inline void precompute(const Eigenv&, unsigned /*d*/, const _Var& /*x*/, _Coeff* /*res*/) { }

    static inline constexpr size_t total_buffer_size(const Eigenv& ev) {
      return Base::indices()*bases::Ylm::buffer_size(ev.degree());
    }

    inline size_t degree() const { return bases::ylm_index(Base::degree(), Base::degree()); }
  };

  template<unsigned _Dims, unsigned _Deg>
  struct EigenSystem<tuint<_Dims>, tuint<_Deg>, EigenYlm, bases::Ylm> : YlmEigenSystemBase<_Dims, _Deg> {
    using This = EigenSystem<tuint<_Dims>, tuint<_Deg>, EigenYlm, bases::Ylm>;
    using Base = YlmEigenSystemBase<_Dims, _Deg>;
    using Coeff = typename Base::Coeff;

    static_assert(((_Dims&0x1) == 0), "Number of dimensions must be even");

    EigenSystem(const t_EigenVectors<Base>& ev):Base(ev) {}
    EigenSystem():Base() {}

    template<Basis::CompType _ttype, class _Func>
    inline _Func compute(const t_Var<Base>* x, t_EvalData<Base>& ed, const _Func& _f) const {
      _Func f(_f);
      Coeff phi(1.0);
      unsigned i(0), d(0), id(0);
      Base::template precompute<_ttype>(x, ed);
      const size_t _deg = Base::degree();
      This::reset_eval(ed);
      for(d = 1; d<Base::indices(); ++d) phi *= ed[d][0];
      do {
        for(id = 0; id<=_deg; ++id) {
          f(ed.ptr[0][id]*phi);
        }
        for(i = 1; (i != Base::indices()) && (++ed.ptr[i] == ed.ptr_end[i]); ++i) ed.ptr[i] = ed[i];
        phi = 1.0;
        for(d=1; d != Base::indices(); ++d) phi *= (*ed.ptr[d]);
      }
      while(i != Base::indices());
      return f;
    }

    template<class _TIndices>
    static inline bool integral(const _TIndices& idx, double& I) {
      for(unsigned d=0; d<Base::indices(); ++d) {
        if((idx[2*d] == 0) && (idx[2*d + 1] == 0)) I *= bases::Ylm::integral(0);
        else return false;
      }
      return true;
    }

    template<class _TIndices, class _Coeff>
    static inline bool integral(const _TIndices& idx, unsigned a, _Coeff& I) {
      for(unsigned d=0; d<Base::indices(); ++d) {
        // theta / l
        if(2*d == a) {
          if(idx[2*d+1] == 0) I *= bases::Ylm::norm(idx[a], idx[a+1]) * bases::Fourier::integral(0);// / bases::Legendre::norm(idx[a]);
          else return false;
        }
        // phi / m
        else if((2*d+1) == a) {
          if(!bases::ALP::zero_integral(idx[a-1], idx[a]))
            I *= bases::Ylm::norm(idx[a-1], idx[a]) * bases::ALP::integral(idx[a-1], idx[a]); // / bases::Fourier::norm(idx[a]);
          else return false;
        }
        else {
          if((idx[2*d] == 0) && (idx[2*d + 1] == 0)) I *= bases::Ylm::integral(0);
          else return false;
        }
      }
      return true;
    }

    template<class _TIndices, class _Coeff>
    static inline bool integral(const _TIndices& idx, unsigned a1, unsigned a2, _Coeff& I) {
      assert(((a1&1) == 0) && (a2 == (a1+1)) && "Invalid indices for Ylm");
      unused_parameters(a2);
      for(unsigned d=0; d<Base::indices(); ++d) {
        if(2*d != a1) {
          if((idx[2*d] == 0) && (idx[2*d + 1] == 0)) I *= bases::Ylm::integral(0);
          else return false;
        }
      }
      return true;
    }

    template<class _EvalData, class _EigenIter>
    static inline Coeff basis_product(const _EvalData& ed, unsigned ed_base_idx, const _EigenIter& eit, Coeff prod) {
      for(unsigned d=0; d<Base::indices(); ++d) {
        prod *= ed[ed_base_idx + d][bases::ylm_index(eit[2*d], eit[2*d+1])];
      }
      return prod;
    }
  };

  /**
   * Symmetry factor. When summation is changed from :
   * re{(\sum_{-N}^{N} a) to \sum_{0}^{N} (a + conj(a))
   * The 0:th term is counted twice, and this factor compensates.
   */
  inline double ylm_dm(int m) { return m==0 ? 0.5 : 1.0; }

  /** Extra normalization factor, compensating for no negative m */
  inline double ylm_cm(int m) { return m==0 ? 1.0 : M_SQRT2; }

  /** Symmetry factor for multiplication of two Ylm */
  static inline constexpr double ylm_elmkn(int l1, int m1, int l2, int m2) {
    return (((l1==l2) && (m1==m2)) ? 1.0 : 2.0);
  }
  static inline constexpr double ylm_elk(int l1, int l2) {
    return (l1==l2) ? 1.0 : 2.0;
  }

  // fixes for creating 1D transforms
  template<class _E, unsigned _Dim>
  struct _t_transform_1D<Transform<_E, bases::Ylm>, _Dim> :
  tvalued<Transform<EigenVectors<1, c_eigen_degree<_E, _Dim>(), EigenFullTensor>, tcond<((_Dim&1)==0), bases::Legendre, bases::Fourier>>> {};

  template<class _Transf, unsigned _Dims, unsigned _Deg>
  struct TransformImpl<_Transf, EigenSystem<tuint<_Dims>, tuint<_Deg>, EigenYlm, bases::Ylm>> :
  DefaultTransformImpl<_Transf, EigenSystem<tuint<_Dims>, tuint<_Deg>, EigenYlm, bases::Ylm>> {
    using Base = DefaultTransformImpl<_Transf, EigenSystem<tuint<_Dims>, tuint<_Deg>, EigenYlm, bases::Ylm>>;
    using EigenSys = EigenSystem<tuint<_Dims>, tuint<_Deg>, EigenYlm, bases::Ylm>;
    using Trf2D = t_Transform<EigenSystem<tuint<2>, tuint<_Deg>, EigenYlm, bases::Ylm>>;
    using Trf2DDyn = t_Transform<EigenSystem<tuint<2>, tuint<0>, EigenYlm, bases::Ylm>>;
    using Eigen2D = t_EigenVectors<Trf2DDyn>;

    using LegTrf1D = t_Transform<EigenSystem<tuint<1>, tuint<_Deg>, EigenFullTensor, bases::Legendre>>;
    using FouTrf1D = t_Transform<EigenSystem<tuint<1>, tuint<_Deg>, EigenFullTensor, bases::Fourier>>;

    using AmpLegTrf1D = t_Transform<EigenSystem<tuint<1>, tuint<0>, EigenALP, bases::ALP>>;
    using AmpFouTrf1D = t_Transform<EigenSystem<tuint<1>, tuint<0>, EigenFullTensor, bases::Fourier>>;

    static inline TransformVariableBase* create_linear_axis_transform(const _Transf& trf, unsigned d, const std::string& name, const EigenSys& eigens, unsigned axis) {
      if(d >= eigens.dims()) return 0;
      if((d&1) == 0) {
        auto *atrf = new LegTrf1D(t_EigenVectors<LegTrf1D>(), name, true);
        linear_axis_transform_theta(trf, *atrf, axis);
        return atrf;
      }
      else {
        auto *atrf = new FouTrf1D(t_EigenVectors<FouTrf1D>(), name, true);
        linear_axis_transform_phi(trf, *atrf, axis);
        return atrf;
      }
    }

    static inline TransformVariableBase* create_amplitude_axis_transform(const _Transf& trf, unsigned d, const std::string& name, const EigenSys& eigens, unsigned axis) {
      if(d >= eigens.dims()) return 0;
      if((d&1) == 0) {
        auto *atrf = new AmpLegTrf1D(t_EigenVectors<AmpLegTrf1D>(2*eigens.max_degree()), name, true);
        amplitude_axis_transform_theta(trf, *atrf, axis);
        return atrf;
      }
      else {
        auto *atrf = new AmpFouTrf1D(t_EigenVectors<AmpFouTrf1D>(eigens.max_degree()), name, true);
        amplitude_axis_transform_phi(trf, *atrf, axis);
        return atrf;
      }
    }

    static inline TransformVariableBase* create_transform_1d(unsigned d, const std::string& name, const EigenSys& eigens) {
      if(d >= eigens.dims()) return 0;
      else return create_transform_1d_impl<EigenSys::is_dynamic()>(d, name, eigens);
    }

    template<bool _is_dyn>
    static inline TransformVariableBase* create_transform_1d_impl(unsigned d, const std::string& name, const EigenSys& eigens, teif<_is_dyn>* = 0) {
      if((d&1) == 0) return new LegTrf1D(t_EigenVectors<LegTrf1D>(eigens.max_degree()), name, true);
      else return new FouTrf1D(t_EigenVectors<FouTrf1D>(eigens.max_degree()), name, true);
    }
    template<bool _is_dyn>
    static inline TransformVariableBase* create_transform_1d_impl(unsigned d, const std::string& name, const EigenSys&, teif<!_is_dyn>* = 0) {
      if((d&1) == 0) return new LegTrf1D(name, true);
      else return new FouTrf1D(name, true);
    }

    static inline TransformVariableBase* create_marginalized_2d(const _Transf& trf, unsigned a1, unsigned a2, const std::string& name, const EigenSys& eigens, unsigned axis1, unsigned axis2) {
      if(((a1&1) == 0) && (a2 == (a1+1))) {
        if(trf.is_amplitude()) return create_marginalized_2d_impl<true>(trf, axis1, axis2, name, eigens);
        else return create_marginalized_2d_impl<EigenSys::is_dynamic()>(trf, axis1, axis2, name, eigens);
      }
      return 0;
    }

    template<bool _is_dyn>
    static inline TransformVariableBase* create_marginalized_2d_impl(const _Transf& trf, unsigned a1, unsigned a2, const std::string& name, const EigenSys& eigens, teif<_is_dyn>* = 0) {
      auto* mtrf = new Trf2DDyn(Eigen2D((trf.is_amplitude() ? 2:1)*eigens.max_degree()), name, true);
      if(trf.is_amplitude()) marginalize_2d_amp(trf, a1, a2, *mtrf);
      else {
        Base::linear_marginalize_2d(trf, *mtrf, a1, a2);
      }
      return mtrf;
    }

    static inline void marginalize_2d_amp(const _Transf& trf, unsigned a1, unsigned a2, Trf2DDyn& res) {
      using Coeff = t_CoefficienctType<_Transf>;
      res.clear();
      Coeff p1(0.0), p2(0.0);
      int l1, l2, m1, m2;
      const int deg = trf.var_degree(a1);
      for(auto& eit : trf.eigenvectors()) {
        l1 = eit[a1];
        m1 = eit[a2];
        p1 = trf.par(eit.par_idx());
        auto ei = eit.indices();
        for(l2=0; l2<=deg; ++l2) {
          for(m2=0; m2<=min(m1,l2);++m2) {
            ei.set(a1, l2);
            ei.set(a2, m2);
            p2 = trf.par(ei.index());
            double y = 2.0*ylm_dm(m1)*ylm_dm(m2)*ylm_elk(m1,m2)/(ylm_cm(m1)*ylm_cm(m2));
            bases::ylm_param_multiply(l1, m1, l2, m2, res.parameters(), y*p1*p2);
            bases::ylm_param_multiply(l1, m1, l2, -m2, res.parameters(), negone_to(m2)*y*p1*std::conj(p2));
          }
        }
      }
    }

    template<bool _is_dyn>
    static inline TransformVariableBase* create_marginalized_2d_impl(const _Transf& trf, unsigned a1, unsigned a2, const std::string& name, const EigenSys&, teif<!_is_dyn>* = 0) {
      auto* mtrf = new Trf2D(name, true);
      Base::linear_marginalize_2d(trf, *mtrf, a1, a2);
      return mtrf;
    }

    static inline void amplitude_axis_transform_theta(const _Transf& trf, AmpLegTrf1D& res, unsigned axis) {
      res.clear();
      std::complex<double> p1(0.0), p2(0.0);
      const int deg = trf.var_degree(axis);
      int l, m, l2;
      for(auto& eit : trf.eigenvectors()) {
        l = eit[axis];
        m = eit[axis+1];
        p1 = trf.par(eit.par_idx());
        auto ei = eit.indices();
        for(l2=m; l2<=deg; ++l2) {
          ei.set(axis, l2);
          ei.set(axis+1, m);
          p2 = trf.par(ei.index());
          double y = ylm_elk(m,m)*bases::Ylm::norm(l, m)*bases::Ylm::norm(l2, m);
          if(m==0) bases::alp_param_multiply(l, 0, l2, 0, res.parameters(), y*std::real(p1*p2));
          bases::alp_param_multiply(l, m, l2, m, res.parameters(), y*std::real(p1*std::conj(p2)));
        }
      }
    }

    static inline void amplitude_axis_transform_phi(const _Transf& trf, AmpFouTrf1D& res, unsigned axis) {
      using Coeff = t_CoefficienctType<_Transf>;
      res.clear();
      Coeff p1(0.0), p2(0.0);
      const int deg = res.var_degree(0);
      int l1, l2, m1, m2;
      for(auto& eit : trf.eigenvectors()) {
        l1 = eit[axis-1];
        m1 = eit[axis];
        p1 = trf.par(eit.par_idx());
        auto ei = eit.indices();
        for(l2=0; l2<=deg; ++l2) {
          for(m2=0; m2<=min(m1,l2);++m2) {
            if(!bases::alp_2_zerointegral(l1, m1, l2, m2)) {
              ei.set(axis-1, l2);
              ei.set(axis, m2);
              p2 = trf.par(ei.index());
              double y = 0.5*bases::alp_2_integral(l1, m1, l2, m2)*bases::Ylm::norm(l1,m1)*bases::Ylm::norm(l2,m2)*ylm_elk(m1,m2);
              res.par(m1-m2) += y*p1*std::conj(p2)/bases::Fourier::norm(m1-m2);
              if(m1+m2 <= deg) res.par(m1+m2) += y*p1*p2/bases::Fourier::norm(m1+m2);
            }
          }
        }
      }
    }

    static inline void linear_axis_transform_phi(const _Transf& trf, FouTrf1D& res, unsigned axis) {
      Base::linear_axis_transform(trf, res, axis);
      for(unsigned i=0; i<res.nparams(); ++i) res[i] = res[i]/bases::Fourier::norm(i);
    }

    static inline void linear_axis_transform_theta(const _Transf& trf, LegTrf1D& res, unsigned axis) {
      Base::linear_axis_transform(trf, res, axis);
      // M_SQRT2 compensates for the fact that int F = sqrt(2) and not 2.
      for(unsigned i=0; i<res.nparams(); ++i) res[i] = M_SQRT2*res[i]/bases::Legendre::norm(i);
    }
  };
}

#endif // sfi_trans_Ylm_h
