#ifndef sfi_ClebschGordan_h
#define sfi_ClebschGordan_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Log.h"
#include "sfi_math.h"

namespace sfi {
  inline constexpr int64_t cg_index_m1(int64_t l1, int64_t l2, int64_t M, int64_t m1) {
    return min(l1, l2 + M) - m1;
  }

  inline constexpr int64_t cg_index_M(int64_t l1, int64_t l2, int64_t M) {
    return (M < (l1 - l2)) ? (2*l2 + 1)*(M+1)  : (M*(1LL - M + 2LL*(l1 + l2))  + l1*(1LL - l1 + 2LL*l2) + l2*(3LL - l2) + 2LL)/2LL;
  }

  /**
   * (6*(-l1*l1 + l1 - l2*l2 + 3*l2 + 2*l1*l2 + 2)*(L - l1 + l2 + 1) + 3*((1 + 2*l1 + 2*l2)*(L*(L+1) - (l1 - l2 - 1)*(l1 - l2))) - (L*(L+1)*(2*L+1) - (l1 - l2 - 1)*(l1 - l2)*(2*l1 - 2*l2 - 1)))/12
   */
  inline constexpr int64_t cg_index_L(int64_t l1, int64_t l2, int64_t L) {
    return (-L*(L + 1)*(2*L + 1) + (l1 - l2)*(-2*l1 + 2*l2 + 1)*(-l1 + l2 + 1))/12 + (L*(L + 1) + (l1 - l2)*(-l1 + l2 + 1))*(2*l1 + 2*l2 + 1)/4 + (L - l1 + l2 + 1)*(l1*(-l1 + 2*l2 + 1) + l2*(-l2 + 3) + 2)/2;
  }

  /**
   * (l1 + 1)*(l2+1) + ((48*l1 + 40)*l2*(l2+1))/(12*2) + ((4*l1 + 2)*l2*(l2+1)*(2*l2 + 1))/6 - (4*(l2*(l2+1))*(l2*(l2+1)))/12
   */
  inline constexpr int64_t cg_index_l2(int64_t l1, int64_t l2) {
    return (l2 + 1)*(3*l1 - l2*l2*(l2 + 1) + l2*(2*l1 + 1)*(2*l2 + 1) + l2*(6*l1 + 5) + 3)/3;
  }

  inline constexpr int64_t cg_npar(int64_t L) {
    return (L*(L*(L*(L*(6LL*L + 15LL*3LL) + 10LL*13LL) + 30LL*6LL) + 119LL))/30LL + 1LL;
  }

  /**
   * (L*(L*(-5*L + 15*l1 + 15*l2 + 15) + l1*(- 15*l1 + 30*l2) + l2*(- 15*l2 + 30) + 10*2) + l1*(l1*(l1*(l1*(6*l1 + 15) + 15) - 15*3*l2 - 15) + l2*(l2*(10*4*l2 + 15*5) - 10) - 3*7) + (l2*(l2*(l2*(- 10*l2 + 5) + 5*5) + 10)))/30 + cg_index_M(l1, l2, M-1) + cg_index_m1(l1, l2, M, _m1);
   */
  inline int64_t cg_index(int64_t l1, int64_t l2, int64_t L, int64_t M, int64_t _m1) {
    return cg_index_m1(l1, l2, M, _m1) + (((M-1)<(l1-l2)) ?
        L*(L*(-5*L + 15*l1 + 15*l2 + 15) + l1*(- 15*l1 + 30*l2) + l2*(- 15*l2 + 30) + 20) + M*(60*l2 + 30) +                  l1*(l1*(l1*(l1*(6*l1 + 15) + 15) - 45*l2 - 15) + l2*(l2*(40*l2 + 75) - 10) - 21) + l2*(l2*(l2*(- 10*l2 + 5) + 25) + 10) :
        L*(L*(-5*L + 15*l1 + 15*l2 + 15) + l1*(- 15*l1 + 30*l2) + l2*(- 15*l2 + 30) + 20) + M*(- 15*M + 30*l1 + 30*l2 + 45) + l1*(l1*(l1*(l1*(6*l1 + 15) + 15) - 45*l2 - 30) + l2*(l2*(40*l2 + 75) + 20) - 36) + l2*(l2*(l2*(- 10*l2 + 5) + 10) + 25))/30;
  }

  inline double cg_Cplus(int64_t l, int64_t m) { return sqrt(l*(l+1) - m*(m+1)); }
  inline double cg_Cmin(int64_t l, int64_t m) { return sqrt(l*(l+1) - m*(m-1)); }

  inline int64_t cg_Cplus2(int64_t l, int64_t m) { return int64_t(l*(l+1)) - m*(m+1); }
  inline int64_t cg_Cmin2(int64_t l, int64_t m) { return int64_t(l*(l+1)) - m*(m-1); }


  struct cg_state {
    cg_state(int l1, int l2, int L, int M, int m1):m_l1(l1), m_l2(l2), m_L(L), m_M(M), m_m1(m1) {}
    int m_l1, m_l2, m_L, m_M, m_m1;
  };

  inline bool cg_valid_state(int l1, int l2, int L, int M, int m1) {
    return (abs(m1) <= l1) && (abs(M-m1) <= l2) && (M <= L) && (M >= 0) && (L <= (l1 + l2)) && (L >= (l1 - l2));
  }

  inline std::ostream& operator<<(std::ostream& out, const cg_state& s) {
    bool valid = cg_valid_state(s.m_l1, s.m_l2, s.m_L, s.m_M, s.m_m1);
    if(!valid) out<<" INVALID ";
    return out<<"<"<<s.m_l1<<", "<<s.m_m1<<" "<<s.m_l2<<", "<<(s.m_M-s.m_m1)<<" | "<<s.m_L<<", "<<s.m_M<<">";
  }

  /**
   * @class sfi::ClebschGordanTable
   *
   * Reference:
   * Clebsch–Gordan coefficients, https://en.wikipedia.org/w/index.php?title=Clebsch%E2%80%93Gordan_coefficients&oldid=971088860 (last visited Oct. 7, 2020).
   */
  class ClebschGordanTable {
  public:
    ClebschGordanTable();

    inline double coefficient(int l1, int l2, int L, int M, int m1) const {
      if(l2 > l1) {
        // <l1 m1 l2 m2|L M> = (-1)^(l1+l2-L)<l2 m2 l1 m1|L M>
        return negone_to(l1+l2-L)*coefficient(l2, l1, L, M, M - m1);
      }
      else if(M < 0) {
        // <l1 m1 l2 m2|L M> = (-1)^(l1+l2-L)<l1 -m1 l2 -m2|L -M>
        return negone_to(l1+l2-L)*coefficient(l1, l2, L, -M, -m1);
      }
      if(cg_valid_state(l1, l2, L, M, m1)) return m_table[cg_index(l1, l2, L, M, m1)];
      else return 0.0;
    }

    static const ClebschGordanTable& instance();
  protected:
    void build_table(unsigned Lmax);

    double m_table[cg_npar(2*g_alp_max_L)];
  };
}

#endif // sfi_ClebschGordan_h
