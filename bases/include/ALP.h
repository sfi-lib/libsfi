#ifndef sfi_ALP_H
#define sfi_ALP_H

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Transform.h"
#include "sfi_math.h"
#include <inttypes.h>

#include <cmath>
#include "Basis.h"

namespace sfi {
  namespace bases {
    /** @return Number of valid products Pml*Pmk in an expansion of (A(x,y,))^2 */
    inline constexpr size_t alp_2_nparams(unsigned L) { return (L+1)*(L+2)*(L+3)/6; }

    /** @return parameter index given l,m,k */
    inline constexpr size_t alp_2_index(unsigned l, unsigned m, unsigned k) {
      return alp_2_nparams(l-1)  + (m*(2*l - m + 1) + 2*k)/2;
    }

    /** @return parameter index given l,m,k */
    inline constexpr size_t alp_index(unsigned l, unsigned m) { return size_t((l*(l+1))/2 + m); }

    /** @return parameter index given l,m,k */
    inline constexpr size_t alp_nparams(unsigned l) { return alp_index(l,l) + 1; }


    /** @return Number of nonzero terms when integrating Plm */
    inline constexpr unsigned alp_nonzero(unsigned l) {
      return (l&1) ? (l*l + 2*l + 6)/4 : (l*(l+2)/4 + 1);
    }

    /**
     * Compute sqrt((l+n)!/(l-n)!)
     * For a range of l and m, such that m <= l
     */
    class SqrtPartFactorial {
    public:
      SqrtPartFactorial(unsigned l):m_lmax(l), m_l(0), m_m(0), m_value(1.0), m_index(0) {}

      inline void reset() { m_index = m_l = m_m = 0; m_value = 1.0; }

      inline double operator()() const { return m_value; }

      inline unsigned l() const { return m_l; }

      inline unsigned m() const { return m_m; }

      inline unsigned index() const { return m_index; }

      inline bool next() {
        if(++m_m > m_l) {
          if(++m_l > m_lmax) return false;
          m_m = 0;
          m_value = 1.0;
        }
        else {
          m_value *= sqrt(m_l + m_m)*sqrt(m_l - m_m + 1.0);
        }
        ++m_index;
        return true;
      }
    protected:
      unsigned m_lmax, m_l, m_m;
      double m_value;
      unsigned m_index;
    };

    /**
     * Compute i!/j!
     * TODO: Move to math
     * TODO: Better impl
     * TODO: UT
     * TODO: Caching if needed
     */
    double fact_over_fact_d(uint64_t i, uint64_t j);

    /**
     * Compute sqrt(i!/j!)
     * Not fast!
     */
    double sqrt_fact_over_fact_d(uint64_t i, uint64_t j);

    /**
     * Perform Pl1m1*Pl2m2
     * @param buff Pointer to buffer with all Plm values
     */
    double alp_multiply(int l1, int m1, int l2, int m2, double* buff);

    /**
     * @return If the indices are valid
     */
    bool alp_valid_indices(int l1, int m1, int l2, int m2);

    void legendre_multiply(int l1, int l2, Vector<double>& par, double p);

    /**
     * Multiply two ALP:s and store the result in a table indexed by <L,M|
     *
     * Reference:
     * > Dong and Lemus, "The Overlap Integral of Three Associated Legendre Polynomials",
     *   Applied Mathematics Letters 15 (2002) 541-546
     *
     * @return If the computation went well
     */
    bool alp_param_multiply(int l1, int m1, int l2, int m2, Vector<double>& res, double p);

    /**
     * Integrate Pl1m1Pl2m2
     *
     * Reference:
     * > Dong and Lemus, "The Overlap Integral of Three Associated Legendre Polynomials",
     *   Applied Mathematics Letters 15 (2002) 541-546
     *
     * TODO: Use known cases when I = 0
     * TODO: Use cached factorial vals
     */
    double alp_2_integral(int l1, int m1, int l2, int m2);

    /**
     * This function doesn't cover all cases when the integral is 0
     * @return true if the integral of Plm*Pnk is 0
     */
    bool alp_2_zerointegral(int l1, int m1, int l2, int m2);

    /**
     * Compute: I=integral(-1,1) Plm
     * If l even and m odd or vice verse: I=0
     * If m==0 and l>0: I=0
     *
     * Reference:
     * > Dong and Lemus, "The Overlap Integral of Three Associated Legendre Polynomials",
     *   Applied Mathematics Letters 15 (2002) 541-546
     */
    double alp_integral(unsigned l, int m);

    struct ALP : public BasisImpl<true, false> {
      static const std::string& name () {
        static std::string s_name = "ALP";
        return s_name;
      }

      static constexpr inline bool zero_integral(unsigned l, int m) {
        return ((m==0) && (l>0)) || ((l&1) != (m&1));
      }

      struct Constants {
        inline double integral(unsigned l, unsigned m) const { return m_integrals[alp_index(l, m)]; }
        inline double polar(unsigned l) const { return m_polar[l]; }
        inline const double* denom(unsigned l) const { return &m_denom[alp_index(l, 0)]; }
        static const Constants& instance();
      protected:
        Constants();
        double m_integrals[alp_nparams(2*g_alp_max_L)];
        double m_denom[alp_nparams(2*g_alp_max_L)];
        double m_polar[(2*g_alp_max_L+1)];
      };

      static inline const Constants& constants() { return Constants::instance(); }

      static inline double integral(unsigned l, unsigned m) { return constants().integral(l,m); }

      static constexpr inline double norm(unsigned) { return 1.0; }

      static inline double norm(unsigned /*l*/, unsigned /*m*/) { return 1.0; }

      template<class T, CompType ctype>
      static void compute(const T _x0, unsigned n, T* res) {
        // polar region
        if(fabs(_x0) > (1-1e-8)) {
          // clear
          const unsigned N(alp_nparams(n));
          for(unsigned i=0; i<N; ++i) res[i] = 0.0;
          for(unsigned i=0; i<=n; ++i) *(res + mempos(i, 0)) = constants().polar(i);
          return;
        }
        // Note factor of 2 needed in the computation below
        const T _cost(_x0), _sint(sqrt(1 - _x0*_x0)), _cott(2.0*_cost/_sint);

        res[0] = 1.0;
        T y0(0.0), y1(0.0), y(0.0);
        T p0(-_sint);

        res[1] = _cost;
        res[2] = p0;

        T* pos = 0;
        const double *den = 0; // precomputed denominators
        for (unsigned i=2; i <= n; ++i) {
          pos = res + mempos(i, 0);
          den = constants().denom(i);

          y0 = (2*i - 1)*p0;
          y1 = _cost*y0;
          y0 = -_sint*y0;
          p0 = y0;

          pos[i] = y0;
          pos[i-1] = y1;

          for(int m=(i-2); m >= 0; --m) {
            y = den[m]*(double(m+1)*_cott * y1 + y0);
            pos[m] = y;
            y0 = y1;
            y1 = y;
          }
        }
      }

      static constexpr inline size_t mempos(unsigned l, unsigned m) { return alp_index(l, m); }

      /** Size of base function buffers, make sure it's even */
      static constexpr inline size_t buffer_size(size_t deg) { return alp_nparams(deg); }
    };
  }
  template<> struct _t_coefficient_type<bases::ALP> : tvalued<double> {};
  template<> struct _t_is_basis<bases::ALP> : ttrue {};

  /**** ALP square  ****/

  struct EigenALP {};

  template<unsigned _Deg>
  struct _eigen_npars<1, _Deg, EigenALP> {
    static inline constexpr size_t value() { return bases::alp_nparams(_Deg); }
  };

  template<>
  struct _eigen_npars<1, 0U, EigenALP> {
    static inline size_t value(unsigned _deg) { return bases::alp_nparams(_deg); }
  };

  template<unsigned _Dim, unsigned _Deg, class _IdxType>
  struct EigenIndices<_Dim, _Deg, EigenALP, _IdxType> : EigenIndicesBase<_Dim, _Deg, EigenALP, _IdxType> {
    using This = EigenIndices<_Dim, _Deg, EigenALP, _IdxType>;
    using Base = EigenIndicesBase<_Dim, _Deg, EigenALP, _IdxType>;
    using Data = EigenVectorsData<_Dim, _Deg, EigenALP>;

    EigenIndices(const Data& da):Base(da) { }

    EigenIndices(EigenIndices&& o):Base(std::forward<EigenIndices>(o)) {}

    EigenIndices(const EigenIndices& o):Base(o) {}

    inline This& operator=(const This& o) { return Base::set(o); }
  };

  template<>
  struct EigenSystem<tuint<1U>, tuint<0U>, EigenALP, bases::ALP> : DynamicEigenSystem<tuint<1U>, EigenALP, bases::ALP, tuint<1U>> {
    using Base = DynamicEigenSystem<tuint<1U>, EigenALP, bases::ALP, tuint<1U>>;
    using Eigenv = t_EigenVectors<Base>;
    using Var = double;
    using EvalData = t_EvalData<Base>;
    using Coeff = t_CoefficienctType<bases::ALP>;

    EigenSystem(const Eigenv& ev):Base(ev) {}

    template<Basis::CompType _ttype>
    inline void precompute(const Var* x, EvalData& ed) const { precompute<_ttype>(Base::eigenvectors(), x, ed[0]); }

    template<Basis::CompType _ttype, class _Var = Var, class _Coeff = Coeff>
    static inline void precompute(const Eigenv& ev, const _Var* x, _Coeff* res) {
      bases::ALP::template compute<_Var, _ttype>(x[0], ev.degree(), res);
    }

    template<Basis::CompType _ttype, class _Var = Var, class _Coeff = Coeff>
    static inline void precompute(const Eigenv&, unsigned /*d*/, const _Var& /*x*/, _Coeff* /*res*/) { }

    inline size_t degree() const { return bases::alp_index(Base::degree(), Base::degree()); }

    // TODO: This might be handled in a generic way if ES supported Ndeg != Nparams per dim
    template<Basis::CompType _ttype, class _Func>
    inline _Func compute(const Var* x, EvalData& ed, const _Func& _f) const {
      _Func f(_f);
      precompute<_ttype>(x, ed);
      const size_t _deg = Base::degree();
      This::reset_eval(ed);
      for(unsigned l=0; l<=_deg; ++l) {
        for(unsigned m=0; m<=l; ++m) {
          f(*ed.ptr[0]);
          ++ed.ptr[0];
        }
      }
      return f;
    }

    template<class _TIndices>
    static inline bool integral(const _TIndices& /*idx*/, double& /*I*/) { return true; }

    template<class _TIndices, class _Coeff>
    static inline bool integral(const _TIndices& /*idx*/, unsigned /*a*/, _Coeff& /*I*/) { return true; }

    template<class _TIndices, class _Coeff>
    static inline bool integral(const _TIndices& /*idx*/, unsigned /*a1*/, unsigned /*a2*/, _Coeff& /*I*/) { return true; }

    template<class _EvalData, class _EigenIter>
    static inline Coeff basis_product(const _EvalData& /*ed*/, unsigned /*ed_base_idx*/, const _EigenIter& /*eit*/, Coeff prod) {
      /*for(unsigned d=0; d<Base::indices(); ++d) {
        prod *= ed[ed_base_idx + d][bases::ylm_index(eit[2*d], eit[2*d+1])];
      }*/
      return prod;
    }
  };

  template<class _Transf>
  struct TransformImpl<_Transf, EigenSystem<tuint<1>, tuint<0>, EigenALP, bases::ALP>> : DefaultTransformImpl<_Transf, EigenSystem<tuint<1>, tuint<0>, EigenALP, bases::ALP>> {
    using EigenSys = EigenSystem<tuint<1>, tuint<0>, EigenALP, bases::ALP>;

    static inline TransformVariableBase* create_linear_axis_transform(const _Transf&, unsigned, const std::string&, const EigenSys&, unsigned) {
      return 0;
    }

    static inline TransformVariableBase* create_amplitude_axis_transform(const _Transf&, unsigned, const std::string&, const EigenSys&, unsigned) {
      return 0;
    }

    static inline TransformVariableBase* create_transform_1d(unsigned, const std::string&, const EigenSys&) {
      return 0;
    }

    static inline TransformVariableBase* create_marginalized_2d(const _Transf&, unsigned, unsigned, const std::string&, const EigenSys&, unsigned, unsigned) {
      return 0;
    }

    static inline double linear_integral(const _Transf& trf) {
      double s=0;
      const size_t _deg = trf.var_degree(0);
      for(unsigned l=0; l<=_deg; ++l) {
        for(unsigned m=0; m<=l; ++m) {
          if(!bases::ALP::zero_integral(l,m))
            s += trf.par(bases::alp_index(l,m))*bases::ALP::integral(l,m);
        }
      }
      return s;
    }
  };
}
#endif // sfi_ALP_H
