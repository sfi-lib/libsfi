#ifndef sfi_legendre_h
#define sfi_legendre_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ALP.h"

#include <math.h>
#include <vector>
#include <string>
#include "Basis.h"

namespace sfi {
  constexpr const unsigned g_legendre_max_deg = 60;

  namespace bases {
    struct Legendre : public BasisImpl<true, false>  {
      static const std::string& name () {
        static std::string s_name = "legendre_on";
        return s_name;
      }

      static inline double eval(double _x, int _k) {
        switch(_k) {
        case 0: return norm(0) * 1.0;
        case 1: return norm(1) * _x;
        case 2: return norm(2) * 0.5*(3.0*_x*_x - 1.0);
        case 3: return norm(3) * 0.5*_x*(5.0*_x*_x - 3.0);
        case 4: return norm(4) * 0.125*(3.0 + _x*_x*(35.0*_x*_x - 30.0));
        case 5: return norm(5) * 0.125*_x*(15.0 + _x*_x*(63.0*_x*_x - 70.0));
        }
        double p0(1.0), p1(_x), p2(0.0);
        for(int i=2; i<=_k; ++i) {
          p2 = ((2.0*double(i-1) + 1.0)*_x*p1 - double(i-1)*p0)/(double(i-1) + 1);
          p0 = p1;
          p1 = p2;
        }
        return norm(_k) * p2;
      }

      static constexpr inline bool zero_integral(int _k) { return _k > 0; }

      struct Constants {
        Constants() {
          for(unsigned i=0; i<g_legendre_max_deg; ++i) {
            m_norm[i] = sqrt((2.0*double(i) + 1.0)/2.0);
          }
        }
        inline double operator()(unsigned i) const {
          assert(i < g_legendre_max_deg && "Invalid degree");
          return m_norm[i];
        }
      protected:
        double m_norm[g_legendre_max_deg];
      };

      /**
       * Evaluate the integral
       * Warning: Should not be called for _k that gives zero integral, i.e. check first with zero_integral(_k)
       */
      static constexpr inline double integral(int) { return M_SQRT2; }

      static inline Constants& constants() {
        static Constants s_constants;
        return s_constants;
      }

      static inline double norm(unsigned n) { return constants()(n); }

      template<class T, class R, CompType ctype>
      static void compute(T _x, unsigned n, R* res) {
        assert(n < g_legendre_max_deg && "Invalid degree");
        res[0] = norm(0);
        T p0(0.0), p1(1.0);
        for(unsigned i=1; i<=n; i += 2) {
          p0 = ((2.0*T(i) - 1.0)*_x*p1 - T(i-1)*p0)/T(i);
          p1 = ((2.0*T(i) + 1.0)*_x*p0 - T(i)*p1)/T(i+1);
          res[i] = norm(i)*p0;
          res[i+1] = norm(i+1)*p1;
        }
      }

      /** Size of base function buffers, make sure it's even */
      static constexpr inline size_t buffer_size(size_t deg) { return deg+2; }
    };
  }
  template<> struct _t_is_basis<bases::Legendre> : ttrue {};

  inline double legendre_P_ON(double _x, int _k) { return bases::Legendre::eval(_x, _k); }

  template<class _Transf, class _EigenSys>
  struct LegendreTransformImpl : DefaultTransformImpl<_Transf, _EigenSys> {
    using Base = DefaultTransformImpl<_Transf, _EigenSys>;
    using AxisEigen = t_AxisEigenVectors<t_EigenVectors<_EigenSys>, 0>;
    using AxisTransf = Transform<AxisEigen, t_Basis<_EigenSys>>;

    // TODO: Remove, and use base case?
    static inline TransformVariableBase* create_linear_axis_transform(const _Transf& trf, unsigned d, const std::string& name, const _EigenSys& eigens, unsigned axis) {
      if(d >= eigens.dims()) return 0;
      else {
        auto *atrf = new AxisTransf(AxisEigen(eigens.max_degree()), name, true);
        atrf->is_amplitude(false);
        Base::linear_axis_transform(trf, *atrf, axis);
        return atrf;
      }
    }

    static inline TransformVariableBase* create_amplitude_axis_transform(const _Transf& trf, unsigned d, const std::string& name, const _EigenSys& eigens, unsigned axis) {
      if(d >= eigens.dims()) return 0;
      else {
        auto *atrf = new AxisTransf(AxisEigen(eigens.max_degree()*2), name, true);
        atrf->is_amplitude(false);
        amplitude_axis_transform(trf, *atrf, axis);
        return atrf;
      }
    }

    template<class _ResTransf>
    static inline void amplitude_axis_transform(const _Transf& trf, _ResTransf& res, unsigned axis) {
      assert(res.nparams() == (trf.var_degree(axis)*2 + 1) && "Invalid number of params");
      assert(trf.is_orthonormal() && "Must be ON");
      res.clear();
      double ep(0.0);
      unsigned j;
      for(auto& eit : trf.eigenvectors()) {
        j = eit[axis];
        ep = trf.par(eit.par_idx())*bases::Legendre::norm(j);
        auto idx = eit.indices();
        unsigned maxidx(idx.max_idx(axis));
        for(unsigned l=0; l<=maxidx; ++l) {
          idx.set(axis, l);
          bases::legendre_multiply(j,l,res.parameters(), ep*trf.par(idx.index())*bases::Legendre::norm(l));
        }
      }
      const size_t np(res.nparams());
      for(size_t i=0; i<np; ++i) res[i] *= 1.0/bases::Legendre::norm(i);
    }
  };

  template<class _Transf, unsigned _Dims, unsigned _Deg, class _EigenType>
  struct TransformImpl<_Transf, EigenSystem<tuint<_Dims>, tuint<_Deg>, _EigenType, teif<tneq<_EigenType, EigenSquare>::value(), bases::Legendre>>> :
  LegendreTransformImpl<_Transf, EigenSystem<tuint<_Dims>, tuint<_Deg>, _EigenType, bases::Legendre>> { };
}

#endif // sfi_legendre_h
