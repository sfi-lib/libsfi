#ifndef sfi_fourier_h
#define sfi_fourier_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <math.h>
#include <string>
#include <complex>

#include "cosine.h"

namespace sfi {

  namespace bases {
    /**
     * @class Fourier
     */
    struct Fourier : public BasisImpl<true, false> {
      static const std::string& name () {
        static std::string s_name = "fourier_on";
        return s_name;
      }

      static inline std::complex<double> eval(double _x, int l) {
        return !l ? std::complex<double>(norm(0),0.0) : std::polar(1.0, double(l)*(transform_x(_x)));
      }

      static constexpr inline bool zero_integral(int _k) { return _k > 0; }
      /**
       * Evaluate the integral
       * Warning: Should not be called for _k that gives zero integral, i.e. check first with zero_integral(_k)
       */
      static inline double integral(int /*_k*/) {
        static const double s_sqrt2 = sqrt(2.0);
        return s_sqrt2;
      }

      static inline double norm(unsigned d) {
        static const double s_inv_sqrt2 = 1.0/sqrt(2.0);
        return (d == 0) ? s_inv_sqrt2 : 1.0;
      }

      template<class T, class R, CompType ctype>
      static void compute(T __x, unsigned n, R* res) {
        std::complex<T> p0(1.0, 0.0);
        res[0] = norm(0)*p0;
        const T xtr((ctype == Basis::Project) ? -transform_x(__x) : transform_x(__x));
        T _x(cos(xtr));
        std::complex<T> p1(_x, sin(xtr));
        res[1] = p1;
        if(n == 1) return;
        _x *= 2.0;
        ++res;
        for (unsigned i=2; i <= n; i += 2) {
          p0.real(std::fma(_x, p1.real(), - p0.real()));
          p0.imag(std::fma(_x, p1.imag(), - p0.imag()));
          p1.real(std::fma(_x, p0.real(), - p1.real()));
          p1.imag(std::fma(_x, p0.imag(), - p1.imag()));
          *(++res) = p0;
          *(++res) = p1;
        }
      }

      /** Size of base function buffers, make sure it's even */
      static constexpr inline size_t buffer_size(size_t deg) { return (deg+2) & ~0x1; }

      /** Map -1 -> 1 to 0 -> pi */
      template<class _T>
      static inline _T transform_x(_T x) { return M_PI*(x + 1.0); }
    };
  }
  template<> struct _t_coefficient_type<bases::Fourier> : tvalued<std::complex<double>> {};
  template<> struct _t_is_basis<bases::Fourier> : ttrue {};

  inline double dm(int m) { return (m==0) ? 0.5 : 1.0; }

  inline double ejk(unsigned j, unsigned k) { return (j==k) ? 1.0 : 2.0; }

  inline double iejk(unsigned j, unsigned k) { return (j==k) ? 1.0 : 0.5; }

  template<class _Transf, unsigned _Dims, unsigned _Deg, class _EigenType>
  struct TransformImpl<_Transf, EigenSystem<tuint<_Dims>, tuint<_Deg>, _EigenType, teif<tneq<_EigenType, EigenSquare>::value(), bases::Fourier>>> :
  TrigonometricTransformImpl<_Transf, EigenSystem<tuint<_Dims>, tuint<_Deg>, _EigenType, bases::Fourier>> {
    using Base = TrigonometricTransformImpl<_Transf, EigenSystem<tuint<_Dims>, tuint<_Deg>, _EigenType, bases::Fourier>>;
    using EigenSys = EigenSystem<tuint<_Dims>, tuint<_Deg>, _EigenType, bases::Fourier>;
    using AxisTransf = typename Base::AxisTransf;

    static inline void amplitude_axis_transform(const _Transf& trf, AxisTransf& res, unsigned axis) {
      using Coeff = t_CoefficienctType<_Transf>;
      using TCoeff = t_CoefficienctType<AxisTransf>;
      res.clear();
      Coeff p1(0.0), p2(0.0);
      unsigned j;
      const unsigned deg = res.var_degree(0);
      for(auto& eit : trf.eigenvectors()) {
        j = eit[axis];
        p1 = trf.par(eit.par_idx());
        auto idx = eit.indices();
        unsigned maxidx(idx.max_idx(axis));
        for(unsigned l=0; l<=min(j,maxidx); ++l) {
          idx.set(axis, l);
          p2 = trf.par(idx.index());
          double t = 2.0*ejk(j,l)*dm(j)*dm(l)*bases::Fourier::norm(j)*bases::Fourier::norm(l)*iejk(j,0)*iejk(l,0);
          if((j+l)<= deg) res.par(l+j) += Base::template coeff_trf<TCoeff, Coeff>(p1*p2*t/bases::Fourier::norm(j+l));
          res.par(j-l) += Base::template coeff_trf<TCoeff, Coeff>(p1*std::conj(p2)*t/bases::Fourier::norm(j-l));
        }
      }
      // TODO: Speed up by dividing here!
      //const size_t np(res.nparams());
      //for(size_t i=0; i<np; ++i) res[i] *= 1.0/AxisTransf::norm(0, i);
    }

    static inline TransformVariableBase* create_amplitude_axis_transform(const _Transf& trf, unsigned d, const std::string& name, const EigenSys& eigens, unsigned axis) {
      if(d >= eigens.dims()) return 0;
      else {
        auto *res = new AxisTransf(t_EigenVectors<AxisTransf>(eigens.max_degree()), name, true);
        amplitude_axis_transform(trf, *res, axis);
        return res;
      }
    }
  };
}

#endif // sfi_fourier_h
