#ifndef sfi_chebyshev_h
#define sfi_chebyshev_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <math.h>
#include <cmath>

#include <vector>
#include <string>
#include "Basis.h"

namespace sfi {
  static const double* chebyshev_first_norm() {
    static const double __chebyshev_first_norm[2] = { sqrt(1.0/M_PI), sqrt(2.0/M_PI)};
    return __chebyshev_first_norm;
  }

  struct ChebyshevFirstNorm {
    static inline double norm(unsigned k) {
      return chebyshev_first_norm()[k>0];
    }
  };

  /**
   * Chebyshev of first kind
   */
  inline double chebyshev_first(double _x, int _k) {
    double t1 = 1;
    if (_k==0) return t1;
    if (_k%2==0) {  // only do even functions
      _x=2.*_x*_x-1;
      _k/=2.;
    }
    double t2 = _x;
    if (_k==1) return t2;
    double t3(0.0);
    for (int i=2; i<=_k; ++i) {
      t3 = 2.*_x*t2 - t1;
      t1=t2;
      t2=t3;
    }
    return t3;
  }

  inline double chebyshev_first_ON(double _x, int _k) {
    double t1 = 1;
    if (_k==0) return chebyshev_first_norm()[0]*t1;
    if (_k%2==0) {  // only do even functions
      _x=2.*_x*_x-1;
      _k/=2.;
    }
    double t2 = _x;
    if (_k==1) return chebyshev_first_norm()[1]*t2;
    double t3(0.0);
    for (int i=2; i<=_k; ++i) {
      t3 = 2.*_x*t2 - t1;
      t1=t2;
      t2=t3;
    }
    return chebyshev_first_norm()[1]*t3;
  }

  /**
   * Chebyshev of second kind
   */
  inline double chebyshev_second(double _x, int _k) {
    if(!_k) return 1.0;
    if(_k == 1) return 2.0*_x;
    double u0(1), u1(2.0*_x), u(0.0);
    for(int i=2; i<=_k; ++i) {
      u = 2.0*_x*u1 - u0;
      u0 = u1;
      u1 = u;
    }
    return u;
  }

  namespace bases {
    struct Chebyshev : public BasisImpl<true, true> {
      static const std::string& name () {
        static std::string s_name = "chebyshev_on";
        return s_name;
      }

      static inline double eval(double _x, int _k) { return chebyshev_first_ON(_x, _k); }
      static inline double eval_derivative(unsigned order, double _x, int _k) {
        if(order == 1) return (_k==0) ? 0.0 : double(_k)*chebyshev_second(_x, _k-1)*chebyshev_first_norm()[_k>0];
        else return (_k<2) ? 0.0 : double(_k)*(double(_k+1)*chebyshev_first(_x, _k) - chebyshev_second(_x, _k))/(_x*_x - 1.0)*chebyshev_first_norm()[_k>0];
      }
      static inline double weight(double _x) { return 1.0/(sqrt(1.0 - _x *_x)); }
      static constexpr inline bool zero_integral(int _k) { return (_k & 0x1) == 1; }
      /**
       * Evaluate the integral
       * Warning: Should not be called for _k that gives zero integral, i.e. check first with zero_integral(_k)
       */
      static inline double integral(int _k) { return chebyshev_first_norm()[(_k>0)]*2.0/(1.0 - double(_k*_k)); }

      static inline double norm(unsigned n) { return chebyshev_first_norm()[(n>0)]; }

      template<class T, class R, CompType ctype>
      static void compute(T _x, unsigned n, R* res) {
        if(ctype == Basis::EvalDeriv) {
          res[0] = 0;
          T p0(0.0), p1(norm(1));
          res[1] = p1;
          if(n == 1) return;
          _x *= 2.0;
          for (unsigned i=2; i <= n; i += 2) {
            p0 = std::fma(_x, p1, - p0);
            p1 = std::fma(_x, p0, - p1);
            res[i] = double(i)*p0;
            res[i+1] = double(i+1)*p1;
          }
        } else if(ctype == Basis::EvalDeriv2) {
          res[1] = res[0] = 0;
          if(n == 1) return;
          double denom = 1.0/(_x*_x - 1.0);
          T Tp0(norm(1)), Tp1(norm(1)*_x);
          T Up0(norm(1)), Up1(2.0*norm(1)*_x);
          _x *= 2.0;
          for (unsigned i=2; i <= n; ++i) {
            Tp0 = std::fma(_x, Tp1, - Tp0);
            Up0 = std::fma(_x, Up1, - Up0);
            res[i] = double(i)*(double(i+1)*Tp0 - Up0)*denom;
            std::swap(Tp0,Tp1);
            std::swap(Up0,Up1);
          }
        } else {
          double w((ctype == Basis::Project) ? weight(_x) : 1.0);
          res[0] = norm(0) * w;
          w *= norm(1);
          T p1(_x*w);
          res[1] = p1;
          if(n == 1) return;
          T p0(w);
          _x *= 2.0;
          ++res;
          for (unsigned i=2; i <= n; i += 2) {
            p0 = std::fma(_x, p1, - p0);
            p1 = std::fma(_x, p0, - p1);
            *(++res) = p0;
            *(++res) = p1;
          }
        }
      }

      /** Size of base function buffers, make sure it's even */
      static constexpr inline size_t buffer_size(size_t deg) { return (deg+2) & ~0x1; }
    };
  }
  template<> struct _t_is_basis<bases::Chebyshev> : ttrue {};
}
#endif // sfi_chebyshev_h
