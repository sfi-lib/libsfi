#ifndef sfi_cosine_h
#define sfi_cosine_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <math.h>
#include <string>

#include "chebyshev.h"
#include "TransformImpl.h"

namespace sfi {
  namespace bases {
    struct Cosine : public BasisImpl<true, false> {
      static const std::string& name () {
        static std::string s_name = "cos_on";
        return s_name;
      }

      static inline double eval(double _x, int _k) { return norm(_k)*cos(double(_k)*transform_x(_x)); }
      static inline double eval_derivative(unsigned order, double _x, int _k) {
        if(order == 1) return -norm(_k)*0.5*M_PI*double(_k)*sin(double(_k)*transform_x(_x));
        else return -norm(_k)*square(double(_k)*0.5*M_PI)*cos(double(_k)*transform_x(_x));
      }

      static constexpr inline bool zero_integral(int _k) { return _k > 0; }

      /**
       * Evaluate the integral
       * Warning: Should not be called for _k that gives zero integral, i.e. check first with zero_integral(_k)
       */
      static inline double integral(int /*_k*/) {
        static const double s_sqrt2 = sqrt(2.0);
        return s_sqrt2;
      }

      static inline double norm(unsigned n) {
        static const double s_inv_sqrt2 = 1.0/sqrt(2.0);
        return n ? 1.0 : s_inv_sqrt2;
      }

      template<class T, class R, CompType ctype>
      static void compute(T __x, unsigned n, R* res) {
        T _x(cos(transform_x(__x)));
        *res = (ctype == EvalDeriv) ? 0.0 : norm(0);
        T p1((ctype == EvalDeriv) ? -half_pi*sin(transform_x(__x)) : _x);
        *(++res) = p1;
        if(n == 1) return;
        T p0((ctype == EvalDeriv) ? 0.0 : 1.0);
        _x *= 2.0;
        for (unsigned i=2; i <= n; i += 2) {
          p0 = std::fma(_x, p1, - p0);
          p1 = std::fma(_x, p0, - p1);
          *(++res) = (ctype == EvalDeriv) ? T(i)*p0 : p0;
          *(++res) = (ctype == EvalDeriv) ? T(i+1)*p1 : p1;
        }
      }

      /** Size of base function buffers, make sure it's even */
      static constexpr inline size_t buffer_size(size_t deg) { return (deg+2) & ~0x1; }

      static constexpr double half_pi = 0.5*M_PI;

      /** Map -1 -> 1 to 0 -> pi */
      template<class _T>
      static inline _T transform_x(_T x) { return (x + 1.0)*half_pi; }
    };
  }

  template<> struct _t_is_basis<bases::Cosine> : ttrue {};

  template<class _Transf, class _EigenSys>
  struct TrigonometricTransformImpl : DefaultTransformImpl<_Transf, _EigenSys> {
    using Base = DefaultTransformImpl<_Transf, _EigenSys>;
    using AxisEigen = t_AxisEigenVectors<t_EigenVectors<_EigenSys>, 0>;
    using AxisTransf = Transform<AxisEigen, t_Basis<_EigenSys>>;
    using Eigen2D = EigenVectors<2, c_eigen_degree<t_EigenVectors<_EigenSys>, 0>(), t_EigenType<t_EigenVectors<_EigenSys>, 0>>;
    using Transf2D = Transform<Eigen2D, t_Basis<_EigenSys, 0>>;

    // TODO: Isn't this the base case, and can be removed?
    static inline TransformVariableBase* create_linear_axis_transform(const _Transf& trf, unsigned d, const std::string& name, const _EigenSys& eigens, unsigned axis) {
      if(d >= eigens.dims()) return 0;
      else {
        auto *atrf = new AxisTransf(AxisEigen(eigens.max_degree()), name, true);
        atrf->is_amplitude(false);
        Base::linear_axis_transform(trf, *atrf, axis);
        return atrf;
      }
    }

    static inline TransformVariableBase* create_amplitude_axis_transform(const _Transf& trf, unsigned d, const std::string& name, const _EigenSys& eigens, unsigned axis) {
      if(d >= eigens.dims()) return 0;
      else {
        auto *atrf = new AxisTransf(AxisEigen(eigens.max_degree()*2), name, true);
        atrf->is_amplitude(false);
        amplitude_axis_transform(trf, *atrf, axis);
        return atrf;
      }
    }

    template<class _ResTransf>
    static inline void amplitude_axis_transform(const _Transf& trf, _ResTransf& res, unsigned axis) {
      assert(res.nparams() == (trf.var_degree(axis)*2 + 1) && "Invalid number of params");
      assert(trf.is_orthonormal() && "Must be ON");
      using Coeff = t_CoefficienctType<_Transf>;
      using TCoeff = t_CoefficienctType<_ResTransf>;
      // TODO: take norm for specific axis
      res.clear();
      Coeff ep(0.0), pm(0.0);
      unsigned j;
      for(auto& eit : trf.eigenvectors()) {
        j = eit[axis];
        ep = trf.par(eit.par_idx())*_Transf::norm(0,j);
        auto idx = eit.indices();
        unsigned maxidx(idx.max_idx(axis));
        for(unsigned l=0; l<=maxidx; ++l) {
          idx.set(axis, l);
          pm = trf.par(idx.index())*_Transf::norm(0, l);
          res.par(j+l) += Base::template coeff_trf<TCoeff, Coeff>(ep*pm);
          if(l > j) res.par(l-j) += Base::template coeff_trf<TCoeff, Coeff>(ep*pm);
          else res.par(j-l) += Base::template coeff_trf<TCoeff, Coeff>(ep*pm);
        }
      }
      const size_t np(res.nparams());
      for(size_t i=0; i<np; ++i) res[i] *= 1.0/(2.0 * _Transf::norm(0, i));
    }

    /**
     * Create the covariance matrix for a cosine Transform.
     * cov(i,j) = 0.5a(i-j) + 0.5a(i+j) - ai aj
     * This makes the approximation that i+j<=N,
     * which means that the second term is 0 in case i+j>N.
     * If an overall poisson term should be included this amounts to -removing- the term -c_i c_j.
     * @param include_poisson If the poisson term should be included
     */
    template<class _T>
    static inline void create_covariance(_T& trf, bool include_poisson, teif<tand<teq<_T,_Transf>,teq<t_CoefficienctType<_T>,double>>::value()>* = 0) {
      const unsigned N = trf.npar();
      const double I = trf.integral_pdf();
      const double I1 = I;// + 1.0;

      const double s1 = I1/(I*I);
      const double s2 = 0.5*I1/I;
      trf.compute_uncertainty(false);
      trf.compute_covariance(true);
      auto& c2 = trf.covariance();
      c2.rows(N);
      c2.clear();
      if(include_poisson) {
        for (unsigned r = 0; r < N; ++r) c2(r, 0) += s1 * trf[r] * trf[0];
      }
      for (unsigned r = 1; r < N; ++r) {
        c2(r, r) = 0.5*trf[0]/bases::Cosine::norm(0);
        for (unsigned c = 1; c <= r; ++c) {
          if(!include_poisson) c2(r, c) -= s1 * trf[r] * trf[c];
          if (c < r) c2(r, c) += s2 * trf[r - c];
          if((r+c) < N) c2(r, c) += s2 * trf[r + c];
        }
      }
    }
    template<class _T>
    static inline void create_covariance(_T& /*trf*/, bool /*include_poisson*/, teif<!tand<teq<_T,_Transf>,teq<t_CoefficienctType<_T>,double>>::value()>* = 0) {
    }
  };


  template<class _Transf, unsigned _Dims, unsigned _Deg, class _EigenType>
  struct TransformImpl<_Transf, EigenSystem<tuint<_Dims>, tuint<_Deg>, _EigenType, teif<tneq<_EigenType, EigenSquare>::value(), bases::Cosine>>> :
  TrigonometricTransformImpl<_Transf, EigenSystem<tuint<_Dims>, tuint<_Deg>, _EigenType, bases::Cosine>> { };
}

#endif // sfi_cosine_h
