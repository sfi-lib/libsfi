/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ALP.h"

#include "ClebschGordan.h"

using namespace sfi;
using namespace sfi::bases;

double
sfi::bases::fact_over_fact_d(uint64_t i, uint64_t j) {
  if(j > i) {
    std::cout<<"fact_over_fact_d Invalid "<<i<<" < "<<j<<std::endl;
    return 0.0;
  }
  if(i == j) return 1.0;
  if((i-j) < 10) {
    uint64_t res = 1;
    for(uint64_t k=j+1; k<=i; ++k) {
      if((res*k) < res) {
        std::cout<<"fact_over_fact_d overflow! "<<res<<" "<<(res*k)<<" - "<<k<<" i="<<i<<" j="<<j<<std::endl;
      }
      res *= k;
    }
    return double(res);
  }
  else {
    double res = double(j+1);
    for(uint64_t k=j+2;k<=i; ++k) res *= double(k);
    return res;
  }
}

double sfi::bases::sqrt_fact_over_fact_d(uint64_t i, uint64_t j) {
  if(j > i) {
    return 1.0/sqrt_fact_over_fact_d(j,i);
  }
  if(i == j) return 1.0;
  double res = sqrt(j+1.0);
  for(uint64_t k=j+2;k<=i; ++k) res *= sqrt(k);
  return res;
}

bool
sfi::bases::alp_valid_indices(int l1, int m1, int l2, int m2) {
  return !(((abs(m1+m2) > (l1+l2)) || (l1 < 0) || (l2 < 0) || (abs(m1) > l1) || (abs(m2) > l2)));
}

double
sfi::bases::alp_multiply(int l1, int m1, int l2, int m2, double* buff) {
  auto& cgtab = ClebschGordanTable::instance();
  double res = 0;
  if(!alp_valid_indices(l1, m1, l2, m2)) {
    std::cout<<"alp_multiply ERROR "<<l1<<", "<<m1<<", "<<l2<<", "<<m2<<std::endl;
    return 0;
  }
  int M = m1+m2;
  for(int L = max((int)abs(M), (int)abs(l1 - l2)); L <= (l1+l2); ++L) {
    if(is_even(l1+l2+L)) {
      double cg1 = cgtab.coefficient(l1, l2, L, M, m1);
      double cg2 = cgtab.coefficient(l1, l2, L, 0, 0);
      double Plm = buff[bases::alp_index(L,M)]/sqrt_fact_over_fact_d(L+M, L-M);
      res += cg1*cg2*Plm;
    }
  }
  double ff = sqrt_fact_over_fact_d(l1 + m1, l1 - m1)*sqrt_fact_over_fact_d(l2 + m2, l2 - m2);
  res = res*ff;
  return res;
}

void
sfi::bases::legendre_multiply(int l1, int l2, Vector<double>& par, double p) {
  if(!alp_valid_indices(l1, 0, l2, 0)) {
    std::cout<<"legendre_multiply ERROR "<<l1<<", "<<l2<<std::endl;
    return;
  }
  auto& cgtab = ClebschGordanTable::instance();
  for(int L = abs(l1 - l2); L <= (l1+l2); ++L) {
    if(is_even(l1+l2+L)) par[L] += square(cgtab.coefficient(l1, l2, L, 0, 0))*p;
  }
}

bool
sfi::bases::alp_param_multiply(int l1, int m1, int l2, int m2, Vector<double>& res, double p) {
  auto& cgtab = ClebschGordanTable::instance();
  if(!alp_valid_indices(l1, m1, l2, m2)) {
    std::cout<<"alp_param_multiply ERROR "<<l1<<", "<<m1<<", "<<l2<<", "<<m2<<std::endl;
    return false;
  }
  int M = m1+m2;
  double ff = sqrt_fact_over_fact_d(l1 + m1, l1 - m1)*sqrt_fact_over_fact_d(l2 + m2, l2 - m2);
  for(int L = max((int)abs(M), (int)abs(l1 - l2)); L <= (l1+l2); ++L) {
    if(is_even(l1+l2+L)) {
      double cg1 = cgtab.coefficient(l1, l2, L, M, m1);
      double cg2 = cgtab.coefficient(l1, l2, L, 0, 0);
      res[alp_index(L,M)] += p*cg1*cg2*ff/sqrt_fact_over_fact_d(L+M, L-M);
    }
  }
  return true;
}

double
sfi::bases::alp_2_integral(int l1, int m1, int l2, int m2) {
  if((m1 < 0) && (m2 >= 0)) {
    return (alp_2_integral(l1, -m1, l2, m2)*negone_to(m1))/square(sqrt_fact_over_fact_d(l1-m1, l1+m1));
  }
  else if((m2 < 0) && (m1 >= 0)) {
    return (alp_2_integral(l1, m1, l2, -m2)*negone_to(m2))/square(sqrt_fact_over_fact_d(l2-m2, l2+m2));
  }
  else if((m1 < 0) && (m2 < 0)) {
    return (alp_2_integral(l1, -m1, l2, -m2)*negone_to(m1+m2))/(square(sqrt_fact_over_fact_d(l1-m1, l1+m1))*square(sqrt_fact_over_fact_d(l2-m2, l2+m2)));
  }
  auto& cgtab = ClebschGordanTable::instance();
  double res = 0;
  int M = m1+m2;
  if(M < 0) {
    std::cout<<"alp_2_integral ERROR "<<l1<<", "<<m1<<", "<<l2<<", "<<m2<<" => M="<<M<<" < 0"<<std::endl;
    return 0;
  }
  if(!alp_valid_indices(l1, m1, l2, m2)) {
    std::cout<<"alp_2_integral ERROR "<<l1<<", "<<m1<<", "<<l2<<", "<<m2<<std::endl;
    return 0;
  }
  if(m1 == m2) {
    if(l1 == l2) return square(sqrt_fact_over_fact_d(l1+m1, l1-m1))*2.0/(2.0*l1 + 1);
    else return 0.0;
  }

  for(int L = max((int)abs(M), (int)abs(l1 - l2)); L <= (l1+l2); ++L) {
    if(is_even(l1+l2+L)) {
      double cg1 = cgtab.coefficient(l1, l2, L, M, m1);
      double cg2 = cgtab.coefficient(l1, l2, L, 0, 0);
      double I = bases::ALP::integral(L, M);
      I = I*sqrt_fact_over_fact_d(L-M, L+M);
      res += cg1*cg2*I;
    }
  }
  double ff = sqrt_fact_over_fact_d(l1 + m1, l1 - m1)*sqrt_fact_over_fact_d(l2 + m2, l2 - m2);
  return res*ff;
}

bool
sfi::bases::alp_2_zerointegral(int l1, int m1, int l2, int m2) {
  if ((m1 == m2) && (l1 != l2)) return true;
  bool non_zero_term = false;
  int M = m1 + m2;
  for(int L = max((int)abs(M), (int)abs(l1 - l2)); L <= (l1+l2); ++L) {
    if(is_even(l1+l2+L) && !bases::ALP::zero_integral(L, M)) non_zero_term = true;
  }
  return !non_zero_term;
}

double
sfi::bases::alp_integral(unsigned l, int m) {
  if(abs(m) > l) {
    std::cout<<"alp_integral ERROR "<<l<<", "<<m<<std::endl;
    exit(1);
  }
  if(m == 0) {
    return (l==0) ? 2.0 : 0.0;
  }
  if(m < 0) {
    return negone_to(m)*bases::alp_integral(l, -m)*square(sqrt_fact_over_fact_d(l + m, l - m));
  }
  double res = negone_to(l) + negone_to(m);
  if((l&1)!= (m&1)) return 0.0;

  // 2^{m-2}
  {
    if(m < 2) res *= 1.0/((double)(1LL<<(2-m)));
    else if(m > 2) res *= ((double)(1LL<<(m-2)));
  }
  return res * m * gamma_half(l)*gamma_half(l+m+1)/(factorial_half(l - m)*gamma_half(l+3));
}

ALP::Constants::Constants() {
  size_t base = 0;
  for(unsigned l=0; l<=2*g_alp_max_L; ++l) {
    m_polar[l] = sqrt((2.0*double(l) + 1.0)/(4.0));
    base = alp_index(l, 0);
    for(unsigned m=0; m<=l; ++m) {
      //double d = 1.0;
      //for(unsigned i=(l-m+1);i<=(l+m);++i) d *= double(i);
      //m_norms[base + m] = sqrt((2.0*double(l) + 1.0)/d);
      m_integrals[base + m] = alp_integral(l,m);
      if((l > 1) && (m < l-1))  m_denom[base + m] = -1.0/double((l+m+1)*(l-m));
      else m_denom[base + m] = 0.0;
    }
  }
}

const ALP::Constants&
ALP::Constants::instance() {
  static const Constants s_instance;
  return s_instance;
}
