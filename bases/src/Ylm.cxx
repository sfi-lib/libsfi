/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Ylm.h"

#include "ClebschGordan.h"

using namespace sfi;
using namespace sfi::bases;

std::complex<double>
sfi::bases::ylm_multiply(int l1, int m1, int l2, int m2, std::complex<double>* buff) {
  auto& cgtab = ClebschGordanTable::instance();
  std::complex<double> res = 0;
  if(!alp_valid_indices(l1, m1, l2, m2)) {
    std::cout<<"ylm_multiply ERROR "<<l1<<", "<<m1<<", "<<l2<<", "<<m2<<std::endl;
    return 0;
  }
  int M = m1+m2;
  for(int L = max((int)abs(M), (int)abs(l1 - l2)); L <= (l1+l2); ++L) {
    // TODO: Use ??
    if(((l1+l2+L)&1) == 0) {
      double cg1 = cgtab.coefficient(l1, l2, L, M, m1);
      double cg2 = cgtab.coefficient(l1, l2, L, 0, 0);
      double f = sqrt((M == 0 ? 4.0 : 2.0)/(2.0*L + 1.0));
      std::complex<double> Ylm = buff[bases::ylm_index(L,M)];
      res += f*cg1*cg2*Ylm;
    }
  }
  res *= sqrt(((2.0*l1 + 1.0)*(2.0*l2 + 1.0))/((m1 == 0 ? 4.0 : 2.0)*(m2 == 0 ? 4.0 : 2.0)));
  return res;
}

bool
sfi::bases::ylm_param_multiply(int l1, int m1, int l2, int m2, Vector<std::complex<double>>&res, const std::complex<double>& p) {
  auto& cgtab = ClebschGordanTable::instance();
  if(!alp_valid_indices(l1, m1, l2, m2)) {
    std::cout<<"ylm_param_multiply ERROR "<<l1<<", "<<m1<<", "<<l2<<", "<<m2<<std::endl;
    return false;
  }
  int M = m1+m2;
  if(M < 0) {
    std::cout<<"ylm_param_multiply ERROR "<<l1<<", "<<m1<<", "<<l2<<", "<<m2<<" M ("<<M<<") < 0"<<std::endl;
    return false;
  }
  double ff = sqrt(((2.0*l1 + 1.0)*(2.0*l2 + 1.0))/(4.0*4.0));
  for(int L = max((int)abs(M), (int)abs(l1 - l2)); L <= (l1+l2); ++L) {
    if(((l1+l2+L)&1) == 0) {
      double cg1 = cgtab.coefficient(l1, l2, L, M, m1);
      double cg2 = cgtab.coefficient(l1, l2, L, 0, 0);
      double f = sqrt(4.0/(2.0*L + 1.0));
      auto cc = p*f*ff*cg1*cg2/ylm_cm(M);
      res[ylm_index(L,M)] += cc;
    }
  }
  return true;
}

Ylm::Constants::Constants():ALP::Constants() {
  SqrtPartFactorial spf(g_alp_max_L);
  do {
    double r = sqrt((2.0*double(spf.l()) + 1.0)/(((spf.m()==0) ? 4.0 : 2.0)));
    m_norms[spf.index()] = r/spf();
    m_neg_norms[spf.index()] = r*spf();
  } while(spf.next());
}

const Ylm::Constants&
Ylm::Constants::instance() {
  static const Constants s_instance;
  return s_instance;
}
