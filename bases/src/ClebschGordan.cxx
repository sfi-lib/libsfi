/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ClebschGordan.h"

using namespace sfi;

ClebschGordanTable::ClebschGordanTable() {
  build_table(2*g_alp_max_L);
}

const ClebschGordanTable&
ClebschGordanTable::instance() {
  static ClebschGordanTable s_instance;
  return s_instance;
}

void
ClebschGordanTable::build_table(unsigned _L_max) {
  for(int l1=0; l1<=(int)_L_max; ++l1) {
    for(int l2=0; l2<=l1; ++l2) {
      for(int L = l1-l2; L<=(l1+l2); ++L) {
        // std::cout<<" +++ <"<<l1<<", "<<l2<<" | "<<L<<">"<<std::endl;
        // handle M=L
        // here M >= l1 - l2 => l1 <= M+l2 and l1 is the upper bound for m1
        // Cp(l1, m1-1)<l1 m1-1 l2 m2|LL> + Cp(l2 m2-1)<l1 m1 l2 m2-1|LL> = 0
        // <l1 m1 l2 m2-1|LL> = - Cp(l1, m1-1)<l1 m1-1 l2 m2|LL> / Cp(l2 m2-1)
        // (m2-1 => m2) <l1 m1 l2 m2|LL> = - Cp(l1, m1-1)<l1 m1-1 l2 m2+1|LL> / Cp(l2 m2)
        // c0 x0 / d0 = x1 , c1 x1 / d1 = x2 = c0 c1 x0 /(d0 d1)  ...
        // x0^2 + x1^2 ... = 1 = x0^2(1 + c0/d0(1 + c1/d1(1 + ...)))
        //do_log_debug(cg_state(l1, l2, L, L, l1)<<" # terms: "<<(l1 - L + l2 + 1));
        double sum = 1;
        for(int m1=(L - l2)+1; m1<=l1; ++m1) {
          int m2 = L - m1;
          sum = (sum * double(cg_Cplus2(l2, m2))) / double(cg_Cplus2(l1, m1-1)) + 1.0;
        }
        double cg0 = sqrt(1.0/sum);
        for(int m1=l1; m1>=L-l2; --m1) {
          int m2 = L - m1;
          // TODO: don't compute index, iterate
          m_table[cg_index(l1, l2, L, L, m1)] = cg0;
          //do_log_debug(" "<<cg_state(l1, l2, L, L, m1)<<" = "<<cg0);
          cg0 = - cg0 * cg_Cplus(l2, m2) / cg_Cplus(l1, m1-1);
        }

        for(int M = L-1; M>=0; --M) {
          for(int m1=min(l1, l2 + M); m1>=max(-l1, M - l2); --m1) {
            int m2 = M - m1;
            double cg = std::nan("A");

            cg = (cg_Cmin(l1, m1+1)*coefficient(l1, l2, L, M+1, m1+1) + cg_Cmin(l2, m2+1)*coefficient(l1, l2, L, M+1, m1))/cg_Cmin(L,M+1);
            m_table[cg_index(l1, l2, L, M, m1)] = cg;
            //do_log_debug(" "<<cg_state(l1, l2, L, M, m1)<<" ->  "<<cg_state(l1, l2, L, M+1, m1+1)<<" + "<<cg_state(l1, l2, L, M+1, m1)<<" = "<<coefficient(l1, l2, L, M+1, m1+1)<<" + "<<coefficient(l1, l2, L, M+1, m1));
          }
        }
      }
    }
  }
}
