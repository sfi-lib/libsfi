{
  speed: <default:0.25, help:"Amp transform speed">,
  nevents: <default:20000U>,
  max_diff: <default:1e-4, help:"Amp transform tolerance">,
  file_prefix: <default:"explots/example_8">,
  min_degrees: <default:8U>,
  max_degrees: <default:50U>,
  ce_parts: <default:5U>,
  do_ce:<default:false>,
  analysis: {
    name : "spiral 2D",
    
    channels : {
      spiral : {
        variables:[
          {name:"x", title:"x", external_interval:[-1.0,1.0]},
          {name:"y", title:"y", external_interval:[-1.0,1.0]}
        ],
        builder:{name:"xy", type_id:"spiral", library:"libSFIExamples.dylib", dims:2},
        sources : {
          S : {is_signal:true, index:0, nevents:nevents},
        },
      }
    }
  },
  transformations: {
    amp: {
      name: "amp", print_level: "warning"
      max_diff: max_diff, max_iterations: 100,
      normalize: true, speed: speed,
      nested: {
        name: "p",
        print_level: "warning",
        eigen : { degree : 100U }
      }
    },
    amp_opt: {
      name: "amp_opt", max_diff: max_diff, max_iterations: 100, normalize: true, speed: speed,
      nested: {
        name: "p", eigen : { degree : 20U }
      }
    },
    amp_prune: {
      name: "amp_prune", max_diff: max_diff, max_iterations: 200, normalize: true, speed: 0.5,
      nested: {
        print_level: "debug",
        name: "p", eigen : { degree : 100U }
        prune_significance: 3.0,
      }
    },
    amp_os: {
      name: "amp_os", max_diff: max_diff, max_iterations: 200, normalize: true, speed: 0.1,
      nested: {
        name: "p", eigen : { degree : 100U }
        do_oversampling: true,
        oversampling_smear_all: true,
        oversampling: 2,
        oversampling_width: 0.1
      }
    },
    bs: {
      name:"bs", nrepeats:20, poisson:false, ignore_fails:false,nevents:1000,
      nested: {
        name: "amp", max_diff: max_diff, max_iterations: 100, normalize: true, speed: speed,
        print_level:"verbose",
        nested: {
          name: "p", eigen : { degree : 10U }
        }
      },
    }
  },
  plots: {
    opt: { npointsx:30, npointsy:30, canvas:"PDF opt", color:1, draw_string: "SURF"}
    prune: { npointsx:30, npointsy:30, canvas:"PDF prune", color:1, draw_string: "SURF"}
    os: { npointsx:30, npointsy:30, canvas:"PDF OS", color:1, draw_string: "SURF"}
    bs: { npointsx:30, npointsy:30, canvas:"PDF BS", color:1, draw_string: "SURF"}
  }
}

