#ifndef sfi_ParamEst_h
#define sfi_ParamEst_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "OvertrainingTest.h"
#include "example_util.h"
#include "ParamSampleBuilder.h"
#include "transform_functions.h"
#include "transform_plots.h"
#include "stat_functions.h"
#include "sparse_system.h"
#include "full_system.h"
#include "cosine.h"
#include "DataTransformPlot1D.h"
#include "PseudoExperiment.h"
#include "CombinationTransformation.h"

namespace sfi {

  template<class _PSTransfo, class _CTransfo>
  class ParamEst {
  public:
    ParamEst(const std::string& prefix, Analysis& ana, const OptionMap& opts):m_prefix(prefix),
      m_log(Log::get(ana.main_context(), "ParamEst{"+prefix+"}")), m_ana(ana), m_pstransfo(ana.main_context(), opts.sub("transformations.tpl_ps")) {
    }

    /** Get the coplete result */
    auto create_template_res(SampleSet<1U>& samples, OptionMap& opts, bool plot_pdf = false, bool _plot_slices = false, bool check_overtraining = true) {
      auto tres = perform_transformations(m_ana.main_context(), m_log, samples, m_pstransfo, EvenEventSel());
      auto tpl_res = create_param_template<_CTransfo>(m_ana.main_context(), opts, m_log, tres.transforms, samples, "s", "s0");
      tres.transforms.detach_objects();
      
      if(!tpl_res) {
        do_log_error("create_template() Failed to create template");
        return tpl_res;
      }
      if(plot_pdf) {
        create_canvas(m_prefix+" P MC");
        TransformPlot2D(m_ana.main_context(), tpl_res.transform).npointsx(40).npointsy(40).title("P").color(1).plot("SURF");
      }
      if(check_overtraining) {
        auto overt = overtraining_test(tpl_res.transform.template slice_evaluator<0>(true, 1.0, true));
        auto res = overt.compute(tpl_res.samples, tpl_res.points, EvenEventSel(), OddEventSel());
        do_log_info("create_template() overtraining KS="<<res.first<<" prob="<<res.second);

        if(overt.histogram_training() && overt.histogram_test()) {
          create_canvas(m_prefix+" ln(p)");
          draw_histogram(m_prefix+"_lnp_train", *overt.histogram_training(), "", 1);
          draw_histogram(m_prefix+"_lnp_test", *overt.histogram_test(), "same", 2);

          create_canvas(m_prefix+" int. ln(p)");
          draw_histogram(m_prefix+"_int_lnp_train", overt.histogram_training()->integrate(), "", 1);
          draw_histogram(m_prefix+"_int_lnp_test", overt.histogram_test()->integrate(),"same", 2);
        }
        else {
          do_log_error("create_template() Overtraining test seems to have failed");
        }
      }
      if(_plot_slices) {
        plot_slices(m_ana.main_context(), "S", tpl_res);

        create_canvas("S raw slices");
        for(unsigned i=0; i<tpl_res.points.rows(); ++i) {
          auto hs = tpl_res.samples[i].histogram_1D(0,100,true,AllEventSel());
          draw_histogram("S 2 "+std::to_string(i), hs, i? "SAME" : "", 1);
          TransformPlot1D(m_ana.main_context(), tres.transforms[i]).scale(tpl_res.samples[i].events()).external_coordinates(true).superimpose(true).plot();
        }
      }
      return tpl_res;
    }

    t_Transform<_CTransfo> create_template(SampleSet<1U>& samples, OptionMap& opts, bool plot_pdf = false, bool _plot_slices = false, bool check_overtraining = true) {
      auto res = create_template_res(samples, opts, plot_pdf, _plot_slices, check_overtraining);
      return res.transform;
    }
    
    inline _PSTransfo& pstransfo() { return m_pstransfo; }
  protected:
    std::string m_prefix;
    Log m_log;
    Analysis& m_ana;
    _PSTransfo m_pstransfo;
  };
}

using namespace sfi;
using namespace sfi::bases;

template<unsigned ndims, unsigned ndeg = 20>
struct param_est_defs {
  using Eig1 = EigenVectors<ndims, ndeg, EigenMonomicSparse>;
  using BaseTransfo = ProjectionTransformation<Eig1, Cosine>;
  using XTransf = t_Transform<BaseTransfo>;
  using Eig2 = EigenVectors<ndims, 5, EigenFullTensor>;
  using ParamTransf = Transform<Eig2, Chebyshev>;
  using CTransfo = CombinationTransformation<XTransf, ParamTransf>;
  using Transf = t_Transform<CTransfo>;
  // Ordinary (non amplitude) transformations
  using PSTransfo = BaseTransfo;
  using PSTransf = t_Transform<PSTransfo>;

  using AmpPSTransfo = AmplitudeTransformation<BaseTransfo>;
  using AmpPSTransf = t_Transform<AmpPSTransfo>;
};

#endif // sfi_ParamEst_h
