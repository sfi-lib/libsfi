#ifndef sfi_PDF1DAnalysis_h
#define sfi_PDF1DAnalysis_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <map>

#include "ProjectionTransformation.h"
#include "AmplitudeTransformation.h"
#include "cosine.h"
#include "Analysis.h"
#include "ParamSampleBuilder.h"
#include "timer.h"
#include "transform_plots.h"
#include "full_system.h"
#include "sparse_system.h"

#include <TApplication.h>
#include <TH1D.h>
#include <TRandom3.h>
#include <TF1.h>
#include <TGraph.h>

namespace sfi {

  class PDFAnalysis : public Analysis {
  public:
    PDFAnalysis(OptionMap& opts);

    void maxevt(unsigned evt, const std::string& ch_name = "s");

    void mean(double m, const std::string& ch_name = "s");

    void set_gaus(const std::string& ch_name = "s");

    void set_gaus_uniform(double frac, const std::string& ch_name = "s");

    void set_expo(const std::string& ch_name = "s");

    void set_gaus_expo(double frac, const std::string& ch_name = "s");

    void set_breit_wigner(const std::string& ch_name = "s");

    void set_breit_wigner_uniform(double frac, const std::string& ch_name = "s");

    void set_step(double frac, const std::string& ch_name = "s");

    void set_sine(const std::string& ch_name = "s");

    template<class _Transf>
    inline SampleSet<_Transf::dims()> samples(bool normalize = true, const std::string& ch_name = "s") {
      return channel_samples<_Transf::dims()>(ch_name, false, normalize);
    }

    inline TRandom3& rnd() { return m_rnd; }

  protected:
    Log m_log;
    TRandom3 m_rnd;
    struct ch_data {
      ParamSampleBuilder* m_builder;
      ParamDataSource* m_source;
    };
    std::map<std::string, ch_data> m_channels;
  };
}

#endif // sfi_PDF1DAnalysis_h
