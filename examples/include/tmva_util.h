#ifndef sfi_tmva_util_h
#define sfi_tmva_util_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <TMVA/Factory.h>
#include <TMVA/Tools.h>
#include <TMVA/DataLoader.h>
#include <TMVA/MethodBase.h>

#include "BinaryClassifierPlots.h"
#include "LatexTable.h"

namespace sfi {
  void plot_tmva_results(BinaryClassifierPlotsBase& bcplots, TFile* inFile, bool train);
  void copy_tmva_efficiencies(LatexTable& eff_table, TMVA::MethodBase* m, unsigned row);
  void copy_tmva_summary(LatexTable& table, unsigned row, TMVA::MethodBase* m, TMVA::Factory* factory, const TString& mname);
}

#endif // sfi_tmva_util_h
