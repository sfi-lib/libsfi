#ifndef TMinuitMaximizer_h
#define TMinuitMaximizer_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Analysis.h"
#include "ILikelihoodMaximizer.h"
#include "ILikelihood.h"
#include "LikelihoodMaximizerFactory.h"

class TMinuit;

namespace sfi {
  class TMinuitMaximizer : public ILikelihoodMaximizer {
  public:
    TMinuitMaximizer(Analysis& ana, const OptionMap& opts, ILikelihood* lh, const OptionMap& lhopts, const OptionMap* meta = nullptr);

    TMinuitMaximizer(Parameters&& par, Context& ctx, const OptionMap& opts, ILikelihood* lh, const OptionMap& lhopts);

    // ---- implementation of ILikelihoodMaximizer ----

    virtual ~TMinuitMaximizer();

    virtual void do_fix_parameter(unsigned par, double val);

    virtual Parameters& get_parameters() { return m_parameters; }
    virtual const Parameters& get_parameters() const { return m_parameters; }

    virtual Parameter& get_parameter(const std::string& name) { return m_parameters[name]; }

    /** Maxmimize the likelihood */
    virtual bool do_maximize();

    virtual bool get_profile(unsigned par, unsigned npts, Vector<double>& xv, Vector<double>& fv, double sigmas_range = 3.0);

    virtual bool get_contour(unsigned par1, unsigned par2, unsigned npts, Vector<double>& v1, Vector<double>& v2);

    /** Evaluate the likelihood at point given by the working values of Parameters */
    virtual double do_eval() { return m_lhood->do_eval(this->m_context); }

    virtual void get_pdfs(std::vector<IPDF*>& pdfs) { m_lhood->get_pdfs(pdfs); }

    /** @return The minimum value of the likelihood */
    virtual double get_minval() const;

    virtual bool get_has_asymetrical_uncertainties() const { return true; }

    virtual const ILikelihood* get_likelihood() const { return m_lhood; }

    virtual ILikelihoodMaximizer& set_strategy(int i);

    virtual void set_print_level(PrintLevel);

    /** Set valuye of given parameter */
    virtual bool set_parameter_value(unsigned p, double v);

    virtual bool get_covariance(MatrixSym<double>&);

    virtual bool do_run_migrad(unsigned maxcalls, double tole);
    virtual bool do_run_minos();
    virtual bool do_run_hesse();

    virtual void set_enable_minos(bool b) { m_do_minos = b; }

    virtual void set_enable_hesse(bool b) { m_do_hesse = b; }
  protected:
    void init(const OptionMap& opts, const OptionMap& lhopts);

    void init_params(bool randomize);
    
    bool copy_params(bool minos_unc);

    static TMinuitMaximizer* s_instance;
    static void fcn(int &, double *, double &f, double *par, int);

    Log m_log;
    Context& m_context;
    Parameters m_parameters;
    TMinuit* m_minuit;
    ILikelihood* m_lhood;
    unsigned m_max_calls_per_fit;
    unsigned m_max_repeats;
    double m_tolerance;
    bool m_debug;
    bool m_do_minos;
    bool m_do_hesse;
  };

  class TMinuitFactory : public ILikelihoodMaximizerFactory {
    public:
    TMinuitFactory(const OptionMap& opts):m_options(opts) {}

    virtual ~TMinuitFactory() {};

    virtual const std::string& get_type() const {
      static std::string tname = type_name<TMinuitFactory>();
      return tname;
    }

    virtual const std::string& get_name() const {
      static std::string iname;
      return iname;
    }

    virtual const OptionMap& get_options() const { return m_options; }

    virtual OptionMap& get_options() { return m_options; }

    virtual ILikelihoodMaximizer* do_create(Parameters&& pars, Context& ctx, const OptionMap& opts, ILikelihood* lh, const OptionMap& lhopts) {
      return new TMinuitMaximizer(std::move(pars),ctx,opts,lh,lhopts);
    }

    protected:
    OptionMap m_options;
  };
}

#endif // TMinuitMaximizer_h
