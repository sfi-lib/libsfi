#ifndef sfi_ParamSampleBuilder_h
#define sfi_ParamSampleBuilder_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "SampleBuilderBase.h"

namespace sfi {
  class ParamSampleBuilder : public SampleBuilderBase {
  public:
    using This = ParamSampleBuilder;

    ParamSampleBuilder(Context& ctx, OptionMap&);

    virtual ~ParamSampleBuilder();

    // ---- impl of ISampleBuilder ----

    virtual ISample* do_build(Context& ctx, const PVariables& vars, const DataSource& evt);

    virtual std::shared_ptr<DataSource> get_data_source(const OptionMap& opts) const;
  protected:

  };

  class ParamDataSource : public DefaultDataSource {
  public:
    using This = ParamDataSource;

    enum Mode { Gaus, GausExpo, GausUniform, BreitWigner, BreitWignerExpo, BreitWignerUniform, Expo, UniformUniform, Sine, Gamma, Uniform};

    ParamDataSource(const std::string& name, unsigned dims, const std::string& mode = "gaus", unsigned nevents = 0, unsigned index = 0);

    ParamDataSource(const std::string& name, const std::string& mode, double m, double s, unsigned nevents = 0, unsigned index = 0);

    ParamDataSource(const OptionMap& opts, unsigned dims);

    virtual ~ParamDataSource() {}

    // ---- implementation of DataSource ----

    virtual std::shared_ptr<DataSource> do_clone() const;

    // ---- exported interface ----

    inline This& mean(double m, unsigned d = 0) { m_means[d] = m; return *this; }

    inline This& spread(double s, unsigned d = 0) { m_spreads[d] = s; return *this; }

    inline const std::string& mode() const { return m_mode; }

    inline This& mode(const std::string& m) { m_mode = m; return *this; }

    inline This& uniform_fraction(double f) { m_uniform_fraction = f; return *this; }
  protected:
    std::string m_mode;
    double m_xscale;
    double m_uniform_fraction;
    std::vector<double> m_means, m_spreads;
  };
}

#endif // sfi_ParamSampleBuilder_h
