#ifndef sfi_ylm_util_h
#define sfi_ylm_util_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <sample_functions.h>
#include <Ylm.h>

namespace sfi {
  template<class _Transf, class _Rnd>
  inline void sample_random_2d_ylm(Log& _log, sfi::Sample<2>* res, const _Transf& trans, unsigned nevents, _Rnd& rnd, double upper = 1.8, bool debug = false) {
    sample_random_2d(res, trans, nevents, rnd, upper, debug);
    res->is_normalized(false);
    res->normalize(_log);
  }

  inline PVariables create_ylm_2d_variables() {
    auto vars = std::make_shared<Variables>(2);
    auto& v0 = vars->at(0);
    v0.name() = "#theta";
    v0.trans_type(Variable::Cos);
    v0.external_interval(0, M_PI);
    v0.lock();
    auto& v1 = vars->at(1);
    v1.name() = "#phi";
    v1.external_interval(0, 2.0*M_PI);
    v1.lock();
    return vars;
  }
}

#endif // sfi_ylm_util_h
