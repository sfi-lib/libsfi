#ifndef sfi_example_util_h
#define sfi_example_util_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <sample_functions.h>
#include "AmplitudeTransformation.h"
#include "Analysis.h"
#include "cosine.h"
#include "DataTransformPlot1D.h"
#include "ext_utils.h"
#include "full_system.h"
#include "ProjectionTransformation.h"
#include "Sample.h"
#include "stat_functions.h"
#include "transform_functions.h"
#include "sparse_system.h"
#include "FracEventSel.h"
#include "transform_plots.h"

#include <TApplication.h>
#include <TH1D.h>
#include <TFile.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TRandom3.h>

#include "BinaryClassifierPlots.h"
#include "cosine.h"
#include "gui.h"

namespace sfi {
  template<class _Transf, class _EventSel>
  void transform_stats(const std::string& pfx, const _Transf& trf, const Sample<_Transf::dims()>& spl, const _EventSel& esel, bool details = false) {
    Vector<double> H = compute_hessian_diagonal(spl, trf, esel);
    double trH_over_p = H.esum()/double(H.rows());
    Vector<double> g = compute_grad(spl, trf, esel);
    double grad_over_p = g.esum()/double(g.rows());
    std::cout<<pfx<<" sample:"<<spl.name()<<" stat_invp_sum="<<stat_invp_sum(spl, trf, esel)<<" trH/p="<<trH_over_p<<" grad/p="<<grad_over_p<<std::endl;
    if(details) {
      std::cout<<pfx<<" H: "<<H<<std::endl;
      std::cout<<pfx<<" grad: "<<g<<std::endl;
    }
  }

  // TODO: use DataTransformPlot
  template<class _Trans, unsigned _Dims, class _T2>
  Canvas compare_data_and_transform(Context& ctx, const std::string& prefix, const std::string& suffix, bool external, const _Trans& trf, const Sample<_Dims, _T2>& s, unsigned npdfbins, unsigned var = 0) {
    std::string title(prefix+(external ? " pdf ext. " : " pdf int. ")+suffix);
    return DataTransformPlot1D(ctx, trf).title(title).npoints(npdfbins).variable(var).sample(s, AllEventSel()).external_coordinates(external).plot();
  }

  template<class _Transfo>
  TransformSet<t_Transform<_Transfo>>
  transform_samples_and_compare_ext_with_int(Context& ctx, const std::string& prefix, const std::string& suffix, const _Transfo& trfo, const Sample<1U>& s, unsigned npdfbins, bool dump_trf = false) {
    auto* tr = new t_Transform<_Transfo>;
    auto res = perform_transformation(ctx, s, trfo, AllEventSel(), *tr);
    plot_amp_transform_details(prefix+" "+suffix, res, "");
    if(dump_trf) {
      tr->dump();
      std::cout<<"name: "<<tr->name()<<" I: "<<tr->integral_pdf()<<std::endl;
    }
    compare_data_and_transform(ctx, prefix, suffix, false, *tr, s, npdfbins);
    compare_data_and_transform(ctx, prefix, suffix, true, *tr, s, npdfbins);
    return TransformSet<t_Transform<_Transfo>>({tr});
  }

  template<class _Transfo>
  TransformSet<t_Transform<_Transfo>>
  transform_samples_and_compare_ext_with_int(Context& ctx, const std::string& prefix, const std::string& suffix, const _Transfo& trfo, const Sample<1U>& s0, const Sample<1U>& s1, unsigned npdfbins) {
    auto trfs = transform_samples_and_compare_ext_with_int(ctx, prefix, "lin", trfo, s0, npdfbins);
    trfs += transform_samples_and_compare_ext_with_int(ctx, prefix, suffix, trfo, s1, npdfbins);
    return trfs;
  }

  Sample<1>* create_a_sample_with_gaussian_numbers(Analysis& ana, bool add_floor = false, double mean = 0.1, double width = 0.05, bool normalize = true);

  void plot_sample(const Sample<1>& spl, const std::string& title, bool ext);

  void plot_samples(const SampleSet<1>& spls, const std::string& title, bool ext);

  // TODO: Remove and remove param_template_result
  template<class _CTransf, class _Trf>
  void plot_slices(Context& ctx, const std::string& title, param_template_result<_CTransf, _Trf>& tpl_res) {
    auto seval = tpl_res.transform.template slice_evaluator<0>(true, 1.0, true);
    create_canvas(title+" slices");
    for(unsigned i=0; i<tpl_res.points.rows(); ++i) {
      seval->slice(tpl_res.points.row_data(i));
      auto hs = tpl_res.samples[i].histogram_1D(0,100,true,AllEventSel());
      draw_histogram(title+" "+std::to_string(i), hs, i? "SAME" : "", 1);
      TransformPlot1D(ctx, seval.get()).scale(tpl_res.samples[i].events()).external_coordinates(true).superimpose(true).plot();
    }
  }

  template<class _Transf>
  void plot_samples_and_transforms(Context& ctx, const std::string& title, const SampleSet<1>& s, const TransformSet<_Transf>& trf) {
    plot_samples(s, title, false);
    for(unsigned i=0; i<trf.size(); ++i) {
      TransformPlot1D(ctx, trf[i]).scale(s[i].events()).npoints(100).external_coordinates(false).superimpose(true).plot();
    }
    plot_samples(s, title, true);
    for(unsigned i=0; i<trf.size(); ++i) {
      TransformPlot1D(ctx, trf[i]).scale(s[i].events()).npoints(100).external_coordinates(true).superimpose(true).plot();
    }
  }

  template<class _Transf>
  void plot_nominal(Context& ctx, const std::string& title, const SampleSet<1>& spls, const TransformSet<_Transf>& trfs, bool external_coordinates) {
    int col = 1;
    Canvas& can = Canvas::get(title+" nominal "+(external_coordinates ? "ext" : "int"));
    for(auto& s : spls) {
      std::cout<<"plot_nominal() "<<s.name()<<" var: '"<<s.data_source()->variant()<<"'"<<std::endl;
      if(s.data_source()->variant().empty()) {
        auto* trf = transform_by_sample(trfs, s);
        if(trf) {
          can.draw(s.name(), s.histogram_1D(0, 100, external_coordinates, AllEventSel())).color(col);
          can.draw(new TransformPlot1D(ctx, *trf)).scale(s.events()).external_coordinates(external_coordinates).color(col);
          ++col;
        }
      }
    }
    can.plot();
  }

  template<class _Transf>
  void plot_nominal(Context& ctx, const std::string& title, const SampleSet<1>& spls, const TransformSet<_Transf>& trfs) {
    plot_nominal(ctx, title, spls, trfs, false);
    plot_nominal(ctx, title, spls, trfs, true);
  }

  void plot_multi_graph(const std::string& title, const Vector<double>& xvals, const std::vector<Vector<double>>& yvals, double ymin, double ymax, const std::string& xtitle = "", const std::string& ytitle = "");

  void plot_coefficients_1D(const std::string& prefix, IEvaluator& ev, unsigned ncoeff, unsigned npts, const Matrix<double>& points, const TransformSet<ITransform>& trfs);

  template<class _Tpl>
  void plot_coefficients_1D(const std::string& prefix, const _Tpl& tpl, unsigned ncoeff, unsigned npts) {
    auto slev = tpl.transform.template slice_evaluator<0>(true,1.0,false);
    plot_coefficients_1D(prefix, *slev, ncoeff, npts, tpl.points, tpl.point_transforms.generic());
  }

  template<class _Trf, class _Sample, class _TrainEvSel, class _TestEvSel>
  void plot_2lnp(const std::string& prefix, _Trf& trf, _Sample& spl, const _TrainEvSel& train_evsel, const _TestEvSel& test_evsel, unsigned nbins = 100) {
    auto ev = trf.evaluator();
    double minlnp(std::numeric_limits<double>::max()), maxlnp(std::numeric_limits<double>::min());
    auto limfunc = [&](const double* p, double) {
      double lnp = -2.0*ln((*ev)(p));
      minlnp = std::min(minlnp, lnp);
      maxlnp = std::max(maxlnp, lnp);
    };
    spl.apply(AllEventSel(),limfunc);
    
    Histogram<> lnp_train(nbins, minlnp, maxlnp);
    auto sfunc_train = [&](const double* p, double) {
      double lnp = -2.0*ln((*ev)(p));
      lnp_train.fill(lnp);
    };
    spl.apply(train_evsel,sfunc_train);
    
    Histogram<> lnp_test(nbins, minlnp, maxlnp);
    auto sfunc_test = [&](const double* p, double) {
      double lnp = -2.0*ln((*ev)(p));
      lnp_test.fill(lnp);
    };
    spl.apply(test_evsel,sfunc_test);
    
    auto can_lnp = Canvas::get(prefix+"-2lnp");
    can_lnp.draw("train", lnp_train).color(1);
    can_lnp.draw("test", lnp_test).color(2);
    can_lnp.axis_title(0,"-#int 2ln(p)");
    can_lnp.plot();
    
    auto can_int_lnp = Canvas::get(prefix+"-int 2lnp");
    can_int_lnp.draw("train", lnp_train.integrate()).color(1);
    can_int_lnp.draw("test", lnp_test.integrate()).color(2);
    can_int_lnp.axis_title(0,"-#int 2ln(p)");
    can_int_lnp.plot();
  }

  using FullTensor = EigenFullTensor;
  using Sparse = EigenMonomicSparse;
}

using namespace sfi;
using namespace sfi::bases;

#define DefineGeneralExample(FUNC,CONfIGDIR) int main(int argc, char** argv) { \
    std::cout<<std::setprecision(6); \
    init_style(); \
    Options opts; \
    bool help = false; \
    opts.bind("help", help); \
    opts.parse_options(argc, argv); \
    if(opts.parse_opts_file(#CONfIGDIR"/config/"#FUNC".opts")) { \
      if(help) { opts.print_help(std::cout); } \
      else { \
        opts.print(std::cout);  \
        TApplication app(argv[0], &argc, argv); \
        FUNC(opts); \
        app.Run(); \
      } } }


#define DefineExample(FUNC) DefineGeneralExample(FUNC,examples);

#endif // sfi_example_util_h
