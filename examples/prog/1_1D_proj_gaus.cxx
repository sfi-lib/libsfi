#include "example_util.h"

void
example1() {
  using Transf = t_Transform<tdimensions<1>, tdegree<10>, FullTensor, Cosine>;
  Analysis ana("1D");
  ProjectionTransformation<Transf> proj(ana.main_context(), "p");
  auto* sample = create_a_sample_with_gaussian_numbers(ana, false, 0.1, 0.2);
  Transf* trf = perform_transformation(ana.main_context(), *sample, proj);

  DataTransformPlot1D plot(ana.main_context(), *trf);
  plot.npoints(100).sample(*sample).title("Gaus").file_name("explots/example_1.pdf").plot();
  delete trf;
  sample->release();
}

int
main(int argc, char** argv) {
  init_style();
  TApplication app(argv[0], &argc, argv);
  example1();
  app.Run();
}
