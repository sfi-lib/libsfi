#include "example_util.h"

void
example2_a() {
  using Transf = t_Transform<tdimensions<1>, tdegree<10>, FullTensor, Cosine>;
  Analysis ana("1D");
  ProjectionTransformation<Transf> proj(ana.main_context(), "p");
  auto* sample = create_a_sample_with_gaussian_numbers(ana, false);
  Transf* trf = perform_transformation(ana.main_context(), *sample, proj);
  DataTransformPlot1D(ana.main_context(), *trf).npoints(100).sample(*sample).title("Gaus P").file_name("explots/example_2_1D_p_gaus.pdf").plot();

  delete trf;
  sample->release();
}

void
example2_b() {
  using Transf = t_Transform<tdimensions<1>, tdegree<20>, FullTensor, Cosine>;
  Analysis ana("1D");
  ProjectionTransformation<Transf> proj(ana.main_context(), "p");
  auto* sample = create_a_sample_with_gaussian_numbers(ana, true);
  Transf* trf = perform_transformation(ana.main_context(), *sample, proj);
  DataTransformPlot1D(ana.main_context(), *trf).npoints(100).sample(*sample).title("Gaus Uni P").file_name("explots/example_2_1D_p_gaus_uni.pdf").plot();

  AmplitudeTransformation<ProjectionTransformation<Transf>, ttrue> amp(ana.main_context(), "a");
  Transf* atrf = perform_transformation(ana.main_context(), *sample, amp);
  DataTransformPlot1D(ana.main_context(), *atrf).npoints(100).sample(*sample).title("Gaus Uni A").file_name("explots/example_2_1D_a_gaus_uni.pdf").plot();

  delete trf;
  delete atrf;
  sample->release();
}

int
main(int argc, char** argv) {
  init_style();
  TApplication app(argv[0], &argc, argv);
  example2_a();
  example2_b();
  app.Run();
}
