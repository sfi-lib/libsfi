#include "example_util.h"
#include "ylm_util.h"
#include "PDF1DComparison.h"
#include "PDF2DComparison.h"

template<class _Rnd, class _Transf>
void
cmp_transforms_2d(const std::string&, _Rnd& rnd, const _Transf& trf_1, const _Transf& trf_2) {
  auto& v0 = trf_1.variables()->at(0);
  auto& v1 = trf_1.variables()->at(1);

  auto ev_1 = trf_1.evaluator(1.0, true);
  auto ev_2 = trf_2.evaluator(1.0, true);

  std::uniform_real_distribution<> uni_x(v0.minval(), v0.maxval());
  std::uniform_real_distribution<> uni_y(v1.minval(), v1.maxval());

  SampleExtStat diffstat;
  double x[2];
  for(unsigned i=0; i<10000; ++i) {
    x[0] = uni_x(rnd);
    x[1] = uni_y(rnd);
    auto va1 = (*ev_1)(x);
    auto va2 = (*ev_2)(x);
    double diff = std::abs(va1 - va2);
    diffstat += diff;
  }
  std::cout<<"cmp_transforms_2d() "<<diffstat<<std::endl;
}

void
ylm_pdf(Options& opts) {
  Analysis ana("Ylm");
  Log log(Log::get(ana.main_context(), "ypl_pdf"));

  unsigned nevents(0);
  opts.get_if("nevents", nevents);

  const unsigned L = 6;
  using Eigen = EigenVectors<2, L, EigenYlm>;
  using Transf = Transform<Eigen, Ylm>;
  using PTrfo = ProjectionTransformation<Transf>;

  Transf trf("ylm", true);
  log_info(log, "npar: "<<trf.npar()<<" nparams: "<<trf.nparams()<<" EV: "<<Eigen::params());

  auto vars = create_ylm_2d_variables();
  log_info(log, "Variables: "<<*vars);
  trf.variables(vars);
  trf.is_amplitude(false);

  trf[Ylm::mempos(0,0)] = std::complex<double>(100.0, 100.0);
  trf[Ylm::mempos(6,5)] = std::complex<double>(20.0, 0.0);

  TransformPlot2D(ana.main_context(), trf, opts.sub("plots.ylm_true")).plot();
  TransformPlot2D(ana.main_context(), trf, opts.sub("plots.ylm_lin_true")).plot();

  auto* spl = ana.main_context().memory_pool().pool<Sample<2>>()->get(nevents, "");
  spl->variables(vars);
  sample_random_2d_ylm(log, spl, trf, nevents, ana.main_context().rnd_gen(), 1.5);

  // compare data and true transform
  PDF2DComparison(ana.main_context(), opts.sub("plots.ylm_cmp")).make_histogram(*spl, AllEventSel()).transform(trf).plot();
  PDF2DComparison(ana.main_context(), opts.sub("plots.ylm_cmp_ext")).make_histogram(*spl, AllEventSel()).transform(trf).plot();

  if(opts.get<bool>("do_lin")) {
    PTrfo trafo(ana.main_context(), "ps");
    Transf trf_proj("ylm_proj", true);
    trafo.transform(ana.main_context(), create_transform_call(*spl, AllEventSel()), trf_proj);
    log_info(log, "Linear projection npar: "<<trf_proj.npar()<<" I="<<trf_proj.integral_pdf());

    TransformPlot2D(ana.main_context(), trf_proj, opts.sub("plots.ylm_p_proj")).plot();
    TransformPlot2D(ana.main_context(), trf_proj, opts.sub("plots.ylm_p_proj_lin")).plot();

    cmp_transforms_2d("Ylm P", ana.main_context().rnd_gen(), trf, trf_proj);

    if(opts.get<bool>("plot_1D_details")) {
      auto trf1d = transform_set_2D(perform_single_transformations(ana.main_context(), *spl, trafo, AllEventSel()));
      {
        PDF1DComparison pdfcmp(ana.main_context(), "Ylm P");
        pdfcmp.n_pdf_bins(100).sample(*spl, AllEventSel()).transforms_1D(trf1d.generic()).transforms(TransformSet<ITransform>({&trf_proj}, false));
        pdfcmp.plot();
      }
      {
        PDF1DComparison pdfcmp(ana.main_context(), "Ylm true P");
        pdfcmp.n_pdf_bins(100).sample(*spl, AllEventSel()).transforms_1D(trf1d.generic()).transforms(TransformSet<ITransform>({&trf}, false));
        pdfcmp.plot();
      }
    }
  }
  if(opts.get<bool>("do_amp")) {
    // ---- amplitude ----
    using ATrfo = AmplitudeTransformation<PTrfo, ttrue>;

    Transf trf_proj("ylm_proj", true);

    ATrfo atrfo(ana.main_context(), opts.sub("transformations.amp"));
    auto ares = atrfo.transform(ana.main_context(), create_transform_call(*spl, AllEventSel()), trf_proj);
    log_info(log, " Amplitude transform npar: "<<trf_proj.npar()<<" I="<<trf_proj.integral_pdf()<<" is amp: "<<trf_proj.is_amplitude());
    if(opts.get<bool>("plot_amp_details")) plot_amp_transform_details("Ylm A", ares, "plots/");

    TransformPlot2D(ana.main_context(), trf_proj, opts.sub("plots.ylm_a_proj")).plot();
    TransformPlot2D(ana.main_context(), trf_proj, opts.sub("plots.ylm_a_proj_lin")).plot();

    if(opts.get<bool>("plot_1D_details")) {
      auto atrf1d = transform_set_2D(perform_single_transformations(ana.main_context(), *spl, atrfo, AllEventSel()));
      {
        PDF1DComparison apdfcmp(ana.main_context(), "Ylm A");
        apdfcmp.n_pdf_bins(100).sample(*spl, AllEventSel()).transforms_1D(atrf1d.generic()).transforms(TransformSet<ITransform>({&trf_proj}, false));
        apdfcmp.plot();
      }
      {
        PDF1DComparison apdfcmp(ana.main_context(), "Ylm A true");
        apdfcmp.n_pdf_bins(100).sample(*spl, AllEventSel()).transforms_1D(atrf1d.generic()).transforms(TransformSet<ITransform>({&trf}, false));
        apdfcmp.plot();
      }
    }
    cmp_transforms_2d("Ylm A", ana.main_context().rnd_gen(), trf, trf_proj);
  }
  spl->release();
}

DefineExample(ylm_pdf)
