#include "example_util.h"

void
example4() {
  using Transf = t_Transform<tdimensions<1>, tdegree<10>, FullTensor, Cosine>;

  Analysis ana("1D");
  auto* sample = create_a_sample_with_gaussian_numbers(ana, true, 0.0, 0.05, false);
  sample->variables()->at(0).external_interval(-1,1).intermediary_interval(-7, 7).trans_type(Variable::Atan);
  sample->normalize(ana.log());

  AmplitudeTransformation<ProjectionTransformation<Transf>, ttrue> amp(ana.main_context(), "a");
  amp.max_iterations(20).speed(0.5);
  Transf* atrf = new Transf("a");
  auto tres = perform_transformation(ana.main_context(), *sample, amp, AllEventSel(), *atrf);

  DataTransformPlot1D(ana.main_context(), *atrf).external_coordinates(false).npoints(100).sample(*sample).title("Amp Gaus Uni Int").file_name("explots/example_4_int.pdf").plot();
  DataTransformPlot1D(ana.main_context(), *atrf).external_coordinates(true).npoints(100).sample(*sample).title("Amp Gaus Uni Ext").file_name("explots/example_4_ext.pdf").plot();

  delete atrf;
  sample->release();
}

int
main(int argc, char** argv) {
  init_style();
  TApplication app(argv[0], &argc, argv);
  example4();
  app.Run();
}
