#include "example_util.h"
#include "sparse_system.h"
#include "cross_entropy.h"
#include "BinaryClassifier.h"
#include "BootstrapTransformation.h"

using Transf = t_Transform<tdimensions<2>,Sparse,Cosine>;
using PSTrafo = AmplitudeTransformation<ProjectionTransformation<Transf>, ttrue>;
using BSTrafo = BootstrapTransformation<PSTrafo>;

void
example_8(OptionMap& op) {
  Analysis ana(op);

  // Get signal and background samples
  auto samples = ana.channel_samples<2U, false>("spiral", true, false);

  // Get sample by name
  auto* s(samples["spiral_S"]);
  
  PSTrafo pstr(ana.main_context(), op.sub("transformations.amp"));
  
  {
    auto tres0 = perform_transformations(ana.main_context(), ana.log(), samples, pstr, EvenEventSel());
    Transf& str0(tres0.transforms[0]);
    plot_2lnp("S", str0, *s, EvenEventSel(), OddEventSel(), 400);
  }
  if(op.get_as("do_ce", false)) {
    // Allow these values to be configured
    unsigned min_degrees = op.get_as<unsigned>("min_degrees");
    unsigned max_degrees = op.get_as<unsigned>("max_degrees");
    unsigned ce_parts = op.get_as<unsigned>("ce_parts");
    unsigned Ndeg = max_degrees - min_degrees;
    
    // Make the plot of cross entropy
    plot_cross_entropy(ana,ana.log(),*s,"S",Ndeg,ce_parts,min_degrees,pstr);
  }
  {
    PSTrafo pstr_opt(ana.main_context(), op.sub("transformations.amp_opt"));
    auto tres = perform_transformations(ana.main_context(), ana.log(), samples, pstr_opt, EvenEventSel());
    Transf& str(tres.transforms[0]);
    TransformPlot2D(ana.main_context(), str, op.sub("plots.opt")).plot();
    plot_2lnp("S opt", str, *s, EvenEventSel(), OddEventSel(), 400);
  }
  
  {
    PSTrafo pstr_prune(ana.main_context(), op.sub("transformations.amp_prune"));
    auto tres1 = perform_transformations(ana.main_context(), ana.log(), samples, pstr_prune, EvenEventSel());
    Transf& str1(tres1.transforms[0]);
    TransformPlot2D(ana.main_context(), str1, op.sub("plots.prune")).plot();
    
    plot_2lnp("S prune", str1, *s, EvenEventSel(), OddEventSel(), 400);
    plot_amp_transform_details("prune", tres1.results[0], "explots/example_8_amp_prune");
  }
  
  {
    PSTrafo pstr_os(ana.main_context(), op.sub("transformations.amp_os"));
    auto tres2 = perform_transformations(ana.main_context(), ana.log(), samples, pstr_os, EvenEventSel());
    Transf& str(tres2.transforms[0]);
    TransformPlot2D(ana.main_context(), str, op.sub("plots.os")).plot();
    
    plot_2lnp("S OS", str, *s, EvenEventSel(), OddEventSel(), 400);
    plot_amp_transform_details("OS", tres2.results[0], "explots/example_8_amp_OS");
  }
  
  {
    BSTrafo pstr_bs(ana.main_context(), op.sub("transformations.bs"));
    MultiTransform<Transf> trf;
    pstr_bs.transform(ana.main_context(), create_transform_call(*s, EvenEventSel()), trf);
    
    TransformPlot2D(ana.main_context(), trf, op.sub("plots.bs")).plot();
    
    plot_2lnp("S BS", trf, *s, EvenEventSel(), OddEventSel(), 400);
  }
}

DefineExample(example_8)
