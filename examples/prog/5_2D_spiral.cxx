#include "example_util.h"
#include "BinaryClassifier.h"
#include "sparse_system.h"
#include "PDF2DComparison.h"

using Transf = t_Transform<tdimensions<2>,Sparse,Cosine>;
using PSTrafo = AmplitudeTransformation<ProjectionTransformation<Transf>>;

void
example_5(OptionMap& op) {
  Analysis ana(op);

  auto samples = ana.channel_samples<2U, false>("spiral", true, false);

  PSTrafo pstr(ana.main_context(), op.sub("transformations.amp"));
  auto tres = perform_transformations(ana.main_context(), ana.log(), samples, pstr, EvenEventSel());
  Transf& str(tres.transforms[0]), &btr(tres.transforms[1]);

  auto classifier = make_binary_classifier(ana, str, btr, samples, EvenEventSel(), OddEventSel());
  classifier.n_roc_bins(1000).training_time(tres.time);
  classifier.make_1D_transforms(PSTrafo(ana.main_context(), op.sub("transformations.amp")).speed(1.0).max_diff(0.1), samples);
  classifier.compute();

  auto bin_cls_plots = make_binary_classifier_plots(classifier, op.sub("plots.bc"));
  bin_cls_plots.make_plots();

  // plot PDF:s for signal and background
  PDF2DComparison(ana.main_context(), op.sub("plots.cmp2d_s")).make_histogram(samples[0], EvenEventSel()).transform(str).plot();
  PDF2DComparison(ana.main_context(), op.sub("plots.cmp2d_b")).make_histogram(samples[1], EvenEventSel()).transform(btr).plot();
  
  // Getting and plotting the marginalized distribution for a transform
  auto* at = str.axis_transform(1);
  DataTransformPlot1D(ana.main_context(), *at).title("Axis 1 signal").variable(1).scale_to_data(true).sample(samples[0],OddEventSel()).plot();
}

DefineExample(example_5)
