#include "example_util.h"
#include "PDFAnalysis.h"

using namespace sfi;

const unsigned degrees = 30;

using Eig1 = EigenVectors<1, degrees, EigenFullTensor>;
using Trafo = ProjectionTransformation<Eig1, bases::Cosine>;
using ATrafo = AmplitudeTransformation<Trafo, ttrue>;
using Transf = t_Transform<Trafo>;

using SampleTy = Sample<1,ttrue>;

SampleTy* create_sample(Analysis& ana, TRandom3& rnd, unsigned Nevents, bool unit_w = false) {
  SampleTy* res = ana.memory_pool().pool<SampleTy>()->get(Nevents,"");
  for(size_t e = 0; e<Nevents; ++e) {
    auto& data = res->data(e);
    double x(0.0);
    if(unit_w) {
      if(e % 10 == 0) {
        while(fabs(x = rnd.Gaus(0.2, 0.05)) > 1.0) {}
        data.weight(-1);
      }
      else {
        while(fabs(x = rnd.Gaus(0.2, 0.2)) >= 1.0) {}
        data.weight(1);
      }
    }
    else {
      x = rnd.Uniform(-1,1);
      if(e % 10 == 0) data.weight(-gaus(x, 1.0, 0.05));
      else data.weight(gaus(x, 1.0, 0.2));
    }
    data[0] = x;
  }
  auto vars = std::make_shared<Variables>(1);
  vars->at(0).external_interval(-1, 1);
  res->variables(vars);
  res->is_normalized(true);
  return res;
}

void
transform_sample(Analysis& ana, const SampleTy& spl, const std::string& prefix) {
  // Linear transform
  Trafo trfo(ana.main_context(), "mc");
  Transf t1;
  trfo.transform(ana.main_context(), create_transform_call(spl, AllEventSel()), t1);
  
  // Amplitude transform
  ATrafo atfo(ana.main_context(), "at", Trafo(ana.main_context(), "mc_t"));
  atfo.speed(0.5).max_diff(1e-6).max_iterations(40).print_level(sfi::Verbose);
  Transf at0;
  auto res_s = atfo.transform(ana.main_context(), create_transform_call(spl, AllEventSel()), at0);
  
  // Plot comparision
  DataTransformPlot1D(ana.main_context(), t1).sample(spl).title(prefix+" pdf").scale_to_data(false).plot();
  TransformPlot1D(ana.main_context(), at0).title("b").color(kRed).superimpose(true).plot();

  plot_amp_transform_details(prefix+"S", res_s, "plots/");
}

void
example_7_weighted_pdf() {
  unsigned Nevents = 10000;

  Analysis ana("weighted_pdf");
  TRandom3 rnd;
  auto* spl = create_sample(ana, rnd, Nevents);
  transform_sample(ana, *spl, "gaus w");

  auto* spl2 = create_sample(ana, rnd, Nevents, true);
  transform_sample(ana, *spl2, "gaus w unit w");

  spl->release();
  spl2->release();
}

int
main(int argc, char** argv) {
  init_style();
  TH1::SetDefaultSumw2(kTRUE);
  TApplication app(argv[0], &argc, argv);
  example_7_weighted_pdf();
  app.Run();
}
