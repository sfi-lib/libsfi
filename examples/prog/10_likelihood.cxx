#include "ModelTransform.h"
#include "LikelihoodPlot.h"
#include "pseudo_experiment_plots.h"
#include "Analysis.h"
#include "cosine.h"
#include "ProjectionTransformation.h"
#include "Sample.h"
#include "Transform.h"
#include "AmplitudeTransformation.h"
#include "transform_plots.h"
#include "transform_functions.h"
#include "sparse_system.h"
#include "LikelihoodMaximizerFactory.h"
#include "PseudoExperiment.h"
#include "example_util.h"
#include "ExtendedLogLikelihood.h"
#include "TemplateTransformation.h"
#include "ModelTransformation.h"
#include "model_plots.h"
#include "template_plots.h"

using namespace sfi;
using namespace sfi::bases;

using PSTransfo = AmplitudeTransformation<ProjectionTransformation<EigenVectors<1, 10, EigenFullTensor>, Cosine>>;
using ParTransf = Transform<EigenVectors<1, 4, EigenFullTensor>, Chebyshev>;
using CTransfo = CombinationTransformation<t_Transform<PSTransfo>, ParTransf>;

void
example_10(Options& opts) {
  Analysis ana(opts);
  Log log(Log::get(ana.main_context(), "example_10"));
  
  // Create transformation and init from options
  PSTransfo pstrf(ana.main_context(), opts.sub("transformations.ps_trafo"));

  // Get samples and transforms for the two channels, including nominal and variants
  // the nominal parameters are set in: analysis.channels.ch1.sources
  // parameters for variants are in: analysis.channels.ch1.variants
  auto s1 = ana.channel_samples<1,false>("ch1", false, true);
  auto s2 = ana.channel_samples<1,false>("ch2", false, true);
  auto ch1_trfs = perform_transformations(ana.main_context(), log, s1, pstrf, AllEventSel());
  auto ch2_trfs = perform_transformations(ana.main_context(), log, s2, pstrf, AllEventSel());

  if(opts.get<bool>("plot_nominal")) {
    plot_nominal(ana.main_context(), "ch1", s1, ch1_trfs.transforms);
    plot_nominal(ana.main_context(), "ch2", s2, ch2_trfs.transforms);
  }

  // Fixed transforms
  auto t_ch1_b = transform_by_name_suffix(ch1_trfs.transforms,"ch1_b");
  auto t_ch1_c = transform_by_name_suffix(ch1_trfs.transforms,"ch1_c");
  auto t_ch2_xb = transform_by_name_suffix(ch2_trfs.transforms,"ch2_xb");
  
  // Parametrized transforms
  auto s1_tpl_res = TemplateTransformation<t_Transform<CTransfo>>(ana.main_context(), opts.sub("transformations.tpl_ch1_s")).transform(ch1_trfs.transforms, s1);
  auto s2_tpl_res = TemplateTransformation<t_Transform<CTransfo>>(ana.main_context(), opts.sub("transformations.tpl_ch2_s")).transform(ch2_trfs.transforms, s2);
  auto b2_tpl_res = TemplateTransformation<t_Transform<CTransfo>>(ana.main_context(), opts.sub("transformations.tpl_ch2_b")).transform(ch2_trfs.transforms, s2);

  if(opts.get<bool>("plot_slices")) {
    plot_template_slices(ana.main_context(), "ch1_s", s1_tpl_res);
    plot_template_slices(ana.main_context(), "ch2_s", s2_tpl_res);
    plot_template_slices(ana.main_context(), "ch2_b", b2_tpl_res);
  }

  ana.add_transform(&s1_tpl_res.transform);
  ana.add_transform(t_ch1_b);
  ana.add_transform(t_ch1_c);
  ana.add_transform(&s2_tpl_res.transform);
  ana.add_transform(&b2_tpl_res.transform);
  ana.add_transform(t_ch2_xb);

  auto experiment_set_name = opts.get<std::string>("experiment_set");
  auto experiment_name = opts.get<std::string>("experiment");
  auto mini_opts = opts.sub(opts.get<std::string>("minimizer"));
  auto paramset_name = opts.get<std::string>("parameter_set");
  auto likelihood_opts = opts.sub("likelihoods."+opts.get<std::string>("likelihood"));

  // Make a dataset according to the options: analysis.experiments.ex1
  auto expr = ana.experiment_samples(experiment_set_name,experiment_name, false);

  if(opts.get<bool>("do_fit")) {
    // Create a likelihood maximizer
    // The maximizer uses options to initialize the parameters
    // Note: In practice this is of course a -minimizer- and the log-likelihoods return -2ln(L)
    auto lm = likelihood_maximizer(ana, mini_opts, extended_log_likelihood(ana, expr, paramset_name, likelihood_opts), likelihood_opts);

    if(opts.get<bool>("plot_pre_fit")) {
      // Use initial values of the parameters to make a pre-fit plot
      LikelihoodPlot(ana.main_context(), *lm).data_pdf_comparison(opts.sub("plots.ch1_pre_fit"), *expr["ch1"]);
      LikelihoodPlot(ana.main_context(), *lm).data_pdf_comparison(opts.sub("plots.ch2_pre_fit"), *expr["ch2"]);
    }

    lm->do_maximize();
    log_info(log, lm->get_parameters());

    if(opts.get<bool>("plot_post_fit")) {
      // Use ML values of the parameters to make a post-fit plot
      LikelihoodPlot(ana.main_context(), *lm).data_pdf_comparison(opts.sub("plots.ch1_post_fit"), *expr["ch1"]);
      LikelihoodPlot(ana.main_context(), *lm).data_pdf_comparison(opts.sub("plots.ch2_post_fit"), *expr["ch2"]);
    }

    if(opts.get<bool>("plot_profiles")) {
      auto& pars = lm->get_parameters();
      for(unsigned p=0; p<pars.size(); ++p) {
        LikelihoodPlot(ana.main_context(), *lm).plot_profile(p, pars[p].name(), 20, 3, true);
        log_info(log, lm->get_parameters());
      }
    }
    delete lm;
  }
}

DefineExample(example_10);
