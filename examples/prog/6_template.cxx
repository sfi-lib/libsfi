#include "example_util.h"
#include "TemplateTransformation.h"
#include "template_plots.h"

using Transf = t_Transform<tdimensions<1>,FullTensor,Cosine>;
using PSTrafo = AmplitudeTransformation<ProjectionTransformation<Transf>>;
using ParamTransf = t_Transform<tdimensions<1>, tdegree<8>, FullTensor, Chebyshev>;
using CombTransf = Transform<Transf,ParamTransf>;

void
example_6(OptionMap& op) {
  Analysis ana(op);

  auto samples = ana.channel_samples<1U, false>("s");
  
  // Initialize main transformation
  PSTrafo trfo(ana.main_context(), op.sub("transformations.tpl_ps"));
  
  // Transform all samples using all events
  auto tres = perform_transformations(ana.main_context(), ana.log(), samples, trfo, AllEventSel());
  
  // Create transformation for the template
  TemplateTransformation<CombTransf> ttrfo(ana.main_context(), op.sub("transformations.comb_s_s0"));
  
  // Create the template
  auto tpl_res = ttrfo.transform(tres.transforms, samples);
  
  // Plot 2D transform
  TransformPlot2D(ana.main_context(), tpl_res.transform, op.sub("plots.tpl")).plot();
  
  // Plot slices of the 2D transform
  plot_template_slices(ana.main_context(), "S", tpl_res);

  // Get a slice of a combined Transform
  auto slev = tpl_res.transform.slice_evaluator<0>(true,1.0,true);
  double pt[] = {0.12};
  slev->slice(pt);
  
  // Plot slice together with the nominal sample, they should have the same mean
  DataTransformPlot1D(ana.main_context(), slev.get()).sample(*samples["s_s0"]).title("Slice").scale_to_data(true).plot();
}

DefineExample(example_6)
