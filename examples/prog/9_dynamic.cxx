#include "example_util.h"
#include "DTransform.h"

using PSTrafo = AmplitudeTransformation<ProjectionTransformation<DTransform>>;

void
example_9(OptionMap& op) {
  Analysis ana(op);

  // Get signal and background samples
  auto samples = ana.channel_samples<2U, false>("spiral", true, false);

  // Get sample by name
  auto* s(samples["spiral_S"]);
  
  PSTrafo pstr(ana.main_context(), op.sub("transformations.amp"));
  
  auto deg = op.get<unsigned>("degree");
  auto basis = op.get<std::string>("basis");
  auto sparse = op.get<bool>("sparse");
  DTransform trf("A", sparse ? eig_sparse(2, deg, basis) : eig_full(2, deg, basis));
  // Alternate way of defining the full system,
  // as a composite system of two one dimensional systems:
  //DTransform trf("A", eig_comp(eig_full(1, deg, basis), eig_full(1, deg, basis)));
  pstr.transform(ana.main_context(), create_transform_call(*s, AllEventSel()), trf);
  
  TransformPlot2D(ana.main_context(), trf, op.sub("plots.opt")).plot();
}

DefineExample(example_9)
