#include "example_util.h"

void
example3(bool set_speed) {
  using Transf = t_Transform<tdimensions<1>, tdegree<5>, FullTensor, Cosine>;
  Analysis ana("1D");
  auto* sample = create_a_sample_with_gaussian_numbers(ana, true);

  AmplitudeTransformation<ProjectionTransformation<Transf>, ttrue> amp(ana.main_context(), "a");

  if(set_speed) amp.max_iterations(20).speed(0.5);
  else amp.max_iterations(100);

  Transf* atrf = new Transf("a");
  auto tres = perform_transformation(ana.main_context(), *sample, amp, AllEventSel(), *atrf);
  std::string sfx(set_speed ? " b" : " a");
  plot_amp_transform_details(sfx, tres, "explots/example_3_amp");

  DataTransformPlot1D(ana.main_context(), *atrf).npoints(100).sample(*sample).title("Gaus Uni P"+sfx).file_name("explots/example_3_gaus_uni "+sfx+".pdf").plot();

  delete atrf;
  sample->release();
}

int
main(int argc, char** argv) {
  init_style();
  TApplication app(argv[0], &argc, argv);
  example3(false);
  example3(true);
  app.Run();
}
