/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "example_util.h"

using namespace sfi;

Sample<1>* sfi::create_a_sample_with_gaussian_numbers(Analysis& ana, bool add_floor, double mean, double width, bool normalize) {
  size_t Nevents = 10000;
  auto vars = std::make_shared<Variables>(1);
  vars->at(0).name("x").external_interval(-1,1);
  auto* spl = ana.create_object<Sample<1>>(Nevents, "gaus", vars);
  auto& gen = ana.main_context().rnd_gen();
  std::normal_distribution<> gaus(mean, width);
  std::uniform_real_distribution<> uni(-1, 1);
  for(size_t i=0; i<Nevents; ++i) {
    double x;
    if(add_floor && (i%10 == 0)) x = uni(gen);
    else  while(fabs(x = gaus(gen)) > 1) {};
    spl->x(i,0) = x;
  }
  if(normalize) spl->normalize(ana.log());
  return spl;
}

void sfi::plot_sample(const Sample<1>& spl, const std::string& title, bool ext) {
  create_canvas(std::string(title)+(ext ? " ext":" int"));
  int col = 2;
  auto h = spl.histogram_1D(0,100,ext,AllEventSel());
  draw_histogram((spl.name()+"_")+(ext ? "ext" : "int"), h, "", col++);
}

void sfi::plot_samples(const SampleSet<1>& spls, const std::string& title, bool ext) {
  create_canvas(std::string(title)+(ext ? " ext":" int"));
  bool first = true;
  int col = 2;
  for(auto& s : spls) {
    auto h = s.histogram_1D(0,100,ext,AllEventSel());
    draw_histogram((s.name()+"_")+(ext ? "ext" : "int"), h, first ? "" : "SAME", col++);
    first = false;
  }
}

void sfi::plot_multi_graph(const std::string& title, const Vector<double>& xvals, const std::vector<Vector<double>>& yvals, double ymin, double ymax, const std::string& xtitle, const std::string& ytitle) {
  auto * can = create_canvas(title);
  const unsigned N = yvals.size();
  for(unsigned c=0; c<N; ++c) {
    auto* gr = create_graph(xvals, yvals[c], "");
    gr->SetLineColor(c+1);
    gr->Draw(c ? "LP" : "ALP");
    if(!c) {
      gr->GetXaxis()->SetTitle(xtitle.c_str());
      gr->GetYaxis()->SetTitle(ytitle.c_str());
    }
    gr->SetMaximum(ymax);
    gr->SetMinimum(ymin);
    gr->GetYaxis()->SetRangeUser(ymin, ymax);
  }
  can->Update();
}

void sfi::plot_coefficients_1D(const std::string& prefix, IEvaluator& ev, unsigned ncoeff, unsigned npts, const Matrix<double>& points, const TransformSet<ITransform>& trfs) {
  Vector<double> xvals(npts);
  std::vector<Vector<double>> yvals(ncoeff, Vector<double>(npts));
  std::vector<Vector<double>> uncvals(ncoeff, Vector<double>(npts));
  std::vector<Vector<double>> signvals(ncoeff, Vector<double>(npts));
  for(unsigned i=0; i<npts; ++i) {
    xvals[i] = -1.0 + 2.0*double(i)/double(npts-1);
    ev.set_slice(&xvals[i]);
    auto* st = const_cast<ITransform*>(ev.get_slice_transform());
    auto* cov = st->do_create_covariance(true);
    auto* pars = st->get_parameters();
    for(unsigned c=0; c<ncoeff; ++c) {
      yvals[c][i] = (*pars)(c);
      uncvals[c][i] = sqrt((*cov)(c,c));
      signvals[c][i] = std::abs(yvals[c][i]/uncvals[c][i]);
      if(!std::isfinite(signvals[c][i])) signvals[c][i] = 0.0;
    }
  }

  auto vars = ev.get_transform()->get_variables();
  const unsigned ntrfs = trfs.size();
  Vector<double> cvals(ntrfs);
  std::vector<Vector<double>> pvals(ncoeff, Vector<double>(ntrfs));
  for(unsigned i=0; i<ntrfs; ++i) {
    // TODO: Not generic!
    cvals[i] = vars->at(1).vtrans(points(i,0));
    for(unsigned c=0; c<ncoeff; ++c) pvals[c][i] = trfs[i].get_param(c).value();
  }

  plot_multi_graph(prefix+" Coefficient values", xvals, yvals, -1.1, 1.6, "parameter", "coefficient");
  TLegend* leg = new TLegend(0.2,0.76,0.93,0.94);
  for(unsigned c=0; c<ncoeff; ++c) {
    auto* gr = create_graph(cvals, pvals[c]);
    gr->SetMarkerSize(1.0);
    gr->SetMarkerStyle(20+c);
    gr->Draw("P");
    leg->AddEntry(gr,Form("Coeff %d",c));
  }
  leg->SetNColumns(2);
  leg->Draw();

  plot_multi_graph(prefix+" Coefficient uncertainties", xvals, uncvals, 0, 2.0);
  plot_multi_graph(prefix+" Coefficient significance", xvals, signvals, 0, 2.0);
}
