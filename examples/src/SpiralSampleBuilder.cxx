/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Sample.h"
#include "IAnalysis.h"
#include "SampleBuilderBase.h"

namespace sfi {
  class SpiralSampleBuilder : public SampleBuilderBase {
  public:
    SpiralSampleBuilder(Context& ctx, OptionMap&);

    virtual ~SpiralSampleBuilder();

    // ---- implementation of ISampleBuilder ----

    virtual ISample* do_build(Context& ctx, const PVariables& vars, const DataSource& src_data);
  };
}

using namespace sfi;

extern "C" {
  sfi::IComponent* sfi_factory_spiral(Context& ctx, OptionMap& opts) {
    return new sfi::SpiralSampleBuilder(ctx, opts);
  }
}

template<bool _UseWeight>
void build(Sample<1U,tbool<_UseWeight>>& spl, Context& ctx, unsigned nevents, bool is_signal) {
  std::normal_distribution<> norm(0.1, 0.06);
  auto& rnd = ctx.rnd_gen();
  double w,xi;
  for (unsigned i=0; i<nevents; ++i) {
    while(fabs(xi=norm(rnd))>1);
    if(!is_signal) {
      xi=-xi;
      w = -1;
    }
    else {
      w = 1;
    }
    spl.x(i, 0) = xi;
    if(_UseWeight) spl.w(i, w);
  }
  spl.name(std::string("spiral_1D_")+(!is_signal ? "D_bg" : "D_sig"));
  spl.is_normalized(true);
}

template<bool _UseWeight>
void build(Sample<2U,tbool<_UseWeight>>& spl, Context& ctx, unsigned nevents, bool is_signal, double uniform_frac) {
  std::uniform_real_distribution<> uni(0.0, 1.0);
  std::uniform_real_distribution<> uni2(-1.0, 1.0);
  std::normal_distribution<> norm(0.1, 0.07);
  auto& rnd = ctx.rnd_gen();
  double w,xi,yi;
  for (unsigned i=0; i<nevents; ++i) {
    if(uni(rnd) < uniform_frac) {
      w = 1;
      xi = uni2(rnd);
      yi = uni2(rnd);
    }
    else {
      double b=1.0/(4.*M_PI);
      double v=1.5 + 3.*M_PI*i/(double)nevents;
      while(fabs(xi=norm(rnd))>1);
      while(fabs(yi=norm(rnd))>1);
      if(!is_signal == 1) {
        xi-=b*v*cos(v);
        yi-=b*v*sin(v);
        w = -1;
      }
      else {
        xi+=b*v*cos(v);
        yi+=b*v*sin(v);
        w = 1;
      }
    }
    if(xi >= 0.99) xi = 0.99;
    else if(xi <= -0.99) xi = -0.99;
    if(yi >= 0.99) yi = 0.99;
    else if(yi <= -0.99) yi = -0.99;

    spl.x(i, 0) = xi;
    spl.x(i, 1) = yi;
    if(_UseWeight) spl.w(i, w);
  }
  spl.name(std::string("spiral_2D_")+(!is_signal ? "D_bg" : "D_sig"));
  spl.is_normalized(true);
}

template<bool _UseWeight>
void build(Sample<3U,tbool<_UseWeight>>& spl, Context& ctx, unsigned nevents, bool is_signal) {
  std::normal_distribution<> norm(0.1, 0.07);
  auto& rnd = ctx.rnd_gen();
  double w,xi,yi,zi;
  for (unsigned i=0; i<nevents; ++i) {
    double b=1.0/(4.*M_PI);
    double v=1.5 + 3.*M_PI*i/(double)nevents;
    while(fabs(xi=norm(rnd))>1);
    while(fabs(yi=norm(rnd))>1);
    while(fabs(zi=norm(rnd))>1);
    zi = -0.5 + zi + double(i)/(double)nevents;
    if(!is_signal == 1) {
      xi-=b*v*cos(v);
      yi-=b*v*sin(v);
      w = -1;
    }
    else {
      xi+=b*v*cos(v);
      yi+=b*v*sin(v);
      w = 1;
    }
    if(xi >= 0.99) xi = 0.99;
    else if(xi <= -0.99) xi = -0.99;
    if(yi >= 0.99) yi = 0.99;
    else if(yi <= -0.99) yi = -0.99;

    spl.x(i, 0) = xi;
    spl.x(i, 1) = yi;
    spl.x(i, 2) = zi;
    if(_UseWeight) spl.w(i, w);
  }
  spl.name(std::string("spiral_3D_")+(!is_signal ? "D_bg" : "D_sig"));
  spl.is_normalized(true);
}

SpiralSampleBuilder::SpiralSampleBuilder(Context& ctx, OptionMap& opts):SampleBuilderBase(ctx, opts) { }

SpiralSampleBuilder::~SpiralSampleBuilder() { }

// ---- implementation of ISampleBuilder ----

ISample*
SpiralSampleBuilder::do_build(Context& ctx, const PVariables& vars, const DataSource& src_data) {
  bool is_signal = src_data.options().get<bool>("is_signal");
  unsigned nevents = src_data.options().get<unsigned>("nevents");
  double uniform_frac = 0.0;
  if(src_data.options().has_type<double>("uniform_fraction")) {
    uniform_frac = src_data.options().get<double>("uniform_fraction");
  }
  ISample* res = this->create_sample(ctx,m_dims,m_is_weighted, nevents);
  res->set_variables(vars);
  if(!res) return res;
  do_log_info("do_build() dims="<<m_dims<<" nevents="<<nevents<<" is_weighted="<<m_is_weighted<<" is_signal="<<is_signal);

  if((m_dims == 1) && m_is_weighted) build(*dynamic_cast<Sample<1U,ttrue>*>(res), ctx, nevents, is_signal);
  else if((m_dims == 1) && !m_is_weighted) build(*dynamic_cast<Sample<1U,tfalse>*>(res), ctx, nevents, is_signal);
  else if((m_dims == 2) && m_is_weighted) build(*dynamic_cast<Sample<2U,ttrue>*>(res), ctx, nevents, is_signal, uniform_frac);
  else if((m_dims == 2) && !m_is_weighted) build(*dynamic_cast<Sample<2U,tfalse>*>(res), ctx, nevents, is_signal, uniform_frac);
  else if((m_dims == 3) && m_is_weighted) build(*dynamic_cast<Sample<3U,ttrue>*>(res), ctx, nevents, is_signal);
  else if((m_dims == 3) && !m_is_weighted) build(*dynamic_cast<Sample<3U,tfalse>*>(res), ctx, nevents, is_signal);
  return res;
}
