/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "PDFAnalysis.h"

using namespace sfi;

PDFAnalysis::PDFAnalysis(OptionMap& opts): Analysis(opts), m_log(Log::get(main_context(), "PDFAnalysis")) {
}

void
PDFAnalysis::maxevt(unsigned evt, const std::string& ch_name) {
  if(!this->has_channel(ch_name)) {
    do_log_error("maxevt() no channel named '"<<ch_name<<"'");
    return;
  }
  auto* ds = dynamic_cast<ParamDataSource*>(this->channel(ch_name).source("s0").get());
  ds->nevents(evt);
}

void
PDFAnalysis::mean(double m, const std::string& ch_name) {
  if(!this->has_channel(ch_name)) {
    do_log_error("mean() no channel named '"<<ch_name<<"'");
    return;
  }
  auto* ds = dynamic_cast<ParamDataSource*>(this->channel(ch_name).source("s0").get());
  ds->mean(m);
}

void
PDFAnalysis::set_gaus(const std::string& ch_name) {
  if(!this->has_channel(ch_name)) {
    do_log_error("set_gaus_uniform() no channel named '"<<ch_name<<"'");
    return;
  }
  auto* ds = dynamic_cast<ParamDataSource*>(this->channel(ch_name).source("s0").get());
  ds->mode("gaus");
}

void
PDFAnalysis::set_gaus_uniform(double frac, const std::string& ch_name) {
  if(!this->has_channel(ch_name)) {
    do_log_error("set_gaus_uniform() no channel named '"<<ch_name<<"'");
    return;
  }
  auto* ds = dynamic_cast<ParamDataSource*>(this->channel(ch_name).source("s0").get());
  ds->mode("gaus_uniform");
  ds->uniform_fraction(frac);
}

void
PDFAnalysis::set_expo(const std::string& ch_name) {
  if(!this->has_channel(ch_name)) {
    do_log_error("set_expo() no channel named '"<<ch_name<<"'");
    return;
  }
  auto* ds = dynamic_cast<ParamDataSource*>(this->channel(ch_name).source("s0").get());
  ds->mode("expo");
}

void
PDFAnalysis::set_gaus_expo(double frac, const std::string& ch_name) {
  if(!this->has_channel(ch_name)) {
    do_log_error("set_gaus_expo() no channel named '"<<ch_name<<"'");
    return;
  }
  auto* ds = dynamic_cast<ParamDataSource*>(this->channel(ch_name).source("s0").get());
  ds->mode("gaus_expo");
  ds->uniform_fraction(frac);
}

void
PDFAnalysis::set_breit_wigner(const std::string& ch_name) {
  if(!this->has_channel(ch_name)) {
    do_log_error("set_breit_wigner() no channel named '"<<ch_name<<"'");
    return;
  }
  auto* ds = dynamic_cast<ParamDataSource*>(this->channel(ch_name).source("s0").get());
  ds->mode("breit_wigner");
}

void
PDFAnalysis::set_breit_wigner_uniform(double frac, const std::string& ch_name) {
  if(!this->has_channel(ch_name)) {
    do_log_error("set_breit_wigner_uniform() no channel named '"<<ch_name<<"'");
    return;
  }
  auto* ds = dynamic_cast<ParamDataSource*>(this->channel(ch_name).source("s0").get());
  ds->mode("breit_wigner_uniform");
  ds->uniform_fraction(frac);
}

void
PDFAnalysis::set_step(double frac, const std::string& ch_name) {
  if(!this->has_channel(ch_name)) {
    do_log_error("set_step() no channel named '"<<ch_name<<"'");
    return;
  }
  auto* ds = dynamic_cast<ParamDataSource*>(this->channel(ch_name).source("s0").get());
  ds->mode("step");
  ds->uniform_fraction(frac);
}

void
PDFAnalysis::set_sine(const std::string& ch_name) {
  if(!this->has_channel(ch_name)) {
    do_log_error("set_step() no channel named '"<<ch_name<<"'");
    return;
  }
  auto* ds = dynamic_cast<ParamDataSource*>(this->channel(ch_name).source("s0").get());
  ds->mode("sine");
}
