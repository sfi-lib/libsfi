/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "tmva_util.h"

#include <TCanvas.h>
#include <TLegend.h>

#include "ext_utils.h"

using namespace sfi;

void sfi::plot_tmva_results(BinaryClassifierPlotsBase& bcplots, TFile* inFile, bool train) {
  TCanvas* can = bcplots.roc_canvas(train ? BinaryClassifierBase::train : BinaryClassifierBase::test);
  can->cd();
  TString hiname = train ? "trainingRejBvsS" : "rejBvsS";
  TLegend* legend = bcplots.roc_legend(train ? BinaryClassifierBase::train : BinaryClassifierBase::test);

  TH1D* hi_bdt = sfi::draw_histo_clone(inFile, "data/Method_BDT/BDT/MVA_BDT_"+hiname, 2, legend, "BDT");
  TH1D* hi_mlp = sfi::draw_histo_clone(inFile, "data/Method_MLP/MLP/MVA_MLP_"+hiname, 3, legend, "MLP");
  TH1D* hi_ld = sfi::draw_histo_clone(inFile, "data/Method_LD/LD/MVA_LD_"+hiname, 4, legend, "LD");
  TH1D* hi_ldd = sfi::draw_histo_clone(inFile, "data/Method_LDD/LDD/MVA_LDD_"+hiname, 6, legend, "LDD");
  if(legend) legend->Draw();

  std::string prefix = std::string("ROC ")+(train ? "train" : "test");
  if(hi_bdt) std::cout<<prefix<<" BDT "<<(0.01*hi_bdt->Integral(1,9999))<<std::endl;
  if(hi_mlp) std::cout<<prefix<<" MLP "<<(0.01*hi_mlp->Integral(1,9999))<<std::endl;
  if(hi_ld) std::cout<<prefix<<" LD  "<<(0.01*hi_ld->Integral(1,9999))<<std::endl;
  if(hi_ldd) std::cout<<prefix<<" LDD "<<(0.01*hi_ldd->Integral(1,9999))<<std::endl;
  can->Update();
}

void sfi::copy_tmva_efficiencies(LatexTable& eff_table, TMVA::MethodBase* m, unsigned row) {
  double err;
  eff_table(row,0) = m->GetEfficiency("Efficiency:0.01", TMVA::Types::kTesting, err);
  double max_err = err;
  eff_table(row,1) = m->GetTrainingEfficiency("Efficiency:0.01");
  eff_table(row,2) = m->GetEfficiency("Efficiency:0.1", TMVA::Types::kTesting, err);
  max_err = std::max(max_err, err);
  eff_table(row,3) = m->GetTrainingEfficiency("Efficiency:0.1");
  eff_table(row,4) = m->GetEfficiency("Efficiency:0.3", TMVA::Types::kTesting, err);
  max_err = std::max(max_err, err);
  eff_table(row,5) = m->GetTrainingEfficiency("Efficiency:0.3");
  std::cout<<"copy_efficiencies() max_err="<<max_err<<std::endl;
}

void sfi::copy_tmva_summary(LatexTable& table, unsigned row, TMVA::MethodBase* m, TMVA::Factory* factory, const TString& mname) {
  table(row,0) = m->GetTrainTime();
  table(row,1) = m->GetTestTime();
  table(row,2) = factory->GetROCIntegral("data", mname);
  std::cout<<"ROC '"<<mname<<"'="<<table(row,2)<<" time= "<<table(row,0)<<" test time="<<table(row,1)<<std::endl;
}
