/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Sample.h"
#include "ParamSampleBuilder.h"
#include "IAnalysis.h"

#include <iomanip>

using namespace sfi;

extern "C" {
  sfi::IComponent* sfi_factory_ExampleParam(Context& ctx, OptionMap& opts) {
    return new sfi::ParamSampleBuilder(ctx, opts);
  }
}

template<unsigned _Dims, class _Rnd>
bool
fill_event(Sample<_Dims>& s, Log log, const PVariables& vars, const DataSource& evt, _Rnd& _rnd) {
  const unsigned maxevents = evt.nevents();
  auto& opts = evt.options();
  double xscale = opts.get_as<double>("xscale");
  double uniform_fraction = opts.get_as<double>("uniform_fraction");
  int mode = ParamDataSource::Gaus;
  if(opts.has_type<std::string>("mode")) {
    std::string distr = opts.get<std::string>("mode");
    if(distr == "gaus_expo") mode = ParamDataSource::GausExpo;
    else if(distr == "expo") mode = ParamDataSource::Expo;
    else if(distr == "gaus") mode = ParamDataSource::Gaus;
    else if(distr == "gaus_uniform") mode = ParamDataSource::GausUniform;
    else if(distr == "breit_wigner") mode = ParamDataSource::BreitWigner;
    else if(distr == "breit_wigner_uniform") mode = ParamDataSource::BreitWignerUniform;
    else if(distr == "step") mode = ParamDataSource::UniformUniform;
    else if(distr == "sine") mode = ParamDataSource::Sine;
    else if(distr == "gamma") mode = ParamDataSource::Gamma;
    else if(distr == "uniform") mode = ParamDataSource::Uniform;
    else {
      log_error(log,"fill_event() invalid mode '"<<distr<<"'");
      return false;
    }
  }
  s.variables(vars);
  double offset = opts.get_as<double>("offset", 0.0);
  
  bool all_ok = true;
  Random<Context::RandomTy> rnd(_rnd);
  double x;
  for(unsigned eventno=0;  eventno<maxevents; ++eventno) {
    for(unsigned d=0; d<_Dims; ++d) {
      const double c1(((!d) && opts.has("mean")) ? opts.get_as<double>("mean") : opts.get<std::vector<double>>("means")[d]);
      const double c2(((!d) && opts.has("spread")) ? opts.get_as<double>("spread") : opts.get<std::vector<double>>("spreads")[d]);
      const double minv = vars->at(d).external_interval_set() ? s.variables()->at(d).minval() : -1;
      const double maxv = vars->at(d).external_interval_set() ? s.variables()->at(d).maxval() : 1;
      if(!eventno) {
        log_debug(log, "fill_event() x "<<minv<<" -> "<<maxv<<" c1: "<<c1<<" c2: "<<c2<<" N: "<<maxevents);
      }
      if(((mode == ParamDataSource::GausUniform) || (mode == ParamDataSource::BreitWignerUniform) || (mode == ParamDataSource::UniformUniform)) && (rnd.uniform(0,1)< uniform_fraction)) {
        s.x(eventno, d) = rnd.uniform(minv,maxv);
      }
      else if(((mode == ParamDataSource::GausExpo) || (mode == ParamDataSource::BreitWignerExpo)) && (rnd.uniform(0,1)< uniform_fraction)) {
        while(fabs(x = rnd.expo(0.5)) >= 2.0);
        s.x(eventno, d) = x - 1.0;
      }
      else if((mode == ParamDataSource::BreitWigner) || (mode == ParamDataSource::BreitWignerExpo) || (mode == ParamDataSource::BreitWignerUniform)) {
        do {
          x = rnd.breit_wigner(xscale*c1, c2);
        }
        while((x < minv) || (x > maxv));
        s.x(eventno, d) = x;
      }
      else if((mode == ParamDataSource::Gaus) || (mode == ParamDataSource::GausExpo) || (mode == ParamDataSource::GausUniform)) {
        do {
          x = rnd.gaus(xscale*c1, c2);
        }
        while((x < minv) || (x > maxv));
        s.x(eventno, d) = x;
      }
      else if(mode == ParamDataSource::Expo) {
        do { x = rnd.expo(c1) + offset; }
        while((x < minv) || (x > maxv));
        s.x(eventno, d) = x;
      }
      else if(mode == ParamDataSource::UniformUniform) {
        s.x(eventno, d) = rnd.uniform(minv,0);
      }
      else if(mode == ParamDataSource::Sine) {
        double y;
        do {
          x = rnd.uniform(minv, maxv);
          y = rnd.uniform(-1,1);
        } while(y < sin(M_PI*x + c1));
        s.x(eventno, d) = x;
      }
      else if(mode == ParamDataSource::Gamma) {
        std::gamma_distribution<> ga(square(c1/c2), c2*c2/c1);
        do { x = ga(_rnd); }
        while((x < minv) || (x > maxv));
        s.x(eventno, d) = x;
      }
      else if(mode == ParamDataSource::Uniform) {
        s.x(eventno, d) = rnd.uniform(minv,maxv);
      }
      else {
        log_error(log,"fill_event() invalid mode_ "<<mode);
        all_ok = false;
      }
    }
  }
  return all_ok;
}

ParamSampleBuilder::ParamSampleBuilder(Context& ctx, OptionMap& opts):SampleBuilderBase(ctx, opts) { }

ParamSampleBuilder::~ParamSampleBuilder() { }

ISample*
ParamSampleBuilder::do_build(Context& ctx, const PVariables& vars, const DataSource& evt) {
  if(m_dims > 5) return 0;

  const unsigned maxevents = evt.nevents();
  if(m_dims == 1) {
    auto* s = ctx.create_object<Sample<1>>(maxevents, "");
    fill_event<1>(*s, m_log, vars, evt, ctx.rnd_gen());
    return s;
  }
  else if(m_dims == 2) {
    auto* s = ctx.create_object<Sample<2>>(maxevents, "");
    fill_event<2>(*s, m_log, vars, evt, ctx.rnd_gen());
    return s;
  }
  else if(m_dims == 3) {
    auto* s = ctx.create_object<Sample<3>>(maxevents, "");
    fill_event<3>(*s, m_log, vars, evt, ctx.rnd_gen());
    return s;
  }
  else if(m_dims == 4) {
    auto* s = ctx.create_object<Sample<4>>(maxevents, "");
    fill_event<4>(*s, m_log, vars, evt, ctx.rnd_gen());
    return s;
  }
  else if(m_dims == 5) {
    auto* s = ctx.create_object<Sample<5>>(maxevents, "");
    fill_event<5>(*s, m_log, vars, evt, ctx.rnd_gen());
    return s;
  }
  else {
    do_log_error("do_build() Invalid dimensions "<<m_dims);
    return 0;
  }
}

std::shared_ptr<DataSource> ParamSampleBuilder::get_data_source(const OptionMap& opts) const {
  return std::make_shared<ParamDataSource>(opts, m_dims);
}

ParamDataSource::ParamDataSource(const std::string& name, unsigned dims, const std::string& mode, unsigned nevents, unsigned index):DefaultDataSource(name, index, nevents, dims),
    m_mode(mode), m_xscale(1.0), m_uniform_fraction(0.05), m_means(dims, 0.0), m_spreads(dims, 0.0) {
  m_src_opts.bind("mode", m_mode).bind("xscale", m_xscale).bind("uniform_fraction", m_uniform_fraction);
  m_src_opts.bind("means", m_means).bind("spreads", m_spreads);
}

ParamDataSource::ParamDataSource(const std::string& name, const std::string& mode, double m, double s, unsigned nevents, unsigned index):DefaultDataSource(name, index, nevents, 1),
    m_mode(mode), m_xscale(1.0), m_uniform_fraction(0.05), m_means({m}), m_spreads({s}) {
  m_src_opts.bind("mode", m_mode).bind("xscale", m_xscale).bind("uniform_fraction", m_uniform_fraction);
  m_src_opts.bind("means", m_means).bind("spreads", m_spreads);
}

ParamDataSource::ParamDataSource(const OptionMap& opts, unsigned dims):DefaultDataSource(opts),
    m_mode("gaus"), m_xscale(1.0), m_uniform_fraction(0.05) {
  m_src_opts.bind("mode", m_mode).bind("xscale", m_xscale).bind("uniform_fraction", m_uniform_fraction);
  m_src_opts.bind("means", m_means).bind("spreads", m_spreads);
  m_means.resize(dims, 0.0);
  m_spreads.resize(dims, 0.0);
}

std::shared_ptr<DataSource> ParamDataSource::do_clone() const { return std::make_shared<ParamDataSource>(this->m_src_opts, this->dims()); }
