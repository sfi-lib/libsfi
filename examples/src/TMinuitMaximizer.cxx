/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "TMinuitMaximizer.h"

#include <TMinuit.h>

using namespace sfi;

extern "C" {
  sfi::IComponent* sfi_factory_TMinuit(Context&, OptionMap& opts) {
    return (sfi::IComponent*)(new sfi::TMinuitFactory(opts));
  }
}

TMinuitMaximizer::TMinuitMaximizer(Analysis& ana, const OptionMap& opts, ILikelihood* lh, const OptionMap& lhopts, const OptionMap* meta):
    m_log(Log::get(ana.main_context(), "TMinuitMaximizer")),
    m_context(ana.main_context()), m_parameters(ana.build_parameters(opts.get_as<std::string>("parameter_set"), meta)),
    m_minuit(0), m_lhood(lh),
    m_max_calls_per_fit(opts.get_as<unsigned>("max_calls_per_fit", 10000)),
    m_max_repeats(opts.get_as<unsigned>("max_repeats", 40)),
    m_tolerance(opts.get_as<double>("tolerance", 0.1)),
    m_debug(opts.get_as<bool>("debug_minuit", false)),
    m_do_minos(opts.get_as<bool>("do_minos", false)),
    m_do_hesse(opts.get_as<bool>("do_hesse", false)) {
  init(opts, lhopts);
}

TMinuitMaximizer::TMinuitMaximizer(Parameters&& pars, Context& ctx, const OptionMap& opts, ILikelihood* lh, const OptionMap& lhopts):
    m_log(Log::get(ctx, "TMinuitMaximizer")),
    m_context(ctx), m_parameters(std::move(pars)),
    m_minuit(0), m_lhood(lh),
    m_max_calls_per_fit(opts.get_as<unsigned>("max_calls_per_fit", 10000)),
    m_max_repeats(opts.get_as<unsigned>("max_repeats", 40)),
    m_tolerance(opts.get_as<double>("tolerance", 0.1)),
    m_debug(opts.get_as<bool>("debug_minuit", false)),
    m_do_minos(opts.get_as<bool>("do_minos", false)),
    m_do_hesse(opts.get_as<bool>("do_hesse", false)) {
  init(opts, lhopts);
}

void TMinuitMaximizer::init(const OptionMap& opts, const OptionMap& lhopts) {
  m_lhood->do_init_parameters(lhopts, m_parameters);
  m_minuit = new TMinuit(m_parameters.size());
  int ierflg = 0;
  double arglist[10];

  if(m_debug) {
    m_minuit->SetPrintLevel(3);
    arglist[0] = -1;
    m_minuit->mnexcm("SET DEB", arglist ,1,ierflg);
  }
  else m_minuit->SetPrintLevel(-1);

  init_params(false);
  
  m_minuit->SetFCN(fcn);
  arglist[0] = 1;
  m_minuit->mnexcm("SET ERR", arglist ,1,ierflg);
  int str = 0;
  opts.get_if("strategy", str);
  set_strategy(str);
  s_instance = this;
}

void TMinuitMaximizer::fcn(int &, double *, double &f, double *par, int) {
  s_instance->m_parameters.set_working_values(par);
  f = s_instance->m_lhood->do_eval(s_instance->m_context);
}

// ---- implementation of ILikelihoodMaximizer ----

TMinuitMaximizer::~TMinuitMaximizer() {
  if(m_minuit) {
    delete m_minuit;
    m_minuit = 0;
  }
  if(m_lhood) {
    delete m_lhood;
    m_lhood = 0;
  }
}

void TMinuitMaximizer::do_fix_parameter(unsigned par, double val) {
  set_parameter_value(par, val);
  m_minuit->FixParameter(par);
}

bool TMinuitMaximizer::do_maximize() {
  s_instance = this;
  double arglist[10];
  int ierflg = 0;
  if(m_debug) {
    m_minuit->mnexcm("SHOW PAR",arglist,0,ierflg);
  }
  if(!do_run_migrad(m_max_calls_per_fit, m_tolerance)) return false;

  if(m_do_hesse) {
    if(!do_run_hesse()) return false;
  }
  if(m_do_minos) {
    if(!do_run_minos()) return false;
  }
  return true;
}

bool TMinuitMaximizer::get_profile(unsigned par, unsigned npts, Vector<double>& xv, Vector<double>& fv, double sigmas_range) {
  auto& p = m_parameters[par];
  const double xmin = std::max(p.limit_min(), p.value() - sigmas_range*p.uncertainty());
  const double xmax = std::min(p.limit_max(), p.value() + sigmas_range*p.uncertainty());
  const double dx = (xmax - xmin)/double(npts - 1);

  std::cout<<"TMinuitMaximizer::get_profile() value: "<<p.value()<<" +- "<<p.uncertainty()<<" x "<<xmin<<" -> "<<xmax<<std::endl;
  double x = xmin;
  int ierflg = 0;
  double arglist[10];
  m_minuit->FixParameter(par);
  for(unsigned i=0; i<npts; ++i) {
    if(p.is_limited()) m_minuit->mnparm(par, p.name(), x, p.step_length(), p.limit_min(), p.limit_max(), ierflg);
    else m_minuit->mnparm(par, p.name(), x, p.step_length(), 0 , 0, ierflg);

    arglist[0] = m_max_calls_per_fit;
    arglist[1] = m_tolerance;
    m_minuit->mnexcm("MIGRAD",arglist,2,ierflg);

    xv[i] = x;
    fv[i] = m_minuit->fAmin;
    x += dx;
  }
  m_minuit->Release(par);
  
  // Reset minimum
  if(p.is_limited()) m_minuit->mnparm(par, p.name(), p.value(), p.step_length(), p.limit_min(), p.limit_max(), ierflg);
  else m_minuit->mnparm(par, p.name(), p.value(), p.step_length(), 0 , 0, ierflg);
  
  arglist[0] = m_max_calls_per_fit;
  arglist[1] = m_tolerance;
  m_minuit->mnexcm("MIGRAD",arglist,2,ierflg);
  
  return true;
}

bool TMinuitMaximizer::get_contour(unsigned par1, unsigned par2, unsigned npts, Vector<double>& v1, Vector<double>& v2) {
  int ierrf(0);
  v1.rows(npts);
  v2.rows(npts);
  m_minuit->mncont(par1, par2, npts, v1.data_ptr(), v2.data_ptr(), ierrf);
  return ierrf == 0;
}

double TMinuitMaximizer::get_minval() const {
  double amin, z;
  int q;
  m_minuit->mnstat(amin, z, z, q, q, q);
  return amin;
}

ILikelihoodMaximizer& TMinuitMaximizer::set_strategy(int i) {
  int ierflg = 0;
  double arglist[10];
  arglist[0] = i;
  m_minuit->mnexcm("SET STR",arglist,1,ierflg);
  return *this;
}

void TMinuitMaximizer::set_print_level(PrintLevel pl) {
  if(pl <= PrintLevel::Debug) {
    double arglist[10];
    int ierflg = 0;
    m_minuit->SetPrintLevel(3);
    arglist[0] = -1;
    m_minuit->mnexcm("SET DEB", arglist ,1,ierflg);
  }
  else m_minuit->SetPrintLevel(-1);
}

bool TMinuitMaximizer::set_parameter_value(unsigned p, double v) {
  int ierflg = 0;
  auto& pa = m_parameters[p];
  m_minuit->mnparm(p, pa.name().c_str(), v, pa.step_length(), pa.limit_min(), pa.limit_max(), ierflg);
  return !ierflg;
}

bool TMinuitMaximizer::get_covariance(MatrixSym<double>& cov) {
  m_minuit->mnemat(cov.data_ptr(), cov.rows());
  return true;
}

bool TMinuitMaximizer::do_run_migrad(unsigned maxcalls, double tole) {
  double arglist[10];
  int ierflg = 0;
  arglist[0] = maxcalls;
  arglist[1] = tole;
  unsigned nrepeats = 0;
  bool repeat(true);
  while(repeat && (nrepeats < m_max_repeats)) {
    m_minuit->mnexcm("MIGRAD",arglist,2,ierflg);
    if(ierflg) {
      do_log_warning("do_run_migrad() failed, will retry, attempt "<<nrepeats);
      init_params(true);
      repeat = true;
    }
    else repeat = false;
    ++nrepeats;
  }
  if((nrepeats > 1)) {
    if(repeat) {
      do_log_error("do_run_migrad() failed");
    }
    else {
      do_log_ok("do_run_migrad() Succeeded after "<<nrepeats<<" attempts");
    }
  }
  copy_params(false);
  return !repeat;
}

bool TMinuitMaximizer::do_run_minos() {
  double arglist[10];
  int ierflg = 0;
  unsigned nrepeats = 0;
  bool repeat(true);
  
  while(repeat && (nrepeats < m_max_repeats)) {
    m_minuit->mnexcm("MINOS",arglist,0,ierflg);
    if(ierflg) {
      do_log_error("MINOS failed");
      return false;
    }
    if(!copy_params(true)) {
      while(!do_run_migrad(m_max_calls_per_fit, m_tolerance)) {}
      repeat = true;
    }
    else repeat = false;
    ++nrepeats;
  }
  if(nrepeats > 1) {
    if(repeat) {
      do_log_error("do_run_minos() Failed after "<<nrepeats<<" attempts");
    }
    else {
      do_log_ok("do_run_minos() Succeeded after "<<nrepeats<<" attempts");
    }
  }
  return !repeat;
}

bool TMinuitMaximizer::do_run_hesse() {
  double arglist[10];
  int ierflg = 0;
  m_minuit->mnexcm("HESSE",arglist,0,ierflg);
  if(ierflg) {
    do_log_error("HESSE failed");
    return false;
  }
  copy_params(false);
  return true;
}

// ---- protected methods ----

void TMinuitMaximizer::init_params(bool randomize) {
  auto& rnd = m_context.rnd_gen();
  int ierflg = 0;
  for(unsigned i=0; i<m_parameters.size(); ++i) {
    auto& p = m_parameters[i];
    if(!p.is_fixed() && randomize) {
      std::normal_distribution<> norm(p.initial(), p.initial()*0.5);
      if(p.is_limited()) {
        double val(p.initial());
        do {
          val = norm(rnd);
        }
        while((val <= p.limit_min()) || (val >= p.limit_max()));
        m_minuit->mnparm(i, p.name(), val, p.step_length(), p.limit_min(), p.limit_max(), ierflg);
      }
      else m_minuit->mnparm(i, p.name(), norm(rnd), p.step_length(), 0 , 0, ierflg);
    } else {
      if(p.is_limited()) m_minuit->mnparm(i, p.name(), p.initial(), p.step_length(), p.limit_min(), p.limit_max(), ierflg);
      else m_minuit->mnparm(i, p.name(), p.initial(), p.step_length(), 0 , 0, ierflg);
    }
    
    if(p.is_fixed()) m_minuit->FixParameter(i);
  }
}

bool TMinuitMaximizer::copy_params(bool minos_unc) {
  double v,e;
  bool all_ok = true;
  for(unsigned i=0; i<m_parameters.size(); ++i) {
    m_minuit->GetParameter(i, v, e);
    m_parameters[i].value(v).uncertainty(e);
    if(minos_unc && !m_parameters[i].is_fixed()) {
      double ep, em, epar, gcc;
      m_minuit->mnerrs (i, ep, em, epar, gcc);
      bool epd = (ep != 0.0) && (std::abs((ep - e)/e) > 0.5);
      bool emd = (em != 0.0) && (std::abs((-em - e)/e) > 0.5);
      bool epard = std::abs((epar - e)/e) > 0.5;
      bool ez = (ep == 0.0) && (em == 0.0);
      bool ed = (ep != 0.0) && (em != 0.0) && (std::abs((ep + em)/em) > 0.5);
      if(epd || emd || epard || ed || ez) {
        do_log_warning("copy_params() "<<m_parameters[i]<<" ep: "<<ep<<"("<<epd<<")"<<" em: "<<em<<"("<<emd<<")"<<" epar: "<<epar<<"("<<epard<<") zero: "<<ez<<" asym: "<<ed);
      }
      if(ez) all_ok = false;
    }
  }
  return all_ok;
}

TMinuitMaximizer* TMinuitMaximizer::s_instance = 0;
