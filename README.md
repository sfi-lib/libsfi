SFI is a C++ library that implements sparse Fourier interpolation of densities in higher dimensions documented in [https://arxiv.org/abs/2202.13801](https://arxiv.org/abs/2202.13801).

It depends on the ROOT framework (see root.cern.ch).

To checkout the code do

```
git clone https://gitlab.com/sfi-lib/libsfi.git
```

# Building

Set correct paths to cmake and ROOT.

To initialize the build in a separate directory (./build/) and target install directory (install_opt/):

```
cmake -DCMAKE_BUILD_TYPE=Release -DWITH_ROOT=TRUE -DWITH_PYSFI=TRUE -DCMAKE_INSTALL_PREFIX=install_opt/ -B build/ -S .

make -C build/ -j6
```
WITH_ROOT Enable packages dependent on ROOT

WITH_PYSFI Enable package pysfi, the python binding for SFI

The option '-j6' enables parallel building, 6 is the number of processes.

Libraries and binaries will be placed in the build/ directory.

To debug build with:

```
cmake -DCMAKE_BUILD_TYPE=Debug -B build/ -S .
```

To build in optimized mode with sanitizers and debug symbols

```
cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo -B build/ -S .
```

The release build will disable all assertions in the library, 
it is therefore recommended to test in both debug and release builds. 

Make an installation of the build:

```
make -C build/ install
```

# Environment

```
SFI_PATH
```
Location of the SFI installation. Default location for options files.

```
SFI_PYLIB_PATH
```
Location of library for pysfi.

# Linking to SFI
The package ext_test contains a minimal example of how to link to SFI from external software.

Build with

```
export CMAKE_PREFIX_PATH=<INSTALL>
cmake -DCMAKE_BUILD_TYPE=Release -B build/ -S .
make -C build
```
where <INSTALL> is the target installation directory for SFI.

* CMakeLists.txt builds all executables below prog and link with ROOT and SFI.
* dummy_data.cxx Creates a text file with dummy data

```
build/dummy_data --Nevents=10000 --file_name="dummy_data.txt"
```

* exttest.cxx Reads the dummy data file and creates a transform

```
./build/exttest --input_file="dummy_data.txt" --degrees=15 --output_prefix="Ext"
```

# Running

## Paper plots:

### PDF 1D

```
./build/paperexamples/pdf_1D
```
Available options:
- ``nevents`` Number of events
- ``npdfbins`` Number of bins for the pdf plotting
- ``mean`` "Mean" of distributions
- ``degrees`` Number of degrees in the transform
- ``do_pe`` (bool) Run pseudo experiments
- ``do_ml`` (bool) Run ML transformation
- ``do_amp`` (bool) Do the Amp.transformation
- ``do_plot_details`` (bool)
- ``trans_type`` (string) Type of eigen function : cos, fourier, legendre

### Transformation properties
```
./build/paperexamples/coefficients --run_pseudo_experiments=true --nexperiments=100 --nevents=1000 --nbsevents=1000 --do_lin=false --do_amp=true
```

Configuration:

```
paperexamples/config/coefficients.opts
```


### 2D classification

```
./build/paperexamples/class_spiral_2d
```

Configuration:

```
paperexamples/config/spiral_2D.opts
```

### 1D parameter estimation

```
./build/paperexamples/param_1d
```

Configuration:

```
paperexamples/config/param_1d.opts
```

## Examples

```
./build/examples/1_1D_proj_gaus
./build/examples/2_1D_amp_gaus
./build/examples/3_1D_hard_amp
./build/examples/4_1D_var_trans
./build/examples/5_2D_spiral
```

### Spherical harmonics

```
./build/examples/ylm_pdf --do_amp=true --do_lin=true
```

Configuration:

```
examples/config/ylm_pdf.opts
```

## Python examples:
Initialize with

```
> export SFI_PYLIB_PATH=<BUILD_DIR>/pysfi/
> export PYTHONPATH=$PYTHONPATH:./pysfi/python/
```
Simple 1D transformation of a Gaus

```
python pysfi/examples/gaus_1D.py
```

Transformation of a 2D spiral

```
python pysfi/examples/spiral_2D.py
```

Plot of spherical harmonics

```
python pysfi/examples/ylm.py
```
