#include "pseudo_experiment_plots.h"

#include "ext_utils.h"
#include "gui.h"

// for plotting
#include <TGraphErrors.h>
#include <TH2D.h>
#include <TH1D.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TLine.h>

namespace sfi {
  void draw_case_graph(const std::string& title, const std::vector<int>& mcolors, const std::vector<int>& mstyles, const double dx, const std::vector<TGraphErrors*>& grs,
      const std::vector<std::string>& case_names, const std::string& ytitle="", const std::string& file_name_prefix = "",
      bool extra_left_space = false, double scale_y_axis = 0.05, bool center_y = false, double hor_line = std::nan("0")) {
    assert(mcolors.size() == grs.size() && "Invalid size");
    assert(mstyles.size() == grs.size() && "Invalid size");
    assert(case_names.size() == grs.size() && "Invalid size");

    double legend_x1 = 0.21;
    auto& can = Canvas::get(title);
    auto* tc = can.canvas();

    tc->SetTopMargin(0.07);
    tc->SetRightMargin(0.07);
    if(extra_left_space) {
      legend_x1 = 0.29;
      tc->SetLeftMargin(0.25);
    }

    TLegend *leg = new TLegend(legend_x1,0.83, 0.92,0.91);
    double gmin(1e10), gmax(-1e10);
    for(unsigned nr=0; nr<grs.size(); ++nr) {
      auto r = draw_graph_errors(tc, grs[nr], case_names[nr], (nr == 0) ? "AP": "P SAME", mcolors[nr], mstyles[nr], leg, nr, dx);
      gmin = min(gmin, r.first);
      gmax = max(gmax, r.second);
    }
    leg->SetNColumns(mcolors.size());
    gmax *= (1.0 + scale_y_axis);
    if(gmin < 0) gmin *= (1.0 + scale_y_axis);
    else gmin *= (1.0 - scale_y_axis);

    if(center_y && (gmax > 0) && (gmin < 0)) {
      if(fabs(gmax) > fabs(gmin)) gmin = -gmax;
      else gmax = -gmin;
    }

    grs[0]->SetTitle(title.c_str());
    grs[0]->SetMinimum(gmin);
    grs[0]->SetMaximum(gmax);
    grs[0]->GetYaxis()->SetRange(gmin, gmax);
    grs[0]->GetYaxis()->SetMaxDigits(4);
    grs[0]->GetYaxis()->SetTitle(ytitle.c_str());
    if(extra_left_space) {
      grs[0]->GetYaxis()->SetTitleOffset(2.1);
    }
    else {
      grs[0]->GetYaxis()->SetTitleOffset(1.4);
    }
    grs[0]->GetXaxis()->SetTitle("parameter value");
    grs[0]->GetXaxis()->SetNdivisions(303, kTRUE);
    tc->SetGridx(true);
    tc->SetGridy(true);
    leg->Draw();
    if(!std::isnan(hor_line)) {
      TLine* l = new TLine(grs[0]->GetXaxis()->GetXmin(), hor_line, grs[0]->GetXaxis()->GetXmax(), hor_line);
      l->SetLineColor(kBlack);
      l->Draw();
    }
    tc->Update();
    if(!file_name_prefix.empty()) {
      tc->Print((file_name_prefix+"_"+title+".pdf").c_str());
    }
  }
}

void
sfi::plot_pseudo_experiment_results(PseudoExperimentBase& expr, bool plot_details, bool /*plot_covariance*/) {
  std::vector<int> mcolors;
  std::vector<int> mstyles;
  std::vector<std::string> case_names;
  for(unsigned nc=0; nc<expr.ncases(); ++nc) {
    auto& cas = expr.test_case(nc);
    case_names.push_back(cas.title());
    mcolors.push_back(cas.marker_color());
    mstyles.push_back(cas.marker_style());
  }

  const unsigned npullpts(expr.nparam_values());
  for(unsigned pa=0; pa<expr.nparams(); ++pa) {
    std::vector<TGraphErrors*> gr_param_v, gr_param_rel_v, gr_param_e_v, gr_param_pull_v, gr_rtime_v;
    // TODO: Might not be good enough
    bool is_const = expr.nparam_values() > 1 && (expr.param_value(0)[pa] == expr.param_value(1)[pa]);

    for(unsigned nc=0; nc<expr.ncases(); ++nc) {
      auto& cas = expr.test_case(nc);
      const std::string suf0 = expr.param_name(pa)+" "+cas.name();

      TGraphErrors *gr_param = new TGraphErrors(npullpts);
      TGraphErrors *gr_param_rel = new TGraphErrors(npullpts);
      TGraphErrors *gr_param_e = new TGraphErrors(npullpts);
      TGraphErrors *gr_param_pull = new TGraphErrors(npullpts);
      TGraphErrors *gr_rtime = 0;
      if(expr.time_statistics()) gr_rtime = new TGraphErrors(npullpts);
      std::map<std::string, TGraphErrors *> aux_param_plots;
      if(!pa) {
        for(auto& ap : cas.result(0).aux_param) {
          std::cout<<"plot_pseudo_experiment_results() aux "<<ap.first<<std::endl;
          aux_param_plots[ap.first] = new TGraphErrors(npullpts);
        }
      }
      
      for(unsigned k=0; k<npullpts; ++k) {
        const double xval = is_const ? k : expr.param_value(k)[pa];

        gr_param->SetPoint(k, xval, cas.result(k).param[pa].value());
        gr_param->SetPointError(k, 0, cas.result(k).param[pa].uncertainty());

        gr_param_rel->SetPoint(k, xval, cas.result(k).param_reldiff[pa].value());
        gr_param_rel->SetPointError(k, 0, cas.result(k).param_reldiff[pa].uncertainty());

        gr_param_e->SetPoint(k, xval, cas.result(k).param_e[pa].value());
        gr_param_e->SetPointError(k, 0, cas.result(k).param_e[pa].uncertainty());

        gr_param_pull->SetPoint(k, xval, cas.result(k).param_pull[pa].value());
        gr_param_pull->SetPointError(k, 0, cas.result(k).param_pull[pa].uncertainty());

        if(expr.time_statistics()) {
          gr_rtime->SetPoint(k, xval, cas.result(k).rtime.value());
          gr_rtime->SetPointError(k, 0, cas.result(k).rtime.uncertainty());
        }
        if(!pa) {
          for(auto& ap : cas.result(k).aux_param) {
            if(aux_param_plots.find(ap.first) != aux_param_plots.end()) {
              TGraphErrors* agr = aux_param_plots[ap.first];
              agr->SetPoint(k, xval, ap.second.value());
              agr->SetPointError(k, 0, ap.second.uncertainty());
            }
            else {
              std::cout<<"plot_pseudo_experiment_results() ERROR graph not found for aux "<<ap.first<<std::endl;
            }
          }
        }
        if(expr.use_histograms() && plot_details) {
          /*sfi::plot_histograms({cas.statistics(ne, k).Nhi, cas.statistics(ne, k).N_ehi, cas.statistics(ne, k).Nphi});
          for(unsigned p=0; p<cas.nparams(); ++p) {
            sfi::plot_histograms({cas.statistics(ne, k).c1hi[p], cas.statistics(ne, k).c1_reldiffhi[p], cas.statistics(ne, k).c1_ehi[p], cas.statistics(ne, k).c1phi[p]});
          }*/
          /* QQQ: if(cas.data_transform) {
              sfi::plot_histograms({cas.statistics(ne, k).Ndatahi});
            }*/
          //if(expr.time_statistics()) sfi::plot_histograms({cas.statistics(ne, k).rtime_hi});
        }
      }
      gr_param_v.push_back(gr_param);
      gr_param_rel_v.push_back(gr_param_rel);
      gr_param_e_v.push_back(gr_param_e);
      gr_param_pull_v.push_back(gr_param_pull);

      if(expr.time_statistics()) gr_rtime_v.push_back(gr_rtime);
      for(auto& ap : aux_param_plots) {
        TGraphErrors* agr = ap.second;
        auto* tc = create_canvas("TPE "+cas.title()+" "+ap.first+" ");
        draw_graph_errors(tc, agr, cas.title(), "AP", cas.marker_color(), cas.marker_style(), 0, 0, 0);
      }
    }
    const double dx = is_const ? 0.4 : std::abs((expr.nparam_values() > 1) ? (expr.param_value(1)[pa] - expr.param_value(0)[pa])/double(expr.ncases()) : 0);
    sfi::draw_case_graph("TPE mean '"+expr.param_name(pa)+"'", mcolors, mstyles, dx, gr_param_v, case_names, "mean. param.", expr.file_name_prefix(), false, 0.5, true);
    sfi::draw_case_graph("TPE unc. '"+expr.param_name(pa)+"'", mcolors, mstyles, dx, gr_param_e_v, case_names, "unc. param.", expr.file_name_prefix(), false, 0.5, true);
    sfi::draw_case_graph("TPE pull '"+expr.param_name(pa)+"'", mcolors, mstyles, dx, gr_param_pull_v, case_names, "pull param.", expr.file_name_prefix(), false, 0.5, true);

    if(expr.time_statistics())  {
      sfi::draw_case_graph("TPE rtime", mcolors, mstyles, dx, gr_rtime_v, case_names, "time [s]", expr.file_name_prefix());
    }
  }
  auto& errcan = Canvas::get("TPE errors");
  auto& outcan = Canvas::get("TPE outliers");
  for(unsigned nc=0; nc<expr.ncases(); ++nc) {
    auto& cas = expr.test_case(nc);
    Vector<double> yv_err(npullpts), yv_out(npullpts), xv(npullpts);
    for(unsigned k=0; k<npullpts; ++k) {
      auto& stat = cas.statistics(k);
      yv_err[k] = stat.nerrors;
      yv_out[k] = stat.noutliers;
      xv[k] = k;
    }
    errcan.draw(xv,yv_err).marker(cas.marker_color(), cas.marker_style());
    outcan.draw(xv,yv_out).marker(cas.marker_color(), cas.marker_style());
  }
  errcan.plot();
  outcan.plot();
}
