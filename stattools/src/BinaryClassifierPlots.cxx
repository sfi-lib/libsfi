/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "BinaryClassifierPlots.h"

#include "ext_utils.h"

using namespace sfi;

#include <TH1D.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TGraph.h>

BinaryClassifierPlotsBase::BinaryClassifierPlotsBase(BinaryClassifierBase& cls, const std::string& title, bool details):
    m_log(Log::get(cls.analysis().main_context(), "BinaryClassifierPlotsBase")),
    m_classifier(cls), m_title(title), m_file_prefix(""), m_details(details),
    m_plot_ext_coordinates(true), m_save_plots(false), m_n_pdf_bins(100),
    m_roc_canvases(2,0), m_roc_legends(2,0) {
}

BinaryClassifierPlotsBase::BinaryClassifierPlotsBase(BinaryClassifierBase& cls, const OptionMap& opts):BinaryClassifierPlotsBase(cls,"",false) {
  if(opts.has("print_level")) m_log.print_level(opts.get<std::string>("print_level"));
  opts.get_if("title", m_title).get_if("details", m_details).get_if("external_coordinates", m_plot_ext_coordinates).get_if("npdfbins", m_n_pdf_bins);
  opts.get_if("save_plots", m_save_plots).get_if("file_prefix", m_file_prefix);
}

void
BinaryClassifierPlotsBase::plot_S(const std::string& suffix, RegID reg_id) {
  create_canvas(m_title+" S "+suffix, 500, 500);
  auto* S_sig_hi = create_histogram("S_sig"+suffix, m_classifier.histogram_S(SID{SplID::ssig, reg_id}));
  auto* S_bg_hi = create_histogram("S_bg"+suffix, m_classifier.histogram_S(SID{SplID::sbg, reg_id}));
  S_sig_hi->SetLineColor(kBlack);
  S_sig_hi->Draw();
  S_bg_hi->SetLineColor(kRed);
  S_bg_hi->Draw("SAME");
}

void
BinaryClassifierPlotsBase::plot_ROC(const std::string& suffix, RegID reg_id) {
  m_roc_canvases[reg_id] = create_canvas(m_title+" ROC "+suffix, 500, 500);
  TGraph* roc = create_graph(m_classifier.roc_tp(reg_id), m_classifier.roc_rej(reg_id), "");
  roc->SetLineWidth(4);
  roc->Draw("APL");
  roc->GetXaxis()->SetTitle("true pos.");
  roc->GetYaxis()->SetTitle("1 - false pos.");
  do_log_info("plot_ROC() "<<suffix<<" ROC "<<roc->Integral());
  m_roc_legends[reg_id] = new TLegend(0.22,0.20, 0.48,0.55);
  m_roc_legends[reg_id]->AddEntry(roc, "SFI");
}

void
BinaryClassifierPlotsBase::plot_p_or_lnp(bool is_p, bool integrate, const std::string& suffix, PDFID pdf_id) {
  std::string idx = (is_p ? "_p" : "_lnp")+std::string(integrate ? "_integrated_":"_");
  std::string xt = std::string(integrate ? "#int " : "")+ (is_p ? "p" : "-2ln(p)");
  auto* can = create_canvas(m_title+idx+suffix, 500, 500);
  TLegend* legend = new TLegend(0.68, 0.42, 0.95, 0.75);
  legend->SetTextSize(0.04);
  std::pair<std::string, PID> r_pids[] = {
      {"ssig in train", PID{pdf_id, SplID::ssig, RegID::train}},
      {"ssig in test",  PID{pdf_id, SplID::ssig, RegID::test}},
      {"sbg in train",  PID{pdf_id, SplID::sbg,  RegID::train}},
      {"sbg in test",   PID{pdf_id, SplID::sbg,  RegID::test}}
  };
  unsigned i=0;
  std::vector<TH1D*> his;
  for(auto& pidv : r_pids) {
    const HistTy& rhi = is_p ? m_classifier.histogram_p(pidv.second) : m_classifier.histogram_lnp(pidv.second);
    TH1D * hi(0);
    if(integrate) hi = create_histogram(pidv.first+idx+" "+suffix, rhi.integrate());
    else hi = create_histogram(pidv.first+idx+" "+suffix, rhi);

    hi->SetLineColor(kBlack+i);
    hi->SetLineWidth(3.0);
    hi->GetXaxis()->SetTitle(xt.c_str());
    if(integrate) {
      can->SetBottomMargin(0.2);
      hi->GetXaxis()->SetTitleOffset(1.6);
    }
    if(!i) hi->Draw();
    else hi->Draw("SAME");
    ++i;
    his.push_back(hi);
    legend->AddEntry(hi, pidv.first.c_str());
  }
  legend->Draw();
  adjust_range(can, his);
  can->Update();
  m_canvases.insert(m_canvases.end(), can);
}

void
BinaryClassifierPlotsBase::plot_p(const std::string& suffix, PDFID pdf_id) {
  plot_p_or_lnp(true, false, suffix, pdf_id);
  plot_p_or_lnp(true, true, suffix, pdf_id);
}

void
BinaryClassifierPlotsBase::plot_lnp(const std::string& suffix, PDFID pdf_id) {
  plot_p_or_lnp(false, false, suffix, pdf_id);
  plot_p_or_lnp(false, true, suffix, pdf_id);
}

void
BinaryClassifierPlotsBase::make_plots() {
  std::pair<std::string,RegID> regs[] = {{"training", RegID::train}, {"test", RegID::test}};
  for(auto& r : regs) {
    plot_S(r.first, r.second);
    plot_ROC(r.first, r.second);
  }
  std::pair<std::string,PDFID> pdfs[] = {{"ps", PDFID::psig}, {"pb", PDFID::pbg}};
  for(auto& p : pdfs) {
    if(m_classifier.make_p_histograms()) plot_p(p.first, p.second);
    if(m_classifier.make_lnp_histograms()) plot_lnp(p.first, p.second);
  }
  if(m_save_plots && !m_file_prefix.empty()) save_plots();
}

void
BinaryClassifierPlotsBase::save_plots() {
  if(m_file_prefix.empty()) {
    do_log_error("save_plots() No file prefix given!");
    return;
  }
  std::pair<std::string,RegID> regs[] = {{"training", RegID::train}, {"test", RegID::test}};
  for(auto& r : regs) {
    TCanvas* can = m_roc_canvases[r.second];
    std::string fname = m_file_prefix+m_title+"_roc_"+r.first+".pdf";
    can->Print(fname.c_str());
  }
  for(auto* can : m_canvases) {
    std::string fname = m_file_prefix+can->GetName()+".pdf";
    can->Print(fname.c_str());
  }
}
