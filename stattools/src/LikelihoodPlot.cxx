/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "LikelihoodPlot.h"
#include "TransformPlot.h"
#include "ext_utils.h"
#include "IPDF.h"
#include "ISample.h"

#include <TGraph.h>
#include <TGraph2D.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TH1D.h>

using namespace sfi;

LikelihoodPlot::LikelihoodPlot(Context& ctx, ILikelihoodMaximizer& pe, const std::initializer_list<double>& par):m_context(ctx), m_pe(pe), m_par(par), m_work_values_set(false) {
  init_par();
}

LikelihoodPlot::LikelihoodPlot(Context& ctx, ILikelihoodMaximizer& pe, const std::vector<double>& par):m_context(ctx), m_pe(pe), m_par(par), m_work_values_set(false) {
  init_par();
}

LikelihoodPlot::~LikelihoodPlot() {
  if(this->m_work_values_set) {
    m_pe.get_parameters().clear_working_values();
  }
}

bool LikelihoodPlot::draw(const std::string& prefix, unsigned par, unsigned npts, double pmin, double pmax) {
  auto& pars = m_pe.get_parameters();
  auto& p1 = pars[par];
  create_canvas(prefix+" "+p1.name());
  plot(par, npts, pmin, pmax)->Draw("ALP");
  return true;
}

TGraph* LikelihoodPlot::plot(unsigned par, unsigned npts, double pmin, double pmax) {
  auto& pars = m_pe.get_parameters();
  auto& param = pars[par];
  if(m_par.size() != pars.size()) {
    std::cout<<"LikelihoodPlot Error: not all parameters were initialized!"<<std::endl;
    m_par.resize(pars.size(), 0.0);
  }
  for(unsigned i=0; i<pars.size(); ++i) pars[i].work_value(m_par[i]);
  double pval = pmin;
  const double pstep((pmax - pmin)/double(npts));
  TGraph* res = new TGraph(npts);
  res->SetMarkerSize(2.0);
  double val;
  for(unsigned i=0; i<npts; ++i) {
    param.work_value(pval);
    val = m_pe.do_eval();
    if(val == val) {
      res->SetPoint(i, pval, val);
    }
    else res->SetPoint(i, pval, 0);
    pval += pstep;
  }
  return res;
}

bool LikelihoodPlot::draw2D(const std::string& prefix, unsigned par1, unsigned npts1, double pmin1, double pmax1, unsigned par2, unsigned npts2, double pmin2, double pmax2) {
  auto& pars = m_pe.get_parameters();
  auto& p1 = pars[par1];
  auto& p2 = pars[par2];
  create_canvas(prefix+" "+p1.name()+" vs. "+p2.name());
  plot2D(par1, npts1, pmin1, pmax1, par2, npts2, pmin2, pmax2)->Draw("SURF");
  return true;
}

TGraph2D* LikelihoodPlot::plot2D(unsigned par1, unsigned npts1, double pmin1, double pmax1, unsigned par2, unsigned npts2, double pmin2, double pmax2) {
  auto& pars = m_pe.get_parameters();
  auto& param1 = pars[par1];
  auto& param2 = pars[par2];

  if(m_par.size() != pars.size()) {
    std::cout<<"LikelihoodPlot Error: not all parameters were initialized!"<<std::endl;
    m_par.resize(pars.size(), 0.0);
  }
  for(unsigned i=0; i<pars.size(); ++i) pars[i].work_value(m_par[i]);
  const unsigned npts = npts1*npts2;
  //std::vector<double> pvals(m_par);
  //double& pval1 = pvals[par1];
  //double& pval2 = pvals[par2];
  double pval1(pmin1), pval2(pmin2);

  const double pstep1((pmax1 - pmin1)/double(npts1 - 1));
  const double pstep2((pmax2 - pmin2)/double(npts2 - 1));

  TGraph2D* res = new TGraph2D(npts);
  res->SetMarkerSize(2.0);
  unsigned n(0);
  for(unsigned i=0; i<npts1; ++i) {
    pval2 = pmin2;
    for(unsigned j=0; j<npts2; ++j) {
      // avoid overflow
      pval1 = std::min(pval1, pmax1);
      pval2 = std::min(pval2, pmax2);
      param1.work_value(pval1);
      param2.work_value(pval2);
      double L = m_pe.do_eval();
      res->SetPoint(n, pval1, pval2, L);
      pval2 += pstep2;
      ++n;
    }
    pval1 += pstep1;
  }
  return res;
}

TGraph* LikelihoodPlot::plot_profile(unsigned par, const std::string& name, unsigned nppts, double nsigmas, bool do_draw) {
  Vector<double> xv(nppts), fv(nppts);
  // auto os = m_pe.minimizer().strategy();
  // m_pe.minimizer().strategy(Minuit::Speed);
  TGraph *g = 0;
  if(m_pe.get_profile(par, nppts, xv, fv, nsigmas)) {
    g = draw_graph(xv, fv, name, do_draw);
    if(do_draw) {
      auto& pars = m_pe.get_parameters();
      // auto& mini = m_pe.minimizer();
      auto mv = m_pe.get_minval();
      draw_horizontal_line(0, mv, kGray);
      draw_horizontal_line(0, mv + 1.0, kGray);
      auto& p = pars[par];
      draw_vertical_line(0, p.value(), kGray);
      if(m_pe.get_has_asymetrical_uncertainties()) {
        draw_vertical_line(0, p.value() + p.uncertainty_down(), kGray);
        draw_vertical_line(0, p.value() + p.uncertainty_up(), kGray);
      }
      else {
        draw_vertical_line(0, p.value() - p.uncertainty(), kGray);
        draw_vertical_line(0, p.value() + p.uncertainty(), kGray);
      }
      gPad->Update();
    }
  }
  // m_pe.minimizer().strategy(os);
  return g;
}

void
LikelihoodPlot::data_pdf_comparison(const OptionMap& opts, const ISample& spl) {
  int nbins = opts.get<int>("nbins");
  std::string evalt = opts.get<std::string>("eval");
  EvalPoint ep = EvalML;
  if(evalt == "initial") ep = EvalInitial;
  else if(evalt == "current") ep = EvalCurrent;
  else if(evalt == "ml") ep = EvalML;

  std::vector<IPDF*> pdfs;
  m_pe.get_pdfs(pdfs);
  TransformPlot1D plot(m_context, opts);
  plot.external_coordinates(true).superimpose(false);

  std::vector<Option*> trfs;
  opts.get_if("transforms", trfs);
  for(auto* trfo : trfs) {
    auto* trfopts = dynamic_cast<OptionMap*>(trfo);
    for(auto* pdf : pdfs) {
      if(pdf->get_transform()) {
        if(starts_with(pdf->get_transform()->get_name(), trfopts->get<std::string>("name"))) {
          pdf->do_update_parameters(ep);
          // TODO: Leaking evaluator
          plot.subevals().push_back(TransformPlot1D(m_context, pdf->get_evaluator()->get_clone(false), *trfopts).scale(pdf->get_N(ep,"",false)));
        }
      }
    }
  }
  std::string title = opts.get<std::string>("title");
  auto* can = create_canvas(title);
  plot.plot();
  auto* hi = draw_histogram(title+"_data", spl.get_histogram_1D(0,nbins,true,AllEventSelID), "SAME", 1);
  hi->SetMarkerStyle(20);
  can->Update();
}

TGraph*
LikelihoodPlot::plot_contour(unsigned par1, unsigned par2, unsigned npts) {
  Vector<double> ptx(npts), pty(npts);
  // m_pe.minimizer().print_level(PrintLevel::Debug);
  m_pe.get_contour(par1, par2, npts, ptx, pty);
  auto& pars = m_pe.get_parameters();
  std::string title = "Contour "+pars[par1].name()+" "+pars[par2].name();
  auto *gr = create_graph(ptx, pty, title);
  auto * can = create_canvas(title);
  gr->Draw("ALP");
  can->Update();
  gr->GetXaxis()->SetTitle(pars[par1].name().c_str());
  gr->GetYaxis()->SetTitle(pars[par2].name().c_str());
  can->Update();
  return gr;
}

void LikelihoodPlot::init_par() {
  auto &pars = m_pe.get_parameters();
  
  if(m_par.empty()) {
    m_par.resize(pars.size());
    for (unsigned i = 0; i < pars.size(); ++i) m_par[i] = pars[i].initial();
  }
  if(!pars[0].has_work_value()) {
    pars.set_working_values(&m_par[0]);
    m_work_values_set = true;
  }
}
