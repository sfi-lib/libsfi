/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "BinaryClassifierBase.h"

#include "OvertrainingTest.h"
#include "timer.h"
#include "stat.h"

using namespace sfi;

constexpr BinaryClassifierBase::PID BinaryClassifierBase::pids[];
constexpr BinaryClassifierBase::SID BinaryClassifierBase::sids[];

constexpr BinaryClassifierBase::SID BinaryClassifierBase::ssig_in_train;
constexpr BinaryClassifierBase::SID BinaryClassifierBase::ssig_in_test;
constexpr BinaryClassifierBase::SID BinaryClassifierBase::sbg_in_train;
constexpr BinaryClassifierBase::SID BinaryClassifierBase::sbg_in_test;

BinaryClassifierBase&
BinaryClassifierBase::bg_efficiency_points(const std::vector<double>& bge) {
  m_bg_efficiency_points = bge;
  return *this;
}

void
BinaryClassifierBase::compute() {
  compute_init();

  Timer<true> timer;
  timer.start();
  eval_all_pdfs();
  m_evaluation_time = timer.stop();
  do_log_info("compute() evaluated PDF:s in "<<m_evaluation_time<<"s");

  timer.start();
  for(auto sid : sids) fill_S_histogram(sid);
  do_log_info("compute() filled S histograms in "<<timer.stop()<<" s");

  // check S histograms
  for(auto sid : sids) {
    auto& spl = get_sample(sid.spl_id);
    HistTy& hi = histogram_S(sid);
    auto& esel = get_event_selector(sid.reg_id);
    unsigned nev = spl.get_events()*esel.get_fraction();
    if(nev != hi.sum_weights(false)) {
      do_log_warning("compute() Sample '"<<spl.get_name()<<"' has "<<nev<<" events in region '"<<str_id(sid.reg_id)<<"', histogram has "<<hi.sum_weights(false)<<" entries");
    }
  }

  // make some margins
  m_pmin *= 0.99; m_pmax *= 1.01;
  do_log_info("compute() pmin="<<m_pmin<<" pmax="<<m_pmax);

  timer.start();
  if(m_make_p_histograms) {
    m_hi_p.resize(8, HistTy(m_n_p_bins, m_pmin, m_pmax));
    m_sum_p.resize(8, 0.0);
    for(auto pid : pids) fill_p_histogram(pid);
  }
  if(m_make_lnp_histograms) {
    m_hi_lnp.resize(8, HistTy(m_n_lnp_bins, -2.0*ln(m_pmax), -2.0*ln(m_pmin)));
    m_sum_lnp.resize(8, 0.0);
    for(auto pid : pids) fill_lnp_histogram(pid);
  }
  do_log_info("compute() filled p/lnp histograms in "<<timer.stop()<<" s");

  timer.start();
  compute_roc();
  do_log_info("compute() ROC computed in "<<timer.stop()<<" s");

  // print statistics
  const PID pids[] = {
      PID{PDFID::psig,SplID::ssig,RegID::test}, PID{PDFID::psig,SplID::sbg,RegID::test},
      PID{PDFID::pbg,SplID::ssig,RegID::test}, PID{PDFID::pbg,SplID::sbg,RegID::test}};
  if(m_make_p_histograms) {
    for(auto& pid : pids) {
      PID pid_tr = PID{pid.pdf_id, pid.spl_id, RegID::train};
      auto ks = kolmogorov_smirnov_test(histogram_p(pid), histogram_p(pid_tr));
      double diff = fabs(sum_p(pid) - sum_p(pid_tr))/sum_p(pid);
      do_log_info("compute() sum p "<<str_id(pid.pdf_id)<<" in "<<str_id(pid.spl_id)<<" : "<<sum_p(pid)<<" "<<sum_p(pid_tr)<<" diff="<<diff<<" KS score: "<<ks.first<<" prob: "<<ks.second);
    }
  }
  if(m_make_lnp_histograms) {
    for(auto& pid : pids) {
      PID pid_tr = PID{pid.pdf_id, pid.spl_id, RegID::train};
      auto ks = kolmogorov_smirnov_test(histogram_lnp(pid), histogram_lnp(pid_tr));
      double diff = fabs(sum_lnp(pid) - sum_lnp(pid_tr))/sum_lnp(pid);
      do_log_info("compute() sum lnp "<<str_id(pid.pdf_id)<<" in "<<str_id(pid.spl_id)<<" : "<<sum_lnp(pid)<<" "<<sum_lnp(pid_tr)<<" diff="<<diff<<" KS score: "<<ks.first<<" prob: "<<ks.second);
    }
  }
  for(size_t w=0; w<m_bg_efficiency_points.size(); ++w) {
    do_log_info("compute() training efficiency sig="<<m_sig_efficiencies[RegID::train][w]<<" at bg="<<m_bg_efficiency_points[w]<<" (diff "<<m_bg_efficiency_diff[RegID::train][w]<<")");
  }
  do_log_info("compute() training AUC "<<m_roc_auc[RegID::train]);
  for(size_t w=0; w<m_bg_efficiency_points.size(); ++w) {
    do_log_info("compute() test efficiency sig="<<m_sig_efficiencies[RegID::test][w]<<" at bg="<<m_bg_efficiency_points[w]<<" (diff "<<m_bg_efficiency_diff[RegID::test][w]<<")");
  }
  do_log_info("compute() test AUC "<<m_roc_auc[RegID::test]);


  m_summaryTable.caption("The area under curve numbers for different classifier methods");
  m_summaryTable.precision(2).col_title(0, "Training time (s)").col_title(1, "Test time (s)").col_title(2, "AUC Test");
  m_summaryTable.row_title(0, "SFI");
  m_summaryTable(0,0) = m_training_time;
  m_summaryTable(0,1) = m_evaluation_time;
  m_summaryTable(0,2) = m_roc_auc[RegID::test];

  m_efficiencyTable = LatexTable("efficiency", m_bg_efficiency_points.size()*2, 1);
  m_efficiencyTable.caption("Signal efficiency for different background efficiencies").precision(2);
  for(size_t w=0; w<m_bg_efficiency_points.size(); ++w) {
    m_efficiencyTable.col_title(w*2, "Test @"+std::to_string(m_bg_efficiency_points[w]));
    m_efficiencyTable.col_title(w*2 + 1, "Train @"+std::to_string(m_bg_efficiency_points[w]));
    m_efficiencyTable(0, w*2) = m_sig_efficiencies[RegID::test][w];
    m_efficiencyTable(0, w*2 + 1) = m_sig_efficiencies[RegID::train][w];
  }
  m_efficiencyTable.row_title(0, "SFI");
}

double
BinaryClassifierBase::compute_S(const double* p) {
  double ps(p[PDFID::psig]), pb(p[PDFID::pbg]);
  m_pmin = std::min(m_pmin, ps);
  m_pmin = std::min(m_pmin, pb);
  m_pmax = std::max(m_pmax, ps);
  m_pmax = std::max(m_pmax, pb);
  return ps/(ps + pb);
}

void
BinaryClassifierBase::compute_roc() {
  m_roc_tp.resize(2, std::vector<double>(m_n_roc_bins + 2, 0.0));
  m_roc_rej.resize(2, std::vector<double>(m_n_roc_bins + 2, 0.0));

  const size_t N_wps = m_bg_efficiency_points.size();
  m_sig_efficiencies.resize(2, std::vector<double>(N_wps, 0));
  m_bg_efficiency_diff.resize(2, std::vector<double>(N_wps, 1e10));

  m_roc_auc.resize(2, 0.0);
  compute_roc(RegID::train);
  compute_roc(RegID::test);
}

void
BinaryClassifierBase::compute_roc(RegID reg_id) {
  auto& bg_hi = histogram_S(SID{SplID::sbg, reg_id});
  auto& sig_hi = histogram_S(SID{SplID::ssig, reg_id});

  auto& _roc_tp = roc_tp(reg_id);
  auto& _roc_rej = roc_rej(reg_id);

  const double bt = bg_hi.integral(1, m_n_roc_bins);
  const double st = sig_hi.integral(1, m_n_roc_bins);

  const size_t N_wps = m_bg_efficiency_points.size();
  double accs(st), accb(bt), fp(0), tp(0);
  for(size_t i=1; i<= m_n_roc_bins; ++i) {
    tp = accs/st;
    fp = accb/bt;
    _roc_tp[i-1] = tp;
    _roc_rej[i-1] = 1.0 - fp;
    accs -= sig_hi.bin_content(i);
    accb -= bg_hi.bin_content(i);
    for(unsigned w=0; w<N_wps; ++w) {
      double diff = std::abs(m_bg_efficiency_points[w] - fp);
      if(diff < m_bg_efficiency_diff[reg_id][w]) {
        m_sig_efficiencies[reg_id][w] = tp;
        m_bg_efficiency_diff[reg_id][w] = diff;
      }
    }
  }
  _roc_tp[m_n_roc_bins] = 0;
  _roc_rej[m_n_roc_bins] = 1.0;
  _roc_tp[m_n_roc_bins + 1] = 0;
  _roc_rej[m_n_roc_bins + 1] = 0.0;

  m_roc_auc[reg_id] = 0.0;
  for(size_t i=0; i<= m_n_roc_bins; ++i) {
    double dtp = fabs(_roc_tp[i] - _roc_tp[i+1]);
    m_roc_auc[reg_id] += dtp*0.5*(_roc_rej[i] + _roc_rej[i+1]);
  }
}

BinaryClassifierBase::BinaryClassifierBase(Analysis& ana):m_log(Log::get(ana.main_context(), "BinaryClassifier")), m_ana(ana),
    m_evaluation_time(0.0),
    m_training_time(0.0),
    m_n_roc_bins(100),
    m_n_p_bins(400),
    m_n_lnp_bins(400),
    m_bg_efficiency_points({0.01, 0.1, 0.3}),
    m_pmin(1e10), m_pmax(0),
    m_make_p_histograms(true),
    m_make_lnp_histograms(true),
    m_summaryTable("Summary", 3,1),
    m_efficiencyTable("Efficiencies",6,1) {
}
