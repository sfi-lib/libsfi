#ifndef sfi_BinaryClassifier_h
#define sfi_BinaryClassifier_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "BinaryClassifierBase.h"
#include "transform_functions.h"

namespace sfi {
  /**
   * @class sfi::BinaryClassifier
   *
   * Class template for binary classification.
   * Usage:
   *
   * auto classifier = make_binary_classifier(<sig trf>, <bg trf>, SampleSet&, <train event selecor>, <test event selector>)
   * classifier.compute();
   * Make sure the signal sample is first in the sample set.
   *
   * Plot results using BinaryClassifierPlot.
   */
  template<class _TransfSig, class _TransfBg, typename _S2, class _TrainEvSel, class _TestEvSel>
  class BinaryClassifier : public BinaryClassifierBase {
    static_assert(_TransfBg::dims() == _TransfSig::dims(), "Must have the same dimensionality");
    static constexpr size_t Dims = _TransfBg::dims();
  public:
    using This = BinaryClassifier<_TransfSig, _TransfBg, _S2, _TrainEvSel, _TestEvSel>;
    using Base = BinaryClassifierBase;

    BinaryClassifier(Analysis& ana, const _TransfSig& tsig, const _TransfBg& tbg,
        const SampleSet<Dims, _S2>& samples, const _TrainEvSel& train_esel, const _TestEvSel& test_esel):
          BinaryClassifierBase(ana), m_trf_bg(tbg), m_trf_sig(tsig), m_samples(samples), m_train_esel(train_esel), m_test_esel(test_esel) {
    }

    BinaryClassifier(BinaryClassifier&&) = default;

    ~BinaryClassifier() { }

    /**
     * Make 1D comparison PDF:s using the provided Transformation
     */
    template<class _Transf>
    This& make_1D_transforms(const _Transf& trfo, const SampleSet<Dims, _S2>& spls) {
      m_1D_transforms_train = perform_single_transformations(m_ana.main_context(), spls, trfo, m_train_esel);
      m_1D_transforms_test = perform_single_transformations(m_ana.main_context(), spls, trfo, m_test_esel);
      return *this;
    }

    inline const SampleSet<Dims, _S2>& samples() const { return m_samples; }

    inline const Sample<Dims, _S2>& sample(SplID spl_id) const { return m_samples[spl_id]; }

    inline const _TransfBg& background_transform() const { return m_trf_bg; }

    inline const _TransfSig& signal_transform() const { return m_trf_sig; }

    inline const _TrainEvSel& event_selector_training() const { return m_train_esel; }

    inline const _TestEvSel& event_selector_test() const { return m_test_esel; }

    inline TransformSet<ITransform> generic_transforms() {
      return TransformSet<ITransform>(std::vector<ITransform*>{(ITransform*)&m_trf_sig,(ITransform*)&m_trf_bg}, false);
    }
  protected:

    virtual void compute_init() {
      do_log_info("compute_init() signal sample: '"<<m_samples[SplID::ssig].name()<<"' with "<<m_samples[SplID::ssig].size()<<" events");
      do_log_info("compute_init() background sample: '"<<m_samples[SplID::sbg].name()<<"' with "<<m_samples[SplID::sbg].size()<<" events");
      do_log_info("compute_init() signal transform: '"<<m_trf_sig.name()<<"'");
      do_log_info("compute_init() background transform: '"<<m_trf_bg.name()<<"'");

      m_hi_S.resize(4, HistTy(m_n_roc_bins, 0, 1.0));
      m_pmin = 1e10;
      m_pmax = 0.0;

      auto* mp = m_ana.memory_pool().template pool<PSampleTy>();
      for(auto& s : m_samples) m_psamples.add(mp->get(s.size(), "p_"+s.name()));
    }

    virtual void eval_all_pdfs() {
      bool is_normalized = m_samples[0].is_normalized();
      Evaluator<_TransfBg> bgev(&m_trf_bg, 1.0, !is_normalized);
      Evaluator<_TransfSig> sigev(&m_trf_sig, 1.0, !is_normalized);
      for(size_t i=0; i<m_samples.size(); ++i) {
        if(is_normalized != m_samples[i].is_normalized()) {
          do_log_error("eval_all_pdfs() sample '"<<m_samples[i].name()<<"' normalized="<<m_samples[i].is_normalized()<<" expected "<<is_normalized);
        }
        bgev.eval_all(m_samples[i], m_psamples[i], PDFID::pbg);
        sigev.eval_all(m_samples[i], m_psamples[i], PDFID::psig);
      }
    }

    template<class _Func>
    inline void apply(SplID splid, RegID regid, const _Func& func) {
      if(regid == RegID::train) m_psamples[splid].apply(m_train_esel, func);
      else m_psamples[splid].apply(m_test_esel, func);
    }

    virtual void fill_S_histogram(SID sid) {
      auto& hi = histogram_S(sid);
      auto sfunc = [&hi,this](const double* p, double) { hi.fill(this->compute_S(p)); };
      apply(sid.spl_id, sid.reg_id, sfunc);
    }

    virtual void fill_p_histogram(PID pid) {
      auto& hi = histogram_p(pid);
      double& _sum_p = sum_p(pid);
      _sum_p = 0.0;
      auto sfunc = [&hi,pid,&_sum_p](const double* p, double) {
        double _p = p[pid.pdf_id];
        _sum_p += _p;
        hi.fill(_p);
      };
      apply(pid.spl_id, pid.reg_id, sfunc);
    }

    virtual void fill_lnp_histogram(PID pid) {
      auto& hi = histogram_lnp(pid);
      double& _sum_lnp = sum_lnp(pid);
      _sum_lnp = 0.0;
      auto sfunc = [&hi,pid,&_sum_lnp](const double* p, double) {
        double lnp = -2.0*ln(p[pid.pdf_id]);
        _sum_lnp += lnp;
        hi.fill(lnp);
      };
      apply(pid.spl_id, pid.reg_id, sfunc);
    }

    virtual const ISample& get_sample(SplID sid) { return sample(sid); }

    virtual const IEventSelector& get_event_selector(RegID rid) {
      return (rid == RegID::test) ? static_cast<const IEventSelector&>(m_test_esel) : static_cast<const IEventSelector&>(m_train_esel);
    }

    const _TransfBg& m_trf_bg;
    const _TransfSig& m_trf_sig;
    const SampleSet<Dims, _S2>& m_samples;
    _TrainEvSel m_train_esel;
    _TestEvSel m_test_esel;
  };

  template<class _TransfSig, class _TransfBg,typename _S2, class _TrainEvSel, class _TestEvSel>
  BinaryClassifier<_TransfSig, _TransfBg, _S2, _TrainEvSel, _TestEvSel>
  make_binary_classifier(Analysis& ana, const _TransfSig& tsig, const _TransfBg& tbg,
      const SampleSet<_TransfBg::dims(), _S2>& samples, const _TrainEvSel& train_esel, const _TestEvSel& test_esel) {
    return BinaryClassifier<_TransfSig, _TransfBg, _S2, _TrainEvSel, _TestEvSel>(ana, tsig, tbg, samples, train_esel, test_esel);
  }
}

#endif // sfi_BinaryClassifier_h
