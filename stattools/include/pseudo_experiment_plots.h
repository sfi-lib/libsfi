#ifndef sfi_pseudo_experiment_plots_h
#define sfi_pseudo_experiment_plots_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "PseudoExperimentBase.h"

namespace sfi {
  // TODO: Make class
  void plot_pseudo_experiment_results(PseudoExperimentBase& expr, bool plot_details, bool plot_covariance = false);
}

#endif // sfi_pseudo_experiment_plots_h
