#ifndef sfi_BinaryClassifierBase_h
#define sfi_BinaryClassifierBase_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Histogram.h"
#include "Sample.h"
#include "Analysis.h"
#include "LatexTable.h"
#include "ITransform.h"

#include <vector>

namespace sfi {
  /**
   * @class sfi::BinaryClassifierBase
   *
   * Base class for the BinaryClassifier template.
   * All members and methods that do not depend on template parameters.
   */
  class BinaryClassifierBase {
  public:
    using This = BinaryClassifierBase;
    using HistTy = Histogram<false, double>;

    enum SplID { ssig = 0, sbg = 1 };
    enum PDFID { psig = 0, pbg = 1 };
    enum RegID { train = 0, test = 1 };
    struct SID { SplID spl_id; RegID reg_id;
    inline operator size_t() const { return spl_id<<1 | reg_id; }
    };
    struct PID { PDFID pdf_id; SplID spl_id; RegID reg_id;
    inline operator size_t() const { return pdf_id<<2 | spl_id<<1 | reg_id; }
    };

    static constexpr SID ssig_in_train{SplID::ssig, RegID::train};
    static constexpr SID ssig_in_test{SplID::ssig, RegID::test};
    static constexpr SID sbg_in_train{SplID::sbg, RegID::train};
    static constexpr SID sbg_in_test{SplID::sbg, RegID::test};

    static constexpr PID psig_for_ssig_in_train{PDFID::psig, SplID::ssig, RegID::train};
    static constexpr PID psig_for_sbg_in_train{PDFID::psig, SplID::sbg, RegID::train};
    static constexpr PID psig_for_ssig_in_test{PDFID::psig, SplID::ssig, RegID::test};
    static constexpr PID psig_for_sbg_in_test{PDFID::psig, SplID::sbg, RegID::test};
    static constexpr PID pbg_for_ssig_in_train{PDFID::pbg, SplID::ssig, RegID::train};
    static constexpr PID pbg_for_sbg_in_train{PDFID::pbg, SplID::sbg, RegID::train};
    static constexpr PID pbg_for_ssig_in_test{PDFID::pbg, SplID::ssig, RegID::test};
    static constexpr PID pbg_for_sbg_in_test{PDFID::pbg, SplID::sbg, RegID::test};

    static constexpr PID pids[] = {psig_for_ssig_in_train, psig_for_sbg_in_train, psig_for_ssig_in_test, psig_for_sbg_in_test,
        pbg_for_ssig_in_train, pbg_for_sbg_in_train, pbg_for_ssig_in_test, pbg_for_sbg_in_test};

    static constexpr SID sids[] = {ssig_in_train, ssig_in_test, sbg_in_train, sbg_in_test};

    static std::string str_id(SplID spl_id) {
      return (spl_id == SplID::ssig) ? "SplSig" : "SplBg";
    }

    static std::string str_id(PDFID pdf_id) {
      return (pdf_id == PDFID::psig) ? "TrfSig" : "TrfBg";
    }

    static std::string str_id(RegID reg_id) {
      return (reg_id == RegID::train) ? "training" : "test";
    }

    inline This& training_time(double _ttime) { m_training_time = _ttime; return *this; }

    inline double evaluation_time() const { return m_evaluation_time; }

    inline This& n_roc_bins(size_t nrb) { m_n_roc_bins = nrb; return *this; }

    inline This& n_p_bins(size_t nrb) { m_n_p_bins = nrb; return *this; }

    inline This& n_lnp_bins(size_t nrb) { m_n_lnp_bins = nrb; return *this; }

    inline HistTy& histogram_S(SID sid) { return m_hi_S[sid]; }

    inline bool make_p_histograms() const { return m_make_p_histograms; }

    inline bool make_lnp_histograms() const { return m_make_lnp_histograms; }

    inline HistTy& histogram_p(PID pid) { return m_hi_p[pid]; }

    inline HistTy& histogram_lnp(PID pid) { return m_hi_lnp[pid]; }

    inline double& sum_p(PID pid) { return m_sum_p[pid]; }

    inline double& sum_lnp(PID pid) { return m_sum_lnp[pid]; }

    This& bg_efficiency_points(const std::vector<double>& bge);

    /** @return The number of background efficiency points specified */
    inline size_t n_bg_efficiency_points() { return m_bg_efficiency_points.size(); }

    /** @return Signal efficiency and the specified bg efficiency points */
    inline const std::vector<double>& sig_efficiencies(RegID reg_id) const { return m_sig_efficiencies[reg_id]; }

    /** @return How close the bg efficiency is to the specified bg efficiency points */
    inline const std::vector<double>& bg_efficiency_diff(RegID reg_id) const { return m_bg_efficiency_diff[reg_id]; }

    inline std::vector<double>& roc_tp(RegID reg_id) { return m_roc_tp[reg_id]; }

    inline std::vector<double>& roc_rej(RegID reg_id) { return m_roc_rej[reg_id]; }

    inline LatexTable& summary_table() { return m_summaryTable; }

    inline LatexTable& efficiency_table() { return m_efficiencyTable; }

    inline bool has_1D_transforms() const { return m_1D_transforms_train.size() > 0; }

    inline const TransformSet2D<ITransform>& transforms_1D(RegID reg_id) const {
      return (reg_id == train) ? m_1D_transforms_train : m_1D_transforms_test;
    }

    void compute();
    
    inline Analysis& analysis() const { return m_ana; }
  protected:
    using PSampleTy = Sample<2U, tfalse>;

    double compute_S(const double* p);

    void compute_roc();

    void compute_roc(RegID reg_id);

    BinaryClassifierBase(Analysis& ana);

    BinaryClassifierBase(BinaryClassifierBase&&) = default;

    virtual ~BinaryClassifierBase() {};

    virtual void compute_init()  = 0;

    virtual void eval_all_pdfs() = 0;

    virtual void fill_S_histogram(SID sid) = 0;

    virtual void fill_p_histogram(PID pid) = 0;

    virtual void fill_lnp_histogram(PID pid) = 0;

    virtual const ISample& get_sample(SplID sid) = 0;

    virtual const IEventSelector& get_event_selector(RegID rid) = 0;

    Log m_log;
    Analysis& m_ana;
    SampleSet<2U, tfalse> m_psamples;
    double m_evaluation_time, m_training_time;
    size_t m_n_roc_bins, m_n_p_bins, m_n_lnp_bins;

    std::vector<double> m_bg_efficiency_points;
    std::vector<std::vector<double>> m_sig_efficiencies;
    std::vector<std::vector<double>> m_bg_efficiency_diff;

    std::vector<double> m_roc_auc;

    double m_pmin, m_pmax;
    bool m_make_p_histograms;
    bool m_make_lnp_histograms;
    std::vector<HistTy> m_hi_S;
    std::vector<HistTy> m_hi_p;
    std::vector<HistTy> m_hi_lnp;
    std::vector<double> m_sum_p;
    std::vector<double> m_sum_lnp;
    std::vector<std::vector<double>> m_roc_tp; // true positive
    std::vector<std::vector<double>> m_roc_rej; // 1 - false pos

    LatexTable m_summaryTable;
    LatexTable m_efficiencyTable;

    TransformSet2D<ITransform> m_1D_transforms_train;
    TransformSet2D<ITransform> m_1D_transforms_test;
  };
}

#endif // sfi_BinaryClassifierBase_h
