#ifndef sfi_OvertrainingTest_h
#define sfi_OvertrainingTest_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Histogram.h"
#include "stat.h"
#include "EventSelector.h"

#include <TMath.h>

namespace sfi {
  /** @return the KS score for level alpha, for samples of size N and M */
  inline double kolmogorov_smirnov_score(double alpha, double N, double M) {
    return sqrt(-0.5*ln(0.5*alpha)*(N + M)/(N*M));
  }

  inline double kolmogorov_smirnov_prob(double D, double N, double M) {
    return TMath::KolmogorovProb(D*sqrt((N*M)/(N + M)));
  }

  /**
   * Perform a KS test, return a pair of score and probability
   *
   * Reference:
   * Kolmogorov–Smirnov test, https://en.wikipedia.org/w/index.php?title=Kolmogorov%E2%80%93Smirnov_test&oldid=981961514 (last visited Oct. 13, 2020).
   */
  template<class _Hist>
  inline std::pair<double, double> kolmogorov_smirnov_test(const _Hist& hi_test, const _Hist& hi_train) {
    auto hi_int_test = hi_test.integrate();
    auto hi_int_train = hi_train.integrate();
    double ks_score = hi_int_test.find_max_diff(hi_int_train);
    double ks_prob = kolmogorov_smirnov_prob(ks_score, hi_test.sum_weights(), hi_train.sum_weights());
    return std::make_pair(ks_score, ks_prob);
  }

  /**
   * @class OvertrainingTest<_Evaluator>
   * Fills 1D histograms of -2ln(p) for a number of samples
   * for both train and test. Then computes the K.S. score
   * and probability.
   *
   * Usage:
   * auto test = overtraining_test(evaluator)
   *
   * Compute range for histograms
   * for(auto& s : samples) test.compute_range(s)
   * Fill histograms of ln(p) for training and test
   * for(auto& s : samples) test.accumulate(s, TrainEventSel(), TestEventSel())
   * Compute KS score and probability
   * auto res = test.result()
   */
  template<class _EvaluatorPtr>
  class OvertrainingTest {
  public:
    OvertrainingTest(_EvaluatorPtr&& eval, size_t nbins = 400):m_eval(std::forward<_EvaluatorPtr>(eval)), m_nbins(nbins),
    m_min_lnp(1e10), m_max_lnp(-1e10), m_hi_train(0), m_hi_test(0) {}

    OvertrainingTest(OvertrainingTest&& o):m_eval(std::move(o.m_eval)), m_nbins(o.m_nbins),
        m_min_lnp(o.m_min_lnp), m_max_lnp(o.m_max_lnp), m_hi_train(std::move(o.m_hi_train)), m_hi_test(std::move(o.m_hi_test)) {
    }

    using ResTy = std::pair<double, double>;
    ~OvertrainingTest() {
      if(m_hi_train) {
        delete m_hi_train;
        m_hi_train = 0;
      }
      if(m_hi_test) {
        delete m_hi_test;
        m_hi_test = 0;
      }
    }

    template<class _Sample>
    void compute_range(const _Sample& spl) {
      spl.apply(AllEventSel(), [this] (const double* d, double) {
        double lnp = -2.0*ln((*m_eval)(d));
        m_min_lnp = std::min(m_min_lnp, lnp);
        m_max_lnp = std::max(m_max_lnp, lnp);
      });
    }

    template<class _Sample, class _TrainingEvSel, class _TestEvSel>
    void accumulate(const _Sample& spl, const _TrainingEvSel& train_ev_sel, const _TestEvSel& test_ev_sel) {
      if(!m_hi_train) {
        m_hi_train = new Histogram<>(m_nbins, m_min_lnp, m_max_lnp);
        m_hi_test = new Histogram<>(m_nbins, m_min_lnp, m_max_lnp);
      }
      spl.apply(train_ev_sel, [this] (const double* d, double) {
        m_hi_train->fill(-2.0*ln((*m_eval)(d)));
      });
      spl.apply(test_ev_sel, [this] (const double* d, double) {
        m_hi_test->fill(-2.0*ln((*m_eval)(d)));
      });
    }

    inline ResTy result() {
      if(m_hi_train && m_hi_test) return kolmogorov_smirnov_test(*m_hi_test, *m_hi_train);
      else return std::make_pair(0.0, 0.0);
    }

    /**
     * Compute K.S. for a range of Samples, at different points,
     * i.e. the evaluator is a SliceEvaluator
     */
    template<class _SampleSet, class _TrainingEvSel, class _TestEvSel>
    ResTy compute(const _SampleSet& samples, const Matrix<double>& points,
        const _TrainingEvSel& train_ev_sel, const _TestEvSel& test_ev_sel) {
      const size_t N(samples.size());
      if(N != points.rows()) return std::make_pair(0.0, 0.0);
      for(unsigned i=0; i<N; ++i) {
        m_eval->slice(points.row_data(i));
        compute_range(samples[i]);
      }
      for(unsigned i=0; i<N; ++i) {
        m_eval->slice(points.row_data(i));
        accumulate(samples[i], train_ev_sel, test_ev_sel);
      }
      return result();
    }

    /** Access the histograms of lnp */
    inline Histogram<>* histogram_training() { return m_hi_train; }
    inline Histogram<>* histogram_test() { return m_hi_test; }
  protected:
    _EvaluatorPtr m_eval;
    size_t m_nbins;
    double m_min_lnp;
    double m_max_lnp;
    Histogram<>* m_hi_train;
    Histogram<>* m_hi_test;
  };

  template<class _EvaluatorPtr>
  inline OvertrainingTest<_EvaluatorPtr> overtraining_test(_EvaluatorPtr&& eval) {
    return OvertrainingTest<_EvaluatorPtr>(std::forward<_EvaluatorPtr>(eval));
  }
}

#endif // sfi_OvertrainingTest_h
