#ifndef sfi_BinaryClassifierPlotsBase_h
#define sfi_BinaryClassifierPlotsBase_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "BinaryClassifierBase.h"
#include "PDF1DComparison.h"

class TCanvas;
class TLegend;

namespace sfi {
  class BinaryClassifierPlotsBase {
  public:
    using This = BinaryClassifierPlotsBase;

    BinaryClassifierPlotsBase(BinaryClassifierBase& cls, const std::string& title, bool details);

    BinaryClassifierPlotsBase(BinaryClassifierBase& cls, const OptionMap& opts);

    using SID = BinaryClassifierBase::SID;
    using SplID = BinaryClassifierBase::SplID;
    using RegID = BinaryClassifierBase::RegID;
    using PID = BinaryClassifierBase::PID;
    using PDFID = BinaryClassifierBase::PDFID;
    using HistTy = BinaryClassifierBase::HistTy;

    inline This& n_pdf_bins(size_t nb) { m_n_pdf_bins = nb; return *this; }

    inline This& plot_ext_coordinates(bool b) { m_plot_ext_coordinates = b; return *this; }

    void plot_S(const std::string& suffix, RegID reg_id);

    void plot_ROC(const std::string& suffix, RegID reg_id);

    void plot_p_or_lnp(bool is_p, bool integrate, const std::string& suffix, PDFID pdf_id);

    void plot_p(const std::string& suffix, PDFID pdf_id);

    void plot_lnp(const std::string& suffix, PDFID pdf_id);

    void make_plots();

    inline TCanvas* roc_canvas(RegID reg_id) const { return m_roc_canvases[reg_id]; }

    inline TLegend* roc_legend(RegID reg_id) const { return m_roc_legends[reg_id]; }

    void save_plots();
  protected:
    Log m_log;
    BinaryClassifierBase& m_classifier;
    std::string m_title, m_file_prefix;
    bool m_details;
    bool m_plot_ext_coordinates;
    bool m_save_plots;
    size_t m_n_pdf_bins;

    std::vector<TCanvas*> m_roc_canvases;
    std::vector<TLegend*> m_roc_legends;
    std::vector<TCanvas*> m_canvases;
  };

  template<class _BinClass>
  class BinaryClassifierPlots : public BinaryClassifierPlotsBase {
  public:
    using Base = BinaryClassifierPlotsBase;
    using This = BinaryClassifierPlots<_BinClass>;

    BinaryClassifierPlots(_BinClass& cls, const std::string& title, bool details);

    BinaryClassifierPlots(_BinClass& cls, const OptionMap& opts);

    void make_plots();
  protected:
    _BinClass& m_t_classifier;
  };

  template<class _BinClass>
  inline BinaryClassifierPlots<_BinClass> make_binary_classifier_plots(_BinClass& cls, const std::string& title, bool details = true) {
    return BinaryClassifierPlots<_BinClass>(cls, title, details);
  }

  template<class _BinClass>
  inline BinaryClassifierPlots<_BinClass> make_binary_classifier_plots(_BinClass& cls, const OptionMap& opts) {
    return BinaryClassifierPlots<_BinClass>(cls, opts);
  }

  template<class _BinClass>
  BinaryClassifierPlots<_BinClass>::BinaryClassifierPlots(_BinClass& cls, const std::string& title, bool details):Base(cls, title, details), m_t_classifier(cls) { }

  template<class _BinClass>
  BinaryClassifierPlots<_BinClass>::BinaryClassifierPlots(_BinClass& cls, const OptionMap& opts):Base(cls, opts), m_t_classifier(cls) { }

  template<class _BinClass>
  void BinaryClassifierPlots<_BinClass>::make_plots() {
    Base::make_plots();
    if(!m_t_classifier.has_1D_transforms()) {
      do_log_error("make_plots() No 1D transforms");
      return;
    }
    PDF1DComparison pdf_train(m_log.context(), m_title+" train");
    pdf_train.debug(m_log.debug()).print_level(m_log.print_level()).plot_external_coordinates(m_plot_ext_coordinates).n_pdf_bins(this->m_n_pdf_bins).transforms(m_t_classifier.generic_transforms()).samples(m_t_classifier.samples(), m_t_classifier.event_selector_training());
    pdf_train.file_prefix(m_file_prefix+m_title+"_train").transforms_1D(m_t_classifier.transforms_1D(RegID::train).generic()).plot();

    PDF1DComparison pdf_test(m_log.context(), m_title+" test");
    pdf_test.debug(m_log.debug()).print_level(m_log.print_level()).plot_external_coordinates(m_plot_ext_coordinates).n_pdf_bins(this->m_n_pdf_bins).transforms(m_t_classifier.generic_transforms()).samples(m_t_classifier.samples(), m_t_classifier.event_selector_test());
    pdf_test.file_prefix(m_file_prefix+m_title+"_test").transforms_1D(m_t_classifier.transforms_1D(RegID::test).generic()).plot();
  }
}

#endif // sfi_BinaryClassifierPlotsBase_h
