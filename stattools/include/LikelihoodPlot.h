#ifndef sfi_LikelihoodPlot_h
#define sfi_LikelihoodPlot_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ILikelihoodMaximizer.h"

#include <vector>
#include <initializer_list>
#include <string>

class TGraph;
class TGraph2D;

#include "Options.h"

namespace sfi {
  class ISample;

  class LikelihoodPlot {
  public:
    LikelihoodPlot(Context& ctx, ILikelihoodMaximizer& pe, const std::initializer_list<double>& par);

    LikelihoodPlot(Context& ctx, ILikelihoodMaximizer& pe, const std::vector<double>& par = {});

    ~LikelihoodPlot();
    
    /** Plot the likelihood for a single parameter, the others are held fixed. */
    bool draw(const std::string& prefix, unsigned par, unsigned npts, double pmin, double pmax);
    TGraph* plot(unsigned par, unsigned npts, double pmin, double pmax);

    /** Plot the likelihood for 2 parameters, the others are held fixed. */
    bool draw2D(const std::string& prefix, unsigned par1, unsigned npts1, double pmin1, double pmax1, unsigned par2, unsigned npts2, double pmin2, double pmax2);
    TGraph2D* plot2D(unsigned par1, unsigned npts1, double pmin1, double pmax1, unsigned par2, unsigned npts2, double pmin2, double pmax2);

    TGraph* plot_profile(unsigned par, const std::string& name, unsigned nppts = 100, double nsigmas = 3.0, bool do_draw = true);

    void data_pdf_comparison(const OptionMap& opts, const ISample& spl);

    TGraph* plot_contour(unsigned par1, unsigned par2, unsigned npts);
  protected:
    void init_par();

    Context& m_context;
    ILikelihoodMaximizer& m_pe;
    std::vector<double> m_par;
    bool m_work_values_set;
  };
}

#endif // sfi_LikelihoodPlot_h
