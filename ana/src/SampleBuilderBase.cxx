/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "SampleBuilderBase.h"
#include "Variables.h"
#include "Sample.h"

using namespace sfi;

SampleBuilderBase::~SampleBuilderBase() {}

// ---- implementation of IComponent ----

const std::string&
SampleBuilderBase::get_type() const { return m_type_id; }

const std::string&
SampleBuilderBase::get_name() const { return m_name; }

OptionMap&
SampleBuilderBase::get_options() { return m_options; }

const OptionMap&
SampleBuilderBase::get_options() const { return m_options; }


// ---- implementation of ISampleBuilder ----

std::shared_ptr<DataSource> SampleBuilderBase::get_data_source(const OptionMap& opts) const {
  return std::make_shared<DefaultDataSource>(opts);
}

// ---- protected methods ----

SampleBuilderBase::SampleBuilderBase(Context& ctx, const OptionMap& opts):
    m_log(Log::get(ctx, opts.get<std::string>("name"))), m_options(opts), m_dims(-1), m_is_weighted(false) {
  bind_options();
  if(opts.has("print_level")) m_log.print_level(opts.get<std::string>("print_level"));
  m_variables = initialize_variables(opts);
}

void
SampleBuilderBase::bind_options() {
  m_options.bind("name", m_name);
  m_options.bind("type_id", m_type_id);
  m_options.bind("dims", m_dims);
  m_options.bind("is_weighted", m_is_weighted);
}

template<class _use_w>
ISample* create_sample_impl(Log& log, Context& ctx, unsigned dims, unsigned nevents) {
  switch(dims) {
  case 1: { return ctx.memory_pool().pool<Sample<1, _use_w>>()->get(nevents, ""); }
  case 2: { return ctx.memory_pool().pool<Sample<2, _use_w>>()->get(nevents, ""); }
  case 3: { return ctx.memory_pool().pool<Sample<3, _use_w>>()->get(nevents, ""); }
  case 4: { return ctx.memory_pool().pool<Sample<4, _use_w>>()->get(nevents, ""); }
  case 5: { return ctx.memory_pool().pool<Sample<5, _use_w>>()->get(nevents, ""); }
  case 6: { return ctx.memory_pool().pool<Sample<6, _use_w>>()->get(nevents, ""); }
  case 7: { return ctx.memory_pool().pool<Sample<7, _use_w>>()->get(nevents, ""); }
  case 8: { return ctx.memory_pool().pool<Sample<8, _use_w>>()->get(nevents, ""); }
  case 9: { return ctx.memory_pool().pool<Sample<9, _use_w>>()->get(nevents, ""); }
  case 10: { return ctx.memory_pool().pool<Sample<10, _use_w>>()->get(nevents, ""); }
  }
  log_error(log, "create_sample_impl() dims: "<<dims<<" nevents: "<<nevents<<" Use weight: "<<type_name<_use_w>());
  return 0;
}

ISample*
SampleBuilderBase::create_sample(Context& ctx, unsigned dims, bool use_weight, unsigned nevents) {
  if(!dims) {
    do_log_error("create_sample() dims == 0!");
    return 0;
  }
  if(use_weight) return ::create_sample_impl<ttrue>(m_log, ctx, dims, nevents);
  else return ::create_sample_impl<tfalse>(m_log, ctx, dims, nevents);
}

PVariables
SampleBuilderBase::initialize_variables(const OptionMap& opts) const {
  if(!opts.has_sub_vec("variables")) return nullptr;
  auto& ovar = opts.sub_vec("variables");
  if(ovar.size() != (unsigned)m_dims) {
    do_log_error("initialize_variables() Invalid variable count, expected "<<m_dims<<" variables in 'variables'");
    return nullptr;
  }
  return std::make_shared<Variables>(ovar);
}

