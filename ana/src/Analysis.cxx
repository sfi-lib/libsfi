/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Analysis.h"
#include "util.h"
#include "ComponentHandler.h"
#include "ISampleBuilder.h"
#include "timer.h"
#include "ITransform.h"
#include "Parameters.h"

#include <iostream>

using namespace sfi;

// ---- AnalysisDataSource ----

AnalysisDataSource::AnalysisDataSource(const std::string& name, const std::shared_ptr<DataSource>& ds):m_source(ds) {
  if(ds->name().empty()) ds->options().set("name", name);
}

AnalysisDataSource::AnalysisDataSource(const std::shared_ptr<DataSource>& ds):m_source(ds) {}

void AnalysisDataSource::add_variant(const std::shared_ptr<DataSource>& vds) {
  m_variants[vds->name()] = vds;
}

AnalysisDataSource::~AnalysisDataSource() {}

// ---- AnalysisChannel ----

AnalysisChannel::AnalysisChannel(const std::string& name, ISampleBuilder* sb, const PVariables& vars):
        m_name(name), m_sample_builder(sb), m_variables(vars) {
}

AnalysisChannel::~AnalysisChannel() {
  if(m_sample_builder) {
    // delete m_sample_builder;
    m_sample_builder = 0;
  }
}

std::shared_ptr<DataSource>
AnalysisChannel::source(const std::string& name) const {
  auto it = m_sources.find(name);
  if(it != m_sources.end()) return it->second.source();
  return 0;
}

bool
AnalysisChannel::has_analysis_source(const std::string& name) const {
  return m_sources.find(name) != m_sources.end();
}

AnalysisDataSource&
AnalysisChannel::analysis_source(const std::string& name) {
  auto it = m_sources.find(name);
  if(it != m_sources.end()) return it->second;
  throw AnalysisException("Unknown source '"+name+"' in channel '"+m_name+"'");
}

// ---- Analysis::Experiment ----

Analysis::Experiment::Experiment(const std::string& name, const OptionMap& opts):m_name(name), m_options(opts) { }

Analysis::Experiment::Experiment(Experiment&& ex):m_name(std::move(ex.m_name)), m_options(std::move(ex.m_options)) {}

// ---- Analysis::ParameterSet ----

Analysis::ParameterSet::ParameterSet(const std::string& name):m_name(name) { }

Analysis::ParameterSet::ParameterSet(ParameterSet&& ex):m_name(std::move(ex.m_name)), m_parameters(std::move(ex.m_parameters)) {}

Analysis::ParameterSet::Param::Param():
    m_type(Parameter::Normal),m_initial(std::nan("")),m_minv(std::nan("")),m_maxv(std::nan("")), m_step(std::nan(""))/*, m_norm(std::nan(""))*/,
    m_fixed(false),m_limited(false) {}

// ---- Analysis ----

Analysis::Analysis(const std::string& name):m_name(name),
    m_default_sample_builder(0), m_main_context(),
    m_log(Log::get(m_main_context, "Analysis{"+name+"}"))
#ifdef SFI_ENABLE_MP
    , m_dispatcher(m_main_context)
#endif
{
  m_options.bind("name", m_name);
  m_options.sub("default_source", true).set<std::string>("name", "default");
}

Analysis::Analysis(OptionMap& _opts):m_name(_opts.get<std::string>("analysis.name")),
    m_options(_opts.sub("analysis"), true),
    m_default_sample_builder(0), m_main_context(),
    m_log(Log::get(m_main_context, "Analysis{"+m_name+"}"))
#ifdef SFI_ENABLE_MP
    , m_dispatcher(m_main_context, _opts.sub("analysis.dispatcher"))
#endif
{
  m_options.bind("name", m_name);
  m_options.sub("default_source", true).set<std::string>("name", "default");
  if(m_options.has("print_level")) m_log.print_level(m_options.get<std::string>("print_level"));
  if(m_options.has_sub("sources")) {
    do_log_error("Analysis() Specify sources per channel!");
  }
  if(m_options.has_sub("channels")) init_channels();
  if(m_options.has_sub("experiments")) init_experiments();
  if(m_options.has_sub("parameters")) init_parameter_sets();
}

Analysis::Analysis(Analysis&& a):
    m_name(std::move(a.m_name)),
    m_options(std::move(a.m_options)),
    m_default_sample_builder(a.m_default_sample_builder),
    m_channels(std::move(a.m_channels)),
    m_experiment_sets(std::move(a.m_experiment_sets)),
    m_parameter_sets(std::move(a.m_parameter_sets)),
    m_transforms(std::move(a.m_transforms)),
    m_main_context(std::move(a.m_main_context)),
    m_log(std::move(a.m_log))
#ifdef SFI_ENABLE_MP
    , m_dispatcher(m_main_context)
#endif
{
  a.m_channels.clear();
  a.m_experiment_sets.clear();
  a.m_parameter_sets.clear();
  a.m_transforms.clear();
}

Analysis::~Analysis() {
  do_log_debug("Stopping");
}

AnalysisChannel&
Analysis::channel(const std::string& cname) {
  auto itm = m_channels.find(cname);
  if(itm != m_channels.end()) return itm->second;
  throw AnalysisException("Unknown channel '"+cname+"'");
}

const Analysis::ParameterSet&
Analysis::parameter_set(const std::string& psetname) {
  auto it = m_parameter_sets.find(psetname);
  if(it == m_parameter_sets.end()) throw AnalysisException("Unknown parameter set '"+psetname+"'");
  auto& ps = it->second;

  for(auto& pari : ps) {
    auto& par = pari.second;
    if(!par.limited() && !par.transform().empty()) {
      auto ti = m_transforms.find(par.transform());
      if(ti != m_transforms.end()) {
        ITransform* trf = ti->second;
        auto vars = trf->get_variables();
        if(!vars) {
          do_log_error("parameter_set() Parameter '"<<par.name()<<"' has transform '"<<trf->get_name()<<"' with no variables");
        }
        else {
          auto* v = vars->lookup(par.variable());
          if(v) {
            par.m_minv = v->minval();
            par.m_maxv = v->maxval();
            par.m_limited = true;
          }
          else {
            do_log_error("parameter_set() Transform '"<<par.transform()<<"' has no variable '"<<par.variable()<<"'");
          }
        }
      }
      else {
        do_log_error("parameter_set() Transform '"<<par.transform()<<"' not found for parameter '"<<par.name()<<"'");
      }
    }
  }
  return ps;
}

bool Analysis::has_experiment_set(const std::string& exset) const {
  return m_experiment_sets.find(exset) != m_experiment_sets.end();
}

/** @return The experiments in an experiment set */
std::vector<std::string> Analysis::experiments_in_set(const std::string& exset) const {
  auto esit = m_experiment_sets.find(exset);
  if(esit == m_experiment_sets.end()) throw AnalysisException("Undefined experiment set '"+exset+"'");
  auto& es = esit->second;
  std::vector<std::string> res;
  for(auto& exi : es.experiments) res.push_back(exi.first);
  return res;
}

bool Analysis::has_experiment(const std::string& exset, const std::string& exname) const {
  auto esit = m_experiment_sets.find(exset);
  if(esit == m_experiment_sets.end()) throw AnalysisException("Undefined experiment set '"+exset+"'");
  auto& es = esit->second;
  return es.experiments.find(exname) != es.experiments.end();
}

const Analysis::Experiment& Analysis::experiment(const std::string& exset, const std::string& exname) const {
  auto esit = m_experiment_sets.find(exset);
  if(esit == m_experiment_sets.end()) throw AnalysisException("Undefined experiment set '"+exset+"'");
  auto& es = esit->second;
  auto exi = es.experiments.find(exname);
  if(exi == es.experiments.end()) throw AnalysisException("Experiment '"+exname+"' in set '"+exset+"' not defined");
  return exi->second;
}

SampleMap
Analysis::experiment_samples(const std::string& exset, const std::string& exname, bool normalize_vars) {
  auto esit = m_experiment_sets.find(exset);
  if(esit == m_experiment_sets.end()) throw AnalysisException("Undefined experiment set '"+exset+"'");
  auto& es = esit->second;
  
  auto exi = es.experiments.find(exname);
  if(exi == es.experiments.end()) {
    do_log_error("experiment_samples() no experiment '"<<exname<<"' defined in set '"+exset+"'");
    throw AnalysisException("Experiment '"+exname+"' not defined");
  }
  Timer<true> ti;
  ti.start();
  auto& aexpr = exi->second;
  SampleMap res;
  for(auto& _ch : m_channels) {
    auto& ch = _ch.second;
    if(_ch.first != ch.name()) {
      do_log_warning("experiment_samples() Channel '"<<ch.name()<<"' registered as '"<<_ch.first<<"'");
    }
    if(aexpr.has_channel(ch.name())) {
      do_log_debug("experiment_samples() generating samples for channel '"<<_ch.first<<"'");
      ISample* ch_spl = experiment_channel_sample(ch, aexpr);
      if(ch_spl) {
        do_log_debug("experiment_samples() Sample for channel '"<<ch.name()<<"' contains "<<ch_spl->get_events()<<" events");
        if(normalize_vars) ch_spl->do_normalize(m_log);
        res.put(ch.name(), ch_spl);
      }
    }
  }
  double time = ti.stop();
  if(!res.empty()) {
    do_log_debug("experiment_samples() experiment '"<<exname<<"' generated in "<<time<<" s");
  } else {
    do_log_debug("experiment_samples() experiment '"<<exname<<"' generated no samples");
  }
  return res;
}

bool Analysis::add_transform(ITransform* trf) {
  if(trf->get_name().empty()) {
    do_log_error("add_transform() Transform has no name");
    return false;
  }
  auto it = m_transforms.find(trf->get_name());
  if(it != m_transforms.end()) {
    do_log_error("add_transform() Transform with name '"<<trf->get_name()<<"' already registered");
    return false;
  }
  m_transforms[trf->get_name()] = trf;
  return true;
}

ITransform* Analysis::transform(const std::string& name) {
  auto it = m_transforms.find(name);
  if(it != m_transforms.end()) return it->second;
  else return 0;
}

bool Analysis::build_parameters(const ParameterSet& pset, Parameters& pars, const OptionMap* meta) {
  for(auto& pari : pset) {
    auto& p = pari.second;
    auto& par = pars[p.name()];
    par.type(p.type());
    if((meta != nullptr) && (!p.source().empty()) ) { // && meta->has(p.source())
      par.initial(meta->get_as<double>(p.source()));
    } else par.initial(p.initial());
    par.fixed(p.fixed());
    if(p.limited()) par.limit(p.minval(), p.maxval());
    if(std::isfinite(p.step())) par.step_length(p.step());
    else par.step_length(0.01);
  }
  return true;
}

ISampleBuilder* Analysis::channel_sample_builder(const std::string& cname) const {
  auto itm = m_channels.find(cname);
  if(itm != m_channels.end()) {
    return itm->second.sample_builder() ? itm->second.sample_builder() : m_default_sample_builder;
  }
  else return 0;
}

std::vector<ISample*>
Analysis::channel_samples(const std::string& cname) {
  AnalysisChannel& cdef = channel(cname);
  std::vector<ISample*> samples;

  std::map<std::string, std::shared_ptr<DataSource>> sources;
  for(auto& ach_src : cdef.sources()) sources[ach_src.second.name()] = ach_src.second.source();
  if(sources.empty()) {
    do_log_error("channel_samples() no source for channel '"<<cname<<"'");
    return samples;
  }
  ISampleBuilder* sbuilder = cdef.sample_builder() ? cdef.sample_builder() : m_default_sample_builder;
  if(sbuilder == 0) {
    do_log_error("channel_samples() no SampleBuilder for channel '"<<cname<<"'");
    return samples;
  }
  for(auto sb = sources.begin(); sb != sources.end(); ++sb) {
    do_log_debug("channel_samples() Getting nominal sample from source '"<<sb->second->name()<<"' for channel '"<<cdef.name()<<"'");
    ISample* spl = channel_source_sample(sbuilder, sb->second, cdef);
    if(spl) samples.push_back(spl);
    if(cdef.has_analysis_source(sb->first)) {
      AnalysisDataSource& asrc = cdef.analysis_source(sb->first);
      if(asrc.has_variants()) {
        for(auto& varit : asrc.variants()) {
          do_log_debug("channel_samples() Getting variant '"<<varit.first<<"' from source '"<<varit.second->name()<<"' for channel '"<<cdef.name()<<"'");
          spl = channel_source_sample(sbuilder, varit.second, cdef);
          if(spl) samples.push_back(spl);
        }
      }
    }
  }
  if(samples.empty()) {
    do_log_error("channel_samples() No samples generated for channel '"<<cname<<"'");
  }
  return samples;
}

ISample*
Analysis::channel_sample(const std::string& cname, const std::shared_ptr<const DataSource>& src) {
  AnalysisChannel& cdef = channel(cname);
  ISampleBuilder* sbuilder = cdef.sample_builder() ? cdef.sample_builder() : m_default_sample_builder;
  return channel_source_sample(sbuilder, src, cdef);
}

ISample*
Analysis::channel_source_sample(ISampleBuilder* sbuilder, const std::shared_ptr<const DataSource>& src, AnalysisChannel& cdef) {
  if(!cdef.variables()) {
    do_log_error("channel_source_sample() No variables for channel '"<<cdef.name()<<"' builder '"<<sbuilder->get_name()<<"'");
    return 0;
  }
  auto cvars = sbuilder->get_variables(*src);
  if(!cvars) cvars = cdef.variables();
  if(!cvars) {
    do_log_error("channel_source_sample() Unable to determine variables for builder '"<<sbuilder->get_name()<<"' channel '"<<cdef.name()<<"' source '"<<src->name()<<"'");
    return 0;
  }
  ISample* spl = sbuilder->do_build(m_main_context, cvars, *src);
  if(!spl) {
    do_log_error("channel_source_sample() Failed to get ISample from '"<<sbuilder->get_name()<<"'");
    return 0;
  }
  spl->set_name(cdef.name() + "_" + src->name());
  spl->set_data_source(src);
  do_log_debug("channel_source_sample() sample: '"<<spl->get_name()<<"' from source '"<<src->name()<<"' of type '"<<spl->get_type_name()<<"' with "<<spl->get_events()<<" events, variables: "<<*spl->get_variables());
  auto svars = spl->get_variables();
  if(!svars || (spl->get_variables()->nvariables() == 0)) {
    do_log_error("channel_source_sample() No variables for sample from '"<<sbuilder->get_name()<<"' for channel '"<<cdef.name()<<"' source '"<<src->name()<<"'");
    return 0;
  }
  if(!cdef.variables()->compare(*svars)) {
    do_log_error("channel_source_sample() Variables mismatch for sample from '"<<sbuilder->get_name()<<"' for channel '"<<cdef.name()<<"' source '"<<src->name()<<"'");
    do_log_error(""<<*cdef.variables());
    do_log_error(""<<*svars);
    return 0;
  }
  return spl;
}

ISample*
Analysis::experiment_channel_sample(AnalysisChannel& ch, Experiment& expr) {
  if(!expr.has_channel(ch.name())) {
    do_log_error("experiment_channel_sample() No config options for channel '"<<ch.name()<<"'");
    return 0;
  }
  const OptionMap& ch_op = expr.options().sub(ch.name());
  ISample* ch_spl = 0;
  auto& rgen = m_main_context.rnd_gen();
  for(auto& _ch_src : ch.sources()) {
    AnalysisDataSource& ch_src = _ch_src.second;
    if(_ch_src.first != ch_src.name()) {
      do_log_warning("experiment_channel_sample() Source '"<<ch_src.name()<<"' for channel '"<<ch.name()<<"' registered as '"<<_ch_src.first<<"'");
    }
    if(ch_op.has_sub(ch_src.name())) {
      auto& ch_src_op = ch_op.sub(ch_src.name());
      do_log_debug("experiment_channel_sample() Generating samples for source '"<<ch_src.name()<<"' in channel '"<<ch.name()<<"'");
      ISample* ch_src_spl = 0;
      if(ch_src.has_sample_generator()) {
        do_log_debug("experiment_channel_sample() Invoking custom sample generator for source '"<<ch_src.name()<<"' in channel '"<<ch.name()<<"'");
        ch_src_spl = ch_src.sample_generator()(ch_src_op);
        if(!ch_src_spl) {
          do_log_error("experiment_channel_sample() Failed to get sample from custom sample generator for source '"<<ch_src.name()<<"' for channel '"<<ch.name()<<"' is 0");
        }
      }
      else if(ch_src.source()) {
        // TODO: shared ptr
        auto ch_data_src = ch_src.source()->do_clone();
        for(auto& po : ch_src_op) {
          auto& parname = po.first;
          if(parname == "N") {
            if(!ch_src_op.get_as<bool>("N.fixed", false)) {
              std::poisson_distribution<> po(ch_src_op.get_as<double>("N.value"));
              int Nev = po(rgen);
              do_log_debug("experiment_channel_sample() Parameter 'N' with mean "<<ch_src_op.get_as<double>("N.value")<<" po "<<Nev);
              ch_data_src->nevents(Nev);
              // TODO: can have unc.
            }
            else {
              ch_data_src->nevents(ch_src_op.get_as<double>("N.value"));
            }
          }
          else {
            if(ch_data_src->options().has(parname)) {
              if(ch_src_op.has_sub(parname)) {
                auto & parval = ch_src_op[parname+".value"];
                do_log_debug("experiment_channel_sample() Parameter '"<<parname<<"' with value: "<<parval);
                ch_data_src->options()[parname] = parval;
                // TODO: can have unc.
              }
              else if(ch_src_op.has(parname)) {
                auto & parval = ch_src_op[parname];
                do_log_debug("experiment_channel_sample() Parameter '"<<parname<<"' with value: "<<parval);
                ch_data_src->options()[parname] = parval;
              }
            }
            else {
              do_log_error("experiment_channel_sample() Parameter '"<<parname<<"' not defined for source '"<<ch_src.name()<<"' in channel '"<<ch.name()<<"'");
            }
          }
        }
        ch_src_spl = channel_sample(ch.name(), ch_data_src);
      }
      else {
        do_log_error("experiment_channel_sample() Source '"<<_ch_src.first<<"' for channel '"<<ch.name()<<"' is 0");
      }
      do_log_verbose("experiment_channel_sample() Sample for channel '"<<ch.name()<<"' source '"<<ch_src.name()<<"'  @"<<(void*)ch_src_spl);
      if(ch_src_spl) {
        do_log_debug("experiment_channel_sample() Sample for source '"<<ch_src.name()<<"' in channel '"<<ch.name()<<"' variables: "<<*ch_src_spl->get_variables());
        if(ch_spl) {
          if(!ch_spl->do_append(*ch_src_spl)) {
            do_log_error("experiment_channel_sample() Unable to append sample '"<<ch_src_spl->get_name()<<"' for source '"<<ch_src.name()<<"' to sample '"<<ch_spl->get_name()<<"'");
          }
          ch_src_spl->do_release();
        }
        else {
          // the first source
          ch_spl = ch_src_spl;
          ch_spl->set_data_source(ch_src.source());
        }
        auto& meta = ch_spl->get_meta_data();
        
        // Copy options from DataSource
        auto ds = ch_spl->get_data_source();
        auto& mops = meta.sub(ch.name()+"."+ch_src.name(), true);
        mops = ds->options();
        
        // Override options from experiment
        for(auto& po : ch_src_op) {
          auto& parname = po.first;
          if((parname != "N") && mops.has(parname)) {
            if(ch_src_op.has_sub(parname)) mops[parname] = ch_src_op[parname+".value"];
            else if(ch_src_op.has(parname)) mops[parname] = ch_src_op[parname];
          }
        }
        mops.set("N", ch_src_spl->get_events());
      }
      else {
        do_log_error("experiment_channel_sample() Failed to get sample for source '"<<ch_src.name()<<"' in channel '"<<ch.name()<<"'");
      }
    }
    else {
      do_log_error("experiment_channel_sample() No config options for source '"<<ch_src.name()<<"' in channel '"<<ch.name()<<"'");
    }
  }
  if(ch_spl) {
    auto vars = ch_spl->get_variables();
    if(!vars) {
      do_log_error("experiment_channel_sample() Sample for channel '"<<ch.name()<<"' has no variables!");
      return 0;
    }
    else {
      vars->lock();
    }
  }
  return ch_spl;
}

void Analysis::init_channels() {
  for (auto& ch : m_options.sub("channels")) {
    do_log_debug("** channel name="<<ch.first<<" : "<<*ch.second);
    // create builder
    OptionMap* omap = dynamic_cast<OptionMap*>(ch.second);
    if (omap && omap->has_sub("builder")) init_channel(ch.first, *omap);
    else {
      do_log_error("Analysis() invalid channel Option type: "<<*ch.second);
    }
  }
}

void Analysis::init_channel(const std::string& chname, OptionMap& omap) {
  auto& src_opts = omap.sub("builder");
  std::string libn;
  src_opts.get_if("library", libn);
  IComponent* src_comp = m_main_context.component_handler()->component(m_main_context, src_opts.get<std::string>("type_id"), libn, src_opts);
  if(!src_comp) {
    do_log_error("Analysis() unable to create component: '"<<src_opts.get<std::string>("type_id")<<"'");
  }

  ISampleBuilder* sb = dynamic_cast<ISampleBuilder*>(src_comp);
  if(!sb) {
    do_log_error("Analysis() unable to create channel: "<<omap<<" component '"<<src_comp->get_name()<<"' of type '"<<src_comp->get_type()<<"' is no ISampleBuilder");
  }

  if(!omap.has_sub_vec("variables")) {
    do_log_error("Analysis() Expected 'variables' for channel '"<<chname<<"'");
    return;
  }
  auto& varopts = omap.sub_vec("variables");
  auto vars = std::make_shared<Variables>(varopts);
  auto& ach = add_channel(chname, sb, vars);
  // TODO: Remove: -->
  if(omap.has_sub("source")) {
    auto& comap = omap.sub("source");
    if(comap.has_type<std::string>("name")) {
      auto& nm = comap.get<std::string>("name");
      ach.m_sources.insert( std::make_pair(nm, AnalysisDataSource(nm, sb->get_data_source(comap))));
    } else {
      do_log_error("Analysis() invalid channel source Option type: "<<comap);
    }
  }
  // <--
  if(omap.has_sub("sources")) {
    for(auto& coo : omap.sub("sources")) {
      do_log_debug("*** channel name="<<chname<<" source name="<<coo.first<<" : "<<*coo.second);
      auto* comap = dynamic_cast<OptionMap*>(coo.second);
      if(comap)
        ach.m_sources.insert( std::make_pair(coo.first, AnalysisDataSource(coo.first, sb->get_data_source(*comap))));
      else {
        do_log_error("Analysis() invalid channel source Option type: "<<*coo.second);
      }
    }
  }
  if(omap.has_sub("variants")) {
    for(auto& vch : omap.sub("variants")) {
      if(ach.has_analysis_source(vch.first)) {
        AnalysisDataSource& ads = ach.analysis_source(vch.first);
        auto* vmap = dynamic_cast<OptionMap*>(vch.second);
        if(vmap) {
          for(auto& varop : *vmap) init_variant(varop.first, ach, ads, dynamic_cast<OptionMap*>(varop.second));
        } else {
          do_log_error("Analysis() Invalid variant options for source '"<<vch.first<<"' for channel '"<<ach.name()<<"'");
        }
      } else {
        do_log_error("Analysis() Unknown source '"<<vch.first<<"' for channel '"<<ach.name()<<"'");
      }
    }
  }
}

void Analysis::init_variant(const std::string& varname, AnalysisChannel& ch, AnalysisDataSource& src, const OptionMap* varmap) {
  if(varmap) {
    if(varmap->has("variables")) {
      do_log_error("Analysis() Invalid options for variant '"<<varname<<"' source '"<<src.name()<<"' channel '"<<ch.name()<<"' must not override 'variables'");
      throw AnalysisException("Variant must not override 'variables'");
    }
    auto vds = src.source()->get_variant(*varmap, varname);
    do_log_debug("*** variant '"<<varname<<"' => '"<<vds->name()<<"' for channel name="<<ch.name()<<" source name="<<src.name()<<" : "<<*varmap<<" => "<<vds->options());
    for(auto& vop : src.source()->options()) {
      if(!vds->options().has(vop.first)) {
        vds->options().copy(vop.first, *vop.second);
        do_log_debug("*** variant '"<<varname<<"' for channel name="<<ch.name()<<" source name="<<src.name()<<" : retained option '"<<vop.first<<"' : "<<*vop.second);
      } else {
        do_log_debug("*** variant '"<<varname<<"' for channel name="<<ch.name()<<" source name="<<src.name()<<" : setting '"<<vop.first<<"' to "<<vds->options()[vop.first]);
      }
    }
    src.add_variant(vds);
  } else {
    do_log_error("Analysis() Invalid options for variant '"<<varname<<"' source '"<<src.name()<<"' channel '"<<ch.name()<<"'");
  }
}

AnalysisChannel&
Analysis::add_channel(const std::string& name, ISampleBuilder* sb, const PVariables& vars) {
  if(has_channel(name)) {
    do_log_error("add_channel() Channel with name '"<<name<<"' already exists");
    throw AnalysisException("Channel with name '"+name+"' already exists");
  }
  if(!sb) {
    do_log_error("add_channel() SampleBuilder is 0 for channel '"+name+"'");
    throw AnalysisException("SampleBuilder is 0 for channel '"+name+"'");
  }
  auto r = m_channels.insert(std::make_pair(name, AnalysisChannel(name, sb, vars)));
  if(r.second) {
    do_log_debug("add_channel() Added channel name '"<<name<<"' builder: "<<sb->get_name());
    AnalysisChannel& ch = r.first->second;
    if(!ch.m_variables->nvariables()) {
      throw AnalysisException("Variables not initialized");
    }
    return ch;
  }
  throw AnalysisException("Failed to add channel '"+name+"'");
}

Analysis&
Analysis::add_source(const std::string& channel_name, const std::string& name, const std::shared_ptr<DataSource>& es) {
  AnalysisChannel& cdef = channel(channel_name);
  if(cdef.has_analysis_source(name)) throw AnalysisException("Channel '"+channel_name+"' already has source '"+name+"'");
  cdef.m_sources.insert(std::make_pair(name, AnalysisDataSource(es)));
  return *this;
}

void Analysis::init_experiments() {
  auto& expso = m_options.sub("experiments");
  for(auto& exo : expso) {
    do_log_debug("init_experiments() adding experiment set '"<<exo.first<<"'");
    auto& es = m_experiment_sets[exo.first];
    auto* esop = dynamic_cast<OptionMap*>(exo.second);
    if(esop) {
      for(auto& expo : *esop) {
        do_log_debug("init_experiments() adding experiment '"<<expo.first<<"'");
        es.experiments.emplace(std::piecewise_construct, std::forward_as_tuple(expo.first), std::forward_as_tuple(expo.first,dynamic_cast<OptionMap&>(*expo.second)));
      }
    }
    else {
      do_log_error("init_experiments() Invalid experiment set '"+exo.first+"', must be {}");
    }
  }
}

void Analysis::init_parameter_sets() {
  auto& pso = m_options.sub("parameters");
  for(auto& psop : pso) {
    auto& parsop = dynamic_cast<OptionMap&>(*psop.second);
    do_log_debug("init_parameter_sets() adding parameter set '"<<psop.first<<"'");
    auto psit = m_parameter_sets.emplace(std::piecewise_construct, std::forward_as_tuple(psop.first), std::forward_as_tuple(psop.first));
    auto& parset = psit.first->second;
    for(auto& p : parsop) {
      auto* _po = dynamic_cast<OptionMap*>(p.second);
      if(_po) {
        auto& po = *_po;
        ParameterSet::Param par;
        par.m_name = p.first;
        par.m_type = Parameter::Normal;

        std::string ptype;
        po.get_if("type", ptype);
        if(ptype == "nuisance") par.m_type = Parameter::Nuisance;
        else if(ptype == "absnorm") par.m_type = Parameter::AbsoluteNorm;
        else if(ptype == "relnorm") par.m_type = Parameter::RelativeNorm;

        if(po.has("initial")) par.m_initial = po.get_as<double>("initial");
        else {
          do_log_warning("init_parameter_sets() Parameter '"<<p.first<<"' has no initial value");
        }

        if(po.has("fixed")) par.m_fixed = po.get_as<bool>("fixed");
        if(po.has("transform")) par.m_transform = po.get_as<std::string>("transform");
        if(po.has("variable")) par.m_variable = po.get_as<std::string>("variable");
        if(po.has("source")) par.m_source = po.get_as<std::string>("source");
        if(po.has("min")) {
          par.m_minv = po.get_as<double>("min");
          par.m_maxv = po.get_as<double>("max");
          par.m_limited = true;
        }
        if(po.has("step")) par.m_step = po.get_as<double>("step");
        parset.add(std::move(par));
      }
      else {
        do_log_error("init_parameter_sets() Parameter '"<<p.first<<"' is not an object {}");
      }
    }
  }
}
