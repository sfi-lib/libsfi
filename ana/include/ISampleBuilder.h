#ifndef sfi_ISampleBuilder_h
#define sfi_ISampleBuilder_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string>

#include "IComponent.h"
#include "Context.h"
#include "DataSource.h"
#include "Variables.h"

namespace sfi {
  class ISample;
  class Variables;

  class ISampleBuilder : public IComponent {
  public:
    virtual ~ISampleBuilder() {}
    
    /** Build the Sample */
    virtual ISample* do_build(Context& ctx, const PVariables& vars, const DataSource& src_data) = 0;

    /** @return Number of dimensions */
    virtual int get_dims() const = 0;

    /** @return If the samples are weighted */
    virtual bool get_is_weighted() const = 0;

    /** @return Variables if the given source has variables defined, else nullptr */
    virtual PVariables get_variables(const DataSource& ds) const = 0;

    /** @return DataSource implementation for this builder */
    virtual std::shared_ptr<DataSource> get_data_source(const OptionMap& opts) const = 0;
  };
}

#endif // sfi_ISampleBuilder_h
