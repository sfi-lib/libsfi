#ifndef sfi_Analysis_h
#define sfi_Analysis_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string>
#include <vector>
#include <map>

#include <Sample.h>
#include <Options.h>
#include <Context.h>
#include <Variables.h>
#include <IAnalysis.h>
#include <Parameters.h>
#ifdef SFI_ENABLE_MP
#include <mp/Dispatcher.h>
#endif

namespace sfi {
  class ISampleBuilder;
  class ComponentHandler;
  class DataSource;
  class Analysis;
  class ITransform;
  
  class AnalysisException : public Exception {
  public:
    AnalysisException(const std::string& msg):Exception(msg) {}
  };

  class AnalysisDataSource {
  public:
    using SampleGeneratorTy = std::function<ISample*(const OptionMap& src_op)>;
    using VariantsMap = std::map<std::string, std::shared_ptr<DataSource>>;

    ~AnalysisDataSource();
    
    friend class Analysis;

    inline const std::string& name() const { return m_source->name(); }

    inline std::shared_ptr<DataSource> source() const { return m_source; }

    inline AnalysisDataSource& sample_generator(SampleGeneratorTy&& sg) { m_sample_gen = std::move(sg); return *this; }

    inline SampleGeneratorTy& sample_generator() { return m_sample_gen; }

    inline bool has_sample_generator() const { return m_sample_gen != nullptr; }

    inline bool has_variants() const { return !m_variants.empty(); }

    inline VariantsMap& variants() { return m_variants; }
  protected:
    AnalysisDataSource(const std::string& name, const std::shared_ptr<DataSource>& sds);

    AnalysisDataSource(const std::shared_ptr<DataSource>& ds);

    void add_variant(const std::shared_ptr<DataSource>& vds);

    std::shared_ptr<DataSource> m_source;
    SampleGeneratorTy m_sample_gen;
    VariantsMap m_variants;
  };

  class AnalysisChannel {
  public:
    ~AnalysisChannel();
    
    friend class Analysis;

    inline const std::string& name() const { return m_name; }

    inline ISampleBuilder* sample_builder() const { return m_sample_builder; }

    std::shared_ptr<DataSource> source(const std::string& name) const;

    bool has_analysis_source(const std::string& name) const;

    AnalysisDataSource& analysis_source(const std::string& name);

    inline void variables(const PVariables& v) { m_variables = v; }
    inline const PVariables& variables() const { return m_variables; }

    inline const std::map<std::string, AnalysisDataSource>& sources() const { return m_sources; }
    inline std::map<std::string, AnalysisDataSource>& sources() { return m_sources; }
  protected:
    AnalysisChannel(const std::string& name, ISampleBuilder* sb, const PVariables& vars);
    
    std::string m_name;
    ISampleBuilder* m_sample_builder;
    std::map<std::string, AnalysisDataSource> m_sources;
    PVariables m_variables;
  };

  /**
   * @class sfi::Analysis
   *
   * Functionality for handling more complex use cases
   * with several "channels" each with data from several
   * sources. Each source may also have several variants with
   * different settings. A variant typically corresponds to a systematic
   * shift, or to a point in a grid used to build a parametried Transform.
   * A channel has a "builder", an instance of ISampleBuilder that
   * is responsible for materializing Sample. Each source specifies
   * different options to this single builder.
   *
   * The analysis may contain experiments, that are templates for
   * generating samples for multiple channels and sources. Multiple
   * experiments are grouped in a named set.
   *
   * The analysis may contain parameter sets, used in likelihood
   * construction and maximization. Each parameter may be linked
   * to a specific dimension of a Transform using the options "transform"
   * and "variable". A parameter may also be linked to a data source
   * option, using the option "source".
   *
   * Analysis also manages shared resources:
   * - ComponentHandler : Dynamic instantiation of IComponent.
   * - Context : The main computation context
   * - MemoryPool : Shared memory pool
   * - Dispatcher : Multi threading functionality
   *
   *  Options handled by the Analysis:
   *    analysis: {
   *      name: <string>
   *      default_source:
   *      print_level: <string>
   *      channels: {
   *        <ch name>: {
   *          builder: {
   *            library:<string>
   *            type_id: <string>
   *            (Builder specific options)
   *          }
   *          variables: <variable array>
   *          sources: {
   *            <src name>: {
   *              (Builder specific options)
   *            }
   *          }
   *          variants: {
   *            <src name> : {
   *              <var name>: {
   *                (Builder specific options)
   *              }
   *            }
   *          }
   *        }
   *      }
   *      experiments: {
   *      <exset>:{
   *        <ex name>: {
   *          <ch name>: {
   *            <src name>: {
   *              N: {fixed: <bool>, value:<int>}
   *              (Builder spcific options)
   *            }
   *          }
   *        }
   *      }
   *      }
   *      parameters: {
   *        <name>: {
   *          <param name>: {
   *            type: <normal, nuisance,absnorm,relnorm>
   *            initial: <double>
   *            fixed: <bool>
   *            transform:<string>
   *            source: <string>
   *            variable: <string>
   *            min: <double>
   *            max: <double>
   *            step: <double>
   *          }
   *        }
   *      }
   *    }
   *
   *    <variable array>: [ {name:<string>, } ]
   */
  class Analysis : public IAnalysis {
  public:
    Analysis(const std::string& name);

    Analysis(OptionMap& opts);

    Analysis(Analysis&& a);

    ~Analysis();
    
    // ---- Implementation of IAnalysis ----

    virtual const std::string& get_name() const { return this->m_name; }

    virtual ComponentHandler* get_component_handler() const { return this->m_main_context.component_handler(); }

    virtual const OptionMap& get_options() const { return m_options; }

    // ---- Public methods ----

    inline const std::string& name() const { return this->m_name; }

    inline const OptionMap& options() const { return this->m_options; }

    inline Log& log() { return this->m_log; }
    
    inline bool has_channel(const std::string& cname) const { return m_channels.find(cname) != m_channels.end(); }

    AnalysisChannel& channel(const std::string& cname);

    inline const std::map<std::string, AnalysisChannel>& channels() const { return m_channels; }

    /** Set the default sample builder to use */
    inline Analysis& default_sample_builder(ISampleBuilder* sb) {
      m_default_sample_builder = sb;
      return *this;
    }

    template<unsigned _NDims, bool _UseWeight = false>
    SampleSet<_NDims, tbool<_UseWeight>> channel_samples(const std::string& cname, bool sort_by_index = false, bool normalize_vars = true) {
      SampleSet<_NDims, tbool<_UseWeight>> samples(channel_samples(cname));
      if(sort_by_index) samples.sort_by_index();
      if(normalize_vars) samples.normalize_variables(m_log);
      return samples;
    }

    template<unsigned _NDims, bool _UseWeight = false>
    Sample<_NDims, tbool<_UseWeight>>* channel_sample(const std::string& cname, const std::shared_ptr<const DataSource>& ds, bool normalize_vars = true) {
      auto* spl = dynamic_cast<Sample<_NDims, tbool<_UseWeight>>*>(channel_sample(cname, ds));
      if(spl && normalize_vars) spl->normalize(m_log);
      return spl;
    }

    template<unsigned _NDims, bool _UseWeight = false>
    Sample<_NDims, tbool<_UseWeight>>* channel_sample(const std::string& cname, const std::string& dsid, bool normalize_vars = true) {
      auto& ch = channel(cname);
      if(ch.source(dsid)) return channel_sample<_NDims, _UseWeight>(cname, ch.source(dsid), normalize_vars);
      else return 0;
    }
    
    class Experiment {
    public:
      friend class Analysis;
      Experiment(const std::string& name, const OptionMap& opts);
      Experiment(Experiment&& ex);

      inline const std::string& name() const { return m_name; }

      inline const OptionMap& options() const { return m_options; }

      inline bool has_channel(const std::string& ch_name) const {
        return m_options.has_sub(ch_name);
      }
    protected:
      std::string m_name;
      OptionMap m_options;
    };
    
    struct ExperimentSet {
      std::map<std::string, Experiment> experiments;
    };
    
    class ParameterSet {
    public:
      friend class Analysis;

      ParameterSet(const std::string& name);
      ParameterSet(ParameterSet&& ex);

      inline const std::string& name() const { return m_name; }

      class Param {
      public:
        friend class Analysis;

        inline const std::string& name() const { return m_name; }
        inline Parameter::Type type() const { return m_type; }
        inline bool fixed() const { return m_fixed; }
        inline double initial() const { return m_initial; }
        inline bool limited() const { return m_limited; }
        inline const std::string& transform() const { return m_transform; }
        inline const std::string& variable() const { return m_variable; }
        inline double minval() const { return m_minv; }
        inline double maxval() const { return m_maxv; }
        inline const std::string& source() const { return m_source; }
        inline double step() const { return m_step; }
      protected:
        Param();
        std::string m_name, m_transform, m_variable, m_source;
        Parameter::Type m_type;
        double m_initial, m_minv, m_maxv, m_step;
        bool m_fixed;
        bool m_limited;
      };
      using PMap = std::map<std::string,Param>;

      inline PMap::const_iterator begin() const { return m_parameters.begin(); }
      inline PMap::const_iterator end() const { return m_parameters.end(); }

      inline PMap::iterator begin() { return m_parameters.begin(); }
      inline PMap::iterator end() { return m_parameters.end(); }

      inline const Param* operator[](const std::string& n) const {
        auto it = m_parameters.find(n);
        if(it != m_parameters.end()) return &it->second;
        else return 0;
      }

      inline void add(Param&& p) {
        m_parameters.emplace(std::make_pair(p.name(),std::move(p)));
      }
    protected:
      std::string m_name;
      PMap m_parameters;
    };

    /** @return if named parameter set exists */
    inline bool has_parameter_set(const std::string& psetname) const {
      return m_parameter_sets.find(psetname) != m_parameter_sets.end();
    }

    /** @return Named parameter set, throws if no such parameter set is defined */
    const ParameterSet& parameter_set(const std::string& psetname);

    /** @return if named experiment set exists */
    bool has_experiment_set(const std::string& exset) const;
    
    /** @return The experiments in an experiment set */
    std::vector<std::string> experiments_in_set(const std::string& exset) const;

    /** @return If named Experiment is defined */
    bool has_experiment(const std::string& exset, const std::string& exname) const;

    /** @return Named Experiment or throw AnalysisException if undefined */
    const Experiment& experiment(const std::string& exset, const std::string& exname) const;

    /** @return Samples for named experiment */
    SampleMap experiment_samples(const std::string& exset, const std::string& exname, bool normalize_vars = true);

    /** @return Reference to the main context */
    inline Context& main_context() { return m_main_context; }

    /** @return Reference to the main pool of samples */
    inline MemoryPool& memory_pool() { return m_main_context.memory_pool(); }

    /** Create an instance using the main context */
    template<class _T, class ... _Args>
    inline _T* create_object(_Args&& ... args) {
      return memory_pool().pool<_T>()->get(std::forward<_Args>(args)...);
    }

    /**
     * Add a transform, it must have a valid and unique name
     * @return true if all went well
     */
    bool add_transform(ITransform*);

    /** @return Named transform */
    ITransform* transform(const std::string& name);

    /**
     * If a parameter is already defined, update
     * If the parameter meta is given, look up the initial values in that OptionMap using the "source" attribute.
     */
    bool build_parameters(const ParameterSet& pset, Parameters&, const OptionMap* meta = 0);

    /** @return Named set of Parameters */
    inline Parameters build_parameters(const std::string& pset_name, const OptionMap* meta = 0) {
      Parameters params;
      build_parameters(parameter_set(pset_name), params, meta);
      return params;
    }
#ifdef SFI_ENABLE_MP
    inline mp::Dispatcher& dispatcher() { return m_dispatcher; }
#endif
  protected:
    Analysis(const Analysis&) = delete;

    ISampleBuilder* channel_sample_builder(const std::string& cname) const;

    std::vector<ISample*> channel_samples(const std::string& cname);

    ISample* channel_sample(const std::string& cname, const std::shared_ptr<const DataSource>& src);

    ISample* channel_source_sample(ISampleBuilder* sbuilder, const std::shared_ptr<const DataSource>& src, AnalysisChannel& cdef);

    ISample* experiment_channel_sample(AnalysisChannel& ch, Experiment& expr);

    void init_channels();

    /** Add one channel defined in options */
    void init_channel(const std::string& chname, OptionMap& omap);

    /** Add a variant to a source */
    void init_variant(const std::string& varname, AnalysisChannel& ch, AnalysisDataSource& src, const OptionMap* varmap);

    AnalysisChannel& add_channel(const std::string& name, ISampleBuilder* sb, const PVariables &vars);

    Analysis& add_source(const std::string& channel_name, const std::string& name, const std::shared_ptr<DataSource>& es);

    void init_experiments();

    void init_parameter_sets();

    std::string m_name;
    OptionMap m_options;
    ISampleBuilder* m_default_sample_builder;
    std::map<std::string, AnalysisChannel> m_channels;
    std::map<std::string, ExperimentSet> m_experiment_sets;
    std::map<std::string, ParameterSet> m_parameter_sets;
    std::map<std::string, ITransform*> m_transforms;
    Context m_main_context;
    Log m_log;
#ifdef SFI_ENABLE_MP
    mp::Dispatcher m_dispatcher;
#endif
  };
}
#endif // sfi_Analysis_h
