#ifndef sfi_SampleBuilderBase_h
#define sfi_SampleBuilderBase_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string>

#include <ISampleBuilder.h>
#include <Log.h>

namespace sfi {
  class SampleBuilderBase : public ISampleBuilder {
  public:
    virtual ~SampleBuilderBase();

    // ---- implementation of IComponent ----

    virtual const std::string& get_type() const;

    virtual const std::string& get_name() const;

    virtual OptionMap& get_options();

    virtual const OptionMap& get_options() const;

    // ---- implementation of ISampleBuilder ----

    virtual int get_dims() const { return m_dims; }

    virtual bool get_is_weighted() const { return m_is_weighted; }

    virtual std::shared_ptr<DataSource> get_data_source(const OptionMap& opts) const;

    /** @return Variables if the given source has variables defined, else nullptr */
    virtual PVariables get_variables(const DataSource& ds) const { return variables(ds); }
  protected:
    SampleBuilderBase(Context& ctx, const OptionMap& opts);

    void bind_options();

    ISample* create_sample(Context& ctx, unsigned dims, bool use_weight, unsigned nevents);

    PVariables initialize_variables(const OptionMap& opts) const;

    /** Determine Variables for DataSource */
    PVariables variables(const DataSource& src) const {
      auto srcvars = initialize_variables(src.options());
      return srcvars ? srcvars : m_variables;
    }

    Log m_log;
    std::string m_name, m_type_id;
    OptionMap m_options;
    PVariables m_variables;
    int m_dims;
    bool m_is_weighted;
  };
}

#endif // sfi_SampleBuilderBase_h
