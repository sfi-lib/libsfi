#ifndef sfi_IAnalysis_h
#define sfi_IAnalysis_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string>
#include "Options.h"

namespace sfi {
  class ComponentHandler;

  class IAnalysis {
  public:
    virtual ~IAnalysis() {}

    virtual const std::string& get_name() const = 0;

    virtual ComponentHandler* get_component_handler() const = 0;

    virtual const OptionMap& get_options() const = 0;
  };

}
#endif // sfi_IAnalysis_h
