/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iomanip>

#include "MatrixOp.h"

extern "C" {
  int dsyevd_(char*, char*, int*, double*, int*, double*, double*, int*, int*, int*, int*);
  int dpotrf_(char*, int*, double*, int*, int*);
  int dpotri_(char*, int*, double*, int*, int*);
  int dgelsd_(int*, int*, int*, double*, int*, double*, int*, double*, double*, int*, double*, int*, int*, int*);
}

using namespace sfi;

MatrixSymEigenDecomposition::MatrixSymEigenDecomposition(bool compute_eigen_vectors, bool order_descending):
    m_eigenvectors(0), m_compute_eigen_vectors(compute_eigen_vectors), m_order_descending(order_descending) {
}

void
MatrixSymEigenDecomposition::rows(size_t r) {
  m_eigenv.rows(r);
  if(m_compute_eigen_vectors) m_eigenvectors.rows(r);
}

bool
MatrixSymEigenDecomposition::decompose(MatrixSym<double>& m) {
  if(m.rows() != m_eigenv.rows()) {
    if(m_eigenv.rows() > 0) {
      std::cout<<"decompose() Warning: matrix has "<<m.rows()<<" rows, expected "<<m_eigenv.rows()<<" resizing!"<<std::endl;
    }
    if(m_compute_eigen_vectors) m_eigenvectors.rows(m.rows());
    m_eigenv.rows(m.rows());
    m_work.rows(0);
    m_iwork.rows(0);
  }
  char jobz = 'N';
  char uplo = 'U';
  int N = m.rows();
  int lda = m_compute_eigen_vectors ? m_eigenvectors.row_length() : m.row_length();
  int lwork = -1;
  int liwork = -1;
  int info = 0;
  double* data = 0;
  if(m_compute_eigen_vectors) {
    jobz = 'V';
    lda = m_eigenvectors.row_length();
    data = m_eigenvectors.data_ptr();
    m_eigenvectors = m;
  }
  else {
    jobz = 'N';
    lda = m.row_length();
    data = m.data_ptr();
  }

  if(m_work.size() == 0) {
    // query about the optimal sizes of work arrays
    double work0;
    int iwork0;
    dsyevd_(&jobz, &uplo, &N, data, &lda, m_eigenv.data_ptr(), &work0, &lwork, &iwork0, &liwork, &info);
    if(info == 0) {
      lwork = work0;
      liwork = iwork0;
      m_work.rows(lwork);
      m_iwork.rows(liwork);
    }
    else {
      std::cout<<"decompose() Failed to compute work array sizes, INFO="<<info<<std::endl;
      return false;
    }
  }
  else {
    lwork = m_work.size();
    liwork = m_iwork.size();
  }
  dsyevd_(&jobz, &uplo, &N, data, &lda, m_eigenv.data_ptr(), m_work.data_ptr(), &lwork, m_iwork.data_ptr(), &liwork, &info);
  if(info == 0) {
    if(m_compute_eigen_vectors) {
      if(m_order_descending) m_eigenvectors.reverse_rows();
      m_eigenvectors.transpose();
    }
    if(m_order_descending) this->m_eigenv.reverse();
    return true;
  }
  else {
    std::cout<<"decompose() Failed INFO="<<info<<std::endl;
    return false;
  }
}

template<>
bool Cholesky<double>::decompose(const MatrixSym<double>& m) {
  Base::operator =(m);
  char uplo = 'U';
  int nrows = m.rows();
  int lda = this->row_length();
  int info = 0;
  dpotrf_(&uplo, &nrows, this->data_ptr(), &lda, &info);
  return m_status = (info == 0);
}

namespace sfi {
  svd_solve_result svd_solve(const Matrix<double>& m, const Vector<double>& y, double tolerance, Vector<double>& x) {
    svd_solve_result res;

    Matrix<double> A(m.cols(), m.rows());
    int M = m.rows();
    int N = m.cols();
    for(int r=0; r<N; ++r) {
      for(int c=0; c<M; ++c) A(r,c) = m(c,r);
    }
    int Nrhs = 1;
    int lda = m.rows();
    x = y;
    int ldb = x.rows();
    Vector<double> singular_values(std::min(M,N));
    Vector<double> work;
    int lwork = -1;
    Vector<int> iwork;
    int liwork = 0;
    int info = 0;
    {
      double work0 = 0;
      int iwork0 = 0;
      dgelsd_(&M, &N, &Nrhs, A.data_ptr(), &lda, x.data_ptr(), &ldb, singular_values.data_ptr(),
          &tolerance, &res.rank, &work0, &lwork, &iwork0, &info);
      if(info != 0) {
        std::cout<<"svd_solve() failed to compute worspace size INFO="<<info<<std::endl;
        return res;
      }
      lwork = work0;
      liwork = iwork0;
    }
    work.rows(lwork);
    iwork.rows(liwork);
    dgelsd_(&M, &N, &Nrhs, A.data_ptr(), &lda, x.data_ptr(), &ldb, singular_values.data_ptr(),
        &tolerance, &res.rank, work.data_ptr(), &lwork, iwork.data_ptr(), &info);
    if(info != 0) {
      std::cout<<"svd_solve() failed to solve INFO="<<info<<std::endl;
      return res;
    }
    x.rows(res.rank);
    res.status = true;
    res.condnr = singular_values[0]/singular_values[res.rank - 1];
    return res;
  }

  svd_solve_result svd_solve(const Matrix<double>& m, Matrix<double>& y, double tolerance) {
    svd_solve_result res;
    if(m.rows() != y.cols()) {
      std::cout<<"svd_solve() Invalid sizes"<<std::endl;
      return res;
    }

    Matrix<double> A(m.cols(), m.rows());
    int M = m.rows();
    int N = m.cols();
    int Nrhs = y.rows();
    for(int r=0; r<N; ++r) {
      for(int c=0; c<M; ++c) A(r,c) = m(c,r);
    }
    int lda = m.rows();
    int ldb = y.cols();
    Vector<double> singular_values(std::min(M,N));
    Vector<double> work;
    int lwork = -1;
    Vector<int> iwork;
    int liwork = 0;
    int info = 0;
    {
      double work0[1] = {0};
      int iwork0[1] = {0};
      dgelsd_(&M, &N, &Nrhs, A.data_ptr(), &lda, y.data_ptr(), &ldb, singular_values.data_ptr(),
          &tolerance, &res.rank, work0, &lwork, iwork0, &info);
      if(info != 0) {
        std::cout<<"svd_solve() failed to compute worspace size INFO="<<info<<std::endl;
        return res;
      }
      lwork = work0[0];
      liwork = iwork0[0];
      if(liwork == 0) {
        // Workaround, when liwork
        liwork = 3 * std::min(M,N) * std::min(M,N) + 11 * std::min(M,N);
      }
    }
    work.rows(lwork);
    iwork.rows(liwork);

    dgelsd_(&M, &N, &Nrhs, A.data_ptr(), &lda, y.data_ptr(), &ldb, singular_values.data_ptr(),
        &tolerance, &res.rank, work.data_ptr(), &lwork, iwork.data_ptr(), &info);
    if(info != 0) {
      std::cout<<"svd_solve() failed to solve INFO="<<info<<std::endl;
      return res;
    }
    res.status = true;
    res.condnr = singular_values[0]/singular_values[res.rank - 1];
    return res;
  }

  rope_invert_result
  rope_regularization(MatrixSymEigenDecomposition& ed, Vector<double>& eigenv, MatrixSym<double>& icov, double rho, bool prune_eigen, bool inverse) {
    rope_invert_result res{false};
    auto& Q = ed.eigen_vectors();
    const unsigned N(Q.rows());
    eigenv.rows(N);
    icov.rows(N);

    const double eigen_limit = sqrt(0.001*8.0*rho);

    auto &reigenv = ed.eigen_values();
    MatrixDia<double> D(eigenv);
    res.det = res.orig_det = 1.0;
    res.nvalues = 1;

    res.Tr_orig = res.Tr_reg = 0.0;

    for(unsigned i=0; i<N; ++i) {
      const double evi = reigenv(i);
      res.orig_det *= evi;
      res.Tr_orig += evi;
      if(!prune_eigen || (evi/reigenv(0) >= eigen_limit)) {
        if(inverse) D(i,i) = 2.0/(evi + sqrt(square(evi) + 8.0*rho));
        else D(i,i) = 0.5*(evi + sqrt(square(evi) + 8.0*rho));
        res.Tr_reg += 0.5*(evi + sqrt(square(evi) + 8.0*rho));
        res.det *= D(i,i);
        res.nvalues = i + 1;
      }
      else D(i,i) = 0.0;
    }
    Q.lrmult(D, icov, res.nvalues);
    eigenv.rows(res.nvalues);
    res.status = true;
    return res;
  }

  void print_matrix_code(const MatrixSqr<double>& a) {
    std::ostringstream sout;
    sout<<std::setprecision(16);
    std::scientific(sout);
    for(unsigned r=0; r<a.rows(); ++r) {
      sout<<"m.row("<<r<<", {";
      for(unsigned c=0; c<a.cols(); ++c ) {
        if(c) sout<<", ";
        sout<<a(r,c);
      }
      sout<<"});"<<std::endl;
    }
    std::cout<<sout.str();
  }

  void print_matrix_code(const Matrix<double>& a) {
    std::ostringstream sout;
    sout<<std::setprecision(16);
    std::scientific(sout);
    for(unsigned r=0; r<a.rows(); ++r) {
      sout<<"m.row("<<r<<", {";
      for(unsigned c=0; c<a.cols(); ++c ) {
        if(c) sout<<", ";
        sout<<a(r,c);
      }
      sout<<"});"<<std::endl;
    }
    std::cout<<sout.str();
  }

  void print_matrix_code(const Vector<double>& a) {
    std::ostringstream sout;
    sout<<std::setprecision(16);
    std::scientific(sout);
    sout<<"v = {";
    for(unsigned r=0; r<a.size(); ++r) {
      if(r) sout<<", ";
      sout<<a[r];
    }
    sout<<"};"<<std::endl;
    std::cout<<sout.str();
  }


  void print_matrix_code(const MatrixSym<double>& a, unsigned offs) {
    for(unsigned r=offs; r<a.rows(); ++r) {
      for(unsigned c=offs; c<a.rows(); ++c ) {
        std::cout<<"m("<<(r-offs)<<", "<<(c-offs)<<") = "<<a(r,c)<<";"<<std::endl;
      }
    }
  }

  bool invert_matrix(int nrows, MatrixSym<double>& m) {
    char uplo = 'U';
    int lda = m.row_length();
    int info = 0;
    dpotrf_(&uplo, &nrows, m.data_ptr(), &lda, &info);
    if(info == 0) {
      dpotri_(&uplo, &nrows, m.data_ptr(), &lda, &info);
    }
    return info == 0;
  }
}
