/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ComponentHandler.h"

#include <dlfcn.h>

using namespace sfi;

using RawFunction = IComponent*(*)(Context&, OptionMap&);

ComponentHandler::ComponentHandler(Context& ctx):m_log(Log::get(ctx, "ComponentHandler")) {}

IComponent*
ComponentHandler::component(Context& ctx, const std::string& id, const std::string& libn, OptionMap& opts) {
  auto it = m_component_factories.find(id);
  if(it != m_component_factories.end()) {
    do_log_debug("component() found cached factory for id '"<<id<<"'");
    IComponent* res = it->second(ctx, opts);
    do_log_debug("component() id '"<<id<<"' created "<<(void*)res);
    return res;
  }
  else {
    std::string sym = "sfi_factory_"+id;
    void* f_func = 0;
    if(libn.empty()) f_func = dlsym(RTLD_DEFAULT, sym.c_str());
    else {
      void* libhandle = 0;
      auto lib = m_libraries.find(libn);
      if(lib != m_libraries.end()) libhandle = lib->second;
      else {
        libhandle = dlopen(libn.c_str(), RTLD_NOW);
        if(!libhandle) {
          do_log_error("component() Unable to open library '"<<libn<<"'");
          return 0;
        }
        m_libraries[libn] = libhandle;
        do_log_info("component() loaded library '"<<libn<<"'");
      }
      f_func = dlsym(libhandle, sym.c_str());
    }
    if(f_func) {
      FactoryFunc ff((RawFunction)f_func);
      m_component_factories[id] = ff;
      IComponent* res = ff(ctx, opts);
      if(res) {
        do_log_debug("component() id '"<<id<<"' created "<<(void*)res);
      }
      else {
        do_log_error("component() factory returned null for component with id '"<<id<<"'");
      }

      return res;
    }
    else {
      do_log_error("component() unable to create component with id '"<<id<<"'");
      return 0;
    }
  }
}
