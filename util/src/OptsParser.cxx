/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "OptsParser.h"

using namespace sfi;

inline bool is_delim(char c) {
  // TODO: might include more
  return (c == '\"') || (c == '{') || (c == '}') || (c == '[') || (c == ']') || (c == ',') || (c == ':') || (c == '<') || (c == '>') || (c == '@') || (c == '(') || (c == ')');
}

OptsParser::OptsParser(const std::string& text, OptionMap* root):
            m_text(text), m_pos(0), m_N(text.length()),
            m_state({Begin}), m_opts({root}), m_ext_opts(true) {
}

bool
OptsParser::parse() {
  std::string name, next_token;
  while(m_pos < m_N) {
    if(m_state.empty()) throw OptionException("(begin) internal error @"+std::to_string(m_pos)+" No state!");
    const char cc = next_t(next_token);
    const opts_state st = m_state.back();
    if((st == Begin)  && (cc == 0)) {
      /* EOT ok */
      break;
    }

    if(m_opts.empty()) throw OptionException("(begin) internal error @"+std::to_string(m_pos)+" No option!");

    Option* op = m_opts.back();
    std::string states;
    for(auto& _st : m_state) states += std::to_string(_st)+" ";
    std::ostringstream ops;
    for(auto& _op : m_opts) ops<<((void*)_op)<<" ";

    if(st == Begin) {
      if(cc == '{') m_state.push_back(InObject);
      else if(cc == 0) { /* EOT ok */ }
      else throw OptionException("(begin) syntax error @"+std::to_string(m_pos)+" unexpected "+std::to_string((int)cc)+" '"+cc+"' : "+text_around_error(m_pos));
    }
    else if(st == InObject) {
      auto* obj = dynamic_cast<OptionMap*>(op);
      if((obj == 0) || (op->get_type() != Option::Object)) throw OptionException("(object) internal error @"+std::to_string(m_pos)+" state is not object : "+text_around_error(m_pos));
      if(cc == '\"') {
        m_state.push_back(BeginName);
        name = next_token;
      }
      else if(cc == '}') {
        m_state.pop_back();
        m_opts.pop_back();
      }
      else if(cc == '>') {
        m_state.pop_back();
        m_opts.pop_back();
        if(!define_option(dynamic_cast<OptionMap*>(op))) throw OptionException("syntax error @"+std::to_string(m_pos)+" : "+text_around_error(m_pos));
      }
      else if(cc == ',') {
        if(obj->get_value().empty()) throw OptionException("syntax error @"+std::to_string(m_pos)+" unexpected ',' : "+text_around_error(m_pos));
      }
      else if(m_ext_opts && std::isalpha(cc)) {
        // Allow unquoted identifiers in extended Opts
        m_state.push_back(BeginName);
        name = next_token;
      }
      // TODO: This is an extension
      else if(cc == '@') {
        m_state.push_back(BeginGen);
        m_opts.push_back(new OptionMap("__gen__"));
      }
      else throw OptionException("(begin) syntax error @"+std::to_string(m_pos)+" unexpected " + std::to_string((int)cc)+" '"+cc+"' : "+text_around_error(m_pos));
    }
    else if(st == BeginVector) {
      OptionVec* vec = dynamic_cast<OptionVec*>(op);
      if((vec == 0) || (op->get_type() != Option::Vector)) throw OptionException("(vector) internal error @"+std::to_string(m_pos)+" state is not vector");
      if(cc == ']') {
        m_state.pop_back();
        m_opts.pop_back();
        if(!add_vector(vec)) return false;
      }
      else if(cc == ',') {
        if(vec->get_value().empty()) throw OptionException("(vector) syntax error @"+std::to_string(m_pos)+" unexpected " + std::to_string((int)cc)+" '"+cc+"' : "+text_around_error(m_pos));
      }
      else if((cc == ':') || (cc == '}') || (cc == '<') || (cc == '>')) throw OptionException("(vector) syntax error @"+std::to_string(m_pos)+" unexpected " + std::to_string((int)cc)+" '"+cc+"' : "+text_around_error(m_pos));
      else {
        m_state.push_back(BeginValue);
        if(!parse_value(cc, op, "", next_token)) return false;
      }
    }
    else if(st == BeginName) {
      if(cc == ':') m_state.back() = BeginValue;
      else throw OptionException("(name) syntax error @"+std::to_string(m_pos)+" unexpected " + std::to_string((int)cc)+" '"+cc+"' : "+text_around_error(m_pos));
    }
    else if(st == BeginValue) {
      if(!parse_value(cc, op, name, next_token)) return false;
      name = "";
    }
    else if(st == BeginGen) {
      //log_debug(m_log, "parse() (ext) gen "<<m_pos<<" cc='"<<cc<<"' '"<<next_token<<"'");
      auto* obj = dynamic_cast<OptionMap*>(op);
      if(!obj) throw OptionException("(gen) internal error, no opt map!");
      if(std::isalpha(cc)) {
        if(!obj->has_type<std::string>("_type"))  {
          if(next_token != "range") throw OptionException("(gen) syntax error @"+std::to_string(m_pos)+" invalid generator type '"+next_token+"'");
          else obj->set("_type", next_token);
        }
        else throw OptionException("(gen) syntax error @"+std::to_string(m_pos)+" unexpected " + std::to_string((int)cc)+" '"+cc+"' : "+text_around_error(m_pos));
      }
      else if(cc == '(') {
        m_state.push_back(GenArgs);
      }
      else if(cc == ':') {
        m_state.push_back(BeginValue);
        name = "_gen";
        if(obj->has_impl("_gen_values") && ((*obj)["_gen_values"].get_type() == Option::Vector)) {
          std::string vname = obj->get<std::string>("_name");
          std::vector<double> ovec;
          obj->get_if("_gen_values", ovec);
          obj->set(vname, ovec[0]);
          // Ugly, this is how variables are substituted
          obj->option(vname)->help("__gen_"+vname);
          // log_debug(m_log, "parse() (gen) generating: "<<*obj<<" @ "<<(void*)obj->option(vname));
        }
        else throw OptionException("(gen) Invalid option value for generator");
      }
      else if(cc == '@') {
        std::string vname = obj->get<std::string>("_name");
        std::vector<double> ovec;
        obj->get_if("_gen_values", ovec);
        auto& _gen = obj->sub("_gen");
        // log_debug(m_log, "parse() (ext) end gen var="<<vname<<" values: "<<ovec<<" gen: "<<_gen);

        m_state.pop_back();
        m_opts.pop_back();

        OptionMap* parent = dynamic_cast<OptionMap*>(m_opts.back());
        unsigned index = 0;
        for(auto& val : ovec) {
          auto* nop = new OptionImpl<double>(val);
          nop->help("__gen_"+vname);
          for(auto gop : _gen) {
            auto* cgop = gop.second->clone();
            replace_option(cgop, nop);
            std::string opname = gop.first+"_"+std::to_string(index);
            //log_verbose(m_log, "parse() (ext) '"<<opname<<"' "<<*cgop);
            parent->add(opname, cgop);
          }
          ++index;
        }
        // log_debug(m_log, "parse() (ext) parent @"<<(void*)parent<<" : "<<*parent);
        delete obj;
      }
      else throw OptionException("(gen) syntax error @"+std::to_string(m_pos)+" unexpected " + std::to_string((int)cc)+" '"+cc+"' : "+text_around_error(m_pos));
    }
    else if(st == GenArgs) {
      auto* obj = dynamic_cast<OptionMap*>(op);
      if(!obj) throw OptionException("(gen) internal error, no opt map!");
      if(cc == ')') {
        m_state.pop_back();
      }
      else if(cc == ',') {
        m_state.push_back(BeginValue);
        name = "_gen_values";
      }
      else if(std::isalpha(cc)) {
        obj->set("_name", next_token);
      }
      else throw OptionException("(gen) syntax error @"+std::to_string(m_pos)+" unexpected " + std::to_string((int)cc)+" '"+cc+"' : "+text_around_error(m_pos));
    }
  }
  return true;
}

bool
OptsParser::parse_value(char cc, Option* op, const std::string& name, const std::string& next_token) {
  bool res = true;
  const bool in_obj = op->get_type() == Option::Object;
  if(in_obj && name.empty()) throw OptionException("parse_value() internal error a) @"+std::to_string(m_pos)+" name is empty");
  if(cc == '[') {
    // added at ']'
    m_state.back() = BeginVector;
    m_opts.push_back(new OptionVec(in_obj ? name : ""));
  }
  else if((cc == '{') || (cc == '<')) {
    m_state.back() = InObject;
    auto* opm = dynamic_cast<OptionMap*>(op);
    if(opm) {
      if(!opm->has_impl(name)) {
        auto *nopm = new OptionMap(name);
        opm->add(name, nopm);
        m_opts.push_back(nopm);
      }
      else {
        auto* nopm = dynamic_cast<OptionMap*>(opm->option(name));
        if(!nopm) {
          if(cc == '{') throw OptionException("parse_value() internal error '"+name+"' is not an object");
          else {
            m_opts.push_back(new OptionMap(name));
          }
        }
        else {
          m_opts.push_back(nopm);
        }
      }
    }
    else {
      auto* opv = dynamic_cast<OptionVec*>(op);
      if(opv) {
        auto *nopm = new OptionMap();
        opv->add(nopm);
        m_opts.push_back(nopm);
      }
      else throw OptionException("parse_value() internal error '"+name+"' is not a vector");
    }
  }
  // strings may be empty
  else if(cc == '\"') {
    m_state.pop_back();
    res = add_value(op, next_token, name);
  }
  else if(!next_token.empty()) {
    m_state.pop_back();
    unsigned uival;
    int ival;
    double dval;
    if(try_parse_uint(next_token, uival)) res = add_value(op, uival, name);
    else if(try_parse_int(next_token, ival)) res = add_value(op, ival, name);
    else if(try_parse_double(next_token, dval)) res = add_value(op, dval, name);
    else if((next_token == "true") || (next_token == "false")) res = add_value(op, (next_token == "true"), name);
    else {
      if(!m_ext_opts) throw OptionException("parse_value() syntax error b) @"+std::to_string(m_pos)+" unexpected '"+next_token+"' : "+text_around_error(m_pos));
      auto* opt0 = locate_named_option(next_token);
      if(!opt0) throw OptionException("parse_value() error b) @"+std::to_string(m_pos)+" option '"+next_token+"' not found : "+text_around_error(m_pos));
      auto* opm = dynamic_cast<OptionMap*>(op);
      if(!opm) {
        auto* ovec = dynamic_cast<OptionVec*>(op);
        if(!ovec) throw OptionException("parse_value() error c) @"+std::to_string(m_pos)+" unexpected '"+next_token+"' no parent! : "+text_around_error(m_pos));
        ovec->add(opt0);
      }
      else if(opm->has_impl(name)) {
        auto* opt = opm->option(name);
        if(opt->get_type_info() == opt0->get_type_info()) {
          opt->set_value(opt0->get_value_address());
        }
        else throw OptionException("parse_value() error d) @"+std::to_string(m_pos)+" defined with type '"+type_name(opt->get_type_info())+"' : "+text_around_error(m_pos));
      }
      else opm->add(name, opt0);
    }
  }
  else throw OptionException("parse_value() syntax error e) @"+std::to_string(m_pos)+" unexpected " + std::to_string((int)cc)+" '"+cc+"' : "+text_around_error(m_pos));
  if(!res) throw OptionException("parse_value() syntax error e) @"+std::to_string(m_pos)+" : "+text_around_error(m_pos));
  return res;
}

Option*
OptsParser::locate_named_option(const std::string& on) {
  for(auto oit = m_opts.rbegin(); oit != m_opts.rend(); ++oit) {
    auto* opm0 = dynamic_cast<OptionMap*>(*oit);
    if(opm0) {
      auto* opt0 = opm0->suboption(on);
      if(opt0) return opt0;
    }
  }
  return 0;
}

char
OptsParser::next_t(std::string& next_token) {
  next_token = "";
  if(remove_space()) return 0;
  char res = m_text[m_pos];
  if(res == '\"') return next_str(next_token);
  else if(res == '#') {
    while((m_pos < m_N) && (m_text[m_pos] != '\r') && (m_text[m_pos] != '\n')) {
      ++m_pos;
    }
    return next_t(next_token);
  }
  else if(is_delim(res)) ++m_pos;
  else {
    size_t bpos = m_pos;
    while((m_pos < m_N) && (!std::isspace(m_text[m_pos])) && !is_delim(m_text[m_pos])) ++m_pos;
    if(m_pos == m_N) res = 0;
    else {
      next_token = m_text.substr(bpos, m_pos - bpos);
      remove_space();
    }
  }
  return res;
}

/** eat all from " -> " */
char
OptsParser::next_str(std::string& next_token) {
  if(m_text[m_pos] != '\"') throw OptionException("next_str() expected '\"' @"+std::to_string(m_pos)+" : "+text_around_error(m_pos));
  char res = '\"';
  size_t bpos = m_pos;
  //log_debug(m_log, "next_str() @"<<m_pos<<" '"<<m_text[m_pos]<<"'");
  ++m_pos;
  while((m_pos < m_N) && (m_text[m_pos] != '\"')) {
    if((m_text[m_pos] == '\\') && (m_pos < (m_N-1)) && (m_text[m_pos+1] == '\"')) m_pos += 2;
    else ++m_pos;
  }
  if(m_pos == m_N) res = 0;
  else {
    next_token = m_text.substr(bpos + 1, m_pos - bpos - 1);
    //log_debug(m_log, "next_str() @"<<m_pos<<" '"<<next_token<<"'");
    ++m_pos;
  }
  return res;
}

bool
OptsParser::define_option(OptionMap* opm) {
  if(!opm) return false;
  OptionMap* parent = m_opts.size() ? dynamic_cast<OptionMap*>(m_opts.back()) : 0;
  if(!parent) return false;
  std::string name = opm->name();
  bool is_req = opm->has_type<bool>("required") && opm->get<bool>("required");
  std::string help = opm->has_type<std::string>("help") ? opm->get<std::string>("help") : "";
  Option* oop = parent->option(name);
  if(oop != opm) {
    // option exists
    // TODO: check type
    oop->help(help).is_required(is_req);
    delete opm;
  }
  else {
    if(opm->has("default")) {
      Option* op = opm->option("default")->clone();
      op->name(name).is_required(is_req).help(help);
      parent->remove(name);
      parent->add(name, op);
    }
    else if(opm->has_type<std::string>("type")) {
      std::string type = opm->get<std::string>("type");
      parent->remove(name);
      if(type == "int") parent->define_impl<int>(name, is_req, help);
      else if(type == "unsigned") parent->define_impl<unsigned>(name, is_req, help);
      else if(type == "double") parent->define_impl<double>(name, is_req, help);
      else if(type == "bool") parent->define_impl<bool>(name, is_req, help);
      else if(type == "string") parent->define_impl<std::string>(name, is_req, help);
      else {
        return false;
      }
    }
    else {
      return false;
    }
  }
  return true;
}

bool
OptsParser::add_vector(OptionVec* vec) {
  auto& opv = vec->get_value();
  bool replaced = true;
  if(!opv.empty()) {
    //log_debug(m_log, "parse() (vector) adding a) @"<<m_pos<<" "<<type_name(opv[0]->get_type_info()));
    if(opv[0]->get_type_info() == typeid(unsigned)) {
      replaced = replace_value_vector<unsigned>(vec);
    }
    else if(opv[0]->get_type_info() == typeid(int)) {
      replaced = replace_value_vector<int>(vec);
    }
    else if(opv[0]->get_type_info() == typeid(double)) {
      replaced = replace_value_vector<double>(vec);
    }
    else if(opv[0]->get_type_info() == typeid(std::string)) {
      replaced = replace_value_vector<std::string>(vec);
    }
    else if(opv[0]->get_type_info() == typeid(bool)) {
      replaced = replace_value_vector<bool>(vec);
    }
    else replaced = false;
  }
  else replaced = false;
  if(!replaced) {
    //log_debug(m_log, "parse() (vector) adding c) @"<<m_pos<<" :"<<*vec);

    auto* opm = dynamic_cast<OptionMap*>(m_opts.back());
    if(opm) opm->add(vec->name(), vec);
    else {
      if(m_opts.back()->get_type() == Option::Vector) {
        dynamic_cast<OptionVec*>(m_opts.back())->add(vec);
      }
      else throw OptionException("add_vector() internal error @"+std::to_string(m_pos)+" state is not vector");
    }
  }
  return true;
}

std::string
OptsParser::text_around_error(size_t pos) {
  size_t bp = (pos > 10) ? pos-10 : 0;
  size_t ep = ((pos + 10) < m_N) ? pos + 10 : m_N;
  return m_text.substr(bp, ep - bp);
}

void
OptsParser::replace_option(Option* _opm, Option* new_op) {
  auto* opm = dynamic_cast<OptionMap*>(_opm);
  if(opm) {
    if(!opm->replace(new_op)) throw OptionException("replace_option() error @"+std::to_string(m_pos)+" unable to replace variable '"+new_op->name()+"'");
  }
  else {
    if(new_op->help() == _opm->help()) {
      *_opm = *new_op;
    }
    else throw OptionException("replace_option() error @"+std::to_string(m_pos)+" no OptionMap '"+_opm->name()+"'");
  }
}
