/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "LatexTable.h"

#include <sstream>
#include <iomanip>

using namespace sfi;

LatexTable::LatexTable(const std::string& name, unsigned ncols, unsigned nrows):
    m_name(name),
    m_col_titles(ncols, ""),
    m_row_titles(nrows, ""),
    m_data(nrows, ncols),
    m_precision(6) {
}

LatexTable&
LatexTable::rows(size_t rows) {
  m_data.rows(rows);
  m_row_titles.resize(rows, "");
  return *this;
}

std::ostream&
LatexTable::print(std::ostream& out) const {
  out<<"\\begin{table}[!hbp]"<<std::endl;
  out<<"\\begin{tabular}{l ";
  for(unsigned c=0; c<m_col_titles.size(); ++c) out<<" r @{.} l ";
  out<<"}"<<std::endl;
  out<<"\\hline"<<std::endl;
  out<<" ";
  for(auto& ct : m_col_titles) out<<" & \\multicolumn{2}{c}{"<<ct<<"} ";
  out<<"\\\\"<<std::endl;
  out<<"\\hline"<<std::endl;
  for(unsigned r=0; r<m_row_titles.size(); ++r) {
    out<<m_row_titles[r]<<" "<<std::endl;
    for(unsigned c=0; c<m_col_titles.size(); ++c) {
      std::ostringstream sout;
      sout<<std::fixed<<std::setprecision(m_precision)<<m_data(r,c);
      std::string datastr(sout.str());
      for(auto& c : datastr) {
        if(c == '.') c = '&';
      }
      out<<" & "<<datastr<<" ";
    }
    out<<"\\\\"<<std::endl;
  }
  out<<"\\hline"<<std::endl;
  out<<"\\end{tabular}"<<std::endl;
  out<<"\\caption{"<<m_caption<<"\\label{"<<m_name<<"}}"<<std::endl;
  out<<"\\end{table}"<<std::endl;

  return out;
}
