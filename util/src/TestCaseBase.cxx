/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "TestCaseBase.h"
#include "sfi_math.h"
#include "timer.h"

using namespace sfi::test;
using namespace sfi;

TestCaseBase::TestCaseBase(Context& ctx, const std::string& name):
    m_log(Log::get(ctx, "TestCase("+name+")")), m_context(ctx), m_name(name), m_current_entry(0),
    m_summary{0,0,0,0} {
}

TestCaseBase::~TestCaseBase() {
}

// ---- Implementation of IComponent -----

const std::string& TestCaseBase::get_type() const { return m_name; }

const OptionMap& TestCaseBase::get_options() const { return m_options; }

OptionMap& TestCaseBase::get_options() { return m_options; }

// ---- Implementation of ITestCase -----

const std::string& TestCaseBase::get_name() const { return m_name; }

bool
TestCaseBase::do_initialize() { return true; }

bool
TestCaseBase::do_run() {
  m_summary = TestCaseSummary{0,0,0,0};
  do_log_info("run() Running test case '"<<m_name<<"'");
  unsigned nfailed(0);
  for(auto tb(m_entries.begin()), te(m_entries.end()); tb != te; ++tb) {
    try {
      do_log_info("run() Running test '"<<tb->name<<"'");
      m_current_entry = &*tb;
      Timer<true> ti;
      ti.start();
      tb->func();
      double time = ti.stop();
      m_summary.nunits += tb->ntests;
      m_summary.nfailed_units += tb->errors;
      if(tb->errors) {
        do_log_error("run() Test '"<<tb->name<<"' failed with "<<tb->errors<<" errors of "<<tb->ntests<<" in "<<time<<" s");
        ++nfailed;
      }
      else {
        do_log_info("run() Test '"<<tb->name<<"' ran "<<tb->ntests<<" units in "<<time<<" s");
      }
    }
    catch(const std::exception& ex) {
      do_log_error("run() Exception in test case '"<<tb->name<<"' : "<<ex.what());
      tb->exception = true;
      ++nfailed;
    }
  }
  if(nfailed) {
    do_log_error("run() Test case '"<<m_name<<"' finished with "<<nfailed<<" failed tests.");
  }
  else {
    do_log_ok("run() Test case '"<<m_name<<"' finished with no failed tests.");
  }
  m_summary.ntests = m_entries.size();
  m_summary.nfailed_tests = nfailed;
  m_current_entry = 0;
  return (nfailed == 0);
}

void
TestCaseBase::do_print_summary() {
  do_log_info("Case '"<<get_name()<<"'");
  for(auto tb(m_entries.begin()), te(m_entries.end()); tb != te; ++tb) {
    if(tb->exception) {
      do_log_error("Uncaught exception in '"<<tb->name<<"'");
    }
    else if(tb->errors) {
      do_log_error("Test '"<<tb->name<<"' failed "<<tb->errors<<" out of "<<tb->ntests<<" tests");
    }
    else {
      do_log_info("Test '"<<tb->name<<"' ran "<<tb->ntests<<" successful tests");
    }
  }
}

// ---- public methods -----

// ---- protected methods -----

void
TestCaseBase::test(const std::string& name, bool b) {
  if(!m_current_entry) {
    do_log_error("test() no current entry!");
    return;
  }
  ++m_current_entry->ntests;
  if(!b) {
    do_log_error("test() Test case '"<<m_current_entry->name<<"' failed '"<<name<<"'");
    ++m_current_entry->errors;
  }
}

void
TestCaseBase::test_approx_abs(const std::string& name, double a, double b, double tol, double _tol_e) {
  if(!m_current_entry) {
    do_log_error("test_approx_abs() no current entry!");
    return;
  }
  ++m_current_entry->ntests;
  double tol_w(_tol_e > 0 ? tol : 0), tol_e(_tol_e > 0 ? _tol_e : tol);
  if(sfi::abs(a - b) > tol_e) {
    do_log_error("test_approx_abs() Test case '"<<m_current_entry->name<<"' failed '"<<name<<"' : |"<<a<<" - "<<b<<" | = "<<sfi::abs(a - b)<<" > "<<tol_e);
    ++m_current_entry->errors;
  }
  else if((tol_w > 0) && (sfi::abs(a - b) > tol_w)) {
    do_log_warning("test_approx_rel() Test case '"<<m_current_entry->name<<"' test '"<<name<<"' : |"<<a<<" - "<<b<<"| = "<<std::abs(a - b)<<" > "<<tol_w);
  }
}

void
TestCaseBase::test_approx_abs(const std::string& name, std::complex<double> a, std::complex<double> b, double tol) {
  if(!m_current_entry) {
    do_log_error("test_approx_abs() no current entry!");
    return;
  }
  ++m_current_entry->ntests;
  if(std::abs(a - b) > tol) {
    do_log_error("test_approx_abs() Test case '"<<m_current_entry->name<<"' failed '"<<name<<"' : |"<<a<<" - "<<b<<" | = "<<std::abs(a - b)<<" > "<<tol);
    ++m_current_entry->errors;
  }
}

void
TestCaseBase::add(const std::string& name, const std::function<void()>& func) {
  m_entries.emplace_back(name, func);
}

const std::string&
TestCaseBase::case_name() const {
  if(!m_current_entry) {
    do_log_error("case_name() no current entry!");
    static std::string _dummy("");
    return _dummy;
  }
  return m_current_entry->name;
}
