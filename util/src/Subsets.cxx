/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Subsets.h"
#include <numeric>

using namespace sfi;

Subsets::Subsets(unsigned minv, unsigned maxv, unsigned size):m_minv(minv), m_maxv(maxv), m_sum(0), m_include(size, false) {}

Subsets::Subsets(unsigned minv, unsigned maxv, const std::vector<bool>& inc):m_minv(minv), m_maxv(maxv), m_include(inc) {
  m_sum = std::accumulate(m_include.begin(), m_include.end(), 0.0);
}

bool
Subsets::next() {
  do {
    unsigned i=0;
    do {
      ++m_sum;
      if(m_include[i] || (m_sum > m_maxv)) {
        m_sum -= (1 + m_include[i]);
        m_include[i] = false;
        ++i;
      }
      else {
        m_include[i] = true;
        i = m_include.size()+1;
      }
    }
    while(i < m_include.size());
    if(i == m_include.size()) return false;
  }
  while(m_sum < m_minv);
  return true;
}
