/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "stat.h"
#include "Histogram.h"

namespace sfi {

  Qty::Qty():m_value(0), m_uncertainty(0) {}

  Qty::Qty(double v, double unc):m_value(v), m_uncertainty(unc) {}

  SampleExtStat::SampleExtStat():SampleStat(), m_minv(1e10), m_maxv(-1e10) {}

  SampleExtStat::SampleExtStat(const SampleExtStat& s):SampleStat(s), m_minv(s.m_minv), m_maxv(s.m_maxv) {}

  SampleExtStat&
  SampleExtStat::operator+=(double x) {
    if(std::isfinite(x)) {
      add(x);
      m_minv = std::min(x, m_minv);
      m_maxv = std::max(x, m_maxv);
    }
    return *this;
  }

  void
  SampleExtStat::clear() {
    SampleStat::clear();
    m_minv = 1e10;
    m_maxv = -1e10;
  }

  std::ostream&
  SampleExtStat::print(std::ostream& out) const {
    return out<<result()<<" {min="<<m_minv<<" max="<<m_maxv<<"}";
  }

  SampleCovariance::SampleCovariance(unsigned n):m_c(n), m_mean(n), m_v1(n), m_N(0) { }

  // TODO: Really place here?
  template class HistogramBase<true,double>;
  template class HistogramBase<false,double>;

  template class Histogram<true,double>;
  template class Histogram<false,double>;

  template class Histogram2D<true,double>;
  template class Histogram2D<false,double>;
}
