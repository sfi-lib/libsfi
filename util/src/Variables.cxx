/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <Variables.h>
#include <util.h>

#include <iostream>
#include <iomanip>

using namespace sfi;

Variable::Variable(const std::string& name, double minval, double maxval):
  m_name(name), m_title(name), m_ext_minval(minval), m_ext_maxval(maxval), m_intermediary_minval(-1.0), m_intermediary_maxval(1.0),
  m_tr_intermediary_minval(0.0), m_ext_2_interm(0.0), m_interm_2_int(0.0), m_interm_2_ext(0.0), m_int_2_interm(0.0),
  m_trans_type(TransType::Linear), m_external_set(false), m_intermediary_set(false), m_locked(false) {
}

Variable&
Variable::external_interval(double emi, double ema) {
  if(m_locked) throw VariableException("external_interval() Attempt to alter locked variable '"+name()+"'");
  else {
    m_ext_minval = emi; m_ext_maxval = ema; m_external_set = true;
  }
  return *this;
}

Variable&
Variable::intermediary_interval(double imi, double ima) {
  if(m_locked) throw VariableException("intermediary_interval() Attempt to alter locked variable '"+name()+"'");
  else {
    m_intermediary_minval = imi; m_intermediary_maxval = ima; m_intermediary_set = true;
  }
  return *this;
}

double
Variable::vtrans(double x) const {
  if(!(( x >= m_ext_minval) && (x <= m_ext_maxval))) throw VariableException("vtrans() value "+std::to_string(x)+" for variable '"+name()+"' out of range ("+std::to_string(m_ext_minval)+" -> "+std::to_string(m_ext_maxval)+")");
  if(!m_locked) throw VariableException("vtrans() Variable '"+name()+"' not locked!");
  switch(m_trans_type) {
  case Ln : {
    double _x = m_ext_2_interm*(x - m_ext_minval) + m_intermediary_minval;
    return m_interm_2_int*(ln(_x) - m_tr_intermediary_minval) - 1.0;
  }
  case Acos : {
    double _x = m_ext_2_interm*(x - m_ext_minval) - 1.0;
    return m_interm_2_int*acos(_x) - 1.0;
  }
  case Atan : {
    double _x = m_ext_2_interm*(x - m_ext_minval) + m_intermediary_minval;
    return m_interm_2_int*(atan(_x) - m_tr_intermediary_minval) - 1.0;
  }
  case Cos : {
    // to 0 -> pi
    double _x = m_ext_2_interm*(x - m_ext_minval);
    return m_interm_2_int*(cos(_x) + 1.0) - 1.0;
  }
  default: return m_ext_2_interm*(x - m_ext_minval) - 1.0;
  }
}

double
Variable::vtrans(double x, double& xo) const {
  if(!(( x >= m_ext_minval) && (x <= m_ext_maxval))) throw VariableException("vtrans() value "+std::to_string(x)+" for variable '"+name()+"' out of range ("+std::to_string(m_ext_minval)+" -> "+std::to_string(m_ext_maxval)+")");
  if(!m_locked) throw VariableException("vtrans() Variable '"+name()+"' not locked!");
  switch(m_trans_type) {
  case Ln : {
    double _x = m_ext_2_interm*(x - m_ext_minval) + m_intermediary_minval;
    xo = m_interm_2_int*(ln(_x) - m_tr_intermediary_minval) - 1.0;
    return fabs(m_interm_2_int*m_ext_2_interm/_x);
  }
  case Acos : {
    double _x = m_ext_2_interm*(x - m_ext_minval) - 1.0;
    xo = m_interm_2_int*acos(_x) - 1.0;
    return fabs(m_interm_2_int*m_ext_2_interm/sqrt(1 - _x*_x));
  }
  case Atan : {
    double _x = m_ext_2_interm*(x - m_ext_minval) + m_intermediary_minval;
    xo = m_interm_2_int*(atan(_x) - m_tr_intermediary_minval) - 1.0;
    return fabs(m_interm_2_int*m_ext_2_interm/(1.0 + _x*_x));
  }
  case Cos : {
    // to 0 -> pi
    double _x = m_ext_2_interm*(x - m_ext_minval);
    xo = m_interm_2_int*(cos(_x) + 1.0) - 1.0;
    return fabs(sin(_x)*m_ext_2_interm*m_interm_2_int);
  }
  default: {
    xo = m_ext_2_interm*(x - m_ext_minval) - 1.0;
    return m_ext_2_interm;
  }
  }
}

double
Variable::vitrans(double y) const {
  if(!(( y >= -1.0) && (y <= 1.0))) throw VariableException("vitrans() value "+std::to_string(y)+" for variable '"+name()+"' out of range (-1,1) ");
  if(!m_locked) throw VariableException("vitrans() Variable '"+name()+"' not locked!");
  switch(m_trans_type) {
  case Ln : {
    double _y = ::exp((y + 1.0)*m_int_2_interm + m_tr_intermediary_minval);
    return (_y - m_intermediary_minval)*m_interm_2_ext + m_ext_minval;
  }
  case Acos : {
    double _y = cos((y + 1.0)*m_int_2_interm);
    return (_y + 1.0)*m_interm_2_ext + m_ext_minval;
  }
  case Atan : {
    double _y = tan((y + 1.0)*m_int_2_interm + m_tr_intermediary_minval);
    return (_y - m_intermediary_minval)*m_interm_2_ext + m_ext_minval;
  }
  case Cos : {
    double _y = acos((y + 1.0)*m_int_2_interm - 1.0);
    return _y*m_interm_2_ext + m_ext_minval;
  }
  default: return (y + 1.0)*m_interm_2_ext + m_ext_minval;
  }
}

Variable&
Variable::trans_type(TransType tt) {
  if(m_locked) throw VariableException("trans_type() Variable '"+name()+"' is locked!");
  else m_trans_type = tt;
  return *this;
}

bool
Variable::lock() {
  if(m_locked) throw VariableException("lock() Variable '"+name()+"' is already locked!");
  if(!m_external_set) throw VariableException("lock() Variable '"+name()+"' has no external interval");
  if(!m_intermediary_set && ((m_trans_type == Ln) || (m_trans_type == Atan))) throw VariableException("lock() Variable '"+name()+"' trans types Ln and Atan requires an intermediary interval");
  // Atan: int_minval = -int_maxval
  if((m_trans_type == Atan) && (m_intermediary_minval != -m_intermediary_maxval )) throw VariableException("lock() Variable '"+name()+"' trans types Atan requires a symmetric intermediary interval "+std::to_string(m_intermediary_minval)+", "+std::to_string(m_intermediary_maxval));
  const double d_internal = 2.0;
  double d_external = m_ext_maxval - m_ext_minval;

  switch(m_trans_type) {
    case Ln : {
      m_tr_intermediary_minval = ln(m_intermediary_minval);
      double d_tr_intermediary = ln(m_intermediary_maxval) - m_tr_intermediary_minval;

      m_ext_2_interm = (m_intermediary_maxval - m_intermediary_minval)/d_external;
      m_interm_2_int = d_internal/d_tr_intermediary;
      break;
    }
    case Acos : {
      m_ext_2_interm = 2.0/d_external;
      m_interm_2_int = d_internal/M_PI;
      break;
    }
    case Atan : {
      m_tr_intermediary_minval = atan(m_intermediary_minval);
      double d_tr_intermediary = atan(m_intermediary_maxval) - m_tr_intermediary_minval;

      m_ext_2_interm = 2.0*m_intermediary_maxval/d_external;
      m_interm_2_int = d_internal/d_tr_intermediary;
      break;
    }
    case Cos : {
      m_ext_2_interm = M_PI/d_external;
      m_interm_2_int = d_internal*0.5;
      break;
    }
    default: {
      m_ext_2_interm = d_internal/d_external;
      m_interm_2_int = 1.0;
      break;
    }
  }

  m_interm_2_ext = 1.0/m_ext_2_interm;
  m_int_2_interm = 1.0/m_interm_2_int;

  m_locked = true;
  return true;
}

std::ostream&
Variable::print(std::ostream& out) const {
  return out<<"'"<<name()<<"' ("<<minval()<<" -> "<<maxval()<<") trans: "<<m_trans_type<<" locked: "<<is_locked()<<" ";
}

void
Variable::write_vtrans_cpp(const std::string& varname, std::ostream& out) const {
  switch(m_trans_type) {
  case Ln : {
    out<<m_interm_2_int<<"*(log("<<m_ext_2_interm<<"*("<<varname<<" - "<<m_ext_minval<<") + "<<m_intermediary_minval<<") - "<<m_tr_intermediary_minval<<") - 1.0";
    break;
  }
  case Acos : {
    out<<m_interm_2_int<<"*acos("<<m_ext_2_interm<<"*("<<varname<<" - "<<m_ext_minval<<") - 1.0) - 1.0";
    break;
  }
  case Atan : {
    out<<m_interm_2_int<<"*(atan("<<m_ext_2_interm<<"*("<<varname<<" - "<<m_ext_minval<<") + "<<m_intermediary_minval<<") - "<<m_tr_intermediary_minval<<") - 1.0";
    break;
  }
  case Cos : {
    out<<m_interm_2_int<<"*(cos("<<m_ext_2_interm<<"*("<<varname<<" - "<<m_ext_minval<<")) + 1.0) - 1.0";
    break;
  }
  default: {
    out<<m_ext_2_interm<<"*("<<varname<<" - "<<m_ext_minval<<") - 1.0";
    break;
  }
  }
}

Variables::Variables() {
}

Variables::Variables(size_t n) {
  init(n);
}

Variables::Variables(const Variables& v):m_vars(v.m_vars) {
}

Variables::Variables(const OptionVec& ov) {
  init(ov);
}

void
Variables::init(size_t n) {
  m_vars.resize(n, Variable());
}

void
Variables::init(const OptionVec& ovar) {
  m_vars.resize(ovar.size(), Variable());
  for(size_t i = 0; i<ovar.size(); ++i) {
    auto& var = at(i);
    auto* omap = dynamic_cast<const OptionMap*>(ovar[i]);
    if(omap) {
      var.name(omap->get<std::string>("name"));
      if(omap->has("title")) {
        var.title(omap->get<std::string>("title"));
      }
      if(omap->has("transform")) {
        std::string trf = omap->get<std::string>("transform");
        Variable::TransType ttype = Variable::Linear;
        if(trf == "linear") ttype = Variable::Linear;
        else if(trf == "ln") ttype = Variable::Ln;
        else if(trf == "acos") ttype = Variable::Acos;
        else if(trf == "atan") ttype = Variable::Atan;
        else if(trf == "cos") ttype = Variable::Cos;
        var.trans_type(ttype);
      }
      // TODO: Remove internal_interval
      if(omap->has("internal_interval")) {
        auto& iiv = omap->get<std::vector<double>>("internal_interval");
        if(iiv.size() == 2) {
          var.intermediary_interval(iiv[0], iiv[1]);
        }
      }
      else if(omap->has("intermediary_interval")) {
        auto& iiv = omap->get<std::vector<double>>("intermediary_interval");
        if(iiv.size() == 2) {
          var.intermediary_interval(iiv[0], iiv[1]);
        }
      }
      if(omap->has("external_interval")) {
        auto& iiv = omap->get<std::vector<double>>("external_interval");
        if(iiv.size() == 2) {
          var.external_interval(iiv[0], iiv[1]);
        }
      }
      else if(omap->has("interval")) {
        auto& iiv = omap->get<std::vector<double>>("interval");
        if(iiv.size() == 2) {
          var.external_interval(iiv[0], iiv[1]);
        }
      }
    }
  }
}

void Variables::write(OptionVec& ov) const {
  for(unsigned i=0; i<m_vars.size(); ++i) {
    auto& v = m_vars[i];
    auto* vo = new OptionMap;
    vo->set("name", v.name());
    vo->set("title", v.title());

    switch(v.trans_type()) {
    case Variable::Linear: vo->set<std::string>("transform", "linear"); break;
    case Variable::Ln: vo->set<std::string>("transform", "ln"); break;
    case Variable::Acos: vo->set<std::string>("transform", "acos"); break;
    case Variable::Atan: vo->set<std::string>("transform", "atan"); break;
    case Variable::Cos: vo->set<std::string>("transform", "cos"); break;
    }

    vo->set("interval",std::vector<double>({v.minval(), v.maxval()}));
    if(v.intermediary_interval_set())
      vo->set("intermediary_interval",std::vector<double>({v.intermediary_minval(), v.intermediary_maxval()}));
    ov.add(vo);
  }
}

std::ostream&
Variables::print(std::ostream& out) const {
  out<<"Variables:"<<std::endl;
  for(auto& v : m_vars) out<<v<<std::endl;;
  return out;
}


bool
Variables::lock() {
  bool all_ok = true;
  for(auto& v : m_vars) {
    if(!v.is_locked()) {
      all_ok = v.lock() && all_ok;
    }
  }
  return all_ok;
}

bool
Variables::compare(const Variables& ov) const {
  if(nvariables() != ov.nvariables()) return false;
  bool all_ok = true;
  for(unsigned i=0; i<nvariables(); ++i) {
    auto& v0 = at(i);
    auto& v1 = ov.at(i);
    if(! ((v0.name() == v1.name()) && (v0.minval() == v1.minval()) && (v0.maxval() == v1.maxval()))) all_ok = false;
  }
  return all_ok;
}

Variable*
Variables::lookup(const std::string& name) {
  for(auto& v : m_vars) {
    if(v.name() == name) return &v;
  }
  return 0;
}

