/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <Log.h>
#include <Context.h>

#ifdef SFI_ENABLE_MP
#include <mp/Dispatcher.h>
#endif

#include <map>
#include <unistd.h>
#include <iomanip>

inline bool is_stdout_tty() {
  static bool _stdout_isatty = ::isatty(::fileno(::stdout));
  return _stdout_isatty;
}

struct ansi_fg {
  ansi_fg(int code):m_code(code) {}
  int m_code;
};

struct ansi_default {};

inline std::ostream& operator<<(std::ostream& out, const ansi_fg& a) { return out<<"\x1b["<<a.m_code<<"m"; }
inline std::ostream& operator<<(std::ostream& out, const ansi_default&) { return out<<ansi_fg(39); }

using namespace sfi;

Log::Log(Context& ctx, LogEntry *entry):m_context(ctx), m_entry(entry) {}

Log::Log(const Log& o):m_context(o.m_context), m_entry(o.m_entry) {}

Log
Log::get(Context& ctx, const std::string& name) {
  static std::map<std::string, LogEntry*> s_logs;
  auto item = s_logs.find(name);
  if(item == s_logs.end()) {
    LogEntry* log = new LogEntry{name};
    s_logs[name] = log;
    return Log(ctx, log);
  }
  else return Log(ctx, item->second);
}

void
Log::print_level(const std::string& pl) {
  if(pl == "trace") print_level(Trace);
  else if(pl == "verbose") print_level(Verbose);
  else if(pl == "debug") print_level(Debug);
  else if(pl == "message") print_level(Message);
  else if(pl == "warning") print_level(Warning);
  else if(pl == "error") print_level(Error);
}

Log::LogStream
Log::log(PrintLevel pl) const {
  LogStream ls;
  switch(pl) {
  case Trace:   ls.m_out<<"  Trace "; break;
  case Verbose: ls.m_out<<"Verbose "; break;
  case Debug:   ls.m_out<<"  Debug "; break;
  case Message: ls.m_out<<"Message "; break;
  case Warning: {
    if(is_stdout_tty()) ls.m_out<<ansi_fg(33)<<"Warning "<<ansi_default();
    else ls.m_out<<"Warning ";
    break;
  }
  case Error: {
    if(is_stdout_tty()) ls.m_out<<ansi_fg(31)<<"  Error "<<ansi_default();
    else ls.m_out<<"  Error ";
    break;
  }
  case OK: {
    if(is_stdout_tty()) ls.m_out<<ansi_fg(32)<<" **OK** "<<ansi_default();
    else ls.m_out<<" **OK** ";
    break;
  }
  case Info: ls.m_out<<"  Info  "; break;
  }
  ls.m_out<<name()<<std::setprecision(m_entry->m_precison)<<" ";
  return ls;
}

void
Log::end(LogStream ls) const {
#ifdef SFI_ENABLE_MP
  auto* dis = m_context.dispatcher();
  if(dis->enabled()) dis->iojob([ls = std::move(ls)](Context&){ std::cout<<"-- "<<ls.m_out.str()<<std::endl; });
  else {
    std::cout<<"*"<<(ls.m_out.str())<<std::endl;
  }
#else
  std::cout<<ls.m_out.str()<<std::endl;
#endif
}
