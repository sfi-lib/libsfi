/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Parameters.h"
#include "sfi_math.h"

using namespace sfi;

#include <cmath>
#include <iostream>
#include <iomanip>

bool
Parameter::limit(double minv, double maxv) {
  if((minv == 0) && (maxv == 0)) clear_limits();
  else {
    m_min_value = minv; m_max_value = maxv;
    m_limited = true;
  }
  return true;
}

void
Parameter::clear_limits() {
  m_min_value = m_max_value = 0;
  m_limited = false;
}

Parameter::Parameter(const std::string& name, unsigned index):
    m_name(name), m_type(Normal), m_ext_index(index), m_initial_value(0.0), m_min_value(0.0), m_max_value(0.0), m_step_length(0.1),
    m_work_value(0), m_ml_value(0.0), m_ml_unc(0.0), m_ml_unc_up(0.0), m_ml_unc_down(0.0),m_ml_global_cc(0.0), m_ml_unc_parab(0.0),
    m_internal_index(-1),
    m_s_p(0.0), m_s_p_save(0.0), m_s_dirin(0.0), m_s_grad(0.0), m_s_deriv2(0.0), m_s_step(0.0),
    m_s_ml_unc(0.0), m_s_ml_global_cc(0.0),
    m_limited(false), m_fixed(false) {
}

void Parameter::clear_uncertainties(bool only_asym) {
  m_ml_unc_up = m_ml_unc_down = 0.0;
  if(!only_asym) m_ml_global_cc = m_ml_unc_parab = m_ml_unc = 0.0;
}

std::ostream&
Parameter::print(std::ostream& out) const {
  out<<"'"<<m_name<<"' init:"<<m_initial_value;
  if(this->is_limited()) out<<" {"<<m_min_value<<","<<m_max_value<<"}";
  if(m_work_value) {
    out<<" value: "<<m_ml_value<<" +- "<<m_ml_unc;
    auto ud = m_ml_unc_down;
    if(!std::isnan(ud) && (ud != 0)) out<<" + "<<m_ml_unc_up<<" - "<<m_ml_unc_down;
  }
  if(this->m_fixed) out<<" fixed";
  return out;
}

ParameterArray::ParameterArray(size_t npars, const std::string& name, const std::vector<Parameter*>& pars):
    m_npars(npars), m_name(name), m_params(pars), m_vals(m_npars, 0.0) {
}

ParameterArray::ParameterArray(ParameterArray&& o):
    m_npars(o.m_npars), m_name(std::move(o.m_name)), m_params(std::move(o.m_params)), m_vals(std::move(o.m_vals)) {
}

ParameterArray::ParameterArray():m_npars(0), m_name("") {}

ParameterArray& ParameterArray::operator=(ParameterArray&& o) {
  if(&o != this) {
    m_npars = o.m_npars;
    m_name = std::move(o.m_name);
    m_params = std::move(o.m_params);
    m_vals = std::move(o.m_vals);
  }
  return *this;
}

const double*
ParameterArray::values() {
  for(unsigned p=0; p<m_npars; ++p) m_vals[p] = (double)(*m_params[p]);
  return m_vals.data();
}

Parameters::Parameters():m_dummy_param("dummy",0), m_locked(false) {}

Parameters::Parameters(Parameters&& o):m_parameters(std::move(o.m_parameters)),
    m_parameters_vec(std::move(o.m_parameters_vec)),
    m_fixed_parameters(std::move(o.m_fixed_parameters)),
    m_dummy_param("dummy",0), m_locked(o.m_locked) {
  o.m_parameters.clear();
  o.m_fixed_parameters.clear();
  o.m_parameters_vec.clear();
}

Parameter&
Parameters::operator[](const std::string& n) {
  std::map<std::string, Parameter>::iterator it(m_parameters.find(n));
  if(it != m_parameters.end()) return it->second;
  else {
    if(m_locked) return m_dummy_param;
    auto item = m_parameters.emplace(n, Parameter(n, m_parameters.size()));
    m_parameters_vec.push_back(&item.first->second);
    return item.first->second;
  }
}

void
Parameters::set_working_values(double* par) {
  for(auto& p : m_parameters) p.second.m_work_value = par + p.second.m_ext_index.value();
}

void Parameters::clear_working_values() {
  for(auto& p : m_parameters) p.second.m_work_value = nullptr;
}

ParameterArray
Parameters::par_array(const std::string& name, size_t npars) {
  std::vector<Parameter*> pars(npars, 0);
  for(unsigned p=0; p<npars; ++p) pars[p] = &(*this)[name+"_"+std::to_string(p)];
  return ParameterArray(npars, name, pars);
}

void Parameters::fix(Parameter& p) {
  m_fixed_parameters.push_back(&p);
  p.m_fixed = true;
}


Parameter* Parameters::release() {
  if(!m_fixed_parameters.empty()) {
    Parameter* p = m_fixed_parameters.back();
    m_fixed_parameters.pop_back();
    p->m_fixed = false;
    return p;
  }
  else return 0;
}

bool Parameters::release(Parameter& p) {
  for(auto it = m_fixed_parameters.begin(); it != m_fixed_parameters.end(); ++it) {
    if(*it == &p) {
      m_fixed_parameters.erase(it);
      p.m_fixed = false;
      return true;
    }
  }
  return false;
}

bool Parameters::lock() {
  if(m_locked) return false;
  if(m_parameters.size() != m_parameters_vec.size()) return false;
  for(auto& p : m_parameters) {
    if(p.second.is_fixed()) {
      bool is_in_fixed = false;
      for(auto it = m_fixed_parameters.begin(); !is_in_fixed &&(it != m_fixed_parameters.end()); ++it) {
        if(*it == &p.second) is_in_fixed = true;
      }
      if(!is_in_fixed) m_fixed_parameters.push_back(&p.second);
    }
  }
  m_locked = true;
  return true;
}

std::ostream& Parameters::print(std::ostream& out) const {
  for(auto* p : m_parameters_vec) out<<*p<<std::endl;
  return out;
}

bool Parameters::has_param_at_limit() const {
  static const double eps2 = 2.0*sqrt(4.0*std::numeric_limits<double>::epsilon());
  bool res = false;
  for(auto* p : m_parameters_vec) {
    if(p->is_limited()) {
      double y = 2.0*(p->work_value() - p->limit_min())/(p->limit_max() - p->limit_min()) - 1.0;
      res = (y*y > (1.0 - eps2)) || res;
    }
  }
  return res;
}

Parameters& Parameters::operator=(Parameters&& o) {
  if(&o != this) {
    m_parameters = std::move(o.m_parameters);
    m_parameters_vec = std::move(o.m_parameters_vec);
    m_fixed_parameters = std::move(o.m_fixed_parameters);
    m_dummy_param = o.m_dummy_param;
    m_locked = o.m_locked;
  }
  return *this;
}
