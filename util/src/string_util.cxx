/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "string_util.h"

#include <iostream>

namespace sfi {
  bool ends_with(const std::string& str, const std::string& suff) {
    if(str.length() >= suff.length()) {
      std::string estr(str.substr(str.length() - suff.length()));
      return estr == suff;
    }
    return false;
  }

  bool starts_with(const std::string& str, const std::string& match) {
    if(str.length() < match.length()) return false;
    else return str.substr(0, match.length()) == match;
  }

  std::string trim_string(const std::string& str) {
    const size_t len(str.length());
    if(!len) return "";

    size_t ipos(0);
    for(; (ipos<len) && std::isspace(str[ipos]); ++ipos);

    size_t epos(len - 1);
    for(; (epos>ipos) && std::isspace(str[epos]); --epos);
    return str.substr(ipos, epos - ipos + 1);
  }

  std::vector<std::string> split_string(const std::string& str, char delim) {
    std::vector<std::string> res;
    size_t opos(0), pos(0);
    while(pos != std::string::npos) {
      pos = str.find_first_of(delim, opos);
      std::string s = trim_string(str.substr(opos, pos - opos));
      if(!s.empty()) res.push_back(s);
      opos = pos+1;
    }
    return res;
  }

  inline bool is_quote(char c) { return (c == '\'') || (c == '\"'); }

  std::vector<std::string>
  split_quoted_string(const std::string& _str) {
    std::vector<std::string> res;
    std::string str = trim_string(_str);
    char quote(0), cc(0);
    size_t pos(0), bpos(0);
    const size_t len = str.length();
    while(pos < len) {
      cc = str[pos];
      // skip escaped quotes
      if((cc == '\\') && (len > (pos+1)) && is_quote(str[pos+1])) pos += 2;
      else if(is_quote(cc)) {
        // beginning of quoted string
        if(!quote) {
          if(false && (pos > bpos)) {
            std::cout<<"split_quoted_string() Syntax error: '"<<str<<"' @"<<pos<<std::endl;
            ++pos;
          }
          else {
            quote = cc;
            bpos = ++pos;
          }
        }
        // end of quoted string
        else {
          if(quote != cc) {}
          else if(bpos != std::string::npos) {
            res.push_back(str.substr(bpos, pos - bpos));
            bpos = std::string::npos;
            quote = 0;
          }
          else {
            std::cout<<"split_quoted_string() Invalid bpos: '"<<str<<"' @"<<pos<<std::endl;
          }
          ++pos;
        }
      }
      else if(quote) ++pos;
      else if(cc == ',') {
        if(bpos != std::string::npos) {
          res.push_back(trim_string(str.substr(bpos, pos - bpos)));
        }
        bpos = ++pos;
      }
      else if(pos == (len-1)) {
        if(bpos != std::string::npos) {
          res.push_back(trim_string(str.substr(bpos, pos - bpos + 1)));
          bpos = std::string::npos;
        }
        ++pos;
      }
      else ++pos;
    }

    return res;
  }

  bool
  from_string<bool>::apply(const std::string& str, bool& value) {
    value = ((str == "true") || (str == "1"));
    return true;
  }

  bool
  from_string<std::string>::apply(const std::string& str, std::string& value) {
    value = str;
    return true;
  }

  bool
  from_string<std::vector<bool>>::apply(const std::string& str, std::vector<bool>& value) {
    std::vector<std::string> parts = split_quoted_string(str);
    const size_t N(parts.size());
    value.resize(N);
    for(size_t i=0; i<N; ++i) value[i] = ((parts[i] == "true") || (parts[i] == "1"));
    return true;
  }

  bool try_parse_uint(const std::string& str, unsigned& value) {
    char *end(0);
    if(((str.back() == 'U') || (str.back() == 'u')) && (str.find('.') == std::string::npos) && (str.find('e') == std::string::npos) && (str.find('-') == std::string::npos)) {
      value = std::strtoul(str.c_str(), &end, 0);
      return (end != str.c_str());
    }
    else return false;
  }

  bool try_parse_int(const std::string& str, int& value) {
    char *end(0);
    if((str.find('.') == std::string::npos) && (str.find('e') == std::string::npos)) {
      value = std::strtol(str.c_str(), &end, 0);
      return (end != str.c_str());
    }
    else return false;
  }

  bool try_parse_double(const std::string& str, double& value) {
    char *end(0);
    value = std::strtod(str.c_str(), &end);
    return (end != str.c_str());
  }
}
