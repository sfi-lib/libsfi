/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Options.h"
#include "util.h"
#include "OptsParser.h"

#include <fstream>
#include <stdlib.h>

using namespace sfi;

inline std::string fix_quoted_string(const std::string& value) {
  if(value.length() > 2) {
    if( ((value[0] == '\'') && (value.back() == '\'')) || ((value[0] == '"') && (value.back() == '"'))) {
      return value.substr(1, value.length() - 2);
    }
  }
  return value;
}

Option::~Option() {}

Option&
Option::operator=(const Option& op) {
  if(this != &op) {
    if(get_type_info() == op.get_type_info()) {
      m_references = op.m_references;
      // m_name = op.m_name;
      m_help = op.m_help;
      m_required = op.m_required;
      m_is_set = op.m_is_set;
      set_value(op.get_value_address());
    }
    else throw OptionException("Invalid option type '"+type_name(op.get_type_info())+" expected "+type_name(get_type_info()));
  }
  return *this;
}

bool
Option::get_is_valid() const { return m_is_set; }

void
Option::set_is_valid(bool b) { m_is_set = b; }

Option&
Option::operator[](const std::string& _name) { throw OptionException("Unknown option '"+_name+"' in '"+name()+"'"); }

const Option&
Option::operator[](const std::string& _name) const { throw OptionException("Unknown option '"+_name+"' in '"+name()+"'"); }

template<class _Dst>
inline bool convert_primitives(const Option* opt, _Dst& dst) {
  bool res = true;
  if(opt->get_type_info() == typeid(uint32_t)) dst = static_cast<_Dst>(dynamic_cast<const OptionBase<uint32_t>*>(opt)->get_value());
  else if(opt->get_type_info() == typeid(int32_t)) dst = static_cast<_Dst>(dynamic_cast<const OptionBase<int32_t>*>(opt)->get_value());
  else if(opt->get_type_info() == typeid(uint64_t)) dst = static_cast<_Dst>(dynamic_cast<const OptionBase<uint64_t>*>(opt)->get_value());
  else if(opt->get_type_info() == typeid(int64_t)) dst = static_cast<_Dst>(dynamic_cast<const OptionBase<int64_t>*>(opt)->get_value());
  else if(opt->get_type_info() == typeid(double)) dst = static_cast<_Dst>(dynamic_cast<const OptionBase<double>*>(opt)->get_value());
  else if(opt->get_type_info() == typeid(bool)) dst = static_cast<_Dst>(dynamic_cast<const OptionBase<bool>*>(opt)->get_value());
  else if(opt->get_type_info() == typeid(std::string)) {
    res = from_string<_Dst>::apply(dynamic_cast<const OptionBase<std::string>*>(opt)->get_value(), dst);
  }
  else res = false;
  return res;
}

bool
Option::convert(uint32_t& dst) const {
  return convert_primitives<uint32_t>(this, dst);
}

bool
Option::convert(int32_t& dst) const {
  return convert_primitives<int32_t>(this, dst);
}

bool
Option::convert(uint64_t& dst) const {
  return convert_primitives<uint64_t>(this, dst);
}

bool
Option::convert(int64_t& dst) const {
  return convert_primitives<int64_t>(this, dst);
}

bool
Option::convert(double& dst) const {
  return convert_primitives<double>(this, dst);
}

bool
Option::convert(std::string& dst) const {
  bool res = true;
  if(this->get_type_info() == typeid(std::string)) dst = dynamic_cast<const OptionBase<std::string>*>(this)->get_value();
  else if(this->get_type_info() == typeid(int)) {
    dst = std::to_string(dynamic_cast<const OptionBase<int>*>(this)->get_value());
  }
  else if(this->get_type_info() == typeid(unsigned)) {
    dst = std::to_string(dynamic_cast<const OptionBase<unsigned>*>(this)->get_value());
  }
  else if(this->get_type_info() == typeid(double)) {
    dst = std::to_string(dynamic_cast<const OptionBase<double>*>(this)->get_value());
  }
  else if(this->get_type_info() == typeid(bool)) {
    dst = std::to_string(dynamic_cast<const OptionBase<bool>*>(this)->get_value());
  }
  else  res = false;
  return res;
}

bool
Option::convert(std::vector<bool>::reference dst) const {
  bool src(dst);
  if(convert(src)) {
    dst = src;
    return true;
  }
  return false;
}

bool
Option::convert(bool& dst) const {
  bool res = true;
  if(this->get_type_info() == typeid(std::string)) {
    auto& str = dynamic_cast<const OptionBase<std::string>*>(this)->get_value();
    if(str == "true") dst = true;
    else if(str == "false") dst = false;
    else res = false;
  }
  else res = convert_primitives<bool>(this, dst);
  return res;
}

bool Option::convert(size_t& dst) const {
  return convert_primitives<size_t>(this, dst);
}

bool Option::convert(sfi::Vector<double>& dst) const {
  if(this->get_type_info() == typeid(std::vector<double>)) {
    dst = dynamic_cast<const OptionBase<std::vector<double>>*>(this)->get_value();
    return true;
  }
  return false;
}

Option::Option(const std::string& name, bool is_req, const std::string& help):
            m_references(0), m_name(name), m_help(help), m_required(is_req), m_is_set(false) { }

Option::Option(const Option& o):m_references(0), m_name(o.m_name), m_help(o.m_help), m_required(o.m_required), m_is_set(o.m_is_set) {}

// ---- OptionMap ----

OptionMap::OptionImpl(const std::string& name, bool is_req, const std::string& help):Base(name, is_req, help) { }

OptionMap::OptionImpl(const OptionMap& o):Base(o.m_name, o.m_required, o.m_help) {
  for(auto& e : o.m_options) add(e.first, e.second->clone());
}

OptionMap::OptionImpl(OptionImpl& o, bool shallow):Base(o){
  if(shallow) {
    for(auto& e : o.m_options) add(e.first, e.second);
  }
  else {
    for(auto& e : o.m_options) add(e.first, e.second->clone());
  }
}

OptionMap::OptionImpl(OptionMap&& o):Base(o.m_name, o.m_required, o.m_help),
    m_options(std::forward<OptMap>(o.m_options)) {
}

OptionMap::~OptionImpl() {
  for(auto& o : m_options) {
    if(o.second) remove(o.second);
    else {
      std::cout<<"~OptionMap ERROR option '"<<o.first<<"' is null"<<std::endl;
    }
  }
  m_options.clear();
}

OptionMap::This& OptionMap::operator=(const This& o) {
  // TODO: Move to method clear
  for(auto& e : m_options) delete e.second;
  m_options.clear();
  for(auto& e : o.m_options) add(e.first, e.second->clone());
  return *this;
}

bool
OptionMap::set(const std::string&) { return false; }

bool
OptionMap::set_value(const void* addr) {
  m_options = *static_cast<const OptMap*>(addr);
  return false;
}

const void*
OptionMap::get_value_address() const { return &m_options; }

const OptMap&
OptionMap::get_value() const { return m_options; }

OptMap&
OptionMap::get_value() { return m_options; }

void
OptionMap::do_print(std::ostream& out) const {
  print(out,0);
}

void
OptionMap::do_print_help(std::ostream& out) const {
  for(auto& o : m_options) {
    out<<" "<<o.first<<" : ";
    o.second->do_print_help(out);
    out<<std::endl;
  }
}

Option::Type
OptionMap::get_type() const { return Option::Object; }

Option&
OptionMap::operator[](const std::string& name) {
  auto* op = suboption(name);
  if(op) return *op;
  else throw OptionException("Unknown option '"+name+"' in '"+this->name()+"'");
}

const Option&
OptionMap::operator[](const std::string& name) const {
  auto* op = suboption(name);
  if(op) return *op;
  else throw OptionException("Unknown option '"+name+"' in '"+this->name()+"'");
}

Option*
OptionMap::clone() const { return new OptionMap(*this); }

OptionMap&
OptionMap::copy(const std::string& name, const Option& op) {
  auto parts = split_string(name,'.');
  auto* so = suboptions(parts);
  if(so) {
    if(!so->has(parts.back())) {
      so->add(parts.back(), op.clone());
      return *this;
    }
    else throw OptionException("copy"+name+"' already set");
  }
  throw OptionException("copy"+name+"' parent not found");
}

OptionMap&
OptionMap::sub(const std::string& name, bool define_if_missing) {
  auto parts = split_string(name,'.');
  auto* so = suboptions(parts, define_if_missing);
  if(so) {
    if(!so->has_impl(parts.back())) {
      if(define_if_missing) {
        auto *no = new OptionMap(parts.back());
        so->add(parts.back(), no);
        return *no;
      }
    }
    else {
      if(so->has_sub(parts.back())) {
        return dynamic_cast<OptionMap&>((*so)[parts.back()]);
      }
    }
  }
  throw OptionException("sub: unknown option '"+name+"' in '"+this->name()+"'");
}

const OptionMap&
OptionMap::sub(const std::string& name) const {
  auto parts = split_string(name,'.');
  auto* so = suboptions(parts);
  if(so) return dynamic_cast<const OptionMap&>((*so)[parts.back()]);
  else throw OptionException("sub: unknown option '"+name+"' in '"+this->name()+"'");
}

OptionVec&
OptionMap::sub_vec(const std::string& name, bool define_if_missing) {
  auto parts = split_string(name,'.');
  auto* so = suboptions(parts, define_if_missing);
  if(so) {
    if(!so->has_impl(parts.back())) {
      if(define_if_missing) {
        auto *no = new OptionVec(parts.back());
        so->add(parts.back(), no);
        return *no;
      }
    }
    else {
      if(so->has_sub_vec(parts.back())) {
        return dynamic_cast<OptionVec&>((*so)[parts.back()]);
      }
    }
  }
  throw OptionException("sub_vec: unknown option '"+name+"' in '"+this->name()+"'");
}

const OptionVec&
OptionMap::sub_vec(const std::string& name) const {
  auto parts = split_string(name,'.');
  auto* so = suboptions(parts);
  if(so) {
    auto* xo = so->option(parts.back());
    if(xo) {
      auto* vo = dynamic_cast<const OptionVec*>(xo);
      if(vo) return *vo;
      else {
        std::ostringstream sout;
        sout<<*xo;
        throw OptionException("sub_vec: option '"+name+"' in '"+this->name()+"' is not a vector : "+sout.str());
      }
    }
  }
  std::ostringstream sout;
  sout<<*this;
  throw OptionException("sub_vec: unknown option '"+name+"' in '"+this->name()+"' : "+sout.str());
}

OptionMap::iterator
OptionMap::find(const std::string& name) { return m_options.find(name); }

bool
OptionMap::has_impl(const std::string& name) const { return m_options.find(name) != m_options.end(); }

bool
OptionMap::has_type(const std::string& name, const std::type_info& ti) const {
  auto parts = split_string(name,'.');
  auto* so = suboptions(parts);
  if(so) return so->has_type_impl(parts.back(), ti);
  else return false;
}

bool
OptionMap::has_type_impl(const std::string& name, const std::type_info& ti) const {
  auto it = m_options.find(name);
  if(it != m_options.end()) return (it->second->get_type_info() == ti);
  else return false;
}

void
OptionMap::add(const std::string& name, Option* opt) {
  auto it = m_options.find(name);
  if(it != m_options.end()) remove(it->second);
  m_options[name] = opt;
  ++opt->m_references;
}

bool
OptionMap::remove(const std::string& name) {
  auto it = m_options.find(name);
  if(it != m_options.end()) {
    if(it->second) remove(it->second);
    else {
      std::cout<<"OptionMap::remove ERROR option '"<<it->first<<"' to remove is null"<<std::endl;
    }
    m_options.erase(it);
    return true;
  }
  else return false;
}

void
OptionMap::remove(Option*o) {
  if(o) {
    if((--o->m_references) == 0) delete o;
  }
  else {
    std::cout<<"OptionMap::remove ERROR option to remove is null"<<std::endl;
  }
}

bool
OptionMap::replace(Option* new_op) {
  if(new_op->help().empty()) throw OptionException("Cannot replace option with empty help");
  bool replaced = false;
  for(auto& oit : m_options) {
    if(oit.second->help() == new_op->help()) {
      remove(oit.second);
      oit.second = new_op;
      new_op->m_references++;
      replaced = true;
    }
    else {
      auto* opm = dynamic_cast<OptionMap*>(oit.second);
      if(opm) {
        if(opm->replace(new_op)) replaced = true;
      }
    }
  }
  return replaced;
}

Option*
OptionMap::option(const std::string& name) {
  auto it = m_options.find(name);
  if(it != m_options.end()) return it->second;
  else return 0;
}

const Option*
OptionMap::option(const std::string& name) const {
  auto it = m_options.find(name);
  if(it != m_options.end()) return it->second;
  else return 0;
}

template<class _Src, class _Dst>
inline bool
try_cast(const std::type_info& src_ty, const std::type_info& dst_ty, const void* src, void* dst) {
  return ((src_ty == typeid(_Src)) && (dst_ty == typeid(_Dst))) ?
      (*static_cast<_Dst*>(dst) = static_cast<_Dst>(*static_cast<const _Src*>(src)), true) :
      false;
}

bool
OptionMap::can_cast(const std::type_info& src_ty, const std::type_info& dst_ty, const void* src, void* dst) {
  if(try_cast<unsigned, int>(src_ty, dst_ty, src, dst)) return true;
  if(try_cast<unsigned, size_t>(src_ty, dst_ty, src, dst)) return true;

  if(try_cast<size_t, int>(src_ty, dst_ty, src, dst)) return true;
  if(try_cast<size_t, unsigned>(src_ty, dst_ty, src, dst)) return true;

  if(try_cast<int, size_t>(src_ty, dst_ty, src, dst)) return true;
  if(try_cast<int, unsigned>(src_ty, dst_ty, src, dst)) return true;

  return false;
}

Option*
OptionMap::typed_option(const std::string& name, const std::type_info& type) const {
  auto item = m_options.find(name);
  if(item != m_options.end()) {
    Option* opt = item->second;
    if(type == opt->get_type_info()) return opt;
    else {
      throw OptionException("option '"+name+"' with type '"+type_name(type)+"' defined with another type "+type_name(opt->get_type_info()));
    }
  }
  else {
    throw OptionException("option '"+name+"' with type '"+type_name(type)+"' is not defined");
  }
  return 0;
}

void OptionMap::print(std::ostream& out, unsigned indent) const {
  out<<std::scientific<<"{ "<<std::endl;
  bool first = true;
  for(auto& o : m_options) {
    if(first) first = false;
    else out<<","<<std::endl;
    for(unsigned i=0;i<indent;++i) out<<" ";
    out<<o.first<<" : ";
    auto* op = o.second;
    if(op->get_type() == Option::Object) dynamic_cast<const OptionMap*>(op)->print(out, indent+2);
    else o.second->do_print(out);
  }
  out<<std::endl;
  for(unsigned i=0;i<indent;++i) out<<" ";
  out<<"}";
}

OptionMap*
OptionMap::suboptions(const std::vector<std::string>& names, bool create_missing) {
  if(!names.size()) throw OptionException("No name given!");
  const size_t N(names.size() - 1);
  OptionMap* sopt = this;
  for(size_t i=0; (sopt != 0) && (i<N); ++i) {
    if(sopt->has_impl(names[i])) sopt = dynamic_cast<OptionMap*>(sopt->option(names[i]));
    else if(create_missing) {
      auto* nsop = new OptionMap(names[i]);
      sopt->add(names[i], nsop);
      sopt = nsop;
    }
    else sopt = 0;
  }
  return sopt;
}

const OptionMap*
OptionMap::suboptions(const std::vector<std::string>& names) const {
  if(!names.size()) throw OptionException("No name given!");
  const size_t N(names.size() - 1);
  const OptionMap* sopt = this;
  for(size_t i=0; (sopt != 0) && (i<N); ++i) {
    if(sopt->has_impl(names[i])) {
      sopt = dynamic_cast<const OptionMap*>(sopt->option(names[i]));
    }
    else sopt = 0;
  }
  return sopt;
}

Option*
OptionMap::suboption(const std::string& name) {
  auto parts = split_string(name,'.');
  auto* so = suboptions(parts);
  if(so) return so->option(parts.back());
  else return 0;
}

const Option*
OptionMap::suboption(const std::string& name) const {
  auto parts = split_string(name,'.');
  auto* so = suboptions(parts);
  if(so) return so->option(parts.back());
  else return 0;
}

// ---- OptionVec ----

OptionImpl<OptVec>::OptionImpl(const std::string& name, bool is_req, const std::string& help):Base(name, is_req, help) {}

OptionImpl<OptVec>::OptionImpl(const OptionImpl<OptVec>& o):Base(o.m_name, o.m_required, o.m_help) {
  m_value.reserve(o.m_value.size());
  for(auto* e : o.m_value) m_value.push_back(e->clone());
}

OptionImpl<OptVec>::~OptionImpl() {
  for(auto* e : m_value) {
    if((--e->m_references) == 0) delete e;
  }
  m_value.clear();
}

bool
OptionImpl<OptVec>::set(const std::string&) { return false; }

bool
OptionImpl<OptVec>::set_value(const void*) { return false; }

const void*
OptionImpl<OptVec>::get_value_address() const { return &m_value; }

const OptVec&
OptionImpl<OptVec>::get_value() const { return m_value; }

OptVec&
OptionImpl<OptVec>::get_value() { return m_value; }

void
OptionImpl<OptVec>::do_print(std::ostream& out) const {
  out<<"[ ";
  bool first = true;
  for(auto*o : m_value) {
    if(first) first = false;
    else out<<", ";
    o->do_print(out);
  }
  out<<"]";
}

void
OptionImpl<OptVec>::do_print_help(std::ostream& out) const {
  bool first = true;
  out<<"[";
  for(auto* op : m_value) {
    if(first) first = false;
    else out<<", ";
    op->do_print_help(out);
  }
}

Option*
OptionImpl<OptVec>::clone() const {
  return new OptionImpl<OptVec>(*this);
}

void
OptionImpl<OptVec>::add(Option* op) {
  ++op->m_references;
  m_value.push_back(op);
}

// ---- Options ----

Options::Options(int argc, char** argv) {
  parse_options(argc, (const char**)argv);
}

Options::Options(int argc, const char** argv) {
  parse_options(argc, argv);
}

Options::Options(const Options& o):OptionMap(o), m_exe_name(o.m_exe_name) {
}

Options::Options(Options&& o):OptionMap(std::forward<OptionMap>(o)), m_exe_name(std::forward<std::string>(o.m_exe_name)) {
}

Options::Options() { }

Options::~Options() { }

void
Options::parse_options(int argc, char** argv) {
  parse_options(argc, (const char**)argv);
}

void
Options::parse_options(int argc, const char** argv) {
  if(argc > 0) m_exe_name = argv[0];
  for(int a=1; a<argc; ++a) {
    std::string arg(trim_string(argv[a]));
    if((arg.length() > 2) && (arg[0] == '-') && (arg[1] == '-')) {
      auto pos = arg.find_first_of('=');
      if(pos == std::string::npos) {
        std::string flagn = trim_string(arg.substr(2, arg.length() - 2));
        set<bool>(flagn, true);
      }
      else {
        std::string oname = trim_string(arg.substr(2, pos - 2));
        std::string value = trim_string(arg.substr(pos + 1, arg.length() - pos - 1));
        set_string(oname, value);
      }
    }
  }
}

bool
Options::parse_opts(const std::string& Opts) {
  OptsParser jp(Opts, this);
  return jp.parse();
}

bool
read_opts_file(std::ifstream& fin, Options* opts) {
  std::string res, line;
  while(std::getline(fin, line)) res += line+'\n';
  OptsParser jp(res, opts);
  return jp.parse();
}

bool
Options::parse_opts_file(const std::string& file_name) {
  std::ifstream fin(file_name);
  if(fin.good()) return read_opts_file(fin, this);
  char* path = ::getenv("SFI_PATH");
  if(path) {
    std::ifstream fin2(std::string(path)+"/"+file_name);
    if(fin2.good()) return read_opts_file(fin2, this);
  }
  // TODO: log
  std::cout<<"Options::parse_opts_file(): unable to read from file '"<<file_name<<"'"<<std::endl;
  return false;
}

std::ostream&
Options::print(std::ostream& out) const {
  OptionMap::print(out,2);
  return out;
}

void
Options::print_help(std::ostream& out) const {
  out<<m_exe_name<<" options: "<<std::endl;
  do_print_help(out);
}

bool Options::write(const std::string& file_name) const {
  std::ofstream fout(file_name);
  if(fout.good()) {
    OptionMap::print(fout,2);
    return true;
  }
  else {
    std::cout<<"Options::write(): unable to write to file '"<<file_name<<"'"<<std::endl;
    return false;
  }
}

Options&
Options::set_string(const std::string& _name, const std::string& value) {
  auto parts = split_string(_name, '.');
  auto* sopt = suboptions(parts, true);
  if(!sopt) return *this;
  if(!sopt->has_impl(parts.back())) {
    unsigned uival(0);
    int ival(0);
    double dval(0.0);
    std::vector<std::string> opts = split_quoted_string(value);
    if(opts.size() == 1) {
      if(try_parse_uint(value, uival)) sopt->add(parts.back(), new OptionImpl<unsigned>(uival, parts.back()));
      else if(try_parse_int(value, ival)) sopt->add(parts.back(), new OptionImpl<int>(ival, parts.back()));
      else if(try_parse_double(value, dval)) sopt->add(parts.back(), new OptionImpl<double>(dval, parts.back()));
      else if((value == "true") || (value == "false")) sopt->add(parts.back(), new OptionImpl<bool>((value == "true"), parts.back()));
      else sopt->add(parts.back(), new OptionImpl<std::string>(fix_quoted_string(value), parts.back()));
    }
    else if(opts.size() > 1) {
      bool all_ok = false;
      if(try_parse_uint(opts[0], uival)) {
        std::vector<unsigned> ivec(opts.size(), 0);
        ivec[0] = uival;
        all_ok = true;
        for(size_t i=1; i<opts.size(); ++i) {
          all_ok = all_ok && try_parse_uint(opts[i], uival);
          ivec[i] = uival;
        }
        if(all_ok) sopt->add(parts.back(), new OptionImpl<std::vector<unsigned>>(ivec, parts.back()));
      }
      if(try_parse_int(opts[0], ival)) {
        std::vector<int> ivec(opts.size(), 0);
        ivec[0] = ival;
        all_ok = true;
        for(size_t i=1; i<opts.size(); ++i) {
          all_ok = all_ok && try_parse_int(opts[i], ival);
          ivec[i] = ival;
        }
        if(all_ok) sopt->add(parts.back(), new OptionImpl<std::vector<int>>(ivec, parts.back()));
      }
      if(!all_ok && try_parse_double(opts[0], dval)) {
        std::vector<double> dvec(opts.size(), 0.0);
        dvec[0] = dval;
        all_ok = true;
        for(size_t i=1; i<opts.size(); ++i) {
          all_ok = all_ok && try_parse_double(opts[i], dval);
          dvec[i] = dval;
        }
        if(all_ok) sopt->add(parts.back(), new OptionImpl<std::vector<double>>(dvec, parts.back()));
      }
      if(!all_ok && ((opts[0] == "true") || (opts[0] == "false"))) {
        std::vector<bool> bvec(opts.size(), false);
        bvec[0] = (opts[0] == "true");
        all_ok = true;
        for(size_t i=1; i<opts.size(); ++i) {
          all_ok = all_ok && ((opts[i] == "true") || (opts[i] == "false"));
          bvec[i] = (opts[i] == "true");
        }
        if(all_ok) sopt->add(parts.back(), new OptionImpl<std::vector<bool>>(bvec, parts.back()));
      }
      if(!all_ok) sopt->add(parts.back(), new OptionImpl<std::vector<std::string>>(opts, parts.back()));
    }
  }
  else {
    auto& opt = (*sopt)[parts.back()];
    if(opt.get_type_info() == typeid(std::string)) opt.set(fix_quoted_string(value));
    else opt.set(value);
  }
  return *this;
}

namespace sfi {
  template class OptionBase<int>;
  template class OptionBase<unsigned>;
  template class OptionBase<double>;
  template class OptionBase<bool>;
  template class OptionBase<std::string>;
  template class OptionBase<void>;
  template class OptionBase<OptMap>;
  template class OptionBase<OptVec>;

  template class OptionImpl<int>;
  template class OptionImpl<unsigned>;
  template class OptionImpl<double>;
  template class OptionImpl<bool>;
  template class OptionImpl<std::string>;
  template class OptionImpl<void>;
  template class OptionImpl<OptMap>;
  template class OptionImpl<OptVec>;

  template class BoundOption<int>;
  template class BoundOption<unsigned>;
  template class BoundOption<double>;
  template class BoundOption<bool>;
  template class BoundOption<std::string>;
}
