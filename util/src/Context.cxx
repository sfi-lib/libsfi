#include <Context.h>
#include <ComponentHandler.h>

#ifdef SFI_ENABLE_MP
#include <mp/Dispatcher.h>
#endif

using namespace sfi;

Context::Context():m_component_handler(new ComponentHandler(*this))
#ifdef SFI_ENABLE_MP
,m_dispatcher(nullptr)
#endif
{}

Context::Context(Context&& o):m_component_handler(o.m_component_handler)
#ifdef SFI_ENABLE_MP
,m_dispatcher(o.m_dispatcher)
#endif
{
  o.m_component_handler = 0;
}

Context::~Context() {
  if(m_component_handler) {
    delete m_component_handler;
    m_component_handler = 0;
  }
}
