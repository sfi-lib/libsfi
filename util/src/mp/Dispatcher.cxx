#include <mp/Dispatcher.h>
#include <timer.h>

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

using namespace sfi;
using namespace sfi::mp;

namespace sfi {
  namespace mp {
    class WorkThread {
    public:
      WorkThread(Dispatcher&, size_t id, Dispatcher::InQueueTy& inq, Dispatcher::OutQueueTy& outq);

      void run();

      inline void stop() { m_running.store(false); }

      inline void join() {
        if(m_running.load() == false) m_thread.join();
      }

      inline size_t id() const { return m_id; }

      inline std::thread::id thread_id() const { return m_thread.get_id(); }

      inline SampleExtStat exec_time() const { return m_exec_time; }

      inline size_t tries() const { return m_tries; }

      inline size_t failed_jobs() const { return m_failed; }
    protected:
      size_t m_id;
      Log m_log;
      Dispatcher::InQueueTy& m_in_queue;
      Dispatcher::OutQueueTy& m_out_queue;
      std::atomic<bool> m_running;
      std::thread m_thread;
      SampleExtStat m_exec_time;
      size_t m_tries;
      size_t m_failed;
      Context m_context;
    };
  }
}
Dispatcher::Dispatcher(Context& ctx):
    m_main_context(ctx),
    m_log(Log::get(ctx, "Dispatcher")),
    m_nworkers(0),
    m_enable_multi_processing(false),
    m_running(false),
    m_serial_queue(1000),
    m_io_queue(1000),
    m_input_queue(1000),
    m_output_queue(1000),
    m_main_thread_id(std::this_thread::get_id()),
    m_io_thread(nullptr) {
#ifdef SFI_ENABLE_MP
  ctx.dispatcher(this);
#endif
  std::cout<<"Dispatcher() id: "<<m_main_thread_id<<" "<<std::this_thread::get_id()<<" n workers: "<<m_nworkers<<std::endl;
  m_log.print_level(PrintLevel::Warning);
}

Dispatcher::Dispatcher(Context& ctx, const OptionMap& opts):
    m_main_context(ctx),
    m_log(Log::get(ctx, "Dispatcher")),
    m_nworkers(opts.get_as<int>("nworkers",0)),
    m_enable_multi_processing(opts.get_as<bool>("enable", false)),
    m_running(false),
    m_serial_queue(opts.get_as<unsigned>("serial_timeout", 200)),
    m_io_queue(opts.get_as<unsigned>("io_timeout", 200)),
    m_input_queue(opts.get_as<unsigned>("input_timeout", 200)),
    m_output_queue(opts.get_as<unsigned>("ouput_timeout", 200)),
    m_main_thread_id(std::this_thread::get_id()),
    m_io_thread(nullptr) {
#ifdef SFI_ENABLE_MP
  ctx.dispatcher(this);
#endif
  if(!m_nworkers) m_nworkers = std::thread::hardware_concurrency()/2;
  std::cout<<"Dispatcher() id: "<<m_main_thread_id<<" "<<std::this_thread::get_id()<<" enable: "<<m_enable_multi_processing<<" n workers: "<<m_nworkers<<" opts: "<<opts<<std::endl;
  m_log.print_level(opts.get_as<std::string>("print_level","warning"));
  if(m_enable_multi_processing) start();
}

Dispatcher::~Dispatcher() {
  stop();
}

Dispatcher&
Dispatcher::nworkers(size_t nw) {
  if(m_running.load()) {
    do_log_error("nworkers() id: "<<m_main_thread_id<<" can't set number of workers while running");
  }
  else {
    m_nworkers = nw;
  }
  return *this;
}

void
Dispatcher::start() {
  bool was_running(false);
  if(m_running.compare_exchange_weak(was_running, true)) {
    do_log_info("start() starting: "<<m_nworkers<<" threads");
    m_workers.reserve(m_nworkers);
    for(unsigned i=0; i<m_nworkers; ++i) {
      m_workers.push_back(new WorkThread(*this, i, m_input_queue, m_output_queue));
    }
    m_io_thread = new std::thread(&Dispatcher::handle_io_jobs, this);
  }
  else {
    do_log_warning("start() already running "<<m_nworkers<<" threads");
  }
}

void
Dispatcher::stop() {
  bool was_running(true);
  if(m_running.compare_exchange_weak(was_running, false)) {
    do_log_debug("stop() closing queues");
    m_input_queue.close();
    m_output_queue.close();
    do_log_debug("stop() stopping workers");
    for(auto* w : m_workers) w->stop();
    do_log_debug("stop() joining workers");
    for(auto* w : m_workers) {
      w->join();
      delete w;
    }
    m_workers.clear();
    add_io_job(nullptr);
    m_io_thread->join();
  }
  else {
    do_log_debug("stop() not running");
  }
}



void
Dispatcher::dump() {
  size_t njobs(0);
  for(auto* w : m_workers) {
    auto et = w->exec_time();
    njobs += et.N();
    do_log_info(""<<w->id()<<" njobs: "<<et.N()<<" mean time: "<<et.mean()<<" total: "<<(et.mean()*et.N())<<" n failed: "<<w->failed_jobs()<<" n tries: "<<w->tries());
  }
#ifdef EnableDispatcherStatistics
  do_log_info("total jobs: "<<njobs<<" time jobset="<<m_js_time);
  do_log_info("job total="<<m_job_time);
  do_log_info("job run="<<m_job_run_time);
  do_log_info("job in="<<m_job_in_time);
  do_log_info("job out="<<m_job_out_time);
  m_js_time.clear();
  m_job_time.clear();
  m_job_run_time.clear();
  m_job_in_time.clear();
  m_job_out_time.clear();
#endif
}

const Dispatcher::EndRun Dispatcher::done;

void
Dispatcher::run() {
  if(!is_main_thread()) {
    do_log_error("run() Not in main thread");
    return;
  }
  if(!m_running.load()) {
    do_log_error("run() Not running");
    return;
  }
  Job* job(0);
  while(m_running.load()) {
    if(m_output_queue.pop(job, false)) {
      if(job) {
        handle_finished_job(job);
        job = 0;
      }
      else {
        do_log_error("run() got null job");
      }
    }
    job = 0;
    if(m_serial_queue.pop(job, false)) {
      if(job) {
        do_log_verbose("run() got job "<<job<<" "<<type_name(typeid(*job)));
        job->stop_in();
        JobContainer* js = dynamic_cast<JobContainer*>(job);
        if(js) {
          do_log_debug("run() got job set of size: "<<js->jobs.size());
          for(auto* j : js->jobs) {
            j->start_in();
            m_input_queue.push(j);
          }
        }
        else {
          try {
            do_log_verbose("run() executing: "<<job);
            Timer<true> runt;
            runt.start();
            job->execute(m_main_context);
            job->time_run(runt.stop());
            if(job->next()) {
              add_serial_job(job->next());
              // m_serial_queue.push(job->next());
            }
            else {
              JobContainer* cont = job->parent();
              if(cont) {
                do_log_debug("run() finished job sequence");
                m_job_sequences.erase(cont);
                delete cont;
              }
            }
          }
          catch(...) {
            do_log_error("run() exception while running serial job");
            // TODO: handle failed job sequence
          }
        }
      }
      else {
        do_log_debug("run() got null run function, exiting run loop");
        break;
      }
    }
  }
}

std::vector<std::thread::id>
Dispatcher::worker_thread_ids() const {
  std::vector<std::thread::id> ids;
  ids.reserve(m_nworkers);
  for(auto* w : m_workers) ids.push_back(w->thread_id());
  return ids;
}

Dispatcher&
Dispatcher::add_serial_job(Job* j) {
  if(j) {
    do_log_verbose("add_serial_job() "<<j<<" "<<type_name(typeid(*j)));
    j->start_in();
  }
  else {
    do_log_verbose("add_serial_job() 0");
  }
  m_serial_queue.push(j);
  return *this;
}

Dispatcher&
Dispatcher::add_job_sequence(JobContainer* jsb) {
  do_log_verbose("add_job_sequence() "<<jsb<<" #jobs "<<jsb->jobs.size());
  if(m_log.verbose()) {
    for(auto*j : jsb->jobs) {
      do_log_verbose("add_job_sequence() - "<<j<<" parent: "<<j->parent()<<" next: "<<j->next());
    }
  }
  m_job_sequences.insert(jsb);
  add_serial_job(jsb->jobs.front());
  // m_serial_queue.push(jsb->jobs.front());
  return *this;
}

Dispatcher& Dispatcher::add_io_job(Job* j) {
  m_io_queue.push(j);
  return *this;
}

void
Dispatcher::handle_finished_job(Job* job) {
  job->stop_out();
  JobContainer* js = job->parent();
  if(!js) {
    do_log_error("handle_finished_job() Invalid parent!");
    return;
  }
  if((++js->finished_jobs) == js->jobs.size()) {
    Job* next = js->next();
    if(next) {
      // m_serial_queue.push(next);
      add_serial_job(next);
    }
  }
}

void Dispatcher::handle_io_jobs() {
  do_log_debug("handle_io_jobs() starting");
  bool run = true;
  while(run) {
    Job* j = 0;
    if(m_io_queue.pop(j, true)) {
      if(j) {
        j->execute(m_main_context);
      }
      else run = false;
    }
  }
  do_log_debug("handle_io_jobs() finishing");
}

WorkThread::WorkThread(Dispatcher& disp, size_t id, Dispatcher::InQueueTy& inq, Dispatcher::OutQueueTy& outq):
                    m_id(id), m_log(Log::get(m_context, "WorkThread{"+std::to_string(id)+"}")), m_in_queue(inq), m_out_queue(outq), m_running(true),
                    m_thread(&WorkThread::run, this), m_tries(0), m_failed(0),
                    m_context() {
#ifdef SFI_ENABLE_MP
  m_context.dispatcher(&disp);
#endif // SFI_ENABLE_MP
  unused_parameters(disp);
  do_log_debug("starting: "<<m_id);
  m_log.print_level(PrintLevel::Warning);
}

void
WorkThread::run() {
  do_log_debug("run() ("<<m_id<<") starting...");
  Job* job(0);
  bool has_job(false);
  size_t nhandled(0);

  while((has_job = m_in_queue.pop(job)) || m_running.load()) {
    if(has_job && job) {
      job->stop_in();
      do_log_verbose("run() ("<<m_id<<") running job, queue="<<m_in_queue.size()<<" pushed="<<m_in_queue.pushed()<<" "<<m_in_queue.poped());
      Timer<true> timer;
      timer.start();
      job->status(false);
      try {
        job->execute(m_context);
        job->status(true);
      }
      catch(const std::exception& ex) {
        do_log_error("run() ("<<m_id<<") failed to run job, exception: "<<ex.what());
        ++m_failed;
      }
      catch(...) {
        do_log_error("run() ("<<m_id<<") failed to run job ");
        ++m_failed;
      }
      ++nhandled;
      job->time_run(timer.stop());
      m_exec_time += job->time_run();
      do_log_debug("run() ("<<m_id<<") finished job status: "<<job->status());
      job->start_out();
      m_out_queue.push(job);
    }
    else {
      ++m_tries;
    }
  }
  do_log_debug("run() ("<<m_id<<") stopped");
}
