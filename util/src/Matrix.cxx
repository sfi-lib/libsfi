/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Matrix.h"

#ifdef SFI_OSX
#include <Accelerate/Accelerate.h>
#else
extern "C" {
#include "cblas.h"
}
#endif

namespace sfi {
  template<>
  void Vector<double>::add(const This& o, double scale) {
    assert((o.rows() == this->rows()) && "Invalid vector size");
    ::cblas_daxpy(this->rows(), scale, o.data_ptr(), 1, this->data_ptr(), 1);
  }

  template<>
  void Vector<double>::add(const This& o, This& res, double scale) const {
    assert((o.rows() == this->rows()) && "Invalid vector size");
    assert((res.rows() == this->rows()) && "Invalid vector size");
    ::cblas_dcopy(this->rows(), this->data_ptr(), 1, res.data_ptr(), 1);
    ::cblas_daxpy(this->rows(), scale, o.data_ptr(), 1, res.data_ptr(), 1);
  }

  template<>
  void Vector<double>::assign(const This& o, double scale) {
    assert((o.rows() == this->rows()) && "Invalid vector size");
    ::cblas_dcopy(this->rows(), o.data_ptr(), 1, this->data_ptr(), 1);
    if(scale != 1.0) ::cblas_dscal(this->rows(), scale, this->data_ptr(), 1);
  }

  template<>
  double Vector<double>::mult(const This& o) const {
    assert((o.rows() == this->rows()) && "Invalid vector size");
    return ::cblas_ddot(this->rows(), this->data_ptr(), 1, o.data_ptr(), 1);
  }

  template<>
  void MatrixSym<double>::mult(const Vector<double>& x, Vector<double>& res, double scale) const {
    assert(this->m_rows == res.rows() && "Invalid size");
    assert(this->m_rows == x.rows() && "Invalid size");
    // TODO: x may have adopted, incx != 1
    ::cblas_dsymv(CblasRowMajor, CblasLower,this->m_rows, scale, this->data_ptr(), this->row_length(), x.data_ptr(), 1, 0.0, res.data_ptr(), 1);
  }

  template<>
  void MatrixSym<double>::rank1_update(const Vector<double>& x, double scale) {
    // TODO: x may have adopted, incx != 1
    ::cblas_dsyr(CblasRowMajor, CblasLower,this->m_rows, scale, x.data_ptr(), 1, this->data_ptr(), this->row_length());
  }

  template class Vector<double>;
  template class Vector<std::complex<double>>;
  template class Matrix<double>;
  template class Matrix<std::complex<double>>;
  template class MatrixDia<double>;
  template class MatrixDia<std::complex<double>>;
  template class MatrixSqr<double>;
  template class MatrixSqr<std::complex<double>>;
  template class MatrixSym<double>;
  template class MatrixSym<std::complex<double>>;
  template class MatrixTria<double>;
  template class MatrixTria<std::complex<double>>;
}
