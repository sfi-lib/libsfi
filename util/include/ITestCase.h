#ifndef sfi_test_ITestCase_h
#define sfi_test_ITestCase_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string>
#include <IComponent.h>

namespace sfi {
  namespace test {

    struct TestCaseSummary {
      size_t ntests;
      size_t nfailed_tests;
      size_t nunits;
      size_t nfailed_units;

      inline TestCaseSummary& operator+=(const TestCaseSummary& o) {
        this->ntests += o.ntests;
        this->nfailed_tests += o.nfailed_tests;
        this->nunits += o.nunits;
        this->nfailed_units += o.nfailed_units;
        return *this;
      }
    };
    class ITestCase : public IComponent {
    public:
      virtual ~ITestCase() {}

      virtual const std::string& get_name() const = 0;

      virtual bool do_initialize() = 0;

      virtual bool do_setup() = 0;

      virtual bool do_run() = 0;

      virtual void do_print_summary() = 0;

      virtual TestCaseSummary get_summary() = 0;
    };

  }
}

#endif // sfi_test_ITestCase_h
