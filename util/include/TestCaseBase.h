#ifndef sfi_test_TestCaseBase_h
#define sfi_test_TestCaseBase_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <ITestCase.h>
#include <Log.h>
#include <type_util.h>
#include <util.h>
#include <Options.h>

#include <string>
#include <functional>
#include <iostream>
#include <vector>
#include <complex>

namespace sfi {
  class Context;

  namespace test {
    class TestCaseBase : public ITestCase {
    public:
      TestCaseBase(Context& ctx, const std::string& name);

      virtual ~TestCaseBase();

      // ---- Implementation of IComponent -----

      virtual const std::string& get_type() const;

      virtual const OptionMap& get_options() const;

      virtual OptionMap& get_options();

      // ---- Implementation of ITestCase -----

      virtual const std::string& get_name() const;

      virtual bool do_initialize();

      virtual bool do_setup() = 0;

      virtual bool do_run();

      virtual void do_print_summary();

      virtual TestCaseSummary get_summary() { return m_summary; }

      // ---- public methods -----
    protected:
      void test(const std::string& name, bool b);

      template<class _T>
      void test_eq(const std::string& name, const _T& a, const _T& b) {
        if(!m_current_entry) {
          do_log_error("test_eq() no current entry!");
          return;
        }
        ++m_current_entry->ntests;
        if(a != b) {
          do_log_error("Test case '"<<m_current_entry->name<<"' failed '"<<name<<"' : ("<<sfi::type_name<_T>()<<") "<<a<<" != "<<b);
          ++m_current_entry->errors;
        }
      }

      template<class _T>
      void test_gt(const std::string& name, const _T& a, const _T& b) {
        if(!m_current_entry) {
          do_log_error("test_gt() no current entry!");
          return;
        }
        ++m_current_entry->ntests;
        if(a <= b) {
          do_log_error("Test case '"<<m_current_entry->name<<"' failed '"<<name<<"' : ("<<sfi::type_name<_T>()<<") "<<a<<" < "<<b);
          ++m_current_entry->errors;
        }
      }

      /** Test if b < a, fail if b < ae, warn if aw < b < ae */
      template<class _T>
      void test_gt(const std::string& name, const _T& ae, const _T& aw, const _T& b) {
        if(!m_current_entry) {
          do_log_error("test_gt() no current entry!");
          return;
        }
        if(aw > ae) {
          do_log_error("test_gt() ae must be > aw!");
        }
        ++m_current_entry->ntests;
        if(ae <= b) {
          do_log_error("Test case '"<<m_current_entry->name<<"' failed '"<<name<<"' : ("<<sfi::type_name<_T>()<<") "<<ae<<" < "<<b);
          ++m_current_entry->errors;
        }
        else if(aw <= b) {
          do_log_warning("Test case '"<<m_current_entry->name<<"' '"<<name<<"' : ("<<sfi::type_name<_T>()<<") "<<aw<<" < "<<b<<" < "<<ae);
        }
      }

      template<class _T1, class _T2>
      void test_eq_type(const std::string& name) {
        if(!m_current_entry) {
          do_log_error("test_eq_type() no current entry!");
          return;
        }
        ++m_current_entry->ntests;
        if(!teq<_T1, _T2>::value()) {
          do_log_error("Test case '"<<m_current_entry->name<<"' failed '"<<name<<"' : types '"<<sfi::type_name<_T1>()<<"'  and '"<<sfi::type_name<_T2>()<<"' are not equal");
          ++m_current_entry->errors;
        }
      }

      template<class _T1, class _T2>
      void test_eq_type_dyn(const std::string& name, const _T1* o) {
        if(!m_current_entry) {
          do_log_error("test_eq_type_dyn() no current entry!");
          return;
        }
        ++m_current_entry->ntests;
        const _T2* oo = dynamic_cast<const _T2*>(o);
        if(oo == 0) {
          do_log_error("Test case '"<<m_current_entry->name<<"' failed '"<<name<<"' : Instance of type '"<<sfi::type_name<_T1>()<<"' is not an '"<<sfi::type_name<_T2>()<<"'");
          ++m_current_entry->errors;
        }
      }

      template<class _Exception, class _Lambda>
      void test_exception(const std::string& name, const _Lambda& t) {
        if(!m_current_entry) {
          do_log_error("test_exception() no current entry!");
          return;
        }
        ++m_current_entry->ntests;
        bool success = false;
        try { t(); }
        catch(const _Exception& e) {
          success = true;
        }
        catch(...) {
          do_log_error("Test case '"<<m_current_entry->name<<"' failed '"<<name<<"' unknown exception");
        }

        if(!success) {
          do_log_error("Test case '"<<m_current_entry->name<<"' failed '"<<name<<"' : exception '"<<type_name<_Exception>()<<" not caught");
          ++m_current_entry->errors;
        }
      }

      void test_approx_abs(const std::string& name, double a, double b, double tol, double tol_e = -1);

      /** check if |(a - b)/b| < tol */
      template<class _T>
      inline void test_approx_rel(const std::string& name, _T a, _T b, double tol) {
        if(!m_current_entry) {
          do_log_error("test_approx_rel() no current entry!");
          return;
        }
        ++m_current_entry->ntests;
        if((b == 0.0) || (std::abs((a - b)/b) > tol)) {
          do_log_error("test_approx_rel() Test case '"<<m_current_entry->name<<"' failed '"<<name<<"' : |("<<a<<" - "<<b<<")/"<<b<<" | = "<<std::abs((a - b)/b)<<" > "<<tol);
          ++m_current_entry->errors;
        }
      }

      void test_approx_abs(const std::string& name, std::complex<double> a, std::complex<double> b, double tol);

      /**
       * check if |(a - b)/b| < tol
       * If the value is above tol_w a warning is emitted, if above tol_e then the test fails.
       */
      template<class _T>
      void test_approx_rel(const std::string& name, _T a, _T b, double tol_w, double tol_e) {
        if(!m_current_entry) {
          do_log_error("test_approx_rel() no current entry!");
          return;
        }
        if(tol_e < tol_w) {
          do_log_error("test_approx_rel() Invalid tolerances, tol_w ("<<tol_w<<") > tol_e ("<<tol_e<<")");
          return;
        }
        ++m_current_entry->ntests;
        const double d = std::abs((a - b)/b);
        if((b == 0.0) || (d > tol_e)) {
          do_log_error("test_approx_rel() Test case '"<<m_current_entry->name<<"' failed '"<<name<<"' : |("<<a<<" - "<<b<<")/"<<b<<" | = "<<std::abs((a - b)/b)<<" > "<<tol_e);
          ++m_current_entry->errors;
        }
        else if(d > tol_w) {
          do_log_warning("test_approx_rel() Test case '"<<m_current_entry->name<<"' test '"<<name<<"' : |("<<a<<" - "<<b<<")/"<<b<<" | = "<<std::abs((a - b)/b)<<" > "<<tol_w);
        }
      }

      void add(const std::string& name, const std::function<void()>& func);

      Log m_log;

      const std::string& case_name() const;
    protected:
      Context& m_context;
    private:
      struct TestCaseEntry {
        TestCaseEntry(const std::string& _name, const std::function<void()>& _func):
          name(_name), func(_func), ntests(0), errors(0), exception(false) {
        }
        std::string name;
        std::function<void()> func;
        unsigned ntests;
        unsigned errors;
        bool exception;
      };

      std::string m_name;
      std::vector<TestCaseEntry> m_entries;
      TestCaseEntry* m_current_entry;
      TestCaseSummary m_summary;
      OptionMap m_options;
    };
  }
}
#define DefineTestCase(CASE) \
  namespace sfi { class IComponent; } \
extern "C" { \
  sfi::IComponent* sfi_factory_##CASE(sfi::Context& ctx, sfi::OptionMap&) { \
    return new CASE(ctx); \
  } \
}

#endif // sfi_test_TestCaseBase_h
