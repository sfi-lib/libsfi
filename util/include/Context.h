#ifndef sfi_Context_h
#define sfi_Context_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <MemoryPool.h>

#include <random>

namespace sfi {
  class ComponentHandler;
#ifdef SFI_ENABLE_MP
  namespace mp {
    class Dispatcher;
  }
#endif

  /**
   * @class sfi::Context
   *
   * Provides resources to parts of the computation.
   */
  class Context {
  public:
    Context();

    Context(Context&&);

    ~Context();

    using RandomTy = std::mt19937;

    inline RandomTy& rnd_gen() { return m_rnd_gen; }

    inline MemoryPool& memory_pool() { return m_memory_pool; }

    inline ComponentHandler* component_handler() const { return this->m_component_handler; }

    template<class _T, class ... _Args>
    inline _T* create_object(_Args&& ... args) {
      return m_memory_pool.pool<_T>()->get(std::forward<_Args>(args)...);
    }
#ifdef SFI_ENABLE_MP
    inline mp::Dispatcher* dispatcher() { return m_dispatcher; }
    inline void dispatcher(mp::Dispatcher* disp) { m_dispatcher = disp; }
#endif
  protected:
    RandomTy m_rnd_gen;
    MemoryPool m_memory_pool;
    ComponentHandler* m_component_handler;
#ifdef SFI_ENABLE_MP
    mp::Dispatcher* m_dispatcher;
#endif
  };

  template<class _Random>
  class Random {
  public:
    Random(_Random& rnd):m_rnd(rnd) {}

    inline double poisson(double mu) { return m_poisson(m_rnd, std::poisson_distribution<>::param_type(mu)); }

    inline double uniform(double x1, double x2) { return m_uniform(m_rnd, std::uniform_real_distribution<>::param_type(x1, x2)); }

    inline double gaus(double m, double s) { return m_gaus(m_rnd, std::normal_distribution<>::param_type(m, s)); }

    inline double expo(double m) { return m_expo(m_rnd, std::exponential_distribution<>::param_type(m)); }

    inline double breit_wigner(double m, double s) { return m_cauchy(m_rnd, std::cauchy_distribution<>::param_type(m,s)); }
  protected:
    _Random& m_rnd;
    std::uniform_real_distribution<> m_uniform;
    std::poisson_distribution<> m_poisson;
    std::normal_distribution<> m_gaus;
    std::exponential_distribution<> m_expo;
    std::cauchy_distribution<> m_cauchy;
  };

  template<class _Context>
  using t_ContextRandom = typename _Context::RandomTy;
}
#endif // sfi_Context_h
