#ifndef sfi_IComponent_h
#define sfi_IComponent_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string>

#include "Options.h"

namespace sfi {
  class IComponent {
  public:
    virtual ~IComponent() {}

    /** @return the logical type of the component, not necessarily equal to the type name */
    virtual const std::string& get_type() const = 0;

    /** @return The name of the component instance */
    virtual const std::string& get_name() const = 0;

    /** @return Options for this component */
    virtual const OptionMap& get_options() const = 0;

    /** @return Options for this component */
    virtual OptionMap& get_options() = 0;
  };
}

#endif // sfi_IComponent_h
