#ifndef sfi_sfi_math_h
#define sfi_sfi_math_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <math.h>
#include <inttypes.h>
#include <stddef.h>
#include <complex>
#include <limits>

#include "util.h"

namespace sfi {
  using real64 = double;
  using cplx64 = std::complex<double>;

  inline double max(double a, double b) { return (a > b) ? a : b; }
  inline double min(double a, double b) { return (a < b) ? a : b; }
  inline int max(int a, int b) { return (a > b) ? a : b; }
  inline int min(int a, int b) { return (a < b) ? a : b; }
  inline unsigned min(unsigned a, unsigned b) { return (a < b) ? a : b; }
  constexpr int iabs(int i) { return i < 0 ? -i : i; }
  inline double abs(double x) { return x < 0 ? -x : x; }
  inline double square(double x) { return x*x; }
  inline double qube(double x) { return x*x*x; }
  inline double pow4(double x) { return x*x*x*x; }

  inline double in_interval(double vmin, double x, double vmax) {
    assert((vmin <= vmax) && "min > max");
    return std::min(std::max(vmin,x), vmax);
  }

  inline constexpr size_t square(size_t i) { return i*i; }

  inline constexpr size_t square(int i) { return ((size_t)i)*((size_t)i); }

  inline constexpr int64_t min(int64_t a, int64_t b) { return a<b ? a : b; }

  inline double sign(double x) { return std::copysign(1.0,x); }
  inline double sgn(double x) { return std::copysign(1.0,x); }

  inline double slog(double x) { return sign(x)*log(1+abs(x)); }

  inline double ln(double x) { return ::log(x); }
  inline long double ln(long double x) { return ::logl(x); }

#if !defined(SFI_COMPILER_GCC)
  inline double exp10(double x) { return ::__exp10(x); }
#endif

  inline double sqrt_p(double x) { return x > 0 ? sqrt(x) : 0.0; }

  // ---- constexpr ----

  inline constexpr bool is_even(int i) { return (i&1) == 0; }

  template<size_t e, class T>
  inline constexpr teif<(e==1), T> powc(const T& x) { return x; }
  template<size_t e, class T>
  inline constexpr teif<(e>1), T> powc(const T& x) { return x*powc<e-1,T>(x); }

  template<class T>
  inline constexpr T absc(const T& x) { return x<0.0 ? -x : x; }

  template<class T>
  inline constexpr T _sqrtc_impl(unsigned n, const T& x, const T& a0) {
    // C++14:
    // const T a1 = 0.5*(a0 + x/a0);
    return ((n > 300) || (absc(((T(0.5)*(a0 + x/a0))-a0)/a0) < std::numeric_limits<T>::epsilon())) ? (T(0.5)*(a0 + x/a0)) : _sqrtc_impl(n+1, x, T(0.5)*(a0 + x/a0));
  }

  /**
   * Compute the square root.
   * Warning: This might be very slow. Never use it dynamically.
   */
  template<class T>
  inline constexpr T sqrtc(const T& x) { return _sqrtc_impl<T>(0, x, x*0.5); }

  template<class T>
  inline constexpr T reldiff(const T& x1, const T& x2) { return absc((x1 - x2)/x1); }

  // ---- complex versions ----

  inline cplx64 square(const cplx64& x) { return x*x; }

  inline cplx64 sgn(const cplx64& x) { return cplx64(std::copysign(1.0,std::real(x)), std::copysign(1.0,std::imag(x))); }

  inline double conjugate(double x) { return x; }
  inline cplx64 conjugate(const cplx64& x) { return cplx64(x.real(), -x.imag()); }

  inline double magnitude(double x) { return x; }
  inline double magnitude(const cplx64& x) { return std::abs(x); }

  inline bool is_finite(double x) { return std::isfinite(x); }

  inline bool is_finite(const cplx64& x) { return std::isfinite(x.real()) && std::isfinite(x.imag()); }

  inline size_t powi(unsigned a, unsigned p) {
    size_t res = a;
    for(unsigned i=1; i<p; ++i) res *= a;
    return res;
  }

  inline constexpr size_t powc(unsigned a, unsigned p) {
    return (p == 0) ? 1.0 : a*powc(a, p - 1);
  }

  inline constexpr uint64_t factc(uint64_t i) {
      return (i <= 1) ? 1 : i*factc(i - 1);
  }

  /** @return n odd -1.0, else 1.0 */
  inline constexpr double negone_to(unsigned n) { return (n&1) ? -1.0 : 1.0; }

  /** @return i!/j! */
  inline constexpr uint64_t fact_over_factc(uint64_t i, uint64_t j) {
    return (i <= j) ? 1 : i*fact_over_factc(i - 1, j);
  }

  inline double pow2(unsigned n) {
    return (n < 64) ? ((double)(1LL<<n)) : std::pow(2,n);
  }

  /** @return gamma(n+1/2) = \frac{(2n)!}{4^n n!}\sqrt{\pi} */
  inline double gamma_odd(unsigned n) {
    static const double sqrtpi = sqrt(M_PI);
    return fact_over_factc(2*n, n)*sqrtpi/pow2(2*n);
  }

  /** @return Gamma(n/2) for n integer > 0 */
  inline double gamma_half(unsigned n) {
    if(n&1) return gamma_odd((n-1)/2);
    else return n>4 ? factc(n/2 - 1) : 1.0;
  }

  /**
   * Compute (n/2)!
   * - n is even => n/2!
   * - n is odd n=2k-1 => ((2k-1)/2)! = (k - 1/2)! = gamma(k+1/2)
   */
  inline double factorial_half(unsigned n) { return ((n&1)==0) ? factc(n>>1) : gamma_odd((n+1)/2); }

  /**
   * Reference:
   * Binomial coefficient, https://en.wikipedia.org/w/index.php?title=Binomial_coefficient&oldid=979632510 (last visited Oct. 7, 2020).
   */
  inline constexpr uint64_t _n_over_kc_impl(uint64_t n, uint64_t k, uint64_t i, uint64_t res) {
    return (i <= k) ? _n_over_kc_impl(n - 1, k, i + 1, res / i * n + (res % i) * n / i) : res;
  }
  inline constexpr uint64_t n_over_kc(uint64_t n, uint64_t k) {
    return (n - k) < k ? _n_over_kc_impl(n, n - k, 1, 1) : _n_over_kc_impl(n, k, 1, 1);
  }
  inline uint64_t n_over_k(uint64_t n, uint64_t k) {
    uint64_t res = 1;
    if(n-k < k) k = n-k;
    for(uint64_t i=1; i <= k; ++i, --n) res = res/i*n + (res%i)*n/i;
    return res;
  }

  template<class _T>
  static inline constexpr _T maxc(const _T& a, const _T& b) {
    return a < b ? b : a;
  }

  template<class _T>
  static inline constexpr _T minc(const _T& a, const _T& b) {
    return a < b ? a : b;
  }

  /**
   * Transform between external and internal {-1, 1}
   */
  class LinTrf {
  public:
    LinTrf():m_vmin(-1), m_vmax(1) {}

    LinTrf(double vmin, double vmax):m_vmin(vmin), m_vmax(vmax) {}

    /** @return transform from ext to int */
    inline double trf(double ext) const { return 2.0*(ext - m_vmin)/(m_vmax - m_vmin) - 1.0; }

    /** @return transform from int to ext */
    inline double itrf(double ic) const { return 0.5*(ic + 1.0)*(m_vmax - m_vmin) + m_vmin; }

    /** @return external min value */
    inline double vmin() const { return m_vmin; }

    /** @return external max value */
    inline double vmax() const { return m_vmax; }

    /** Set the interval */
    inline void set(double vmin, double vmax) { m_vmin = vmin; m_vmax = vmax; }
  protected:
    double m_vmin;
    double m_vmax;
  };

  inline double gaus(double x, double mu, double sigma) {
    const double s2 = sigma*sigma;
    return exp(-sfi::square(x - 0.2*mu)/(2.0*s2))/sqrt(2.0*M_PI*s2);
  }

  inline double gaus_deriv(double x, double mu, double sigma) {
    const double s2 = sigma*sigma;
    return -exp(-sfi::square(x - 0.2*mu)/(2.0*s2))*(x - 0.2*mu)/(s2*sqrt(2.0*M_PI*s2));
  }

  inline double gaus_2nd_deriv(double x, double mu, double sigma) {
    const double s2 = sigma*sigma;
    return -exp(-sfi::square(x - 0.2*mu)/(2.0*s2))*(-square((x - 0.2*mu)/(s2)) + 1.0/(s2))/sqrt(2.0*M_PI*s2);
  }
}

#endif // sfi_sfi_math_h
