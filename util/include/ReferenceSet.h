#ifndef sfi_util_ReferenceSet_h
#define sfi_util_ReferenceSet_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <vector>
#include <initializer_list>

namespace sfi {
  // TODO: Move to util.h and add unit test
  template<class _Ret, class _Class, class ... _Args>
  inline _Ret _t_member_return_type(_Ret (_Class::*)(_Args...)) {}
  template<class _Ret, class _Class, class ... _Args>
  inline _Ret _t_member_return_type(_Ret (_Class::*)(_Args...) const) {}

  template<class _T, class = void> struct _t_has_clone : tfalse {};
  template<class _T>
  struct _t_has_clone<_T, tvalue<t_void<decltype(_t_member_return_type( &_T::clone))>>> : ttrue {};

  template<class _T, class = void> struct _t_has_release : tfalse {};
  template<class _T>
  struct _t_has_release<_T, tvalue<t_void<decltype(_t_member_return_type( &_T::release))>>> : ttrue {};

  template<class _T>
  struct ref_iterator {
    ref_iterator(typename std::vector<_T*>::const_iterator iter):m_iter(iter) {}

    inline _T& operator*() {
      return **m_iter;
    }
    inline ref_iterator& operator++() {
      ++m_iter;
      return *this;
    }
    inline bool operator!=(const ref_iterator& o) {
      return m_iter != o.m_iter;
    }
  protected:
    typename std::vector<_T*>::const_iterator m_iter;
  };

  template<class _T>
  struct ref_const_iterator {
    ref_const_iterator(typename std::vector<_T*>::const_iterator iter):m_iter(iter) {}

    inline const _T& operator*() {
      return **m_iter;
    }
    inline ref_const_iterator& operator++() {
      ++m_iter;
      return *this;
    }
    inline bool operator!=(const ref_const_iterator& o) {
      return m_iter != o.m_iter;
    }
  protected:
    typename std::vector<_T*>::const_iterator m_iter;
  };
  /**
   * @class sfi::ReferenceSet
   *
   * Indexed set of objects stored by pointer but accessed by reference.
   * The set manages its stored objects, and deletes or recycles them
   * when the set is destroyed.
   * Cannot be copied.
   * Is able to produce a generic container that contains a copy of the
   * pointers, thus it cannot outlive its parent set.
   *
   * ReferenceSet<MyType, IMyInterface> rs;
   * ReferenceSet<IMyInterface, IMyInterface> gen_set = rs.generic();
   */
  template<class _T, class _TGeneric>
  class ReferenceSet {
    static_assert(std::is_base_of<_TGeneric, _T>::value, "Type myst be subclass of generic");
  public:
    using This = ReferenceSet<_T, _TGeneric>;
    using iterator = ref_iterator<_T>;
    using const_iterator = ref_const_iterator<_T>;

    template<class _T2, class _TGeneric2> friend class ReferenceSet;

    ReferenceSet():m_owns_objects(true) {}

    ReferenceSet(size_t size):m_owns_objects(true) { this->m_objects.reserve(size); }

    ReferenceSet(const std::initializer_list<_T*>& objs, bool owns_objects = true):m_objects(objs), m_owns_objects(owns_objects) { }

    ReferenceSet(std::vector<_T*>&& objs, bool owns_objects = true):m_objects(std::move(objs)), m_owns_objects(owns_objects) {
      objs.clear();
    }

    ReferenceSet(const std::vector<_TGeneric*>& objs, bool owns_objects = true):m_owns_objects(owns_objects) {
      assign(objs);
    }

    template<class _TG>
    ReferenceSet(std::vector<_TG*>&& objs, bool owns_objects = true, teif<!teq<_T, _TG>::value()>* = 0):m_owns_objects(owns_objects) {
      assign(objs);
      objs.clear();
    }

    ReferenceSet(This&& o):m_objects(std::move(o.m_objects)), m_owns_objects(o.m_owns_objects) {
      o.m_objects.clear();
    }

    ReferenceSet(This& o):m_objects(std::move(o.m_objects)), m_owns_objects(o.m_owns_objects) {
      o.m_objects.clear();
    }

    template<class _T2>
    ReferenceSet(ReferenceSet<_T2, _TGeneric>&& o):m_objects(o.m_objects.begin(), o.m_objects.end()), m_owns_objects(o.m_owns_objects) {
      o.m_objects.clear();
    }

    ~ReferenceSet() { this->clear(); }

    /** Add an object */
    inline This& add(_T* obj) { this->m_objects.push_back(obj); return *this; }

    /** Add all from another set */
    inline This& add(This&& objs) {
      this->m_objects.insert(this->m_objects.end(), objs.m_objects.begin(), objs.m_objects.end());
      objs.m_objects.clear();
      return *this;
    }

    /** @return object with given index */
    inline const _T& operator[](size_t i) const {
      assert(i < this->m_objects.size() && "Invalid index");
      return *this->m_objects[i];
    }
    inline _T& operator[](size_t i) {
      assert(i < this->m_objects.size() && "Invalid index");
      return *this->m_objects[i];
    }

    /** Add an object */
    inline This& operator+=(_T* obj) { return add(obj); }

    /** Add another set */
    inline This& operator+=(This&& o) { return add(std::forward<This>(o)); }

    /** @return Size of set */
    inline size_t size() const { return this->m_objects.size(); }

    /** @return if empty */
    inline bool empty() const { return this->m_objects.empty(); }
    inline operator bool() const { return !empty(); }

    /** Clear the set, deletes instances if applicable */
    inline void clear() {
      destroy<_T>();
      this->m_objects.clear();
    }

    /** @return Generic version of this set */
    inline ReferenceSet<_TGeneric, _TGeneric> generic() const {
      return ReferenceSet<_TGeneric, _TGeneric>(std::vector<_TGeneric*>(this->m_objects.begin(), this->m_objects.end()), false);
    }

    /** Clone all objects in the set */
    This clone(bool deep = false) const { return clone_impl<_T>(deep); }

    /** Assign from another set, delete previous instances */
    template<class _T2>
    inline This& operator=(ReferenceSet<_T2, _TGeneric>&& o) {
      return claim(std::forward<ReferenceSet<_T2, _TGeneric>>(o));
    }
    template<class _T2>
    inline This& operator=(ReferenceSet<_T2, _TGeneric>& o) {
      return claim(std::move(o));
    }

    inline iterator begin() { return iterator(this->m_objects.begin()); }
    inline iterator end() { return iterator(this->m_objects.end()); }

    inline const_iterator begin() const { return const_iterator(this->m_objects.begin()); }
    inline const_iterator end() const { return const_iterator(this->m_objects.end()); }

    /**
     * Clear the set of objects, but don't delete the instances.
     * Warning: In order not to leak memory, the objects must be explicitly deleted.
     */
    inline void detach_objects() { this->m_objects.clear(); }

    inline bool owns_objects() const { return m_owns_objects; }
  protected:
    ReferenceSet(const This& o) = delete;

    template<class _T2>
    inline void assign(const std::vector<_T2*>& objs) {
      m_objects.reserve(objs.size());
      for(auto* obj : objs) {
        _T* tobj = dynamic_cast<_T*>(obj);
        if(tobj) this->m_objects.push_back(tobj);
      }
    }

    template<class _T2>
    inline This& claim(ReferenceSet<_T2, _TGeneric>&& o) {
      destroy<_T>();
      this->m_objects = std::move(o.m_objects);
      o.m_objects.clear();
      this->m_owns_objects = o.m_owns_objects;
      return *this;
    }

    inline This& operator=(const This& o) = delete;

    template<class _T2>
    inline void destroy(teif<tnot<_t_has_release<_T2>>::value()>* = 0) {
      if(m_owns_objects) for(auto* obj : this->m_objects) delete obj;
    }

    template<class _T2>
    inline void destroy(teif<_t_has_release<_T2>::value()>* = 0) {
      if(m_owns_objects) for(auto* obj : this->m_objects) obj->release();
    }

    template<class _T2>
    This clone_impl(bool deep, teif<_t_has_clone<_T2>::value()>* = 0) const {
      This res(size());
      for(auto& s : this->m_objects) res.add(s->clone(deep));
      return res;
    }
    template<class _T2>
    This clone_impl(bool /*deep*/, teif<!_t_has_clone<_T2>::value()>* = 0) const {
      return This();
    }

    std::vector<_T*> m_objects;
    bool m_owns_objects;
  };

  template<class _Transf, class _TGeneric>
  class ReferenceSet2D {
  public:
    using This = ReferenceSet2D<_Transf, _TGeneric>;
    using ElementType = ReferenceSet<_Transf, _TGeneric>;
    template<class _Transf2, class _TGeneric2> friend class ReferenceSet2D;

    ReferenceSet2D(unsigned rows) {
      m_sets.reserve(rows);
      for(unsigned i=0; i<rows; ++i) m_sets.emplace_back();
    }

    ReferenceSet2D() = default;

    ReferenceSet2D(This&& trfs) = default;

    ~ReferenceSet2D() { }

    /** @return Set with given index */
    inline const ElementType& operator[](size_t i) const {
      assert((i < this->m_sets.size()) && "Invalid index");
      return this->m_sets[i];
    }
    inline ElementType& operator[](size_t i) {
      assert((i < this->m_sets.size()) && "Invalid index");
      return this->m_sets[i];
    }

    /** @return Size of set */
    inline size_t size() const { return this->m_sets.size(); }

    /** @return if empty */
    inline bool empty() const { return m_sets.empty(); }

    inline operator bool() const { return !empty(); }

    inline ReferenceSet2D<_TGeneric, _TGeneric> generic() const {
      ReferenceSet2D<_TGeneric, _TGeneric> res(size());
      for(unsigned i=0; i<size(); ++i) res[i] = m_sets[i].generic();
      return res;
    }

    inline This& operator=(This&& o) = default;

    template<class _Transf2>
    inline This& operator=(ReferenceSet2D<_Transf2, _TGeneric>&& o) {
      return assign<_Transf, _Transf2>(std::forward<ReferenceSet2D<_Transf2, _TGeneric>>(o));
    }
    template<class _Transf2>
    inline This& operator=(ReferenceSet2D<_Transf2, _TGeneric>& o) {
      return assign<_Transf, _Transf2>(std::move(o));
    }
  protected:
    ReferenceSet2D(const This& trfs) = delete;

    inline This& operator=(const This& o) = delete;

    template<class _Transf1, class _Transf2>
    inline This& assign(ReferenceSet2D<_Transf2, _TGeneric>&& o, teif<teq<_Transf1, _TGeneric>::value()>* = 0) {
      const size_t N(o.size());
      for(size_t i = 0; i<N; ++i) m_sets.emplace_back(std::move(o.m_sets[i]));
      o.m_sets.clear();
      return *this;
    }

    std::vector<ReferenceSet<_Transf, _TGeneric>> m_sets;
  };
}

#endif // sfi_util_ReferenceSet_h
