#ifndef sfi_Buffer_h
#define sfi_Buffer_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <vector>
#include <initializer_list>
#include <cstdlib>
#include <complex>
#include <algorithm>

#include "util.h"

namespace sfi {
  constexpr const double g_buffer_prealloc = 1.3;

  /**
   * Basic typed memory buffer.
   * Can operate in two modes: It either owns its data, or uses external data.
   * In the first case the buffer may be resized.
   * In the second case the buffer pointer is fixed (but the elements can be modified)
   *
   * When resizing, the new capacity will be larger than necessary by an
   * amount determined by g_buffer_prealloc (e.g. 1.3 means 30% more memory will
   * be allocated than requested);
   */
  template<class _T>
  class Buffer {
  public:
    using This = Buffer<_T>;
    using ConstValType = tcond<std::is_arithmetic<_T>::value, _T, const _T&>;

    /** Allocate of specific size */
    Buffer(size_t s = 0, ConstValType ival = _T()):m_data(0), m_size(0), m_capacity(0), m_owns_memory(true) {
      resize_and_init(s, ival);
    }

    /** Adopt a memory buffer */
    Buffer(size_t s, _T* data, size_t stride = 0):m_data(data), m_size(s), m_capacity(stride), m_owns_memory(false) { }

    Buffer(const std::initializer_list<_T>& il):Buffer(il.size()) {
      // TODO: use insert instead
      std::copy(il.begin(), il.end(), this->m_data);
    }

    Buffer(const std::vector<_T>& o):Buffer(o.size()) {
      std::copy_n(o.data(), o.size(), this->m_data);
    }

    Buffer(const This& o):Buffer(o.size()) {
      std::copy_n(o.data(), o.size(), this->m_data);
    }

    Buffer(This&& o):m_data(o.m_data), m_size(o.m_size), m_capacity(o.m_capacity), m_owns_memory(o.m_owns_memory) {
      o.m_data = 0;
      o.m_size = o.m_capacity = 0;
      o.m_owns_memory = false;
    }

    ~Buffer() {
      if(m_owns_memory && m_data) {
        ::free(m_data);
        m_data = 0;
      }
    }

    enum Error { OK=0, MemoryNotOwned=1, InvalidData=2, InvalidSizeForAppend=3 };

    This& operator=(const This& o);

    This& operator=(This&& o);

    inline This& operator=(const std::initializer_list<_T>& il) {
      if(m_owns_memory) resize_and_insert(il.size(), il.begin(), false);
      return *this;
    }

    inline This& operator=(const std::vector<_T>& v) {
      if(m_owns_memory) resize_and_insert(v.size(), v.data(), false);
      return *this;
    }

    /** @return number of elements */
    inline size_t size() const { return this->m_size; }

    /** set number of elements */
    inline bool size(size_t s, ConstValType ival = _T()) { return this->resize_and_init(s, ival) == OK; }

    /** @return the maximum number of elements before a resize is needed */
    inline size_t capacity() const { return this->m_capacity; }

    /** @return If the buffer owns its memory */
    inline bool owns_memory() const { return this->m_owns_memory; }

    /** @return element number i */
    inline _T& operator[](size_t i) {
      assert((i < this->m_size) && "Index out of bounds");
      return this->m_data[i];
    }
    inline ConstValType operator[](size_t i) const {
      assert((i < this->m_size) && "Index out of bounds");
      return this->m_data[i];
    }

    /** @return data pointer */
    inline _T* data() { return this->m_data; }
    inline const _T* data() const { return this->m_data; }

    /** @return first memory pos */
    inline _T* begin() { return this->m_data; }
    inline const _T* begin() const { return this->m_data; }

    /** @return past last memory pos */
    inline _T* end() { return this->m_data + this->m_size; }
    inline const _T* end() const { return this->m_data + this->m_size; }

    inline Error append(const This& o) {
      return resize_and_insert(o.size(), o.m_data, true);
    }
  protected:
    /** Change buffer size. Preserves data. */
    Error resize_and_init(size_t val_size, ConstValType ival);
    Error resize_and_insert(size_t osize, const _T* odata, bool append);

    _T* m_data;
    size_t m_size;
    size_t m_capacity;
    bool m_owns_memory;
  };

  extern template class Buffer<int>;
  extern template class Buffer<double>;
  extern template class Buffer<std::complex<double>>;
}

template<class _T>
sfi::Buffer<_T>& sfi::Buffer<_T>::operator=(const This& o) {
  if(this != &o) {
    if(!o.owns_memory()) {
      if(m_owns_memory && m_data) ::free(m_data);
      m_data = o.m_data;
      m_size = o.m_size;
      m_capacity = o.m_capacity;
      m_owns_memory = false;
    }
    else {
      if(!m_owns_memory) {
        m_data = 0;
        m_size = m_capacity = 0;
      }
      resize_and_insert(o.size(), o.data(), false);
    }
  }
  return *this;
}

template<class _T>
sfi::Buffer<_T>& sfi::Buffer<_T>::operator=(This&& o) {
  if(this != &o) {
    if(m_owns_memory && m_data) ::free(m_data);
    m_size = o.m_size;
    m_capacity = o.m_capacity;
    m_data = o.m_data;
    m_owns_memory = o.m_owns_memory;
    o.m_data = 0;
    o.m_capacity = o.m_size = 0;
    o.m_owns_memory = false;
  }
  return *this;
}

template<class _T>
typename sfi::Buffer<_T>::Error sfi::Buffer<_T>::resize_and_init(size_t val_size, ConstValType ival) {
  if(!m_owns_memory) return MemoryNotOwned;
  if(!val_size) {
    m_size = 0;
    return OK;
  }
  if(!m_data) {
    // allocate the requested amount
    m_data = (_T*)std::malloc(sizeof(_T)*val_size);
    std::fill_n(m_data, val_size, ival);
    m_capacity = m_size = val_size;
  }
  else if(val_size < m_capacity) m_size = val_size;
  else {
    size_t n = ((double)sizeof(_T)*val_size)*g_buffer_prealloc;
    _T* data = (_T*)std::malloc(n);
    std::copy_n(m_data, m_size, data);
    std::fill(data + m_size, data + val_size, ival);
    ::free(m_data);
    m_data = data;
    m_size = val_size;
    m_capacity = n/sizeof(_T);
  }
  return OK;
}

template<class _T>
typename sfi::Buffer<_T>::Error sfi::Buffer<_T>::resize_and_insert(size_t osize, const _T* odata, bool append) {
  if(!m_owns_memory) return MemoryNotOwned;
  if(!osize) {
    // Inserting 0 values is always OK
    if(!append) m_size = 0;
    return OK;
  }
  if(odata == 0) return InvalidData;
  const size_t tot_size = (append ? (osize+m_size) : osize);
  if(!m_data) {
    // allocate the requested amount
    m_data = (_T*)std::malloc(sizeof(_T)*osize);
    std::copy_n(odata, osize, m_data);
    m_capacity = m_size = osize;
  }
  // Is the capacity enough
  else if(tot_size <  m_capacity) {
    if(append) std::copy_n(odata, osize, m_data + m_size);
    else std::copy_n(odata, osize, m_data);
    m_size = tot_size;
  }
  else {
    size_t n = ((double)sizeof(_T)*tot_size)*g_buffer_prealloc;
    _T* data = (_T*)std::malloc(n);
    if(append) {
      std::copy_n(m_data, m_size, data);
      std::copy_n(odata, osize, data + m_size);
    }
    else std::copy_n(odata, osize, data);
    ::free(m_data);
    m_data = data;
    m_size = tot_size;
    m_capacity = n/sizeof(_T);
  }
  return OK;
}

#endif // sfi_Buffer_h
