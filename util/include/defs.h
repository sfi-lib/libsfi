#ifndef sfi_defs_h
#define sfi_defs_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <functional>
#include <string>

const double DoDebug = true;
const double DoVerbose = false;

const double EnableDebugLog = true;
const double EnableVerboseLog = true;

#define EnableDispatcherStatistics 1
#define EnableLockedQueueStatistics 1

#if defined(__APPLE__) && defined(__MACH__)
#define SFI_OSX
#else
#undef SFI_OSX
#endif

#if defined(__clang__)
#define SFI_COMPILER_CLANG
#elif defined(__GNUC__) || defined(__GNUG__)
#define SFI_COMPILER_GCC
#endif

#if defined(SFI_OSX) && defined(SFI_ENABLE_SEMAPHORE)
#define EnableSemaphore 1
#else
#undef EnableSemaphore
#endif

#if defined(SFI_ENABLE_MP)
#define EnableMP 1
constexpr const bool g_enable_mp = true;
#else
#undef EnableMP
constexpr const bool g_enable_mp = false;
#endif

#if defined(SFI_ALP_MAX_L)
constexpr const unsigned g_alp_max_L = SFI_ALP_MAX_L;
#else
constexpr const unsigned g_alp_max_L = 20;
#endif

namespace sfi {
  using func_t = std::function<double(const double*, const double*)>;
}

#endif // sfi_defs_h
