#ifndef sfi_Options_h
#define sfi_Options_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <map>
#include <string>
#include <iostream>
#include <sstream>

#include "Exception.h"
#include "string_util.h"
#include "type_util.h"
#include "util.h"
#include "Matrix.h"

namespace sfi {
  class OptsParser;
  class Options;
  class Option;
  using OptMap = std::map<std::string, Option*>;
  using OptVec = std::vector<Option*>;
  template<class _T> class OptionImpl;
  using OptionMap = OptionImpl<OptMap>;
  using OptionVec = OptionImpl<OptVec>;

  class OptionException : public Exception {
  public:
    OptionException(const std::string& _what):Exception(_what) {}
  };

  class Option {
  public:
    friend OptionMap;
    friend OptionVec;

    virtual ~Option();

    inline const std::string& name() const { return m_name; }
    inline Option& name(const std::string& _n) { m_name = _n; return *this; }

    inline bool is_required() const { return m_required; }
    inline Option& is_required(bool b) { m_required = b; return *this; }

    inline const std::string& help() const { return m_help; }
    inline Option& help(const std::string& _h) { m_help = _h; return *this; }

    template<class _T> inline Option& operator=(const _T& value) {
      if(typeid(_T) == get_type_info()) set_value(&value);
      return *this;
    }

    Option& operator=(const Option& op);

    // ---- interface  ----

    virtual const std::type_info& get_type_info() const = 0;

    virtual bool set(const std::string& val) = 0;

    virtual bool set_value(const void*) = 0;

    virtual const void* get_value_address() const = 0;

    virtual void do_print(std::ostream&) const = 0;

    virtual void do_print_help(std::ostream&) const = 0;

    virtual bool get_is_valid() const;

    virtual void set_is_valid(bool b);

    enum Type { Value, Vector, Object };
    virtual Type get_type() const = 0;

    virtual Option& operator[](const std::string&);

    virtual const Option& operator[](const std::string&) const;

    /** @return A deep clone of this Option that should be able to outlive this */
    virtual Option* clone() const = 0;

    bool convert(uint32_t& dst) const;

    bool convert(int32_t& dst) const;

    bool convert(uint64_t& dst) const;

    bool convert(int64_t& dst) const;

    bool convert(double& dst) const;

    bool convert(std::string& dst) const;

    bool convert(std::vector<bool>::reference dst) const;

    bool convert(bool& dst) const;

    bool convert(size_t& dst) const;

    bool convert(sfi::Vector<double>& dst) const;

    template<class _T> bool convert(_T& dst) const {
      if(get_type_info() == typeid(_T)) {
        dst = *static_cast<const _T*>(get_value_address());
        return true;
      }
      return false;
    }

    template<class _T> _T value() const {
      _T res;
      if(!convert(res)) throw OptionException("Option '"+name()+"' of type '"+type_name(get_type_info())+"' cannot be obtained as '"+type_name<_T>()+"'");
      return res;
    }
  protected:
    Option(const std::string& name = "", bool is_req = false, const std::string& help = "");

    Option(const Option& o);

    unsigned m_references;
    std::string m_name, m_help;
    bool m_required, m_is_set;
  };

  template<class _T>
  class OptionBase : public Option {
  public:
    using ref_ty = tvalue<tcond<teq<_T,void>::value(), tvalued<void>, tref_<_T>>>;
    using const_ref_ty = tvalue<tcond<teq<_T,void>::value(), tvalued<void>, tref_<const _T>>>;

    virtual ~OptionBase() {}

    // ---- implementation of Option ----

    virtual const std::type_info& get_type_info() const { return typeid(_T); }

    virtual Type get_type() const { return tis_vector<_T>::value() ? Option::Vector : Option::Value; }

    // ---- public methods ----

    virtual const_ref_ty get_value() const = 0;

    virtual ref_ty get_value() = 0;
  protected:
    OptionBase(const std::string& name = "", bool is_req = false, const std::string& help = ""):Option(name, is_req, help) { }

    OptionBase(const OptionBase& o):Option(o) {}
  };

  template<class _T>
  inline void print_option_value(std::ostream& out, const _T& v) { out<<std::boolalpha<<v; }
  inline void print_option_value(std::ostream& out, const std::string& v) { out<<"\""<<v<<"\""; }
  template<class _T>
  inline void print_option_value(std::ostream& out, const std::vector<_T>& v) {
    out<<"[";
    for(size_t i=0; i<v.size(); ++i) {
      if(i) out<<", ";
      print_option_value(out,v[i]);
    }
    out<<"]";
  }

  template<class _T>
  class OptionImpl : public OptionBase<_T> {
  public:
    using Base = OptionBase<_T>;

    OptionImpl(const std::string& name = "", bool is_req = false, const std::string& help = ""):Base(name, is_req, help) {}

    OptionImpl(const _T& val, const std::string& name = "", bool is_req = false, const std::string& help = ""):Base(name, is_req, help), m_value(val) {
      this->m_is_set = true;
    }

    virtual ~OptionImpl() {}

    virtual bool set(const std::string& val) { return (this->m_is_set = from_string<_T>::apply(val, m_value)); }

    virtual bool set_value(const void* val) {
      m_value = *static_cast<const _T*>(val);
      return (this->m_is_set = true);
    }

    virtual const void* get_value_address() const { return &m_value; }

    virtual const _T& get_value() const { return m_value; }

    virtual _T& get_value() { return m_value; }

    virtual void do_print(std::ostream& out) const { print_option_value(out, m_value); }

    virtual void do_print_help(std::ostream& out) const {
      out<<"default: "<<get_value()<<" "<<(this->m_required ? "required " : "optional ")<<this->m_help;
    }

    virtual Option* clone() const { return new OptionImpl<_T>(*this); }

    // ---- public methods ----

    inline const _T& value() const { return m_value; }
  protected:
    OptionImpl(const OptionImpl& o):Base(o), m_value(o.m_value) {}

    _T m_value;
  };

  template<>
  class OptionImpl<void> : public OptionBase<void> {
  public:
    using Base = OptionBase<void>;

    OptionImpl(const std::string& name = "", bool is_req = false, const std::string& help = ""):Base(name, is_req, help) {}

    virtual ~OptionImpl() {}

    virtual bool set(const std::string&) { return false; }

    virtual bool set_value(const void*) { return false; }

    virtual const void* get_value_address() const { return 0; }

    virtual void get_value() const { }

    virtual void get_value() { }

    virtual void do_print(std::ostream&) const { }

    virtual void do_print_help(std::ostream&) const { }

    // TODO: ?
    virtual Option* clone() const { return 0; }
  };

  template<class _T>
  class BoundOption : public OptionBase<_T> {
  public:
    using Base = OptionBase<_T>;

    BoundOption(_T& val, const std::string& name = "", bool is_req = false, const std::string& help = ""):Base(name, is_req, help), value(val) {}

    virtual ~BoundOption() {}

    virtual bool set(const std::string& val) { return (this->m_is_set = from_string<_T>::apply(val, value)); }

    virtual bool set_value(const void* val) {
      value = *static_cast<const _T*>(val);
      return (this->m_is_set = true);
    }

    virtual const void* get_value_address() const { return &value; }

    virtual const _T& get_value() const { return value; }

    virtual _T& get_value() { return value; }

    virtual void do_print(std::ostream& out) const { print_option_value(out, value); }

    virtual void do_print_help(std::ostream& out) const {
      out<<"default: "<<get_value()<<" "<<(this->m_required ? "required " : "optional ")<<this->m_help;
    }

    virtual Option* clone() const { return new OptionImpl<_T>(value, this->m_name, this->m_required, this->m_help); }
  protected:
    _T& value;
  };

  template<>
  class OptionImpl<OptMap> : public OptionBase<OptMap> {
  public:
    friend OptsParser;
    friend Options;

    using Base = OptionBase<OptMap>;
    using This = OptionImpl<OptMap>;

    explicit OptionImpl(const std::string& name = "", bool is_req = false, const std::string& help = "");

    /** Copy all options, if shallow then keep instances */
    OptionImpl(OptionImpl& o, bool shallow = false);

    OptionImpl(const OptionImpl& o);

    OptionImpl(OptionImpl&& o);

    This& operator=(const This& o);
    
    virtual ~OptionImpl();

    virtual bool set(const std::string& val);

    virtual bool set_value(const void* val);

    virtual const void* get_value_address() const;

    virtual const OptMap& get_value() const;

    virtual OptMap& get_value();

    virtual void do_print(std::ostream& out) const ;

    virtual void do_print_help(std::ostream& out) const;

    virtual Type get_type() const;

    virtual Option& operator[](const std::string&);

    virtual const Option& operator[](const std::string&) const;

    virtual Option* clone() const;

    // ---- public methods ----

    using iterator = OptMap::iterator;
    inline iterator begin() { return m_options.begin(); }
    inline iterator end() { return m_options.end(); }

    using const_iterator = OptMap::const_iterator;
    inline const_iterator begin() const { return m_options.begin(); }
    inline const_iterator end() const { return m_options.end(); }

    inline size_t size() const { return m_options.size(); }

    This& copy(const std::string& name, const Option& op);

    template<class _T> This& bind(const std::string& name, _T& var, bool is_req = false, const std::string& help = "") {
      auto parts = split_string(name,'.');
      auto* so = suboptions(parts, true);
      if(so) so->bind_impl(parts.back(), var, is_req, help);
      return *this;
    }

    template<class _T> This& define(const std::string& name, bool is_req = false, const std::string& help = "", const _T& def = _T()) {
      auto parts = split_string(name,'.');
      auto* so = suboptions(parts, true);
      if(so) so->define_impl<_T>(parts.back(), is_req, help, def);
      return *this;
    }

    inline bool has(const std::string& name) const {
      auto parts = split_string(name,'.');
      auto* so = suboptions(parts);
      if(so) return so->has_impl(parts.back());
      else return false;
    }

    template<class _T>
    inline bool has_type(const std::string& name) const { return has_type(name, typeid(_T)); }

    inline bool has_sub(const std::string& name) const { return has_type(name, typeid(OptMap)); }

    template<class _T>
    inline This& set(const std::string& name, const _T& val, bool is_req = false, const std::string& help = "") {
      auto parts = split_string(name,'.');
      auto* so = suboptions(parts, true);
      if(so) so->set_impl(parts.back(), val, is_req, help);
      return *this;
    }

    inline This& set(const std::string& name, const sfi::Vector<double>& val, bool is_req = false, const std::string& help = "") {
      return set(name, std::vector<double>(val.begin(), val.end()), is_req, help);
    }

    inline This& set(const std::string& /*name*/, const sfi::Vector<std::complex<double>>& /*val*/, bool /*is_req*/ = false, const std::string& /*help*/ = "") {
      return *this;
    }

    template<class _T>
    inline const _T& get(const std::string& name) const {
      auto parts = split_string(name,'.');
      auto* so = suboptions(parts);
      if(so) return so->get_impl<_T>(parts.back());
      else throw OptionException("get: option '"+name+"' with type '"+type_name<_T>()+"' not found");
    }

    template<class _T>
    inline _T& get(const std::string& name) {
      auto parts = split_string(name,'.');
      auto* so = suboptions(parts);
      if(so) return so->get_impl<_T>(parts.back());
      else throw OptionException("get: option '"+name+"' with type '"+type_name<_T>()+"' not found");
    }

    template<class _T>
    inline const This& get_if(const std::string& name, _T& val) const {
      auto parts = split_string(name,'.');
      auto* so = suboptions(parts);
      if(so) so->get_if_impl<_T>(parts.back(), val);
      return *this;
    }

    template<class _T>
    inline _T get_as(const std::string& name) const {
      auto parts = split_string(name,'.');
      auto* so = suboptions(parts);
      if(so) return so->get_as_impl<_T>(parts.back());
      else throw OptionException("get_as: option '"+name+"' with type '"+type_name<_T>()+"' not found");
    }

    template<class _T>
    inline _T get_as(const std::string& name, const _T& val) const {
      if(has_type<_T>(name)) return get_as<_T>(name);
      else return val;
    }
    
    This& sub(const std::string& name, bool define_if_missing = false);
    const This& sub(const std::string& name) const;

    OptionVec& sub_vec(const std::string& name, bool define_if_missing = false);
    const OptionVec& sub_vec(const std::string& name) const;
    inline bool has_sub_vec(const std::string& name) const { return has_type(name, typeid(OptVec)); }
  protected:
    iterator find(const std::string& name);

    bool has_impl(const std::string& name) const;

    bool has_type(const std::string& name, const std::type_info& ti) const;

    bool has_type_impl(const std::string& name, const std::type_info& ti) const;

    void add(const std::string& name, Option* opt);

    bool remove(const std::string& name);

    void remove(Option* o);

    bool replace(Option* new_op);

    /** @return named option in this */
    Option* option(const std::string& name);
    const Option* option(const std::string& name) const;

    bool can_cast(const std::type_info& src_ty, const std::type_info& dst_ty, const void* src, void* dst);

    template<class _T>
    inline bool bind_impl(const std::string& name, _T& val, bool is_req = false, const std::string& help = "") {
      if(!has_impl(name)) {
        add(name, new BoundOption<_T>(val, name, is_req, help));
        return true;
      }
      else {
        Option* opt = option(name);
        if(opt->get_type_info() == typeid(_T)) {
          BoundOption<_T>* bopt = dynamic_cast<BoundOption<_T>*>(opt);
          if(bopt) throw OptionException("bind: option '"+name+"' with type '"+type_name<_T>()+"' already bound in '"+this->name()+"'");
          else {
            OptionImpl<_T>* opti = dynamic_cast<OptionImpl<_T>*>(opt);
            val = opti->value();
            add(name, new BoundOption<_T>(val, name, is_req, help));
            return true;
          }
        }
        else if(can_cast(opt->get_type_info(), typeid(_T), opt->get_value_address(), &val)) {
          add(name, new BoundOption<_T>(val, name, is_req, help));
          return true;
        }
        else throw OptionException("bind: option '"+name+"' with type '"+type_name<_T>()+"' already defined with type '"+type_name(opt->get_type_info())+"' in '"+this->name()+"'");
      }
    }

    template<class _T>
    bool define_impl(const std::string& name, bool is_req = false, const std::string& help = "", const _T& def = _T()) {
      if(!has_impl(name)) {
        auto* opt = new OptionImpl<_T>(def, name, is_req, help);
        add(name, opt);
        opt->set_is_valid(false);
        return true;
      }
      else throw OptionException("define: option '"+name+"' with type '"+type_name<_T>()+"' already defined in '"+this->name()+"'");
    }

    template<class _T>
    inline bool set_impl(const std::string& name, const _T& val, bool is_req = false, const std::string& help = "") {
      if(!has_impl(name)) {
        add(name, new OptionImpl<_T>(val, name, is_req, help));
        return true;
      }
      else {
        Option* opt = m_options[name];
        if(typeid(_T) == opt->get_type_info()) {
          opt->set_value(&val);
          return true;
        }
        else throw OptionException("set: option '"+name+"' with type '"+type_name<_T>()+"' already defined with another type '"+type_name(opt->get_type_info())+"' in '"+this->name()+"'");
      }
      return false;
    }

    template<class _T>
    inline const _T& get_impl(const std::string& name) const {
      Option* opt = typed_option(name, typeid(_T));
      if(opt) {
        OptionBase<_T>* topt = dynamic_cast<OptionBase<_T>*>(opt);
        if(topt) return topt->get_value();
        else throw OptionException("get: option '"+name+"' with type '"+type_name<_T>()+"' is no OptionBase<"+type_name<_T>()+"> but '"+type_name(opt->get_type_info())+"' '"+type_name(typeid(*opt))+"' in '"+this->name()+"'");
      }
      throw OptionException("get_impl: option '"+name+"' with type '"+type_name<_T>()+"' not found in '"+this->name()+"'");
    }

    template<class _T>
    inline _T get_as_impl(const std::string& name) const {
      auto* opt = option(name);
      if(opt) {
        _T res;
        if(!opt->convert(res)) throw OptionException("Option '"+name+"' of type '"+type_name(opt->get_type_info())+"' cannot be obtained as '"+type_name<_T>()+"' in '"+this->name()+"'");
        return res;
      }
      else throw OptionException("get_as_impl: option '"+name+"' not found in '"+this->name()+"'");
    }

    template<class _T>
    inline void get_if_impl(const std::string& name, _T& val) const {
      auto* opt = option(name);
      if(opt) {
        if(!opt->convert(val)) throw OptionException("Option '"+name+"' of type '"+type_name(opt->get_type_info())+"' cannot be obtained as '"+type_name<_T>()+"' in '"+this->name()+"'");
      }
    }

    template<class _T>
    inline _T& get_impl(const std::string& name) {
      Option* opt = typed_option(name, typeid(_T));
      if(opt) {
        OptionBase<_T>* topt = dynamic_cast<OptionBase<_T>*>(opt);
        if(topt) return topt->get_value();
        else throw OptionException("get: option '"+name+"' with type '"+type_name<_T>()+"' is no OptionBase<"+type_name<_T>()+"> in '"+this->name()+"'");
      }
      throw OptionException("get_impl: option '"+name+"' with type '"+type_name<_T>()+"' not found in '"+this->name()+"'");
    }

    This* suboptions(const std::vector<std::string>& name, bool create_missing = false);

    const This* suboptions(const std::vector<std::string>& name) const;

    Option* suboption(const std::string& name);

    const Option* suboption(const std::string& name) const;

    Option* typed_option(const std::string& name, const std::type_info& type) const;

    void print(std::ostream& out, unsigned indent) const;

    OptMap m_options;
  };

  template<>
  class OptionImpl<OptVec> : public OptionBase<OptVec> {
  public:
    using Base = OptionBase<OptVec>;

    OptionImpl(const std::string& name = "", bool is_req = false, const std::string& help = "");

    virtual ~OptionImpl();

    virtual bool set(const std::string& val);

    virtual bool set_value(const void* val);

    virtual const void* get_value_address() const;

    virtual const OptVec& get_value() const;

    virtual OptVec& get_value();

    virtual void do_print(std::ostream& out) const;

    virtual void do_print_help(std::ostream& out) const;

    virtual Option* clone() const;

    void add(Option*);

    inline size_t size() const { return m_value.size(); }

    inline Option* operator[](size_t i) { return m_value[i]; }
    inline const Option* operator[](size_t i) const { return m_value[i]; }
  protected:
    OptionImpl(const OptionImpl& o);

    OptVec m_value;
  };

  class Options : public OptionMap {
  public:
    friend OptsParser;

    Options(int argc, char** argv);
    Options(int argc, const char** argv);

    Options(const Options&);

    Options(Options&&);

    Options();

    ~Options();

    void parse_options(int argc, char** argv);
    void parse_options(int argc, const char** argv);

    bool parse_opts(const std::string& Opts);

    bool parse_opts_file(const std::string& file_name);

    inline const std::string& exe_name() const { return this->m_exe_name; }

    std::ostream& print(std::ostream&) const;

    void print_help(std::ostream&) const;

    bool write(const std::string& file_name) const;
  protected:
    Options& set_string(const std::string& name, const std::string& value);

    std::string m_exe_name;
  };

  inline std::ostream& operator<<(std::ostream& out, const Options& opts) { return opts.print(out); }
  inline std::ostream& operator<<(std::ostream& out, const Option& opt) { opt.do_print(out); return out; }

  extern template class OptionBase<int>;
  extern template class OptionBase<unsigned>;
  extern template class OptionBase<double>;
  extern template class OptionBase<bool>;
  extern template class OptionBase<std::string>;
  extern template class OptionBase<void>;
  extern template class OptionBase<OptMap>;
  extern template class OptionBase<OptVec>;

  extern template class OptionImpl<int>;
  extern template class OptionImpl<unsigned>;
  extern template class OptionImpl<double>;
  extern template class OptionImpl<bool>;
  extern template class OptionImpl<std::string>;
  extern template class OptionImpl<void>;
  extern template class OptionImpl<OptMap>;
  extern template class OptionImpl<OptVec>;

  extern template class BoundOption<int>;
  extern template class BoundOption<unsigned>;
  extern template class BoundOption<double>;
  extern template class BoundOption<bool>;
  extern template class BoundOption<std::string>;
}

#endif // sfi_Options_h
