#ifndef sfi_Log_h
#define sfi_Log_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <sstream>
#include <iomanip>

namespace sfi {
  class Context;

  enum PrintLevel {
    Trace=0, Verbose = 1, Debug = 2, Message = 3, Warning = 4, Error = 5, Info = 6, OK = 7
  };

  class Log {
    struct LogEntry {
      LogEntry(const std::string& name):
        m_name(name), m_print_level(Warning), m_precison(8) {}
      const std::string m_name;
      PrintLevel m_print_level;
      unsigned m_precison;
    };

  public:
    struct LogStream {
      LogStream(const LogStream&) = delete;
      LogStream(LogStream&& ls):m_out(std::move(ls.m_out)) {}
      LogStream() {}
      std::ostringstream m_out;
      template<class _T> inline LogStream operator<<(const _T& o) {
        m_out<<o;
        return std::move(*this);
      }
    };
    Log(const Log&);

    static Log get(Context& ctx, const std::string& name);

    inline const std::string& name() const { return this->m_entry->m_name; }

    inline PrintLevel print_level() const { return this->m_entry->m_print_level; }

    inline void print_level(PrintLevel pl) { this->m_entry->m_print_level = pl; }
    void print_level(const std::string& pl);

    inline void precision(unsigned p) { this->m_entry->m_precison = p; }

    inline bool trace() const { return this->m_entry->m_print_level <= Trace; }
    inline bool verbose() const { return this->m_entry->m_print_level <= Verbose; }
    inline bool debug() const { return this->m_entry->m_print_level <= Debug; }
    inline bool message() const { return this->m_entry->m_print_level <= Message; }
    inline bool warning() const { return this->m_entry->m_print_level <= Warning; }
    inline bool error() const { return this->m_entry->m_print_level <= Error; }

    LogStream log(PrintLevel pl) const;

    void end(LogStream) const;
    
    inline Context& context() const { return m_context; }
  protected:
    Log(Context&, LogEntry *entry);
    Context& m_context;
    LogEntry* m_entry;
  };
}

#define do_log_trace(MSG)   do { if(EnableVerboseLog && this->m_log.trace())   { this->m_log.end(this->m_log.log(sfi::PrintLevel::Trace)<<MSG);}} while(0);
#define do_log_verbose(MSG) do { if(EnableVerboseLog && this->m_log.verbose()) { this->m_log.end(this->m_log.log(sfi::PrintLevel::Verbose)<<MSG); }} while(0);
#define do_log_debug(MSG)   do { if(EnableDebugLog && this->m_log.debug())     { this->m_log.end(this->m_log.log(sfi::PrintLevel::Debug)<<MSG); }} while(0);
#define do_log_message(MSG) do { if(this->m_log.message())                     { this->m_log.end(this->m_log.log(sfi::PrintLevel::Message)<<MSG); }} while(0);
#define do_log_warning(MSG) do { if(this->m_log.warning())                     { this->m_log.end(this->m_log.log(sfi::PrintLevel::Warning)<<MSG); }} while(0);
#define do_log_error(MSG)   do { if(this->m_log.error())                       { this->m_log.end(this->m_log.log(sfi::PrintLevel::Error)<<MSG); }} while(0);
#define do_log_info(MSG)    do {                                                 this->m_log.end(this->m_log.log(sfi::PrintLevel::Info)<<MSG); } while(0);
#define do_log_ok(MSG)      do {                                                 this->m_log.end(this->m_log.log(sfi::PrintLevel::OK)<<MSG); } while(0);

#define log_trace(LOG, MSG)   do { if(EnableVerboseLog && LOG.trace())   { Log z_log = LOG; z_log.end(z_log.log(sfi::PrintLevel::Trace)<<MSG); }} while(0);
#define log_verbose(LOG, MSG) do { if(EnableVerboseLog && LOG.verbose()) { Log z_log = LOG; z_log.end(z_log.log(sfi::PrintLevel::Verbose)<<MSG); }} while(0);
#define log_debug(LOG, MSG)   do { if(EnableDebugLog && LOG.debug())     { Log z_log = LOG; z_log.end(z_log.log(sfi::PrintLevel::Debug)<<MSG); }} while(0);
#define log_message(LOG, MSG) do { if(LOG.message())                     { Log z_log = LOG; z_log.end(z_log.log(sfi::PrintLevel::Message)<<MSG); }} while(0);
#define log_warning(LOG, MSG) do { if(LOG.warning())                     { Log z_log = LOG; z_log.end(z_log.log(sfi::PrintLevel::Warning)<<MSG); }} while(0);
#define log_error(LOG, MSG)   do { if(LOG.error())                       { Log z_log = LOG; z_log.end(z_log.log(sfi::PrintLevel::Error)<<MSG); }} while(0);
#define log_info(LOG, MSG)    do {                                         Log z_log = LOG; z_log.end(z_log.log(sfi::PrintLevel::Info)<<MSG); } while(0);
#define log_ok(LOG, MSG)      do {                                         Log z_log = LOG; z_log.end(z_log.log(sfi::PrintLevel::OK)<<MSG); } while(0);

#endif // sfi_Log_h
