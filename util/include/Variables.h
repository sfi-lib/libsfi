#ifndef sfi_Variables_h
#define sfi_Variables_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string>
#include <vector>
#include <array>
#include <memory>

#include <math.h>

#include <Options.h>
#include <Exception.h>

namespace sfi {
  class VariableException : public Exception {
  public:
    VariableException(const std::string& msg):Exception(msg) {}
  };

  class Variable {
  public:
    Variable(const std::string& name="", double minval = 0.0, double maxval = 0.0);
    
    inline std::string& name() { return m_name; }
    inline const std::string& name() const { return m_name; }
    inline Variable& name(const std::string& name) { m_name = name; return *this; }

    inline std::string& title() { return m_title; }
    inline const std::string& title() const { return m_title; }
    inline Variable& title(const std::string& title) { m_title = title; return *this; }

    /** External min and max */
    inline double minval() const { return m_ext_minval; }
    inline double maxval() const { return m_ext_maxval; }
    Variable& external_interval(double emi, double ema);
    inline bool external_interval_set() const { return this->m_external_set; }

    /** Intermediary min and max */
    inline double intermediary_minval() const { return m_intermediary_minval; }
    inline double intermediary_maxval() const { return m_intermediary_maxval; }
    Variable& intermediary_interval(double imi, double ima);
    inline bool intermediary_interval_set() const { return this->m_intermediary_set; }

    /** Internal min and max */
    inline static constexpr double internal_minval() { return -1; }
    inline static constexpr double internal_maxval() { return 1; }

    /** Transform from external to internal coordinates */
    double vtrans(double x) const;

    /** Transform from external to internal coordinates and return the weight*/
    double vtrans(double x, double& xo) const;

    /** Transform from internal to external coordinates */
    double vitrans(double y) const;

    enum TransType { Linear = 0, Ln = 1, Acos = 2, Atan = 3, Cos = 4 };

    inline TransType trans_type() const { return m_trans_type; }
    Variable& trans_type(TransType tt);

    /** @return If this variable is locked */
    inline bool is_locked() const { return this->m_locked; }

    /** Prepare transforms, make sure all intervals are set, prevent further changes */
    bool lock();

    /** Enable changes */
    inline Variable& unlock() { this->m_locked = false; return *this; }

    std::ostream& print(std::ostream& out) const;

    void write_vtrans_cpp(const std::string& varname, std::ostream& out) const;
  protected:
    std::string m_name, m_title;
    double m_ext_minval, m_ext_maxval;
    double m_intermediary_minval, m_intermediary_maxval;

    // derived
    double m_tr_intermediary_minval;
    double m_ext_2_interm, m_interm_2_int;
    double m_interm_2_ext, m_int_2_interm;

    TransType m_trans_type;
    bool m_external_set, m_intermediary_set, m_locked;
  };

  inline std::ostream& operator<<(std::ostream& out, const Variable& var) {
    return var.print(out);
  }

  class Variables {
  public:
    Variables();

    Variables(size_t n);

    Variables(const Variables& v);

    Variables(const OptionVec& ov);

    void init(size_t n);

    void init(const OptionVec&);

    void write(OptionVec&) const;

    inline size_t nvariables() const { return m_vars.size(); }

    inline std::string& name(size_t i) { return m_vars[i].name(); }
    inline const std::string& name(size_t i) const { return m_vars[i].name(); }

    inline Variable& operator[](size_t i) { return m_vars[i]; }
    inline const Variable& operator[](size_t i) const { return m_vars[i]; }

    inline Variable& at(size_t i) { return m_vars[i]; }
    inline const Variable& at(size_t i) const { return m_vars[i]; }

    /** Transform from external to internal and return weight for the transformation */
    inline double transform(const double* x, double* xo) const {
      double w = 1.0;
      const size_t ND(nvariables());
      for(unsigned d=0; d<ND; ++d) w *= m_vars[d].vtrans(x[d], xo[d]);
      return w;
    }

    std::ostream& print(std::ostream& out) const;

    bool lock();

    /** Compare names and external intervals to other */
    bool compare(const Variables&) const;

    Variable* lookup(const std::string& name);
  protected:
    std::vector<Variable> m_vars;
  };

  inline std::ostream& operator<<(std::ostream& out, const Variables& vars) {
    return vars.print(out);
  }

  using PVariables = std::shared_ptr<Variables>;
}

#endif // sfi_Variables_h
