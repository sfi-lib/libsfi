#ifndef sfi_LatexTable_h
#define sfi_LatexTable_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string>
#include <vector>
#include "Matrix.h"

namespace sfi {
  class LatexTable {
  public:
    using This = LatexTable;

    LatexTable(const std::string& name, unsigned ncols, unsigned nrows);

    LatexTable(This&&) = default;

    This& operator=(This&& o) = default;

    /** Set the number of rows, data is preserved (in case Nrows >= Ncurrentrows)*/
    LatexTable& rows(size_t rows);

    std::ostream& print(std::ostream& out) const;

    inline This& precision(unsigned p) { m_precision = p; return *this; }

    inline This& col_title(unsigned c, const std::string& title) {
      m_col_titles[c] = title;
      return *this;
    }

    inline This& row_title(unsigned r, const std::string& title) {
      m_row_titles[r] = title;
      return *this;
    }

    inline double& operator()(unsigned r, unsigned c) { return m_data(r, c); }

    inline This& caption(const std::string& cap) { m_caption = cap; return *this; }

    inline const Matrix<double>& data() const { return m_data; }
  protected:
    std::string m_name;
    std::vector<std::string> m_col_titles;
    std::vector<std::string> m_row_titles;
    Matrix<double> m_data;
    unsigned m_precision;
    std::string m_caption;
  };

  inline std::ostream& operator<<(std::ostream& out, const sfi::LatexTable& tab) {
    return tab.print(out);
  }
}

#endif // sfi_LatexTable_h
