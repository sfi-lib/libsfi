#ifndef sfi_type_util_h
#define sfi_type_util_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string>

namespace sfi {
  std::string type_name(const std::type_info& ti);

  template<class _T>
  inline std::string type_name() { return type_name(typeid(_T)); }

  template<class _T>
  inline std::string type_name(const _T&) { return type_name(typeid(_T)); }
}

#endif // sfi_type_util_h
