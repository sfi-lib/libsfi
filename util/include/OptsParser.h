#ifndef sfi_OptsParser_h
#define sfi_OptsParser_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Options.h"

namespace sfi {

  /**
   * @class sfi::OptsParser
   *
   * To be used with Options. This parser builds an Options.
   * Vectors of primitives are automatically converted to native vector options.
   * The format is JSON with extensions.
   */
  class OptsParser {
  public:
    enum opts_state {
      Begin, InObject, BeginVector, InVector, BeginName, BeginValue, BeginGen, GenArgs
    };

    OptsParser(const std::string& text, OptionMap* root);

    bool parse();

    template<class _T>
    bool replace_value_vector(OptionVec* vec) {
      auto& src = vec->get_value();
      std::vector<_T> target(src.size(), _T());
      bool all_ok = true;
      for(size_t i = 0; all_ok && i<src.size(); ++i) {
        all_ok = all_ok && src[i]->convert(target[i]);
      }
      //log_debug(m_log, "replace_value_vector() type '"<<type_name<_T>()<<"' all: "<<all_ok<<" "<<target);
      if(all_ok) {
        Option* opn = m_opts.back();
        auto* opm = dynamic_cast<OptionMap*>(opn);
        auto* opv = dynamic_cast<OptionVec*>(opn);
        if(opm) {
          opm->set_impl(vec->name(), target);
          delete vec;
          return true;
        }
        else if(opv) {
          auto* res = new OptionImpl<std::vector<_T>>(target);
          auto& pvec = opv->get_value();
          pvec.push_back(res);
          delete vec;
          return true;
        }
      }
      //log_debug(m_log, "replace_value_vector() type '"<<type_name<_T>()<<"' failed ");
      return false;
    }

    bool parse_value(char cc, Option* op, const std::string& name, const std::string& next_token);
  protected:
    char next_t(std::string& next_token);

    /** eat all from " -> " */
    char next_str(std::string& next_token);

    /** Advance until not whitespace, return true if EOT */
    inline bool remove_space() {
      while((m_pos < m_N) && std::isspace(m_text[m_pos])) ++m_pos;
      return (m_pos == m_N);
    }

    template<class _T>
    bool
    add_value(Option* op, const _T& value, const std::string& name) {
      auto* opm = dynamic_cast<OptionMap*>(op);
      if(opm) {
        if(opm->set_impl(name, value)) return true;
      }
      else {
        if(op->get_type() == Option::Vector) {
          dynamic_cast<OptionVec*>(op)->get_value().push_back(new OptionImpl<_T>(value, "", false, ""));
          return true;
        }
      }
      throw OptionException("add_value<"+type_name<_T>()+"> failed to add '"+name+"'");
    }

    bool define_option(OptionMap* opm);

    bool add_vector(OptionVec* vec);

    std::string text_around_error(size_t pos);

    Option* locate_named_option(const std::string& on);

    void replace_option(Option* opm, Option* new_op);

    const std::string& m_text;
    size_t m_pos;
    const size_t m_N;
    std::vector<opts_state> m_state;
    std::vector<Option*> m_opts;
    bool m_ext_opts;
  };
}

#endif // sfi_OptsParser_h
