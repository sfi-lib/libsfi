#ifndef sfi_Subsets_h
#define sfi_Subsets_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <vector>

namespace sfi {
class Subsets {
  public:
    Subsets(unsigned minv, unsigned maxv, unsigned size);

    Subsets(unsigned minv, unsigned maxv, const std::vector<bool>& inc);

    bool next();

    inline unsigned sum() const { return m_sum; }

    inline const std::vector<bool>& includes() const { return m_include; }
  protected:
    unsigned m_minv, m_maxv, m_sum;
    std::vector<bool> m_include;
  };
}

#endif // sfi_Subsets_h
