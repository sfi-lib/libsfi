#ifndef sfi_mp_concurrentqueue_h
#define sfi_mp_concurrentqueue_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <mp/Semaphore.h>
#include <atomic>
#include <iostream>

namespace sfi {
  namespace mp {
    /**
     * @class sfi::mp::NonBlockingConcurrentQueue
     * M.M. Michael and M.L. Scott, "Simple, Fast, and Practical Non-Blocking and Blocking Concurrent Queue Algorithms"
     */
    template<class _T>
    class NonBlockingConcurrentQueue {
    public:
      NonBlockingConcurrentQueue()
#ifdef EnableLockedQueueStatistics
      :m_pushed(0), m_poped(0)
#endif
      {
        auto node = node_ptr_t{new node_t{_T()},0};
        m_head.store(node);
        m_tail.store(node);
      }

#ifdef EnableLockedQueueStatistics
      inline size_t size() {
        size_t res(0);
        node_ptr_t node = m_head.load().ptr->next.load();
        while(node.ptr) {
          ++res;
          node = node.ptr->next.load();
        }
        return res;
      }
      inline size_t pushed() const { return m_pushed; }
      inline size_t poped() const { return m_poped; }
#endif

      void push(_T val) {
        auto node = new node_t(std::move(val));
        node_ptr_t tail, next;
        do {
          tail = m_tail.load();
          next = tail.ptr->next.load();
          if(tail == m_tail.load()) {
            if(next.ptr == nullptr) {
              if(tail.ptr->next.compare_exchange_weak(next, node_ptr_t{node, next.count+1})) {
                break;
              }
            } else {
              m_tail.compare_exchange_weak(tail, node_ptr_t{next.ptr, tail.count+1});
            }
          }
        } while(true);
#ifdef EnableLockedQueueStatistics
        m_pushed.fetch_add(1);
#endif
      }

      bool pop(_T& rval) {
        node_ptr_t head, tail, next;
        do {
          head = m_head.load();
          tail = m_tail.load();
          next = head.ptr->next.load();
          if(head == m_head.load()) {
            if(head.ptr == tail.ptr) {
              if(next.ptr == nullptr) {
                // The queue is empty
                return false;
              } else {
                m_tail.compare_exchange_weak(tail, node_ptr_t{next.ptr, tail.count+1});
              }
            } else {
              rval = next.ptr->value;
              if(m_head.compare_exchange_weak(head, node_ptr_t{next.ptr, head.count+1})) {
                break;
              }
            }
          }
        } while(true);
        if(head.ptr->next.compare_exchange_weak(next, node_ptr_t{nullptr,0xffffff}))
          delete head.ptr;
        else std::cout<<"NonBlockingConcurrentQueue() FAIL"<<std::endl;
#ifdef EnableLockedQueueStatistics
        m_poped.fetch_add(1);
#endif
        return true;
      }

    protected:
      template<class _TP>
      struct ptr_t {
        _TP* ptr;
        size_t count;
        inline bool operator==(const ptr_t& o) const {
          return (ptr == o.ptr) && (count == o.count);
        }
      };
      struct node_t;
      using node_ptr_t = ptr_t<node_t>;
      struct node_t {
        node_t(_T val):next(node_ptr_t{nullptr,0}), value(std::move(val)) {}
        std::atomic<node_ptr_t> next;
        _T value;
      };
      std::atomic<node_ptr_t> m_head, m_tail;
#ifdef EnableLockedQueueStatistics
      std::atomic<unsigned> m_pushed, m_poped;
#endif
    };

    /**
     * @class sfi::mp::BlockingConcurrentQueue
     */
    template<class _T>
    class BlockingConcurrentQueue {
    public:

      BlockingConcurrentQueue(unsigned timeout):m_queue(), m_timeout(timeout), m_closed(false) {}

#ifdef EnableLockedQueueStatistics
      inline size_t size() { return m_queue.size(); }

      inline size_t pushed() const { return m_queue.pushed(); }

      inline size_t poped() const { return m_queue.poped(); }
#endif

      void push(_T val) {
        if(!m_closed) {
          m_queue.push(std::move(val));
          m_sema.release();
        }
      }

      bool pop(_T& rval, bool do_wait = true) {
        if(m_closed) return false;
        if(!m_queue.pop(rval)) {
          if(do_wait) {
            m_sema.try_aquire_for(m_timeout);
            return !m_closed && m_queue.pop(rval);
          }
          else return false;
        }
        else return true;
      }

      void close() {
        m_closed = true;
        m_sema.release();
      }

    protected:
      NonBlockingConcurrentQueue<_T> m_queue;
      Semaphore m_sema;
      unsigned m_timeout;
      bool m_closed;
    };
  }
}

#endif // sfi_mp_concurrentqueue_h
