#ifndef sfi_mp_Dispatcher_h
#define sfi_mp_Dispatcher_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <mp/ConcurrentQueue.h>
#include <mp/Job.h>
#include <defs.h>
#include <stat.h>
#include <timer.h>
#include <Options.h>
#include <Log.h>

#include <thread>
#include <unordered_set>
#include <vector>

namespace sfi {
  class Context;

  namespace mp {
    class WorkThread;

    /**
     * @class Dispatcher
     *
     * Simple thread and job manager.
     * The main thread executes serial jobs.
     *
     * Example of how to queue a serial job:
     * dispatcher<<[] { // do some work
     * };
     *
     * Serial and concurrent jobs can be chained in a JobSequence.
     * The jobs in a sequence will be executed in order.
     *
     * Example:
     * auto js = job_set_create<int, double>();
     *  js += [] (int i) { return 10.0*i; }
     *  ...
     *
     * auto jseq = job_seq_create([] { / * init sequence * / return 17; })
     *   + js
     *   + [](std::vector<double>&& res) { .... };
     *
     * dispatcher<<jseq;
     *
     * In the above example one serial job perform some initial work,
     * then returns a value. The next item in the sequence is a set of
     * concurrent jobs, each of them will get the value from the preceding
     * step. The jobs in the set each produces a value (double) and the values
     * for all jobs are stored in a vector. When the concurrent jobs are all
     * done, the third step is invoked with a vector of results from the
     * concurrent jobs.
     *
     * If the last job in a JobSequence has a return statement of type T,
     * the sequence produces a std::future<T>.
     *
     * Example:
     * auto jseq = job_seq_create([] { return 10.0; });
     * std::future<double> res = disp<<jseq;
     * int value = res.get();
     */
    class Dispatcher {
    public:
      Dispatcher(Context& ctx);

      Dispatcher(Context& ctx, const OptionMap& opts);

      ~Dispatcher();
      
      Dispatcher& nworkers(size_t nw);

      inline size_t nworkers() const { return m_nworkers; }

      using SerialQueueTy = BlockingConcurrentQueue<Job*>;
      using InQueueTy = BlockingConcurrentQueue<Job*>;
      using OutQueueTy = BlockingConcurrentQueue<Job*>;

      inline bool enabled() const { return m_enable_multi_processing; }
      
      /** @return If jobs should be split, i.e. if MP is enabled and we're on the main thread */
      inline bool enable_multi_processing() const {
        return m_enable_multi_processing && is_main_thread();
      }

      inline Dispatcher& enable_multi_processing(bool e) { m_enable_multi_processing = e; return *this; }

      inline bool is_main_thread() const { return (std::this_thread::get_id() == m_main_thread_id); }

      inline bool is_running() const { return m_running.load(); }

      inline Context& main_context() { return m_main_context; }

      void start();

      void stop();

      void dump();

      struct EndRun {};
      static const EndRun done;
      inline Dispatcher& operator<<(const EndRun&) { return add_serial_job(0); }

      /** Add a serial job */
      template<class _Lambda>
      inline Dispatcher& operator<<(_Lambda& rf) { return add_serial_job(job_create(std::move(rf), 0)); }

      template<class _Lambda>
      inline Dispatcher& operator<<(_Lambda&& rf) { return add_serial_job(job_create(std::forward<_Lambda>(rf), 0)); }

      /** Add a sequence of serial or concurrent jobs */
      template<class ... _Lambda>
      inline teif<t_job_seq_res_last_is_void<_Lambda...>::value(),Dispatcher&> operator<<(JobSequence<_Lambda...>& js) {
        return add_job_sequence(job_seq_impl_create(std::move(js)));
      }

      template<class ... _Lambda>
      inline teif<t_job_seq_res_last_is_void<_Lambda...>::value(),Dispatcher&> operator<<(JobSequence<_Lambda...>&& js) {
        return add_job_sequence(job_seq_impl_create(std::forward<JobSequence<_Lambda...>>(js)));
      }

      template<class ... _Lambda>
      inline teif<!t_job_seq_res_last_is_void<_Lambda...>::value(), std::future<t_job_seq_res_last<_Lambda...>>>
      operator<<(JobSequence<_Lambda...>& js) {
        auto jsi = job_seq_impl_create(std::move(js));
        add_job_sequence(jsi);
        return jsi->future();
      }

      template<class ... _Lambda>
      inline teif<!t_job_seq_res_last_is_void<_Lambda...>::value(), std::future<t_job_seq_res_last<_Lambda...>>>
      operator<<(JobSequence<_Lambda...>&& js) {
        auto jsi = job_seq_impl_create(std::forward<JobSequence<_Lambda...>>(js));
        add_job_sequence(jsi);
        return jsi->future();
      }

      void run();

      /** @return vector of all worker thread ids */
      std::vector<std::thread::id> worker_thread_ids() const;

      template<class _Lambda>
      inline Dispatcher& iojob(_Lambda&& rf) { return add_io_job(job_create(std::forward<_Lambda>(rf), 0)); }
    protected:
      Dispatcher& add_serial_job(Job* jsb);

      Dispatcher& add_job_sequence(JobContainer* jsb);

      Dispatcher& add_io_job(Job* jsb);

      void handle_finished_job(Job*);

      void handle_io_jobs();

      Context& m_main_context;
      Log m_log;
      size_t m_nworkers;
      bool m_enable_multi_processing;
      std::atomic<bool> m_running;
      SerialQueueTy m_serial_queue;
      SerialQueueTy m_io_queue;
      InQueueTy m_input_queue;
      OutQueueTy m_output_queue;
      std::vector<WorkThread*> m_workers;
      std::unordered_set<JobContainer*> m_job_sequences;
      std::thread::id m_main_thread_id;
      std::thread* m_io_thread;
#ifdef EnableDispatcherStatistics
      SampleExtStat m_js_time, m_job_time, m_job_run_time, m_job_in_time, m_job_out_time;
#endif // EnableDispatcherStatistics
    };
  }
}
#endif // sfi_mp_Dispatcher_h
