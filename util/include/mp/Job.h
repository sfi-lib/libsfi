#ifndef sfi_mp_Job_h
#define sfi_mp_Job_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "util.h"
#include "defs.h"
#include "Context.h"
#include "timer.h"

#include <functional>
#include <tuple>
#include <future>

namespace sfi {
  namespace mp {
    struct JobContainer;

    struct Job {
      virtual ~Job() {}

      virtual void execute(Context&) = 0;

      inline JobContainer* parent() { return m_parent; }

      inline Job* next() { return m_next; }
      inline Job& next(Job* _next) { m_next = _next; return *this; }

      inline bool status() { return m_status; }
      inline Job& status(bool s) { m_status = s; return *this; }

      inline void start_in() { m_timer_in.start(); }
      inline void stop_in() { m_time_in = m_timer_in.stop(); }

      inline void start_out() { m_timer_out.start(); }
      inline void stop_out() { m_time_out = m_timer_out.stop(); }

      inline double time_in() const { return this->m_time_in; }
      inline double time_out() const { return this->m_time_out; }

      inline double time_run() const { return this->m_time_run; }
      inline void time_run(double t) { this->m_time_run = t; }
    protected:
      Job(JobContainer* parent):m_parent(parent), m_next(0), m_time_in(0), m_time_out(0), m_time_run(0), m_status(false) {}

      JobContainer* m_parent;
      Job* m_next;
      Timer<true> m_timer_in, m_timer_out;
      double m_time_in, m_time_out, m_time_run;
      bool m_status;
    };

    template<class _Func>
    struct JobImpl : public Job {
      JobImpl(_Func&& f, JobContainer* p):Job(p), m_func(std::forward<_Func>(f)) {}

      virtual ~JobImpl() {}

      virtual void execute(Context& ctx) { m_func(ctx); }
    protected:
      _Func m_func;
    };

    template<class _Func, class _Ret>
    struct JobRetImpl : public Job {
      JobRetImpl(_Func&& f, JobContainer* p):Job(p), m_func(std::forward<_Func>(f)) {}

      virtual ~JobRetImpl() {}

      virtual void execute(Context& ctx) {
        _Ret value = m_func(ctx);
        m_promise.set_value(value);
      }

      inline std::future<_Ret> future() { return m_promise.get_future(); }
    protected:
      _Func m_func;
      std::promise<_Ret> m_promise;
    };

    template<class _Func>
    inline JobImpl<_Func>* job_create(_Func&& f, JobContainer* p) {
      return new JobImpl<_Func>(std::forward<_Func>(f), p);
    }

    struct JobContainer : public Job {
      JobContainer(JobContainer* parent):Job(parent), finished_jobs(0) {}

      virtual ~JobContainer() {
        for(auto* j : jobs) delete j;
        jobs.clear();
      }

      template<class _Lam>
      inline void add_job(_Lam&& lam) {
        Job* j = job_create(std::forward<_Lam>(lam), this);
        if(!jobs.empty()) jobs.back()->next(j);
        jobs.push_back(j);
      }

      std::vector<Job*> jobs;
      size_t finished_jobs;
    };

    /**
     * @class template JobSet
     * Helper class for creating JobSetImpl
     *
     * Usage:
     * auto jset = job_seq_create([] { ....}, [] (ArgType a) { .... }, ...);
     *
     * or:
     * auto jset = job_seq_create<ArgType, RetType>()
     * jset += [] (ArgType a) { return ... }
     */
    template<class _Arg, class _Ret>
    struct JobSet {
      using functions_type = std::vector<t_func2<Context&,_Arg, _Ret>>;
      JobSet() { }

      JobSet(const JobSet<_Arg, _Ret>& js) = delete;

      JobSet(JobSet<_Arg, _Ret>&& js):functions(std::forward<functions_type>(js.functions)) { }

      template<class ... _Lambdas>
      JobSet(_Lambdas&&... lam) {
        const bool __x[] = {(functions.emplace_back(std::forward<_Lambdas>(lam)), false)...};
        unused_parameters(__x);
      }

      template<class _TLam>
      inline JobSet<_Arg, _Ret>& operator+=(_TLam&& lam) {
        functions.emplace_back(std::forward<_TLam>(lam));
        return *this;
      }
      functions_type functions;
    };

    template<class _Arg, class _Ret>
    struct JobSetImpl : public JobContainer {
      JobSetImpl(JobContainer* parent):JobContainer(parent) {}

      virtual ~JobSetImpl() { }

      // Only for testing!
      virtual void execute(Context& ctx) { for(auto* j : this->jobs) j->execute(ctx); }
    };

    /**
     * @class template JobSequence
     * Helper class for creating JobSequenceImpl
     *
     * Usage:
     * auto jseq = job_seq_create([] { ....}) + [] (ArgType a) { .... };
     */
    template<class ..._T>
    struct JobSequence {
      using functions_type = std::tuple<_T...>;
      JobSequence(functions_type&& _funcs):functions(std::forward<functions_type>(_funcs)) { }

      JobSequence(const JobSequence<_T...>& o) = delete;

      JobSequence(JobSequence<_T...>&& o):functions(std::forward<functions_type>(o.functions)) { }

      template<class _T1>
      inline JobSequence<_T..., _T1> operator+(_T1&& j) && {
        return JobSequence<_T..., t_raw<_T1>>(std::tuple_cat(std::forward<functions_type>(functions), std::make_tuple(std::forward<_T1>(j))));
      }

      template<class _T1>
      inline JobSequence<_T..., _T1> operator+(_T1& j) && {
        return JobSequence<_T..., t_raw<_T1>>(std::tuple_cat(std::forward<functions_type>(functions), std::make_tuple(std::move(j))));
      }

      template<class ..._T1>
      inline JobSequence<_T..., _T1...> operator+(JobSequence<_T1...>&& j) && {
        return JobSequence<_T..., t_raw<_T1>...>(std::tuple_cat(std::forward<functions_type>(functions), std::forward<std::tuple<_T1...>>(j.functions)));
      }

      template<class ..._T1>
      inline JobSequence<_T..., _T1...> operator+(JobSequence<_T1...>& j) && {
        return JobSequence<_T..., t_raw<_T1>...>(std::tuple_cat(std::forward<functions_type>(functions), std::move(j.functions)));
      }

      functions_type functions;
    };

    template<class _Lambda, class _Arg>
    struct _t_job_seq_elem_retval : tvalued<t_result<_Lambda(Context&,_Arg)>> { };
    template<class _Lambda>
    struct _t_job_seq_elem_retval<_Lambda,void> : tvalued<t_result<_Lambda(Context&)>>{ };
    template<class _Ret, class _Arg>
    struct _t_job_seq_elem_retval<JobSet<_Arg, _Ret>, _Arg> : tvalued<std::vector<_Ret>> { };
    template<class _Ret>
    struct _t_job_seq_elem_retval<JobSet<void, _Ret>, void> : tvalued<std::vector<_Ret>> { };

    template<size_t i, class _Lambda0, class ..._Lambda>
    struct _t_job_seq_res {
      template<class ... _Res>
      using tvalue = t_value<_t_job_seq_res<i+1, _Lambda...>, _Res..., tvalue<_t_job_seq_elem_retval<t_raw<_Lambda0>, t_get<i-1, _Res...>>>>;
    };

    // first
    template<class _Lambda0, class ..._Lambda>
    struct _t_job_seq_res<0, _Lambda0, _Lambda...> {
      template<class ... _Res> using tvalue = t_value<_t_job_seq_res<1, _Lambda...>, _Res..., tvalue<_t_job_seq_elem_retval<t_raw<_Lambda0>, void>>>;
    };

    // last
    template<size_t i, class _Lambda0>
    struct _t_job_seq_res<i, _Lambda0> { template<class ... _Res> using tvalue = std::tuple<_Res...>; };

    template<class ..._TLambda> using t_job_seq_res = t_value<_t_job_seq_res<0, _TLambda...>>;

    // determine return type of last lambda in sequence
    template<class ..._TLambda>
    using t_job_seq_res_last = tvalue<_t_job_seq_elem_retval<t_raw<t_get<sizeof...(_TLambda)-1, _TLambda...>>, t_tuple_element<sizeof...(_TLambda)-2, t_job_seq_res<_TLambda...>>>>;

    template<class ..._TLambda>
    struct t_job_seq_res_last_is_void : teq<t_job_seq_res_last<_TLambda...>, void> {};

    template<class _TLast>
    struct JobSequenceImplBase : public JobContainer  {
      JobSequenceImplBase(JobContainer* parent):JobContainer(parent) { }

      virtual ~JobSequenceImplBase() {}

      inline std::future<_TLast> future() { return this->m_promise.get_future(); }

      using last_type = _TLast;
    protected:
      std::promise<_TLast> m_promise;
    };

    template<>
    struct JobSequenceImplBase<void> : public JobContainer  {
      JobSequenceImplBase(JobContainer* parent):JobContainer(parent) { }

      virtual ~JobSequenceImplBase() {}

      using last_type = void;
    };

    template<class ..._TLam>
    struct JobSequenceImpl : public JobSequenceImplBase<t_job_seq_res_last<_TLam...>> {
      using Base = JobSequenceImplBase<t_job_seq_res_last<_TLam...>>;
      using lambdas = std::tuple<_TLam...>;
      using result_type = t_job_seq_res<_TLam...>;
      constexpr static const size_t Nlam = sizeof...(_TLam);

      static_assert(Nlam>1, "Must have at least 2 sub jobs in a sequence");

      JobSequenceImpl(JobContainer* parent, lambdas&& lam):Base(parent) { create_jobs<0>(std::forward<lambdas>(lam)); }

      virtual ~JobSequenceImpl() {}

      // just for testing
      virtual void execute(Context& ctx) { for(auto* j : this->jobs) j->execute(ctx);  }

    protected:
      template<size_t i> void create_jobs(lambdas&& _lam, teif<(i<(Nlam-1))>* = 0) {
        create_job<i>(std::get<i>(std::forward<lambdas>(_lam)));
        create_jobs<i+1>(std::forward<lambdas>(_lam));
      }

      // the last job in a sequence of at least two jobs
      template<size_t i> void create_jobs(lambdas&& _lam, teif<i==(Nlam-1) && (Nlam>1)>* = 0) {
        constexpr const size_t idx = Nlam-1;
        using elem_type = t_tuple_element<idx-1, result_type>;
        auto&& lam = std::get<idx>(std::forward<lambdas>(_lam));
        using TLam = t_raw<decltype(lam)>;
        add_last_job<idx-1, elem_type, typename Base::last_type>(std::forward<TLam>(lam));
      }

      template<size_t idx, class _Elem, class _Last, class _Lambda>
      void add_last_job(_Lambda&& lam, teif<teq<_Last, void>::value()>* = 0) {
        this->add_job([this,lam] (Context& ctx) { lam(ctx, std::forward<_Elem>(std::get<idx>(this->result))); });
      }

      template<size_t idx, class _Elem, class _Last, class _Lambda>
      void add_last_job(_Lambda&& lam, teif<!teq<_Last, void>::value()>* = 0) {
        this->add_job([this,lam] (Context& ctx) {
          this->m_promise.set_value(lam(ctx, std::forward<_Elem>(std::get<idx>(this->result))));
        });
      }

      // the first job in the sequence
      template<size_t i, class _Lambda>
      inline void create_job(_Lambda&& lam, teif<i == 0>* = 0) {
        this->add_job([this,lam] (Context& ctx) { std::get<0>(this->result) = lam(ctx); });
      }

      // the first job in the sequence
      template<size_t i, class _Arg, class _Ret>
      void create_job(JobSet<_Arg, _Ret>&& js, teif<i == 0>* = 0) {
        static_assert(teq<_Arg, void>::value(), "Argument of first lambda must be void");
        auto* jsi = new JobSetImpl<_Arg, _Ret>(this);
        std::get<0>(this->result).resize(js.functions.size(), _Ret());
        size_t j = 0;
        for(auto f: js.functions) {
          jsi->add_job([this,j,f] (Context& ctx) { std::get<0>(this->result)[j] = f(ctx); });
          ++j;
        }
        if(!this->jobs.empty()) this->jobs.back()->next(jsi);
        this->jobs.push_back(jsi);
      }

      template<size_t i, class _Lambda>
      inline void create_job(_Lambda&& lam, teif<(i > 0)>* = 0) {
        using elem_type = t_tuple_element<i - 1, result_type>;
        this->add_job([this,lam] (Context& ctx) { std::get<i>(this->result) = lam(ctx, std::forward<elem_type>(std::get<i-1>(this->result))); });
      }

      template<size_t i, class _Arg, class _Ret>
      void create_job(JobSet<_Arg, _Ret>&& js, teif<(i > 0)>* = 0) {
        using elem_type = t_tuple_element<i - 1, result_type>;
        static_assert(teq<_Arg, elem_type>::value(), "Invalid type");
        std::get<i>(this->result).resize(js.functions.size(), _Ret());
        auto* jsi = new JobSetImpl<_Arg, _Ret>(this);
        size_t j = 0;
        for(auto f: js.functions) {
          jsi->add_job([this,j,f] (Context& ctx) { std::get<i>(this->result)[j] = f(ctx, std::forward<elem_type>(std::get<i-1>(this->result))); });
          ++j;
        }
        if(!this->jobs.empty()) this->jobs.back()->next(jsi);
        this->jobs.push_back(jsi);
      }
    public:
      result_type result;
    };

    template<class ... _T>
    inline JobSequenceImpl<_T...>* job_seq_impl_create(JobSequence<_T...>&& js) {
      return new JobSequenceImpl<_T...>(0, std::forward<std::tuple<_T...>>(js.functions));
    }

    template<class _Ret, class _Class, class _Arg>
    inline std::pair<_Arg, _Ret> _t_job_member_types(_Ret (_Class::*)(Context&,_Arg) const) {}

    // determine the argument and return types for a lambda
    template<class _Lambda>
    using t_job_lambda_types = decltype(_t_job_member_types( &std::remove_reference<_Lambda>::type::operator()));

    // determine the type of a job_set from a pack of lambdas
    template<class ..._T>
    using _t_job_set_type = JobSet<t_pair_first<t_job_lambda_types<t_get<0, _T...>>>, t_pair_second<t_job_lambda_types<t_get<0, _T...>>>>;

    template<class ..._T>
    inline _t_job_set_type<_T...> job_set_create(_T&& ...lam) { return _t_job_set_type<_T...>(std::forward<_T>(lam)...); }

    template<class _Arg, class _Ret>
    inline JobSet<_Arg, _Ret> job_set_create() { return JobSet<_Arg, _Ret>(); }

    template<class ... _T>
    inline JobSequence<t_raw<_T>...> job_seq_create(_T&& ...lam) { return JobSequence<t_raw<_T>...>(std::tuple<t_raw<_T>...>(std::forward<t_raw<_T>>(lam)...)); }

    // get the result type for a JobSequence
    template<class _T> struct _t_job_seq_res_type : tvalued<void> { };
    template<class ... _T> struct _t_job_seq_res_type<JobSequence<_T...>> : tvalued<t_job_seq_res<_T...>> { };

    template<class _T> using t_job_seq_res_type = _t_job_seq_res_type<_T>;
  }
}
#endif // sfi_mp_Job_h
