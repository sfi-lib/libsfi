#ifndef sfi_mp_semaphore_h
#define sfi_mp_semaphore_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <defs.h>

#ifdef SFI_OSX

#include <dispatch/dispatch.h>
namespace sfi {
  namespace mp {

    /**
     * @class sfi::mp::Semaphore
     * Simple wrapper for dispatch semaphore
     */
    class Semaphore {
    public:
      Semaphore():m_sema(::dispatch_semaphore_create(1)) { }

      void release() { ::dispatch_semaphore_signal(m_sema); }

      void aquire() { ::dispatch_semaphore_wait(m_sema, DISPATCH_TIME_FOREVER); }

      bool try_aquire_for(unsigned long micros) {
        if(::dispatch_semaphore_wait(m_sema, ::dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_USEC*micros))) {
          // TODO: check error
          return false;
        }
        else return true;
      }
    protected:
      ::dispatch_semaphore_t m_sema;
    };
  }
}

#else

#include <sys/semaphore.h>
#include <chrono>

namespace sfi {
  namespace mp {

    /**
     * @class sfi::mp::Semaphore
     * Simple wrapper for semaphore
     */
    class Semaphore {
    public:
      Semaphore():m_sema(nullptr) {
        if(::sem_init(m_sema, 0, 0)) {
          // TODO: Error
        }
      }

      void release() { ::sem_post(m_sema); }

      void aquire() { ::sem_wait(m_sema); }

      bool try_aquire_for(unsigned long micros) {
        std::chrono::microseconds ct(micros);
        timespec times{0, (long)(micros*1000UL)};
        if(::sem_timedwait(m_sema, &times)) {
          // TODO: check error
          return false;
        }
        else return true;
      }
    protected:
      sem_t* m_sema;
    };
  }
}

#endif

#endif // sfi_mp_semaphore_h
