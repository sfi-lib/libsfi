#ifndef sfi_MatrixOp_h
#define sfi_MatrixOp_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Matrix.h"

namespace sfi {
  class MatrixSymEigenDecomposition {
  public:
    using This = MatrixSymEigenDecomposition;

    MatrixSymEigenDecomposition(bool compute_eigen_vectors = true, bool order_descending = true);

    void rows(size_t r);

    bool decompose(MatrixSym<double>& m);

    inline Vector<double>& eigen_values() { return this->m_eigenv; }

    inline MatrixSqr<double>& eigen_vectors() { return m_eigenvectors; }
  protected:
    MatrixSqr<double> m_eigenvectors;
    Vector<double> m_eigenv;
    Vector<double> m_work;
    Vector<int> m_iwork;
    bool m_compute_eigen_vectors;
    bool m_order_descending;
  };

  template<class _T>
  class Cholesky : public MatrixTria<_T> {
  public:
    using Base = MatrixTria<_T>;
    Cholesky(unsigned rows):Base(rows), m_status(false) { }

    explicit Cholesky(const MatrixSym<_T>& m):Base(m.rows()), m_status(false) { m_status = decompose(m); }

    bool decompose(const MatrixSym<_T>& m);

    inline bool status() const { return this->m_status; }
  protected:
    bool m_status;
  };

  struct svd_solve_result : result {
    svd_solve_result():result(false), condnr(0), rank(0) {}
    double condnr;
    int rank;
  };

  template<class _T>
  bool Cholesky<_T>::decompose(const MatrixSym<_T>&) { return m_status = false; }

  template<>
  bool Cholesky<double>::decompose(const MatrixSym<double>&);

  svd_solve_result svd_solve(const Matrix<double>& m, const Vector<double>& y, double tolerance, Vector<double>& x);

  /** Solve mx=y using SVD, y is a COL MAJOR matrix, initially with x and at success - y. */
  svd_solve_result svd_solve(const Matrix<double>& m, Matrix<double>& y, double tolerance);

  struct rope_invert_result : invert_result {
    rope_invert_result(bool b):invert_result(b), max_asymm(0) {}
    double max_asymm; // For SVD - maximum relative difference between A(r,c) and A(c,r)
    double Tr_orig, Tr_reg;
  };

  /**
   * Regularize a covaraince matrix using ROPE.
   * icov = QDQ^t
   *   where D is a diagonal matrix with elements: d_i = 2.0/(d_i + sqrt(d_i^2 + 8 rho)) if the inverse is to be computed
   *   or 1.0/d_i if not.
   * @param Q The Matrix decomposition
   * @param eigenv The regularized eigenvectors of the inverse
   * @param icov The inverted and regularizd matrix
   * @param prune_eigen If eigenvector should be pruned, if true only eigenvectors with eigenvalue/(largest eigenval) > sqrt(0.001*8.0*rho) are kept
   * @param inverse If the inverse shold be computed
   * The result contains the determinant and condition number for the regularized inverse matrix
   * @return rope_invert_result if nothing went wrong.
   *
   * Reference:
   * M.O. Kuismin, J.T. Kemppainen, and M.J. Sillanpää. Precision matrix estimation with rope.
   * Journal of Computational and Graphical Statistics, 26(3):682–694, 2017.
   */
  rope_invert_result rope_regularization(MatrixSymEigenDecomposition& Q, Vector<double>& eigenv, MatrixSym<double>& icov, double rho, bool prune_eigen, bool inverse);

  inline rope_invert_result compute_eigen_inverse_rope(MatrixSymEigenDecomposition& Q, Vector<double>& eigenv, MatrixSym<double>& icov, double rho, bool prune_eigen) {
    return rope_regularization(Q, eigenv, icov, rho, prune_eigen, true);
  }

  void print_matrix_code(const MatrixSqr<double>& a);

  void print_matrix_code(const Matrix<double>& a);

  void print_matrix_code(const Vector<double>& a);

  void print_matrix_code(const MatrixSym<double>& a, unsigned offs);

  bool invert_matrix(int nrows, MatrixSym<double>& m);
}

#endif // sfi_MatrixOp_h
