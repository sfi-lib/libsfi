#ifndef sfi_stat_h
#define sfi_stat_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <array>
#include <math.h>
#include <iostream>
#include <vector>

#include "sfi_math.h"
#include "timer.h"
#include "Matrix.h"

namespace sfi {
  class Qty {
  public:
    Qty();

    Qty(double v, double unc);

    inline double value() const { return m_value; }
    inline double uncertainty() const { return m_uncertainty; }

    inline double& value() { return m_value; }
    inline double& uncertainty() { return m_uncertainty; }

    inline Qty& value(double v) { m_value = v; return *this; }
    inline Qty& uncertainty(double u) { m_uncertainty = u; return *this; }

    inline operator double() const { return m_value; }
  protected:
    double m_value, m_uncertainty;
  };

  inline std::ostream& operator<<(std::ostream& out, const Qty& qty) {
    return out<<qty.value()<<" ("<<qty.uncertainty()<<")";
  }

  template<class _Elem>
  class SampleStatG {
  public:
    using This = SampleStatG<_Elem>;

    SampleStatG():m_m(0.0), m_c(0.0), m_N(0) {}

    SampleStatG(const SampleStatG& s):m_m(s.m_m), m_c(s.m_c), m_N(s.m_N) {}

    This& add(const _Elem& x) {
      if(is_finite(x)) {
        ++m_N;
        _Elem m = m_m;
        m_m += (x - m)/double(m_N);
        m_c += std::real(conjugate(x - m)*(x - m_m));
      }
      return *this;
    }

    inline This& operator+=(const _Elem& x) { return this->add(x); }

    inline void clear() { m_m = 0.0; m_c = 0.0; m_N = 0; }

    inline operator _Elem() const { return mean(); }

    inline Qty result() const { return Qty(std::real(m_m), mean_unc()); }

    inline Qty mean_and_spread() const { return Qty(std::real(m_m), stddev()); }

    inline _Elem mean() const { return m_m; }

    inline double stddev() const { return sqrt_p(variance()); }

    inline double variance() const { return m_c/double(m_N - 1); }

    inline double mean_unc() const { return stddev()/sqrt(double(m_N)); }

    inline unsigned N() const { return m_N; }
  protected:
    _Elem m_m;
    double m_c;
    unsigned m_N;
  };

  using SampleStat = SampleStatG<double>;

  template<class _Elem>
  inline std::ostream& operator<<(std::ostream& out, const SampleStatG<_Elem>& s) {
    return out<<s.result();
  }

  class SampleExtStat : public SampleStat {
  public:
    using This = SampleExtStat;

    SampleExtStat();

    SampleExtStat(const SampleExtStat& s);

    This& operator+=(double x);

    void clear();

    inline double min_value() const { return m_minv; }

    inline double max_value() const { return m_maxv; }

    std::ostream& print(std::ostream&) const;
  protected:
    double m_minv, m_maxv;
  };

  inline std::ostream& operator<<(std::ostream& out, const SampleExtStat& s) { return s.print(out); }

  template<bool _Enable = true>
  class TimerStats {
  public:
    TimerStats() {}

    inline void start() { m_timer.start(); }

    inline double stop() {
      double ti = m_timer.stop();
      m_stat +=  ti;
      return ti;
    }

    inline const SampleExtStat& result() const { return m_stat; }
  protected:
    Timer<true> m_timer;
    SampleExtStat m_stat;
  };
  template<>
  class TimerStats<false> {
  public:
    TimerStats() {}

    inline void start() { }

    inline double stop() { return 0.0; }

    inline const SampleExtStat& result() const { return m_stat; }
  protected:
    SampleExtStat m_stat;
  };

  template<class _TData>
  class SampleStatsG {
  public:
    using This = SampleStatsG<_TData>;
    using Elem = SampleStatG<_TData>;
    using VecType = std::vector<Elem>;

    SampleStatsG(size_t n, bool mean_and_stddev = false):m_stats(n, Elem()), m_mean_and_stddev(mean_and_stddev) {}

    inline size_t size() const { return this->m_stats.size(); }

    inline bool mean_and_stddev() const { return this->m_mean_and_stddev; }

    inline void mean_and_stddev(bool b) { this->m_mean_and_stddev = b; }

    inline Elem& operator[](size_t i) {
      assert(i < this->m_stats.size() && "Invalid index");
      return this->m_stats[i];
    }
    inline const Elem& operator[](size_t i) const {
      assert(i < this->m_stats.size() && "Invalid index");
      return this->m_stats[i];
    }
    template<class _Vec>
    inline This& operator+=(const _Vec& data) {
      assert(data.size() == m_stats.size() && "Invalid vector size");
      const unsigned N(data.size());
      for(unsigned i=0; i<N; ++i) m_stats[i] += data[i];
      return *this;
    }

    using iterator = typename VecType::iterator;
    using const_iterator = typename VecType::const_iterator;

    inline iterator begin() { return m_stats.begin(); }
    inline const_iterator begin() const { return m_stats.begin(); }

    inline iterator end() { return m_stats.end(); }
    inline const_iterator end() const { return m_stats.end(); }
  protected:
    VecType m_stats;
    bool m_mean_and_stddev;
  };

  template<class _TData>
  inline std::ostream& operator<<(std::ostream& out, const SampleStatsG<_TData>& stats) {
    if(stats.mean_and_stddev()) for(auto &st : stats) out<<st.mean_and_spread()<<" ";
    else for(auto &st : stats) out<<st<<" ";
    return out;
  }

  using SampleStats = SampleStatsG<double>;

  /**
   * Online sample covariance computation.
   *
   * Reference:
   * Algorithms for calculating variance, https://en.wikipedia.org/w/index.php?title=Algorithms_for_calculating_variance&oldid=980508289 (last visited Oct. 13, 2020).
   */
  inline void update_online_covariance(const Vector<double>& data, size_t &_N, MatrixSym<double>& _c, Vector<double>& _mean, Vector<double>& vx) {
    assert(data.size() == _c.rows() && "Invalid vector size");
    ++_N;
    vx = data;
    vx.add(_mean, -1.0);
    const double one_over_n(1.0/double(_N));
    _c.rank1_update(vx, 1.0 - one_over_n);
    _mean.add(vx, one_over_n);
  }

  inline void update_online_covariance(const Vector<std::complex<double>>&, size_t &, MatrixSym<double>&, Vector<std::complex<double>>&, Vector<std::complex<double>>&) { }

  class SampleCovariance {
  public:
    using This = SampleCovariance;

    SampleCovariance(unsigned n);

    template<class _T>
    inline This& operator+=(const Vector<_T>& data) {
      update_online_covariance(data, m_N, m_c, m_mean, m_v1);
      return *this;
    }

    inline void compute() { m_c *= 1.0/double(m_N - 1); }

    inline MatrixSym<double>& covariance() { return m_c; }
    inline const MatrixSym<double>& covariance() const { return m_c; }

    inline Vector<double>& mean() { return m_mean; }
    inline const Vector<double>& mean() const { return m_mean; }

    inline double operator()(unsigned r, unsigned c) const { return m_c(r,c); }

    inline size_t nevents() const { return m_N; }

    inline void clear() {
      m_N = 0;
      m_c.clear();
      m_mean.clear();
    }
  protected:
    MatrixSym<double> m_c;
    Vector<double> m_mean, m_v1;
    size_t m_N;
  };

  /**
   * @class CovarianceSummation
   *
   * Helper class for merging covariance matrices
   * each created on a subset of the data sample.
   *
   * Only to be used on the stack.
   */
  class CovarianceSummation {
  public:
    CovarianceSummation(MatrixSym<double>& cov):m_cov(cov) {}

    inline void add_part(const MatrixSym<double>& cov_n, const Vector<double>& par_n, double weight_n) {
      const size_t N(m_cov.rows());
      assert(cov_n.rows() == N && "Invalid matrix dimensions");
      assert(par_n.rows() == N && "Invalid vector dimensions");
      const double Ni = 1.0/double(weight_n);
      for(size_t r=0; r<N; ++r) {
        for(size_t c=0; c<=r; ++c) m_cov(r, c) += cov_n(r, c) + Ni*(par_n(r)*par_n(c));
      }
    }

    void finalize(const Vector<double>& par, double weight) {
      const size_t N(m_cov.rows());
      assert(par.rows() == N && "Invalid vector dimensions");
      const double Nitot = 1.0/double(weight);
      for(size_t r=0; r<N; ++r) {
        for(size_t c=0; c<=r; ++c) m_cov(r, c) -= Nitot*par(r)*par(c);
      }
    }
  protected:
    MatrixSym<double>& m_cov;
  };
}

#endif // sfi_stat_h
