#ifndef sfi_ComponentHandler_h
#define sfi_ComponentHandler_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <map>

#include "IComponent.h"
#include "Context.h"
#include "Log.h"

namespace sfi {
  class ComponentHandler {
  public:
    ComponentHandler(Context&);

    IComponent* component(Context&, const std::string& id, const std::string& libn, OptionMap& opts);
  protected:
    Log m_log;
    using FactoryFunc = std::function<IComponent*(Context&, OptionMap&)>;
    std::map<std::string, FactoryFunc> m_component_factories;
    std::map<std::string, void*> m_libraries;
  };
}

#endif // sfi_ComponentHandler_h
