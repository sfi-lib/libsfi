#ifndef sfi_Matrix_h
#define sfi_Matrix_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <array>
#include <math.h>
#include <iostream>
#include <vector>
#include <initializer_list>

#include "sfi_math.h"
#include "util.h"
#include "Buffer.h"

namespace sfi {
  template<class _T> class Matrix;
  template<class _T> class MatrixSqr;
  template<class _T> class MatrixSym;
  template<class _T> class MatrixDiag;
  template<class _T> class MatrixTria;

  template<class _T = double>
  class Vector {
  public:
    using This = Vector<_T>;
    using ConstValType = typename Buffer<_T>::ConstValType;

    Vector(const std::initializer_list<_T>& init):m_data(init) {}

    Vector(unsigned rows = 0):m_data(rows) { }

    Vector(const This& o):m_data(o.m_data) {}

    Vector(This&& o):m_data(std::move(o.m_data)) { }

    inline unsigned size() const { return m_data.size(); }
    inline unsigned rows() const { return m_data.size(); }

    inline void rows(unsigned rows) { m_data.size(rows); }

    inline unsigned cols() const { return 1; }

    inline void clear(ConstValType _val = 0.0) { std::fill(m_data.begin(), m_data.end(), _val); }

    inline _T& operator()(unsigned r) {
      assert((r < m_data.size()) && "Invalid index");
      return m_data[r];
    }
    inline ConstValType operator()(unsigned r) const {
      assert((r < m_data.size()) && "Invalid index");
      return m_data[r];
    }
    inline _T& operator[](unsigned r) { return this->operator ()(r); }
    inline ConstValType operator[](unsigned r) const { return this->operator ()(r); }

    This& operator*=(double c) &;

    This& operator+=(const This& o);

    std::ostream& print(std::ostream& out) const;

    inline This& operator=(const std::initializer_list<_T>& init) {
      m_data = init;
      return *this;
    }

    inline This& operator=(const std::vector<_T>& data) {
      m_data = data;
      return *this;
    }

    inline This& operator=(const This& o) {
      if(this != &o) m_data = o.m_data;
      return *this;
    }

    inline This& operator=(This&& o) {
      if(this != &o) m_data = std::move(o.m_data);
      return *this;
    }

    /** Access the underlying vector */
    inline Buffer<_T>& data() { return this->m_data; }
    inline const Buffer<_T>& data() const { return this->m_data; }

    void add(const This& o, double scale=1.0);

    void add(const This& o, This& res, double scale=1.0) const;

    void assign(const This& o, double scale=1.0);

    /** Scalar prod */
    _T mult(const This& o) const;

    /** Multiply each element by corresponding elem in o */
    This& elem_mult(const This& o);

    using iterator = _T*;
    using const_iterator = const _T*;

    inline iterator begin() { return m_data.begin(); }

    inline iterator end() { return m_data.end(); }

    inline const_iterator begin() const { return m_data.begin(); }

    inline const_iterator end() const { return m_data.end(); }

    void insert(unsigned r, ConstValType val);

    _T remove(unsigned r);

    /** @return sum of elements */
    inline _T esum() const {
      _T res(0.0);
      for(auto&e : *this) res += e;
      return res;
    }

    /** @return product of elements */
    inline _T eprod() const {
      _T res(1.0);
      for(auto&e : *this) res *= e;
      return res;
    }

    inline _T norm2() const {
      _T res(0.0);
      for(auto&e : *this) res += e*e;
      return res;
    }
    void copy_subset(const This& o, const std::vector<bool>& include);

    /** Reverse the rows */
    void reverse();

    inline _T* data_ptr() { return this->m_data.data(); }
    inline const _T* data_ptr() const { return this->m_data.data(); }
  protected:
    Buffer<_T> m_data;
  };

  template<class _T = double>
  class MatrixBase {
  public:
    using ConstValType = typename Buffer<_T>::ConstValType;

    inline void clear() {
      std::fill(m_data.begin(), m_data.end(), _T());
    }

    inline size_t rows() const { return m_rows; }
    inline size_t row_length() const { return m_row_length; }
    inline bool owns_memory() const { return m_data.owns_memory(); }

    /** Access raw data */
    inline _T* data_ptr() { return this->m_data.data(); }
    inline const _T* data_ptr() const { return this->m_data.data(); }
    inline _T* row_data(size_t r) {
      assert((r < this->m_rows) && "Invalid row");
      return this->m_data.data() + r*this->m_row_length;
    }
    inline const _T* row_data(size_t r) const {
      assert((r < this->m_rows) && "Invalid row");
      return this->m_data.data() + r*this->m_row_length;
    }
  protected:
    MatrixBase(size_t rows, size_t cols):m_rows(rows), m_row_length(cols), m_data(rows*cols) { }

    MatrixBase(const MatrixBase& o):m_rows(o.m_rows), m_row_length(o.m_row_length), m_data(o.m_data) { }

    MatrixBase(MatrixBase&& o):m_rows(o.m_rows), m_row_length(o.m_row_length), m_data(std::move(o.m_data)) {
      o.m_rows = 0;
      o.m_row_length = 0;
    }

    MatrixBase(size_t rows, size_t cols, _T* data, size_t row_length = 0):
      m_rows(rows), m_row_length(row_length ? row_length : cols), m_data(rows*std::max(cols, row_length), data) {}

    void copy_from(const MatrixBase& o);

    void move_from(MatrixBase&& o);

    size_t m_rows, m_row_length;
    Buffer<_T> m_data;
  };

  template<class _T = double>
  class MatrixSym : public MatrixBase<_T> {
  public:
    using This = MatrixSym<_T>;
    using Base = MatrixBase<_T>;
    using ConstValType = typename Base::ConstValType;

    MatrixSym():Base(0,0) { }

    MatrixSym(size_t rows, bool aux = false):Base(rows, aux ? rows+1:rows) { }

    MatrixSym(const MatrixSym& o):Base(o) { }

    MatrixSym(MatrixSym&& o):Base(std::forward<Base>(o)) { }

    MatrixSym(size_t rows, _T* data, size_t row_length = 0):Base(rows, rows, data, row_length) {}

    template<class _Elem2> friend class MatrixSym;
    template<class _Elem2> friend class Matrix;

    void clear(ConstValType v = _T()) {
      _T* rp(0);
      for(size_t i=0; i<this->m_rows; ++i) {
        rp = this->row_data(i);
        for(size_t c=0; c<=i; ++c) rp[c] = v;
      }
    }

    using Base::rows;
    inline size_t cols() const { return this->m_rows; }

    void rows(size_t rows, bool aux = false);

    void remove(size_t row);

    inline _T trace() const {
      _T res(0.0);
      for(size_t i=0; i<this->m_rows; ++i) res += this->data(i,i);
      return res;
    }

    /**
     * TODO: This is generic
     * @return tr(A*B)
     */
    _T trace(const This& B) const;

    /** @return sum |a_ij| */
    _T norm1b() const;

    /** @return sum |a_ij|^2 */
    _T norm_frobenius2() const;

    inline _T& operator()(size_t r, size_t c) {
      assert((r < this->m_rows) && (c < this->m_rows) && "Invalid indices");
      return this->data(r,c);
    }
    inline ConstValType operator()(size_t r, size_t c) const {
      assert((r < this->m_rows) && (c < this->m_rows) && "Invalid indices");
      return this->data(r,c);
    }

    inline This& operator-=(const This& o) { return add(o,-1); }
    inline This& operator+=(const This& o) { return add(o,1); }

    This& operator*=(double c) &;

    This& elem_div(const This& o);

    std::ostream& print(std::ostream& out) const;

    This& operator=(const This& o);
    This& operator=(This&& o);

    void copy_submatrix(const MatrixSym<_T>& X, size_t offs);

    void mult(const MatrixSym<_T>& m, MatrixSqr<_T>& res) const;

    void mult(const Vector<_T>& x, Vector<_T>& res, double scale = 1.0) const;

    This& add(const MatrixSym<_T>& m, double scale);

    /** Multiply from left and right by a vector (a). Take symmetry into account. */
    _T lrmult(const Vector<_T>& a) const;

    void rank1_update(const Vector<_T>& b, ConstValType scale);

    /**
     * A rank-1 update of a matrix inverse using the Sherman-Morrison rule
     */
    void inverse_rank1_update(const Vector<_T>& x, MatrixSym<_T>& res, double scale = 1.0) const;

    void copy_subset(const This& o, const std::vector<bool>& include);

    // TODO : UT
    inline This& row(unsigned r, const std::initializer_list<_T>& init) {
      if(init.size() == this->m_rows) {
        std::copy_n(init.begin(), this->m_rows, this->data_ptr() + r*this->m_rows);
      }
      return *this;
    }
    inline void copy_row(unsigned r, Vector<_T>& rd, double scale = 1.0) {
      const size_t N(this->rows());
      rd.rows(N);
      for(unsigned c=0; c<N; ++c) rd[c] = scale*this->data(r,c);
    }
    void copy_to_aux() {
      if(this->m_row_length == (this->m_rows+1)) {
        _T* rp(this->m_data.data());
        _T* ar(rp + this->m_rows*this->m_row_length - 1);
        for(size_t i=0; i<this->m_rows; ++i) {
          std::copy_n(rp, i+1, ar);
          ar -= (this->m_row_length + 1);
          rp += this->m_row_length;
        }
      }
    }
    void copy_from_aux() {
      if(this->m_row_length == (this->m_rows+1)) {
        _T* rp(this->m_data.data());
        _T* ar(rp + this->m_rows*this->m_row_length - 1);
        for(size_t i=0; i<this->m_rows; ++i) {
          std::copy_n(ar, i+1, rp);
          ar -= (this->m_row_length + 1);
          rp += this->m_row_length;
        }
      }
    }
  protected:
    inline _T& data(size_t r, size_t c) {
      if(c > r) std::swap(r, c);
      return this->m_data[r*this->m_row_length + c];
    }
    inline ConstValType data(size_t r, size_t c) const {
      if(c > r) std::swap(r, c);
      return this->m_data[r*this->m_row_length + c];
    }
  };

  template<class _T>
  inline std::ostream& operator<<(std::ostream& out, const MatrixSym<_T>& m) {
    return m.print(out);
  }

  template<class _T = double>
  class MatrixTria : public MatrixBase<_T> {
  public:
    using This = MatrixTria<_T>;
    using Base = MatrixBase<_T>;
    using ConstValType = typename Base::ConstValType;

    MatrixTria():Base(0,0) { }

    MatrixTria(size_t rows):Base(rows, rows) { }

    MatrixTria(const This& o):Base(o) { }

    MatrixTria(This&& o):Base(std::forward<This>(o)) { }

    MatrixTria(size_t rows, _T* data, size_t row_length = 0):Base(rows, rows, data, row_length) {}

    template<class _Elem2> friend class MatrixTria;
    template<class _Elem2> friend class Matrix;

    using Base::rows;
    inline size_t cols() const { return this->m_rows; }

    void rows(size_t rows);

    inline void cols(size_t cols) { rows(cols); }

    inline _T trace() const {
      _T res(0.0);
      for(size_t i=0; i<this->m_rows; ++i) res += this->data(i, i);
      return res;
    }

    inline _T& operator()(size_t r, size_t c) {
      assert((r < this->m_rows) && (c < this->m_rows) && "Invalid indices");
      return this->data(r, c);
    }
    inline ConstValType operator()(size_t r, size_t c) const {
      assert((r < this->m_rows) && (c < this->m_rows) && "Invalid indices");
      return this->data(r,c);
    }

    /** Solve This*x = y and return square of elements in x */
    _T solve_square(Vector<_T>& y) const;

    /** Solve This*x = y */
    void solve(Vector<_T>& y) const;

    /** Invert and return determinant */
    _T invert(MatrixTria<_T>& res) const ;

    _T determinant() const;

    template<class _Elem2>
    inline This& operator-=(const MatrixTria<_Elem2>& o);
    template<class _Elem2>
    This& operator+=(const MatrixTria<_Elem2>& o);
    This& operator*=(double c) &;

    This& elem_div(const This& o);

    std::ostream& print(std::ostream& out) const;

    This& operator=(const This& o);
    This& operator=(const MatrixSym<_T>& o);
    This& operator=(This&& o);

    void mult(const Vector<_T>& x, Vector<_T>& res) const;

    /** Compute AB */
    void mult(const MatrixTria<_T>& x, MatrixTria<_T>& res) const;

    /** Compute AB^t */
    void mult_t(const MatrixTria<_T>& x, MatrixSym<_T>& res) const;

    /** Compute AtB */
    void t_mult(const MatrixTria<_T>& x, MatrixSym<_T>& res) const;
  protected:
    inline _T& data(size_t r, size_t c) { return this->m_data[r*this->m_row_length + c]; }
    inline ConstValType data(size_t r, size_t c) const { return this->m_data[r*this->m_row_length + c]; }
  };

  template<class _T = double>
  class Matrix : public MatrixBase<_T> {
  public:
    using This = Matrix<_T>;
    using Base = MatrixBase<_T>;
    using ConstValType = typename Base::ConstValType;

    Matrix(size_t rows, size_t cols):Base(rows, cols), m_cols(cols) { }

    Matrix(const This& o):Base(o), m_cols(o.m_cols) { }

    Matrix(This&& o):Base(std::forward<This>(o)), m_cols(o.m_cols) {
      o.m_cols = 0;
    }

    Matrix(size_t rows, size_t cols, _T* data, size_t row_length = 0):Base(rows, cols, data, row_length), m_cols(cols) {}

    using Base::rows;
    inline size_t rows() const { return this->m_rows; }
    inline size_t cols() const { return this->m_cols; }

    inline void rows(size_t rows) {
      if(this->m_data.owns_memory() && (this->m_rows != rows)) {
        this->m_rows = rows;
        this->m_data.size(this->m_rows*this->m_cols);
      }
    }
    inline void size(size_t rows, size_t cols) {
      if(this->m_data.owns_memory()) {
        this->m_rows = rows;
        this->m_cols = cols;
        this->m_row_length = cols;
        this->m_data.size(this->m_rows*this->m_cols);
      }
    }

    inline _T trace() const {
      _T res(0.0);
      for(size_t i=0; i<this->m_rows; ++i) res += this->data(i, i);
      return res;
    }

    inline _T& operator()(size_t r, size_t c) {
      assert((r < this->m_rows) && (c < this->m_cols) && "Invalid indices");
      return this->data(r, c);
    }
    inline ConstValType operator()(size_t r, size_t c) const {
      assert((r < this->m_rows) && (c < this->m_cols) && "Invalid indices");
      return this->data(r, c);
    }

    template<class _Elem2>
    This& operator-=(const Matrix<_Elem2>& o);
    template<class _Elem2>
    This& operator+=(const Matrix<_Elem2>& o);
    This& operator*=(double c);

    std::ostream& print(std::ostream& out) const;

    This& operator=(const This& o);
    This& operator=(This&& o);

    This& row(size_t r, const std::initializer_list<_T>& init);

    inline _T* row_data(size_t r) {
      assert((r < this->m_rows) && "Invalid indices");
      return &this->data(r,0);
    }
    inline const _T* row_data(size_t r) const {
      assert((r < this->m_rows) && "Invalid indices");
      return this->m_data.data() + r*this->m_row_length;
    }

    /** Multiply row r, by element r in vector */
    This& row_mult(const Vector<_T>& v);

    /**
     * Insert all rows from another matrix.
     * The source matrix must have the same number of columns.
     * @return If the operation wen well
     */
    typename Buffer<_T>::Error append(const This& o);
  protected:
    inline _T& data(size_t r, size_t c) { return this->m_data[r*this->m_row_length + c]; }
    inline ConstValType data(size_t r, size_t c) const { return this->m_data[r*this->m_row_length + c]; }

    size_t m_cols;
  };

  template<class _T> class MatrixDia;

  template<class _T>
  class MatrixSqr : public MatrixBase<_T> {
  public:
    using This = MatrixSqr<_T>;
    using Base = MatrixBase<_T>;
    using ConstValType = typename Base::ConstValType;

    MatrixSqr(size_t rows, _T* data, size_t rowlength = 0):Base(rows, rows, data, rowlength) {}

    MatrixSqr(size_t rows):Base(rows, rows) {}

    MatrixSqr(const MatrixSqr& o):Base(o) { }

    MatrixSqr(MatrixSqr&& o):Base(std::forward<This>(o)) { }

    using Base::rows;
    void rows(size_t rows);

    inline size_t rows() const { return this->m_rows; }
    inline size_t cols() const { return this->m_rows; }

    inline _T& operator()(size_t r, size_t c) {
      assert((r < this->m_rows) && (c < this->m_rows) && "Invalid indices");
      return this->data(r, c);
    }
    inline ConstValType operator()(size_t r, size_t c) const {
      assert((r < this->m_rows) && (c < this->m_rows) && "Invalid indices");
      return this->data(r, c);
    }

    This& operator=(const This& o);
    This& operator=(const MatrixSym<_T>& o);
    This& operator=(This&& o);

    /**
     * Multiply given matrix from left and right by this:
     * res = this*D*this^t
     * @param ndiag Number of leading non-zero diagonal elements
     */
    void lrmult(const MatrixDia<_T>& D, MatrixSym<_T>& res, size_t ndiag = 0) const;

    /** res = this*D*B^t */
    void lrmult(const MatrixDia<_T>& D, const This& B, This& res) const;

    /** res = this*B^t */
    void mult(const This& B, This& res) const;

    void mult(const Vector<_T>& v, Vector<_T>& res) const;

    /** Reverse the rows */
    void reverse_rows();

    void transpose();

    std::ostream& print(std::ostream& out) const;

    inline This& row(size_t r, const std::initializer_list<_T>& init) {
      if(init.size() == this->m_rows) {
        std::copy_n(init.begin(), this->m_rows, this->data_ptr() + r*this->m_rows);
      }
      return *this;
    }
  protected:
    inline _T& data(size_t r, size_t c) { return this->m_data[r*this->m_row_length + c]; }
    inline ConstValType data(size_t r, size_t c) const { return this->m_data[r*this->m_row_length + c]; }
  };

  template<class _T>
  class MatrixDia : public MatrixBase<_T> {
  public:
    using This = MatrixDia<_T>;
    using Base = MatrixBase<_T>;
    using ConstValType = typename Base::ConstValType;

    MatrixDia(size_t rows):Base(rows, 1), m_dummy(0.0) {}

    MatrixDia(size_t rows, _T* data, size_t rowlength = 1):Base(rows, 1, data, rowlength), m_dummy(0.0) {}

    MatrixDia(Vector<_T>& data):Base(data.rows(), 1, data.data_ptr(), 1), m_dummy(0.0) {}

    MatrixDia(const This& o):Base(o), m_dummy(0.0) {}

    MatrixDia(This&& o):Base(std::forward<This>(o)), m_dummy(0.0) {}

    using Base::rows;
    inline size_t cols() const { return this->m_rows; }

    inline This& operator=(const This& o) {
      if(this != &o) Base::copy_from(o);
      return *this;
    }
    inline This& operator=(This&& o) {
      if(this != &o) Base::move_from(std::forward<This>(o));
      return *this;
    }

    inline _T& operator[](size_t r) {
      assert((r < this->m_rows) && "Invalid indices");
      return this->m_data[r*this->m_row_length];
    }
    inline ConstValType operator[](size_t r) const {
      assert((r < this->m_rows) && "Invalid indices");
      return this->m_data[r*this->m_row_length];
    }

    inline _T& operator()(size_t r, size_t c) {
      assert((r < this->m_rows) && (c < this->m_rows)&& "Invalid indices");
      return (r==c) ? this->m_data[r*this->m_row_length] : m_dummy;
    }
    inline ConstValType operator()(size_t r, size_t c) const {
      assert((r < this->m_rows) && (c < this->m_rows) && (r == c) && "Invalid indices");
      return (r==c) ? this->m_data[r*this->m_row_length] : m_dummy;
    }

    std::ostream& print(std::ostream& out) const {
      for(size_t i=0; i<this->m_rows; ++i) {
        out<<i<<" : "<<this->m_data[i*this->m_row_length]<<std::endl;
      }
      return out;
    }
  protected:
    _T m_dummy;
  };

  template<class _T>
  inline std::ostream& operator<<(std::ostream& out, const MatrixDia<_T>& m) {
    return m.print(out);
  }

  template<class _T>
  inline std::ostream& operator<<(std::ostream& out, const MatrixSqr<_T>& m) {
    return m.print(out);
  }

  // ---- Vector ----

  template<class _T>
  Vector<_T>& Vector<_T>::operator*=(double c) & {
    const unsigned N(m_data.size());
    for(unsigned i=0; i<N; ++i) this->m_data[i] *= c;
    return *this;
  }

  template<class _T>
  Vector<_T>& Vector<_T>::operator+=(const This& o) {
    assert((o.rows() == m_data.size()) && "Invalid vector size");
    const unsigned N(m_data.size());
    for(unsigned i=0; i<N; ++i) this->m_data[i] += o(i);
    return *this;
  }

  template<class _T>
  std::ostream& Vector<_T>::print(std::ostream& out) const {
    out<<"{";
    for(unsigned i=0; i<m_data.size(); ++i) {
      if(i) out<<", ";
      out<<m_data[i];
    }
    out<<"}";
    return out;
  }

  template<class _T>
  inline void Vector<_T>::add(const This& o, double scale) {
    if(this->rows() == o.rows()) {
      for(unsigned r=0; r<this->m_data.size(); ++r) m_data[r] += scale*o[r];
    }
  }

  template<class _T>
  inline void Vector<_T>::add(const This& o, This& res, double scale) const {
    if(this->rows() == o.rows()) {
      for(unsigned r=0; r<this->m_data.size(); ++r)  res[r] = m_data[r] + scale*o[r];
    }
  }

  template<class _T>
  inline void Vector<_T>::assign(const This& o, double scale) {
    if(this->rows() == o.rows()) {
      for(unsigned r=0; r<this->m_data.size(); ++r) m_data[r] = scale*o[r];
    }
  }

  template<class _T>
  _T Vector<_T>::mult(const This& o) const {
    assert((o.rows() == m_data.size()) && "Invalid vector size");
    _T res(0.0);
    for(unsigned i=0; i<m_data.size(); ++i) res += m_data[i]*o[i];
    return res;
  }

  template<> void Vector<double>::add(const This& o, double scale);

  template<> void Vector<double>::add(const This& o, This& res, double scale) const;

  template<> void Vector<double>::assign(const This& o, double scale);

  template<> double Vector<double>::mult(const This& o) const;

  template<class _T>
  Vector<_T>&  Vector<_T>::elem_mult(const This& o) {
    assert((o.rows() == m_data.size()) && "Invalid vector size");
    for(unsigned i=0; i<m_data.size(); ++i) m_data[i] *= o[i];
    return *this;
  }

  template<class _T>
  void Vector<_T>::copy_subset(const This& o, const std::vector<bool>& include) {
    assert(o.rows() == include.size() && "Invalid size");
    unsigned i=0;
    for(unsigned j=0; j<o.rows(); ++j) {
      if(include[j]) {
        assert((i < m_data.size()) && "Invalid size");
        m_data[i++] = o[j];
      }
    }
  }

  template<class _T>
  void Vector<_T>::insert(unsigned r, ConstValType val) {
    const size_t N(this->size());
    if(r <= N) {
      this->rows(N+1);
      for(size_t i=N; i>r; --i) m_data[i] = m_data[i-1];
      m_data[r] = val;
    }
  }

  template<class _T>
  _T Vector<_T>::remove(unsigned r) {
    const size_t N(this->size()-1);
    if(r <= N) {
      _T val = m_data[r];
      for(size_t i=r; i<N; ++i) m_data[i] = m_data[i+1];
      this->rows(N);
      return val;
    }
    return _T();
  }

  template<class _T>
  void Vector<_T>::reverse() {
    const unsigned Ns = m_data.size()/2;
    for(unsigned r=0; r<Ns; ++r) {
      std::swap(m_data[r], m_data[m_data.size() - 1 - r]);
    }
  }

  template<class _T>
  inline std::ostream& operator<<(std::ostream& out, const Vector<_T>& m) {
    return m.print(out);
  }

  // ---- MatrixBase ----

  template<class _T>
  void MatrixBase<_T>::copy_from(const MatrixBase& o) {
    m_rows = o.m_rows;
    m_row_length = o.m_row_length;
    m_data = o.m_data;
  }

  template<class _T>
  void MatrixBase<_T>::move_from(MatrixBase&& o) {
    m_rows = o.m_rows;
    m_row_length = o.m_row_length;
    m_data = std::move(o.m_data);
    o.m_rows = o.m_row_length = 0;
  }

  // ---- MatrixSym ----

  template<class _T>
  void MatrixSym<_T>::rows(size_t rows, bool aux) {
    if(this->m_data.owns_memory() && (rows != this->m_rows)) {
      size_t rowl = aux ? rows+1 : rows;
      if(rowl > this->m_row_length) {
        // needs to be resized
        // Should be fulfilled (rows > this->m_rows) since m_rows <= m_row_length
        this->m_data.size(rows*rowl);
        if(this->m_rows > 0) {
          for(size_t r=this->m_rows-1; r>0; --r) {
            for(size_t c=r+1; c>0; --c) {
              this->m_data[r*rowl + c - 1] = this->m_data[r*this->m_row_length + c - 1];
            }
            for(size_t c=r+1; c<rows; ++c) this->m_data[r*rowl + c] = 0;
          }
          for(size_t c=1; c<rows; ++c) this->m_data[c] = 0;
        }
        this->m_row_length = rowl;
        this->m_rows = rows;
      }
      else {
        this->m_rows = rows;
        // TODO: Optionally clear mem if rows >= m_rows
      }
    }
  }

  template<class _T>
  void MatrixSym<_T>::remove(size_t row) {
    if(row < this->m_rows) {
      for(size_t r=row; r<this->m_rows-1; ++r) {
        for(size_t c=0; c<row; ++c) this->data(r,c) = this->data(r+1,c);
      }
      for(size_t r=row; r<this->m_rows-1; ++r) {
        for(size_t c=row; c<=r; ++c) this->data(r,c) = this->data(r+1,c+1);
      }
      --this->m_rows;
    }
  }

  template<class _T>
  _T MatrixSym<_T>::trace(const This& B) const {
    _T res(0.0);
    for(size_t r=0; r<this->m_rows; ++r) {
      for(size_t i=0; i<this->m_rows; ++i) res += this->data(r,i)*B(i, r);
    }
    return res;
  }

  template<class _T>
  _T MatrixSym<_T>::norm1b() const {
    _T res(0.0);
    const _T* rp(0);
    for(size_t r=0; r<this->m_rows; ++r) {
      rp = this->row_data(r);
      for(size_t i=0; i<r; ++i, ++rp) res += 2.0*std::abs(*rp);
      res += std::abs(*rp);
    }
    return res;
  }

  template<class _T>
  _T MatrixSym<_T>::norm_frobenius2() const {
    _T res(0.0);
    for(size_t r=0; r<this->m_rows; ++r) {
      for(size_t i=0; i<r; ++i) res += 2.0*square(this->data(r,i));
      res += square(this->data(r,r));
    }
    return res;
  }

  template<class _T>
  MatrixSym<_T>& MatrixSym<_T>::operator*=(double v) & {
    const size_t N(this->m_rows);
    _T* rp(0);
    for(size_t i=0; i<N; ++i) {
      rp = this->row_data(i);
      for(size_t c=0; c<=i; ++c) rp[c] *= v;
    }
    return *this;
  }

  template<class _T>
  MatrixSym<_T>& MatrixSym<_T>::elem_div(const This& o) {
    assert((this->rows() == o.rows()) && "Invalid matrix dimensions");
    _T* rp(0);
    const _T *rpo(0);
    for(size_t i=0; i<this->m_rows; ++i) {
      rp = this->row_data(i);
      rpo = o.row_data(i);
      for(size_t c=0; c<=i; ++c) rp[c] /= rpo[c];
    }
    return *this;
  }

  template<class _T>
  std::ostream& MatrixSym<_T>::print(std::ostream& out) const {
    for(size_t i=0; i<this->m_rows; ++i) {
      out<<i<<" : ";
      for(size_t j=0; j<=i; ++j) out<<this->data(i,j)<<" ";
      out<<std::endl;
    }
    return out;
  }

  template<class _T>
  MatrixSym<_T>& MatrixSym<_T>::operator=(const This& o) {
    if(this != &o) Base::copy_from(o);
    return *this;
  }

  template<class _T>
  MatrixSym<_T>& MatrixSym<_T>::operator=(This&& o) {
    if(this != &o) Base::move_from(std::forward<Base>(o));
    return *this;
  }

  template<class _T>
  void MatrixSym<_T>::copy_submatrix(const MatrixSym<_T>& X, size_t offs) {
    if(this->m_rows < X.rows()) {
      // copy to smaller matrix
      const size_t N(std::min(this->m_rows, X.rows() - offs));
      for(size_t rx=0; rx<N; ++rx) {
        for(size_t cx=0; cx<=rx; ++cx) this->data(rx,cx) = X(rx+offs, cx+offs);
      }
    }
    else {
      // copy to larger matrix
      const size_t N(std::min(X.rows(), this->m_rows - offs));
      for(size_t rx=0; rx<N; ++rx) {
        for(size_t cx=0; cx<=rx; ++cx) this->data(rx + offs,cx + offs) = X(rx, cx);
      }
    }
  }

  template<class _T>
  void MatrixSym<_T>::mult(const MatrixSym<_T>& m, MatrixSqr<_T>& res) const {
    assert(this->m_rows == m.rows() && "Invalid matrix dimension");
    assert(this->m_rows == res.rows() && "Invalid matrix dimension");
    _T s;
    for(size_t r=0; r<this->m_rows; ++r) {
      for(size_t c=0; c<this->m_rows; ++c) {
        s = 0;
        for(size_t i=0; i<this->m_rows; ++i) s+= this->data(r, i)*m(i, c);
        res(r, c) = s;
      }
    }
  }

  template<class _T>
  void MatrixSym<_T>::mult(const Vector<_T>& x, Vector<_T>& res, double scale) const {
    assert(this->m_rows == res.rows() && "Invalid size");
    assert(this->m_rows == x.rows() && "Invalid size");
    for(size_t r=0; r<this->m_rows; ++r) {
      res[r] = 0.0;
      for(size_t c=0; c<this->m_rows; ++c) res[r] += scale*this->data(r, c)*x[c];
    }
  }

  template<class _T>
  MatrixSym<_T>& MatrixSym<_T>::add(const MatrixSym<_T>& o, double scale) {
    assert(this->m_rows == o.m_rows && "Invalid matrix dimensions");
    _T* rp(0);
    const _T* rpo(0);
    for(size_t i=0; i<this->m_rows; ++i) {
      rp = this->row_data(i);
      rpo = o.row_data(i);
      for(size_t c=0; c<=i; ++c) rp[c] += scale*rpo[c];
    }
    return *this;
  }

  template<class _T>
  _T MatrixSym<_T>::lrmult(const Vector<_T>& a) const {
    assert(this->rows() == a.size() && "Invalid vector size");
    _T res(0.0);
    _T ar2(0.0);
    const _T* rp(0);
    for(size_t r=0; r<this->m_rows; ++r) {
      rp = this->row_data(r);
      ar2 = 2.0*a[r];
      for(size_t c=0; c<r; ++c, ++rp) res += ar2*a[c]*(*rp);
      res += square(a[r])*(*rp);
    }
    return res;
  }

  template<class _T>
  void MatrixSym<_T>::rank1_update(const Vector<_T>& b, ConstValType scale) {
    for(size_t r=0; r<this->m_rows; ++r) {
      this->data(r,r) += square(b[r])*scale;
      for(size_t c=0; c<r; ++c) this->data(r,c) += b[r]*b[c]*scale;
    }
  }

  template<> void MatrixSym<double>::mult(const Vector<double>& x, Vector<double>& res, double scale) const;

  template<> void MatrixSym<double>::rank1_update(const Vector<double>& x, double scale);

  template<class _T>
  void MatrixSym<_T>::inverse_rank1_update(const Vector<_T>& x, MatrixSym<_T>& res, double scale) const {
    assert(this->m_rows == res.rows() && "Invalid size");
    assert(this->m_rows == x.rows() && "Invalid size");
    res = *this;
    Vector<_T> b(this->m_rows);
    // b = A^-1x
    this->mult(x, b);
    _T denom = -1.0/(1.0 + x.mult(b));
    res.rank1_update(b, denom);
    if(scale != 1.0) res *= 1.0/scale;
  }

  template<class _T>
  void MatrixSym<_T>::copy_subset(const This& o, const std::vector<bool>& include) {
    assert(o.rows() == include.size() && "Invalid size");
    size_t ri(0), ci(0);
    for(size_t r=0; r<o.rows(); ++r) {
      if(include[r]) {
        assert(ri < this->m_rows && "Invalid size");
        ci = 0;
        for(size_t c=0; c<=r; ++c) {
          if(include[c]) {
            assert(ci < this->m_rows && "Invalid size");
            this->data(ri,ci++) = o(r,c);
          }
        }
        ++ri;
      }
    }
  }

  // ---- MatrixTria ----

  template<class _T>
  void MatrixTria<_T>::rows(size_t rows) {
    if(this->m_data.owns_memory() && (this->m_rows != rows)) {
      if(rows > this->m_row_length) {
        // needs to be resizes
        // Should be fulfilled (rows > this->m_rows) since m_rows <= m_row_length

        this->m_data.size(rows*rows);
        if(this->m_rows > 0) {
          for(size_t r=this->m_rows-1; r>0; --r) {
            for(size_t c=r+1; c>0; --c) {
              this->m_data[r*rows + c - 1] = this->m_data[r*this->m_rows + c - 1];
            }
            for(size_t c=r+1; c<rows; ++c) this->m_data[r*rows + c] = 0;
          }
          for(size_t c=1; c<rows; ++c) this->m_data[c] = 0;
        }
        this->m_row_length = this->m_rows = rows;
      }
      else {
        this->m_rows = rows;
        // TODO: Optionaly clear exposed mem
      }
    }
  }

  template<class _T>
  _T MatrixTria<_T>::solve_square(Vector<_T>& y) const {
    _T s, sqsum(0.0);
    for(size_t r=0; r<this->m_rows; ++r) {
      s = 0.0;
      for(size_t c=0; c<r; ++c) s += this->data(r,c) * y(c);
      y(r) = (y(r) - s)/this->data(r,r);
      sqsum += square(y(r));
    }
    return sqsum;
  }

  template<class _T>
  void MatrixTria<_T>::solve(Vector<_T>& y) const {
    _T s;
    for(size_t r=0; r<this->m_rows; ++r) {
      s = 0.0;
      for(size_t c=0; c<r; ++c) s += this->data(r,c) * y(c);
      y(r) = (y(r) - s)/this->data(r,r);
    }
  }

  template<class _T>
  _T MatrixTria<_T>::invert(MatrixTria<_T>& res) const {
    Vector<_T> y(this->m_rows);
    _T det(1.0);
    for(size_t c=0; c<this->m_rows; ++c) {
      det *= this->data(c,c);
      y.clear();
      y[c] = 1.0;
      this->solve(y);
      for(size_t r=c; r<this->m_rows; ++r) res(r, c) = y(r);
    }
    return det;
  }

  template<class _T>
  _T MatrixTria<_T>::determinant() const {
    _T res(1.0);
    for(size_t i=0; i<this->m_rows; ++i) res *= this->data(i,i);
    return res;
  }

  template<class _T>
  template<class _Elem2>
  MatrixTria<_T>& MatrixTria<_T>::operator-=(const MatrixTria<_Elem2>& o) {
    assert(this->m_rows == o.m_rows && "Invalid matrix dimensions");
    const size_t N(this->m_data.size());
    for(size_t i=0; i<N; ++i) this->m_data[i] -= o.m_data[i];
    return *this;
  }

  template<class _T>
  template<class _Elem2>
  MatrixTria<_T>& MatrixTria<_T>::operator+=(const MatrixTria<_Elem2>& o) {
    assert(this->m_rows == o.m_rows && "Invalid matrix dimensions");
    const size_t N(this->m_data.size());
    for(size_t i=0; i<N; ++i) this->m_data[i] += o.m_data[i];
    return *this;
  }

  template<class _T>
  MatrixTria<_T>& MatrixTria<_T>::operator*=(double c) & {
    const size_t N(this->m_data.size());
    for(size_t i=0; i<N; ++i) this->m_data[i] *= c;
    return *this;
  }

  template<class _T>
  MatrixTria<_T>& MatrixTria<_T>::elem_div(const This& o) {
    assert((rows() == o.rows()) && "Invalid row count");
    const size_t N(this->m_data.size());
    for(size_t i=0; i<N; ++i) this->m_data[i] /= o.m_data[i];
    return *this;
  }

  template<class _T>
  std::ostream& MatrixTria<_T>::print(std::ostream& out) const {
    for(size_t i=0; i<this->m_rows; ++i) {
      out<<i<<" : ";
      for(size_t j=0; j<=i; ++j) out<<this->data(i,j)<<" ";
      out<<std::endl;
    }
    return out;
  }

  template<class _T>
  MatrixTria<_T>& MatrixTria<_T>::operator=(const This& o) {
    if(this != &o) Base::copy_from(o);
    return *this;
  }

  template<class _T>
  MatrixTria<_T>& MatrixTria<_T>::operator=(const MatrixSym<_T>& o) {
    if(this->rows() == o.rows()) {
      for(size_t r=0; r<this->m_rows; ++r) {
        std::copy_n(o.row_data(r), r+1, this->row_data(r));
      }
    }
    return *this;
  }


  template<class _T>
  MatrixTria<_T>& MatrixTria<_T>::operator=(This&& o) {
    if(this != &o) Base::move_from(std::forward<This>(o));
    return *this;
  }

  template<class _T>
  void MatrixTria<_T>::mult(const Vector<_T>& x, Vector<_T>& res) const {
    assert(this->m_rows == x.rows() && "Invalid vector size");
    for(size_t r=0; r<this->m_rows; ++r) {
      res(r) = 0.0;
      for(size_t c=0; c<=r; ++c) res(r) += this->data(r,c) * x(c);
    }
  }

  template<class _T>
  void MatrixTria<_T>::mult(const MatrixTria<_T>& x, MatrixTria<_T>& res) const {
    assert(this->m_rows == x.rows() && "Invalid size");
    assert(this->m_rows == res.rows() && "Invalid size");
    _T s;
    for(size_t r=0; r<this->m_rows; ++r) {
      for(size_t c=0; c<=r; ++c) {
        s = 0.0;
        for(size_t i=0; i<=r; ++i) s+= this->data(r,i)*x(i, c);
        res(r, c) = s;
      }
    }
  }

  template<class _T>
  void MatrixTria<_T>::mult_t(const MatrixTria<_T>& x, MatrixSym<_T>& res) const {
    assert(this->m_rows == x.rows() && "Invalid size");
    assert(this->m_rows == res.rows() && "Invalid target dimensions");
    _T s;
    size_t N;
    for(size_t r=0; r<this->m_rows; ++r) {
      for(size_t c=0; c<=r; ++c) {
        s = 0.0;
        N = std::min(r, c);
        for(size_t i=0; i<=N; ++i) s+= this->data(r,i) * x(c, i);
        res(r, c) = s;
      }
    }
  }

  template<class _T>
  void MatrixTria<_T>::t_mult(const MatrixTria<_T>& x, MatrixSym<_T>& res) const {
    assert(this->m_rows == x.rows() && "Invalid size");
    assert(this->m_rows == res.rows() && "Invalid target dimensions");
    _T s;
    size_t N;
    for(size_t r=0; r<this->m_rows; ++r) {
      for(size_t c=0; c<=r; ++c) {
        s = 0.0;
        N = std::max(r, c);
        for(size_t i=N; i<this->m_rows; ++i) s+= this->data(i,r)*x(i, c);
        res(r, c) = s;
      }
    }
  }

  template<class _T>
  inline std::ostream& operator<<(std::ostream& out, const MatrixTria<_T>& m) {
    return m.print(out);
  }

  // ---- Matrix ----

  template<class _T>
  template<class _Elem2>
  Matrix<_T>& Matrix<_T>::operator-=(const Matrix<_Elem2>& o) {
    assert((this->m_rows == o.m_rows) && (this->m_cols == o.m_cols) && "Invalid matrix dimensions");
    const size_t N(this->m_data.size());
    for(size_t i=0; i<N; ++i) this->m_data[i] -= o.m_data[i];
    return *this;
  }

  template<class _T>
  template<class _Elem2>
  Matrix<_T>& Matrix<_T>::operator+=(const Matrix<_Elem2>& o) {
    assert((this->m_rows == o.m_rows) && (this->m_cols == o.m_cols) && "Invalid matrix dimensions");
    const size_t N(this->m_data.size());
    for(size_t i=0; i<N; ++i) this->m_data[i] += o.m_data[i];
    return *this;
  }

  template<class _T>
  Matrix<_T>& Matrix<_T>::operator*=(double c) {
    const size_t N(this->m_data.size());
    for(size_t i=0; i<N; ++i) this->m_data[i] *= c;
    return *this;
  }

  template<class _T>
  std::ostream& Matrix<_T>::print(std::ostream& out) const {
    for(size_t i=0; i<this->m_rows; ++i) {
      out<<i<<" : ";
      for(size_t j=0; j<this->m_cols; ++j)  out<<this->data(i, j)<<" ";
      out<<std::endl;
    }
    return out;
  }

  template<class _T>
  Matrix<_T>& Matrix<_T>::operator=(const This& o) {
    if(this != &o) {
      Base::copy_from(o);
      m_cols = o.m_cols;
    }
    return *this;
  }

  template<class _T>
  Matrix<_T>& Matrix<_T>::operator=(This&& o) {
    if(this != &o) {
      Base::move_from(std::forward<This>(o));
      m_cols = o.m_cols;
      o.m_cols = 0;
    }
    return *this;
  }

  template<class _T>
  Matrix<_T>& Matrix<_T>::row(size_t r, const std::initializer_list<_T>& init) {
    if(init.size() == this->m_cols) {
      std::copy_n(init.begin(), this->m_cols, row_data(r));
    }
    return *this;
  }

  template<class _T>
  Matrix<_T>& Matrix<_T>::row_mult(const Vector<_T>& v) {
    assert(v.rows() == this->rows() && "Invalid vector size");
    for(size_t i=0; i<this->m_rows; ++i) {
      auto x = v[i];
      for(size_t j=0; j<this->m_cols; ++j) this->data(i,j) *= x;
    }
    return *this;
  }

  template<class _T>
  typename Buffer<_T>::Error Matrix<_T>::append(const This& o) {
    if(this->owns_memory() && (this->m_cols == o.m_cols)) {
      auto res = this->m_data.append(o.m_data);
      if(!res) {
        this->m_rows += o.rows();
        return Buffer<_T>::OK;
      }
      else return res;
    }
    if(!this->owns_memory()) return Buffer<_T>::MemoryNotOwned;
    else return Buffer<_T>::InvalidSizeForAppend;
  }

  template<class _T>
  inline std::ostream& operator<<(std::ostream& out, const Matrix<_T>& m) {
    return m.print(out);
  }


  // ---- MatrixSqr ----

  template<class _T>
  void MatrixSqr<_T>::rows(size_t rows) {
    if(this->m_data.owns_memory() && (rows != this->m_rows)) {
      if(rows > this->m_row_length) {
        // needs to be resizes
        // Should be fulfilled (rows > this->m_rows) since m_rows <= m_row_length
        this->m_data.size(rows*rows);
        if(this->m_rows > 0) {
          for(size_t r=this->m_rows-1; r>0; --r) {
            for(size_t c=this->m_rows; c>0; --c) {
              this->m_data[r*rows + c - 1] = this->m_data[r*this->m_rows + c - 1];
            }
            for(size_t c=this->m_rows; c<rows; ++c) this->m_data[r*rows + c] = 0;
          }
          for(size_t c=this->m_rows; c<rows; ++c) this->m_data[c] = 0;
        }
        this->m_row_length = this->m_rows = rows;
      }
      else {
        this->m_rows = rows;
      }
    }
  }

  template<class _T>
  MatrixSqr<_T>& MatrixSqr<_T>::operator=(const This& o) {
    if(this != &o) Base::copy_from(o);
    return *this;
  }

  template<class _T>
  MatrixSqr<_T>& MatrixSqr<_T>::operator=(const MatrixSym<_T>& o) {
    if(this->m_rows == o.rows()) {
      for(size_t r=0; r<this->m_rows; ++r) {
        for(size_t c=0; c<r; ++c) data(r,c) = data(c,r) = o(r,c);
        data(r,r) = o(r,r);
      }
    }
    return *this;
  }

  template<class _T>
  MatrixSqr<_T>& MatrixSqr<_T>::operator=(This&& o) {
    if(this != &o) Base::move_from(std::forward<This>(o));
    return *this;
  }

  template<class _T>
  void MatrixSqr<_T>::lrmult(const MatrixDia<_T>& D, MatrixSym<_T>& res, size_t ndiag) const {
    assert(D.rows() == this->rows() && "Invalid number of rows");
    assert(res.rows() == this->rows() && "Invalid number of rows");
    assert(ndiag <= this->rows() && "Invalid number of diagonal elements");
    if(!ndiag) ndiag = this->m_rows;
    _T q;
    for(size_t r=0; r<this->m_rows; ++r) {
      for(size_t c=0; c<=r; ++c) {
        q = 0.0;
        for(size_t k=0; k<ndiag; ++k) q += this->data(r,k)*this->data(c,k)*D[k];
        res(r,c) = q;
      }
    }
  }

  template<class _T>
  void MatrixSqr<_T>::lrmult(const MatrixDia<_T>& D, const This& B, This& res) const {
    assert(D.rows() == this->rows() && "Invalid number of rows");
    assert(B.rows() == this->rows() && "Invalid number of rows");
    assert(res.rows() == this->rows() && "Invalid number of rows");
    _T q;
    for(size_t r=0; r<this->m_rows; ++r) {
      for(size_t c=0; c<this->m_rows; ++c) {
        q = 0.0;
        for(size_t k=0; k<this->m_rows; ++k) q += this->data(r,k)*B.data(c,k)*D[k];
        res(r,c) = q;
      }
    }
  }

  template<class _T>
  void MatrixSqr<_T>::mult(const This& B, This& res) const {
    assert(B.rows() == this->rows() && "Invalid number of rows");
    assert(res.rows() == this->rows() && "Invalid number of rows");
    _T q;
    for(size_t r=0; r<this->m_rows; ++r) {
      for(size_t c=0; c<this->m_rows; ++c) {
        q = 0.0;
        for(size_t k=0; k<this->m_rows; ++k) q += this->data(r,k)*B(c,k);
        res(r,c) = q;
      }
    }
  }

  template<class _T>
  void MatrixSqr<_T>::mult(const Vector<_T>& v, Vector<_T>& res) const {
    assert(v.size() == this->rows() && "Invalid number of rows");
    assert(res.size() == this->rows() && "Invalid number of rows");
    _T q;
    for(size_t r=0; r<this->m_rows; ++r) {
      q = 0.0;
      for(size_t c=0; c<this->m_rows; ++c) q += this->data(r,c)*v[c];
      res[r] = q;
    }
  }

  template<class _T>
  void MatrixSqr<_T>::reverse_rows() {
    const size_t Ns = this->m_rows/2;
    for(size_t r=0, r2=(this->m_rows-1); r<Ns; ++r, --r2) {
      for(size_t c=0; c<this->m_rows; ++c) std::swap(this->data(r,c), this->data(r2, c));
    }
  }

  template<class _T>
  void MatrixSqr<_T>::transpose() {
    const size_t Ns = this->m_rows - 1;
    for(size_t r=0; r<Ns; ++r) {
      for(size_t c=r+1; c<this->m_rows; ++c) std::swap(this->data(r,c), this->data(c, r));
    }
  }

  template<class _T>
  std::ostream& MatrixSqr<_T>::print(std::ostream& out) const {
    for(size_t i=0; i<this->m_rows; ++i) {
      out<<i<<" : ";
      for(size_t j=0; j<this->m_rows; ++j) out<<this->data(i,j)<<" ";
      out<<std::endl;
    }
    return out;
  }

  // ----

  inline MatrixSym<double> submatrix(unsigned maxrows, const MatrixSym<double>& m) {
    unsigned rows = std::min<unsigned>(m.rows(), maxrows);
    return MatrixSym<double>(rows, (double*)m.data_ptr(), m.row_length());
  }

  extern template class Vector<double>;
  extern template class Vector<std::complex<double>>;
  extern template class Matrix<double>;
  extern template class Matrix<std::complex<double>>;
  extern template class MatrixDia<double>;
  extern template class MatrixDia<std::complex<double>>;
  extern template class MatrixSqr<double>;
  extern template class MatrixSqr<std::complex<double>>;
  extern template class MatrixSym<double>;
  extern template class MatrixSym<std::complex<double>>;
  extern template class MatrixTria<double>;
  extern template class MatrixTria<std::complex<double>>;
}

#endif // sfi_Matrix_h
