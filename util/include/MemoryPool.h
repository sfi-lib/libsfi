#ifndef sfi_MemoryPool_h
#define sfi_MemoryPool_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string>
#include <vector>
#include <map>
#include <type_traits>
#include <unordered_map>
#include <iostream>

#include "type_util.h"

namespace sfi {

  class IPoolEntry {
  public:
    virtual ~IPoolEntry() {}

    virtual const std::string& get_type_name() const = 0;
  };

  template<class _T>
  class PoolEntry : public IPoolEntry {
  public:
    PoolEntry():m_created(0) {}

    virtual ~PoolEntry() {
      if((m_created - m_pool.size()) != 0) {
        std::cout<<"PoolEntry{"+type_name<_T>()+"} created "<<m_created<<" instances, pool contains "<<m_pool.size()<<" instances."<<std::endl;
      }
      for(auto* s : m_pool) delete s;
      m_pool.clear();
    }

    // ---- implementation of IPoolEntry ----

    virtual const std::string& get_type_name() const {
      static std::string _type_name = sfi::type_name<_T>();
      return _type_name;
    }

    // ---- public methods ----

    template<class ... _Args>
    inline _T* get(_Args&& ... args) {
      _T* res = 0;
      if(!m_pool.empty()) {
        res = m_pool.back();
        m_pool.pop_back();
        res->reinitialize(std::forward<_Args>(args)...);
      }
      else {
        res = new _T(this, std::forward<_Args>(args)...);
        ++m_created;
      }
      return res;
    }

    inline void put(_T* spl) {
      if(spl != 0) m_pool.push_back(spl);
    }

    inline unsigned created() const { return m_created; }

    inline size_t size() const { return m_pool.size(); }
  protected:
    unsigned m_created;
    std::vector<_T*> m_pool;
  };

  class MemoryPool {
  public:
    MemoryPool();

    ~MemoryPool();

    template<class _T>
    inline PoolEntry<_T>* pool() {
      std::string key = sfi::type_name<_T>();
      auto entry = m_pools.find(key);
      if(entry != m_pools.end()) return dynamic_cast<PoolEntry<_T>*>(entry->second);
      else {
        PoolEntry<_T>* p = new PoolEntry<_T>();
        m_pools[key] = p;
        return p;
      }
    }
  protected:
    std::unordered_map<std::string, IPoolEntry*> m_pools;
  };
}

#endif // sfi_MemoryPool_h
