#ifndef sfi_Histogram_h
#define sfi_Histogram_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <vector>
#include <math.h>
#include <cmath>
#include <stddef.h>
#include <assert.h>
#include "sfi_math.h"

namespace sfi {
  /**
   * @class sfi::Histogram
   * @class sfi::Histogram2D
   *
   * Lightweight histogram classes.
   */
  template<bool _Unc = false, class _Elem = double>
  class HistogramBase {
  public:
    using This = HistogramBase<_Unc, _Elem>;

    This& operator=(This&& o);

    This& operator=(const This& o);

    inline _Elem min_x() const { return m_min_x; }

    inline _Elem max_x() const { return m_max_x; }

    inline size_t nbins() const { return this->m_data.size() - 2; }

    inline _Elem bin_content(size_t b) const {
      assert(b < m_data.size() && "Invalid bin");
      return this->m_data[b];
    }

    inline _Elem& bin_content(size_t b) {
      assert(b < m_data.size() && "Invalid bin");
      return this->m_data[b];
    }

    inline _Elem bin_uncertainty(size_t b) const {
      assert(b < m_data.size() && "Invalid bin");
      return this->m_unc[b];
    }

    inline void bin_uncertainty(size_t b, _Elem unc) {
      assert(b < m_data.size() && "Invalid bin");
      this->m_unc.set(b, unc);
    }

    inline _Elem underflow() const { return m_data[0]; }
    inline _Elem& underflow() { return m_data[0]; }

    inline _Elem overflow() const { return m_data.back(); }
    inline _Elem& overflow() { return m_data.back(); }

    /** Scale all bins (including under and overflow) by number */
    inline void scale(_Elem c) { for(auto& a : m_data) a *= c; }

    inline _Elem sum_weights(bool include_overflow = true) const {
      return include_overflow ? m_sum_weights + m_sum_weights_of : m_sum_weights;
    }

    /** @return Number of entries */
    inline size_t entries() const { return m_entries; }

    inline _Elem effective_entries(bool include_overflow = true) const {
      return square(sum_weights(include_overflow))/m_unc.sum_weight2(include_overflow);
    }
  protected:
    HistogramBase();

    HistogramBase(size_t nbins, _Elem minx, _Elem maxx);

    HistogramBase(const This& o);

    HistogramBase(This&& o);

    /** Uncertainty implementation */
    template<bool __Unc, class __Elem> struct _histo_unc {
      _histo_unc() {}
      _histo_unc(size_t) {}
      _histo_unc(const _histo_unc&) = default;
      _histo_unc(_histo_unc&&) = default;
      _histo_unc<__Unc, __Elem>& operator=(_histo_unc<__Unc, __Elem>&&) = default;
      _histo_unc<__Unc, __Elem>& operator=(const _histo_unc<__Unc, __Elem>&) = default;
      inline void add(size_t, __Elem){}
      inline __Elem operator[](size_t) const { return 0.0; }
      inline void set(size_t, __Elem){}
      inline __Elem sum_weight2(bool) const { return 1.0; }
    };

    template<class __Elem> struct _histo_unc<true, __Elem> {
      _histo_unc():m_unc(),m_sum_weight2(0.0), m_sum_weight2_of(0.0) {}
      _histo_unc(size_t nbins):m_unc(nbins, 0.0),m_sum_weight2(0.0), m_sum_weight2_of(0.0) {}
      _histo_unc(const _histo_unc&) = default;
      _histo_unc(_histo_unc&&) = default;
      _histo_unc<true, __Elem>& operator=(_histo_unc<true, __Elem>&&) = default;
      _histo_unc<true, __Elem>& operator=(const _histo_unc<true, __Elem>&) = default;
      inline void add(size_t binid, __Elem w){
        this->m_unc[binid] += w*w;
        (((binid > 0) && (binid < (m_unc.size()-1))) ? this->m_sum_weight2 : this->m_sum_weight2_of) += w*w;
      }
      inline __Elem operator[](size_t binid) const { return sqrt(m_unc[binid]); }
      inline void set(size_t binid, __Elem u){ m_unc[binid] = u*u; }
      inline __Elem sum_weight2(bool include_overflow) const {
        return include_overflow ? m_sum_weight2 + m_sum_weight2_of : m_sum_weight2;
      }
      std::vector<__Elem> m_unc;
      __Elem m_sum_weight2, m_sum_weight2_of;
    };

    void fill_bin(size_t i, _Elem w);

    std::vector<_Elem> m_data;
    _Elem m_min_x, m_max_x, m_sum_weights, m_sum_weights_of;
    _histo_unc<_Unc,_Elem> m_unc;
    size_t m_entries;
  };

  template<bool _Unc = false, class _Elem = double>
  class Histogram : public HistogramBase<_Unc, _Elem>{
  public:
    using This = Histogram<_Unc, _Elem>;
    using Base = HistogramBase<_Unc, _Elem>;

    Histogram();

    Histogram(size_t nbins, _Elem minx, _Elem maxx);

    Histogram(const This& o);

    Histogram(This&& o);

    This& operator=(This&& o);

    This& operator=(const This& o);

    inline size_t nbins_x() const { return this->nbins(); }

    void fill(_Elem x, _Elem w = 1.0);

    /** @return the sum of elements in bin i0->i1 (inclusively) */
    _Elem integral(size_t i0, size_t i1) const;

    /**
     * @param include_overflow If overflow (and underflow) should be included in the integrated histogram
     * @return An integrated version of this, i.e. the EDF (empirical distribution function)
     */
    This integrate(bool include_overflow = true) const;

    _Elem find_max_diff(const This& o);
  protected:
    size_t binid(_Elem x) const;

    _Elem m_inv_bin_width;
  };

  template<bool _Unc = false, class _Elem = double>
  class Histogram2D : public HistogramBase<_Unc, _Elem> {
  public:
    using This = Histogram2D<_Unc, _Elem>;
    using Base = HistogramBase<_Unc, _Elem>;

    Histogram2D();

    Histogram2D(size_t nbinsx, _Elem minx, _Elem maxx, size_t nbinsy, _Elem minxy, _Elem maxy);

    Histogram2D(const This& o);

    Histogram2D(This&& o);

    This& operator=(This&& o);

    This& operator=(const This& o);

    inline _Elem min_y() const { return m_min_y; }

    inline _Elem max_y() const { return m_max_y; }

    inline size_t nbins_x() const { return m_nbins_x; }

    inline size_t nbins_y() const { return m_nbins_y; }

    void fill(_Elem x, _Elem y, _Elem w = 1.0);

    inline _Elem bin_content(size_t bx, size_t by) const { return Base::bin_content(by + (bx-1)*m_nbins_y); }

    inline _Elem& bin_content(size_t bx, size_t by) { return Base::bin_content(by + (bx-1)*m_nbins_y); }

    inline _Elem bin_uncertainty(size_t bx, size_t by) const { return Base::bin_uncertainty(by + (bx-1)*m_nbins_y); }

    inline void bin_uncertainty(size_t bx, size_t by, _Elem unc) { Base::bin_uncertainty(by + (bx-1)*m_nbins_y, unc); }
  protected:
    size_t binid(_Elem x, _Elem y) const;

    size_t m_nbins_x, m_nbins_y;
    _Elem m_inv_bin_width_x, m_inv_bin_width_y;
    _Elem m_min_y, m_max_y;
  };

  /**** HistogramBase ****/
  template<bool _Unc, class _Elem>
  HistogramBase<_Unc, _Elem>::HistogramBase():
    m_data(), m_min_x(0), m_max_x(0), m_sum_weights(0.0), m_sum_weights_of(0.0), m_unc(), m_entries(0) {
  }

  template<bool _Unc, class _Elem>
  HistogramBase<_Unc, _Elem>::HistogramBase(size_t nbins, _Elem minx, _Elem maxx):
    m_data(nbins+2, 0.0), m_min_x(minx), m_max_x(maxx), m_sum_weights(0.0), m_sum_weights_of(0.0), m_unc(nbins+2), m_entries(0) {
  }

  template<bool _Unc, class _Elem>
  HistogramBase<_Unc, _Elem>::HistogramBase(const HistogramBase<_Unc, _Elem>& o):
    m_data(o.m_data), m_min_x(o.m_min_x), m_max_x(o.m_max_x), m_sum_weights(o.m_sum_weights), m_sum_weights_of(o.m_sum_weights_of),
    m_unc(o.m_unc), m_entries(o.m_entries) {
  }

  template<bool _Unc, class _Elem>
  HistogramBase<_Unc, _Elem>::HistogramBase(HistogramBase<_Unc, _Elem>&& o):
    m_data(std::move(o.m_data)), m_min_x(o.m_min_x), m_max_x(o.m_max_x), m_sum_weights(o.m_sum_weights), m_sum_weights_of(o.m_sum_weights_of),
    m_unc(std::move(o.m_unc)), m_entries(o.m_entries) {
  }

  template<bool _Unc, class _Elem>
  HistogramBase<_Unc, _Elem>& HistogramBase<_Unc, _Elem>::operator=(HistogramBase<_Unc, _Elem>&& o) {
    if(this != &o) {
      m_data = std::move(o.m_data);
      m_min_x = o.m_min_x;
      m_max_x = o.m_max_x;
      m_sum_weights = o.m_sum_weights;
      m_sum_weights_of = o.m_sum_weights_of;
      m_unc = std::move(o.m_unc);
      m_entries = o.m_entries;
    }
    return *this;
  }

  template<bool _Unc, class _Elem>
  HistogramBase<_Unc, _Elem>& HistogramBase<_Unc, _Elem>::operator=(const HistogramBase<_Unc, _Elem>& o) {
    if(this != &o) {
      m_data = o.m_data;
      m_min_x = o.m_min_x;
      m_max_x = o.m_max_x;
      m_sum_weights = o.m_sum_weights;
      m_sum_weights_of = o.m_sum_weights_of;
      m_unc = o.m_unc;
      m_entries = o.m_entries;
    }
    return *this;
  }

  template<bool _Unc, class _Elem>
  void HistogramBase<_Unc, _Elem>::fill_bin(size_t i, _Elem w) {
    ++this->m_entries;
    this->m_data[i] += w;
    this->m_unc.add(i, w);
    (((i > 0) && (i < (this->m_data.size()-1))) ? this->m_sum_weights : this->m_sum_weights_of) += w;
  }

  /**** Histogram ****/

  template<bool _Unc, class _Elem>
  Histogram<_Unc, _Elem>::Histogram():Base(), m_inv_bin_width(0) { }

  template<bool _Unc, class _Elem>
  Histogram<_Unc, _Elem>::Histogram(size_t nbins, _Elem minx, _Elem maxx):Base(nbins, minx,maxx),
  m_inv_bin_width(double(nbins)/(maxx-minx)) {
  }

  template<bool _Unc, class _Elem>
  Histogram<_Unc, _Elem>::Histogram(const Histogram<_Unc, _Elem>& o):Base(o), m_inv_bin_width(o.m_inv_bin_width) {}

  template<bool _Unc, class _Elem>
  Histogram<_Unc, _Elem>::Histogram(Histogram<_Unc, _Elem>&& o):Base(std::move(o)), m_inv_bin_width(o.m_inv_bin_width) {}

  template<bool _Unc, class _Elem>
  Histogram<_Unc, _Elem>& Histogram<_Unc, _Elem>::operator=(Histogram<_Unc, _Elem>&& o) {
    if(this != &o) {
      Base::operator =(std::move(o));
      m_inv_bin_width = o.m_inv_bin_width;
    }
    return *this;
  }

  template<bool _Unc, class _Elem>
  Histogram<_Unc, _Elem>& Histogram<_Unc, _Elem>::operator=(const Histogram<_Unc, _Elem>& o) {
    if(this != &o) {
      Base::operator =(o);
      m_inv_bin_width = o.m_inv_bin_width;
    }
    return *this;
  }

  template<bool _Unc, class _Elem>
  void Histogram<_Unc, _Elem>::fill(_Elem x, _Elem w) {
    if(std::isfinite(x)) this->fill_bin(this->binid(x), w);
  }

  template<bool _Unc, class _Elem>
  inline _Elem Histogram<_Unc, _Elem>::integral(size_t i0, size_t i1) const {
    assert((i0 < this->m_data.size()) && (i1 < this->m_data.size()) && "Invalid bin");
    _Elem res(0.0);
    for(unsigned i=i0; i<=i1; ++i) res += this->m_data[i];
    return res;
  }

  template<bool _Unc, class _Elem>
  Histogram<_Unc, _Elem> Histogram<_Unc, _Elem>::integrate(bool include_overflow) const {
    const size_t N(this->nbins());
    Histogram<_Unc, _Elem> res(N, this->m_min_x, this->m_max_x);
    _Elem acc(include_overflow ? this->underflow() : 0.0);
    const _Elem nf = 1.0/_Elem(this->sum_weights(include_overflow));
    for(size_t i=1; i<=N; ++i) {
      acc += this->bin_content(i);
      if(include_overflow && (i==N)) acc += this->overflow();
      res.bin_content(i) = acc*nf;
    }
    return res;
  }

  template<bool _Unc, class _Elem>
  _Elem Histogram<_Unc, _Elem>::find_max_diff(const This& o) {
    const size_t N(this->nbins());
    if(N != o.nbins()) return 0.0;
    _Elem max_diff(0.0);
    for(size_t i=1; i<=N; ++i) max_diff = std::max(max_diff, fabs(this->bin_content(i) - o.bin_content(i)));
    return max_diff;
  }

  template<bool _Unc, class _Elem>
  size_t Histogram<_Unc, _Elem>::binid(_Elem x) const {
    if(x < this->m_min_x) return 0;
    else if(x > this->m_max_x) return this->m_data.size()-1;
    else return (size_t)((x - this->m_min_x)*m_inv_bin_width + 1);
  }

  /**** Histogram2D ****/

  template<bool _Unc, class _Elem>
  Histogram2D<_Unc, _Elem>::Histogram2D():Base(), m_nbins_x(0), m_nbins_y(0),
    m_inv_bin_width_x(0), m_inv_bin_width_y(0), m_min_y(0), m_max_y(0) {
  }

  template<bool _Unc, class _Elem>
  Histogram2D<_Unc, _Elem>::Histogram2D(size_t nbinsx, _Elem minx, _Elem maxx, size_t nbinsy, _Elem miny, _Elem maxy):
    Base(nbinsx*nbinsy, minx, maxx),
    m_nbins_x(nbinsx), m_nbins_y(nbinsy),
    m_inv_bin_width_x(double(nbinsx)/(maxx-minx)), m_inv_bin_width_y(double(nbinsy)/(maxy-miny)),
    m_min_y(miny), m_max_y(maxy) {
  }

  template<bool _Unc, class _Elem>
  Histogram2D<_Unc, _Elem>::Histogram2D(const Histogram2D<_Unc, _Elem>& o):
    Base(o),
    m_nbins_x(o.m_nbins_x), m_nbins_y(o.m_nbins_y),
    m_inv_bin_width_x(o.m_inv_bin_width_x), m_inv_bin_width_y(o.m_inv_bin_width_y),
    m_min_y(o.m_min_y), m_max_y(o.m_max_y) {
  }

  template<bool _Unc, class _Elem>
  Histogram2D<_Unc, _Elem>::Histogram2D(Histogram2D<_Unc, _Elem>&& o):
    Base(o),
    m_nbins_x(o.m_nbins_x), m_nbins_y(o.m_nbins_y),
    m_inv_bin_width_x(o.m_inv_bin_width_x), m_inv_bin_width_y(o.m_inv_bin_width_y),
    m_min_y(o.m_min_y), m_max_y(o.m_max_y) {
  }

  template<bool _Unc, class _Elem>
  Histogram2D<_Unc, _Elem>& Histogram2D<_Unc, _Elem>::operator=(const Histogram2D<_Unc, _Elem>& o) {
    if(this != &o) {
      Base::operator =(o);
      m_nbins_x = o.m_nbins_x;
      m_nbins_y = o.m_nbins_y;
      m_inv_bin_width_x = o.m_inv_bin_width_x;
      m_inv_bin_width_y = o.m_inv_bin_width_y;
      m_min_y = o.m_min_y;
      m_max_y = o.m_max_y;
    }
    return *this;
  }

  template<bool _Unc, class _Elem>
  Histogram2D<_Unc, _Elem>& Histogram2D<_Unc, _Elem>::operator=(Histogram2D<_Unc, _Elem>&& o) {
    if(this != &o) {
      Base::operator =(std::move(o));
      m_nbins_x = o.m_nbins_x;
      m_nbins_y = o.m_nbins_y;
      m_inv_bin_width_x = o.m_inv_bin_width_x;
      m_inv_bin_width_y = o.m_inv_bin_width_y;
      m_min_y = o.m_min_y;
      m_max_y = o.m_max_y;
    }
    return *this;
  }

  template<bool _Unc, class _Elem>
  void Histogram2D<_Unc, _Elem>::fill(_Elem x, _Elem y, _Elem w) {
    if(std::isfinite(x)) this->fill_bin(this->binid(x, y), w);
  }

  template<bool _Unc, class _Elem>
  size_t Histogram2D<_Unc, _Elem>::binid(_Elem x, _Elem y) const {
    if(x < this->m_min_x) return 0;
    else if(x >= this->m_max_x) return this->m_data.size()-1;
    else if(y < this->m_min_y) return 0;
    else if(y >= this->m_max_y) return this->m_data.size()-1;
    else {
      size_t xp = (size_t)((x - this->m_min_x)*m_inv_bin_width_x);
      size_t yp = (size_t)((y - this->m_min_y)*m_inv_bin_width_y);
      return yp + xp*m_nbins_y + 1;
    }
  }

  extern template class HistogramBase<true,double>;
  extern template class HistogramBase<false,double>;

  extern template class Histogram<true,double>;
  extern template class Histogram<false,double>;

  extern template class Histogram2D<true,double>;
  extern template class Histogram2D<false,double>;
}

#endif // sfi_Histogram_h
