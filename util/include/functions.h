#ifndef sfi_functions_h
#define sfi_functions_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "util.h"

namespace sfi {
  struct NoopFunc {
    inline double eval(const double*) const {
      return 1.0;
    }
  };

  template<class _Lambda>
  struct LambdaFunc {
    LambdaFunc(_Lambda lambda):m_lambda(std::move(lambda)) {}

    inline double eval(const double*x) const { return m_lambda(x); }
  protected:
    _Lambda m_lambda;
  };

  template<class _Lambda>
  auto lambda_func(_Lambda lam) { return LambdaFunc<_Lambda>(std::move(lam)); }

  template<class _F1, class _F2>
  struct CombinedFunc {
    CombinedFunc(const _F1& f1, const _F2& f2):_f1(f1), _f2(f2) {
    }
    CombinedFunc(_F1&& f1, _F2&& f2):_f1(std::forward(f1)), _f2(std::forward(f2)) {
    }
    inline double eval(const double* x) const {
      return _f1.eval(x) * _f2.eval(x);
    }
  protected:
    _F1 _f1;
    _F2 _f2;
  };

  template<class _F1, class _F2>
  inline CombinedFunc<_F1, _F2> create_combined_func(const _F1& f1, const _F2& f2) {
    return CombinedFunc<_F1, _F2>(f1, f2);
  }

  template<class _TFunc> struct tremap_if_single : public tfalse {};

  template<class _TFunc>
  struct RemapFunc {
    using Val = t_result<decltype(&_TFunc::eval)(_TFunc, const double*)>;

    RemapFunc(const _TFunc& f, unsigned idx):m_f(f), m_idx(idx) {}
    inline Val eval(const double* x) const {
      m_data[0] = x[m_idx];
      return m_f.eval(m_data);
    }

    inline _TFunc& function() { return m_f; }
    inline const _TFunc& function() const { return m_f; }
  private:
    _TFunc m_f;
    unsigned m_idx;
    mutable double m_data[1];
  };

  template<class _TFunc>
  struct PassThroughFunc {
    using Val = t_result<decltype(&_TFunc::eval)(_TFunc, const double*)>;
    PassThroughFunc(const _TFunc& f):m_f(f) {}
    inline Val eval(const double* x) const {
      return m_f.eval(x);
    }
    private:
      _TFunc m_f;
  };

  template<class _TFunc>
  inline RemapFunc<_TFunc>
  create_single_var_func(const _TFunc& f, unsigned idx, typename std::enable_if<tremap_if_single<_TFunc>::value()>::type* = 0) {
    return RemapFunc<_TFunc>(f, idx);
  }

  template<class _TFunc>
  inline _TFunc
  create_single_var_func(const _TFunc& f, unsigned /*idx*/, typename std::enable_if<!tremap_if_single<_TFunc>::value()>::type* = 0) {
    return _TFunc(f);
  }

  /**
   * Evaluate 1D lambda style function, store x and y in vectors
   */
  template<class _Func>
  void evaluate_function_1D(_Func& f, unsigned npts, double xmin, double xmax, Vector<double>& xv, Vector<double>& yv) {
    xv.rows(npts); yv.rows(npts);
    for(unsigned i=0; i<npts; ++i) {
      xv[i] = xmin + (xmax - xmin)*double(i)/(npts - 1);
      yv[i] = f(xv[i]);
    }
  }

}
#endif // sfi_functions_h
