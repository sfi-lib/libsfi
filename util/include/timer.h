#ifndef sfi_timer_h
#define sfi_timer_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <chrono>

namespace sfi {
  /**
   * @class sfi::Timer
   *
   * Class responsible for measuring elapsed time, the template
   * parameter can be used to completely disable time measurements.
   *
   * Usage:
   * Timer<true> timer;
   * timer.start();
   * //do something time consuming
   * double elapsed_seconds = timer.stop();
   */
  template<bool _Enable = true> class Timer {
  public:
    using clock = std::chrono::high_resolution_clock;
    Timer(){}

    inline void start() {
      m_start = clock::now();
    }

    inline double stop() {
      std::chrono::duration<double> res = clock::now() - m_start;
      return res.count();
    }
  protected:
    std::chrono::time_point<clock> m_start;
  };

  template<> class Timer<false> {
  public:
    Timer(){}

    inline void start() { }

    inline double stop() { return 0.0; }
  };
}

#endif // sfi_timer_h
