#ifndef sfi_Parameters_h
#define sfi_Parameters_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <vector>
#include <map>
#include <string>
#include <assert.h>
#include <iostream>

namespace sfi {
  struct ExtID {
    explicit ExtID(size_t i):m_idx(i) {}
    explicit ExtID():m_idx(std::numeric_limits<size_t>::max()) {}
    inline bool valid() const { return m_idx != std::numeric_limits<size_t>::max(); }
    inline void operator++() { ++m_idx; }
    inline bool operator<(const ExtID& o) const { return m_idx < o.m_idx; }
    inline bool operator<=(const ExtID& o) const { return m_idx <= o.m_idx; }
    inline bool operator>(const ExtID& o) const { return m_idx > o.m_idx; }
    inline bool operator>=(const ExtID& o) const { return m_idx >= o.m_idx; }
    inline bool operator==(const ExtID& o) const { return m_idx == o.m_idx; }
    inline bool operator!=(const ExtID& o) const { return m_idx != o.m_idx; }
    inline size_t value() const { return m_idx; }
    inline std::ostream& print(std::ostream& out) const {
      return out<<"{"<<m_idx<<"}";
    }
  private:
    size_t m_idx;
  };
  inline std::ostream& operator<<(std::ostream& out, const ExtID& ei) {
    return ei.print(out);
  }

  class Parameter {
  public:
    friend class Parameters;
    friend class LikelihoodMaximizerBase;
    friend class Minuit;
    using This = Parameter;

    enum Type { Normal, Nuisance, AbsoluteNorm, RelativeNorm };

    inline const std::string& name() const { return m_name; }

    inline Type type() const { return m_type; }
    inline This& type(Type _t) { m_type = _t; return *this; }

    inline operator double() const { return work_value(); }

    inline bool has_work_value() const { return m_work_value != nullptr; }
    
    inline double work_value() const {
      assert(m_work_value !=0 && "Not properly initialized");
      return *m_work_value;
    }

    inline void work_value(double v) {
      assert(m_work_value !=0 && "Not properly initialized");
      *m_work_value = v;
    }

    inline double initial() const { return m_initial_value; }
    inline This& initial(double iv) { m_initial_value = iv; return *this; }

    bool limit(double minv, double maxv);
    void clear_limits();

    inline bool is_limited() const { return m_limited; }
    inline double limit_min() const { return m_min_value; }
    inline double limit_max() const { return m_max_value; }

    inline This& step_length(double sl) { m_step_length = sl; return *this; }
    inline double step_length() const { return m_step_length; }

    inline This& fixed(bool b) { m_fixed = b; return *this; }
    inline bool is_fixed() const { return m_fixed; }

    // ---- access the result from the minimization ----

    inline double value() const { return m_ml_value; }
    inline This& value(double v) { m_ml_value = v; return *this; }
    inline double uncertainty() const { return m_ml_unc; }
    inline This& uncertainty(double v) { m_ml_unc = v; return *this; }
    inline double uncertainty_up() const { return m_ml_unc_up; }
    inline This& uncertainty_up(double v) { m_ml_unc_up = v; return *this; }
    inline double uncertainty_down() const { return m_ml_unc_down; }
    inline This& uncertainty_down(double v) { m_ml_unc_down = v; return *this; }

    inline double global_cc() const { return m_ml_global_cc; }
    inline double uncertainty_parabolic() const { return m_ml_unc_parab; }

    void clear_uncertainties(bool only_asym = false);

    std::ostream& print(std::ostream& out) const;
  protected:
    Parameter(const std::string& name, unsigned index);

    inline ExtID ext_index() const { return m_ext_index; }
    inline int int_index() const { return m_internal_index; }

    inline This& ext_index(ExtID ei) { m_ext_index = ei; return *this; }
    inline This& int_index(int i) { m_internal_index = i; return *this; }
    inline This& global_cc(double v) { m_ml_global_cc = v; return *this; }
    inline This& uncertainty_parabolic(double v) { m_ml_unc_parab = v; return *this; }

    inline void save_ml_unc() { m_s_ml_unc = m_ml_unc; m_s_ml_global_cc = m_ml_global_cc; }
    inline void restore_ml_unc() { m_ml_unc = m_s_ml_unc; m_ml_global_cc = m_s_ml_global_cc; }

    std::string m_name;
    Type m_type;
    ExtID m_ext_index;
    double m_initial_value, m_min_value, m_max_value;
    double m_step_length;
    // value during minimization
    double* m_work_value;
    // result
    double m_ml_value, m_ml_unc, m_ml_unc_up, m_ml_unc_down, m_ml_global_cc, m_ml_unc_parab;
    // internal
    int m_internal_index;
    // Saved values
    double m_s_p, m_s_p_save, m_s_dirin, m_s_grad, m_s_deriv2, m_s_step;
    double m_s_ml_unc, m_s_ml_global_cc;
    bool m_limited;
    bool m_fixed;
  };

  class ParameterArray {
  public:
    ParameterArray(size_t npars, const std::string& name, const std::vector<Parameter*>& pars);

    ParameterArray(ParameterArray&&);

    ParameterArray();

    inline Parameter& operator[](size_t idx) {
      assert(idx < m_npars && "Invalid index");
      return *this->m_params[idx];
    }

    ParameterArray& operator=(ParameterArray&&);

    const double* values();

  protected:
    size_t m_npars;
    std::string m_name;
    std::vector<Parameter*> m_params;
    std::vector<double> m_vals;
  };

  inline std::ostream& operator<<(std::ostream& out, const Parameter& par) {
    return par.print(out);
  }

  class Parameters {
  public:
    friend class Minuit;

    Parameters();
    Parameters(const Parameters&) = delete;
    Parameters(Parameters&&);

    inline size_t size() const { return this->m_parameters.size(); }

    inline bool has(const std::string& name) const {
      return m_parameters.find(name) != m_parameters.end();
    }

    Parameter& operator[](const std::string& n);

    inline Parameter& operator[](unsigned n) {
      assert(n < m_parameters.size() && "Invalid index");
      return *m_parameters_vec[n];
    }

    inline const Parameter& operator[](unsigned n) const {
      assert(n < m_parameters.size() && "Invalid index");
      return *m_parameters_vec[n];
    }

    void set_working_values(double* par);

    /** Set all working value pointers to 0 */
    void clear_working_values();

    ParameterArray par_array(const std::string& name, size_t npars);

    void fix(Parameter& p);

    inline size_t nfixed() const { return m_fixed_parameters.size(); }

    Parameter* release();

    bool release(Parameter& p);

    inline std::vector<Parameter*>::iterator begin() { return m_parameters_vec.begin(); }
    inline std::vector<Parameter*>::iterator end() { return m_parameters_vec.end(); }

    inline std::vector<Parameter*>::const_iterator begin() const { return m_parameters_vec.begin(); }
    inline std::vector<Parameter*>::const_iterator end() const { return m_parameters_vec.end(); }

    /** Check for fixed parameters, put them in m_fixed_parameters */
    bool lock();

    inline bool is_locked() const { return m_locked; }

    std::ostream& print(std::ostream& out) const;

    /** @return If at least one of the parameters is close to one of its limits */
    bool has_param_at_limit() const;

    Parameters& operator=(Parameters&& o);
  protected:
    std::map<std::string, Parameter> m_parameters;
    std::vector<Parameter*> m_parameters_vec;
    std::vector<Parameter*> m_fixed_parameters;
    Parameter m_dummy_param;
    bool m_locked;
  };

  inline std::ostream& operator<<(std::ostream& out, const Parameters& pars) {
    return pars.print(out);
  }
}

#endif // sfi_Parameters_h
