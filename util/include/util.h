#ifndef sfi_util_h
#define sfi_util_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <type_traits>
#include <string>
#include <iostream>
#include <vector>
#include <assert.h>

#include "defs.h"

namespace sfi {
  /** Basic TMP */
  template<class _T> struct tvalued { using tvalue = _T; };
  template<class _T> using tvalue = typename _T::tvalue;

  /** TMP bool */
  template<bool _bval>
  struct tbool {
    static inline constexpr bool value() { return _bval; }
  };

  using ttrue = tbool<true>;
  using tfalse = tbool<false>;

  /** TMP uint and enum */
  template<unsigned _v>
  struct tuint {
    static constexpr unsigned value() { return _v; }
  };

  template<typename _Et, _Et val>
  struct tenum {
    static constexpr _Et value() { return val; }
  };

  /** Type equality */
  template<typename _T1, typename _T2> struct teq_ : tfalse { };
  template<typename _T1> struct teq_<_T1, _T1> : ttrue { };
  template<typename _T1, typename _T2> using teq = teq_<_T1, _T2>;
  template<typename _T1, typename _T2> using tneq = tbool<!teq_<_T1, _T2>::value()>;

  /** Logic */
  template<typename _T1, typename _T2> using tand = tbool<_T1::value() && _T2::value()>;
  template<typename _T1, typename _T2> using tor = tbool<_T1::value() || _T2::value()>;
  template<typename _T1> using tnot = tbool<!_T1::value()>;

  /** Conditional type */
  template<bool c, typename T1, typename T2> struct tcond_ : tvalued<T1> { };
  template<typename T1, typename T2> struct tcond_<false, T1, T2> : tvalued<T2> { };
  template<bool c, typename T1, typename T2> using tcond = tvalue<tcond_<c, T1, T2>>;

  /** Basic SFINAE */
  template<bool c, typename T = void> struct teif_ { };
  template<typename T> struct teif_<true, T> : tvalued<T> { };
  template<bool c, typename T = void> using teif = tvalue<teif_<c, T>>;

  template<class _T> struct t_void : tvalued<void> {};

  // static check for derived class
  template<class _Base, class _T>
  inline static constexpr bool c_is_base_of() { return std::is_base_of<_Base, _T>::value; }

  template<class _T>
  struct tref_ : tvalued<_T&> { };

  using Ftype = double(double*, double*);

  template<class _T>
  struct tis_vector : tfalse {};

  template<class _T>
  struct tis_vector<std::vector<_T>> : ttrue {};

  template<typename ..._T> inline void unused_parameters(_T&& ...) {}

  template<class _T>
  using t_raw = typename std::remove_cv<typename std::remove_reference<typename std::remove_pointer<_T>::type>::type>::type;

  template<class _T, class ... _Args>
  using t_value = typename _T::template tvalue<_Args...>;

  template<class _T> using t_result = typename std::result_of<t_raw<_T>>::type;
  template<size_t i, class _T> using t_tuple_element = typename std::tuple_element<i, _T>::type;

  // get the i:th element of a param pack
  template<size_t n, size_t i, class _A0, class ... _Args>
  struct _t_get { using tvalue = sfi::tvalue<tcond<n==i, tvalued<_A0>, _t_get<n+1,i,_Args...>>>; };
  template<size_t i, class _A0> struct _t_get<i,i,_A0> : tvalued<_A0> { };

  template<size_t i, class ... _Args>
  struct _t_get_impl : _t_get<0, i,_Args...>{
    static_assert((i < sizeof...(_Args)), "Index out of bounds");
  };
  template<size_t i, class ... _Args> using t_get = tvalue<_t_get_impl<i,_Args...>>;


  template<class _Pair> using t_pair_first = typename _Pair::first_type;
  template<class _Pair> using t_pair_second = typename _Pair::second_type;


  template<class _Arg, class _Ret>
  struct _t_func1 : tvalued<std::function<_Ret(_Arg)>> { };
  template<class _Ret>
  struct _t_func1<void,_Ret> : tvalued<std::function<_Ret()>> { };

  template<class _Arg, class _Ret>
  using t_func1 = tvalue<_t_func1<_Arg, _Ret>>;

  template<class _Arg1, class _Arg2, class _Ret>
  struct _t_func2 : tvalued<std::function<_Ret(_Arg1, _Arg2)>> { };
  template<class _Arg1, class _Ret>
  struct _t_func2<_Arg1,void,_Ret> : tvalued<std::function<_Ret(_Arg1)>> { };

  template<class _Arg1, class _Arg2, class _Ret>
  using t_func2 = tvalue<_t_func2<_Arg1, _Arg2, _Ret>>;

  // deferred construction
  template<unsigned ... _Idx> struct t_indices {};

  template<class _A, class _B> struct _t_cat_indices {};

  template<unsigned ... _IA, unsigned ... _IB>
  struct _t_cat_indices<t_indices<_IA...>, t_indices<_IB...>> : tvalued<t_indices<_IA..., _IB...>> { };

  template<class _T0, class _T1> struct _t_make_indices {};

  template<unsigned _I0, unsigned _Size>
  struct _t_make_indices<tuint<_I0>, tuint<_Size>> : _t_cat_indices<t_indices<_I0>, tvalue<_t_make_indices<tuint<_I0+1>, tuint<_Size>>>> { };

  template<unsigned _Size>
  struct _t_make_indices<tuint<(_Size-1)>, tuint<_Size>> : tvalued<t_indices<(_Size-1)>> { };

  template<unsigned _Size>
  using t_make_indices = tvalue<_t_make_indices<tuint<0>, tuint<_Size>>>;

  template<class _Tpl, class ..._Arg>
  struct _t_wrapper : tvalued<_Tpl> {
    _t_wrapper(_Arg...arg):args(std::forward<_Arg>(arg)...) {}

    template<class ... _Arg2>
    _t_wrapper<_Tpl, _Arg2..., _Arg...> prepend(_Arg2...arg2) {
      return prepend_impl<_Arg2...>(t_make_indices<sizeof...(_Arg)>(), std::forward<_Arg2>(arg2)...);
    }

    template<class ... _Arg2, unsigned ..._Idx>
    _t_wrapper<_Tpl, _Arg2..., _Arg...> prepend_impl(t_indices<_Idx...>&&, _Arg2...arg2) {
      return _t_wrapper<_Tpl, _Arg2..., _Arg...>(std::forward<_Arg2>(arg2)..., std::forward<_Arg>(std::get<_Idx>(this->args))...);
    }

    std::tuple<_Arg...> args;
  };

  template<class _T>
  struct t_wrapped {
    t_wrapped(const _T& val):value(val) {}
    t_wrapped(_T&& val):value(std::move(val)){}

    template<class ... _Args>
    t_wrapped(_t_wrapper<_T, _Args...>&& wr):
    t_wrapped(std::move(wr), t_make_indices<sizeof...(_Args)>()) {}

    template<class ..._Args, unsigned ... _Idx>
    t_wrapped(_t_wrapper<_T,_Args...>&& wr, t_indices<_Idx...>&&):
    value(std::forward<_Args>(std::get<_Idx>(wr.args))...) { }

    inline _T* operator->() { return &value; }
    inline const _T* operator->() const { return &value; }
    _T value;
  };

  template<class _T> struct _t_wrapper_type { };
  template<class _T, class ... _Args> struct _t_wrapper_type<_t_wrapper<_T,_Args...>> : tvalued<_T> { };

  template<class _T>  using t_wrapper_type = tvalue<_t_wrapper_type<_T>>;

  template<class _T>
  struct t_construct {
    template<class _Tpl, class ..._Arg, unsigned ... _Idx>
    inline static _T apply(_t_wrapper<_Tpl,_Arg...>&& wr, t_indices<_Idx...>&&) {
      return _T(std::forward<_Arg>(std::get<_Idx>(wr.args))...);
    }

    template<class _Tpl, class ..._Arg>
    inline static _T apply(_t_wrapper<_Tpl,_Arg...>&& wr) {
      return apply(std::move(wr), t_make_indices<sizeof...(_Arg)>());
    }
  };

  template<class _T, typename ... _Args>
  inline _T _t_wrapper_construct(_t_wrapper<_T, _Args...>&& wr) {
    return t_construct<_T>::apply(std::move(wr));
  }

  // array/vector printing
  template<class _T, size_t N>
  inline std::ostream& operator<<(std::ostream& out, const std::array<_T, N>& a) {
    for(unsigned i=0; i<N; ++i) {
      if(i) out<<", ";
      out<<a[i];
    }
    return out;
  }

  template<class _T>
  inline std::ostream& operator<<(std::ostream& out, const std::vector<_T>& vec) {
    const size_t N(vec.size());
    for(unsigned i=0; i<N; ++i) {
      if(i) out<<", ";
      out<<vec[i];
    }
    return out;
  }

  /** Generic result */
  struct result {
    result(bool st):status(st) {}
    inline operator bool() const { return this->status; }
    bool status;
  };

  struct invert_result : result{
    invert_result(bool s):result(s),nvalues(0),det(0.0),condnr(0.0), orig_det(0.0), orig_condnr(0.0) {}
    unsigned nvalues;
    double det;
    double condnr;
    double orig_det;
    double orig_condnr;
  };

  template<class _T>
  void print_array(const _T* a, size_t len) {
    for(unsigned i=0; i<len; ++i) {
      if(i) std::cout<<", ";
      std::cout<<a[i];
    }
  }

  template<class _Tpl, class ... _Args>
  using tapply = typename _Tpl::template apply<_Args...>;
}

namespace std {
  template<class _TC, class _TT, class _T>
  inline basic_ostream<_TC, _TT>& operator<<(basic_ostream<_TC, _TT>& out, const pair<_T, _T>& p) {
    return out<<"{"<<p.first<<", "<<p.second<<"}";
  }
}
#endif // sfi_util_h
