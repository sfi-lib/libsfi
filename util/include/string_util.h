#ifndef sfi_string_util_h
#define sfi_string_util_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string>
#include <sstream>
#include <vector>

namespace sfi {

  bool ends_with(const std::string& str, const std::string& suff);

  bool starts_with(const std::string& str, const std::string& match);

  std::string trim_string(const std::string& str);

  std::vector<std::string> split_string(const std::string& str, char delim);

  std::vector<std::string> split_quoted_string(const std::string& str);

  template<class _T>
  struct from_string {
    static inline bool apply(const std::string& str, _T& value) {
      std::istringstream sin(str);
      sin>>value;
      return !sin.fail();
    }
  };

  template<>
  struct from_string<bool> {
    static bool apply(const std::string& str, bool& value);
  };

  template<>
  struct from_string<std::string> {
    static bool apply(const std::string& str, std::string& value);
  };

  template<class _T>
  struct from_string<std::vector<_T>> {
    static inline bool apply(const std::string& str, std::vector<_T>& value) {
      std::vector<std::string> parts = split_quoted_string(str);
      const size_t N(parts.size());
      value.resize(N);
      for(size_t i=0; i<N; ++i) {
        std::istringstream sin(parts[i]);
        sin>>value[i];
      }
      return true;
    }
  };

  template<>
  struct from_string<std::vector<bool>> {
    static bool apply(const std::string& str, std::vector<bool>& value);
  };

  bool try_parse_uint(const std::string& str, unsigned& value);

  bool try_parse_int(const std::string& str, int& value);

  bool try_parse_double(const std::string& str, double& value);
}

#endif // sfi_string_util_h
