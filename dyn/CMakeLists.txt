file(GLOB DYN_LIBSOURCES "src/*.cxx")
add_library(SFIDyn SHARED ${DYN_LIBSOURCES})
target_include_directories(SFIDyn PUBLIC $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include> $<INSTALL_INTERFACE:include/dyn/>)
target_link_libraries(SFIDyn SFIAna SFIBases)

file(GLOB TESTS "tests/*.cxx")
IF(NOT "${TESTS}" STREQUAL "")
  add_library(SFIDynTests SHARED ${TESTS})
  target_link_libraries(SFIDynTests SFIDyn)
  install(TARGETS SFIDyn SFIDynTests EXPORT SFITargets LIBRARY DESTINATION lib PUBLIC_HEADER DESTINATION include)
ELSE()
  install(TARGETS SFIDyn EXPORT SFITargets LIBRARY DESTINATION lib PUBLIC_HEADER DESTINATION include)
ENDIF()

install(DIRECTORY include/ DESTINATION include/ana FILES_MATCHING PATTERN "include/*.h")
