/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "DTransformCplx.h"
#include "full_system.h"
#include "sparse_system.h"
#include "cosine.h"
#include "legendre.h"
#include "chebyshev.h"
#include "fourier.h"
#include "Ylm.h"

using namespace sfi;

DTransformCplx::DTransformCplx(const OptionMap& opts):Base(opts) {
  if(!m_complex) throw DTransformException("Non complex transforms not allowed");
}

DTransformCplx::DTransformCplx(const DTransformCplx& o):Base(o) { }

DTransformCplx::DTransformCplx(DTransformCplx&& o):Base(o) { }

DTransformCplx::~DTransformCplx() { }

DTransformCplx& DTransformCplx::operator=(const DTransformCplx& o) {
  if(this != &o) assign_from(o);
  return *this;
}

// ---- implementation of ITransform ----

const std::string& DTransformCplx::get_type_name() const {
  static std::string _type_name = "DTransformCplx";
  return _type_name;
}

Qty DTransformCplx::get_param(unsigned i) const {
  return (i < m_nparams) ? this->param_qty(i) : Qty(0,0);
}

void DTransformCplx::set_param(unsigned i, double val) {
  if(i < m_nparams) par(i) = val;
}

std::unique_ptr<IParamIterator> DTransformCplx::get_param_iterator() const {
  return std::make_unique<ParamIterator<const DTransformCplx>>(*this);
}

std::unique_ptr<IEvaluator> DTransformCplx::get_evaluator(double scale, bool external_coordinates) const {
  return std::make_unique<Evaluator<DTransformCplx>>(this,scale,external_coordinates);
}

std::unique_ptr<IEvaluator> DTransformCplx::get_marginalize_evaluator_1D(unsigned /*axis*/, double /*scale*/, bool /*external_coordinates*/) const {
  return nullptr;
}

std::unique_ptr<IEvaluator> DTransformCplx::get_marginalize_evaluator_2D(unsigned /*a1*/, unsigned /*a2*/, double /*scale*/, bool /*external_coordinates*/) const {
  return nullptr;
}

std::unique_ptr<IEvaluator> DTransformCplx::get_slice_evaluator(unsigned /*d2*/, bool /*_do_normalize*/, double /*scale*/, bool /*external_coordinates*/) const {
  return nullptr;
}

std::unique_ptr<IEvaluator> DTransformCplx::get_slice_derivative_evaluator(unsigned /*d2*/, bool /*_do_normalize*/, double /*scale*/, bool /*external_coordinates*/) const {
  return nullptr;
}

double DTransformCplx::get_value(const double* x) const {
  EvalData ed;
  init_eval(ed);
  return eval(x,ed,false);
}

double DTransformCplx::get_integral() const { return integral_pdf(); }

ITransform* DTransformCplx::get_axis_transform(unsigned d) const { return axis_transform(d); }

ITransform* DTransformCplx::get_clone() const { return new DTransformCplx(*this); }

void DTransformCplx::do_dump(bool unc) const {
  std::cout<<"DTransformCplx complex: "<<m_complex<<" ";
  print(std::cout);
  std::cout<<std::endl;
  Base::dump_impl(unc, this->get_dimensions());
}

void DTransformCplx::do_normalize(double norm) { normalize(norm); }

bool DTransformCplx::do_eval(unsigned npts, const double* xv, double* res, double scale, bool external_coordinates) const {
  EvalData ed;
  init_eval(ed);
  unsigned dims = get_dimensions();
  const double* x = xv;
  for(unsigned i=0; i<npts; ++i) {
    res[i] = scale*eval(x, ed, external_coordinates);
    x += dims;
  }
  return true;
}

bool DTransformCplx::do_full_eval(const Matrix<double>& /*points*/, Matrix<double>& /*res*/, bool /*external_coordinates*/) const {
  return false;
}

bool DTransformCplx::do_write(OptionMap& om) const { write(om); return true; }

const MatrixSym<double>* DTransformCplx::do_create_covariance(bool /*poisson*/) { return nullptr; }

const Vector<double>* DTransformCplx::get_parameters() const { return nullptr; }

const MatrixSym<double>* DTransformCplx::get_covariance() const { return &m_covariance; }

bool DTransformCplx::do_copy(const ITransform* o) {
  auto* dt = dynamic_cast<const DTransformCplx*>(o);
  if(dt) {
    copy_from(*dt);
    return true;
  }
  else return false;
}

// ---- public methods ----

void DTransformCplx::full_project(const double* x, EvalData& p) const { compute_all(Basis::Project, x, p); }

template<class _Type, class _TResType> inline void basis_compute(Basis::CompType ctype, double x, unsigned k, _TResType* res) {
  switch(ctype) {
    case Basis::Eval: {_Type::template compute<double,_TResType,Basis::Eval>(x,k,res); break; }
    case Basis::Project: {_Type::template compute<double,_TResType,Basis::Project>(x,k,res); break; }
    case Basis::EvalDeriv: {_Type::template compute<double,_TResType,Basis::EvalDeriv>(x,k,res); break; }
    case Basis::EvalDeriv2: {_Type::template compute<double,_TResType,Basis::EvalDeriv2>(x,k,res); break; }
  }
}

inline void ylm_compute(Basis::CompType ctype, double x, double y, unsigned k, std::complex<double>* res) {
  switch(ctype) {
    case Basis::Eval: {bases::Ylm::template compute<double,Basis::Eval>(x,y,k,res); break; }
    case Basis::Project: {bases::Ylm::template compute<double,Basis::Project>(x,y,k,res); break; }
    case Basis::EvalDeriv: {bases::Ylm::template compute<double,Basis::EvalDeriv>(x,y,k,res); break; }
    case Basis::EvalDeriv2: {bases::Ylm::template compute<double,Basis::EvalDeriv2>(x,y,k,res); break; }
  }
}

void DTransformCplx::compute_all(Basis::CompType ctype, const double* _x, EvalData& p) const {
  for(unsigned d=0; d<m_dims; ++d) {
    switch(p.iter.types[d]) {
      case dyn::EigSysBasis::Cos: { basis_compute<bases::Cosine, std::complex<double>>(ctype, _x[d], p.iter.degrees[d], p.data[d].data()); break; }
      case dyn::EigSysBasis::Chebyshev: { basis_compute<bases::Chebyshev, std::complex<double>>(ctype, _x[d], p.iter.degrees[d], p.data[d].data()); break; }
      case dyn::EigSysBasis::Legendre: { basis_compute<bases::Legendre, std::complex<double>>(ctype, _x[d], p.iter.degrees[d], p.data[d].data()); break; }
      case dyn::EigSysBasis::Fourier: { basis_compute<bases::Fourier, std::complex<double>>(ctype, _x[d], p.iter.degrees[d], reinterpret_cast<std::complex<double>*>(p.data[d].data())); break; }
      case dyn::EigSysBasis::Ylm: {
        ylm_compute(ctype, _x[d], _x[d+1], p.iter.degrees[d], p.data[d].data());
        ++d;
        break;
      }
    }
  }
}

struct _s_eval_cplx {
  _s_eval_cplx(const std::complex<double>* ptr):m_ptr(ptr), m_res(0.0)  {}
  inline void operator()(const std::complex<double>& v) {
    m_res += v.real()*m_ptr->real() - v.imag()*m_ptr->imag();
    ++m_ptr;
  }
  const std::complex<double>* m_ptr;
  double m_res;
};

double DTransformCplx::eval(const double* x, EvalData& p, bool external_coordinates) const {
  if(external_coordinates) {
    std::vector<double> xi(this->dims(), 0.0);
    double w = this->m_variables->transform(x, xi.data());
    auto f = compute<Basis::Eval>(xi.data(), p, _s_eval_cplx(reinterpret_cast<const std::complex<double>*>(m_par.data_ptr())));
    return w*to_pdf(f.m_res);
  } else {
    auto f = compute(Basis::Eval, x, p, _s_eval_cplx(reinterpret_cast<const std::complex<double>*>(m_par.data_ptr())));
    return to_pdf(f.m_res);
  }
}

struct _s_full_eval {
  _s_full_eval(std::complex<double>* res):m_res(res) {}
  inline void operator()(const std::complex<double>& v) {
    *m_res = v;
    ++m_res;
  }
  std::complex<double>* m_res;
};

bool DTransformCplx::full_eval(const Matrix<double>& points, Matrix<Coeff>& res, bool external_coordinates) const {
  if(points.cols() != dims()) return false;
  const size_t npts = points.rows();
  if(npts != res.rows()) return false;
  if(res.cols() != this->npar()) return false;
  EvalData p;
  init_eval(p);

  if(external_coordinates) {
    std::vector<double> xo(dims(), 0.0);
    for(unsigned i=0; i<npts; ++i) {
      this->variables()->transform(points.row_data(i), xo.data());
      this->compute<Basis::Eval>(xo.data(), p, _s_full_eval(res.row_data(i)));
    }
  } else {
    for(unsigned i=0; i<npts; ++i) {
      this->compute<Basis::Eval>(points.row_data(i), p, _s_full_eval(res.row_data(i)));
    }
  }
  return true;
}

TransformVariableBase* DTransformCplx::axis_transform(unsigned /*axis*/) const {
  return nullptr;
}

void DTransformCplx::print(std::ostream& out) const {
  m_eigensys->do_print(out);
}

// ---- public types ----


// ---- protected methods ----

// ---- protected types ----


// ---- Evaluator ----

Evaluator<DTransformCplx>::Evaluator(const DTransformCplx* trf, double scale, bool external_coordinates):
  m_trf(trf), m_scale(scale), m_external_coordinates(external_coordinates) {
  m_trf->init_eval(m_ed);
}

Evaluator<DTransformCplx>::Evaluator(Evaluator<DTransformCplx>&& o):
  m_ed(std::move(o.m_ed)),m_trf(o.m_trf),m_scale(o.m_scale),m_external_coordinates(o.m_external_coordinates) {
}

Evaluator<DTransformCplx>::Evaluator(const This& o):
  m_trf(o.m_trf),m_scale(o.m_scale),m_external_coordinates(o.m_external_coordinates) {
  m_trf->init_eval(m_ed);
}

Evaluator<DTransformCplx>::~Evaluator() {}

double Evaluator<DTransformCplx>::get_value(const double* x) {
  return m_scale*m_trf->eval(x, m_ed, m_external_coordinates);
}

const ITransform* Evaluator<DTransformCplx>::get_transform() { return m_trf; }

void Evaluator<DTransformCplx>::set_scale(double scale) { m_scale = scale; }

void Evaluator<DTransformCplx>::set_external_coordinates(bool ec) { m_external_coordinates = ec; }

unsigned Evaluator<DTransformCplx>::get_dimensions() const { return m_trf->get_dimensions(); }

bool Evaluator<DTransformCplx>::do_eval(unsigned npts, const double* xv, double* res) {
  return m_trf->do_eval(npts, xv, res, m_scale, m_external_coordinates);
}

const Variable& Evaluator<DTransformCplx>::get_variable(unsigned d) const {
  auto vars = m_trf->get_variables();
  return vars->at(d);
}

IEvaluator* Evaluator<DTransformCplx>::get_clone(bool deep) const {
  return new Evaluator<DTransformCplx>(deep ? m_trf->clone() : m_trf, m_scale, m_external_coordinates);
}

unsigned Evaluator<DTransformCplx>::get_nslice_parameters() const { return 0; }

void Evaluator<DTransformCplx>::set_slice(const double *) {}

const ITransform* Evaluator<DTransformCplx>::get_slice_transform() { return nullptr; }

double Evaluator<DTransformCplx>::operator()(const double* x) {
  return m_scale*m_trf->eval(x, m_ed, m_external_coordinates);
}

// ---- ParamIterator ----

ParamIterator<const DTransformCplx>::ParamIterator(const DTransformCplx& t):m_iter(t.iterator()), m_trf(t) {}

ParamIterator<const DTransformCplx>::~ParamIterator() {}

bool ParamIterator<const DTransformCplx>::do_next() { return m_iter.next(); }

unsigned ParamIterator<const DTransformCplx>::get_degree(unsigned d) { return m_iter.degrees[d]; }

Qty ParamIterator<const DTransformCplx>::get_value() { return Qty(std::real(m_trf[m_iter.par_idx()]),0); }
