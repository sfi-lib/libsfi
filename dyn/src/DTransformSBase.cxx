/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <DTransformSBase.h>

using namespace sfi;
using namespace sfi::dyn;

// ---- IEigSysIter ----

IEigSysIter::IEigSysIter(unsigned _dim, unsigned _deg):dims(_dim),deg(_deg) {}

// ---- EigSysIterator ----

EigSysIterator::EigSysIterator():index(0), nparams(0) { }

EigSysIterator::EigSysIterator(EigSysIterator&& o):index(o.index), nparams(o.nparams),
  indices(std::move(o.indices)), iters(std::move(o.iters)), types(std::move(o.types)), degrees(std::move(o.degrees)) {
}

EigSysIterator::EigSysIterator(unsigned _dims, unsigned _nparams):index(0), nparams(_nparams), indices(_dims, 0) {}

EigSysIterator::~EigSysIterator() {
  for(auto* it : iters) delete it;
}

EigSysIterator& EigSysIterator::operator=(EigSysIterator&& o) {
  index = o.index;
  nparams = o.nparams;
  indices = std::move(o.indices);
  iters = std::move(o.iters);
  types = std::move(o.types);
  degrees = std::move(o.degrees);
  return *this;
}

bool EigSysIterator::next() {
  ++index;
  unsigned i = 0;
  uint16_t* idxptr = indices.data();
  while((i < iters.size()) && !iters[i]->do_next(idxptr)) {
    iters[i]->do_clear(idxptr);
    idxptr += iters[i]->dims;
    ++i;
  }
  return index < nparams;
}

void EigSysIterator::clear() {
  for(auto& i : indices) i = 0;
  index = 0;
}

// ---- FullEigSysIter ----

FullEigSysIter::FullEigSysIter(unsigned _dim, unsigned _deg):IEigSysIter(_dim,_deg) {}

bool FullEigSysIter::do_next(uint16_t* indices) {
  unsigned d=0;
  while((d < dims) && ((++indices[d]) > deg)) {
    indices[d] = 0;
    ++d;
  }
  return d < dims;
}

void FullEigSysIter::do_clear(uint16_t* indices) {
  for(unsigned d=0; d<dims; ++d) indices[d] = 0;
}

// ---- SparseEigSysIter ----

SparseEigSysIter::SparseEigSysIter(unsigned _dim, unsigned _deg):IEigSysIter(_dim,_deg), sum(0) {}

bool SparseEigSysIter::do_next(uint16_t* indices) {
  unsigned d=0;
  while((d < dims) && (((++sum,++indices[d]) > deg) || (sum > deg))) {
    sum -= indices[d];
    indices[d] = 0;
    ++d;
  }
  return d < dims;
}

void SparseEigSysIter::do_clear(uint16_t* indices) {
  for(unsigned d=0; d<dims; ++d) indices[d] = 0;
  this->sum = 0;
}

// ---- YlmEigSysIter ----

YlmEigSysIter::YlmEigSysIter(unsigned _dim, unsigned _deg):IEigSysIter(_dim,_deg) {}

bool YlmEigSysIter::do_next(uint16_t* indices) {
  unsigned i=0;
  while((i < dims) && (++indices[i+1] > indices[i])) {
    indices[i+1] = 0;
    if(++indices[i] <= deg) break;
    indices[i] = 0;
    i += 2;
  }
  return i < dims;
}

void YlmEigSysIter::do_clear(uint16_t* indices) {
  for(unsigned d=0; d<dims; ++d) indices[d] = 0;
}

// ---- IEigSys ----

IEigSys::IEigSys(unsigned _dims):dims(_dims) { }
IEigSys::~IEigSys() {}

// ---- EigSys ----

EigSys::~EigSys() {}

EigSys::EigSys(unsigned _dims, unsigned _deg, EigSysBasis _basis, EigSysType _type):
  IEigSys(_dims), deg(_deg), basis(_basis), type(_type) { }

IEigSys* EigSys::do_clone() const { return new EigSys{dims, deg, basis, type}; }

void EigSys::get_iterator(EigSysIterator& it) const {
  for(unsigned d=0; d<this->dims; ++d) {
    it.types.push_back(this->basis);
    it.degrees.push_back(this->deg);
  }
  if(type == EigSysType::FullTensor) it.iters.push_back(new FullEigSysIter(dims, deg));
  else it.iters.push_back(new SparseEigSysIter(dims, deg));
}

void EigSys::do_print(std::ostream& out) const {
  out<<"("<<eig_sys_basis_name(this->basis)<<", "<<eig_sys_type_name(this->type)<<", "<<this->dims<<", "<<this->deg<<")";
}

unsigned EigSys::nparams() const {
  if(type == EigSysType::FullTensor) return powi(deg + 1, dims);
  else return n_over_kc(deg + dims, dims);
}

const std::string& EigSys::basis_name() const {
  static const std::string _cos_basis_name = "cos";
  static const std::string _chebyshev_basis_name = "chebyshev";
  static const std::string _legendre_basis_name = "legendre";
  static const std::string _fourier_basis_name = "fourier";
  static const std::string _ylm_basis_name = "ylm";
  switch(this->basis) {
    case EigSysBasis::Legendre: return _legendre_basis_name;
    case EigSysBasis::Chebyshev: return _chebyshev_basis_name;
    case EigSysBasis::Cos: return _cos_basis_name;
    case EigSysBasis::Fourier: return _fourier_basis_name;
    case EigSysBasis::Ylm: return _ylm_basis_name;
  }
}

const std::string& EigSys::eig_sys_basis_name(EigSysBasis b) {
  static const std::string basis_cos_name = "cos_on";
  static const std::string basis_chebyshev_name = "chebyshev_on";
  static const std::string basis_legendre_name = "legendre_on";
  static const std::string basis_fourier_name = "fourier_on";
  static const std::string basis_ylm_name = "ylm_on";
  switch(b) {
    case EigSysBasis::Cos : return basis_cos_name;
    case EigSysBasis::Chebyshev : return basis_chebyshev_name;
    case EigSysBasis::Legendre : return basis_legendre_name;
    case EigSysBasis::Fourier: return basis_fourier_name;
    case EigSysBasis::Ylm: return basis_ylm_name;
  }
}

const std::string& EigSys::eig_sys_type_name(EigSysType t) {
  static const std::string type_full_name = "full";
  static const std::string type_sparse_name = "sparse";
  static const std::string type_composite_name = "";
  switch(t) {
    case EigSysType::FullTensor: return type_full_name;
    case EigSysType::MonomicSparse: return type_sparse_name;
    case EigSysType::Composite: return type_composite_name;
  }
}

// ---- YlmEigSys ----

YlmEigSys::YlmEigSys(unsigned _dims, unsigned _deg, EigSysType _type):
  EigSys(_dims, _deg, EigSysBasis::Ylm, _type) {
  if(_dims & 0x01) throw DTransformException("Ylm dimensions must be even");
}

YlmEigSys::~YlmEigSys() {}

IEigSys* YlmEigSys::do_clone() const { return new YlmEigSys{dims, deg, type}; }

void YlmEigSys::get_iterator(EigSysIterator& it) const {
  for(unsigned d=0; d<this->dims; ++d) {
    it.types.push_back(EigSysBasis::Ylm);
    it.degrees.push_back(this->deg);
  }
  it.iters.push_back(new YlmEigSysIter(dims, deg));
}

void YlmEigSys::do_print(std::ostream& out) const {
  out<<"(Ylm, "<<eig_sys_type_name(this->type)<<", "<<this->dims<<", "<<this->deg<<")";
}

unsigned YlmEigSys::nparams() const {
  return powi(bases::ylm_nparams(deg), dims/2);
}

const std::string& YlmEigSys::basis_name() const {
  static const std::string _ylm_basis_name = "ylm";
  return _ylm_basis_name;
}

// ---- EigSysComp ----

EigSysComp::~EigSysComp() {
  if(this->left) {
    delete this->left;
    this->left = nullptr;
  }
  if(this->right) {
    delete this->right;
    this->right = nullptr;
  }
}

EigSysComp::EigSysComp(IEigSys* _left, IEigSys* _right):IEigSys(_left->dims + _right->dims), left(_left), right(_right) {}

IEigSys* EigSysComp::do_clone() const { return new EigSysComp{this->left->do_clone(), this->right->do_clone()}; }

EigSysType EigSysComp::get_type() const { return EigSysType::Composite; }

void EigSysComp::get_iterator(EigSysIterator& it) const { left->get_iterator(it); right->get_iterator(it); }

void EigSysComp::do_print(std::ostream& out) const {
  out<<"{";
  left->do_print(out);
  out<<", ";
  right->do_print(out);
  out<<"}";
}

// ---- DTransformSBase - public methods ----

DTransformSBase::~DTransformSBase() {
  if(m_eigensys != nullptr) {
    delete m_eigensys;
    m_eigensys = nullptr;
  }
}

double DTransformSBase::norm(unsigned d, unsigned g) const {
  auto* es = eig_sys_at(d);
  switch(es->basis) {
    case EigSysBasis::Cos : return bases::Cosine::norm(g);
    case EigSysBasis::Chebyshev : return bases::Chebyshev::norm(g);
    case EigSysBasis::Legendre : return bases::Legendre::norm(g);
    case EigSysBasis::Fourier : return bases::Fourier::norm(g);
    case EigSysBasis::Ylm : return bases::Ylm::norm(g);
  }
}

unsigned DTransformSBase::var_degree(unsigned d) const {
  if(d >= m_dims) return 0;
  auto* es = eig_sys_at(d);
  return es ? es->deg : 0;
}

EigSysIterator DTransformSBase::iterator() const {
  EigSysIterator it(m_dims, m_nparams);
  m_eigensys->get_iterator(it);
  return it;
}

EigSysType DTransformSBase::eigen_sys_type(unsigned d) { return this->eig_sys_at(d)->type; }

EigSysBasis DTransformSBase::eigen_sys_basis(unsigned d) { return this->eig_sys_at(d)->basis; }

// ---- DTransformSBase - protected methods ----

DTransformSBase::DTransformSBase(dyn::IEigSys* es):m_eigensys(es) { }

DTransformSBase::DTransformSBase(const OptionMap& opts):
  m_eigensys(add_basis(opts.sub("eigensystem"), opts.sub("eigensystem.eigenv"))) { }

DTransformSBase::DTransformSBase(const DTransformSBase& o) { copy_from(o); }

DTransformSBase::DTransformSBase(DTransformSBase&& o) { move_from(std::move(o)); }

void DTransformSBase::assign_from(const DTransformSBase& o) {
  if(m_eigensys) delete m_eigensys;
  m_eigensys = 0;
  copy_from(o);
}

void DTransformSBase::copy_from(const DTransformSBase& o) {
  m_eigensys = o.m_eigensys->do_clone();
  m_dims = o.m_dims;
  m_nparams = o.m_nparams;
  m_complex = o.m_complex;
  init();
}

void DTransformSBase::move_from(DTransformSBase&& o) {
  m_eigensys = o.m_eigensys;
  o.m_eigensys = nullptr;
  m_dims = o.m_dims;
  m_nparams = o.m_nparams;
  m_complex = o.m_complex;
}

IEigSys* DTransformSBase::add_basis(const Eig& e) { return create_eig_sys(e.dims, e.deg, e.basis, e.type); }

IEigSys* DTransformSBase::add_basis(const OptionMap& eso, const OptionMap& evo) {
  if(eso.has_sub("left")) {
    return new EigSysComp{add_basis(eso.sub("left"),evo.sub("left")), add_basis(eso.sub("right"),evo.sub("right"))};
  }
  else {
    return create_eig_sys(evo.get_as<unsigned>("dims"), evo.get_as<unsigned>("degree"), eso.get<std::string>("basis"), evo.get<std::string>("type"));
  }
}

IEigSys* DTransformSBase::create_eig_sys(unsigned _dims, unsigned _deg, const std::string& _basis, const std::string& _type) {
  EigSysBasis basis = EigSysBasis::Cos;
  if((_basis == "cos_on") || (_basis == "cos")) basis = EigSysBasis::Cos;
  else if((_basis == "legendre_on") || (_basis == "legendre")) basis = EigSysBasis::Legendre;
  else if((_basis == "chebyshev_on") || (_basis == "chebyshev")) basis = EigSysBasis::Chebyshev;
  else if((_basis == "fourier_on") || (_basis == "fourier")) basis = EigSysBasis::Fourier;
  else if((_basis == "ylm_on") || (_basis == "ylm")) basis = EigSysBasis::Ylm;
  
  EigSysType type = EigSysType::FullTensor;
  if(_type == "full") type = EigSysType::FullTensor;
  else if(_type == "sparse") type = EigSysType::MonomicSparse;
  
  if(basis == EigSysBasis::Ylm) return new YlmEigSys(_dims, _deg, type);
  else return new EigSys(_dims, _deg, basis, type);
}

void DTransformSBase::init() {
  m_complex = false;
  m_nparams = init(m_eigensys);
  m_dims = m_eigensys->dims;
}

unsigned DTransformSBase::init(IEigSys* _es) {
  auto* ces = dynamic_cast<EigSysComp*>(_es);
  if(ces) return init(ces->left) * init(ces->right);
  else {
    auto* es = dynamic_cast<EigSys*>(_es);
    if((es->basis == EigSysBasis::Fourier) || (es->basis == EigSysBasis::Ylm)) {
      // This is how a complex eigensystem is detected
      m_complex = true;
    }
    return es->nparams();
  }
}

EigSys* DTransformSBase::eig_sys_at(unsigned d) const {
  auto *es = eig_sys_at(d,0,m_eigensys);
  if(!es) throw DTransformException("Invalid dimension "+std::to_string(d)+" dimensions "+std::to_string(m_dims));
  return es;
}

EigSys* DTransformSBase::eig_sys_at(unsigned d, unsigned d0, IEigSys* _es) const {
  auto* ces = dynamic_cast<EigSysComp*>(_es);
  if(ces) {
    auto* r = eig_sys_at(d,d0,ces->left);
    if(r) return r;
    else return eig_sys_at(d,d0 + ces->left->dims,ces->right);
  }
  else {
    auto* es = dynamic_cast<EigSys*>(_es);
    return (es && (d < (d0+es->dims))) ? es : nullptr;
  }
}

void DTransformSBase::write(OptionMap& eso, OptionMap& evo, IEigSys* es) {
  auto* ces = dynamic_cast<EigSysComp*>(es);
  if(ces) {
    write(eso.sub("left", true), evo.sub("left", true), ces->left);
    write(eso.sub("right", true), evo.sub("right", true), ces->right);
  }
  else {
    auto* _es = dynamic_cast<EigSys*>(es);
    eso.set("basis", EigSys::eig_sys_basis_name(_es->basis));
    eso.set<int>("dims", _es->dims);
    eso.set("is_dynamic", true);
    
    evo.set<int>("degree", _es->deg);
    evo.set<int>("dims", _es->dims);
    evo.set("type", EigSys::eig_sys_type_name(_es->type));
  }
}

