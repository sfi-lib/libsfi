#ifndef sfi_DTransformSBase_h
#define sfi_DTransformSBase_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <Ylm.h>

namespace sfi {
  namespace dyn {
    enum EigSysType { FullTensor, MonomicSparse, Composite };
    
    enum EigSysBasis { Cos, Legendre, Chebyshev, Fourier, Ylm };

    struct IEigSysIter {
      IEigSysIter(unsigned _dim, unsigned _deg);
      virtual ~IEigSysIter() {}
      virtual bool do_next(uint16_t*) = 0;
      virtual void do_clear(uint16_t*) = 0;
      unsigned dims;
      unsigned deg;
    };
    
    struct EigSysIterator {
      EigSysIterator();
      EigSysIterator(EigSysIterator&&);
      EigSysIterator(unsigned _dims, unsigned _nparams);
      ~EigSysIterator();
      inline uint16_t operator[](size_t i) { return this->indices[i]; }
      inline unsigned par_idx() const { return this->index; }
      EigSysIterator& operator=(EigSysIterator&&);
      bool next();
      void clear();
      unsigned index;
      unsigned nparams;
      // current degree per dim
      std::vector<uint16_t> indices;
      // underlying iterators
      std::vector<IEigSysIter*> iters;
      // basis type per dim
      std::vector<EigSysBasis> types;
      // max degree per dim
      std::vector<uint16_t> degrees;
    };
    
    struct FullEigSysIter : IEigSysIter {
      FullEigSysIter(unsigned _dim, unsigned _deg);
      virtual ~FullEigSysIter() {}
      virtual bool do_next(uint16_t*);
      virtual void do_clear(uint16_t*);
    };

    struct SparseEigSysIter : IEigSysIter {
      SparseEigSysIter(unsigned _dim, unsigned _deg);
      virtual ~SparseEigSysIter() {}
      virtual bool do_next(uint16_t*);
      virtual void do_clear(uint16_t*);
      unsigned sum;
    };
    
    struct YlmEigSysIter : IEigSysIter {
      YlmEigSysIter(unsigned _dim, unsigned _deg);
      virtual ~YlmEigSysIter() {}
      virtual bool do_next(uint16_t*);
      virtual void do_clear(uint16_t*);
    };
    
    class IEigSys {
    public:
      IEigSys(unsigned _dims);
      virtual ~IEigSys();
      virtual IEigSys* do_clone() const = 0;
      virtual EigSysType get_type() const = 0;
      virtual void get_iterator(EigSysIterator&) const = 0;
      virtual void do_print(std::ostream&) const = 0;
      unsigned dims;
    };
  
    class EigSys : public IEigSys {
    public:
      EigSys(unsigned _dims, unsigned _deg, EigSysBasis _basis, EigSysType _type);
      virtual ~EigSys();
      virtual IEigSys* do_clone() const;
      virtual EigSysType get_type() const { return this->type; }
      virtual void get_iterator(EigSysIterator&) const;
      virtual void do_print(std::ostream&) const;
      virtual unsigned nparams() const;
      virtual const std::string& basis_name() const;

      static const std::string& eig_sys_basis_name(dyn::EigSysBasis);

      static const std::string& eig_sys_type_name(dyn::EigSysType);

      unsigned deg;
      EigSysBasis basis;
      EigSysType type;
    };
  
    class YlmEigSys : public EigSys {
    public:
      YlmEigSys(unsigned _dims, unsigned _deg, EigSysType _type);
      virtual ~YlmEigSys();
      virtual IEigSys* do_clone() const;
      virtual EigSysType get_type() const { return this->type; }
      virtual void get_iterator(EigSysIterator&) const;
      virtual void do_print(std::ostream&) const;
      virtual unsigned nparams() const;
      virtual const std::string& basis_name() const;
    };
  
    class EigSysComp : public IEigSys {
    public:
      virtual ~EigSysComp();
      EigSysComp(IEigSys* _left, IEigSys* _right);
      virtual IEigSys* do_clone() const;
      virtual EigSysType get_type() const;
      virtual void get_iterator(EigSysIterator& it) const;
      virtual void do_print(std::ostream&) const;
      IEigSys* left;
      IEigSys* right;
    };
  }

  struct FullDynEiegenSystem {
    static constexpr bool is_dynamic() { return true; }
  };

  class DTransformException : public Exception {
  public:
    DTransformException(const std::string& msg):Exception(msg) {}
  };

  class DTransformSBase {
  public:
    virtual ~DTransformSBase();
    
    template<class _ELeft, class _ERight>
    struct EigComp {
      EigComp(const _ELeft& _l, const _ERight& _r):left(_l), right(_r) {}
      _ELeft left;
      _ERight right;
    };
    struct Eig {
      Eig(unsigned _dims, unsigned _deg, const std::string& _basis, const std::string& _type):
        dims(_dims), deg(_deg), basis(_basis), type(_type) {
      }
      unsigned dims;
      unsigned deg;
      std::string basis;
      std::string type;
    };
    
    // ---- exported Transform interface ----
    
    inline unsigned dims() const { return m_dims; }
    
    inline unsigned npar() const { return m_nparams; }

    double norm(unsigned d, unsigned g) const;
    
    unsigned var_degree(unsigned d) const;
    
    /** @return Iterator oveer all eigenvectors */
    dyn::EigSysIterator iterator() const;
    
    struct _t_iterator {
      dyn::EigSysIterator iter;
      inline _t_iterator& operator++() { iter.next(); return *this; }
      inline bool operator!=(const _t_iterator& it) { return iter.index != it.iter.nparams; }
      inline dyn::EigSysIterator& operator*() { return iter; }
    };
    inline _t_iterator begin() const { return _t_iterator{iterator()}; }
    inline _t_iterator end() const { return _t_iterator{iterator()}; }
    
    /** @return Eigen vector type for given dimension */
    dyn::EigSysType eigen_sys_type(unsigned d);
    
    /** @return Basis for given dimension */
    dyn::EigSysBasis eigen_sys_basis(unsigned d);

    // ---- exported complex methods ----
    
    inline bool is_complex() const { return m_complex; }
  protected:
    template<class _Eig>
    DTransformSBase(const std::string&, const _Eig& e):m_eigensys(add_basis(e)) { init(); }
    
    DTransformSBase(dyn::IEigSys*);

    DTransformSBase(const OptionMap& opts);
    
    DTransformSBase(const DTransformSBase&);
    
    DTransformSBase(DTransformSBase&&);
    
    void assign_from(const DTransformSBase& o);
    
    void copy_from(const DTransformSBase&);
    
    void move_from(DTransformSBase&&);

    template<class _ELeft, class _ERight>
    static inline dyn::IEigSys* add_basis(const EigComp<_ELeft, _ERight>& e) {
      return new dyn::EigSysComp{add_basis(e.left), add_basis(e.right)};
    }
    
    static dyn::IEigSys* add_basis(const Eig& e);
    
    static dyn::IEigSys* add_basis(const OptionMap& eso, const OptionMap& evo);
    
    static dyn::IEigSys* create_eig_sys(unsigned _dims, unsigned _deg, const std::string& _basis, const std::string& _type);

    void init();
    
    unsigned init(dyn::IEigSys* _es);
    
    /** @return IEigSys for given dimension d*/
    dyn::EigSys* eig_sys_at(unsigned d) const;
    dyn::EigSys* eig_sys_at(unsigned d, unsigned d0, dyn::IEigSys* es) const;

    static void write(OptionMap&, OptionMap&, dyn::IEigSys* es);
    
    dyn::IEigSys* m_eigensys;
    unsigned m_dims;
    unsigned m_nparams;
    bool m_complex;
  };
}

#endif // sfi_DTransformSBase_h
