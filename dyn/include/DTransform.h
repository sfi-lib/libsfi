#ifndef sfi_DTransform_h
#define sfi_DTransform_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <DTransformBase.h>
#include <TransformationBase.h>
#include <Transformation.h>
#include <Ylm.h>

namespace sfi {
/**
 * @class DTransform
 * This class is a fully dynamic version of Transform.
 * The same interface is implemented (ITransform) and essentially the same
 * methods are exported.
 *
 * Note that the functionality of this transform is slightly reduced.
 *
 * Helper functions are provided to simplify creation of dynamic transforms:
 * Example:
 * DTransform trf("A", eig_comp(eig_comp(eig_sparse(2, 2, "chebyshev"), eig_full(2, 2, "cos")), eig_sparse(2, 3, "legendre")));
 *
 *  eig_comp : Makes a composite transform
 *  eig_sparse/eig_full : Makes a sparse/full sub eigen system
 *  
 * TODO:
 * - Implement get_marginalize_evaluator_1D, get_marginalize_evaluator_2D,  get_slice_evaluator, get_slice_derivative_evaluator
 * - Implement: get_axis_transform,
 * - Implement: do_create_covariance
 */
  class DTransform : public DTransformBase<double> {
  public:
    using This = DTransform;
    using Base = DTransformBase<double>;
    using Coeff = double;
    using Eigenv = DynEigenVectors;
    
    template<class _Eig>
    DTransform(const std::string& name, const _Eig& e):Base(name, e) {
      if(m_complex) throw DTransformException("Complex transforms not allowed");
    }
    
    DTransform(const OptionMap& opts);
    
    DTransform(const DTransform&);
    
    DTransform(DTransform&&);
    
    virtual ~DTransform();

    DTransform& operator=(const DTransform& o);
    
    // ---- implementation of ITransform ----
    
    virtual const std::string& get_type_name() const;

    virtual Qty get_param(unsigned i) const;

    virtual void set_param(unsigned i, double val);

    virtual std::unique_ptr<IParamIterator> get_param_iterator() const;

    virtual std::unique_ptr<IEvaluator> get_evaluator(double scale = 1.0, bool external_coordinates = false) const;

    virtual std::unique_ptr<IEvaluator> get_marginalize_evaluator_1D(unsigned axis, double scale = 1.0, bool external_coordinates = false) const;

    virtual std::unique_ptr<IEvaluator> get_marginalize_evaluator_2D(unsigned a1, unsigned a2, double scale = 1.0, bool external_coordinates = false) const;

    virtual std::unique_ptr<IEvaluator> get_slice_evaluator(unsigned d2, bool _do_normalize, double scale = 1.0, bool external_coordinates = false) const;

    virtual std::unique_ptr<IEvaluator> get_slice_derivative_evaluator(unsigned d2, bool _do_normalize, double scale = 1.0, bool external_coordinates = false) const;
    
    virtual double get_value(const double* x) const;

    virtual double get_integral() const;

    virtual ITransform* get_axis_transform(unsigned d) const;

    virtual ITransform* get_clone() const;

    virtual void do_dump(bool unc = false) const;

    virtual void do_normalize(double norm);

    virtual bool do_eval(unsigned npts, const double* xv, double* res, double scale = 1.0, bool external_coordinates = false) const;

    virtual bool do_full_eval(const Matrix<double>& points, Matrix<double>& res, bool external_coordinates = false) const;

    virtual bool do_write(OptionMap&) const;

    virtual const MatrixSym<double>* do_create_covariance(bool poisson);

    virtual const Vector<double>* get_parameters() const;

    virtual const MatrixSym<double>* get_covariance() const;

    virtual bool do_copy(const ITransform*);
    
    // ---- public methods ----
    
    void full_project(const double* x, EvalData& p) const;

    template<Basis::CompType ctype, class _Func>
    inline _Func compute(const double* x, EvalData& p, const _Func& f) const {
      return compute(ctype, x, p, f);
    }

    void compute_all(Basis::CompType ctype, const double* _x, EvalData& p) const;
    
    /**
     * Evaluate transform at point x
     * @param x point to evaluate at
     * @param p EvalData instance for tmp storage
     * @param external_coordinates if the point is in external corrdinates
     */
    double eval(const double* x, EvalData& p, bool external_coordinates = false) const;
    
    /**
     * Evaluate for all basis vectors in multiple events.
     * @param points Matrix of dimensions N*Dims
     * @param res Matrix of dimensions N*nparams
     */
    bool full_eval(const Matrix<double>& points, Matrix<Coeff>& res, bool external_coordinates) const;
      
    /**
     * Will call axis_transform(axis,trf) to fill the instance.
     * The instance may be reused to get the transform along another axis.
     * @return Axis transform for given axis.
     */
    TransformVariableBase* axis_transform(unsigned axis) const;
    
    void print(std::ostream&) const;
    
    inline DTransform* clone() const { return new DTransform(*this); }
    
    // ---- exported interface ----
    
    inline This& compute_covariance(bool b) {
      this->do_compute_covariance(b, this->npar());
      return *this;
    }

    inline This& compute_uncertainty(bool b) {
      this->do_compute_uncertainty(b, this->npar());
      return *this;
    }
    
    inline const std::string& type_name() { return get_type_name(); }
    
    inline This& eigen_system() { return *this; }
    inline This& eigenvectors() { return *this; }
    
    inline bool init_params() {
      return Base::do_init_params(this->m_complex ? 2*m_nparams : m_nparams);
    }
        
    // ---- exported complex methods ----
    
    inline bool is_complex() const { return m_complex; }
  protected:
      
    template<class _Func>
    inline _Func compute(Basis::CompType ctype, const double* x, EvalData& p, const _Func& _f) const {
      _Func f(_f);
      p.iter.clear();
      compute_all(ctype, x, p);
      
      do {
        double g(1.0);
        for(unsigned d=0; d<dims(); ++d) g *= p[d][p.iter[d]];
        f(g);
      } while(p.iter.next());
      return f;
    }
  };

  inline std::ostream& operator<<(std::ostream& out, const DTransform& t) {
    t.print(out);
    return out;
  }

  /** @return Definition of a full tensor eigen system */
  auto eig_full(unsigned dim, unsigned deg, const std::string& basis) {
    return DTransform::Eig{dim, deg, basis, "full"};
  }

  /** @return Definition of a monomic sparse eigen system */
  auto eig_sparse(unsigned dim, unsigned deg, const std::string& basis) {
    return DTransform::Eig{dim, deg, basis, "sparse"};
  }

  /** @return Definition of a composite eigen system */
  template<class _ELeft, class _ERight>
  auto eig_comp(const _ELeft& _l, const _ERight& _r) {
    return DTransform::EigComp<_ELeft, _ERight>{_l, _r};
  }

  template<>
  struct _transform_type<DTransform,void> : tvalued<DTransform> { };

  template<> struct _t_eigen_system<DTransform> : tvalued<FullDynEiegenSystem> {};

  template<>
  class TransformationEigenBase<DTransform,void,true> : public TransformationBase {
  public:
    using Transf = DTransform;
    using Eigenv = t_EigenVectors<Transf>;

    inline unsigned dims() const {
      DTransform trf(this->m_options);
      return trf.dims();
    }
  
    /** @return New Transform instance */
    inline Transf* create_transform(Context&) const {
      return new DTransform(this->m_options);
    }

    /** @return Axis transform instance */
    inline TransformVariableBase* create_transform_1D(Context&, unsigned /*d*/) const {
      return nullptr;
    }
  protected:
    TransformationEigenBase(Context& ctx, const std::string& name):TransformationBase(ctx, name) { }

    TransformationEigenBase(Context& ctx, const OptionMap& opts):TransformationBase(ctx, opts) { }

    TransformationEigenBase(const TransformationEigenBase& o):TransformationBase(o) { }
  };

  template<class _Trafo>
  struct _t_is_valid_transform<_Trafo, DTransform> : ttrue {};

  template<>
  class Evaluator<DTransform> : public IEvaluator {
  public:
    using This = Evaluator<DTransform>;
    Evaluator(const DTransform* trf, double scale, bool external_coordinates);
    Evaluator(This&&);
    Evaluator(const This& o);
    
    virtual ~Evaluator();

    virtual double get_value(const double* x);
    virtual const ITransform* get_transform();
    virtual void set_scale(double scale);
    virtual void set_external_coordinates(bool ec);
    virtual unsigned get_dimensions() const;
    virtual bool do_eval(unsigned npts, const double* xv, double* res);
    virtual const Variable& get_variable(unsigned d) const;
    virtual IEvaluator* get_clone(bool deep) const;
    virtual unsigned get_nslice_parameters() const;
    virtual void set_slice(const double *c);
    virtual const ITransform* get_slice_transform();
    
    double operator()(const double* x);
  protected:
    DTransform::EvalData m_ed;
    const DTransform* m_trf;
    double m_scale;
    bool m_external_coordinates;
  };

  template<>
  class ParamIterator<const DTransform> : public IParamIterator {
  public:
    ParamIterator(const DTransform& t);
    
    virtual ~ParamIterator();
    
    virtual bool do_next();

    virtual unsigned get_degree(unsigned d);

    virtual Qty get_value();
  protected:
    dyn::EigSysIterator m_iter;
    const DTransform& m_trf;
  };

}
#endif // sfi_DTransform_h
