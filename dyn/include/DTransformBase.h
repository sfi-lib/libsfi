#ifndef sfi_DTransformBase_h
#define sfi_DTransformBase_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <DTransformSBase.h>
#include <TransformBase.h>
#include <TransformationBase.h>
#include <Transformation.h>
#include <Ylm.h>

namespace sfi {

  template<class _Coeff>
  class DTransformBase : public TransformParamBase<_Coeff>, public  DTransformSBase {
  public:
    struct DynEigenVectors {
      static constexpr bool is_dynamic() { return true; }
    };
    using This = DTransformBase<_Coeff>;
    using Base = TransformParamBase<_Coeff>;
    using Coeff = _Coeff;
    using Eigenv = DynEigenVectors;
    
    template<class _Eig>
    DTransformBase(const std::string& name, const _Eig& e):Base(name), DTransformSBase(name, e) {  init(); }
    
    DTransformBase(const OptionMap& opts);
    
    DTransformBase(const DTransformBase&);
    
    DTransformBase(DTransformBase&&);
    
    virtual ~DTransformBase();
    
    // ---- implementation of ITransform ----
    
    virtual const std::string& get_basis_name() const;

    virtual unsigned get_dimensions() const;

    virtual unsigned get_var_degree(unsigned i) const;

    virtual unsigned get_max_degree() const;

    virtual unsigned get_nparams() const;

    //virtual std::unique_ptr<IParamIterator> get_param_iterator() const;
    
    // ---- public methods ----
    
    struct EvalData {
      dyn::EigSysIterator iter;
      std::vector<std::vector<_Coeff>> data;
      inline std::vector<_Coeff>& operator[](size_t i) { return data[i]; }
    };
    
    void init_eval(EvalData& p) const;
    
    /** @return Integral of the squared transform */
    double square_integral() const;
    
    /** @return Integral of the linear transform */
    double linear_integral() const;
    
    /** @return Integral over all eigenvectors */
    double integral_pdf() const;

    /** Ensure that the integral of this transform has the given value. */
    void normalize(double norm);

    /**
     * Will call axis_transform(axis,trf) to fill the instance.
     * The instance may be reused to get the transform along another axis.
     * @return Axis transform for given axis.
     */
    TransformVariableBase* axis_transform(unsigned axis) const;
        
    /** Write to options */
    void write(OptionMap&) const;
    
    void print(std::ostream&) const;
    
    // ---- exported interface ----
    
    inline This& compute_covariance(bool b) {
      this->do_compute_covariance(b, this->npar());
      return *this;
    }

    inline This& compute_uncertainty(bool b) {
      this->do_compute_uncertainty(b, this->npar());
      return *this;
    }
    
    inline This& eigen_system() { return *this; }
    inline This& eigenvectors() { return *this; }
    
    inline bool init_params() {
      return Base::do_init_params(m_nparams);
    }
    
    struct _t_iterator {
      dyn::EigSysIterator iter;
      inline _t_iterator& operator++() { iter.next(); return *this; }
      inline bool operator!=(const _t_iterator& it) { return iter.index != it.iter.nparams; }
      inline dyn::EigSysIterator& operator*() { return iter; }
    };
    inline _t_iterator begin() const { return _t_iterator{iterator()}; }
    inline _t_iterator end() const { return _t_iterator{iterator()}; }
  protected:
    void assign_from(const DTransformBase& o);
    
    void copy_from(const DTransformBase&);
    
    void move_from(DTransformBase&&);
    
    void init();
    
    inline double to_pdf(double c) const { return this->is_amplitude() ? c*c : c; }
  };

  template<class _Coeff>
  DTransformBase<_Coeff>::DTransformBase(const OptionMap& opts):Base(opts), DTransformSBase(opts) {
    init();
  }

  template<class _Coeff>
  DTransformBase<_Coeff>::DTransformBase(const DTransformBase &o):Base(o), DTransformSBase(o) {}

  template<class _Coeff>
  DTransformBase<_Coeff>::DTransformBase(DTransformBase&& o):Base(std::move(o)), DTransformSBase(std::move(o)) {}

  template<class _Coeff>
  DTransformBase<_Coeff>::~DTransformBase() {}

  template<class _Coeff>
  const std::string& DTransformBase<_Coeff>::get_basis_name() const {
    auto* es = dynamic_cast<dyn::EigSys*>(m_eigensys);
    if(es) return es->basis_name();
    else {
      static const std::string _com_basis = "comp";
      return _com_basis;
    }
  }

  template<class _Coeff>
  unsigned DTransformBase<_Coeff>::get_dimensions() const { return m_dims; }

  template<class _Coeff>
  unsigned DTransformBase<_Coeff>::get_var_degree(unsigned i) const { return var_degree(i); }

  template<class _Coeff>
  unsigned DTransformBase<_Coeff>::get_max_degree() const { return 0; }

  template<class _Coeff>
  unsigned DTransformBase<_Coeff>::get_nparams() const { return m_nparams; }


  template<class _Coeff>
  void DTransformBase<_Coeff>::init_eval(EvalData& p) const {
    p.iter = iterator();
    p.data.resize(m_dims);
    for(unsigned d=0; d<m_dims; ++d) {
      size_t s = 0;
      switch(p.iter.types[d]) {
        case dyn::EigSysBasis::Cos: { s = bases::Cosine::buffer_size(p.iter.degrees[d]); break; }
        case dyn::EigSysBasis::Chebyshev: { s = bases::Chebyshev::buffer_size(p.iter.degrees[d]); break; }
        case dyn::EigSysBasis::Legendre: { s = bases::Legendre::buffer_size(p.iter.degrees[d]); break; }
        case dyn::EigSysBasis::Fourier: { s = bases::Fourier::buffer_size(p.iter.degrees[d]); break; }
        case dyn::EigSysBasis::Ylm: { s = (d& 0x1) ? 0 : bases::Ylm::buffer_size(p.iter.degrees[d]); break; }
      }
      p.data[d].resize(s);
    }
  }

  template<class _Coeff>
  double DTransformBase<_Coeff>::square_integral() const {
    double I = 0;
    for(unsigned i=0; i< m_nparams; ++i) I += coefficient_norm(true, this->m_par[i]);
    return I;
  }

  template<class _Type> inline double basis_intgeral(int k) {
    return (_Type::zero_integral(k)) ? 0 : _Type::integral(k);
  }

  template<class _Coeff>
  double DTransformBase<_Coeff>::linear_integral() const {
    auto it = iterator();
    double I(0), p(0);
    do {
      p = 1;
      for(unsigned d=0; d<m_dims; ++d) {
        switch(it.types[d]) {
          case dyn::EigSysBasis::Cos: { p *= basis_intgeral<bases::Cosine>(it.indices[d]); break; }
          case dyn::EigSysBasis::Chebyshev: { p *= basis_intgeral<bases::Chebyshev>(it.indices[d]); break; }
          case dyn::EigSysBasis::Legendre: { p *= basis_intgeral<bases::Legendre>(it.indices[d]); break; }
          case dyn::EigSysBasis::Fourier: { p *= basis_intgeral<bases::Fourier>(it.indices[d]); break; }
          case dyn::EigSysBasis::Ylm : { p = basis_intgeral<bases::Ylm>(bases::ylm_index(it.indices[d],it.indices[d+1])); ++d; break; }
        }
      }
      p *= coefficient_norm(false, this->par(it.index));
      I += p;
    } while(it.next());
    return I;
  }

  template<class _Coeff>
  double DTransformBase<_Coeff>::integral_pdf() const {
    if(this->m_is_amplitude) return square_integral();
    else return linear_integral();
  }

  template<class _Coeff>
  void DTransformBase<_Coeff>::normalize(double norm) {
    if(this->is_amplitude()) this->scale(sqrt(norm/this->integral_pdf()));
    else this->scale(norm/this->integral_pdf());
  }

  template<class _Coeff>
  TransformVariableBase* DTransformBase<_Coeff>::axis_transform(unsigned /*axis*/) const {
    return nullptr;
  }

  template<class _Coeff>
  void DTransformBase<_Coeff>::write(OptionMap& op) const {
    Base::write(op);
    DTransformSBase::write(op.sub("eigensystem", true), op.sub("eigensystem.eigenv", true), m_eigensys);
  }

  template<class _Coeff>
  void DTransformBase<_Coeff>::print(std::ostream& out) const {
    m_eigensys->do_print(out);
  }

  template<class _Coeff>
  void DTransformBase<_Coeff>::assign_from(const DTransformBase& o) {
    Base::copy_from(o);
    DTransformSBase::assign_from(o);
  }

  template<class _Coeff>
  void DTransformBase<_Coeff>::copy_from(const DTransformBase& o) {
    Base::copy_from(o);
    DTransformSBase::copy_from(o);
  }

  template<class _Coeff>
  void DTransformBase<_Coeff>::move_from(DTransformBase&& o) {
    Base::move_from(o);
    DTransformSBase::move_from(o);
  }

  template<class _Coeff>
  void DTransformBase<_Coeff>::init() {
    DTransformSBase::init();
    Base::do_init_params(m_nparams);
  }
}

#endif // sfi_DTransformBase_h
