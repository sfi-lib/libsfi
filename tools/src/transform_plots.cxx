/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "transform_plots.h"

#include <TAxis.h>
#include <TGraph.h>
#include <TCanvas.h>

namespace sfi {
  void plot_amp_transform_details(const std::string& prefix, const amplitude_transform_result<ttrue>& res, const std::string& fprefix) {
    const unsigned niter = res.delta_I_v.size();
    std::vector<double> iternr(niter, 0.0);
    for(unsigned i=0; i<niter; ++i) iternr[i] = i;

    {
      auto* c = create_canvas(prefix+" Delta I");
      auto* g = create_graph(iternr, res.delta_I_v);
      g->SetLineWidth(3.0);
      g->Draw("ALP");
      g->GetXaxis()->SetTitle("Iteration");
      g->GetYaxis()->SetTitle("#delta I");
      c->Update();
      if(!fprefix.empty()) c->Print((fprefix+c->GetName()+".pdf").c_str());
    }
    {
      auto* c = create_canvas(prefix+" Res I");
      auto* g = create_graph(iternr, res.res_I_v);
      g->SetLineWidth(3.0);
      g->Draw("ALP");
      g->GetXaxis()->SetTitle("Iteration");
      g->GetYaxis()->SetTitle("Result I");
      c->Update();
      if(!fprefix.empty()) c->Print((fprefix+c->GetName()+".pdf").c_str());
    }
    {
      auto* c = create_canvas(prefix+" candidate I");
      auto* g = create_graph(iternr, res.candidate_I_v);
      g->SetLineWidth(3.0);
      g->Draw("ALP");
      g->GetXaxis()->SetTitle("Iteration");
      g->GetYaxis()->SetTitle("Candidate I");
      c->Update();
      if(!fprefix.empty()) c->Print((fprefix+c->GetName()+".pdf").c_str());
    }
    {
      auto* c = create_canvas(prefix+" nnegative events I");
      auto* g = create_graph(iternr, res.nnegative_I_v);
      g->SetLineWidth(3.0);
      g->Draw("ALP");
      g->GetXaxis()->SetTitle("Iteration");
      g->GetYaxis()->SetTitle("Negative events");
      c->Update();
      if(!fprefix.empty()) c->Print((fprefix+c->GetName()+".pdf").c_str());
    }
  }
}
