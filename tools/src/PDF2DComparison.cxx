/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "PDF2DComparison.h"

#include "ext_utils.h"
#include "TransformPlot.h"

#include <TH2D.h>

using namespace sfi;

Histogram2DPlot::Histogram2DPlot():m_color(kBlack), m_title(""), m_draw_string("LEGO"),
    m_histogram(0,0.0,0.0,0,0.0,0.0), m_minimum(std::nan("")), m_maximum(std::nan("")) {
  init();
}

Histogram2DPlot::Histogram2DPlot(const OptionMap& opts):m_opts(opts), m_color(kBlack), m_title(""), m_draw_string("LEGO"),
    m_histogram(0,0.0,0.0,0,0.0,0.0), m_minimum(std::nan("")), m_maximum(std::nan("")) {
  init();
}

void
Histogram2DPlot::plot() {
  TH2D* hi2 = create_histogram(m_title, m_histogram);
  hi2->SetLineColor(m_color);
  if(std::isfinite(m_minimum)) hi2->SetMinimum(m_minimum);
  if(std::isfinite(m_maximum)) hi2->SetMaximum(m_maximum);
  hi2->Draw(m_draw_string.c_str());
}

void
Histogram2DPlot::init() {
  m_opts.bind("color", m_color);
  m_opts.bind("title", m_title);
  m_opts.bind("draw", m_draw_string);
  m_opts.bind("minimum", m_minimum);
  m_opts.bind("maximum", m_maximum);
}

PDF2DComparison::PDF2DComparison(Context& ctx, const OptionMap& opts):m_opts(opts), m_v1(0), m_v2(1), m_nbins(20),
    m_maximum(std::nan("")), m_minimum(std::nan("")),
    m_canvas_title(""), m_histo_plot(opts.sub("histogram")), m_trf_plot(ctx, opts.sub("transform")),
    m_external_coordinates(false), m_scale_to_data(false) {
  init();
}

PDF2DComparison::PDF2DComparison(Context& ctx, const std::string& title):m_v1(0), m_v2(1), m_nbins(20),
    m_maximum(std::nan("")), m_minimum(std::nan("")),
    m_canvas_title(title), m_histo_plot(), m_trf_plot(ctx, (IEvaluator*)0),
    m_external_coordinates(false), m_scale_to_data(false){
  init();
}

PDF2DComparison::This& PDF2DComparison::nbins(int _nb) {
  m_nbins = _nb;
  m_trf_plot.npointsx(_nb).npointsy(_nb);
  return *this;
}

PDF2DComparison::This&
PDF2DComparison::external_coordinates(bool ec) {
  m_external_coordinates = ec;
  m_trf_plot.external_coordinates(ec);
  return *this;
}

void
PDF2DComparison::plot() {
  create_canvas(m_canvas_title);
  if(std::isfinite(m_minimum)) {
    m_histo_plot.minimum(m_minimum);
    m_trf_plot.minimum(m_minimum);
  }
  if(std::isfinite(m_maximum)) {
    m_histo_plot.maximum(m_maximum);
    m_trf_plot.maximum(m_maximum);
  }
  m_histo_plot.plot();
  m_trf_plot.superimpose(true).color(kRed);
  if(m_scale_to_data) m_trf_plot.scale(m_histo_plot.histogram().entries());
  m_trf_plot.npointsx(m_nbins).npointsy(m_nbins).external_coordinates(m_external_coordinates);
  m_trf_plot.plot();
}

void
PDF2DComparison::init() {
  m_opts.bind("title", m_canvas_title);
  m_opts.bind("var1", m_v1);
  m_opts.bind("var2", m_v2);
  m_opts.bind("nbins", m_nbins);
  m_opts.bind("external_coordinates", m_external_coordinates);
  m_opts.bind("scale_to_data", m_scale_to_data);
  m_opts.bind("minimum", m_minimum);
  m_opts.bind("maximum", m_maximum);
}
