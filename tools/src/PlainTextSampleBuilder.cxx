/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Sample.h"
#include "SampleBuilderBase.h"

#include <fstream>
#include <sstream>
#include <assert.h>

namespace sfi {
  class PlainTextSampleBuilder : public SampleBuilderBase {
  public:
    PlainTextSampleBuilder(Context& ctx, OptionMap&);

    virtual ~PlainTextSampleBuilder();

    // ---- implementation of ISampleBuilder ----

    virtual ISample* do_build(Context& ctx, const PVariables& vars, const DataSource& src_data);
  protected:
    std::string m_file_name;
    unsigned m_nevents;
  };
}

using namespace sfi;
using namespace std;

int parse_input(const char *_file_input_name, int & _dim, ISample* spl)
{
  int events;
  ifstream input(_file_input_name);
  if (input.is_open()) {
    string line;
    getline(input,line);
    {
      stringstream ss(line);
      ss >> events;
      ss >> _dim;
    }
    cout << "Reading " << events << " events with a dimension of " << _dim << endl;
    if(spl && (spl->get_dim() != (unsigned)_dim)) {
      cout << "Invalid dimensionality " << _dim << endl;
      return 0;
    }

    double dummy;
    int event=0;
    double w;
    while (getline(input,line)) {
      stringstream ss(line);
      if(spl) {
        for (unsigned d=0; d<(unsigned)_dim; ++d) ss >> spl->get_x(event, d);
        ss >> w;
        spl->set_w(event, w);
      }
      else {
        for (unsigned d=0; d<(unsigned)_dim; ++d) ss >> dummy;
        ss >> dummy;
      }
      ++event;
    }
    input.close();
    cout << "In total " << event << " events, done." << endl;
    assert(event==events);
    return event;
  }
  else {
    cout << "Cannot open file '" << _file_input_name << "'" << endl;
    return 0;
  }
}

extern "C" {
  sfi::IComponent* sfi_factory_PlainText(Context& ctx, OptionMap& opts) {
    return new sfi::PlainTextSampleBuilder(ctx, opts);
  }
}

PlainTextSampleBuilder::PlainTextSampleBuilder(Context& ctx, OptionMap& opts):SampleBuilderBase(ctx, opts),
    m_file_name(""), m_nevents(0) {
  m_options.bind("file_name", m_file_name);
  m_nevents = ::parse_input(m_file_name.c_str(), m_dims, 0);
  m_is_weighted = true;
}

PlainTextSampleBuilder::~PlainTextSampleBuilder() { }

// ---- implementation of ISampleBuilder ----

// TODO: Take src data into account?
ISample*
PlainTextSampleBuilder::do_build(Context& ctx, const PVariables& vars, const DataSource& /*src_data*/) {
  ISample* res = this->create_sample(ctx, m_dims, m_is_weighted, m_nevents);
  if(!res) {
    do_log_error("do_build() Error parsing file '"<<m_file_name<<"', no sample generated!");
    return res;
  }
  res->set_variables(vars);
  int dim(0);
  ::parse_input(m_file_name.c_str(), dim, res);
  return res;
}
