/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "gui.h"
#include "ext_utils.h"

#include <TApplication.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TLine.h>
#include <TLegend.h>

using namespace sfi;

constexpr bool _DoDebug = false;

class GraphPlot : public Plot {
public:
  GraphPlot(const Vector<double>& xvals, const Vector<double>& yvals):m_graph(create_graph(xvals, yvals)) {}

  GraphPlot(const std::vector<double>& xvals, const std::vector<double>& yvals):m_graph(create_graph(xvals, yvals)) {}

  GraphPlot(const Vector<double>& xvals, const SampleStats& yvals):m_graph(create_graph(xvals, yvals)) {}

  GraphPlot(const Vector<double>& xvals, const Vector<double>& yvals, const Vector<double>& yuncvals):m_graph(create_graph(xvals, yvals, yuncvals)) {}

  virtual ~GraphPlot() {}

  virtual bool draw(bool first) {
    m_graph->SetLineColor(m_color);
    if(m_marker_color > -1) {
      m_graph->SetMarkerSize(2);
      m_graph->SetMarkerColor(m_marker_color);
      m_graph->SetMarkerStyle(m_marker_style);
    }
    m_graph->SetLineWidth(m_line_size);
    m_graph->Draw(first ? "ALP" : "LP");
    return true;
  }
  TGraph* m_graph;
};

class HistoPlot : public Plot {
public:
  HistoPlot(const std::string& name, const Histogram<false, double>& hi):m_histo(create_histogram(name, hi)) {}

  HistoPlot(const std::string& name, const Histogram<true, double>& hi):m_histo(create_histogram(name, hi)) {}

  virtual ~HistoPlot() {}

  virtual bool draw(bool first) {
    m_histo->SetLineColor(m_color);
    if(m_marker_color > -1) {
      m_histo->SetMarkerSize(2);
      m_histo->SetMarkerColor(m_marker_color);
      m_histo->SetMarkerStyle(m_marker_style);
    }
    m_histo->SetLineWidth(m_line_size);
    m_histo->Draw(first ? "" : "SAME");
    return true;
  }
  TH1D* m_histo;
};

class Histo2DPlot : public Plot {
public:
  Histo2DPlot(const std::string& name, const Histogram2D<false, double>& hi):m_histo(create_histogram(name, hi)) {}

  Histo2DPlot(const std::string& name, const Histogram2D<true, double>& hi):m_histo(create_histogram(name, hi)) {}

  virtual ~Histo2DPlot() {}

  virtual bool draw(bool first) {
    m_histo->SetLineColor(m_color);
    m_histo->SetLineWidth(m_line_size);
    m_histo->Draw(first ? "SURF" : "SURF SAME");
    return true;
  }
  TH2D* m_histo;
};

class LinePlot: public Plot {
public:
  LinePlot(bool horizontal, double value, double minv, double maxv):m_horizontal(horizontal),m_value(value),
  m_minval(minv), m_maxval(maxv) {}

  virtual ~LinePlot() {}

  virtual bool draw(bool) {
    TLine* l = 0;
    if(m_horizontal) l = new TLine(m_minval, m_value, m_maxval, m_value);
    else l = new TLine(m_value, m_minval, m_value, m_maxval);
    l->SetLineColor(m_color);
    l->SetLineWidth(m_line_size);
    l->Draw();
    return true;
  }
  bool m_horizontal;
  double m_value, m_minval, m_maxval;
};

Plot::Plot():m_title(""), m_color(1), m_marker_color(-1), m_marker_style(-1), m_line_size(3.0) { }

Plot::Plot(const Plot& o):m_title(o.m_title), m_color(o.m_color), m_marker_color(o.m_marker_color), m_marker_style(o.m_marker_style), m_line_size(o.m_line_size) {}

Plot::Plot(const OptionMap& opts):m_title(""), m_color(1), m_marker_color(-1), m_marker_style(-1), m_line_size(3.0) { init(opts); }

void Plot::init(const OptionMap& opts) {
  opts.get_if("title", m_title).get_if("color", m_color);
  opts.get_if("marker_color", m_marker_color).get_if("marker_style", m_marker_style);
  opts.get_if("line_size", m_line_size);
}

void Plot::copy_from(const Plot& o) {
  m_title = o.m_title; m_color = o.m_color;
  m_marker_color = o.m_marker_color; m_marker_style = o.m_marker_style;
  m_line_size = o.m_line_size;
}

Canvas& Canvas::get(const std::string& title) {
  static std::map<std::string,Canvas*> s_canvases;
  if(title.empty()) {
    std::cout<<"Error : Canvas::get empty title!"<<std::endl;
  }
  auto it = s_canvases.find(title);
  if(it != s_canvases.end()) return *it->second;
  else {
    auto el = s_canvases.emplace(std::make_pair(title,new Canvas(title)));
    return *el.first->second;
  }
}

Plot& Canvas::horizontal_line(double yval) { return add(new LinePlot(true, yval, 0, 0)); }
Plot& Canvas::vertical_line(double yval) { return add(new LinePlot(false, yval, 0, 0)); }

Plot& Canvas::draw(const std::string& name, const Histogram<false, double>& hi) { return add(new HistoPlot(name, hi)); }
Plot& Canvas::draw(const std::string& name, const Histogram<true, double>& hi) { return add(new HistoPlot(name, hi)); }

Plot& Canvas::draw(const std::string& name, const Histogram2D<false, double>& hi) { return add(new Histo2DPlot(name, hi)); }
Plot& Canvas::draw(const std::string& name, const Histogram2D<true, double>& hi) { return add(new Histo2DPlot(name, hi)); }

Plot& Canvas::draw(const Vector<double>& xvals, const Vector<double>& yvals) { return add(new GraphPlot(xvals, yvals)); }
Plot& Canvas::draw(const std::vector<double>& xvals, const std::vector<double>& yvals) { return add(new GraphPlot(xvals, yvals)); }
Plot& Canvas::draw(const Vector<double>& xvals, const SampleStats& yvals) {
  return add(new GraphPlot(xvals, yvals));
}
Plot& Canvas::draw(const Vector<double>& xvals, const Vector<double>& yvals, const Vector<double>& yuncvals) {
  return add(new GraphPlot(xvals, yvals, yuncvals));
}

template<class _Obj> void handle_plot_obj(_Obj* o, TLegend* leg, bool first, Plot* p, const std::vector<std::string>& axis_titles) {
  if(leg) leg->AddEntry(o, p->title().c_str());
  if(first) {
    o->GetXaxis()->SetTitle(axis_titles[0].c_str());
    o->GetYaxis()->SetTitle(axis_titles[1].c_str());
  }
}

// TODO: Make member
void handle_plot_obj(TCanvas* canvas, Plot* p, TLegend* leg, bool first, const std::vector<std::string>& axis_titles) {
  auto* lp = dynamic_cast<LinePlot*>(p);
  if(lp) {
    if(lp->m_horizontal) {
      lp->m_minval = canvas->GetUxmin();
      lp->m_maxval = canvas->GetUxmax();
    }
    else {
      lp->m_minval = canvas->GetUymin();
      lp->m_maxval = canvas->GetUymax();
    }
  }
  p->draw(first);
  auto* hi = dynamic_cast<HistoPlot*>(p);
  auto* gr = dynamic_cast<GraphPlot*>(p);
  if(hi) handle_plot_obj(hi->m_histo, leg, first, p, axis_titles);
  else if(gr) handle_plot_obj(gr->m_graph, leg, first, p, axis_titles);
}

Canvas& Canvas::plot() {
  if(_DoDebug) std::cout<<"Canvas @"<<(void*)this<<" plot, #plots: "<<m_plots.size()<<" drawn: "<<m_drawn<<std::endl;
  m_canvas->cd();
  if(m_legend && !m_legend_ptr) m_legend_ptr = new TLegend(0.7,0.7,0.90,0.90);
  for(unsigned i=m_drawn; i<m_plots.size(); ++i) {
    handle_plot_obj(m_canvas, m_plots[i], m_legend_ptr, i==0, m_axis_titles);
    if(!i) m_canvas->Update();
  }
  m_drawn = m_plots.size();
  m_canvas->Update();
  return *this;
}

Canvas& Canvas::save(const std::string& file_name) {
  m_canvas->cd();
  m_canvas->Update();
  m_canvas->Print(file_name.c_str());
  return *this;
}

Canvas::Canvas(const std::string& title):m_title(title), m_canvas(new TCanvas(title.c_str(), title.c_str())), m_legend_ptr(nullptr),
    m_axis_titles(3,""), m_drawn(0), m_legend(false) {
  if(_DoDebug) std::cout<<"Canvas @"<<(void*)this<<" create '"<<m_title<<"'"<<std::endl;
}

Canvas::Canvas(Canvas&& o):m_title(std::move(o.m_title)), m_canvas(o.m_canvas), m_legend_ptr(nullptr), m_plots(std::move(o.m_plots)),
    m_axis_titles(o.m_axis_titles), m_drawn(o.m_drawn), m_legend(o.m_legend) {
  if(_DoDebug) std::cout<<"Canvas @"<<(void*)this<<" move '"<<m_title<<"'"<<std::endl;
  o.m_plots.clear();
  o.m_canvas = nullptr;
}

Canvas::Canvas(Canvas& o):m_title(std::move(o.m_title)), m_canvas(o.m_canvas), m_legend_ptr(nullptr), m_plots(std::move(o.m_plots)),
    m_axis_titles(o.m_axis_titles), m_drawn(o.m_drawn), m_legend(o.m_legend) {
  if(_DoDebug) std::cout<<"Canvas @"<<(void*)this<<" steal '"<<m_title<<"'"<<std::endl;
  o.m_plots.clear();
  o.m_canvas = nullptr;
}

Canvas::~Canvas() {
  if(_DoDebug) std::cout<<"Canvas @"<<(void*)this<<" delete '"<<m_title<<"'"<<std::endl;
}

Plot& Canvas::add(Plot* p) {
  if(_DoDebug) std::cout<<"Canvas @"<<(void*)this<<" add, drawn: "<<m_drawn<<std::endl;
  m_plots.push_back(p);
  return *p;
}

Application::Application(int argc, char** argv, const std::string& conf_file):m_app(nullptr) {
  bool help = false;
  m_options.bind("help", help); \
  m_options.parse_options(argc, argv);
  if(!conf_file.empty()) m_options.parse_opts_file(conf_file);
  if(help) m_options.print_help(std::cout);
  else {
    m_app = new TApplication(argv[0], &argc, argv);
    init_style();
  }
}

Application::~Application() {
  if(m_app) {
    delete m_app;
    m_app = 0;
  }
}

bool Application::run() {
  if(m_app) {
    m_app->Run();
    return true;
  }
  else return false;
}
