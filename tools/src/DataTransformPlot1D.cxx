/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "DataTransformPlot1D.h"
#include "ext_utils.h"
#include "ITransform.h"
#include "Variables.h"

using namespace sfi;

// TODO: Init empty histogram
DataTransformPlot1D::DataTransformPlot1D(Context& ctx, const ITransform& tr):m_log(Log::get(ctx,"DataTransformPlot1D")), m_trf_plot(ctx, tr), m_variable(0),
    m_data_histogram(), m_scale_to_data(false) {
}

DataTransformPlot1D::DataTransformPlot1D(Context& ctx, const ITransform& tr, const OptionMap& opts):m_log(Log::get(ctx,"DataTransformPlot1D")), m_trf_plot(ctx, tr, opts), m_variable(0),
    m_data_histogram(), m_scale_to_data(false) {
}

DataTransformPlot1D::DataTransformPlot1D(Context& ctx, IEvaluator* ev):m_log(Log::get(ctx,"DataTransformPlot1D")), m_trf_plot(ctx, ev), m_variable(0),
    m_data_histogram(), m_scale_to_data(false) {
}

Canvas
DataTransformPlot1D::plot() {
  if(m_title.empty()) {
    do_log_error("Must have title");
  }
  auto can = Canvas::get(m_title);
  can.draw(m_title, m_data_histogram);
  auto* trf = m_trf_plot.transform();
  if(trf) {
    auto vars = trf->get_variables();
    if(vars) {
      auto& v = vars->at(0);
      std::string title(v.title().empty() ? v.name() : v.title());
      can.axis_title(0, title).axis_title(1,"Events");
    } else {
      do_log_error("Transform has no Variables");
      return can;
    }
    if(m_scale_to_data) m_trf_plot.scale(m_data_histogram.entries());
    can.draw(&m_trf_plot);
    can.horizontal_line(0.0).color(kBlack);
    can.plot();
    if(!m_file_name.empty()) can.save(m_file_name);
  } else {
    do_log_error("No Transform");
  }
  return can;
}
