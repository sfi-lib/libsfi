/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "PDF1DComparison.h"

#include "TransformPlot.h"
#include <TH1D.h>
#include <TGraph.h>
#include <TLegend.h>
#include <TCanvas.h>
#include "ext_utils.h"

using namespace sfi;

PDF1DComparison::PDF1DComparison(Context& ctx, const std::string& prefix):
    m_log(Log::get(ctx, "PDF1DComparison")), m_prefix(prefix),
    m_n_pdf_bins(100), m_plot_external_coordinates(true), m_debug(true), m_plot_marginalizations(false), m_normalize(true) {
}

PDF1DComparison::~PDF1DComparison() { }

void
PDF1DComparison::plot() {
  pdf_comparison(m_prefix+ " int.", false);
  if(m_plot_external_coordinates) pdf_comparison(m_prefix+ " ext.", true);
}

void
PDF1DComparison::pdf_comparison(const std::string& prefix, bool external_coordinates) {
  if(m_transforms.empty()) {
    do_log_error("pdf_comparison() No transforms!");
    return;
  }
  const size_t NSrcs = m_transforms.size();
  const size_t NDims = m_transforms[0].get_dimensions();
  if(m_samples.size() != NSrcs) {
    do_log_error("pdf_comparison() Invalid number of samples ("<<m_samples.size()<<") expected "<<NSrcs);
    return;
  }
  if(m_1D_transforms.size() != NSrcs) {
    do_log_error("pdf_comparison() Invalid number of 1D transforms ("<<m_1D_transforms.size()<<") expected "<<NSrcs);
    return;
  }
  if(!m_samples[0]->get_variables()) {
    do_log_error("pdf_comparison() No Variables!");
    return;
  }
  auto& vars = *m_samples[0]->get_variables();
  if(vars.nvariables() != NDims) {
    do_log_error("pdf_comparison() Invalid number of variables ("<<vars.nvariables()<<") expected "<<NDims);
    return;
  }
  for(size_t src = 0; src < NSrcs; ++src) {
    if(m_1D_transforms[src].size() != NDims) {
      do_log_error("pdf_comparison() Invalid number of 1D transforms ("<<m_1D_transforms[src].size()<<") for sample '"<<m_samples[src]->get_name()<<"' expected "<<NDims);
      return;
    }
  }

  std::vector<int> data_col = {kBlack, kRed};
  std::vector<int> margin_col = {kGreen, kBlue};
  for(unsigned var = 0; var < NDims; ++var) {
    auto& v = vars[var];
    std::string vname = v.name();
    std::string vtitle = v.title().empty() ? v.name() : v.title();
    auto* tc = create_canvas(prefix+"_"+vname);
    m_canvases.push_back(tc);
    TLegend* legend = new TLegend(0.75, 0.55, 0.92, 0.86);

    std::vector<TH1D*> histos;
    for(size_t src = 0; src < NSrcs; ++src) {
      std::string splname = m_samples[src]->get_name();
      auto& _dhi = (external_coordinates ? m_histos_ext : m_histos_int)[src][var];
      auto* dhi = create_histogram(m_prefix + vname + (external_coordinates ? " ext.":" int."), _dhi);
      histos.push_back(dhi);
      dhi->SetLineColor(data_col[src]);
      dhi->GetXaxis()->SetTitle(vtitle.c_str());
      dhi->Draw(src ? "SAME" : "");
      legend->AddEntry(dhi, (splname+" data").c_str());

      double norm = m_normalize ? _dhi.sum_weights() : 1.0;
      if(m_normalize) m_1D_transforms[src][var].do_normalize(norm);
      auto *sa_plot = TransformPlot1D(m_log.context(), m_1D_transforms[src][var]).title(vname).color(data_col[src]).npoints(m_n_pdf_bins).external_coordinates(external_coordinates).superimpose(true).plot();
      legend->AddEntry(sa_plot, (splname+" single").c_str());
      {
        auto* axt = m_transforms[src].get_axis_transform(var);
        if(axt) {
          do_log_info("pdf_comparison() var: "<<vname<<" histogram: '"<<dhi->GetName()<<"' norm: "<<norm);
          if(m_debug) {
            do_log_info("pdf_comparison() ** I (1D)="<<m_1D_transforms[src][var].get_integral()<<" : "<<m_1D_transforms[src][var].get_type_name());
            m_1D_transforms[src][var].do_dump();
            do_log_info("pdf_comparison() ** I (axis)="<<axt->get_integral()<<" : "<<axt->get_type_name()<<" deg: "<<axt->get_max_degree());
            axt->do_dump();
          }
          if(m_normalize) axt->do_normalize(norm);
          auto* mar_plot = TransformPlot1D(m_log.context(), *axt).title(vname).npoints(m_n_pdf_bins).color(margin_col[src]).external_coordinates(external_coordinates).superimpose(true).plot();
          legend->AddEntry(mar_plot, (splname+" marg.").c_str());
        }
        else {
          do_log_error("pdf_comparison() unable to get axis transform for axis "<<var<<" transf: "<<m_transforms[src].get_name());
        }
        if(m_plot_marginalizations) {
          auto* mev = m_transforms[src].get_marginalize_evaluator_1D(var, 1.0, external_coordinates).release();
          TransformPlot1D(m_log.context(), mev).scale(_dhi.entries()).title(vname).npoints(m_n_pdf_bins).color(kYellow).external_coordinates(external_coordinates).superimpose(true).plot();
        }
      }
    }
    adjust_range(tc, histos);
    legend->Draw();
    tc->Update();
    if(!m_file_prefix.empty()) {
      std::string fname = m_file_prefix+"_"+vname + (external_coordinates ? "_ext":"_int")+".pdf";
      save_canvas(tc, fname);
    }
  }
}
