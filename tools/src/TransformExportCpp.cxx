/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "TransformExportCpp.h"
#include "ITransform.h"
#include "Sample.h"
#include <fstream>
#include <iomanip>
#include "cosine.h"

using namespace sfi;
using namespace sfi::bases;

TransformExportCpp::TransformExportCpp(Context& ctx, const std::string& out_prefix):
    m_log(Log::get(ctx, "TransformExportCpp")),
    m_out_prefix(out_prefix),
    m_precision(10) {
}

bool
TransformExportCpp::do_export(const ITransform& trf) {
  auto vars = trf.get_variables();
  if(!vars) {
    do_log_error("do_export() Transform has no variables");
    return false;
  }

  // TODO: support other
  if(trf.get_basis_name() != "cos_on") {
    do_log_error("do_export() Only transforms with basis 'cos_on' supported");
    return false;
  }
  const unsigned ndims(trf.get_dimensions());
  const std::string file_name(m_out_prefix+"_transform.h");

  std::ofstream out(file_name);
  out<<std::setprecision(m_precision);
  std::string prefix(m_out_prefix);
  for(auto& c : prefix)
    if(c == '/') c = '_';
  out<<"#ifndef "<<prefix<<"_transform_h\n";
  out<<"#define "<<prefix<<"_transform_h\n\n";
  out<<"#include <assert.h>\n";
  out<<"#include <math.h>\n";
  out<<"double "<<prefix<<"(const double* _x) {\n";
  out<<"  double x["<<ndims<<"];\n";
  for(unsigned i=0; i<ndims; ++i) {
    auto& v = vars->at(i);
    std::string vname = "_x["+std::to_string(i)+"]";
    out<<"    assert(( "<<vname<<" >= "<<v.minval()<<") && ( "<<vname<<" <= "<<v.maxval()<<") && \"Variable out of range!\");\n";
    out<<"    x["<<i<<"] = (";
    v.write_vtrans_cpp(vname, out);
    out<<" + 1.0)*0.5*M_PI;\n";
  }
  out<<"  double res = ";
  auto pit = trf.get_param_iterator();
  bool first(true);
  do {
    if(first) first = false;
    else out<<"  + ";
    out<<(double)pit->get_value()<<" * ";
    for(unsigned i=0; i<ndims; ++i) {
      if(i) out<<"*";
      out<<Cosine::norm(pit->get_degree(i))<<"*cos("<<pit->get_degree(i)<<".0 * x["<<i<<"])";
    }
    out<<"\n";
  } while(pit->do_next());
  out<<";\n";
  if(trf.get_is_amplitude()) out<<"  return res*res;\n";
  else out<<"  return res;\n";
  out<<"}\n";
  out<<"#endif // "<<m_out_prefix<<"_transform_h\n";
  out<<std::endl;
  out.close();

  do_log_info("do_export() file '"<<file_name<<"' written");
  return true;
}
