/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ext_utils.h"

#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TLine.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TDecompChol.h>
#include <TDecompSVD.h>
#include <TStyle.h>
#include <TFile.h>
#include <TLegend.h>
#include <TF1.h>
#include <TFitResult.h>
#include <TVectorD.h>
#include <TMatrixD.h>
#include <TMatrixDSym.h>
#include <TMatrixDSymEigen.h>
#include <TDecompSVD.h>

namespace sfi {

  TH1D* create_histogram(const std::string& name, const Histogram<false, double>& hi) {
    TH1D* his = create_histogram(name, hi.nbins(), hi.min_x(), hi.max_x());
    for(unsigned i=1; i<=hi.nbins(); ++i) his->SetBinContent(i, hi.bin_content(i));
    return his;
  }

  TH1D* create_histogram(const std::string& name, const Histogram<true, double>& hi) {
    TH1D* his = create_histogram(name, hi.nbins(), hi.min_x(), hi.max_x());
    for(unsigned i=1; i<=hi.nbins(); ++i) {
      his->SetBinContent(i, hi.bin_content(i));
      his->SetBinError(i, hi.bin_uncertainty(i));
    }
    return his;
  }

  TH1D* draw_histogram(const std::string& name, const Histogram<false, double>& hi, const std::string& draws, int color) {
    TH1D* hi2 = create_histogram(name, hi);
    if(hi2) {
      hi2->SetLineColor(color);
      hi2->Draw(draws.c_str());
    }
    return hi2;
  }

  TH1D* draw_histogram(const std::string& name, const Histogram<true, double>& hi, const std::string& draws, int color) {
    TH1D* hi2 = create_histogram(name, hi);
    if(hi2) {
      hi2->SetLineColor(color);
      hi2->Draw(draws.c_str());
    }
    return hi2;
  }

  TH2D* create_histogram(const std::string& name, const Histogram2D<false, double>& hi) {
    TH2D* hi2 = create_histogram_2d(name, hi.nbins_x(), hi.min_x(), hi.max_x(), hi.nbins_y(), hi.min_y(), hi.max_y(), "", "");
    for(unsigned bx=1; bx<=hi.nbins_x(); ++bx) {
      for(unsigned by=1; by<=hi.nbins_y(); ++by) {
        hi2->SetBinContent(bx, by, hi.bin_content(bx, by));
      }
    }
    return hi2;
  }

  TH2D* create_histogram(const std::string& name, const Histogram2D<true, double>& hi) {
    TH2D* hi2 = create_histogram_2d(name, hi.nbins_x(), hi.min_x(), hi.max_x(), hi.nbins_y(), hi.min_y(), hi.max_y(), "", "");
    for(unsigned bx=1; bx<=hi.nbins_x(); ++bx) {
      for(unsigned by=1; by<=hi.nbins_y(); ++by) {
        hi2->SetBinContent(bx, by, hi.bin_content(bx, by));
        hi2->SetBinError(bx, by, hi.bin_uncertainty(bx, by));
      }
    }
    return hi2;
  }

  TH2D* draw_histogram(const std::string& name, const Histogram2D<false, double>& hi, const std::string& draws, int color) {
    TH2D* hi2 = create_histogram(name, hi);
    if(hi2) {
      hi2->SetLineColor(color);
      hi2->Draw(draws.c_str());
    }
    return hi2;
  }

  TH2D* draw_histogram(const std::string& name, const Histogram2D<true, double>& hi, const std::string& draws, int color) {
    TH2D* hi2 = create_histogram(name, hi);
    if(hi2) {
      hi2->SetLineColor(color);
      hi2->Draw(draws.c_str());
    }
    return hi2;
  }

  TGraph* draw_graph(const std::string& title, const std::vector<std::array<double, 2> >& ppts, bool do_draw) {
    TGraph* np_gr = new TGraph(ppts.size());
    np_gr->SetTitle(title.c_str());
    for (unsigned i = 0; i < ppts.size(); ++i)
      np_gr->SetPoint(i, ppts[i][0], ppts[i][1]);
    if(do_draw) {
      create_canvas(title);
      np_gr->Draw("ALP");
    }
    return np_gr;
  }

  TGraph* draw_graph(const Vector<double>& xvals, const Vector<double>& yvals, const std::string& title, bool do_draw) {
    auto* np_gr = create_graph(xvals, yvals, title);
    if(do_draw) {
      create_canvas(title);
      np_gr->Draw("ALP");
    }
    return np_gr;
  }

  TGraph* create_graph(const std::vector<double>& xvals, const std::vector<double>& yvals, const std::string& title) {
    if(xvals.size() != yvals.size()) {
      std::cout<<"create_graph() Invalid number of points "<<xvals.size()<<" != "<<yvals.size()<<std::endl;
      return 0;
    }
    TGraph* res = new TGraph(xvals.size());
    res->SetName(title.c_str());
    for(unsigned i=0; i<xvals.size(); ++i) {
      res->SetPoint(i, xvals[i], yvals[i]);
    }
    return res;
  }

  TGraph* create_graph(const Vector<double>& xvals, const Vector<double>& yvals, const std::string& title) {
    if(xvals.size() != yvals.size()) {
      std::cout<<"create_graph() Invalid number of points "<<xvals.size()<<" != "<<yvals.size()<<std::endl;
      return 0;
    }
    TGraph* res = new TGraph(xvals.size());
    res->SetName(title.c_str());
    for(unsigned i=0; i<xvals.size(); ++i) res->SetPoint(i, xvals[i], yvals[i]);
    return res;
  }

  TGraphErrors* create_graph(const Vector<double>& xvals, const SampleStats& yvals, const std::string& title) {
    if(xvals.size() != yvals.size()) {
      std::cout<<"create_graph() Invalid number of points "<<xvals.size()<<" != "<<yvals.size()<<std::endl;
      return 0;
    }
    TGraphErrors* res = new TGraphErrors(xvals.size());
    res->SetName(title.c_str());
    for(unsigned i=0; i<xvals.size(); ++i) {
      res->SetPoint(i, xvals[i], yvals[i].mean());
      if(yvals.mean_and_stddev()) res->SetPointError(i, 0.0, yvals[i].stddev());
      else res->SetPointError(i, 0.0, yvals[i].mean_unc());
    }
    return res;
  }

  TGraphErrors* create_graph(const Vector<double>& xvals, const Vector<double>& yvals, const Vector<double>& yuncvals, const std::string& title) {
      if((xvals.size() != yvals.size()) || (xvals.size() != yuncvals.size())) {
        std::cout<<"create_graph() Invalid number of points "<<xvals.size()<<" != "<<yvals.size()<<std::endl;
        return 0;
      }
      TGraphErrors* res = new TGraphErrors(xvals.size());
      res->SetName(title.c_str());
      for(unsigned i=0; i<xvals.size(); ++i) {
        res->SetPoint(i, xvals[i], yvals[i]);
        res->SetPointError(i, 0.0, yuncvals[i]);
      }
      return res;
    }

  TGraph* create_graph(const Matrix<double>& xvals, unsigned idx, const std::vector<double>& yvals) {
    std::vector<double> xpts(xvals.rows(), 0.0);
    for(unsigned i=0; i<xvals.rows(); ++i) xpts[i] = xvals(i,idx);
    return create_graph(xpts, yvals);
  }

  TH1D* set_visible_range_y(TH1D* hi, double ymin, double ymax) {
    hi->GetYaxis()->SetRangeUser(ymin, ymax);
    return hi;
  }

  TLine* draw_horizontal_line(TH1D* hi, double y, int col) {
    double xmin(0.0), xmax(0.0);
    if(hi) {
      xmin = hi->GetXaxis()->GetXmin();
      xmax = hi->GetXaxis()->GetXmax();
    }
    else {
      gPad->Update();
      xmin = gPad->GetUxmin();
      xmax= gPad->GetUxmax();
    }
    TLine* l = new TLine(xmin, y, xmax, y);
    l->SetLineColor(col);
    l->Draw();
    return l;
  }

  TLine* draw_vertical_line(TH1D* hi, double x, int col) {
    double ymin(0.0), ymax(0.0);
    if(hi) {
      ymin = hi->GetYaxis()->GetXmin();
      ymax = hi->GetYaxis()->GetXmax();
    }
    else {
      gPad->Update();
      ymin = gPad->GetUymin();
      ymax= gPad->GetUymax();
    }
    TLine* l = new TLine(x, ymin, x, ymax);
    l->SetLineColor(col);
    l->Draw();
    return l;
  }

  TH1D* create_histogram(const std::string& title, unsigned nbins, double xmin, double xmax) {
    return new TH1D(title.c_str(), title.c_str(), nbins, xmin, xmax);
  }

  TH2D* create_histogram_2d(const std::string& title, unsigned nbinsx, double xmin, double xmax, unsigned nbinsy, double ymin, double ymax, const std::string& xtitle, const std::string& ytitle) {
    auto *hi =  new TH2D(title.c_str(), title.c_str(), nbinsx, xmin, xmax, nbinsy, ymin, ymax);
    hi->GetXaxis()->SetTitle(xtitle.c_str());
    hi->GetYaxis()->SetTitle(ytitle.c_str());
    return hi;
  }

  TH1D* create_hi(const SampleStats& stat, const std::string& title, bool center_bins, bool mean_unc) {
    const unsigned npar(stat.size());
    TH1D* hi = 0;
    if(center_bins) hi = new TH1D(title.c_str(), title.c_str(), npar, -0.5, npar - 0.5);
    else hi = new TH1D(title.c_str(), title.c_str(), npar, 0, npar);

    for(unsigned p=0; p<npar; ++p) {
      hi->SetBinContent(p+1, stat[p].mean());
      hi->SetBinError(p+1, mean_unc ? stat[p].mean_unc() : stat[p].stddev());
    }
    return hi;
  }

  TCanvas* create_canvas(const std::string& title) {
    if(title.empty()) {
      std::cout<<"create_canvas() ERROR no title"<<std::endl;
      return 0;
    }
    TCanvas* tc = new TCanvas(title.c_str(), title.c_str());
    tc->cd();
    return tc;
  }

  TCanvas* create_canvas(const std::string& title, int width, int height) {
    TCanvas* tc = new TCanvas(title.c_str(), title.c_str(), width, height);
    tc->cd();
    return tc;
  }

  void save_canvas(TCanvas* tc, const std::string& fname) {
    tc->cd();
    tc->Update();
    tc->Print(fname.c_str());
  }

  TH1D* fill_histogram(TH1D* hi, double x, double w) {
    hi->Fill(x, w);
    return hi;
  }

  TH2D* fill_histogram_2d(TH2D* hi, double x, double y, double w) {
    hi->Fill(x, y, w);
    return hi;
  }

  TH1D* draw_hi(TH1D* hi, int col, double msize, int mstyle, const char* draws) {
    hi->SetLineColor(col);
    hi->SetMarkerColor(col);
    hi->SetMarkerSize(msize);
    hi->SetMarkerStyle(mstyle);
    hi->Draw(draws);
    return hi;
  }

  bool copy_sym_matrix(const TMatrixD& A, TMatrixDSym& B) {
    const int p = A.GetNrows();
    if((p != A.GetNcols()) || (p != B.GetNrows())) {
      std::cout<<"copy_sym_matrix() Invalid matrix dimensions: "<<p<<" cols: "<<A.GetNcols()<<" target rows: "<<B.GetNrows()<<std::endl;
      return false;
    }
    bool all_ok = true;
    for(int r=0; r<p; ++r) {
      for(int c=0; c<=r; ++c) {
        double ad = fabs(A(r, c) - A(c, r));
        double ra = fabs((A(r, c) - A(c, r))/A(r, c));
        if((ad > 1e-6) && (ra > 1e-6)) {
          std::cout<<"copy_sym_matrix() Error: A "<<A(r, c)<<" != "<<A(c, r)<<" ra: "<<ra<<" ad: "<<ad<<std::endl;
          all_ok = false;
        }
        B(r, c) = B(c, r) = A(r, c);
      }
    }
    return all_ok;
  }

  bool copy_sym_matrix(const TMatrixD& A, MatrixSym<double>& B) {
    bool debug(false);
    const int p = A.GetNrows();
    if((p != A.GetNcols()) || (p != (int)B.rows())) {
      std::cout<<"copy_sym_matrix() Invalid matrix dimensions: "<<p<<" cols: "<<A.GetNcols()<<" target rows: "<<B.rows()<<std::endl;
      return false;
    }
    bool all_ok = true;
    unsigned nerrors(0);
    for(int r=0; r<p; ++r) {
      for(int c=0; c<=r; ++c) {
        double ad = fabs(A(r, c) - A(c, r));
        double ra = fabs((A(r, c) - A(c, r))/A(r, c));
        if((ad > 1e-6) && (ra > 1e-6)) {
          if(debug) {
            std::cout<<"copy_sym_matrix() Error: A "<<A(r, c)<<" != "<<A(c, r)<<" ra: "<<ra<<" ad: "<<ad<<std::endl;
          }
          ++nerrors;
          all_ok = false;
        }
        B(r, c) = B(c, r) = A(r, c);
      }
    }
    if(nerrors) {
      std::cout<<"copy_sym_matrix() Error: the matrix is not symmetric, "<<nerrors<<" differences"<<std::endl;
      //A.Print();
    }
    return all_ok;
  }

  bool copy_sym_matrix(const MatrixSym<double>& A, TMatrixDSym& B, unsigned offset) {
    const unsigned p = B.GetNrows();
    if((p + offset) != A.rows()) {
      std::cout<<"copy_sym_matrix() Invalid matrix dimensions: "<<A.rows()<<" target rows: "<<B.GetNrows()<<" offset: "<<offset<<std::endl;
      return false;
    }
    for(unsigned r=0; r<p; ++r) {
      B(r, r) = A(r + offset, r + offset);
      for(unsigned c=0; c<r; ++c) {
        B(r, c) = B(c, r) = A(r + offset, c + offset);
      }
    }
    return true;
  }

  void adjust_range(TCanvas* can, const std::vector<TH1D*>& hists) {
    double maxv(0.0);
    for(auto* his : hists) maxv = max(maxv, his->GetMaximum());
    for(auto* his : hists) his->SetMaximum(1.1*maxv);
    can->RedrawAxis();
  }

  void adjust_range(TCanvas* can, const std::vector<TGraph*>& grs) {
    double maxv(-1e10), minv(1e10);
    double x,y;
    for(auto* his : grs) {
      unsigned N(his->GetN());
      for(unsigned i=0; i<N; ++i) {
        his->GetPoint(i, x, y);
        maxv = max(maxv, y);
        minv = min(minv, y);
      }
    }
    for(auto* his : grs) {
      his->SetMaximum(maxv < 0 ? 0.9*maxv : 1.1*maxv);
      his->SetMinimum(minv < 0 ? 1.1*minv : 0.9*minv);
    }
    can->RedrawAxis();
  }

  void
  init_style() {
    gStyle->SetPadTopMargin(0.05);
    gStyle->SetPadRightMargin(0.05);
    gStyle->SetPadLeftMargin(0.18);
    gStyle->SetPadBottomMargin(0.15);
    // gStyle->SetTitleFontSize(0.0);
    gStyle->SetLabelSize(0.06, "X");
    gStyle->SetLabelSize(0.06, "Y");
    gStyle->SetOptStat(0);
    gStyle->SetOptTitle(0);
    gStyle->SetTitleSize(0.06, "X");
    gStyle->SetTitleSize(0.06, "Y");

    gStyle->SetTitleOffset(1.1, "X");
    gStyle->SetTitleOffset(1.2, "Y");

    TH1::SetDefaultSumw2(kTRUE);
  }

  void plot_histograms(const std::vector<TH1D*>& his, const std::string& title) {
    create_canvas(title);
    his[0]->Draw("");
    for(unsigned i=1; i<his.size(); ++i) his[i]->Draw("SAME");
  }

  void plot_histograms(const std::vector<TH1D*>& his, const std::vector<int>& colors, const std::string& title) {
    for(unsigned i=0; i<his.size(); ++i) his[i]->SetLineColor(colors[i]);
    plot_histograms(his, title);
  }

  void plot_histograms(const std::vector<TH1D*>& his) {
    for(auto hb(his.begin()), he(his.end()); hb != he; ++hb) {
      auto* histo = *hb;
      TCanvas* cc = new TCanvas(histo->GetTitle(), histo->GetTitle());
      cc->cd();
      histo->Draw();
    }
  }
  void plot_histograms(const std::vector<TH2D*>& his) {
    for(auto hb(his.begin()), he(his.end()); hb != he; ++hb) {
      auto* histo = *hb;
      TCanvas* cc = new TCanvas(histo->GetTitle(), histo->GetTitle());
      cc->cd();
      histo->Draw("LEGO");
    }
  }

  TH1D*
  draw_histo_clone(TFile* f, const TString& hiname, int color, TLegend* leg, const TString& legtitle) {
    TH1D* hi = (TH1D*)f->Get(hiname);
    if(hi) {
      hi = (TH1D*)hi->Clone();
      hi->SetDirectory(0);
      hi->SetLineColor(color);
      //    hi_bdt->SetLineStyle(2);
      hi->SetLineWidth(2);
      hi->Draw("SAME");
      if(leg) leg->AddEntry(hi, legtitle);
    }
    else {
      std::cout<<"draw_histo_clone() ERROR: unable to find histogram '"<<hiname<<"' in file '"<<f->GetName()<<"'"<<std::endl;
    }
    return hi;
  }

  void fit_gaus(const Histogram<true,double>& hi, double xmin, double xmax, double& mu, double& mu_e, double& sigma, double& sigma_e, bool iterate) {
    TH1D* hip = create_histogram("",hi);
    fit_gaus(hip, xmin, xmax, mu, mu_e, sigma, sigma_e, iterate);
    delete hip;
  }

  void fit_gaus(TH1D* hi, double xmin, double xmax, double& mu, double& mu_e, double& sigma, double& sigma_e, bool iterate) {
    TF1* gausf = new TF1("gausf", "gaus(0)",xmin, xmax);
    auto res = hi->Fit(gausf, "QLSR");
    if(res->IsValid()) {
      mu = gausf->GetParameter(1);
      sigma = gausf->GetParameter(2);
      mu_e = gausf->GetParError(1);
      sigma_e = gausf->GetParError(2);
      if(iterate) {
        gausf->SetRange(mu - 3.0*sigma, mu + 3.0*sigma);
        res = hi->Fit(gausf, "QLSR");
        mu = gausf->GetParameter(1);
        sigma = gausf->GetParameter(2);
        mu_e = gausf->GetParError(1);
        sigma_e = gausf->GetParError(2);
      }
    }
    else {
      std::cout<<"fit_gaus invalid fit of "<<hi->GetName()<<std::endl;
      mu = sigma = 0.0;
      mu_e = sigma_e = 0.0;
    }
    delete gausf;
  }


  void draw_graph(TGraphErrors* gr, const std::string& title, const std::string& draws, const std::string& xtitle, const std::string& ytitle) {
    auto*can = create_canvas(title);
    gr->Draw(draws.c_str());
    gr->GetXaxis()->SetTitle(xtitle.c_str());
    gr->GetYaxis()->SetTitle(ytitle.c_str());
    can->Update();
  }

  TGraphErrors* draw_graph(TGraphErrors *gr, unsigned style, unsigned col, const char* draw, const char* title, TLegend* leg) {
    gr->SetMarkerSize(2);
    gr->SetMarkerStyle(style);
    gr->SetMarkerColor(col);
    gr->Draw(draw);
    leg->AddEntry(gr, title);
    return gr;
  }

  std::pair<double, double> draw_graph_errors(TCanvas *tc, TGraphErrors* gr, const std::string& title, const char* draw, int col, int style, TLegend* leg, int idx, double _dx) {
    const double dx = 0.3*_dx;
    tc->cd();
    const unsigned N(gr->GetN());
    double ymin(1e10), ymax(-1e10);
    double xv, yv;
    for(unsigned n=0; n<N; ++n) {
      gr->GetPoint(n, xv, yv);
      gr->SetPoint(n, xv + double(idx)*dx, yv);
      ymin = min(ymin, yv - gr->GetErrorY(n));
      ymax = max(ymax, yv + gr->GetErrorY(n));
    }
    gr->SetLineWidth(2);
    gr->SetLineColor(col);
    gr->SetMarkerSize(2);
    gr->SetMarkerStyle(style);
    gr->SetMarkerColor(col);
    gr->Draw(draw);
    if(leg) leg->AddEntry(gr, title.c_str());
    return std::make_pair(ymin, ymax);
  }


  bool
  compute_eigenvalues(const MatrixSym<double>& cov, Vector<double>& eigenv) {
    eigenv.rows(cov.rows());
    TMatrixDSym rcov(cov.rows());
    copy_sym_matrix(cov, rcov);
    TMatrixDSymEigen eigen(rcov);

    auto &reigenv = eigen.GetEigenValues();
    for(unsigned i=0; i<cov.rows(); ++i) eigenv(i) = reigenv(i);
    return true;
  }

  rope_invert_result
  compute_svd_inverse_rope(const MatrixSym<double>& cov, Vector<double>& eigenv, MatrixSym<double>& icov, double rho, Vector<double>& original_eigenv, bool prune_eigen) {
    rope_invert_result res{false};

    const unsigned N(cov.rows());
    eigenv.rows(N);
    icov.rows(N);
    original_eigenv.rows(N);

    TMatrixDSym rcov(N);
    copy_sym_matrix(cov, rcov, 0);
    TDecompSVD svd(rcov, 0.0);
    if(!svd.Decompose()) {
      std::cout<<"compute_svd_inverse_rope() Error: unable to SVD"<<std::endl;
      return false;
    }
    auto& _U = svd.GetU();
    auto& _V = svd.GetV();
    auto& sv = svd.GetSig();
    TMatrixD Si(N, N);

    const double eigen_limit = sqrt(0.001*8.0*rho);

    size_t Neigen(1);
    double odet(1.0), det(1.0);
    for(unsigned i=0; i<N; ++i) {
      if(prune_eigen && (sv[i]/sv[0] < eigen_limit)) {
        eigenv(i) = 0;
        Si(i,i) = 0.0;
      }
      else {
        eigenv(i) = 2.0/(sv[i] + sqrt(square(sv[i]) + 8.0*rho));
        Si(i,i) = eigenv(i);
        det *= eigenv(i);
        Neigen = i+1;
      }
      original_eigenv(i) = sv[i];
      odet *= original_eigenv(i);
    }

    MatrixSqr<double> U(N, (double*)_U.GetMatrixArray(), N);
    MatrixSqr<double> V(N, (double*)_V.GetMatrixArray(), N);
    MatrixDia<double> D(eigenv);
    MatrixSqr<double> Ai(N);
    V.lrmult(D, U, Ai);

    eigenv.rows(Neigen);

    for(unsigned r=0; r<N; ++r) {
      for(unsigned c=0; c<=r; ++c) {
        double d = std::abs((Ai(r,c) - Ai(c,r))/Ai(r,c));
        res.max_asymm = std::max(res.max_asymm, d);
        icov(r,c) = Ai(r,c);
      }
    }
    if(res.max_asymm > 1e-3) {
      std::cout<<"compute_svd_inverse_rope() Error: assymmetry!"<<std::endl;
    }
    res.nvalues = Neigen;
    res.det = det;
    res.orig_det = odet;
    return res;
  }

}
