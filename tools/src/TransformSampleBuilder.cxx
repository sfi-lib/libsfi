/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <sample_functions.h>
#include "Sample.h"
#include "SampleBuilderBase.h"
#include "cosine.h"
#include "full_system.h"
#include "Transform.h"

namespace sfi {
  class TransformSampleBuilder : public SampleBuilderBase {
  public:
    TransformSampleBuilder(Context& ctx, OptionMap&);

    virtual ~TransformSampleBuilder();

    // ---- implementation of ISampleBuilder ----

    virtual ISample* do_build(Context& ctx, const PVariables& vars, const DataSource& src_data);
  };
}

using namespace sfi;
using namespace std;

extern "C" {
  sfi::IComponent* sfi_factory_TransformBuilder(Context& ctx, OptionMap& opts) {
    return new sfi::TransformSampleBuilder(ctx, opts);
  }
}

TransformSampleBuilder::TransformSampleBuilder(Context& ctx, OptionMap& opts):SampleBuilderBase(ctx, opts) {
  m_is_weighted = false;
}

TransformSampleBuilder::~TransformSampleBuilder() { }

// ---- implementation of ISampleBuilder ----

ISample*
TransformSampleBuilder::do_build(Context& ctx, const PVariables& vars, const DataSource& src_data) {
  ISample* res = this->create_sample(ctx, m_dims, m_is_weighted, src_data.nevents());
  if(!res) {
    do_log_error("do_build() No sample generated!");
    return 0;
  }
  res->set_variables(vars);
  auto& op = src_data.options();
  if(!op.has_sub("transform")) {
    do_log_error("do_build() Option 'transform' missing");
    return 0;
  }
  auto& trf_op = op.sub("transform");
  if(trf_op.get_as<std::string>("eigensystem.basis") == bases::Cosine::name()) {
    t_Transform<tdimensions<1>,EigenFullTensor,bases::Cosine> trf(trf_op);
    trf.variables()->lock();
    if(vars->compare(*trf.variables())) {
      trf.normalize(1.0);
      sample_random_1d(dynamic_cast<Sample<1>*>(res), trf, src_data.nevents(), ctx.rnd_gen());
    }
    else {
      do_log_error("do_build() Variables mismatch. "<<*vars<<" : "<<*trf.variables());
      return 0;
    }
  }
  return res;
}
