/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "TransformPlot.h"
#include "ITransform.h"
#include "Variables.h"
#include "ext_utils.h"

#include <TH1D.h>
#include <TH2D.h>
#include <TLegend.h>
#include <TGraph.h>
#include <TGraph2D.h>
#include <TCanvas.h>
#include <THStack.h>
#include <TStyle.h>
#include <TF2.h>

using namespace sfi;

TransformPlotBase::TransformPlotBase(IEvaluator* ev):Plot(),
    m_eval(ev), m_transform( (ev ? ev->get_transform() : 0) ),
    m_external_coordinates(false), m_scale_to_bin(true), m_superimpose(false), m_owns_evaluator(false),
    m_scale(1.0), m_fill_color(-1), m_marker_size(2.0),
    m_minimum(std::nan("")), m_maximum(std::nan("")), m_draw_string(""), m_style("simple") {
}

TransformPlotBase::TransformPlotBase(const ITransform* tr):Plot(),
    m_eval(0), m_transform(tr),
    m_external_coordinates(false), m_scale_to_bin(true), m_superimpose(false), m_owns_evaluator(false),
    m_scale(1.0), m_fill_color(-1), m_marker_size(2.0),
    m_minimum(std::nan("")), m_maximum(std::nan("")), m_draw_string(""), m_style("simple") {
}

TransformPlotBase::TransformPlotBase(IEvaluator* ev, const OptionMap& opts):TransformPlotBase(ev) {
  init(opts);
}

TransformPlotBase::TransformPlotBase(const ITransform* tr, const OptionMap& opts):TransformPlotBase(tr) {
  init(opts);
}

TransformPlotBase::TransformPlotBase(const OptionMap& opts):TransformPlotBase((ITransform*)0) {
  init(opts);
}

TransformPlotBase::TransformPlotBase(const TransformPlotBase& o):Plot(o),
  m_eval(o.m_eval), m_transform(o.m_transform),
  m_external_coordinates(o.m_external_coordinates), m_scale_to_bin(o.m_scale_to_bin), m_superimpose(o.m_superimpose), m_owns_evaluator(o.m_owns_evaluator),
  m_scale(o.m_scale), m_fill_color(o.m_fill_color), m_marker_size(o.m_marker_size),
  m_minimum(o.m_minimum), m_maximum(o.m_maximum), m_draw_string(o.m_draw_string), m_style(o.m_style) {
}

TransformPlotBase::~TransformPlotBase() {
  if(m_owns_evaluator && m_eval) {
    delete m_eval;
    m_eval = 0;
  }
}

void TransformPlotBase::init(const OptionMap& opts) {
  Plot::init(opts);
  opts.get_if("external_coordinates", m_external_coordinates);
  opts.get_if("scale_to_bin", m_scale_to_bin).get_if("superimporse", m_superimpose).get_if("scale", m_scale);
  opts.get_if("fill_color", m_fill_color);
  opts.get_if("marker_size", m_marker_size);
  opts.get_if("canvas", m_canvas).get_if("minimum", m_minimum).get_if("maximum", m_maximum);
  opts.get_if("draw_string", m_draw_string).get_if("style", m_style);
}

void TransformPlotBase::copy_from(const TransformPlotBase& o) {
  Plot::copy_from(o);
  m_eval = o.m_eval; m_transform = o.m_transform;
  m_external_coordinates = o.m_external_coordinates; m_scale_to_bin = o.m_scale_to_bin;
  m_superimpose = o.m_superimpose; m_owns_evaluator = o.m_owns_evaluator;
  m_scale = o.m_scale; m_fill_color = o.m_fill_color;
  m_marker_size = o.m_marker_size;
  m_minimum = o.m_minimum; m_maximum = o.m_maximum; m_draw_string = o.m_draw_string; m_style = o.m_style;
}

IEvaluator*
TransformPlotBase::evaluator() {
  // TODO: Use the unique ptr
  if(!m_eval && m_transform) {
    m_eval = m_transform->get_evaluator(1.0, m_external_coordinates).release();
    m_owns_evaluator = true;
  }
  return m_eval;
}

TransformPlot1D::TransformPlot1D(Context& ctx):TransformPlotBase((IEvaluator*)0), m_log(Log::get(ctx, "TransformPlot1D")), m_npts(100) { }

TransformPlot1D::TransformPlot1D(Context& ctx, const OptionMap& opts):TransformPlotBase((IEvaluator*)0, opts), m_log(Log::get(ctx, "TransformPlot1D")), m_npts(100) {
  init(opts);
}

TransformPlot1D::TransformPlot1D(Context& ctx, IEvaluator* ev):TransformPlotBase(ev), m_log(Log::get(ctx, "TransformPlot1D")), m_npts(100) { }

TransformPlot1D::TransformPlot1D(Context& ctx, const ITransform& tr):TransformPlotBase(&tr), m_log(Log::get(ctx, "TransformPlot1D")), m_npts(100) { }

TransformPlot1D::TransformPlot1D(Context& ctx, const ITransform* tr):TransformPlotBase(tr), m_log(Log::get(ctx, "TransformPlot1D")), m_npts(100) { }

TransformPlot1D::TransformPlot1D(Context& ctx, IEvaluator* ev, const OptionMap& opts):TransformPlotBase(ev, opts), m_log(Log::get(ctx, "TransformPlot1D")), m_npts(100) {
  init(opts);
}

TransformPlot1D::TransformPlot1D(Context& ctx, const ITransform& tr, const OptionMap& opts):TransformPlotBase(&tr, opts), m_log(Log::get(ctx, "TransformPlot1D")), m_npts(100) {
  init(opts);
}

TransformPlot1D::TransformPlot1D(Context& ctx, const ITransform* tr, const OptionMap& opts):TransformPlotBase(tr, opts), m_log(Log::get(ctx, "TransformPlot1D")), m_npts(100) {
  init(opts);
}

TransformPlot1D::TransformPlot1D(Context& ctx, const TransformPlot1D& o):
    TransformPlotBase(o), m_log(Log::get(ctx, "TransformPlot1D")), m_npts(o.m_npts) {
}

TransformPlot1D::TransformPlot1D(Context& ctx, const std::vector<TransformPlot1D>& plts):
    TransformPlotBase((IEvaluator*)0), m_log(Log::get(ctx, "TransformPlot1D")), m_npts(100), m_subevals(plts) {
}

TransformPlot1D::~TransformPlot1D() {}

bool TransformPlot1D::draw(bool first) {
  superimpose(!first);
  return plot() != nullptr;
}

TransformPlot1D&
TransformPlot1D::operator=(const TransformPlot1D& o) {
  if(this != &o) {
    TransformPlotBase::copy_from(o);
    m_npts = o.m_npts;
  }
  return *this;
}

TGraph*
TransformPlot1D::plot() {
  if((m_transform == 0) && (m_eval == 0) && (m_subevals.empty())) {
    do_log_error("plot() Must have either transform or evaluator or subplots");
    return 0;
  }
  std::string draws;
  if(m_style == "filled") draws="LF";
  else draws = "L";
  if(m_marker_style > 0) draws += "P";

  // TODO: Handle multi-dim transforms
  // TODO: Allow this to be changed!
  const unsigned varnr = 0;
  Vector<double> xvals(m_npts);

  if(!m_subevals.empty()) {
    bool superimp = m_superimpose;
    for(unsigned i=m_subevals.size(); i>0; --i) {
      auto& p = m_subevals[i-1];
      p.scale_to_bin(m_scale_to_bin).npoints(npoints()).external_coordinates(external_coordinates()).superimpose(superimp);
      superimp = true;
    }
    auto &p0 = m_subevals[0];
    IEvaluator* eval = p0.evaluator();
    const Variable& var = eval->get_variable(varnr);
    double xscale;
    create_xvalues_and_scale(var, xvals, xscale);

    // TODO: Look if the canvas is already created
    if(!m_canvas.empty()) create_canvas(m_canvas);
    for(unsigned i=0; i<m_subevals.size(); ++i) {
      auto& p = m_subevals[i];
      p.eval(xvals, xscale);
      if(i) p.m_yvals += m_subevals[i-1].m_yvals;
    }
    if(m_style == "filled") {
      xvals.rows(xvals.rows()+2);
      xvals[xvals.rows()-1] = xvals[0];
      xvals[xvals.rows()-2] = xvals[xvals.rows()-3];
    }
    for(unsigned i=m_subevals.size(); i>0; --i) {
      auto& p = m_subevals[i-1];
      if(m_style == "filled") {
        p.m_yvals.rows(p.m_yvals.rows()+2);
        p.m_yvals[p.m_yvals.rows()-1] = p.m_yvals[p.m_yvals.rows()-2] = 0;
      }
      p.make_plot(xvals, var, draws);
    }

    gPad->Update();

    return 0;
  }
  else {
    IEvaluator* eval = evaluator();
    const Variable& var = eval->get_variable(varnr);

    double xscale;
    create_xvalues_and_scale(var, xvals, xscale);
    this->eval(xvals, xscale);

    // TODO: Look if the canvas is already created
    if(!m_canvas.empty()) create_canvas(m_canvas);
    auto* gr = make_plot(xvals, var, draws);
    gPad->Update();
    do_log_debug("plot() "<<m_transform->get_name()<<" "<<m_transform->get_integral());
    return gr;
  }
}

void
TransformPlot1D::init(const OptionMap& om) {
  om.get_if("npoints",m_npts).get_if("nbins",m_npts);
}

TGraph*
TransformPlot1D::make_plot(const Vector<double>& xvals, const Variable& var, const std::string& _draws) {
  std::string draws = _draws.empty() ? m_draw_string : _draws;
  if(m_superimpose) draws += " SAME";
  else draws = "A"+draws;

  TGraph* gr = create_graph(xvals, m_yvals, m_title);
  gr->SetTitle((";"+var.name()).c_str());
  gr->SetLineColor(m_color);
  gr->SetMarkerColor(m_color);
  if(m_fill_color > 0) gr->SetFillColor(m_fill_color);
  gr->SetMarkerStyle(m_marker_style);
  gr->SetMarkerSize(m_marker_size);
  gr->SetLineWidth(m_line_size);
  if(std::isfinite(m_minimum)) gr->SetMinimum(m_minimum);
  if(std::isfinite(m_maximum)) gr->SetMaximum(m_maximum);

  gr->Draw(draws.c_str());
  gr->GetXaxis()->SetTitle((var.title().empty() ? var.name() : var.title()).c_str());
  return gr;
}

void
TransformPlot1D::create_xvalues_and_scale(const Variable& var, Vector<double>& xvals, double& xscale) {
  const double dx = (m_external_coordinates ? (var.maxval() - var.minval()) : (var.internal_maxval() - var.internal_minval()))/double(m_npts);
  xscale = (m_scale_to_bin ? dx : 1.0);
   // TODO: allow internal to be other than -1->1
  double xv = m_external_coordinates ? var.minval() : var.internal_minval();
  for(unsigned i=0; i<m_npts; ++i, xv += dx) xvals[i] = xv + 0.5*dx;
}

void
TransformPlot1D::eval(const Vector<double>& xvals, double xscale) {
  m_eval->set_scale(xscale*m_scale);
  m_yvals.rows(m_npts);
  m_eval->do_eval(m_npts, xvals.data_ptr(), m_yvals.data_ptr());
}

TransformPlot2D::TransformPlot2D(Context& ctx, IEvaluator* ev):TransformPlotBase(ev), m_log(Log::get(ctx, "TransformPlot2D")),
    m_npts_x(20), m_npts_y(20), m_swap_axes(false), m_bin_center(true), m_use_function(false) {
}

TransformPlot2D::TransformPlot2D(Context& ctx, const ITransform& tr):TransformPlotBase(&tr), m_log(Log::get(ctx, "TransformPlot2D")),
    m_npts_x(20), m_npts_y(20), m_swap_axes(false), m_bin_center(true), m_use_function(false) {
}

TransformPlot2D::TransformPlot2D(Context& ctx, const ITransform* tr):TransformPlotBase(tr), m_log(Log::get(ctx, "TransformPlot2D")),
    m_npts_x(20), m_npts_y(20), m_swap_axes(false), m_bin_center(true), m_use_function(false) {
}

TransformPlot2D::TransformPlot2D(Context& ctx, IEvaluator* ev, const OptionMap& opts):TransformPlotBase(ev, opts), m_log(Log::get(ctx, "TransformPlot2D")),
    m_npts_x(20), m_npts_y(20), m_swap_axes(false), m_bin_center(true), m_use_function(false) {
  init(opts);
}

TransformPlot2D::TransformPlot2D(Context& ctx, const ITransform& tr, const OptionMap& opts):TransformPlotBase(&tr, opts), m_log(Log::get(ctx, "TransformPlot2D")),
    m_npts_x(20), m_npts_y(20), m_swap_axes(false), m_bin_center(true), m_use_function(false) {
  init(opts);
}

TransformPlot2D::TransformPlot2D(Context& ctx, const ITransform* tr, const OptionMap& opts):TransformPlotBase(tr, opts), m_log(Log::get(ctx, "TransformPlot2D")),
    m_npts_x(20), m_npts_y(20), m_swap_axes(false), m_bin_center(true), m_use_function(false) {
  init(opts);
}

TransformPlot2D::TransformPlot2D(Context& ctx, const OptionMap& opts):TransformPlotBase(opts), m_log(Log::get(ctx, "TransformPlot2D")),
    m_npts_x(20), m_npts_y(20), m_swap_axes(false), m_bin_center(true), m_use_function(false) {
  init(opts);
}

TransformPlot2D::~TransformPlot2D() {}

bool TransformPlot2D::draw(bool first) {
  superimpose(!first);
  return plot("") != nullptr;
}

TGraph2D*
TransformPlot2D::plot(const std::string& _draws) {
  if((m_transform == 0) && (m_eval == 0)) {
    do_log_error("plot() Must have either transform or evluator");
    return 0;
  }
  auto vars = m_transform->get_variables();
  if(!vars) {
    do_log_error("plot() transform '"<<m_transform->get_name()<<"' has no variables");
    return 0;
  }
  do_log_debug("TransformPlot2d "<<m_transform->get_type_name()<<" deg: "<<m_transform->get_var_degree(0)<<" "<<m_transform->get_var_degree(1));
  if(m_log.debug()) m_transform->do_dump();

  std::string draws = _draws.empty() ? m_draw_string : _draws;
  if(draws.empty()) draws = "SURF";
  if(m_superimpose) draws += " SAME";

  // TODO: Really use the unique ptr
  IEvaluator* eval(0);
  if(m_eval) {
    eval = m_eval;
    eval->set_external_coordinates(m_external_coordinates);
    eval->set_scale(m_scale);
  }
  else eval = m_transform->get_evaluator(m_scale, m_external_coordinates).release();

  const Variable& varx = eval->get_variable(0);
  const Variable& vary = eval->get_variable(1);

  const size_t Npts = m_npts_x*m_npts_y;
  const unsigned Ndim = eval->get_dimensions();

  if((m_extra_values.size() + 2) != Ndim) {
    do_log_error("plot() transform '"<<m_transform->get_name()<<"' has "<<eval->get_dimensions()<<" dimensions, only "<<m_extra_values.size()<<" extra values provided");
    if(eval && !m_eval) {
      delete eval;
    }
    return 0;
  }
  double dx, dy, offsx, offsy;
  if(m_bin_center) {
    dx = (m_external_coordinates ? (varx.maxval() - varx.minval()) : (varx.internal_maxval() - varx.internal_minval()))/double(m_npts_x);
    dy = (m_external_coordinates ? (vary.maxval() - vary.minval()) : (vary.internal_maxval() - vary.internal_minval()))/double(m_npts_y);
    offsx = 0.5*dx;
    offsy = 0.5*dy;
  }
  else {
    dx = (m_external_coordinates ? (varx.maxval() - varx.minval()) : (varx.internal_maxval() - varx.internal_minval()))/double(m_npts_x-1);
    dy = (m_external_coordinates ? (vary.maxval() - vary.minval()) : (vary.internal_maxval() - vary.internal_minval()))/double(m_npts_y-1);
    offsx = 0;
    offsy = 0;
  }

  eval->set_scale(m_scale*(m_scale_to_bin ? dx*dy : 1.0));

  if(m_use_function) {
    plot_function(draws, eval);
    return 0;
  }

  std::vector<double> xvals(Npts * Ndim, 0.0), yvals(Npts, 0.0);
  double xp, yp;
  for(int k=0; k<m_npts_x; ++k) {
    xp = (m_external_coordinates ? varx.minval() : varx.internal_minval()) + double(k)*dx + offsx;
    for(int l=0; l<m_npts_y; ++l) {
      yp = (m_external_coordinates ? vary.minval() : vary.internal_minval()) + double(l)*dy + offsy;
      double* x = &xvals[(l + k*m_npts_y)*Ndim];
      x[0] = xp;
      x[1] = yp;
      for(unsigned i=0; i<m_extra_values.size(); ++i) x[i + 2] = m_extra_values[i];
      // duplicate first value
      /*if((k==0) && !m_bin_center) {
        double* x2 = &xvals[(l + (m_npts_x-1)*m_npts_y)*Ndim];
        for(unsigned i=0; i<Ndim; ++i) x2[i] = x[i];
      }*/
    }
  }
  eval->do_eval(Npts, xvals.data(), yvals.data());

  TGraph2D* gr = new TGraph2D(Npts);
  gr->SetName(m_title.c_str());
  {
    int i(0);
    for(int k=0; k<m_npts_x; ++k) {
      for(int l=0; l<m_npts_y; ++l) {
        double* x = &xvals[(l + k*m_npts_y)*Ndim];
        if(m_swap_axes) gr->SetPoint(i, x[1], x[0], yvals[l + k*m_npts_y]);
        else gr->SetPoint(i, x[0], x[1], yvals[l + k*m_npts_y]);
        ++i;
      }
    }
  }

  gr->SetTitle("");
  gr->SetLineColor(m_color);
  gr->SetMarkerColor(m_color);
  gr->SetMarkerStyle(m_marker_style);
  gr->SetMarkerSize(m_marker_size);
  gr->SetLineWidth(m_line_size);
  if(std::isfinite(m_minimum)) gr->SetMinimum(m_minimum);
  if(std::isfinite(m_maximum)) gr->SetMaximum(m_maximum);

  // TODO: Look if the canvas is already created
  bool pref_gl = gStyle->GetCanvasPreferGL();
  gStyle->SetCanvasPreferGL(true);
  if(!m_canvas.empty()) {
    auto* can = create_canvas(m_canvas);
    can->UseGL();
  }
  gr->Draw(draws.c_str());

  if(m_swap_axes) {
    gr->GetXaxis()->SetTitle((vary.title().empty() ? vary.name() : vary.title()).c_str());
    gr->GetYaxis()->SetTitle((varx.title().empty() ? varx.name() : varx.title()).c_str());
  }
  else {
    gr->GetXaxis()->SetTitle((varx.title().empty() ? varx.name() : varx.title()).c_str());
    gr->GetYaxis()->SetTitle((vary.title().empty() ? vary.name() : vary.title()).c_str());
  }
  gr->GetXaxis()->SetTitleOffset(1.5);
  gr->GetYaxis()->SetTitleOffset(1.5);
  gPad->Update();
  gStyle->SetCanvasPreferGL(pref_gl);
  
  do_log_debug("plot() "<<m_transform->get_name()<<" I="<<m_transform->get_integral()<<" vars: "<<varx.name()<<", "<<vary.name());

  if(eval && !m_eval) {
    delete eval;
  }
  return gr;
}

void
TransformPlot2D::init(const OptionMap& opts) {
  TransformPlotBase::init(opts);
  opts.get_if("npointsx", m_npts_x).get_if("npointsy", m_npts_y).get_if("swap_axes", m_swap_axes).get_if("bin_center", m_bin_center);
  opts.get_if("extra_values", m_extra_values).get_if("use_function", m_use_function);
}

struct _func_ {
  inline double operator() (const double *x, const double *) { return m_eval->get_value(x); }
  IEvaluator* m_eval;
};
void
TransformPlot2D::plot_function(const std::string& draws, IEvaluator* eval) {
  eval = eval->get_clone(true);

  const Variable& varx = eval->get_variable(0);
  const Variable& vary = eval->get_variable(1);

  TF2* fu = 0;
  if(m_external_coordinates) {
    fu = new TF2(eval->get_transform()->get_name().c_str(), ROOT::Math::ParamFunctor(_func_{eval}), varx.minval(), varx.maxval(), vary.minval(), vary.maxval());
  }
  else {
    fu = new TF2(eval->get_transform()->get_name().c_str(), ROOT::Math::ParamFunctor(_func_{eval}), -1, 1, -1, 1);
  }
  if(std::isfinite(m_minimum)) fu->SetMinimum(m_minimum);
  if(std::isfinite(m_maximum)) fu->SetMaximum(m_maximum);

  fu->Draw(draws.c_str());
  gPad->Update();
}
