/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <sample_tools.h>
#include <TTree.h>

namespace sfi {
  TTree* sample_2_tree(const SampleBase& spl) {
    const unsigned nevents(spl.get_events());
    const unsigned dims(spl.get_dim());
    auto vars = spl.variables();
    if(!vars) return 0;

    TTree* tr = new TTree(("tree"+spl.name()).c_str(), ("tree"+spl.name()).c_str());
    double w;
    std::vector<double> x(dims, 0);

    for(unsigned d=0; d<dims; ++d) {
      tr->Branch(vars->name(d).c_str(), &x[d]);
    }

    tr->Branch("w",&w,"w/D");
    for(unsigned i=0; i<nevents; ++i) {
      for(unsigned d=0; d<dims; ++d) x[d] = spl.get_x(i, d);
      w = spl.get_w(i);
      tr->Fill();
    }
    return tr;
  }
}
