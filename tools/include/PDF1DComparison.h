#ifndef sfi_PDF1DComparison_h
#define sfi_PDF1DComparison_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Transform.h"
#include "Sample.h"

class TCanvas;
class TH1D;

namespace sfi {
  class PDF1DComparison {
  public:
    PDF1DComparison(Context& ctx, const std::string& prefix);

    ~PDF1DComparison();

    inline PDF1DComparison& print_level(PrintLevel pl) { m_log.print_level(pl); return *this; }

    void plot();

    inline PDF1DComparison& transforms(TransformSet<ITransform>&& trfs) {
      m_transforms = std::forward<TransformSet<ITransform>>(trfs);
      return *this;
    }

    inline PDF1DComparison& transforms_1D(TransformSet2D<ITransform>&& trfs) {
      m_1D_transforms = std::forward<TransformSet2D<ITransform>>(trfs);
      return *this;
    }

    template<class _SampleSet, class _EvtSel>
    inline PDF1DComparison& samples(const _SampleSet& sset, const _EvtSel& esel) {
      m_histos_int = sset.histograms_1D(m_n_pdf_bins, false, esel);
      if(m_plot_external_coordinates) m_histos_ext = sset.histograms_1D(m_n_pdf_bins, true, esel);
      for(auto& spl : sset) m_samples.push_back(&spl);
      return *this;
    }

    template<class _Sample, class _EvtSel>
    inline PDF1DComparison& sample(const _Sample& spl, const _EvtSel& esel) {
      m_histos_int = {spl.histograms_1D(m_n_pdf_bins, false, esel)};
      if(m_plot_external_coordinates) m_histos_ext = {spl.histograms_1D(m_n_pdf_bins, true, esel)};
      m_samples.push_back(&spl);
      return *this;
    }

    inline PDF1DComparison& prefix(std::string prefix) { m_prefix = prefix; return *this; }

    inline PDF1DComparison& n_pdf_bins(size_t n_pdf_bins) { m_n_pdf_bins = n_pdf_bins; return *this; }

    inline PDF1DComparison& plot_external_coordinates(bool plot_external_coordinates) {
      m_plot_external_coordinates = plot_external_coordinates; return *this;
    }

    inline PDF1DComparison& debug(bool _debug) {
      m_debug = _debug; return *this;
    }

    inline PDF1DComparison& plot_marginalizations(bool _plot_marginalizations) {
      m_plot_marginalizations = _plot_marginalizations; return *this;
    }

    inline std::vector<TCanvas*> canvases() { return m_canvases; }

    inline TransformSet<ITransform>& transforms() { return this->m_transforms; }
    inline TransformSet2D<ITransform>& transforms_1D() { return this->m_1D_transforms; }

    inline PDF1DComparison& file_prefix(const std::string& fname) { m_file_prefix = fname; return *this; }

    inline PDF1DComparison& normalize(bool n) { m_normalize = n; return *this; }
  protected:
    void pdf_comparison(const std::string& prefix, bool external_coordinates);

    using HistogramTy = Histogram<true,double>;
    Log m_log;
    std::string m_prefix, m_file_prefix;
    size_t m_n_pdf_bins;
    bool m_plot_external_coordinates;
    bool m_debug;
    bool m_plot_marginalizations;
    bool m_normalize;
    TransformSet<ITransform> m_transforms;
    TransformSet2D<ITransform> m_1D_transforms;
    std::vector<TCanvas*> m_canvases;
    std::vector<std::vector<HistogramTy> > m_histos_int;
    std::vector<std::vector<HistogramTy> > m_histos_ext;
    std::vector<const ISample*> m_samples;
  };
}

#endif // sfi_PDF1DComparison_h
