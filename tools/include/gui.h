#ifndef sfi_gui_h
#define sfi_gui_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Options.h"
#include "Histogram.h"
#include "stat.h"

class TApplication;
class TCanvas;
class TLegend;

namespace sfi {
  class Canvas;

  class Plot {
  public:
    Plot();

    virtual ~Plot() {}

    inline const std::string& title() const { return m_title; }
    inline Plot& title(const std::string& t) { m_title = t; return *this; }

    inline Plot& color(int c) { m_color = c; return *this; }

    /** Set marker color and style */
    inline Plot& marker(int c, int s) { m_marker_color = c; m_marker_style = s; return *this; }

    inline Plot& line_size(double ls) { m_line_size = ls; return *this; }

    virtual bool draw(bool first) = 0;
  protected:
    Plot(const Plot&);
    Plot(const OptionMap& opts);

    void init(const OptionMap& opts);

    void copy_from(const Plot& o);

    std::string m_title;
    int m_color;
    int m_marker_color, m_marker_style;
    double m_line_size;
  };

  class Canvas {
  public:
    static Canvas& get(const std::string& title);

    inline TCanvas* canvas() { return m_canvas; }

    inline const std::string& title() const { return m_title; }

    inline Canvas& legend(bool l) { m_legend = l; return *this; }

    inline Canvas& axis_title(unsigned a, const std::string& title) {
      m_axis_titles[a] = title; return *this;
    }

    Plot& horizontal_line(double yval);
    Plot& vertical_line(double yval);

    // ---- plot histograms ----

    Plot& draw(const std::string& name, const Histogram<false, double>& hi);
    Plot& draw(const std::string& name, const Histogram<true, double>& hi);

    Plot& draw(const std::string& name, const Histogram2D<false, double>& hi);
    Plot& draw(const std::string& name, const Histogram2D<true, double>& hi);

    // ---- plot graphs ----

    Plot& draw(const Vector<double>& xvals, const Vector<double>& yvals);
    Plot& draw(const std::vector<double>& xvals, const std::vector<double>& yvals);
    Plot& draw(const Vector<double>& xvals, const SampleStats& yvals);
    Plot& draw(const Vector<double>& xvals, const Vector<double>& yvals, const Vector<double>& yuncvals);

    // ---- plot transforms etc. ----
    template<class _Plot> inline _Plot& draw(_Plot* p) {
      add(p);
      return *p;
    }

    Canvas& plot();

    Canvas& save(const std::string& file_name);

    Canvas(Canvas&);
    Canvas(Canvas&&);
    ~Canvas();
  protected:
    Canvas(const std::string& title);
    Canvas(const Canvas&) = delete;

    Plot& add(Plot*);

    std::string m_title;
    TCanvas* m_canvas;
    TLegend* m_legend_ptr;
    std::vector<Plot*> m_plots;
    std::vector<std::string> m_axis_titles;
    unsigned m_drawn;
    bool m_legend;
  };

  class Application {
  public:
    Application(int argc, char** argv, const std::string& conf_file);

    ~Application();

    inline Options& options() { return m_options; }

    bool run();
  protected:
    TApplication* m_app;
    Options m_options;
  };
}

#endif // sfi_gui_h
