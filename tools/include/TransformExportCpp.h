#ifndef sfi_TransformExportCpp_h
#define sfi_TransformExportCpp_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string>

#include "Variables.h"
#include "Log.h"

namespace sfi {
  class ITransform;
  class ISample;

  class TransformExportCpp {
  public:
    using This = TransformExportCpp;

    TransformExportCpp(Context& ctx, const std::string& out_prefix);

    inline unsigned precision() const { return m_precision; }

    inline This& precision(unsigned p) { m_precision = p; return *this; }

    bool do_export(const ITransform& trf);
  protected:
    Log m_log;
    std::string m_out_prefix;
    unsigned m_precision;
  };
}

#endif // sfi_TransformExportCpp_h
