#ifndef sfi_model_plots_h
#define sfi_model_plots_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ModelTransform.h"
#include "TransformPlot.h"

namespace sfi {

  template<class _Transf, unsigned _NDims>
  void plot_systematics_shifts(Context& ctx, const std::string& title, model_transform_result<_Transf, _NDims>& syst_trfs) {
    auto vars = syst_trfs.shift.variants();
    for(auto& varid : vars) {
      double mu_d = syst_trfs.shift.variant_scale(varid, true);
      double mu_u = syst_trfs.shift.variant_scale(varid, false);
      auto norm_syst = norm_syst_extrap(mu_d, mu_u);
      auto& spl_nom = syst_trfs.samples[varid][1];
      double Nnominal = spl_nom.events();
      plot_samples(syst_trfs.samples[varid], title+" "+varid, true);
      TransformPlot1D(ctx, syst_trfs.transforms[varid][0]).scale(mu_d*Nnominal).npoints(100).external_coordinates(true).superimpose(true).plot();
      TransformPlot1D(ctx, syst_trfs.transforms[varid][1]).scale(Nnominal).npoints(100).external_coordinates(true).superimpose(true).plot();
      TransformPlot1D(ctx, syst_trfs.transforms[varid][2]).scale(mu_u*Nnominal).npoints(100).external_coordinates(true).superimpose(true).plot();

      plot_sample(syst_trfs.samples[varid][0], title+" "+varid+" down", true);
      TransformPlot1D(ctx, syst_trfs.transforms[varid][0]).scale(mu_d*Nnominal).npoints(100).external_coordinates(true).superimpose(true).plot();

      plot_sample(syst_trfs.samples[varid][1], title+" "+varid+" nominal", true);
      TransformPlot1D(ctx, syst_trfs.transforms[varid][1]).scale(Nnominal).npoints(100).external_coordinates(true).superimpose(true).plot();

      plot_sample(syst_trfs.samples[varid][2], title+" "+varid+" up", true);
      TransformPlot1D(ctx, syst_trfs.transforms[varid][2]).scale(mu_u*Nnominal).npoints(100).external_coordinates(true).superimpose(true).plot();
    }
  }
}

#endif // sfi_model_plots_h
