#ifndef sfi_DataTransformPlot1D_h
#define sfi_DataTransformPlot1D_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "TransformPlot.h"
#include "Histogram.h"
#include "EventSelector.h"

namespace sfi {
  class ISample;

  class DataTransformPlot1D {
  public:
    using This = DataTransformPlot1D;

    DataTransformPlot1D(Context& ctx, const ITransform& tr);
    DataTransformPlot1D(Context& ctx, const ITransform& tr, const OptionMap& opts);
    DataTransformPlot1D(Context& ctx, IEvaluator* ev);

    inline This& title(const std::string& t) { m_title = t; return *this; }
    inline const std::string& title() const { return m_title; }

    inline This& file_name(const std::string& t) { m_file_name = t; return *this; }
    inline const std::string& file_name() const { return m_file_name; }

    inline This& variable(unsigned v) { m_variable = v; return *this; }
    inline unsigned variable() const { return m_variable; }

    inline This& external_coordinates(bool ec) { m_trf_plot.external_coordinates(ec); return *this; }

    inline This& npoints(unsigned npts) { m_trf_plot.npoints(npts); return *this; }

    inline This& scale_to_data(bool b) { m_scale_to_data = b; return *this; }

    template<class _Sample, class _EventSel = AllEventSel>
    inline This& sample(const _Sample& spl, const _EventSel& esel = _EventSel()) {
      m_data_histogram = spl.histogram_1D(m_variable, m_trf_plot.npoints(), m_trf_plot.external_coordinates(), esel);
      return *this;
    }

    Canvas plot();
  protected:
    Log m_log;
    TransformPlot1D m_trf_plot;
    std::string m_title, m_file_name;
    unsigned m_variable;
    Histogram<true> m_data_histogram;
    bool m_scale_to_data;
  };
}

#endif // sfi_DataTransformPlot1D_h
