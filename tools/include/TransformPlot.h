#ifndef sfi_TransformPlot_h
#define sfi_TransformPlot_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string>
#include <vector>

#include "Log.h"
#include "Options.h"
#include "Matrix.h"
#include "gui.h"

// TODO: Remove
class TGraph;
class TGraph2D;

namespace sfi {
  class IEvaluator;
  class ITransform;
  class Variable;

  class TransformPlotBase : public Plot {
  public:
    using This = TransformPlotBase;

    inline const ITransform* transform() const { return m_transform; }
  protected:
    TransformPlotBase(const TransformPlotBase&);

    TransformPlotBase(IEvaluator* ev);

    TransformPlotBase(const ITransform* tr);

    TransformPlotBase(IEvaluator* ev, const OptionMap& opts);

    TransformPlotBase(const ITransform* tr, const OptionMap& opts);

    TransformPlotBase(const OptionMap& opts);

    virtual ~TransformPlotBase();

    void init(const OptionMap& opts);

    void copy_from(const TransformPlotBase& o);

    /** Get or create evaluator */
    IEvaluator* evaluator();

    IEvaluator* m_eval;
    const ITransform* m_transform;
    bool m_external_coordinates;
    bool m_scale_to_bin;
    bool m_superimpose;
    bool m_owns_evaluator;
    double m_scale;
    int m_fill_color;
    double m_marker_size;
    double m_minimum, m_maximum;
    std::string m_canvas;
    std::string m_draw_string;
    std::string m_style;
  };

  class TransformPlot1D : public TransformPlotBase {
  public:
    using This = TransformPlot1D;

    TransformPlot1D(Context& ctx);
    TransformPlot1D(Context& ctx, const OptionMap& opts);

    TransformPlot1D(Context& ctx, IEvaluator* ev);

    TransformPlot1D(Context& ctx, const ITransform& tr);

    TransformPlot1D(Context& ctx, const ITransform* tr);

    TransformPlot1D(Context& ctx, IEvaluator* ev, const OptionMap& opts);

    TransformPlot1D(Context& ctx, const ITransform& tr, const OptionMap& opts);

    TransformPlot1D(Context& ctx, const ITransform* tr, const OptionMap& opts);

    TransformPlot1D(Context& ctx, const TransformPlot1D&);

    TransformPlot1D(Context& ctx, const std::vector<TransformPlot1D>& plts);

    virtual ~TransformPlot1D();

    // ---- implementation of Plot ----

    virtual bool draw(bool first);

    // ---- exported interface ----

    This& operator=(const TransformPlot1D&);

    inline This& title(const std::string& title) { m_title = title; return *this; }

    inline This& external_coordinates(bool ec) { m_external_coordinates = ec; return *this; }
    inline bool external_coordinates() const { return m_external_coordinates; }

    inline This& scale_to_bin(bool s2b) { m_scale_to_bin = s2b; return *this; }

    inline This& scale(double s) { m_scale = s; return *this; }

    inline This& color(int col) { m_color = col; return *this; }

    inline This& marker(int ms) { m_marker_style = ms; return *this; }

    inline This& marker_size(double ms) { m_marker_size = ms; return *this; }

    inline This& line_size(double ls) { m_line_size = ls; return *this; }

    inline This& npoints(unsigned npts) { m_npts = npts; return *this; }
    inline unsigned npoints() const { return m_npts; }

    inline This& canvas(const std::string& canvas) { m_canvas = canvas; return *this; }

    inline This& minimum(double m) { m_minimum = m; return *this; }
    inline This& maximum(double m) { m_maximum = m; return *this; }

    inline This& superimpose(bool uf) { m_superimpose = uf; return *this; }

    inline This& subevals(const std::vector<This>& evs) { m_subevals = evs; return *this; }

    inline std::vector<This>& subevals() { return m_subevals; }

    TGraph* plot();
  protected:
    void init(const OptionMap& om);

    TGraph* make_plot(const Vector<double>& xvals, const Variable& var, const std::string& _draws);

    void create_xvalues_and_scale(const Variable& var, Vector<double>& xvals, double& xscale);

    void eval(const Vector<double>& xvals, double xscale);

    Log m_log;
    unsigned m_npts;
    std::vector<This> m_subevals;
    Vector<double> m_yvals;
  };

  class TransformPlot2D : public TransformPlotBase {
  public:
    using This = TransformPlot2D;

    TransformPlot2D(Context& ctx, IEvaluator* ev);

    TransformPlot2D(Context& ctx, const ITransform& tr);

    TransformPlot2D(Context& ctx, const ITransform* tr);

    TransformPlot2D(Context& ctx, IEvaluator* ev, const OptionMap& opts);

    TransformPlot2D(Context& ctx, const ITransform& tr, const OptionMap& opts);

    TransformPlot2D(Context& ctx, const ITransform* tr, const OptionMap& opts);

    TransformPlot2D(Context& ctx, const OptionMap& opts);

    virtual ~TransformPlot2D();

    // ---- implementation of Plot ----

    virtual bool draw(bool first);

    // ---- exported interface ----

    inline This& transform(const ITransform* tr) { this->m_transform = tr; return *this; }

    inline This& transform(IEvaluator* ev) { this->m_eval = ev; return *this; }

    inline This& title(const std::string& title) { m_title = title; return *this; }

    inline This& external_coordinates(bool ec) { m_external_coordinates = ec; return *this; }

    inline This& scale_to_bin(bool s2b) { m_scale_to_bin = s2b; return *this; }

    inline This& bin_center(bool bc) { m_bin_center = bc; return *this; }

    inline This& scale(double s) { m_scale = s; return *this; }

    inline This& color(int col) { m_color = col; return *this; }

    inline This& marker(int ms) { m_marker_style = ms; return *this; }

    inline This& marker_size(double ms) { m_marker_size = ms; return *this; }

    inline This& line_size(double ls) { m_line_size = ls; return *this; }

    inline This& npointsx(int npts) { m_npts_x = npts; return *this; }
    inline This& npointsy(int npts) { m_npts_y = npts; return *this; }

    inline This& canvas(const std::string& canvas) { m_canvas = canvas; return *this; }

    inline This& extra_values(const std::vector<double>& ev) { m_extra_values = ev; return *this; }

    inline This& swap_axes(bool sa) { m_swap_axes = sa; return *this; }

    inline This& draw_string(const std::string& ds) { m_draw_string = ds; return *this; }

    inline This& use_function(bool uf) { m_use_function = uf; return *this; }

    inline This& minimum(double m) { m_minimum = m; return *this; }
    inline This& maximum(double m) { m_maximum = m; return *this; }

    inline This& superimpose(bool uf) { m_superimpose = uf; return *this; }

    TGraph2D* plot(const std::string& draws = "");
  protected:
    void init(const OptionMap& opts);

    void plot_function(const std::string& draws, IEvaluator* eval);

    Log m_log;
    int m_npts_x;
    int m_npts_y;
    std::vector<double> m_extra_values;
    bool m_swap_axes;
    bool m_bin_center;
    bool m_use_function;
  };
}

#endif // sfi_TransformPlot_h
