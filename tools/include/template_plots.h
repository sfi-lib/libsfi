#ifndef sfi_template_plots_h
#define sfi_template_plots_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "TemplateTransformation.h"
#include "TransformPlot.h"

namespace sfi {
  /**
   * Plot data used to create a template transform, together with slices of the template.
   */
  template<class _CTransf, class _Trf>
  void plot_template_slices(Context& ctx, const std::string& title, template_result<_CTransf, _Trf>& tpl_res) {
    Canvas& can = Canvas::get(title+" slices");
    for(unsigned i=0; i<tpl_res.points.rows(); ++i) {
      auto seval = tpl_res.transform.template slice_evaluator<0>(true, 1.0, true);
      seval->slice(tpl_res.points.row_data(i));
      auto hs = tpl_res.samples[i].histogram_1D(0,100,true,AllEventSel());
      can.draw(title+" "+std::to_string(i), hs).color(1);
      // draw_histogram(title+" "+std::to_string(i), hs, i? "SAME" : "", 1);
      can.draw(new TransformPlot1D(ctx, seval.release())).scale(tpl_res.samples[i].events()).external_coordinates(true).superimpose(true).plot();
    }
    can.plot();
  }
}

#endif // sfi_template_plots_h
