#ifndef sfi_PDF2DComparison_h
#define sfi_PDF2DComparison_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Transform.h"
#include "Sample.h"
#include "TransformPlot.h"

namespace sfi {
  class Histogram2DPlot {
  public:
    using This = Histogram2DPlot;

    Histogram2DPlot();

    Histogram2DPlot(const OptionMap& opts);

    inline This& histogram(Histogram2D<true,double>&& hist) {
      m_histogram = std::forward<Histogram2D<true,double>>(hist);
      return *this;
    }
    inline This& histogram(const Histogram2D<true,double>& hist) {
      m_histogram = hist;
      return *this;
    }
    inline Histogram2D<true,double>& histogram() { return m_histogram; }

    inline This& color(int col) { m_color = col; return *this; }
    inline This& title(const std::string& title) { m_title = title; return *this; }
    inline This& draw_string(const std::string& ds) { m_draw_string = ds; return *this; }
    inline This& maximum(double m) { m_maximum = m; return *this; }
    inline This& minimum(double m) { m_minimum = m; return *this; }
    void plot();
  protected:
    void init();

    OptionMap m_opts;
    int m_color;
    std::string m_title;
    std::string m_draw_string;
    Histogram2D<true,double> m_histogram;
    double m_minimum, m_maximum;
  };

  class PDF2DComparison {
  public:
    using This = PDF2DComparison;

    PDF2DComparison(Context& ctx, const OptionMap& opts);

    PDF2DComparison(Context& ctx, const std::string& title);

    template<class _Sample, class _EventSel>
    inline This& make_histogram(const _Sample& spl, const _EventSel& esel) {
      m_histo_plot.histogram(spl.histogram_2D(m_v1, m_v2, m_nbins, m_external_coordinates, esel));
      return *this;
    }

    inline Histogram2DPlot& histogram_plot() { return m_histo_plot; }

    inline This& transform(const ITransform& trf) { m_trf_plot.transform(&trf); return *this; }

    inline TransformPlot2D& transform_plot() { return m_trf_plot; }

    inline This& v1(int _v1) { m_v1 = _v1; return *this; }
    inline This& v2(int _v2) { m_v2 = _v2; return *this; }
    This& nbins(int _nb);
    This& external_coordinates(bool ec);
    inline This& canvas(const std::string& can) { m_canvas_title = can; return *this; }
    This& scale_to_data(bool b) { m_scale_to_data = b; return *this; }
    inline This& maximum(double m) { m_maximum = m; return *this; }
    inline This& minimum(double m) { m_minimum = m; return *this; }
    void plot();
  protected:
    void init();

    OptionMap m_opts;
    int m_v1, m_v2, m_nbins;
    double m_maximum, m_minimum;
    std::string m_canvas_title;
    Histogram2DPlot m_histo_plot;
    TransformPlot2D m_trf_plot;
    bool m_external_coordinates;
    bool m_scale_to_data;
  };
}
#endif // sfi_PDF2DComparison_h
