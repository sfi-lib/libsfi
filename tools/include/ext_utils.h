#ifndef sfi_ext_utils_h
#define sfi_ext_utils_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <string>

#include "stat.h"
#include "Histogram.h"
#include "MatrixOp.h"

class TH1D;
class TH2D;
class TF2;
class TFile;
class TLegend;
class TGraph;
class TGraphErrors;
class TLine;
class TCanvas;
class TTree;

#include <TString.h>
#include <TVectorD.h>
#include <TMatrixD.h>
#include <TMatrixDSym.h>

namespace sfi {
  class SampleBase;

  TH1D* create_histogram(const std::string& name, const Histogram<false, double>& hi);
  TH1D* create_histogram(const std::string& name, const Histogram<true, double>& hi);

  TH1D* draw_histogram(const std::string& name, const Histogram<false, double>& hi, const std::string& draws, int color = 1);
  TH1D* draw_histogram(const std::string& name, const Histogram<true, double>& hi, const std::string& draws, int color = 1);

  TH2D* create_histogram(const std::string& name, const Histogram2D<false, double>& hi);
  TH2D* create_histogram(const std::string& name, const Histogram2D<true, double>& hi);

  TH2D* draw_histogram(const std::string& name, const Histogram2D<false, double>& hi, const std::string& draws, int color = 1);
  TH2D* draw_histogram(const std::string& name, const Histogram2D<true, double>& hi, const std::string& draws, int color = 1);

  /**
   * Create and draw a graph in a new canvas using the points (x,y) in the given vector.
   */
  TGraph* draw_graph(const std::string& title, const std::vector<std::array<double, 2> >& ppts, bool do_draw = true);

  TGraph* draw_graph(const Vector<double>& xvals, const Vector<double>& yvals, const std::string& title = "", bool do_draw = true);

  TGraph* create_graph(const std::vector<double>& xvals, const std::vector<double>& yvals, const std::string& title = "");

  TGraph* create_graph(const Vector<double>& xvals, const Vector<double>& yvals, const std::string& title = "");

  TGraphErrors* create_graph(const Vector<double>& xvals, const SampleStats& yvals, const std::string& title = "");

  TGraphErrors* create_graph(const Vector<double>& xvals, const Vector<double>& yvals, const Vector<double>& yuncvals, const std::string& title = "");

  TGraph* create_graph(const Matrix<double>& xvals, unsigned idx, const std::vector<double>& yvals);

  template<size_t N>
  TGraph* create_graph(const std::vector<std::array<double, N>>& xvals, unsigned idx, const std::vector<double>& yvals) {
    std::vector<double> xpts(xvals.size(), 0.0);
    for(unsigned i=0; i<xvals.size(); ++i) {
      xpts[i] = xvals[i][idx];
    }
    return create_graph(xpts, yvals);
  }

  TH1D* set_visible_range_y(TH1D* hi, double ymin, double ymax);

  TLine* draw_horizontal_line(TH1D* hi, double y, int col);

  TLine* draw_vertical_line(TH1D* hi, double x, int col);

  TH1D* create_histogram(const std::string& title, unsigned nbins, double xmin, double xmax);

  TH2D* create_histogram_2d(const std::string& title, unsigned nbinsx, double xmin, double xmax, unsigned nbinsy, double ymin, double ymax, const std::string& xtitle, const std::string& ytitle);

  TH1D* create_hi(const SampleStats& stat, const std::string& title, bool center_bins = false, bool mean_unc = false);

  TCanvas* create_canvas(const std::string& title);
  TCanvas* create_canvas(const std::string& title, int width, int height);
  void save_canvas(TCanvas*, const std::string& fname);

  TH1D* fill_histogram(TH1D* hi, double x, double w = 1.0);

  TH2D* fill_histogram_2d(TH2D* hi, double x, double y, double w = 1.0);

  TH1D* draw_hi(TH1D* hi, int col, double msize, int mstyle, const char* draws);

  /** Copy a symmetric matrix, return false if the matrix is not symmetric or the matrices are of different sizes */
  bool copy_sym_matrix(const TMatrixD& A, TMatrixDSym& B);

  bool copy_sym_matrix(const TMatrixD& A, MatrixSym<double>& B);

  /** Copy a symmetric matrix, return false if the matrices are of different sizes */
  bool copy_sym_matrix(const MatrixSym<double>& A, TMatrixDSym& B, unsigned offset = 0);

  void adjust_range(TCanvas* can, const std::vector<TH1D*>& hists);

  void adjust_range(TCanvas* can, const std::vector<TGraph*>& grs);

  void init_style();

  /** Plot histograms in the same canvas */
  void plot_histograms(const std::vector<TH1D*>& his, const std::string& title);
  void plot_histograms(const std::vector<TH1D*>& his, const std::vector<int>& colors, const std::string& title);

  /** Plot each histogram in a separate canvas */
  void plot_histograms(const std::vector<TH1D*>& his);
  void plot_histograms(const std::vector<TH2D*>& his);

  TH1D* draw_histo_clone(TFile* f, const TString& hiname, int color, TLegend* leg = 0, const TString& legtitle = "");

  void fit_gaus(const Histogram<true,double>& hi, double xmin, double xmax, double& mu, double& mu_e, double& sigma, double& sigma_e, bool iterate);

  void fit_gaus(TH1D* hi, double xmin, double xmax, double& mu, double& mu_e, double& sigma, double& sigma_e, bool iterate = false);

  /** Draw Graph with errors, in a specified canvas */
  void draw_graph(TGraphErrors* gr, const std::string& title, const std::string& draws, const std::string& xtitle, const std::string& ytitle);

  /** Set marker options on graph, draw it and add it to given legend */
  TGraphErrors* draw_graph(TGraphErrors *gr, unsigned style, unsigned col, const char* draw, const char* title, TLegend* leg);

  /** Plot a graph with a displacement in c given by idx*dx */
  std::pair<double, double> draw_graph_errors(TCanvas *tc, TGraphErrors* gr, const std::string& title, const char* draw, int col, int style, TLegend* leg, int idx, double _dx);

  bool compute_eigenvalues(const MatrixSym<double>& cov, Vector<double>& eigenv);

  /**
   * Perform eigen-decomposition of A.
   * iA = QDQ^t where D is a diagonal matrix with elements: d_i = 2.0/(d_i + sqrt(d_i^2 + 8 rho))
   * @param A The matix to invert
   * @param eigenv The regularized eigenvectors of the inverse
   * @param iA The inverted and regularizd matrix
   * @param original_eigenv The original eigenvalues of A
   * @return true if nothing went wrong.
   *
   * Reference:
   * M.O. Kuismin, J.T. Kemppainen, and M.J. Sillanpää. Precision matrix estimation with rope.
   * Journal of Computational and Graphical Statistics, 26(3):682–694, 2017.
   */
  //invert_result compute_eigen_inverse_rope(const MatrixSym<double>& A, Vector<double>& eigenv, MatrixSym<double>& iA, double rho, Vector<double>& original_eigenv, double eigen_limit = 0.0);

  rope_invert_result compute_svd_inverse_rope(const MatrixSym<double>& cov, Vector<double>& eigenv, MatrixSym<double>& icov, double rho, Vector<double>& original_eigenv, bool prune_eigen);
}

#endif // sfi_ext_utils_h
