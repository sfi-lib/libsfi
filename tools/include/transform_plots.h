#ifndef sfi_transform_plots_h
#define sfi_transform_plots_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Transform.h"
#include "Sample.h"
#include "EventSelector.h"
#include "ext_utils.h"
#include "AmplitudeTransformation.h"
#include "TransformPlot.h"

class TH1D;

namespace sfi {
  template<class _Transf, class _EventSel, bool _SplUseWeight = false>
  TH1D* plot_lhood(const std::string& title, Sample<_Transf::dims(), tbool<_SplUseWeight>>& sample, const _Transf& trf, const _EventSel& esel, unsigned nbins, double lmin, double lmax, double& lhood_sum) {
    TH1D* lhi = create_histogram(title, nbins, lmin, lmax);
    auto tev = trf.evaluator();
    lhood_sum = 0.0;
    sample.apply(esel, [lhi, &tev, &lhood_sum](const double* x, double w) {
      double p = (*tev)(x);
      if(p > 0) {
        double lh = -2.0*ln(p);
        lhood_sum += lh;
        fill_histogram(lhi, lh, w);
      }
      else {
        std::cout<<"plot_lhood() Error: p <= 0 "<<p<<" ";
        for(unsigned i=0; i<_Transf::dims(); ++i) std::cout<<x[i]<<" ";
        std::cout<<std::endl;
      }
    });
    return lhi;
  }

  template<class _Transf, class _EventSel1, class _EventSel2,bool _SplUseWeight = false>
  void compare_lhood(const std::string& title, Sample<_Transf::dims(), tbool<_SplUseWeight>>& sample, const _Transf& trf, const _EventSel1& esel1, const _EventSel2& esel2, unsigned nbins, double lmin, double lmax) {
    double lhood1(0.0), lhood2(0.0);
    TH1D* hi1 = plot_lhood(title+"1", sample, trf, esel1, nbins, lmin, lmax, lhood1);
    TH1D* hi2 = plot_lhood(title+"2", sample, trf, esel2, nbins, lmin, lmax, lhood2);
    plot_histograms({hi1, hi2}, {1, 2}, title);
    std::cout<<"compare_lhood() '"<<title<<"' lhood1: "<<lhood1<<" lhood2: "<<lhood2<<std::endl;
  }

  template<class _Res>
  void plot_amp_transform_details(const std::string& /*prefix*/, const _Res& /*res*/, const std::string& /*fprefix*/) {}

  void plot_amp_transform_details(const std::string& prefix, const amplitude_transform_result<ttrue>& res, const std::string& fprefix);
}

#endif // sfi_transform_plots_h
