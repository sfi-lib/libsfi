/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "TransformBase.h"
#include "Variables.h"

using namespace sfi;

// ---- TransformVariableBase ----

TransformVariableBase::~TransformVariableBase() { }

TransformVariableBase::TransformVariableBase(const std::string& name):
    m_name(name), m_variables(nullptr), m_sample(nullptr),
    m_compute_covariance(false), m_is_amplitude(false), m_compute_uncertainty(false) {
}

TransformVariableBase::TransformVariableBase(const This& o):
    m_name(o.m_name), m_variables(o.m_variables), m_sample(nullptr),
    m_compute_covariance(o.m_compute_covariance), m_is_amplitude(o.m_is_amplitude),
    m_compute_uncertainty(o.m_compute_uncertainty) {
}

TransformVariableBase::TransformVariableBase(This&& o):
    m_name(std::move(o.m_name)), m_variables(std::move(o.m_variables)), m_sample(std::move(o.m_sample)),
    m_compute_covariance(o.m_compute_covariance), m_is_amplitude(o.m_is_amplitude),
    m_compute_uncertainty(o.m_compute_uncertainty) {
  o.m_variables = 0;
}

TransformVariableBase::TransformVariableBase(const OptionMap& om):
    m_name(om.get_as<std::string>("name")),
    m_variables(std::make_shared<Variables>(om.sub_vec("variables"))), m_sample(nullptr),
    m_compute_covariance(false), m_is_amplitude(om.get_as<bool>("is_amplitude")),
    m_compute_uncertainty(false) {
  m_variables->lock();
}

void TransformVariableBase::write(OptionMap& op) const {
  op.set("name", this->m_name);
  op.sub_vec("variables",true);
  if(this->m_variables) this->m_variables->write(op.sub_vec("variables",true));
  op.set("is_amplitude", this->m_is_amplitude);
}

void
TransformVariableBase::copy_from(const TransformVariableBase& o) {
  m_name = o.m_name;
  m_variables = o.m_variables;
  m_sample = o.m_sample;
  m_compute_covariance = o.m_compute_covariance;
  m_is_amplitude = o.m_is_amplitude;
  m_compute_uncertainty = o.m_compute_uncertainty;
}

void
TransformVariableBase::move_from(TransformVariableBase&& o) {
  m_name = std::move(o.m_name);
  m_variables = std::move(o.m_variables);
  o.m_variables = nullptr;
  m_sample = std::move(o.m_sample);
  o.m_sample = nullptr;
  m_compute_covariance = o.m_compute_covariance;
  m_is_amplitude = o.m_is_amplitude;
  m_compute_uncertainty = o.m_compute_uncertainty;
}
