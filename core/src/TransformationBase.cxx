/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Transformation.h"

using namespace sfi;

TransformationBase::TransformationBase(Context& ctx, const std::string& name):
      m_name(name), m_normalize(false), m_compute_covariance(false),
      m_compute_uncertainty(true), m_explicit_post_processing(false),
      m_prune_significance(0.0),
      m_log(Log::get(ctx, name)) {
  bind_options();
}

TransformationBase::TransformationBase(Context& ctx, const OptionMap& opts):
          m_name(), m_normalize(false), m_compute_covariance(false),
          m_compute_uncertainty(true), m_explicit_post_processing(false),
          m_prune_significance(0.0),
          m_log(Log::get(ctx, opts.get<std::string>("name"))), m_options(opts) {
  bind_options();
  if(opts.has("print_level")) m_log.print_level(opts.get<std::string>("print_level"));
}

TransformationBase::TransformationBase(const TransformationBase& o):
      m_name(o.m_name),
      m_normalize(o.m_normalize), m_compute_covariance(o.m_compute_covariance),
      m_explicit_post_processing(o.m_explicit_post_processing),
      m_prune_significance(o.m_prune_significance),
      m_log(o.m_log), m_options(o.m_options) {
  bind_options();
}

void
TransformationBase::bind_options() {
  m_options.bind("name", m_name);
  m_options.bind("normalize", m_normalize);
  m_options.bind("compute_covariance", m_compute_covariance);
  m_options.bind("compute_uncertainty", m_compute_uncertainty);
  m_options.bind("explicit_post_processing", m_explicit_post_processing);
  m_options.bind("prune_significance", m_prune_significance);
}
