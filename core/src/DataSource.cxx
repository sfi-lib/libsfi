/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "DataSource.h"

using namespace sfi;

DataSource::DataSource(const OptionMap& src_opt):
  m_src_opts(src_opt), m_name(""), m_index(0), m_nevents(0), m_dims(0) {
  bind_options();
}

DataSource::DataSource(const std::string& name, unsigned index, unsigned nevents, unsigned dims):
  m_name(name), m_index(index), m_nevents(nevents), m_dims(dims) {
  bind_options();
}

DataSource::DataSource(const DataSource& nom, const OptionMap& vopts, const std::string& variant):
    m_src_opts(vopts), m_name(nom.name()+"_"+variant), m_variant(variant),
    m_index(0), m_nevents(nom.m_nevents), m_dims(0) {
  bind_options();
}

DataSource::~DataSource() { }

void
DataSource::bind_options() {
  m_src_opts.bind("name", m_name).bind("index", m_index);
  m_src_opts.bind("nevents", m_nevents).bind("variant", m_variant);
  m_src_opts.bind("dims", m_dims);
}

DefaultDataSource::DefaultDataSource(const OptionMap& src_opt):DataSource(src_opt) {}

DefaultDataSource::DefaultDataSource(const std::string& name, unsigned index, unsigned nevents, unsigned dims):DataSource(name, index, nevents, dims) {}

DefaultDataSource::DefaultDataSource(const DataSource& nom, const OptionMap& vopts, const std::string& variant):DataSource(nom, vopts, variant) {}

DefaultDataSource::~DefaultDataSource() {}

std::shared_ptr<DataSource> DefaultDataSource::get_variant(const OptionMap& vopts, const std::string& variant) const {
  return std::make_shared<DefaultDataSource>(*this, vopts, variant);
}

std::shared_ptr<DataSource> DefaultDataSource::do_clone() const { return std::make_shared<DefaultDataSource>(this->m_src_opts); }
