/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <sample_functions.h>

namespace sfi {
  void hit_and_miss_1d_from_function(const std::function<double(const double*)>& func, const std::function<void(unsigned i, double)>& target, unsigned nevents, Context::RandomTy& rnd, double upper, double xmin, double xmax, bool debug) {
    std::uniform_real_distribution<> uni_x(xmin, xmax);
    double x, f, pv;

    // scouting
    for(unsigned i=0; i<10000; ++i) {
      x = uni_x(rnd);
      pv = func(&x);
      if(pv < 0) {
        std::cout<<"hit_and_miss_1d_from_function() Error "<<x<<" -> "<<pv<<" (< 0)"<<std::endl;
      }
      else {
        upper = std::max(upper, pv);
      }
    }
    upper *= 1.2;
    if(debug) {
      std::cout<<"hit_and_miss_1d_from_function() upper="<<upper<<std::endl;
    }

    std::uniform_real_distribution<> uni_y(0, upper);
    unsigned ntries(0);
    for(unsigned i=0; i<nevents; ++i) {
      do {
        ++ntries;
        x = uni_x(rnd);
        f = uni_y(rnd);
        pv = func(&x);
        if(pv > upper) {
          std::cout<<"hit_and_miss_1d_from_function() Error "<<x<<" -> "<<pv<<" (> "<<upper<<")"<<std::endl;
        }
        else if(pv < 0) {
          std::cout<<"hit_and_miss_1d_from_function() Error "<<x<<" -> "<<pv<<" (<0)"<<std::endl;
        }
      }
      while(f > pv);
      target(i,x);
    }
    if(debug) {
      std::cout<<"hit_and_miss_1d_from_function() upper="<<upper<<" nevents="<<nevents<<" tries="<<ntries<<std::endl;
    }
  }

  void sample_random_1d_from_function(sfi::Sample<1>* res, const std::function<double(const double*)>& func, unsigned nevents, Context::RandomTy& rnd, double upper, double xmin, double xmax, bool debug) {
    res->events(nevents);
    hit_and_miss_1d_from_function(func, [res](unsigned i, double x) { res->x(i, 0) = x; }, nevents, rnd, upper, xmin, xmax, debug);
  }

  void sample_random_2d_from_function(sfi::Sample<2>* res, const std::function<double(const double*)>& func, unsigned nevents, Context::RandomTy& rnd, double upper,
    double xmin, double xmax, double ymin, double ymax, bool debug) {
    res->events(nevents);
    double x[2];
    double f, pv;
    unsigned ntries(0);
    std::uniform_real_distribution<> uni_x(xmin, xmax);
    std::uniform_real_distribution<> uni_y(ymin, ymax);
    upper = 0;
    // scouting
    for(unsigned i=0; i<10000; ++i) {
      x[0] = uni_x(rnd);
      x[1] = uni_y(rnd);
      pv = func(x);
      if(pv < 0) {
        std::cout<<"sample_random_2d_from_function() Error "<<x[0]<<" "<<x[1]<<" -> "<<pv<<" (< 0)"<<std::endl;
      }
      else {
        upper = std::max(upper, pv);
      }
    }
    upper *= 1.2;
    if(debug) {
      std::cout<<"sample_random_2d_from_function() upper="<<upper<<std::endl;
    }

    std::uniform_real_distribution<> uni2(0, upper);
    for(unsigned i=0; i<nevents; ++i) {
      do {
        ++ntries;
        x[0] = uni_x(rnd);
        x[1] = uni_y(rnd);
        f = uni2(rnd);
        pv = func(x);

        if(pv > upper) {
          std::cout<<"sample_random_2d_from_function() Error "<<x[0]<<" "<<x[1]<<" -> "<<pv<<" (> "<<upper<<")"<<std::endl;
        }
        else if(pv < 0) {
          std::cout<<"sample_random_2d_from_function() Error "<<x[0]<<" "<<x[1]<<" -> "<<pv<<" (<0)"<<std::endl;
        }
      }
      while(f > pv);
      res->x(i, 0) = x[0];
      res->x(i, 1) = x[1];
    }
  }

  void sample_random_1d(sfi::Sample<1>* res, const ITransform& trans, unsigned nevents, Context::RandomTy& rnd, double upper) {
    auto ev = trans.get_evaluator();
    ev->set_external_coordinates(true);
    auto& v0 = trans.get_variables()->at(0);
    sample_random_1d_from_function(res, [&ev](const double *x) -> double { return ev->get_value(x); }, nevents, rnd, upper, v0.minval(), v0.maxval());
  }

  void sample_random_2d(sfi::Sample<2>* res, const ITransform& trans, unsigned nevents, Context::RandomTy& rnd, double upper, bool debug) {
    auto ev = trans.get_evaluator(1.0, true);
    auto vars = trans.get_variables();
    auto& v0 = vars->at(0);
    auto& v1 = vars->at(1);
    sample_random_2d_from_function(res, [&ev](const double *x) -> double { return ev->get_value(x); }, nevents, rnd, upper, v0.minval(), v0.maxval(), v1.minval(), v1.maxval(), debug);
  }
}
