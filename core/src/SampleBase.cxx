/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "SampleBase.h"

#include <fstream>
#include <sstream>
#include <assert.h>

using namespace sfi;
using namespace std;

SampleBase::SampleBase(unsigned nevents, unsigned evsize, const std::string& name, const PVariables& vars):
  m_name(name), m_vars(vars), m_src(0), m_data(nevents, evsize), m_is_normalized(false) {
}

SampleBase::SampleBase(unsigned nevents, unsigned evsize, double*data, const std::string& name, const PVariables& vars):
  m_name(name), m_vars(vars), m_src(0), m_data(nevents, evsize, data), m_is_normalized(false) {
}

SampleBase::~SampleBase() {}

// ---- implementation of ISample ----

std::shared_ptr<const DataSource> SampleBase::get_data_source() const {  return this->data_source(); }

void SampleBase::set_data_source(const std::shared_ptr<const DataSource>& ds) { this->data_source(ds); }

void SampleBase::do_dump() const { this->dump(); }

void SampleBase::do_write(const std::string& filename) { this->write(filename); }

const std::string& SampleBase::get_name() const { return this->name(); }

void SampleBase::set_name(const std::string& n) { this->name(n); }

bool SampleBase::get_is_normalized() const { return this->is_normalized(); }

PVariables SampleBase::get_variables() { return this->variables(); }
const PVariables SampleBase::get_variables() const { return this->variables(); }

void SampleBase::set_variables(const PVariables& vars) { this->variables(vars); }

unsigned SampleBase::get_events() const { return this->m_data.rows(); }

void SampleBase::set_events(unsigned ne) { resize(ne); }

bool SampleBase::get_owns_data() const { return owns_data(); }

Matrix<double>& SampleBase::get_matrix() { return matrix(); }
const Matrix<double>& SampleBase::get_matrix() const { return matrix(); }

OptionMap& SampleBase::get_meta_data() { return m_meta_data; }
const OptionMap& SampleBase::get_meta_data() const { return m_meta_data; }

bool
SampleBase::do_append(const ISample& spl) {
  auto* sb = dynamic_cast<const SampleBase*>(&spl);
  if(sb) return append(*sb);
  else return false;
}

void
SampleBase::do_clear() { this->clear(); }

bool
SampleBase::resize(unsigned ev) {
  if(this->owns_data()) {
    m_data.rows(ev);
    return m_data.rows() == ev;
  }
  else return false;
}

bool
SampleBase::append(const SampleBase& sb) {
  if(this == &sb) throw SampleException("can't append this");
  if(!this->owns_data()) throw SampleException("data not owned");
  const size_t evsize(m_data.cols());
  if(evsize != sb.matrix().cols()) throw SampleException("Invalid data to append, expected "+std::to_string(evsize)+" columns");
  auto res = m_data.append(sb.m_data);
  if(res) throw SampleException("Unable to append ("+std::to_string(res)+") sample '"+sb.name()+"' {"+std::to_string(sb.m_data.rows())+", "+std::to_string(sb.m_data.cols())+", "+std::to_string((size_t)(void*)sb.m_data.data_ptr())+"} to '"+name()+"' {"+std::to_string(m_data.rows())+", "+std::to_string(m_data.cols())+", "+std::to_string((size_t)(void*)m_data.data_ptr())+"}");
  return true;
}

void
SampleBase::clear() { m_data.clear(); }

void
SampleBase::reinitialize(unsigned nevents, const std::string& name, const PVariables& vars, unsigned evsize) {
  if(evsize != m_data.cols()) throw SampleException("Invalid number of columns");
  if(!owns_data()) throw SampleException("Data not owned!");
  if(nevents) {
    if(!resize(nevents)) throw SampleException("Failed to resize");
  }
  this->is_normalized(false);
  this->name(name);
  this->variables(vars);
  clear();
}

void
SampleBase::reinitialize(unsigned nevents, double* data, const std::string& name, const PVariables& vars) {
  m_data = Matrix<double>(nevents, m_data.cols(), data);
  this->is_normalized(false);
  this->name(name);
  this->variables(vars);
}

void SampleBase::copy_to(bool deep_copy, SampleBase* res, unsigned nevents) const {
  unsigned nev = nevents ? nevents : this->events();
  res->m_vars = (this->m_vars && deep_copy) ? std::make_shared<Variables>(*this->m_vars) : this->m_vars;
  res->m_src = this->m_src;
  res->m_is_normalized = this->m_is_normalized;
  res->m_data.size(nev, this->m_data.cols());
  std::copy_n(this->m_data.data_ptr(), nev*this->m_data.cols(), res->m_data.data_ptr());
}

void
SampleBase::dump() const {
  const unsigned nevents(events());
  std::cout<<"Sample '"<<name()<<"' events: "<<nevents<<" dim: "<<this->get_dim()<<" use weights: "<<get_use_weight()<<" owns data: "<<m_data.owns_memory()<<" normalized: "<<get_is_normalized()<<std::endl;
  const unsigned dims(this->get_dim());
  for(unsigned r=0; r<nevents; ++r) {
    for(unsigned c=0; c<dims; ++c) {
      cout<<get_x(r, c)<<" ";
    }
    std::cout<<": "<<get_w(r)<<std::endl;
  }
}

void
SampleBase::write(const std::string& filename) {
  ofstream out(filename);
  if (!out.is_open()) throw SampleException("unable to open file '"+filename+"'");
  const unsigned nevents(events());
  const unsigned dims(this->get_dim());
  out << nevents << " " << dims << endl;
  for (unsigned i=0; i<nevents; ++i) {
    for (unsigned d=0; d<dims; ++d) {
      out << " " << get_x(i, d);
    }
    out << " " << get_w(i);
    out << endl;
  }
  out.close();
}

bool
SampleBase::normalize(Log& _log) {
  if(this->is_normalized()) {
    log_error(_log, "normalize() sample '"<<this->name()<<"' already normalized");
    return false;
  }
  if(!this->m_vars) {
    log_error(_log, "normalize() sample '"<<this->name()<<"' has no Variables");
    return false;
  }
  const unsigned ndims = this->get_dim();
  bool all_ok = true;

  for(size_t j=0; j<ndims; ++j) {
    auto& v = this->m_vars->at(j);
    if(!v.is_locked()) all_ok = v.lock() && all_ok;
  }
  if(!all_ok) {
    log_error(_log, "normalize() failed to lock variables!");
    return false;
  }

  // TODO: Just index in the Matrix
  double* evptr = m_data.data_ptr();
  const unsigned nevents(events());
  const unsigned evsize = ndims + this->get_use_weight();
  double xv, yv;
  unsigned nerr(0), nnan(0);
  for(unsigned i=0; i<nevents; ++i) {
    for(size_t j=0; j<ndims; ++j) {
      // xv = this->x(i, j);
      xv = evptr[j];
      auto& var = this->m_vars->at(j);
      if(!std::isfinite(xv)) ++nnan;
      else if(xv < var.minval()) {
        log_error(_log, "normalize() value "<<xv<<" < minval="<<var.minval());
        evptr[j] = var.minval();
        nerr++;
      }
      else if(xv > var.maxval()) {
        log_error(_log, "normalize() value "<<xv<<" > maxval="<<var.maxval());
        evptr[j] = var.maxval();
        nerr++;
      }
      else {
        yv = var.vtrans(xv);
        if((yv < var.internal_minval()) || (yv > var.internal_maxval())) {
          log_error(_log, "normalize() Internal error: sample '"<<this->name()<<"' variable '"<<var.name()<<"' invalid transformed value "<<yv<<" not in range "<<var.internal_minval()<<" -> "<<var.internal_maxval());
        }
        evptr[j] = yv;
      }
    }
    evptr += evsize;
  }
  if(nerr) {
    log_error(_log, "normalize() sample '"<<this->name()<<"' has "<<nerr<<" values out of range, setting to MIN/MAX");
    all_ok = false;
  }
  if(nnan) {
    log_error(_log, "normalize() sample '"<<this->name()<<"' has "<<nnan<<" invalid values");
    all_ok = false;
  }
  this->is_normalized(true);
  return all_ok;
}

bool
SampleBase::find_limits(Log& _log, double* minv, double* maxv) {
  bool all_ok = true;
  double xv;
  const unsigned nevents(events());
  const unsigned dims = this->get_dim();
  for(unsigned i=0; i<nevents; ++i) {
    for(size_t j=0; j<dims; ++j) {
      //xv = this->x(i, j);
      xv = m_data(i,j);
      if(xv == xv) {
        minv[j] = std::min(minv[j], xv);
        maxv[j] = std::max(maxv[j], xv);
      }
      else {
        log_error(_log, "find_limits() nan found event="<<i<<" variable="<<j);
        all_ok = false;
      }
    }
  }
  return all_ok;
}

namespace sfi {
  // TODO: Samples may have different Variables
  bool _samples_normalize(Log& _log, unsigned dims, const std::vector<SampleBase*>& samples) {
    const size_t ns(samples.size());
    if(ns == 0) {
      log_error(_log, "_samples_normalize() No Samples in set");
      return false;
    }
    auto vars = samples[0]->variables();
    if(!vars) {
      log_error(_log, "_samples_normalize() sample '"<<samples[0]->name()<<"' has no Variables");
      return false;
    }
    const size_t dim(samples[0]->get_dim());
    if(vars->nvariables() != dim) {
      log_error(_log, "_samples_normalize() invalid number of variables ("<<vars->nvariables()<<") expected "<<dim);
      return false;
    }
    if(dims != dim) {
      log_error(_log, "_samples_normalize() invalid number of dimensions ("<<dims<<") expected "<<dim);
      return false;
    }
    bool all_ext_set = true;
    for(unsigned d=0; d<dims; ++d) {
      auto& var = vars->at(d);
      all_ext_set = all_ext_set && var.external_interval_set();
    }
    bool all_ok = true;
    for(size_t s=0; s<ns; ++s) {
      if(samples[s]->variables()->nvariables() != dims) {
        log_error(_log, "_samples_normalize() invalid number of dimensions ("<<dims<<") for sample '"<<samples[s]->name()<<"' expected "<<dim);
        all_ok = false;
      }
    }
    if(!all_ok) return false;

    if(!all_ext_set) {
      std::vector<double> maxv(dims, std::numeric_limits<double>::min()), minv(dims, std::numeric_limits<double>::max());

      for(size_t s=0; s<ns; ++s) {
        auto& spl(*samples[s]);
        /*if(spl.variables() != vars) {
          log_error(_log, "_normalize_samples() Invalid Variables pointer");
          all_ok = false;
        }*/
        all_ok = spl.find_limits(_log, minv.data(), maxv.data()) && all_ok;
      }

      for(size_t s=0; s<ns; ++s) {
        auto va = samples[s]->variables();
        for(unsigned d=0; d<dims; ++d) {
          auto& var = va->at(d);
          if(!var.external_interval_set()) var.external_interval(minv[d], maxv[d]);
          else {
            if((var.minval() != minv[d]) || (var.maxval() != maxv[d])) {
              log_error(_log, "_samples_normalize() invalid external interval for sample '"<<samples[s]->name()<<"' for variable '"<<var.name()<<"' ");
              all_ok = false;
            }
          }
        }
      }
    }
    for(size_t s=0; s<ns; ++s) {
      auto va = samples[s]->variables();
      for(size_t i=0; i<dims; ++i) {
        auto& var = va->at(i);
        if(!var.is_locked() && !var.lock()) {
          log_error(_log, "_normalize_samples() Failed to lock variable '"<<var.name()<<"'"<<dim);
          all_ok = false;
        }
      }
    }
    // debugging
    log_debug(_log, "_normalize_samples() parameter limits:");
    for(size_t i=0; i<dim; ++i) {
      auto& var = vars->at(i);
      log_debug(_log, "#"<<i<<" '"<<var.name()<<"' : "<<var.minval()<<" -> "<<var.maxval()<<" ("<<var.internal_minval()<<" -> "<<var.internal_maxval()<<")");
    }
    // rescale
    for(size_t s=0; s<ns; ++s) samples[s]->normalize(_log);
    return all_ok;
  }
}

// ---- SampleMap ----

SampleMap::SampleMap() { }

SampleMap::SampleMap(SampleMap&& o):m_samples(std::move(o.m_samples)) {
  o.m_samples.clear();
}

SampleMap::~SampleMap() {
  for(auto& spl : m_samples) spl.second->do_release();
  m_samples.clear();
}
