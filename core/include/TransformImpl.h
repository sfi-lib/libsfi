#ifndef sfi_TransformImpl_h
#define sfi_TransformImpl_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Basis.h"
#include "TransformBase.h"
#include "EigenSystem.h"
#include "trans_tmp.h"
#include "square_system.h"

namespace sfi {
  template<typename _T1, typename _T2 = void> class Transform;

  // Determine EigenSystem for a generic Transform
  // ---
  // TODO: Both these probably not needed in the future!
  template<unsigned _Dims, unsigned _Deg, class _Type, class _IdxType, class _TType>
  struct _t_eigen_system<Transform<EigenVectors<_Dims, _Deg, _Type, _IdxType>, _TType>> :
  tvalued<EigenSystem<tuint<_Dims>, tuint<_Deg>, _Type, _TType>> {};

  template<class ... _T>
  struct _t_eigen_system<Transform<EigenSystem<_T...>, void>> :
  tvalued<EigenSystem<_T...>> {};
  // ---

  template<class _E1, class _T1, class _E2, class _T2>
  struct _t_eigen_system<Transform<Transform<_E1, _T1>, Transform<_E2, _T2>>> :
  tvalued<EigenSystem<t_EigenSystem<Transform<_E1, _T1>>, t_EigenSystem<Transform<_E2, _T2>>>> {};

  template<class _E1, class _T1, class _E2>
  struct _t_eigen_system<Transform<BiEigenVectors<_E1, _E2>, _T1>> :
  tvalued<EigenSystem<t_EigenSystem<Transform<_E1, _T1>>, t_EigenSystem<Transform<_E2, _T1>>>> {};

  // Determine Transform type for a generic Transform
  template<class _T1, class _T2, unsigned _Dim>
  struct _t_basis<Transform<_T1, _T2>, _Dim> : _t_basis<t_EigenSystem<Transform<_T1, _T2>>, _Dim> {};

  // Determine coefficienct type
  template<class _Eigen, class _TType> struct _t_coefficient_type<Transform<_Eigen, _TType>> :
  _t_coefficient_type<t_EigenSystem<Transform<_Eigen, _TType>>> {};

  // Determine Transform
  template<class ..._T> struct _t_transform<Transform<_T...>> : tvalued<Transform<_T...>> {};

  template<class ..._T> struct _t_transform<EigenSystem<_T...>> :
  tvalued<Transform<t_EigenVectors<EigenSystem<_T...>>, t_Basis<EigenSystem<_T...>,0>>> {};

  template<class ..._T1, class ..._T2> struct _t_transform<EigenSystem<EigenSystem<_T1...>, EigenSystem<_T2...>>> :
  tvalued<Transform<t_Transform<EigenSystem<_T1...>>, t_Transform<EigenSystem<_T2...>>>> {};

  template<unsigned _Dims, unsigned _Deg, class _EType, class _Basis>
  struct _t_transform<tdimensions<_Dims>, tdegree<_Deg>, _EType, _Basis> : tvalued<Transform<EigenVectors<_Dims,_Deg, _EType>, _Basis>> {
    static_assert(c_is_eigentype<_EType>(),"Must be an eigen type");
    static_assert(c_is_basis<_Basis>(), "Must be a basis");
  };
  template<unsigned _Dim, class _EType, class _Basis>
  struct _t_transform<tdimensions<_Dim>, _EType, _Basis> : tvalued<Transform<EigenVectors<_Dim,0U,_EType>, _Basis>> {
    static_assert(c_is_eigentype<_EType>(),"Must be an eigen type");
    static_assert(c_is_basis<_Basis>(), "Must be a basis");
  };

  template<class ... _T1, class ... _T2>
  struct _t_transform<Transform<_T1...>,Transform<_T2...>> : tvalued<Transform<Transform<_T1...>,Transform<_T2...>>> {};

  // Determine 1D transform
  template<class _T1, class _T2, unsigned _Dim>
  struct _t_transform_1D<Transform<_T1, _T2>, _Dim> {
    using Trf = Transform<_T1, _T2>;
    using EV = t_EigenVectors<Trf>;
    using tvalue = Transform<EigenVectors<1, c_eigen_degree<EV, _Dim>(), t_EigenType<EV, _Dim>>, t_Basis<Trf, _Dim>>;
  };

  template<class _Ta1, class _Ta2, class _Tb1, class _Tb2, unsigned _Dim>
  struct _t_transform_1D<Transform<Transform<_Ta1, _Ta2>, Transform<_Tb1, _Tb2>>, _Dim> :
  tcond<(_Dim < Transform<_Ta1, _Ta2>::dims()),
  _t_transform_1D< Transform<_Ta1, _Ta2>, _Dim>,
  _t_transform_1D< Transform<_Tb1, _Tb2>, _Dim - Transform<_Ta1, _Ta2>::dims()>> {};

  // Check if a given transform is a 1D transform
  template<class _Transf, class _Transf1D, unsigned _Idx, unsigned _MaxIdx>
  struct _t_is_1D_transform_impl : tor<teq<t_Transform1D<_Transf, _Idx>, _Transf1D>, _t_is_1D_transform_impl<_Transf, _Transf1D, _Idx+1, _MaxIdx>> { };

  template<class _Transf, class _Transf1D, unsigned _MaxIdx>
  struct _t_is_1D_transform_impl<_Transf, _Transf1D, _MaxIdx, _MaxIdx> : tfalse {};

  template<class _Transf, class _Transf1D>
  struct _t_is_1D_transform : _t_is_1D_transform_impl<_Transf, _Transf1D, 0, _Transf::dims()> {};

  template<class _Transf, class _Transf1D>
  inline constexpr bool c_is_1D_transform() {
    return _t_is_1D_transform<_Transf, _Transf1D>::value();
  }

  template<class _CoeffDst, class _CoeffSrc> struct _coeff_trf {
    static inline _CoeffDst eval(const _CoeffSrc& src) { return (_CoeffDst)src; }
  };

  template<> struct _coeff_trf<double, std::complex<double>> {
    static inline double eval(const std::complex<double>& src) { return std::real(src); }
  };

  // Determine axis eigen vectors
  template<class _Eigen, unsigned _Dim>
  using t_AxisEigenVectors = EigenVectors<1, 0U, t_EigenType<_Eigen, _Dim>>;

  template<class _Transf, class _EigenSys>
  struct DefaultTransformImpl {
    using AxisEigen = t_AxisEigenVectors<t_EigenVectors<_EigenSys>, 0>;
    using AxisTransf = Transform<AxisEigen, t_Basis<_EigenSys>>;
    using AmpAxisEigen = EigenVectors<1, 0, EigenSquare>;
    using AmpAxisTransf = Transform<AmpAxisEigen, t_Basis<_EigenSys>>;

    using Eigen2D = EigenVectors<2, c_eigen_degree<t_EigenVectors<_EigenSys>, 0>(), t_EigenType<t_EigenVectors<_EigenSys>, 0>>;
    using Transf2D = Transform<Eigen2D, t_Basis<_EigenSys, 0>>;
    using AmpEigen2D = EigenVectors<2, 0, EigenSquare>;
    using AmpTransf2D = Transform<AmpEigen2D, t_Basis<_EigenSys, 0>>;

    using Coeff = t_CoefficienctType<_Transf>;

    static inline TransformVariableBase* create_linear_axis_transform(const _Transf& trf, unsigned d, const std::string& name, const _EigenSys& eigens, unsigned axis) {
      if(d >= eigens.dims()) return 0;
      else {
        auto *atrf = new AxisTransf(AxisEigen(eigens.max_degree()), name, true);
        linear_axis_transform(trf, *atrf, axis);
        return atrf;
      }
    }

    static inline TransformVariableBase* create_amplitude_axis_transform(const _Transf& trf, unsigned d, const std::string& name, const _EigenSys& eigens, unsigned axis) {
      if(d >= eigens.dims()) return 0;
      else {
        auto *atrf = new AmpAxisTransf(AmpAxisEigen(eigens.max_degree()), name, true);
        amplitude_axis_transform(trf, *atrf, axis);
        return atrf;
      }
    }

    static inline TransformVariableBase* create_transform_1d(unsigned d, const std::string& name, const _EigenSys& eigens) {
      if(d >= eigens.dims()) return 0;
      else return create_transform_1d_impl<_EigenSys::is_dynamic()>(name, eigens);
    }

    template<bool _is_dyn>
    static inline TransformVariableBase* create_transform_1d_impl(const std::string& name, const _EigenSys& eigens, teif<_is_dyn>* = 0) {
      return new AxisTransf(AxisEigen(eigens.max_degree()), name, true);
    }
    template<bool _is_dyn>
    static inline TransformVariableBase* create_transform_1d_impl(const std::string& name, const _EigenSys&, teif<!_is_dyn>* = 0) {
      return new t_Transform1D<t_Transform<_EigenSys>>(name, true);
    }

    static inline TransformVariableBase* create_marginalized_2d(const _Transf& trf, unsigned, unsigned, const std::string& name, const _EigenSys& eigens, unsigned axis1, unsigned axis2) {
      if(trf.is_amplitude()) {
        auto* mtrf = new AmpTransf2D(AmpEigen2D(eigens.max_degree()), name, true);
        amplitude_marginalize_2d(trf, *mtrf, axis1, axis2);
        return mtrf;
      }
      else return create_marginalized_2d_impl<_EigenSys::is_dynamic()>(trf, name, eigens, axis1, axis2);
    }

    template<bool _is_dyn>
    static inline TransformVariableBase* create_marginalized_2d_impl(const _Transf& trf, const std::string& name, const _EigenSys& eigens, unsigned axis1, unsigned axis2, teif<_is_dyn>* = 0) {
      auto* mtrf = new Transf2D(Eigen2D(eigens.max_degree()), name, true);
      linear_marginalize_2d(trf, *mtrf, axis1, axis2);
      return mtrf;
    }
    template<bool _is_dyn>
    static inline TransformVariableBase* create_marginalized_2d_impl(const _Transf& trf, const std::string& name, const _EigenSys&, unsigned axis1, unsigned axis2, teif<!_is_dyn>* = 0) {
      auto* mtrf = new Transf2D(name, true);
      linear_marginalize_2d(trf, *mtrf, axis1, axis2);
      return mtrf;
    }

    template<class _ResTransf>
    static inline void amplitude_axis_transform(const _Transf& trf, _ResTransf& res, unsigned axis) {
      using TCoeff = t_CoefficienctType<_ResTransf>;
      res.clear();
      Coeff ep(0.0);
      unsigned j, k, maxidx;
      for(auto& eit : trf.eigenvectors()) {
        j = eit[axis];
        ep = trf.par(eit.par_idx());
        auto idx = eit.indices();
        maxidx = std::min<unsigned>(j, idx.max_idx(axis));
        for(k=0; k <= maxidx; ++k) {
          idx.set(axis, k);
          res.par(square_paridx(j,k)) += coeff_trf<TCoeff, Coeff>(ep*trf.par(idx.index()));
        }
      }
    }

    template<class _ResTransf>
    static inline void linear_axis_transform(const _Transf& trf, _ResTransf& res, unsigned axis) {
      assert(res.nparams() == (trf.var_degree(axis) + 1) && "Invalid number of params");
      using TCoeff = t_CoefficienctType<_ResTransf>;
      for(auto& eit : trf.eigenvectors()) {
        Coeff term(1.0);
        if(_EigenSys::integral(eit.indices(), axis, term))
          res.par(eit[axis]) += coeff_trf<TCoeff, Coeff>(trf.par(eit.par_idx())*term);
      }
    }

    template<class _ResTransf>
    static inline void linear_marginalize_2d(const _Transf& trf, _ResTransf& res, unsigned a1, unsigned a2) {
      using TCoeff = t_CoefficienctType<_ResTransf>;
      auto tei = res.eigenvectors().indices();
      for(auto& eit : trf.eigenvectors()) {
        Coeff term(1.0);
        if(_EigenSys::integral(eit.indices(), a1, a2, term)) {
          tei.set(0, eit[a1]);
          tei.set(1, eit[a2]);
          res.par(tei.index()) += coeff_trf<TCoeff, Coeff>(trf.par(eit.par_idx())*term);
        }
      }
    }

    template<class _ResTransf>
    static inline void amplitude_marginalize_2d(const _Transf& trf, _ResTransf& res, unsigned a1, unsigned a2) {
      using TCoeff = t_CoefficienctType<_ResTransf>;
      res.clear();
      Coeff ep(0.0);
      unsigned i1, i2, j1, j2, maxi2, maxj2, pi1, pi2;
      const unsigned np1 = square_npars(res.var_degree(0));
      for(auto& eit : trf.eigenvectors()) {
        i1 = eit[a1]; j1 = eit[a2];
        ep = trf.par(eit.par_idx());

        auto idx = eit.indices();
        idx.set(a1, 0);
        idx.set(a2, 0);
        maxi2 = std::min<unsigned>(i1, idx.max_idx(a1));
        for(i2=0; i2<=maxi2; ++i2) {
          idx.set(a1, i2);
          pi1 = square_paridx(i1,i2);
          maxj2 = std::min<unsigned>(j1, idx.max_idx(a2));
          for(j2=0; j2<=maxj2; ++j2) {
            pi2 = square_paridx(j1,j2);
            idx.set(a2, j2);
            res.par(pi1 + np1*pi2) += coeff_trf<TCoeff, Coeff>(ep*trf.par(idx.index()));
          }
        }
      }
    }

    static inline double square_integral(const _Transf& trf) {
      double s=0;
      for(auto& p : trf.parameters()) s += coefficient_norm(true, p);
      return s;
    }

    static inline double linear_integral(const _Transf& trf) {
      double s=0;
      for(auto& eit : trf.eigenvectors()) {
        // TODO: This can be speeded up
        double term=1.0;
        if(_EigenSys::integral(eit.indices(), term))
          s += term * coefficient_norm(false, trf.par(eit.par_idx()));
      }
      return s;
    }

    template<class _CoeffDst, class _CoeffSrc>
    static inline _CoeffDst coeff_trf(const _CoeffSrc& src) {
      return _coeff_trf<_CoeffDst, _CoeffSrc>::eval(src);
    }

    static inline void create_covariance(_Transf& /*trf*/, bool /*include_poisson*/) {}
  };

  template<class _Transf, class _EigenSysLeft, class _EigenSysRight>
  struct TransformImpl<_Transf, EigenSystem<_EigenSysLeft, _EigenSysRight>> : DefaultTransformImpl<_Transf, EigenSystem<_EigenSysLeft, _EigenSysRight>> {
    using EigenSys = EigenSystem<_EigenSysLeft, _EigenSysRight>;

    static inline TransformVariableBase* create_linear_axis_transform(const _Transf& trf, unsigned d, const std::string& name, const EigenSys& eigens, unsigned axis) {
      if(d >= eigens.dims()) return 0;
      else if(d < eigens.left_dims()) return TransformImpl<_Transf, t_EigenSystemLeft<EigenSys>>::create_linear_axis_transform(trf, d, name, eigens.left(), axis);
      else return TransformImpl<_Transf, t_EigenSystemRight<EigenSys>>::create_linear_axis_transform(trf, d - eigens.left_dims(), name, eigens.right(), axis);
    }

    static inline TransformVariableBase* create_amplitude_axis_transform(const _Transf& trf, unsigned d, const std::string& name, const EigenSys& eigens, unsigned axis) {
      if(d >= eigens.dims()) return 0;
      else if(d < eigens.left_dims()) return TransformImpl<_Transf, t_EigenSystemLeft<EigenSys>>::create_amplitude_axis_transform(trf, d, name, eigens.left(), axis);
      else return TransformImpl<_Transf, t_EigenSystemRight<EigenSys>>::create_amplitude_axis_transform(trf, d - eigens.left_dims(), name, eigens.right(), axis);
    }

    static inline TransformVariableBase* create_transform_1d(unsigned d, const std::string& name, const EigenSys& eigens) {
      if(d >= eigens.dims()) return 0;
      else if(d < eigens.left_dims()) return TransformImpl<_Transf, t_EigenSystemLeft<EigenSys>>::create_transform_1d(d, name, eigens.left());
      else return TransformImpl<_Transf, t_EigenSystemRight<EigenSys>>::create_transform_1d(d - eigens.left_dims(), name, eigens.right());
    }

    static inline TransformVariableBase* create_marginalized_2d(const _Transf& trf, unsigned d1, unsigned d2, const std::string& name, const EigenSys& eigens, unsigned axis1, unsigned axis2) {
      if((d1 >= d2) || (d2 >= eigens.dims())) return 0;
      else if(d2 < eigens.left_dims()) return TransformImpl<_Transf, t_EigenSystemLeft<EigenSys>>::create_marginalized_2d(trf, d1, d2, name, eigens.left(), axis1, axis2);
      else if(d1 >= eigens.left_dims()) return TransformImpl<_Transf, t_EigenSystemRight<EigenSys>>::create_marginalized_2d(trf, d1 - eigens.left_dims(), d2 - eigens.left_dims(), name, eigens.right(), axis1, axis2);
      else {
        // TODO: Implement hetrogenous 2D
        return 0;
      }
    }
  };
}

#endif // sfi_TransformImpl_h
