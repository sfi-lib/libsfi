#ifndef sfi_Transform_h
#define sfi_Transform_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <limits>
#include <array>
#include <memory>

#include "trans_tmp.h"
#include "defs.h"
#include "util.h"

#include "Evaluator.h"
#include "sfi_math.h"
#include "SliceEvaluator.h"
#include "MarginalizeEvaluator.h"
#include "Variables.h"
#include "ParamIterator.h"
#include "TransformBase.h"
#include "TransformImpl.h"

namespace sfi {
  // Create slice evaluator
  template<class _Transf, class _EigenSys>
  inline std::unique_ptr<IEvaluator> create_slice_evaluator(const _Transf* trf, const _EigenSys&, unsigned d2, bool _do_normalize, double scale = 1.0, bool external_coordinates = false, bool deriv = false, teif<c_is_compound_transform<_Transf>()>* = 0) {
    if((d2+1) == _EigenSys::dims()) {
      if(deriv) return std::unique_ptr<IEvaluator>(new SliceDerivEvaluator<_Transf, (_EigenSys::dims()-1)>(trf, _do_normalize, scale, external_coordinates));
      else return std::unique_ptr<IEvaluator>(new SliceEvaluator<_Transf, (_EigenSys::dims()-1)>(trf, _do_normalize, scale, external_coordinates));
    }
    else return 0;
  }

  template<class _Transf, class _EigenSys>
  inline std::unique_ptr<IEvaluator> create_slice_evaluator(const _Transf*, const _EigenSys&, unsigned, bool, double = 1.0, bool= false, bool = false, teif<!c_is_compound_transform<_Transf>()>* = 0) {
    return 0;
  }
  template<class _Transf, unsigned d>
  inline std::unique_ptr<IEvaluator> create_slice_evaluator_impl(const _Transf* trf, bool _do_normalize, double scale = 1.0, bool external_coordinates = false, bool deriv = false) {
    if(deriv) return std::unique_ptr<IEvaluator>(new SliceDerivEvaluator<_Transf, d>(trf, _do_normalize, scale, external_coordinates));
    else return std::unique_ptr<IEvaluator>(new SliceEvaluator<_Transf, d>(trf, _do_normalize, scale, external_coordinates));
  }

  template<class _Transf, class _EigLeft, class _EigRight>
  inline std::unique_ptr<IEvaluator> create_slice_evaluator(const _Transf* trf, const EigenSystem<_EigLeft, _EigRight>&, unsigned d2, bool _do_normalize, double scale = 1.0, bool external_coordinates = false, bool deriv = false, teif<!teq<t_EigenSystem<_Transf>, EigenSystem<_EigLeft, _EigRight>>::value()>* = 0) {
    using ES = EigenSystem<_EigLeft, _EigRight>;
    if((d2+1) == ES::dims()) return create_slice_evaluator_impl<_Transf, ES::dims()-1>(trf, _do_normalize, scale, external_coordinates, deriv);
    else if(d2 < _EigLeft::dims()) return create_slice_evaluator(trf, trf->eigen_system().left(), d2, _do_normalize, scale, external_coordinates, deriv);
    else return 0;
  }

  template<class _Transf, class _EigLeft, class _EigRight>
  inline std::unique_ptr<IEvaluator> create_slice_evaluator(const _Transf* trf, const EigenSystem<_EigLeft, _EigRight>&, unsigned d2, bool _do_normalize, double scale = 1.0, bool external_coordinates = false, bool deriv = false, teif<teq<t_EigenSystem<_Transf>, EigenSystem<_EigLeft, _EigRight>>::value()>* = 0) {
    if(d2 < _EigLeft::dims()) return create_slice_evaluator(trf, trf->eigen_system().left(), d2, _do_normalize, scale, external_coordinates, deriv);
    else return 0;
  }

  template<class ... _T, unsigned d2>
  struct _t_slice_transform<Transform<_T...>, d2> :
  tvalued<tcond<((d2+1) == Transform<_T...>::dims()), Transform<_T...>, void>> {};

  template<class ... _T1, class ... _T2,unsigned d2>
  struct _t_slice_transform<Transform<Transform<_T1...>, Transform<_T2...>>, d2> {
    using TransLeft = Transform<_T1...>;
    using TransRight = Transform<_T2...>;
    using tvalue =
        tcond<(d2 == (TransLeft::dims()-1)),
        TransLeft,
        tcond<(d2 < TransLeft::dims()),
        t_SliceTransform<TransLeft, d2>,
        Transform<TransLeft, t_SliceTransform<TransRight, d2 - TransLeft::dims()>>
        >
    >;
  };

  template<class ... _T1, class ... _T2>
  struct _t_is_compound_transform<Transform<Transform<_T1...>, Transform<_T2...>>> : ttrue {};

  template<class ..._T>
  struct _t_eigenvectors_iterator<Transform<_T...>> :
  tvalued<t_EigenVectorsIterator<t_EigenVectors<Transform<_T...>>>> {};

  // t_Left and t_RightTransform
  template<typename _T1, typename _T2>
  struct _t_left_transform<Transform<_T1, _T2>> : tvalued<Transform<_T1, _T2>> {};

  template<class ... _T1, class ... _T2>
  struct _t_left_transform<Transform<Transform<_T1...>, Transform<_T2...>>> : tvalued<Transform<_T1...>> {};

  template<class ... _T1, class ... _T2>
  struct _t_right_transform<Transform<Transform<_T1...>, Transform<_T2...>>> : tvalued<Transform<_T2...>> {};

  // Determine base of Transform
  template<typename _T1, typename _T2>
  struct _t_transform_base : tvalued<TransformEigenBase<t_EigenSystem<Transform<_T1, _T2>>>> { };

  template<class ... _T>
  struct _t_transform_base<EigenSystem<_T...>, void> : tvalued<TransformEigenBase<EigenSystem<_T...>>> {};

  template<typename _T1, typename _T2>
  using t_TransformBase = tvalue<_t_transform_base<_T1, _T2>>;

  template<class ... _T>
  using PTransform = std::shared_ptr<Transform<_T...>>;

  template<typename _T1, typename _T2>
  class Transform : public t_TransformBase<_T1, _T2> {
    using This = Transform<_T1, _T2>;
    using Base = t_TransformBase<_T1, _T2>;
    using Coeff = t_CoefficienctType<This>;
  public:
    using Eigenv = t_EigenVectors<Base>;
    using EigenSys = t_EigenSystem<This>;
    static_assert((EigenSys::is_orthonormal()), "Must be orthonormal");
    template<class _t, unsigned d1, unsigned d2, bool, class _t2> friend class SliceEvaluatorBase;

    Transform():Base() {
      static_assert((!EigenSys::is_dynamic()), "Must be called with EigenVector");
    }

    Transform(const std::string&name, bool _init_pars = false):Base(name) {
      static_assert((!EigenSys::is_dynamic()), "Must be called with EigenVector");
      if(_init_pars) this->init_params();
    }

    Transform(const Eigenv& ev, const std::string&name, bool _init_pars = false):Base(ev, name) {
      if(_init_pars) this->init_params();
    }

    Transform(const Transform& o):Base(o) { }

    Transform(Transform&& o):Base(std::forward<Transform>(o)) { }

    Transform(const OptionMap& om):Base(om) { }

    inline This& operator=(This&& o){
      if(this != &o) this->move_from(std::forward<This>(o));
      return *this;
    }

    inline This& operator=(const This& o){
      if(this != &o) this->copy_from(o);
      return *this;
    }

    virtual ~Transform() {}

    // ---- Implementation of ITransform ----

    virtual const std::string& get_type_name() const { return this->type_name(); }

    virtual const std::string& get_basis_name() const  { return EigenSys::basis_name(); }

    virtual unsigned get_max_degree() const { return max_degree(); }

    virtual unsigned get_dimensions() const { return this->dims(); }

    virtual unsigned get_var_degree(unsigned i) const {
      if(i >= This::dims()) throw TransformException("Invalid dimension "+std::to_string(i));
      return this->var_degree(i);
    }

    virtual unsigned get_nparams() const { return this->npar(); }

    virtual Qty get_param(unsigned i) const {
      if(i >= This::nparams()) throw TransformException("Invalid parameter index "+std::to_string(i));
      return this->param_qty(i);
    }

    virtual void set_param(unsigned i, double val);

    virtual std::unique_ptr<IParamIterator> get_param_iterator() const;

    virtual std::unique_ptr<IEvaluator> get_evaluator(double scale = 1.0, bool external_coordinates = false) const;

    virtual std::unique_ptr<IEvaluator> get_marginalize_evaluator_1D(unsigned axis, double scale = 1.0, bool external_coordinates = false) const {
      if(axis >= This::dims()) throw TransformException("Invalid axis "+std::to_string(axis));
      return std::unique_ptr<IEvaluator>(new MarginalizeEvaluator<This,1>(this, axis, external_coordinates, scale));
    }

    virtual std::unique_ptr<IEvaluator> get_marginalize_evaluator_2D(unsigned a1, unsigned a2, double scale = 1.0, bool external_coordinates = false) const {
      if((a1 >= This::dims()) || (a2 >= This::dims()) || (a1 >= a2)) throw TransformException("Invalid axes "+std::to_string(a1)+" "+std::to_string(a2));
      return std::unique_ptr<IEvaluator>(new MarginalizeEvaluator<This,2>(this, a1, a2, external_coordinates, scale));
    }

    virtual std::unique_ptr<IEvaluator> get_slice_evaluator(unsigned d2, bool _do_normalize, double scale = 1.0, bool external_coordinates = false) const {
      if(d2 >= This::dims()) throw TransformException("Invalid axis "+std::to_string(d2));
      return slice_evaluator(d2, _do_normalize, scale, external_coordinates);
    }

    virtual std::unique_ptr<IEvaluator> get_slice_derivative_evaluator(unsigned d2, bool _do_normalize, double scale = 1.0, bool external_coordinates = false) const {
      if(d2 >= This::dims()) throw TransformException("Invalid slice dimension");
      return slice_derivative_evaluator(d2, _do_normalize, scale, external_coordinates);
    }
    
    /** Pretty slow */
    virtual double get_value(const double* x) const;

    virtual double get_integral() const { return this->integral_pdf(); }

    virtual ITransform* get_axis_transform(unsigned d) const {
      if(d >= This::dims()) throw TransformException("Invalid dimension "+std::to_string(d));
      return this->axis_transform(d);
    }

    virtual ITransform* get_clone() const { return this->clone(); }

    virtual void do_dump(bool unc = false) const { this->dump(unc); }

    virtual void do_normalize(double norm) { this->normalize(norm); }

    virtual bool do_eval(unsigned npts, const double* xv, double* res, double scale = 1.0, bool external_coordinates = false) const;

    virtual bool do_full_eval(const Matrix<double>& points, Matrix<double>& res, bool external_coordinates = false) const {
      return full_eval_impl(points, res, external_coordinates);
    }

    virtual const MatrixSym<double>* do_create_covariance(bool poisson) {
      create_covariance(poisson);
      return &this->covariance();
    }

    virtual bool do_copy(const ITransform* trf) {
      const This* ttrf = dynamic_cast<const This*>(trf);
      if(ttrf && (ttrf != this)) {
        *this = *ttrf;
        return true;
      }
      else return false;
    }

    // ---- Transform operations ----

    /** @return Name of basis functions */
    inline const std::string& basis_name() const  { return EigenSys::basis_name(); }

    /** @return Maximum degree for all dimensions */
    inline unsigned max_degree() const { return Base::eigen_system().max_degree(); }

    /** @return Degree for variable d */
    inline unsigned var_degree(unsigned d) const { return Base::eigen_system().var_degree(d); }

    /** @return If the basis is orthonormal */
    static constexpr inline bool is_orthonormal() { return EigenSys::is_orthonormal(); }

    using Base::compute_covariance;
    using Base::compute_uncertainty;

    inline This& compute_covariance(bool b) {
      this->do_compute_covariance(b, this->npar());
      return *this;
    }

    inline This& compute_uncertainty(bool b) {
      this->do_compute_uncertainty(b, this->npar());
      return *this;
    }

    inline const std::string& type_name() const {
      static std::string s_type_name(sfi::type_name<This>());
      return s_type_name;
    }

    inline This* clone(bool=true) const { return new This(*this); }

    /** @return Iterator over the Transform parameters */
    inline std::unique_ptr<ParamIterator<This>> param_iterator() {
      return std::unique_ptr<ParamIterator<This>>(new ParamIterator<This>(this));
    }
    inline std::unique_ptr<ParamIterator<const This>> param_iterator() const {
      return std::unique_ptr<ParamIterator<const This>>(new ParamIterator<const This>(this));
    }

    /** @return Evaluator for this Transform */
    inline std::unique_ptr<Evaluator<This>> evaluator(double scale = 1.0, bool external_coordinates = false) const {
      return std::unique_ptr<Evaluator<This>>(new Evaluator<This>(this, scale, external_coordinates));
    }

    using EvalData = t_EvalData<EigenSys>;

    inline void full_project(const double* x, EvalData& p) const { compute_all<Basis::Project>(x, p); }

    inline void init_eval(EvalData& p) const { Base::eigen_system().init_eval(p); }

    template<Basis::CompType ctype, class _Func>
    inline _Func compute(const double* x, EvalData& p, const _Func& f) const {
      return Base::eigen_system().template compute<ctype>(x, p, f);
    }

    template<Basis::CompType ctype>
    inline void compute_all(const double* _x, EvalData& p) const {
      Base::eigen_system().template precompute<ctype>(_x, p);
    }

    /** Evaluate dimension d at point _x, store in res */
    inline void evaln(double _x, unsigned d, Coeff* res) const {
      Base::eigen_system().template precompute<Basis::Eval>(this->eigenvectors(), d, _x, res);
    }

    /**
     * Evaluate at multiple points, store coefficients in matrix
     * @param points Points to evaluate at
     * @param res matrix of coefficients
     * @param external_coordinates If points are given in external coordinates
     */
    bool full_eval(const Matrix<double>& points, Matrix<Coeff>& res, bool external_coordinates = false) const;

    /**
     *  Compute value of all basis vectors at one point for all dimensions
     *  TODO: Remove!
     */
    inline void full_eval(const double* x, EvalData& p, std::vector<Coeff>& res) const {
      assert(res.size() == this->npar() && "Result vector size mismatch");
      Base::eigen_system().template compute<Basis::Eval>(x, p, _s_full_eval(res.data()));
    }

    /**
     * Evaluate transform at point x
     * @param x point to evaluate at
     * @param p EvalData instance for tmp storage
     * @param external_coordinates if the point is in external corrdinates
     */
    double eval(const double* x, EvalData& p, bool external_coordinates = false) const;

    void eval_gradient(const double* x, EvalData& p, EvalData& dp, Vector<Coeff>& grad, bool external_coordinates = false);

    /**
     * Evaluate multiple points.
     * @param npts Number of points
     * @param xv Data, should be of size npts * dims
     * @param res Result vector of size npts
     */
    inline bool eval(unsigned npts, const double* xv, double* res, double scale = 1.0, bool external_coordinates = false) const {
      EvalData ed;
      this->init_eval(ed);
      return eval(ed, npts, xv, res, scale, external_coordinates);
    }
    bool eval(EvalData& ed, unsigned npts, const double* xv, double* res, double scale = 1.0, bool external_coordinates = false) const;

    /** @return Integral of the squared transform */
    inline double square_integral() const { return TransformImpl<This, EigenSys>::square_integral(*this); }

    /** @return Integral over all eigenvectors */
    inline double integral_pdf() const {
      if(this->m_is_amplitude) return TransformImpl<This, EigenSys>::square_integral(*this);
      else return TransformImpl<This, EigenSys>::linear_integral(*this);
    }

    /** Ensure that the integral of this transform has the given value. */
    inline void normalize(double norm) {
      if(this->is_amplitude()) this->scale(sqrt(norm/this->integral_pdf()));
      else this->scale(norm/this->integral_pdf());
    }

    /**
     * Will call axis_transform(axis,trf) to fill the instance.
     * The instance may be reused to get the transform along another axis.
     * @return Axis transform for given axis.
     */
    TransformVariableBase* axis_transform(unsigned axis) const;

    /** @return 2D marginalized transform */
    TransformVariableBase* marginalize_2d(unsigned a1, unsigned a2) const;

    template<unsigned d2>
    std::unique_ptr<SliceEvaluator<This, d2>>
    slice_evaluator(bool _do_normalize, double scale = 1.0, bool external_coordinates = false) const;

    inline std::unique_ptr<IEvaluator> slice_evaluator(unsigned d2, bool _do_normalize, double scale = 1.0, bool external_coordinates = false) const {
      return create_slice_evaluator(this, This::eigen_system(), d2, _do_normalize, scale, external_coordinates, false);
    }
    
    inline std::unique_ptr<IEvaluator> slice_derivative_evaluator(unsigned d2, bool _do_normalize, double scale = 1.0, bool external_coordinates = false) const {
      return create_slice_evaluator(this, This::eigen_system(), d2, _do_normalize, scale, external_coordinates, true);
    }
    
    /** @return Norm for given dimension/variable and degree */
    static inline double norm(unsigned var, unsigned n) { return EigenSys::norm(var, n); }

    /**
     * Create the covariance matrix given the coefficiencts.
     * For basis functions f_i and f_j and coefficients c_i and c_j:
     * cov(c_i,c_j) = <(f_i - <f_i>)(f_j - <f_j>)>
     *    = <f_i f_j> - <f_i><f_j> = <f_i f_j> - c_i c_j
     * In general f_i f_j can be expanded as a sum:
     * f_i f_j = sum a_n f_n
     * which gives the characteristics of the covariance matrix.
     *
     * This makes the approximation that i+j<=N.
     * If an overall poisson term should be included this amounts to -removing- the term -c_i c_j.
     * @param include_poisson If the poisson term should be included
     */
    inline void create_covariance(bool include_poisson) {
      TransformImpl<This, EigenSys>::create_covariance(*this, include_poisson);
    }
  protected:
    /** @return Turn value of underlying transform into PDF value */
    inline double to_pdf(double c) const { return this->is_amplitude() ? c*c : c; }
  public:
    /**
     * TODO: May need to include vtrans weight?
     */
    template<unsigned d2>
    void slice(const double* point, t_SliceTransform<This, d2>& res, EvalData &ed, bool _do_normalize, bool external_coordinates) const;

    /**
     * Perform just the slicing, given inialized workspace ed
     */
    template<unsigned d2>
    void slice_impl(t_SliceTransform<This, d2>& res, EvalData &ed, bool _do_normalize) const;
    
    /**
     * Evaluate all basis vectors up to second derivative
     * @param point The values of the slice parameters
     * @param external_coordinates If external coordinates are used for the slice parameters
     */
    template<unsigned d2>
    void eval_deriv(const double* point, EvalData &ed, EvalData &ded, EvalData &d2ed, bool external_coordinates) const;
    
    /**
     * Compute the slice derivative, i.e. the derivative of a slice with respect to one of the silice parameters.
     * The internal workspace must have been initialized with eval_deriv before.
     * @param parnr The parameter to take the derivative with
     * @param res The resulting Transform
     * @param ed Internal workspace
     * @param ded Internal workspace
     * @param d2ed Internal workspace
     * @param _do_normalize If result should be normalized
     */
    template<unsigned d2>
    void slice_derivative(unsigned parnr,
                          t_SliceTransform<This, d2>& res, t_SliceTransform<This, d2>& dres, t_SliceTransform<This, d2>& d2res,
                          EvalData &ed, EvalData &ded, EvalData &d2ed, EvalData& work, bool _do_normalize, bool _external_coordinates) const;
  protected:
    struct _s_full_eval {
      _s_full_eval(Coeff* res):m_res(res) {}
      inline void operator()(const Coeff& v) {
        *m_res = v;
        ++m_res;
      }
      Coeff* m_res;
    };

    template<class _T>
    struct _s_eval_dbl {
      _s_eval_dbl(const _T* ptr):m_ptr(ptr), m_res(0.0)  {}
      inline void operator()(_T v) {
        m_res += v*(*m_ptr);
        ++m_ptr;
      }
      const _T* m_ptr;
      _T m_res;
    };

    template<class _T>
    struct _s_eval_cplx {
      _s_eval_cplx(const std::complex<_T>* ptr):m_ptr(ptr), m_res(0.0)  {}
      inline void operator()(const std::complex<_T>& v) {
        m_res += v.real()*m_ptr->real() - v.imag()*m_ptr->imag();
        ++m_ptr;
      }
      const std::complex<_T>* m_ptr;
      _T m_res;
    };

    template<class _T>
    inline bool full_eval_impl(const Matrix<double>& points, Matrix<_T>& res, bool external_coordinates, teif<teq<_T,Coeff>::value()>* = 0) const {
      return full_eval(points, res, external_coordinates);
    }
    template<class _T>
    inline bool full_eval_impl(const Matrix<double>&, Matrix<_T>&, bool /*external_coordinates*/, teif<!teq<_T,Coeff>::value()>* = 0) const {
      return false;
    }

    using _s_eval = tcond<teq<Coeff,std::complex<double>>::value(), _s_eval_cplx<double>, _s_eval_dbl<double>>;
  };

  template<typename _T1, typename _T2>
  void Transform<_T1, _T2>::set_param(unsigned i, double val) {
    if(i >= This::nparams()) throw TransformException("Invalid parameter index "+std::to_string(i));
    this->par(i) = val;
  }

  template<typename _T1, typename _T2>
  double Transform<_T1, _T2>::get_value(const double* x) const {
    EvalData ed;
    init_eval(ed);
    return eval(x, ed);
  }

  template<typename _T1, typename _T2> std::unique_ptr<IParamIterator>
  Transform<_T1, _T2>::get_param_iterator() const {
    return std::unique_ptr<IParamIterator>(new ParamIterator<const This>(this));
  }

  template<typename _T1, typename _T2> std::unique_ptr<IEvaluator>
  Transform<_T1, _T2>::get_evaluator(double scale, bool external_coordinates) const {
    return std::unique_ptr<IEvaluator>(new Evaluator<This>(this, scale, external_coordinates));
  }

  template<typename _T1, typename _T2>
  bool Transform<_T1, _T2>::do_eval(unsigned npts, const double* xv, double* res, double scale, bool external_coordinates) const {
    return this->eval(npts, xv, res, scale, external_coordinates);
  }

  template<typename _T1, typename _T2>
  bool Transform<_T1, _T2>::full_eval(const Matrix<double>& points, Matrix<Coeff>& res, bool external_coordinates) const {
    if(points.cols() != This::dims()) return false;
    const unsigned npts = points.rows();
    if(npts != res.rows()) return false;
    if(res.cols() != this->npar()) return false;
    EvalData p;
    this->init_eval(p);

    // TODO: Assumption about data layout of Matrix
    if(external_coordinates) {
      double xo[This::dims()];
      for(unsigned i=0; i<npts; ++i) {
        this->variables()->transform(points.row_data(i), xo);
        Base::eigen_system().template compute<Basis::Eval>(xo, p, _s_full_eval(res.row_data(i)));
      }
    }
    else {
      for(unsigned i=0; i<npts; ++i) {
        Base::eigen_system().template compute<Basis::Eval>(points.row_data(i), p, _s_full_eval(res.row_data(i)));
      }
    }
    return true;
  }

  template<typename _T1, typename _T2>
  inline double Transform<_T1, _T2>::eval(const double* x, EvalData& p, bool external_coordinates) const {
    if(external_coordinates) {
      double xi[This::dims()];
      double w = this->m_variables->transform(x, xi);
      auto res = Base::eigen_system().template compute<Basis::Eval>(xi, p, _s_eval(this->m_par.data_ptr()));
      return w*this->to_pdf(res.m_res);
    }
    else {
      auto res = Base::eigen_system().template compute<Basis::Eval>(x, p, _s_eval(this->m_par.data_ptr()));
      return this->to_pdf(res.m_res);
    }
  }

  template<typename _T1, typename _T2>
  inline void Transform<_T1, _T2>::eval_gradient(const double* x, EvalData& p, EvalData& dp, Vector<Coeff>& grad, bool /*external_coordinates*/) {
    grad.rows(This::dims());
    grad.clear();
    for(unsigned int j=0; j<This::dims(); ++j) {
      Base::eigen_system().template precompute<Basis::Eval>(this->eigenvectors(), j, x[j], p[j]);
      Base::eigen_system().template precompute<Basis::EvalDeriv>(this->eigenvectors(), j, x[j], dp[j]);
    }

    Coeff term(0.0);
    const Coeff* par_ptr = this->m_par.data().data();
    for(auto& eit : this->eigenvectors()) {
      for (unsigned int i=0; i<This::dims(); ++i) {
        term = *par_ptr;
        for (unsigned int j=0; j<This::dims(); ++j) term *= (i == j) ? dp[j][eit[j]] : p[j][eit[j]];
        grad[i] += term;
      }
      ++par_ptr;
    }
  }

  template<typename _T1, typename _T2>
  inline bool Transform<_T1, _T2>::eval(EvalData& ed, unsigned npts, const double* xv, double* res, double scale, bool external_coordinates) const {
    const double* x = xv;
    for(unsigned ev = 0; ev < npts; ++ev) {
      res[ev] = scale * this->eval(x, ed, external_coordinates);
      x += This::dims();
    }
    return true;
  }

  template<typename _T1, typename _T2>
  TransformVariableBase*
  Transform<_T1, _T2>::axis_transform(unsigned axis) const {
    assert(axis < This::dims() && "Invalid axis");
    TransformVariableBase* res = 0;
    if(this->is_amplitude()) res = TransformImpl<This, EigenSys>::create_amplitude_axis_transform(*this, axis, this->name()+":"+std::to_string(axis), This::eigen_system(), axis);
    else res = TransformImpl<This, EigenSys>::create_linear_axis_transform(*this, axis, this->name()+":"+std::to_string(axis), This::eigen_system(), axis);
    if(!res) throw TransformException("axis_transform() Unable to create transform for axis "+std::to_string(axis));
    if(this->m_variables) {
      PVariables vars = std::make_shared<Variables>(1);
      (*vars)[0] = (*this->m_variables)[axis];
      res->variables(vars);
    }
    return res;
  }

  template<typename _T1, typename _T2>
  TransformVariableBase* Transform<_T1, _T2>::marginalize_2d(unsigned a1, unsigned a2) const {
    if((a1 >= a2) || (a2 >= This::dims())) return 0;
    // TODO: Name
    auto *res = TransformImpl<This, EigenSys>::create_marginalized_2d(*this, a1, a2, "", This::eigen_system(), a1, a2);
    if(!res) throw TransformException("marginalize_2d() Unable to create transform for axes "+std::to_string(a1)+", "+std::to_string(a2));
    if(this->m_variables) {
      auto vars = std::make_shared<Variables>(2);
      (*vars)[0] = (*this->m_variables)[a1];
      (*vars)[1] = (*this->m_variables)[a2];
      res->variables(vars);
    }
    return res;
  }

  template<typename _T1, typename _T2>
  template<unsigned d2>
  std::unique_ptr<SliceEvaluator<Transform<_T1, _T2>, d2>>
  Transform<_T1, _T2>::slice_evaluator(bool _do_normalize, double scale, bool external_coordinates) const {
    static_assert((d2 < This::dims()), "Invalid dimensions");
    return std::unique_ptr<SliceEvaluator<This, d2>>(new SliceEvaluator<This, d2>(this, _do_normalize, scale, external_coordinates));
  }

  template<typename _T1, typename _T2>
  template<unsigned d2>
  void Transform<_T1, _T2>::slice(const double* point, t_SliceTransform<This, d2>& res, EvalData &ed, bool _do_normalize, bool external_coordinates) const {
    static_assert(This::dims() > 1, "Must be of dimension at least 2");
    static_assert((d2 < This::dims()), "Invalid dimensions");
    assert((this->m_variables != 0) && "Must have variables");

    if(!res.init_params()) res.clear();
    unsigned k=0;
    if(external_coordinates) {
      double x;
      for(unsigned j=d2+1; j<This::dims(); ++j, ++k) {
        this->m_variables->at(j).vtrans(point[k], x);
        evaln(x, j, ed[j]);
      }
    }
    else {
      for(unsigned j=d2+1; j<This::dims(); ++j, ++k) evaln(point[k], j, ed[j]);
    }
    slice_impl<d2>(res, ed, _do_normalize);
  }

  template<typename _T1, typename _T2>
  template<unsigned d2>
  void Transform<_T1, _T2>::slice_impl(t_SliceTransform<This, d2>& res, EvalData &ed, bool _do_normalize) const {
    using TCoeff = t_CoefficienctType<t_SliceTransform<This, d2>>;
    unsigned k = 0;
    auto& eigenv = res.eigenvectors();
    const unsigned nparams = eigenv.params();
    auto eit = eigenv.iterator();
    // TODO: Store in slice?
    auto outer_eigenv = slice_outer_eigenvectors<Eigenv, d2>(this->eigenvectors());
    auto eoit = outer_eigenv.iterator();
    do {
      Coeff p(0);
      eoit.clear();
      unsigned l(0);
      do {
        Coeff term = 1.0;
        for(unsigned int j=d2+1; j<This::dims(); ++j) term *= ed[j][eoit[j - d2 - 1]];
        p += this->par(nparams*l + k)*term;
        ++l;
      } while(eoit.next());
      res.par(k) = TransformImpl<This, EigenSys>::template coeff_trf<TCoeff, Coeff>(p);
      ++k;
    } while(eit.next());
    if(_do_normalize) res.normalize(1.0);
  }

  template<typename _T1, typename _T2>
  template<unsigned d2>
  void Transform<_T1, _T2>::eval_deriv(const double* point, EvalData &ed, EvalData &ded, EvalData &d2ed, bool external_coordinates) const {
    unsigned k=0;
    if(external_coordinates) {
      double x;
      for(unsigned j=d2+1; j<This::dims(); ++j, ++k) {
        this->m_variables->at(j).vtrans(point[k], x);
        Base::eigen_system().template precompute<Basis::Eval>(this->eigenvectors(), j, x, ed[j]);
        Base::eigen_system().template precompute<Basis::EvalDeriv>(this->eigenvectors(), j, x, ded[j]);
        Base::eigen_system().template precompute<Basis::EvalDeriv2>(this->eigenvectors(), j, x, d2ed[j]);
      }
    }
    else {
      for(unsigned j=d2+1; j<This::dims(); ++j, ++k) {
        Base::eigen_system().template precompute<Basis::Eval>(this->eigenvectors(), j, point[k], ed[j]);
        Base::eigen_system().template precompute<Basis::EvalDeriv>(this->eigenvectors(), j, point[k], ded[j]);
        Base::eigen_system().template precompute<Basis::EvalDeriv2>(this->eigenvectors(), j, point[k], d2ed[j]);
      }
    }
  }

  template<typename _T1, typename _T2>
  template<unsigned d2>
  void Transform<_T1, _T2>::slice_derivative(unsigned parnr,
                        t_SliceTransform<This, d2>& res, t_SliceTransform<This, d2>& dres, t_SliceTransform<This, d2>& d2res,
                        EvalData &ed, EvalData &ded, EvalData &d2ed, EvalData& work, bool _do_normalize, bool _external_coordinates) const {
    static_assert(This::dims() > 1, "Must be of dimension at least 2");
    static_assert((d2 < This::dims()), "Invalid dimensions");
    assert((this->m_variables != 0) && "Must have variables");
    assert(res.is_amplitude() && "Must be an amplitude");
    
    if(!res.init_params()) res.clear();
    if(!dres.init_params()) dres.clear();
    if(!d2res.init_params()) d2res.clear();
    
    // Get the derivative of the variable transformation
    // TODO: Only works for linear transformation.
    double dummy=0;
    auto& var = this->m_variables->at(parnr);
    const double weight = _external_coordinates ? var.vtrans(var.minval(), dummy) : 1.0;
    
    const unsigned Npar = res.npar();
    // Get the slice, don't normalize it yet
    slice_impl<d2>(res, ed, false);

    // Get the slice derivative
    Base::eigen_system().copy_eval(ed, work);
    Base::eigen_system().copy_eval(ded,work,parnr);
    slice_impl<d2>(dres, ded, false);
    
    // Get the slice second derivative
    Base::eigen_system().copy_eval(d2ed,work,parnr);
    slice_impl<d2>(d2res, d2ed, false);
    
    if(_do_normalize) {
      auto& p = res.parameters();
      auto& dp = dres.parameters();
      auto& d2p = d2res.parameters();

      const double sumb = res.integral_pdf();
      // this is the normalization factor
      const double s = 1.0/sqrt(sumb);

      double da(0.0), d2a(0.0);
      for(unsigned i=0; i<Npar; ++i) {
        da += p[i]*dp[i];
        d2a += square(dp[i]) + p[i]*d2p[i];
      }
      da *= 2.0;
      d2a *= 2.0;

      const double ds(-0.5*std::pow(sumb,-3.0/2.0)*da);
      const double d2s((3.0/4.0)*std::pow(sumb,-5.0/2.0)*square(da) - 0.5*std::pow(sumb,-3.0/2.0)*d2a);
      
      for(unsigned i=0; i<Npar; ++i) {
        dp[i] = ds*p[i] + s*dp[i];
        d2p[i] = d2s*p[i] + 2.0*ds*dp[i] + s*d2p[i];
      }
      dp *= weight;
      d2p *= square(weight);
      res.scale(s);
    }
  }

  template<class ..._T1, class ..._T2>
  class ParamIteratorIndexBase<Transform<Transform<_T1...>, Transform<_T2...>>> :
  public ParamIteratorBase<Transform<Transform<_T1...>, Transform<_T2...>>> {
  public:
    using Transf = Transform<Transform<_T1...>, Transform<_T2...>>;
    ParamIteratorIndexBase(const t_EigenVectorsIterator<Transf>& it):ParamIteratorBase<Transf>(it) { }

    virtual ~ParamIteratorIndexBase() { }
  };

  template<class _Eigen, class _TType>
  class ParamIterator<Transform<_Eigen, _TType>> : public ParamIteratorIndexBase<Transform<_Eigen, _TType>> {
  public:
    using Transf = Transform<_Eigen, _TType>;
    using Base = ParamIteratorIndexBase<Transf>;

    ParamIterator(Transf* tr):Base(tr->eigenvectors().iterator()), m_trans(tr) { }

    virtual ~ParamIterator() { }

    // ---- implementation of IParamIterator ----

    virtual Qty get_value() { return m_trans->param_qty(this->m_iter.par_idx()); }

    // ---- public methods ----

    inline double& par() { return m_trans->par(this->m_iter.par_idx()); }

    inline double& par_unc() { return m_trans->par_unc(this->m_iter.par_idx()); }
  protected:
    Transf* m_trans;
  };

  template<class _Eigen, class _TType>
  class ParamIterator<const Transform<_Eigen, _TType>> : public ParamIteratorIndexBase<Transform<_Eigen, _TType>> {
  public:
    using Transf = Transform<_Eigen, _TType>;
    using Base = ParamIteratorIndexBase<Transf>;

    ParamIterator(const Transf* tr):Base(tr->eigenvectors().iterator()), m_trans(tr) { }

    virtual ~ParamIterator() { }

    // ---- implementation of IParamIterator ----

    virtual Qty get_value() { return m_trans->param_qty(this->m_iter.par_idx()); }

    // ---- public methods ----

    inline double par() { return m_trans->par(this->m_iter.par_idx()); }

    inline double par_unc() { return m_trans->par_unc(this->m_iter.par_idx()); }
  protected:
    const Transf* m_trans;
  };
}

#endif // sfi_Transform_h
