#ifndef sfi_EventSelector_h
#define sfi_EventSelector_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <inttypes.h>
#include <stddef.h>

namespace sfi {
  /**
   * Interface for EventSelectors
   * The event selection mechnism is most efficient if the  exported interface is used.
   */
  struct IEventSelector {
    virtual ~IEventSelector() {}

    virtual bool do_select(unsigned i) const = 0;

    virtual double get_fraction() const = 0;
  };

  template<unsigned, class> class Sample;

  /**
   * @class EvenEventSel
   *
   * Select even events from a Sample.
   *
   * An EventSelector should export the interface:
   * bool operator()(i) - Return if event number i should be accepted
   * double fraction() - Fraction of events selected
   * Selector select(Sample s) - Return a selector, that provides iterators for the event data
   *
   * EventSelectors are able to split the selected subsample in approximately equal parts.
   * This feature is used to enable parallelization of certain computations.
   */
  struct EvenEventSel : public IEventSelector {
    EvenEventSel(uint8_t nparts = 1, uint8_t partnr = 0):m_nparts(nparts), m_partnr(partnr) {}

    virtual ~EvenEventSel() {}

    virtual bool do_select(unsigned i) const { return (*this)(i); };

    virtual double get_fraction() const { return fraction(); }

    inline bool operator()(unsigned i) const { return (i&1) == 0; }

    inline double fraction() const { return (m_nparts == 1) ? 0.5 : 0.5/double(m_nparts); }

    template<unsigned _NDims, class _SplUseWeight>
    struct Selector {
      using SampleTy = Sample<_NDims, _SplUseWeight>;
      Selector(const SampleTy& sample, uint8_t nparts, uint8_t partnr):m_sample(sample), m_nparts(nparts), m_partnr(partnr) { }

      struct iterator {
        iterator(size_t index, const SampleTy& sample):m_index(index), m_sample(sample) {}

        inline typename SampleTy::EvDataConstRef operator*() { return m_sample[m_index]; }

        inline iterator& operator++() { m_index += 2; return *this; }

        inline bool operator!=(const iterator& o) const { return m_index < o.m_index; }

        inline size_t index() const { return this->m_index; }
      protected:
        size_t m_index;
        const SampleTy& m_sample;
      };
      inline iterator begin() { return iterator((m_nparts == 1) ? 0 : ((((size_t)(m_partnr * m_sample.events()/double(m_nparts))) + 0) & ~((size_t)1)), m_sample); }
      inline iterator end() {
        if((m_nparts == 1) || (m_partnr == (m_nparts - 1))) return iterator(m_sample.events(), m_sample);
        else return iterator(((size_t)((m_partnr + 1) * m_sample.events()/double(m_nparts)) + 0) & ~((size_t)1), m_sample);
      }
    protected:
      const SampleTy& m_sample;
      uint8_t m_nparts, m_partnr;
    };

    template<unsigned _NDims, class _SplUseWeight>
    inline Selector<_NDims, _SplUseWeight> select(const Sample<_NDims, _SplUseWeight>& spl) const {
      return Selector<_NDims, _SplUseWeight>(spl, m_nparts, m_partnr);
    }
  protected:
    uint8_t m_nparts, m_partnr;
  };

  /**
   * @class OddEventSel
   *
   * Select odd events.
   * @see EvenEventSel
   */
  struct OddEventSel : public IEventSelector {
    OddEventSel(uint8_t nparts = 1, uint8_t partnr = 0):m_nparts(nparts), m_partnr(partnr) {}

    virtual ~OddEventSel() {}

    virtual bool do_select(unsigned i) const { return (*this)(i); };

    virtual double get_fraction() const { return fraction(); }

    inline bool operator()(unsigned i) const { return (i&1) == 1; }

    inline double fraction() const { return (m_nparts == 1) ? 0.5 : 0.5/double(m_nparts); }

    template<unsigned _NDims, class _SplUseWeight>
    struct Selector {
      using SampleTy = Sample<_NDims, _SplUseWeight>;
      Selector(const SampleTy& sample, uint8_t nparts, uint8_t partnr):m_sample(sample), m_nparts(nparts), m_partnr(partnr) { }

      struct iterator {
        iterator(size_t index, const SampleTy& sample):m_index(index), m_sample(sample) {}

        inline typename SampleTy::EvDataConstRef operator*() { return m_sample[m_index]; }

        inline iterator& operator++() { m_index += 2; return *this; }

        inline bool operator!=(const iterator& o) const { return m_index < o.m_index; }

        inline size_t index() const { return this->m_index; }
      protected:
        size_t m_index;
        const SampleTy& m_sample;
      };
      inline iterator begin() { return iterator((m_nparts == 1) ? 1 : (((size_t)(m_partnr * m_sample.events()/double(m_nparts))) | ((size_t)1)), m_sample); }
      inline iterator end() {
        if((m_nparts == 1) || (m_partnr == (m_nparts - 1))) return iterator(m_sample.events(), m_sample);
        else return iterator(((size_t)((m_partnr + 1)* m_sample.events()/double(m_nparts))) | ((size_t)1), m_sample);
      }
    protected:
      const SampleTy& m_sample;
      uint8_t m_nparts, m_partnr;
    };

    template<unsigned _NDims, class _SplUseWeight>
    inline Selector<_NDims, _SplUseWeight> select(const Sample<_NDims, _SplUseWeight>& spl) const {
      return Selector<_NDims, _SplUseWeight>(spl, m_nparts, m_partnr);
    }
  protected:
    uint8_t m_nparts, m_partnr;
  };

  /**
   * @class AllEventSel
   *
   * Select all events.
   * @see EvenEventSel
   */
  struct AllEventSel : public IEventSelector {
    AllEventSel(uint8_t nparts = 1, uint8_t partnr = 0):m_nparts(nparts), m_partnr(partnr) {}

    virtual ~AllEventSel() {}

    virtual bool do_select(unsigned) const { return true; };

    virtual double get_fraction() const { return 1.0; }

    inline bool operator()(unsigned) const { return true; }

    inline double fraction() const { return (m_nparts == 1) ? 1.0 : 1.0/double(m_nparts); }

    template<unsigned _NDims, class _SplUseWeight>
    struct Selector {
      using SampleTy = Sample<_NDims, _SplUseWeight>;
      Selector(const SampleTy& sample, uint8_t nparts, uint8_t partnr):m_sample(sample), m_nparts(nparts), m_partnr(partnr) { }

      struct iterator {
        iterator(size_t index, const SampleTy& sample):m_index(index), m_sample(sample) { }

        inline typename SampleTy::EvDataConstRef operator*() { return m_sample[m_index]; }

        inline iterator& operator++() { ++m_index; return *this; }

        inline bool operator!=(const iterator& o) const { return m_index != o.m_index; }

        inline size_t index() const { return this->m_index; }
      protected:
        size_t m_index;
        const SampleTy& m_sample;
      };
      inline iterator begin() { return iterator((m_nparts == 1) ? 0 : (m_partnr * m_sample.events()/m_nparts) , m_sample); }
      inline iterator end() {
        if((m_nparts == 1) || (m_partnr == (m_nparts - 1))) return iterator(m_sample.events(), m_sample);
        else return iterator(((m_partnr + 1)* m_sample.events()/m_nparts), m_sample);
      }
    protected:
      const SampleTy& m_sample;
      uint8_t m_nparts, m_partnr;
    };

    template<unsigned _NDims, class _SplUseWeight>
    inline Selector<_NDims, _SplUseWeight> select(const Sample<_NDims, _SplUseWeight>& spl) const {
      return Selector<_NDims, _SplUseWeight>(spl, m_nparts, m_partnr);
    }
  protected:
    uint8_t m_nparts, m_partnr;
  };

  enum EventSelectorID {
    AllEventSelID = 0,
    EvenEventSelID = 1,
    OddEventSelID = 2
  };
}

#endif // sfi_EventSelector_h
