#ifndef sfi_TransBase_h
#define sfi_TransBase_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "trans_tmp.h"

#include <complex>

/**
 * Common functionality for bases.
 */
namespace sfi {
  /**
   * @class Basis
   * Provides default implementations of the exported Basis interface
   */
  struct Basis {
    static inline double weight(double /*_x*/) { return 1.0; }

    /** Special case, norm for a single dimension */
    static constexpr inline double inverse_single_norm_square(int /*deg*/) { return 1.0; }

    enum CompType { Eval, Project, EvalDeriv, EvalDeriv2 };

    using Coeff = double;
  };

  template<bool _is_orthonormal = false, bool _is_weighted = false>
  struct BasisImpl : public Basis {
    static constexpr bool is_orthonormal = _is_orthonormal;

    static constexpr bool is_weighted = _is_weighted;
  };

  inline double coefficient_norm(bool is_amp, double c) { return is_amp ? c*c : c; }

  inline static double coefficient_norm(bool is_amp, const std::complex<double>& c) {
    return is_amp ? std::norm(c) : std::real(c);
  }
}

#endif // sfi_TransBase_h
