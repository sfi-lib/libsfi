#ifndef sfi_ITransform_h
#define sfi_ITransform_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string>
#include <memory>

#include "defs.h"
#include "stat.h"
#include "ReferenceSet.h"
#include "Exception.h"
#include "Options.h"

namespace sfi {

  class ITransform;
  class Variables;
  using PVariables = std::shared_ptr<Variables>;
  class Variable;
  class ISample;
  // TODO: Change to shared_ptr
  using PSample = const ISample*;

  /**
   * @class IEvaluator
   *
   * Interface with methods for evaluating transforms.
   */
  class IEvaluator {
  public:
    virtual ~IEvaluator() {}

    /** Evaluate the Transform */
    virtual double get_value(const double* x) = 0;

    /** @return The underlying Transform */
    virtual const ITransform* get_transform() = 0;

    /** Set scale for evaluation */
    virtual void set_scale(double scale) = 0;

    /** If external coordinates are used */
    virtual void set_external_coordinates(bool ec) = 0;

    /** @return Number of dimensions, may not be the same as for the ITransform */
    virtual unsigned get_dimensions() const = 0;

    /** @see ITransform::do_eval */
    virtual bool do_eval(unsigned npts, const double* xv, double* res) = 0;

    /** @return Variable for -evaluator- dimension d */
    virtual const Variable& get_variable(unsigned d) const = 0;

    /** @return A clone of this, if deep then clone the underlying transform as well. */
    virtual IEvaluator* get_clone(bool deep) const = 0;

    /** @return Number of parameters to the slice */
    virtual unsigned get_nslice_parameters() const = 0;

    /** Evaluate the underlying transform at given point */
    virtual void set_slice(const double *c) = 0;

    /** Get the transform for the current slice (set_slice must be called first) */
    virtual const ITransform* get_slice_transform() = 0;
  };

  /**
   * @class IParamIterator
   *
   * Interface for iterators of the parameters in a transform.
   * Usage:
   * IParamIterator* pi = ...
   * do {
   *   pi->get_value();
   * } while(pi->do_next());
   */
  class IParamIterator {
  public:
    virtual ~IParamIterator() { }

    /**
     * Advance the iterator.
     * @return true as long as there is a current element.
     */
    virtual bool do_next() = 0;

    /** @return The degree for dimension d */
    virtual unsigned get_degree(unsigned d) = 0;

    /** @return value and uncertainty for the current parameter */
    virtual Qty get_value() = 0;
  };

  /**
   * Exception thrown by virtual methods in Transform and related classes.
   */
  class TransformException : public Exception {
  public:
    TransformException(const std::string& _what):Exception(_what) {}
  };

  /**
   * @class ITransform
   *
   * Interface of a transform.
   * Provides methods for inspecting and manipulating
   * the properties and parameters of the transform.
   */
  class ITransform {
  public:
    virtual ~ITransform() {}

    /** @return Transform name */
    virtual const std::string& get_name() const = 0;

    /** @return Transform type name */
    virtual const std::string& get_type_name() const = 0;

    /** @return the name of the basis */
    virtual const std::string& get_basis_name() const = 0;

    /** @return Number of dimensions */
    virtual unsigned get_dimensions() const = 0;

    /** @return Degree for given dimension */
    virtual unsigned get_var_degree(unsigned i) const = 0;

    /** @return Maximum degree */
    virtual unsigned get_max_degree() const = 0;

    /** @return Number of parameters */
    virtual unsigned get_nparams() const = 0;

    /** @return if this is an amplitude */
    virtual bool get_is_amplitude() const = 0;

    /** Set if this is an amplitude */
    virtual void set_is_amplitude(bool) = 0;

    /** @return Parameter i */
    virtual Qty get_param(unsigned i) const = 0;

    /** @return Parameter i */
    virtual void set_param(unsigned i, double val) = 0;

    /** @return An iterator for inspecting parameters */
    virtual std::unique_ptr<IParamIterator> get_param_iterator() const = 0;

    /** @return Evaluator object for faster evaluation */
    virtual std::unique_ptr<IEvaluator> get_evaluator(double scale = 1.0, bool external_coordinates = false) const = 0;

    /** @return Evaluator object for single axis */
    virtual std::unique_ptr<IEvaluator> get_marginalize_evaluator_1D(unsigned axis, double scale = 1.0, bool external_coordinates = false) const = 0;

    /** @return Evaluator object for 2 axes */
    virtual std::unique_ptr<IEvaluator> get_marginalize_evaluator_2D(unsigned a1, unsigned a2, double scale = 1.0, bool external_coordinates = false) const = 0;

    /** @return Evaluator of the slice from dim 0 to dim d2 */
    virtual std::unique_ptr<IEvaluator> get_slice_evaluator(unsigned d2, bool _do_normalize, double scale = 1.0, bool external_coordinates = false) const = 0;

    /** @return Evaluator of the derivative slice from dim 0 to dim d2, may be cast into ISliceDerivEvaluator  */
    virtual std::unique_ptr<IEvaluator> get_slice_derivative_evaluator(unsigned d2, bool _do_normalize, double scale = 1.0, bool external_coordinates = false) const = 0;
    
    /** Evaluate at given point */
    virtual double get_value(const double* x) const = 0;

    /** @return integral */
    virtual double get_integral() const = 0;

    /** @return transform for a single axis */
    virtual ITransform* get_axis_transform(unsigned d) const = 0;

    /** @return a clone */
    virtual ITransform* get_clone() const = 0;

    /** dump on stdout */
    virtual void do_dump(bool unc = false) const = 0;

    /** dump on stdout */
    virtual void do_normalize(double norm) = 0;

    /** @return The variables for this transform */
    virtual const PVariables& get_variables() const = 0;

    /** Set variables for this transform */
    virtual void set_variables(const PVariables&) = 0;

    /**
     * Evaluate the transform at <xv> (row major form) and store the result in res
     * @return true If everything went well
     */
    virtual bool do_eval(unsigned npts, const double* xv, double* res, double scale = 1.0, bool external_coordinates = false) const = 0;

    /** @return If transform includes uncertainties */
    virtual bool get_compute_uncertainties() const = 0;

    /** @return If transform includes covariance */
    virtual bool get_compute_covariance() const = 0;

    /**
     * Evaluate at multiple points, store values for all eigenvectors in result
     * @param points Input Matrix of dimension (Npoints, Dims)
     * @param res Output Matrix of dimension (Npoints, Nparams)
     */
    virtual bool do_full_eval(const Matrix<double>& points, Matrix<double>& res, bool external_coordinates = false) const = 0;

    /** Export to options */
    virtual bool do_write(OptionMap&) const = 0;

    /**
     * Compute covariance matrix
     * @param poisson If poisson uncertainties should be included
     * @return The covaraince matrix
     */
    virtual const MatrixSym<double>* do_create_covariance(bool poisson) = 0;

    /** @return Coefficient vector */
    virtual const Vector<double>* get_parameters() const = 0;

    /** @return Uncertainties vector */
    virtual const Vector<double>* get_uncertainties() const = 0;
    
    /** @return Covariance matrix */
    virtual const MatrixSym<double>* get_covariance() const = 0;

    /**
     * Copy data from another ITransform
     * @return True if the transform could be copied
     */
    virtual bool do_copy(const ITransform*) = 0;

    /** @return Sample for this Transform, may be nullptr */
    virtual const PSample& get_sample() const = 0;

    /** Set Sample for this Transform */
    virtual void set_sample(const PSample&) = 0;
  };

  template<class _Transf>
  using TransformSet = ReferenceSet<_Transf, ITransform>;

  template<class _Transf>
  using TransformSet2D = ReferenceSet2D<_Transf, ITransform>;

  template<class _Transf>
  inline TransformSet<_Transf>
  transform_set(const std::initializer_list<_Transf*>& trfs) { return TransformSet<_Transf>(trfs); }

  template<class _Transf>
  inline TransformSet<_Transf>
  transform_set(std::vector<_Transf*>&& trfs) { return TransformSet<_Transf>(std::move(trfs)); }

  template<class _Transf>
  inline TransformSet2D<_Transf> transform_set_2D(TransformSet<_Transf>&& trfs) {
    TransformSet2D<_Transf> res(1);
    res[0] = std::move(trfs);
    return res;
  }
}

#endif // sfi_ITransform_h
