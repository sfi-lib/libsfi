#ifndef sfi_ITransformation_h
#define sfi_ITransformation_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string>
#include "Options.h"
#include "Context.h"
#include "trans_tmp.h"

namespace sfi {
  class ITransform;
  class ISample;

  class ITransformation {
  public:
    virtual ~ITransformation() {}

    /** @return Name of the transformation */
    virtual const std::string& get_name() const = 0;

    /** @return Type name of the transformation */
    virtual const std::string& get_type_name() const = 0;

    /** @return Options */
    virtual OptionMap* get_options() = 0;

    /** Set print level */
    virtual void set_print_level(int lvl) = 0;

    /**
     * Transform sample
     * @param esel Event selection : 0 means all, 1 even, 2 odd numbered events
     */
    virtual bool do_transform(Context* ctx, const ISample* spl, int esel, ITransform* res) const = 0;

    /** Create a Transform suitable for the underlying Transformation */
    virtual ITransform* do_create_transform(Context* ctx, int degrees) const = 0;

    /** @return sub transformation, or 0 if the Transformation has none */
    virtual ITransformation* get_sub_transformation() = 0;
  };
}

#endif // sfi_ITransformation_h
