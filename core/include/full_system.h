#ifndef sfi_full_system_h
#define sfi_full_system_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "EigenSystem.h"

namespace sfi {
  /**
   * @class EigenFullTensor
   *
   * Tag class that specifies that all combination of basis functions up to a specified degree should be used to
   * build the eigen system.
   */
  struct EigenFullTensor {};
  template<> struct _t_is_eigentype<EigenFullTensor> : ttrue {};

  template<unsigned _Dims, unsigned _Deg>
  struct _eigen_npars<_Dims, _Deg, EigenFullTensor> {
    static inline constexpr size_t value() { return powc(_Deg + 1, _Dims); }
  };

  template<>
  struct t_eigenvector_type_name<EigenFullTensor> {
    static const std::string& apply() {
      static const std::string _s = "full";
      return _s;
    }
  };

  // TODO: Better impl!
  template<unsigned _Dims>
  struct _eigen_npars<_Dims, 0U, EigenFullTensor> {
    static inline size_t value(unsigned _deg) { return powi(_deg + 1, _Dims); }
  };

  template<unsigned _Dims, unsigned _Deg, class _IdxType>
  struct EigenIndices<_Dims, _Deg, EigenFullTensor, _IdxType> : public EigenIndicesBase<_Dims, _Deg, EigenFullTensor, _IdxType> {
    using This = EigenIndices<_Dims, _Deg, EigenFullTensor, _IdxType>;
    using Base = EigenIndicesBase<_Dims, _Deg, EigenFullTensor, _IdxType>;
    using Data = EigenVectorsData<_Dims, _Deg, EigenFullTensor>;

    EigenIndices(const Data& da):Base(da) { }

    EigenIndices(EigenIndices&& o):Base(std::forward<EigenIndices>(o)) {}

    EigenIndices(const EigenIndices& o):Base(o) {}

    inline bool next() {
      unsigned i = 0;
      while((i < This::dims()) && (++this->m_indices[i] > This::degree())) {
        this->m_indices[i] = 0;
        ++i;
      }
      return (i != This::dims());
    }

    inline uint64_t index() const {
      const constexpr unsigned _deg = This::degree() + 1;
      auto* p0 = Base::indices_ptr();
      auto* p = p0 + This::dims() - 1;
      uint64_t res = *p;
      while(p != p0) {
        --p;
        res *= _deg;
        res += *p;
      }
      return res;
    }

    inline This& operator=(const This& o) { Base::set(o); return *this; }
  };

  template<unsigned _Dims, class _IdxType>
  struct EigenIndices<_Dims, 0U, EigenFullTensor, _IdxType> : public EigenIndicesBase<_Dims, 0U, EigenFullTensor, _IdxType> {
    using This = EigenIndices<_Dims, 0U, EigenFullTensor, _IdxType>;
    using Base = EigenIndicesBase<_Dims, 0U, EigenFullTensor, _IdxType>;
    using Data = EigenVectorsData<_Dims, 0U, EigenFullTensor>;

    EigenIndices(const Data& da):Base(da) { }

    EigenIndices(EigenIndices&& o):Base(std::forward<EigenIndices>(o)) {}

    EigenIndices(const EigenIndices& o):Base(o) {}

    inline bool next() {
      const unsigned _deg = Base::degree();
      _IdxType* _idx = Base::indices_ptr();
      _IdxType const* _idx_end = _idx +  Base::dims();
      while((_idx != _idx_end) && (++(*_idx) > _deg)) {
        *_idx = 0;
        ++_idx;
      }
      return (_idx != _idx_end);
    }

    inline uint64_t index() const {
      const unsigned _deg = Base::degree() + 1;
      auto* p0 = Base::indices_ptr();
      auto* p = p0 + Base::dims() - 1;
      uint64_t res = *p;
      while(p != p0) {
        --p;
        res *= _deg;
        res += *p;
      }
      return res;
    }

    inline This& operator=(const This& o) { Base::set(o); return *this; }
  };

  /** Static, full tensor */
  template<unsigned _Dims, unsigned _Deg, class _Trans>
  struct EigenSystem<tuint<_Dims>, tuint<_Deg>, EigenFullTensor, _Trans> :
  StaticEigenSystem<tuint<_Dims>, tuint<_Deg>, EigenFullTensor, _Trans, tuint<_Dims>> {
    using This = EigenSystem<tuint<_Dims>, tuint<_Deg>, EigenFullTensor, _Trans>;
    using Base = StaticEigenSystem<tuint<_Dims>, tuint<_Deg>, EigenFullTensor, _Trans, tuint<_Dims>>;

    EigenSystem(const OptionMap&):Base(){}
    EigenSystem(const t_EigenVectors<Base>&):Base() {}
    EigenSystem():Base() {}

    template<Basis::CompType _ttype, class _Func>
    static inline _Func compute(const t_Var<Base>* x, t_EvalData<Base>& ed, const _Func& _f) {
      _Func f(_f);
      t_CoefficienctType<_Trans> phi(1.0);
      unsigned i(0), d(0), id(0);
      Base::template precompute<_ttype>(x, ed);
      This::reset_eval(ed);
      for(d = 1; d<_Dims; ++d) phi *= ed[d][0];
      do {
        for(id = 0; id<=_Deg; ++id) f(ed.ptr[0][id]*phi);
        for(i = 1; (i != _Dims) && (++ed.ptr[i] == ed.ptr_end[i]); ++i) ed.ptr[i] = ed[i];
        phi = 1.0;
        for(d=1; d != _Dims; ++d) phi *= (*ed.ptr[d]);
      }
      while(i != _Dims);
      return f;
    }
  };

  /** Dynamic, full tensor */
  template<unsigned _Dims, class _Trans>
  struct EigenSystem<tuint<_Dims>, tuint<0U>, EigenFullTensor, _Trans> :
  DynamicEigenSystem<tuint<_Dims>, EigenFullTensor, _Trans, tuint<_Dims>> {
    using This = EigenSystem<tuint<_Dims>, tuint<0U>, EigenFullTensor, _Trans>;
    using Base = DynamicEigenSystem<tuint<_Dims>, EigenFullTensor, _Trans, tuint<_Dims>>;

    EigenSystem(const OptionMap& om):Base(t_EigenVectors<Base>(om.sub("eigenv"))){}
    EigenSystem(const t_EigenVectors<Base>& eig):Base(eig) {}

    template<Basis::CompType _ttype, class _Func>
    inline _Func compute(const t_Var<Base>* x, t_EvalData<Base>& ed, const _Func& _f) const {
      _Func f(_f);
      const unsigned g = Base::degree();
      unsigned i(0), d(0), id(0);
      Base::template precompute<_ttype>(x, ed);
      This::reset_eval(ed);
      t_CoefficienctType<_Trans> phi(1.0);
      for(d = 1; d<_Dims; ++d) phi *= ed[d][0];
      do {
        for(id=0; id<=g; ++id) f(ed.ptr[0][id]*phi);
        for(i = 1; (i != _Dims) && (++ed.ptr[i] == ed.ptr_end[i]); ++i) ed.ptr[i] = ed[i];
        phi = 1.0;
        for(d=1; d != _Dims; ++d) phi *= (*ed.ptr[d]);
      }
      while(i != _Dims);
      return f;
    }
  };
}

#endif // sfi_full_system_h
