#ifndef sfi_TransformBase_h
#define sfi_TransformBase_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ITransform.h"
#include "EigenSystem.h"
#include "Log.h"

#include <complex>

namespace sfi {

  template<class _Transf, class _EigenSys> struct DefaultTransformImpl;

  template<class _Transf, class _EigenSys>
  struct TransformImpl : DefaultTransformImpl<_Transf, _EigenSys> { };

  class TransformVariableBase : public ITransform {
  public:
    virtual ~TransformVariableBase();

    using This = TransformVariableBase;

    // ---- implementation of ITransform ----

    virtual const std::string& get_name() const { return name(); }

    virtual bool get_is_amplitude() const { return this->is_amplitude(); }

    virtual void set_is_amplitude(bool isa) { this->is_amplitude(isa); }

    virtual const PVariables& get_variables() const { return m_variables; }

    virtual void set_variables(const PVariables& v) { m_variables = v; }

    virtual bool get_compute_uncertainties() const { return this->compute_uncertainty(); }

    virtual bool get_compute_covariance() const { return this->compute_covariance(); }

    virtual const PSample& get_sample() const { return m_sample; }

    virtual void set_sample(const PSample& spl) { m_sample = spl; }

    // ---- public methods ----

    inline const std::string& name() const { return m_name; }
    inline This& name(const std::string&n ) { m_name = n; return *this; }

    inline bool compute_covariance() const { return m_compute_covariance; }

    inline bool is_amplitude() const { return m_is_amplitude; }
    inline This& is_amplitude(bool b) { m_is_amplitude = b; return *this; }

    inline bool compute_uncertainty() const { return this->m_compute_uncertainty; }

    inline const PVariables& variables() const { return this->m_variables; }

    inline This& variables(const PVariables& v) {
      this->m_variables = v;
      return *this;
    }

    This& operator=(const This& src) = delete;
    This& operator=(This&& src) = delete;

    void write(OptionMap&) const;
  protected:
    TransformVariableBase(const std::string& name);

    TransformVariableBase(const This& o);

    TransformVariableBase(This&& o);

    TransformVariableBase(const OptionMap& om);

    void copy_from(const TransformVariableBase& o);

    void move_from(TransformVariableBase&& o);

    std::string m_name;
    PVariables m_variables;
    PSample m_sample;
    bool m_compute_covariance;
    bool m_is_amplitude;
    bool m_compute_uncertainty;
  };

  template<class _Param>
  class TransformParamBase : public TransformVariableBase {
  public:
    virtual ~TransformParamBase() {}

    using This = TransformParamBase<_Param>;
    using Base = TransformVariableBase;

    using ConstValType = tcond<std::is_arithmetic<_Param>::value, _Param, const _Param&>;
    using UncType = double;

    // ---- implementation of ITransform ----

    virtual const Vector<double>* get_parameters() const { return get_parameters_impl<_Param>(); }

    virtual const Vector<double>* get_uncertainties() const { return get_uncertainties_impl<_Param>(); }
    
    virtual const MatrixSym<double>* get_covariance() const { return &covariance(); }

    // ---- public methods ----

    inline size_t nparams() const { return m_par.size(); }

    inline _Param& par(size_t i) {
      assert(i < m_par.size() && "Invalid parameter id");
      return m_par[i];
    }

    inline ConstValType par(size_t i) const {
      assert(i < m_par.size() && "Invalid parameter id");
      return m_par[i];
    }

    inline _Param& operator[](size_t i) { return this->par(i); }

    inline ConstValType operator[](size_t i) const { return this->par(i); }

    inline UncType& par_unc(size_t i) {
      assert(i < m_par_unc.size() && "Invalid parameter id");
      return m_par_unc[i];
    }
    inline UncType par_unc(size_t i) const {
      assert(i < m_par_unc.size() && "Invalid parameter id");
      return m_par_unc[i];
    }

    inline UncType& par_cov(size_t i, size_t j) {
      assert(this->compute_covariance() && "Invalid use of par_cov");
      return m_covariance(i, j);
    }
    inline UncType par_cov(size_t i, size_t j) const {
      assert(this->compute_covariance() && "Invalid use of par_cov");
      return m_covariance(i, j);
    }

    inline double par_sign(size_t i) const {
      assert(i < m_par.size() && "Invalid parameter id");
      assert(i < m_par_unc.size() && "Invalid parameter id");
      return std::abs(m_par[i])/m_par_unc[i];
    }

    inline Qty param_qty(size_t i) const {
      assert(i < this->nparams() && "Invalid parameter id");
      return this->m_compute_uncertainty ? Qty(std::real(par(i)), std::real(par_unc(i))) : Qty(std::real(par(i)), 0.0);
    }

    inline Vector<_Param>& parameters() { return this->m_par; }

    inline const Vector<_Param>& parameters() const { return this->m_par; }

    inline Vector<UncType>& uncertainties() { return this->m_par_unc; }

    inline const Vector<UncType>& uncertainties() const { return this->m_par_unc; }

    inline MatrixSym<UncType>& covariance() { return this->m_covariance; }

    inline const MatrixSym<UncType>& covariance() const { return this->m_covariance; }

    /** Multiply all parameter by s */
    void scale(double s, bool scale_unc = true);

    void clear();

    This& operator=(const This& src) = delete;
    This& operator=(This&& src) = delete;
    
    inline void write(OptionMap& op) const {
      Base::write(op);
      op.set("parameters", this->m_par);
    }
  protected:
    TransformParamBase(const std::string& name);

    TransformParamBase(const This& o);

    TransformParamBase(This&& o);

    TransformParamBase(const OptionMap& om);

    /** Print all params to stdout */
    void dump_impl(bool /*unc*/, unsigned ndims) const;

    /**
     * Create param vector, and uncertainties/covariance if enabled
     * @return True if the parameters were created and set to zero, false if the parameters are already defined (they may be non-zero)
     */
    bool do_init_params(unsigned nparams);

    void do_compute_covariance(bool b, unsigned nparams);

    void do_compute_uncertainty(bool b, unsigned nparams);

    void copy_from(const This& o);

    void move_from(This&& o);

    template<class _T>
    inline const Vector<double>* get_parameters_impl(teif<teq<_T,double>::value()>* = 0) const {
      return &this->m_par;
    }
    template<class _T>
    inline const Vector<double>* get_parameters_impl(teif<!teq<_T,double>::value()>* = 0) const {
      return nullptr;
    }

    template<class _T>
    inline const Vector<double>* get_uncertainties_impl(teif<teq<_T,double>::value()>* = 0) const {
      return &this->m_par_unc;
    }
    template<class _T>
    inline const Vector<double>* get_uncertainties_impl(teif<!teq<_T,double>::value()>* = 0) const {
      return nullptr;
    }
    
    Vector<_Param> m_par;
    Vector<UncType> m_par_unc;
    MatrixSym<UncType> m_covariance;
  };

  template<class _EigenSys>
  class TransformEigenBase : public TransformParamBase<t_CoefficienctType<_EigenSys>> {
  public:
    using Coeff = t_CoefficienctType<_EigenSys>;
    using Base = TransformParamBase<Coeff>;
    using Eigenv = t_EigenVectors<_EigenSys>;
    using EigenVector = t_EigenVector<Eigenv>;

    virtual ~TransformEigenBase() {}

    virtual bool do_write(OptionMap& o) const { write(o); return true; }

    static constexpr inline unsigned dims() { return _EigenSys::dims(); }

    static constexpr inline unsigned indices() { return _EigenSys::indices(); }

    inline const Eigenv& eigenvectors() const { return this->m_eigen_system.eigenvectors(); }

    inline unsigned npar() const { return this->m_eigen_system.eigenvectors().params(); }

    inline const _EigenSys& eigen_system() const { return this->m_eigen_system; }

    inline void set(unsigned k, double c, double c_unc, const typename Eigenv::IndicesType& /*idx*/) {
      assert(k < this->m_par.size() && "Invalid parameter id");
      this->m_par[k] = c; this->m_par_unc[k] = c_unc;
    }

    /** Print all params to stdout */
    void dump(bool unc = false) const { this->dump_impl(unc, this->dims()); }

    /**
     * Create param vector, by asking Eigenvectors, if Eigenvectors is not materialized, start by creating them.
     * @return True if the parameters were created and set to zero, false if the parameters are already defined (they may be non-zero)
     */
    bool init_params() { return this->do_init_params(npar()); }

    void write(OptionMap&) const;
  protected:
    TransformEigenBase():Base(""), m_eigen_system(Eigenv()) {
      assert(!_EigenSys::is_dynamic() && "TransformEigenBase must be called with eigenvectors");
    }

    TransformEigenBase(const std::string& name):Base(name), m_eigen_system(Eigenv()) {
      assert(!_EigenSys::is_dynamic() && "TransformEigenBase must be called with eigenvectors");
    }

    TransformEigenBase(const Eigenv& ev, const std::string& name):Base(name), m_eigen_system(ev) { }

    TransformEigenBase(const TransformEigenBase& o):Base(o), m_eigen_system(o.m_eigen_system) { }

    TransformEigenBase(TransformEigenBase&& o):Base(std::forward<TransformEigenBase>(o)),
        m_eigen_system(std::move(o.m_eigen_system)) {
    }

    TransformEigenBase(const OptionMap& om):Base(om), m_eigen_system(om.sub("eigensystem")) {
    }

    inline void copy_from(const TransformEigenBase& o) {
      Base::copy_from(o);
      m_eigen_system = o.m_eigen_system;
    }

    inline void move_from(TransformEigenBase&& o) {
      Base::move_from(std::forward<TransformEigenBase>(o));
      m_eigen_system = std::move(o.m_eigen_system);
    }

    _EigenSys m_eigen_system;
  };

  template<class _Param>
  TransformParamBase<_Param>::TransformParamBase(const std::string& name):Base(name) {}

  template<class _Param>
  TransformParamBase<_Param>::TransformParamBase(const This& o):Base(o),
  m_par(o.m_par), m_par_unc(o.m_par_unc), m_covariance(o.m_covariance) {
  }

  template<class _Param>
  TransformParamBase<_Param>::TransformParamBase(This&& o):Base(std::forward<This>(o)),
  m_par(std::move(o.m_par)), m_par_unc(std::move(o.m_par_unc)), m_covariance(std::move(o.m_covariance)) {
  }

  template<class _Param>
  TransformParamBase<_Param>::TransformParamBase(const OptionMap& om):Base(om) {
    om.get_if("parameters", m_par);
  }

  template<class _Param>
  void TransformParamBase<_Param>::scale(double s, bool scale_unc) {
    m_par *= s;
    if(this->compute_uncertainty() && scale_unc) {
      const double sqrts(sqrt(s));
      m_par_unc *= sqrts;
    }
    if(this->compute_covariance() && scale_unc) m_covariance *= s;
  }

  template<class _Param>
  void TransformParamBase<_Param>::clear() {
    m_par.clear();
    m_par_unc.clear();
    m_covariance.clear();
  }

  template<class _Param>
  void TransformParamBase<_Param>::dump_impl(bool unc, unsigned ndims) const {
    std::cout<<name()<<" ("<<(void*)this<<") eparams: "<<m_par.size()<<" edims: "<<ndims<<" covariance: "<<m_compute_covariance<<" ampl: "<<m_is_amplitude<<std::endl;
    std::ostringstream sout;
    if(unc && (this->compute_covariance() || this->compute_uncertainty())) {
      if(this->compute_covariance()) {
        for(unsigned p=0; p<m_par.size(); ++p) sout<<par(p)<<" ("<<sqrt(par_cov(p, p))<<") ";
      }
      else if(this->compute_uncertainty()) {
        for(unsigned p=0; p<m_par.size(); ++p) sout<<par(p)<<" ("<<par_unc(p)<<") ";
      }
    }
    else for(unsigned p=0; p<m_par.size(); ++p) sout<<par(p)<<" ";
    sout<<std::endl;
    std::cout<<name()<<" parameters: "<<sout.str()<<std::endl;
  }

  template<class _Param>
  bool TransformParamBase<_Param>::do_init_params(unsigned nparams) {
    if(m_par.rows() != nparams) {
      m_par.rows(nparams);
      if(this->compute_covariance()) m_covariance.rows(nparams);
      if(this->compute_uncertainty()) m_par_unc.rows(nparams);
      return true;
    }
    return false;
  }

  template<class _Param>
  void TransformParamBase<_Param>::do_compute_covariance(bool b, unsigned nparams) {
    this->m_compute_covariance = b;
    if(m_par.size() && b) m_covariance.rows(nparams);
  }

  template<class _Param>
  void TransformParamBase<_Param>::do_compute_uncertainty(bool b, unsigned nparams) {
    this->m_compute_uncertainty = b;
    if(m_par.size() && b) m_par_unc.rows(nparams);
  }

  template<class _Param>
  void TransformParamBase<_Param>::copy_from(const This& o) {
    Base::copy_from(o);
    m_par = o.m_par;
    m_par_unc = o.m_par_unc;
    m_covariance = o.m_covariance;
  }

  template<class _Param>
  void TransformParamBase<_Param>::move_from(This&& o) {
    Base::move_from(std::forward<This>(o));
    m_par = std::move(o.m_par);
    m_par_unc = std::move(o.m_par_unc);
    m_covariance = std::move(o.m_covariance);
  }


  template<class _EigenSys>
  void TransformEigenBase<_EigenSys>::write(OptionMap& op) const {
    Base::write(op);
    this->m_eigen_system.write(op.sub("eigensystem", true));
  }
}

#endif // sfi_TransformBase_h
