#ifndef sfi_EigenSystem_h
#define sfi_EigenSystem_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "EigenVectors.h"
#include <complex>
#include "Basis.h"

namespace sfi {
  template<class ... _T> struct EigenSystemEigenBase { };
  template<class ... _T> struct EigenSystemBase { };
  template<class ... _T> struct StaticEigenSystem;
  template<class ... _T> struct DynamicEigenSystem;
  template<class ... _T> struct CompositeEigenSystemEigenBase { };
  template<class ... _T> struct EigenSystem { };

  template<class ... _T> struct _t_eigenvectors<EigenSystem<_T...>> : tvalued<typename EigenSystem<_T...>::Eigenv> {};
  template<class ... _T> struct _t_eigenvectors<EigenSystemEigenBase<_T...>> : tvalued<typename EigenSystemEigenBase<_T...>::Eigenv> {};

  template<class ... _T> struct _t_major_coefficienct_type : tvalued<void> {};

  template<class T> struct _t_major_coefficienct_type<T, T> : tvalued<T> {};
  template<class T> struct _t_major_coefficienct_type<T, std::complex<T>> : tvalued<std::complex<T>> {};
  template<class T> struct _t_major_coefficienct_type<std::complex<T>, T> : tvalued<std::complex<T>> {};

  template<class ... _T> using t_MajorCoefficientType = tvalue<_t_major_coefficienct_type<_T...>>;

  template<class ... _T> struct _t_coefficient_type<EigenSystem<_T...>> : tvalued<typename EigenSystem<_T...>::Coeff> {};

  template<class ... _T, unsigned _Dim> struct _t_basis<EigenSystem<_T...>, _Dim> : tvalued<typename EigenSystem<_T...>::ESData::Impl> {};

  template<class ... _T1, class ... _T2, unsigned _Dim> struct _t_basis<EigenSystem<EigenSystem<_T1...>, EigenSystem<_T2...>>, _Dim> :
  tvalued<tcond<(_Dim < EigenSystem<_T1...>::dims()), t_Basis<EigenSystem<_T1...>, _Dim>, t_Basis<EigenSystem<_T2...>, (_Dim - EigenSystem<_T1...>::dims())>>> {};

  template<class _T> using t_Var = typename _T::Var;

  template<bool _is_comp, bool _is_on, unsigned _Dims, unsigned _Indices, unsigned _Deg, class _Transf>
  struct EigenSystemData {
    static constexpr inline bool is_dynamic() { return (_Deg == 0); }
    static constexpr inline bool is_composite() { return _is_comp; }
    static constexpr inline bool is_orthonormal() { return _is_on; }
    static constexpr inline unsigned dims() { return _Dims; }
    static constexpr inline unsigned indices() { return _Indices; }
    static constexpr inline unsigned degree() { return _Deg; }
    using Impl = _Transf;
  };

  /**
   * @class EigenSystemBase
   *
   * Base class of EigenSystem, the templated class responsible for linking
   * EigenVectors and basis type to an eigen system.
   *
   * dims : Number of free variables in the Transform
   * indices : Internal number of degrees of freedom, may differ from the number of dimensions
   * is_composite : If this ES is made up of two or more sub systems
   * is_dynamic : If this ES has dynamic number of degrees
   * is_orthonormal : If this ES is orthonormal
   */
  template<class _EData>
  struct EigenSystemBase<_EData> {
    static_assert(_EData::is_orthonormal(), "Transform must be orthonormal");
    using ESData = _EData;
    using Coeff = t_CoefficienctType<typename _EData::Impl>;
    using Var = double;

    /** @return Dimensionality */
    static constexpr inline unsigned dims() { return _EData::dims(); }

    /** @return Number of indices */
    static constexpr inline unsigned indices() { return _EData::indices(); }

    /** @return If this system is made up of sub systems */
    static constexpr inline bool is_composite() { return _EData::is_composite(); }

    /** @return If this system has degrees */
    static constexpr inline bool is_dynamic() { return _EData::is_dynamic(); }

    /** @return If this system has dynamic degrees */
    static constexpr inline bool is_dynamic_degree() { return _EData::is_dynamic(); }

    /** @return If the system is orthonormal */
    static constexpr inline bool is_orthonormal() { return _EData::is_orthonormal(); }

    static const std::string& basis_name() { return _EData::Impl::name(); }

    static constexpr inline bool zero_integral(unsigned /*d*/, unsigned g) { return _EData::Impl::zero_integral(g); }

    static inline double integral(unsigned /*d*/, unsigned g) { return _EData::Impl::integral(g); }

    static inline double inverse_single_norm_square(unsigned /*d*/, unsigned g) {
      return _EData::Impl::inverse_single_norm_square(g);
    }

    static inline double norm(unsigned /*d*/, unsigned g) { return _EData::Impl::norm(g); }

    template<class _Indices>
    static inline bool integral(const _Indices& idx, double& I) {
      for(unsigned d=0; d<_EData::dims(); ++d) {
        if(_EData::Impl::zero_integral(idx[d])) return false;
        I *= _EData::Impl::integral(idx[d]);
      }
      return true;
    }

    template<class _Indices, class _Coeff>
    static inline bool integral(const _Indices& idx, unsigned a, _Coeff& I) {
      for(unsigned d=0; d<_EData::dims(); ++d) {
        if(d != a) {
          if(_EData::Impl::zero_integral(idx[d])) return false;
          I *= _EData::Impl::integral(idx[d]);
        }
      }
      return true;
    }

    template<class _Indices, class _Coeff>
    static inline bool integral(const _Indices& idx, unsigned a1, unsigned a2, _Coeff& I) {
      for(unsigned d=0; d<_EData::dims(); ++d) {
        if((d != a1) && (d != a2)) {
          if(_EData::Impl::zero_integral(idx[d])) return false;
          I *= _EData::Impl::integral(idx[d]);
        }
      }
      return true;
    }

    template<class _EvalData, class _EigenIter, class _Coeff>
    static inline _Coeff basis_product(const _EvalData& ed, unsigned ed_base_idx, const _EigenIter& eit, _Coeff prod) {
      for(unsigned d=0; d<_EData::dims(); ++d) prod *= ed[d+ed_base_idx][eit[d]];
      return prod;
    }
  };

  /** Static dims and degree */
  template<class _Eigens, class _EData>
  struct EigenSystemEigenBase<_Eigens, _EData, teif<!_EData::is_dynamic()>> : EigenSystemBase<_EData> {
    using Eigenv = _Eigens;

    EigenSystemEigenBase() {}

    EigenSystemEigenBase(const Eigenv&) {}

    inline unsigned max_degree() const { return m_eigenvectors.degree(); }

    /** @return degree */
    static constexpr inline unsigned degree() { return _EData::degree(); }

    static inline unsigned var_degree(unsigned) { return _EData::degree(); }
    static inline unsigned var_degree(const Eigenv&, unsigned) { return _EData::degree(); }

    inline Eigenv& eigenvectors() { return m_eigenvectors; }
    inline const Eigenv& eigenvectors() const { return m_eigenvectors; }

    inline static const Eigenv& create_eigen_vectors(const Eigenv& ev) { return ev; }
  protected:
    Eigenv m_eigenvectors;
  };

  /** Dynamic dims or degree */
  template<class _Eigens, class _EData>
  struct EigenSystemEigenBase<_Eigens, _EData, teif<_EData::is_dynamic()>> : EigenSystemBase<_EData> {
    using Eigenv = _Eigens;

    EigenSystemEigenBase(const _Eigens& eigens):m_eigenvectors(eigens) {}

    inline unsigned max_degree() const { return m_eigenvectors.degree(); }

    inline Eigenv& eigenvectors() { return m_eigenvectors; }
    inline const Eigenv& eigenvectors() const { return m_eigenvectors; }

    inline static const Eigenv& create_eigen_vectors(const Eigenv& ev) { return ev; }
  protected:
    Eigenv m_eigenvectors;
  };

  /**
   * @class StaticEigenSystem
   * @param _Dims Number of dimensions
   * @param _Deg Number of degrees
   * @param _EType Eigen vectors type
   * @param _Trans Basis type
   * @param _Indices Number of indices
   *
   * Base class to static eigen systems, i.e. ES with statically specified number of degrees.
   */
  template<unsigned _Dims, unsigned _Deg, class _EType, class _Trans, unsigned _Indices>
  struct StaticEigenSystem<tuint<_Dims>, tuint<_Deg>, _EType, _Trans, tuint<_Indices>> :
  EigenSystemEigenBase<EigenVectors<_Dims, _Deg, _EType>, EigenSystemData<false, _Trans::is_orthonormal, _Dims, _Indices, _Deg, _Trans>, void> {
    using This = StaticEigenSystem<tuint<_Dims>, tuint<_Deg>, _EType, _Trans, tuint<_Indices>>;
    using Base = EigenSystemEigenBase<EigenVectors<_Dims, _Deg, _EType>, EigenSystemData<false, _Trans::is_orthonormal, _Dims, _Indices, _Deg, _Trans>, void>;
    using Eigenv = t_EigenVectors<Base>;
    using Var = t_Var<Base>;
    using Coeff = t_CoefficienctType<_Trans>;
    static_assert((Base::dims()>0) && (Base::degree()>0), "Only static allowed");

    struct EvalData {
      EvalData() { }

      EvalData(EvalData&& o):ptr(o.ptr), ptr_end(o.ptr_end), data(o.data) { }

      EvalData(const EvalData&) {}

      EvalData& operator=(const EvalData&) = delete;

      EvalData& operator=(EvalData&&) = delete;

      inline Coeff* operator[](size_t i) { return this->data[i].data(); }
      inline const Coeff* operator[](size_t i) const { return this->data[i].data(); }

      std::array<Coeff*, This::indices()> ptr;
      std::array<Coeff*, This::indices()> ptr_end;
      std::array<std::array<Coeff, _Trans::buffer_size(This::degree())>, This::indices()> data;
    };

    static inline void init_eval(EvalData& ed) {
      for(unsigned d = 0; d<_Dims; ++d) {
        ed.ptr[d] = ed.data[d].data();
        ed.ptr_end[d] = ed.ptr[d] + This::degree() + 1;
      }
    }

    static inline void copy_eval(const EvalData& src, EvalData& dst, int par = -1) {
      if(par == -1) dst.data = src.data;
      else dst.data[par] = src.data[par];
    }
    
    static inline void reset_eval(EvalData& ed) {
      for(unsigned d = 0; d<_Dims; ++d) ed.ptr[d] = ed.data[d].data();
    }

    static inline constexpr size_t total_buffer_size(const Eigenv&) {
      return This::dims()*_Trans::buffer_size(This::degree());
    }

    static inline constexpr size_t buffer_size(size_t) {
      return _Trans::buffer_size(This::degree());
    }

    template<Basis::CompType _ttype, class _Var = Var>
    static inline void precompute(const _Var* x, EvalData& ed) { precompute<_ttype, _Var, Coeff>(x, ed[0]); }

    template<Basis::CompType _ttype, class _Var = Var, class _Coeff = Coeff>
    static inline void precompute(const Eigenv&, const _Var* x, _Coeff* res) {
      precompute<_ttype, _Var, _Coeff>(x, res);
    }

    template<Basis::CompType _ttype, class _Var = Var, class _Coeff = Coeff>
    static inline void precompute(const _Var* x, _Coeff* res) {
      for(unsigned d = 0; d<This::dims(); ++d) {
        _Trans::template compute<_Var, _Coeff, _ttype>(x[d], This::degree(), res);
        res += buffer_size(0);
      }
    }

    // TODO: Should be removed
    template<Basis::CompType _ttype, class _Var = Var, class _Coeff = Coeff>
    static inline void precompute(const Eigenv&, unsigned /*d*/, const _Var& x, _Coeff* res) {
      _Trans::template compute<_Var, _Coeff, _ttype>(x, This::degree(), res);
    }
    
    inline void write(OptionMap& op) const {
      write_es(op);
      this->m_eigenvectors.write(op.sub("eigenv", true));
    }
    
    static inline void write_es(OptionMap& op) {
      op.set("basis", Base::basis_name());
      op.set("dims", Base::dims());
      op.set("is_dynamic", false);
    }
  };

  /**
   * @class DynamicEigenSystem
   * @param _Dims Number of dimensions
   * @param _EType Type of eigen vectors
   * @param _Trans Basis type
   * @param _Indices Number of internal indices
   *
   * Base class of dynamic ES, i.e. ES with dynamically specified number of degrees.
   */
  template<unsigned _Dims, class _EType, class _Trans, unsigned _Indices>
  struct DynamicEigenSystem<tuint<_Dims>, _EType, _Trans, tuint<_Indices>> :
  EigenSystemEigenBase<EigenVectors<_Dims, 0U, _EType>, EigenSystemData<false, _Trans::is_orthonormal, _Dims, _Indices, 0U, _Trans>, void> {
    using This = DynamicEigenSystem<tuint<_Dims>, _EType, _Trans, tuint<_Indices>>;
    using Base = EigenSystemEigenBase<EigenVectors<_Dims, 0U, _EType>, EigenSystemData<false, _Trans::is_orthonormal, _Dims, _Indices, 0U, _Trans>, void>;
    using Eigenv = t_EigenVectors<Base>;
    using Var = t_Var<Base>;
    using Coeff = t_CoefficienctType<_Trans>;

    DynamicEigenSystem(const Eigenv& eig):Base(eig) {}

    /** @return degree */
    inline unsigned degree() const { return Base::eigenvectors().degree(); }

    inline unsigned var_degree(unsigned) const { return degree(); }
    static inline unsigned var_degree(const Eigenv& eig, unsigned) { return eig.degree(); }

    struct EvalData {
      EvalData():data(0) { }

      EvalData(EvalData&& o):ptr(o.ptr), ptr_end(o.ptr_end), data(o.data), ptr0(o.ptr0) {
        o.data = 0;
      }

      EvalData(const EvalData&):data(0) {}

      EvalData& operator=(const EvalData&) = delete;

      EvalData& operator=(EvalData&&) = delete;

      ~EvalData() {
        if(this->data) {
          delete[] this->data;
          this->data = 0;
        }
      }

      inline Coeff* operator[](size_t i) { return this->ptr0[i]; }
      inline const Coeff* operator[](size_t i) const { return this->ptr0[i]; }

      std::array<Coeff*, This::dims()> ptr;
      std::array<Coeff*, This::dims()> ptr_end;
      Coeff* data;
      // pointers to first element per dim
      std::array<Coeff*, This::dims()> ptr0;
    };
    
    inline void init_eval(EvalData& ed) const {
      if(!ed.data) {
        const unsigned bsize = _Trans::buffer_size(degree());
        ed.data = new Coeff[This::dims()*bsize];
        for(unsigned d = 0; d<This::dims(); ++d) {
          ed.ptr[d] = ed.ptr0[d] = ed.data + d*bsize;
          ed.ptr_end[d] = ed.ptr0[d] + degree() + 1;
        }
      }
    }

    inline void copy_eval(const EvalData& src, EvalData& dst, int par = -1) const {
      if(par == -1) {
        std::memcpy(dst.data, src.data,sizeof(Coeff)*(This::dims()*_Trans::buffer_size(degree())));
      } else {
        std::memcpy(dst.ptr[par], src.ptr[par],sizeof(Coeff)*_Trans::buffer_size(degree()));
      }
    }
    
    inline void reset_eval(EvalData& ed) const {
      for(unsigned d = 0; d<This::dims(); ++d) {
        ed.ptr[d] = ed.ptr0[d];
        // TODO: Needed?
        // ed.ptr_end[d] = ed.ptr0[d] + degree() + 1;
      }
    }

    template<Basis::CompType _ttype>
    inline void precompute(const Var* x, EvalData& ed) const { precompute<_ttype, Var, Coeff>(Base::eigenvectors(), x, ed[0]); }

    template<Basis::CompType _ttype, class _Var = Var, class _Coeff = Coeff>
    static inline void precompute(const Eigenv& eig, const _Var* x, _Coeff* res) {
      for(unsigned d = 0; d<This::dims(); ++d) {
        _Trans::template compute<_Var, _Coeff, _ttype>(x[d], eig.degree(), res);
        res += _Trans::buffer_size(eig.degree());
      }
    }

    // TODO: Should be removed
    template<Basis::CompType _ttype, class _Var = Var, class _Coeff = Coeff>
    static inline void precompute(const Eigenv& eig, unsigned /*d*/, const _Var& x, _Coeff* res) {
      _Trans::template compute<_Var, _Coeff, _ttype>(x, eig.degree(), res);
    }

    static inline constexpr size_t total_buffer_size(const Eigenv& eig) {
      return This::dims()*_Trans::buffer_size(eig.degree());
    }

    static inline size_t buffer_size(const Eigenv& eig, size_t) {
      return _Trans::buffer_size(eig.degree());
    }

    inline void write(OptionMap& op) const {
      write_es(op);
      this->m_eigenvectors.write(op.sub("eigenv", true));
    }
    
    static inline void write_es(OptionMap& op) {
      op.set("basis", Base::basis_name());
      op.set("dims", Base::dims());
      op.set("is_dynamic", true);
    }
  };

  template<class ..._T> struct EigenSystemImpl {};

  template<unsigned _Dims, class _EType, class _Trans, unsigned _Indices>
  struct EigenSystemImpl<tuint<_Dims>, tuint<0>, _EType, _Trans, tuint<_Indices>> :
  DynamicEigenSystem<tuint<_Dims>, _EType, _Trans, tuint<_Indices>> {
    using Base = DynamicEigenSystem<tuint<_Dims>, _EType, _Trans, tuint<_Indices>>;
    EigenSystemImpl(const t_EigenVectors<Base>& eig):Base(eig) {}
  };

  template<unsigned _Dims, unsigned _Deg, class _EType, class _Trans, unsigned _Indices>
  struct EigenSystemImpl<tuint<_Dims>, tuint<_Deg>, _EType, _Trans, tuint<_Indices>> :
  StaticEigenSystem<tuint<_Dims>, tuint<_Deg>, _EType, _Trans, tuint<_Indices>> {
    using Base = StaticEigenSystem<tuint<_Dims>, tuint<_Deg>, _EType, _Trans, tuint<_Indices>>;
    EigenSystemImpl(const t_EigenVectors<Base>&):Base() {}
  };

  // ---- Composite EigenSystem  ----

  template<bool _is_orthonormal = false, bool _is_weighted = false>
  struct CompositeTrans : public BasisImpl<_is_orthonormal, _is_weighted> { };

  template<class _ES1, class _ES2>
  struct CompositeEigenSystem : EigenSystemEigenBase<BiEigenVectors<t_EigenVectors<_ES1>, t_EigenVectors<_ES2>>, EigenSystemData<true, _ES1::is_orthonormal() && _ES2::is_orthonormal(), _ES1::dims()+_ES2::dims(), _ES1::indices()+_ES2::indices(), _ES1::is_dynamic() || _ES2::is_dynamic() ? 0U : 1U, CompositeTrans<>>, void> {
    using Base = EigenSystemEigenBase<BiEigenVectors<t_EigenVectors<_ES1>, t_EigenVectors<_ES2>>, EigenSystemData<true, _ES1::is_orthonormal() && _ES2::is_orthonormal(), _ES1::dims()+_ES2::dims(), _ES1::indices()+_ES2::indices(), _ES1::is_dynamic() || _ES2::is_dynamic() ? 0U : 1U, CompositeTrans<>>, void>;
    using Eigenv = t_EigenVectors<Base>;
    CompositeEigenSystem(const Eigenv& eig):Base(Eigenv(_ES1::create_eigen_vectors(eig.left()), _ES2::create_eigen_vectors(eig.right()))) {}
  };

  /**
   * Implementation of a composite ES.
   */
  template<class ... _T1, class ... _T2>
  struct EigenSystem<EigenSystem<_T1...>, EigenSystem<_T2...>> :
  CompositeEigenSystem<EigenSystem<_T1...>, EigenSystem<_T2...>> {
    using SystemLeft = EigenSystem<_T1...>;
    using SystemRight = EigenSystem<_T2...>;
    using Coeff = t_MajorCoefficientType<t_CoefficienctType<SystemLeft>, t_CoefficienctType<SystemRight>>;
    using EigenvLeft = t_EigenVectors<SystemLeft>;
    using EigenvRight = t_EigenVectors<SystemRight>;
    using This = EigenSystem<EigenSystem<_T1...>, EigenSystem<_T2...>>;
    using Base = CompositeEigenSystem<EigenSystem<_T1...>, EigenSystem<_T2...>>;
    using Eigenv = t_EigenVectors<Base>;
    using Var = t_Var<Base>;

    EigenSystem(const OptionMap& om):Base(om.sub("eigenv")) {}
    EigenSystem(const Eigenv& eig):Base(eig) {}

    static const std::string& basis_name() {
      static std::string s_basis_name = "{"+SystemLeft::basis_name()+", "+SystemRight::basis_name()+"}";
      return s_basis_name;
    }

    inline unsigned var_degree(unsigned d) const { return var_degree(Base::eigenvectors(), d); }

    static inline constexpr unsigned left_dims() { return SystemLeft::dims(); }

    static inline constexpr unsigned right_dims() { return SystemRight::dims(); }

    inline SystemLeft left() const { return SystemLeft(Base::eigenvectors().left()); }

    inline SystemRight right() const { return SystemRight(Base::eigenvectors().right()); }

    static inline unsigned var_degree(const Eigenv& eigen, unsigned d) {
      return (d < SystemLeft::dims()) ?
          SystemLeft::var_degree(eigen.left(), d):
          SystemRight::var_degree(eigen.right(), d - SystemLeft::dims());
    }

    static constexpr inline bool zero_integral(unsigned var, unsigned n) {
      return (var < SystemLeft::dims()) ?
          SystemLeft::zero_integral(var, n):
          SystemRight::zero_integral(var - SystemLeft::dims(), n);
    }

    static inline double integral(unsigned var, unsigned n) {
      if(var < SystemLeft::dims()) return SystemLeft::integral(var, n);
      else return SystemRight::integral(var - SystemLeft::dims(), n);
    }

    static inline double inverse_single_norm_square(unsigned var, unsigned n) {
      if(var < SystemLeft::dims()) return SystemLeft::inverse_single_norm_square(var, n);
      else return SystemRight::inverse_single_norm_square(var - SystemLeft::dims(), n);
    }

    static inline double norm(unsigned var, unsigned n) {
      if(var < SystemLeft::dims()) return SystemLeft::norm(var, n);
      else return SystemRight::norm(var - SystemLeft::dims(), n);
    }

    template<class _Indices>
    static inline bool integral(const _Indices& idx, double& I) {
      return SystemLeft::integral(idx.left(), I) && SystemRight::integral(idx.right(), I);
    }

    template<class _Indices, class _Coeff>
    static inline bool integral(const _Indices& idx, unsigned a, _Coeff& I) {
      return SystemLeft::integral(idx.left(), a, I) && SystemRight::integral(idx.right(), a - left_dims(), I);
    }

    template<class _Indices, class _Coeff>
    static inline bool integral(const _Indices& idx, unsigned a1, unsigned a2, _Coeff& I) {
      return SystemLeft::integral(idx.left(), a1, a2, I) && SystemRight::integral(idx.right(), a1 - left_dims(), a2 - left_dims(), I);
    }

    struct EvalData {
      EvalData():data(0) { }

      ~EvalData() {
        if(this->data) {
          delete[] this->data;
          this->data = 0;
        }
      }

      inline Coeff* operator[](size_t i) { return this->ptr0[i]; }
      inline const Coeff* operator[](size_t i) const { return this->ptr0[i]; }

      Coeff* data;
      // pointers to first element per dim
      std::array<Coeff*, Base::indices()> ptr0;
    };

    inline void init_eval(EvalData& ed) const {
      if(!ed.data) {
        std::array<size_t, Base::indices()+1> offs;
        offs[0] = 0;
        for(unsigned d = 0; d<Base::indices(); ++d) offs[d+1] = offs[d] + buffer_size(this->eigenvectors(), d);
        ed.data = new Coeff[offs[Base::indices()]];
        for(unsigned d = 0; d<Base::indices(); ++d) {
          ed.ptr0[d] = ed.data + offs[d];
        }
      }
    }

    inline void copy_eval(const EvalData& src, EvalData& dst, int par = -1) const {
      if(par == -1) {
        std::memcpy(dst.data, src.data, sizeof(Coeff)*total_buffer_size(Base::eigenvectors()));
      } else {
        std::memcpy(dst.ptr0[par], src.ptr0[par],sizeof(Coeff)*buffer_size(Base::eigenvectors(), par));
      }
    }
    
    template<class _EvalData, class _EigenIter, class _Coeff>
    static inline _Coeff basis_product(const _EvalData& ed, unsigned ed_base_idx, const _EigenIter& eit, _Coeff prod) {
      return SystemRight::basis_product(ed, ed_base_idx + SystemLeft::indices(), eit.right(), SystemLeft::basis_product(ed, ed_base_idx, eit.left(), prod));
    }

    template<Basis::CompType _ttype, class _Func>
    inline _Func compute(const Var* x, EvalData& ed, const _Func& _f) const {
      _Func f(_f);
      constexpr unsigned d2 = SystemLeft::indices();
      Coeff phi(1.0);
      precompute<_ttype>(x, ed);
      auto eli = Base::eigenvectors().left().iterator();
      auto eri = Base::eigenvectors().right().iterator();
      do {
        phi = SystemRight::basis_product(ed, d2, eri.indices(), Coeff(1.0));
        eli.clear();
        do {
          f(SystemLeft::basis_product(ed, 0, eli.indices(), phi));
        } while(eli.next());
      }
      while(eri.next());
      return f;
    }

    template<Basis::CompType _ttype>
    inline void precompute(const Var* x, EvalData& ed) const {
      precompute<_ttype>(Base::eigenvectors(), x, ed[0]);
    }

    template<Basis::CompType _ttype, class _Var = Var, class _Coeff = Coeff>
    static inline void precompute(const Eigenv& eig, const _Var* x, _Coeff* res) {
      SystemLeft::template precompute<_ttype, _Var, _Coeff>(eig.left(), x, res);
      SystemRight::template precompute<_ttype, _Var, _Coeff>(eig.right(), x + SystemLeft::indices(), res + SystemLeft::total_buffer_size(eig.left()));
    }

    // TODO: Should be removed
    template<Basis::CompType _ttype, class _Var = Var, class _Coeff = Coeff>
    static inline void precompute(const Eigenv& eig, unsigned i, const _Var& x, _Coeff* res) {
      if(i < SystemLeft::indices()) SystemLeft::template precompute<_ttype, _Var, _Coeff>(eig.left(), i, x, res);
      else SystemRight::template precompute<_ttype, _Var, _Coeff>(eig.right(), i - SystemLeft::indices(), x, res);
    }

    static inline constexpr size_t total_buffer_size(const Eigenv& eig) {
      return SystemLeft::total_buffer_size(eig.left()) + SystemRight::total_buffer_size(eig.right());
    }

    static inline size_t buffer_size(const Eigenv& eig, size_t i) {
      return (i < SystemLeft::indices()) ? This::sub_buffer_size<SystemLeft, EigenvLeft>(eig.left(), i) : This::sub_buffer_size<SystemRight, EigenvRight>(eig.right(), i);
    }
    static inline size_t buffer_size(size_t i) {
      return (i < SystemLeft::indices()) ? This::sub_buffer_size<SystemLeft>(i) : This::sub_buffer_size<SystemRight>(i - SystemLeft::indices());
    }

    static inline void write_es(OptionMap& op) {
      SystemLeft::write_es(op.sub("left", true));
      SystemRight::write_es(op.sub("right", true));
    }
    
    inline void write(OptionMap& op) const {
      Base::eigenvectors().write(op.sub("eigenv", true));
      write_es(op);
    }
  private:
    template<class _Syst, class _Eigen>
    static inline size_t sub_buffer_size(const _Eigen& eig, teif<_Syst::is_dynamic(), size_t> i) { return _Syst::buffer_size(eig, i); }
    template<class _Syst, class _Eigen>
    static inline size_t sub_buffer_size(const _Eigen&, teif<!_Syst::is_dynamic(), size_t> i) { return _Syst::buffer_size(i); }

    template<class _Syst>
    static inline size_t sub_buffer_size(teif<!_Syst::is_dynamic(), size_t> i) { return _Syst::buffer_size(i); }
  };

  template<class _EigenSys>
  using t_EigenSystemLeft = typename _EigenSys::SystemLeft;

  template<class _EigenSys>
  using t_EigenSystemRight = typename _EigenSys::SystemRight;
}

#endif // sfi_EigenSystem_h
