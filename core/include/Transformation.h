#ifndef sfi_transformation_h
#define sfi_transformation_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string>

#include "TransformationBase.h"

namespace sfi {
  template<typename... _T> class Transformation { };
  /**
   * @class sfi::Transformation<T1, T2>
   *
   * Base class of all Transformation s.
   * The params T1 and T2 determines the result Transform.
   * - T1 is EigenVectors and T2 is a trans type
   * - T1 is a Transform and T2 is void
   * - T1 and T2 are Transforms -> Transform<T1, T2>
   *
   * options:
   * name string
   * spectral_filter_exponent int  : Exponent to use for filter, 0 menas off
   * normalize bool                : If normalize to 1
   * compute_covariance bool       : If cov. matrix is to be computed
   * compute_uncertainty bool      : If uncertainties should be computed
   * explicit_post_processing bool : If post processing should NOT be done automatically
   * prune_significance double     : Level of coefficient significance, remove those below, 0.0 means no pruning
   * eigen { degree int }          : Degree for dynamic Transforms. Can be nested if combined Transform.
   */
  template<class _T1, class _T2>
  class Transformation<_T1, _T2> : public TransformationEigenBase<_T1, _T2, t_EigenSystem<tvalue<_transform_type<_T1, _T2>>>::is_dynamic()> {
  public:
    using This = Transformation<_T1, _T2>;
    using Transf = tvalue<_transform_type<_T1, _T2>>;
    using Base = TransformationEigenBase<_T1, _T2, t_EigenSystem<Transf>::is_dynamic()>;
    using Eigenv = t_EigenVectors<Transf>;
  protected:
    Transformation(Context& ctx, const std::string& name):Base(ctx, name) { }

    Transformation(Context& ctx, const OptionMap& opts):Base(ctx, opts) { }

    Transformation(const This& o):Base(o) { }

    template<class _EigenV>
    Transformation(Context& ctx, const std::string& name, const _EigenV& eigen, teif<teq<_EigenV, Eigenv>::value() && Eigenv::is_dynamic()>* = 0):
      Base(ctx, name, eigen) {
    }

    template<class _Transf>
    bool post_process_transform(_Transf& trf) const {
      if(this->m_normalize) {
        trf.scale(trf.is_amplitude() ? sqrt(1.0/trf.integral_pdf()) : 1.0/trf.integral_pdf());
      }
      if(this->m_prune_significance > 0) this->_prune_significance(trf);
      return true;
    }

    template<class _Transf>
    unsigned _prune_significance(_Transf& trf) const {
      unsigned npruned(0);
      const size_t nparams(trf.nparams());
      if(this->m_compute_covariance) {
        for(unsigned i=0; i<nparams; ++i) {
          if(sfi::abs(std::real(trf.par(i)/sqrt(trf.par_cov(i, i)))) < this->m_prune_significance) {
            ++npruned;
            trf.par(i) = 0.0;
          }
        }
        do_log_debug("_prune_significance() '"<<trf.name()<<"' pruned "<<npruned<<" elements, frac: "<<double(npruned)/double(nparams));
      }
      else if(this->m_compute_uncertainty) {
        for(unsigned i=0; i<nparams; ++i) {
          if(trf.par_sign(i) < this->m_prune_significance) {
            ++npruned;
            trf.par(i) = 0.0;
          }
        }
        do_log_debug("_prune_significance() '"<<trf.name()<<"' pruned "<<npruned<<" elements, frac: "<<double(npruned)/double(nparams));
      }
      else {
        do_log_error("_prune_significance() neither uncertainties nor covariance stored");
      }
      return npruned;
    }
  };

  template<class ... _T>
  inline std::ostream& operator<<(std::ostream& out, const Transformation<_T...>& nt) {
    return nt.print(out);
  }

  template<class _Trafo, class _Transf>
  struct _t_is_valid_transform : tbool<teq<t_Transform<_Trafo>, _Transf>::value() || c_is_1D_transform<t_Transform<_Trafo>, _Transf>()> {};

  /** @return true if _Transf is a valid transform for the Transformation _Trafo. */
  template<class _Trafo, class _Transf>
  inline constexpr bool c_is_valid_transform() {
    //return teq<t_Transform<_Trafo>, _Transf>::value() || c_is_1D_transform<t_Transform<_Trafo>, _Transf>();
    return _t_is_valid_transform<_Trafo, _Transf>::value();
  }

  /**
   * @class sfi::NestedTransformation
   *
   * A Transformation that adds capabilities on top of another Transformation.
   *
   * options:
   * nested {} : Options for the nested Transformation
   */
  template<class _SubTransf>
  class NestedTransformation : public Transformation<t_Transform<_SubTransf>, void> {
  public:
    using Transf = t_Transform<_SubTransf>;
    using Eigenv = t_EigenVectors<Transf>;
    using Base = Transformation<Transf, void>;
    using SubTransformation = _SubTransf;

    /** @return Instance of nested Transformation */
    inline _SubTransf& sub_transformation() { return this->m_sub_transformation; }
    inline const _SubTransf& sub_transformation() const { return this->m_sub_transformation; }

    inline std::ostream& print(std::ostream& out) const {
      return Base::print(out)<<this->sub_transformation();
    }

    /** @return New Transform instance */
    inline Transf* create_transform(Context& ctx) const { return m_sub_transformation.create_transform(ctx); }

    inline TransformVariableBase* create_transform_1D(Context& ctx, unsigned d) const {
      return m_sub_transformation.create_transform_1D(ctx, d);
    }

    inline Eigenv eigenvectors() const { return m_sub_transformation.eigenvectors(); }
  protected:
    NestedTransformation(Context& ctx, const std::string& name, const _SubTransf& sub):Base(ctx, name), m_sub_transformation(sub) { }

    NestedTransformation(Context& ctx, const OptionMap& opts):Base(ctx, opts), m_sub_transformation(ctx, this->m_options.sub("nested", true)) { }

    NestedTransformation(const NestedTransformation& o):Base(o), m_sub_transformation(o.m_sub_transformation) { }

    _SubTransf m_sub_transformation;
  };

  template<class _SubTransf>
  inline std::ostream& operator<<(std::ostream& out, const NestedTransformation<_SubTransf>& nt) {
    return nt.print(out);
  }
}

#endif // sfi_transformation_h
