#ifndef sfi_CombinationTransformation_h
#define sfi_CombinationTransformation_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Transformation.h"
#include "MatrixOp.h"

namespace sfi {
  /**
   * @class sfi::CombinationTransformation<T1, T2>
   *
   * Combine N Transforms of types T1 evaluated at a certain point, to a Transform<T1, T2>.
   * The points have dimensionality Dim{T2}.
   * The interpolation of {T1} is expressed by T2.
   * A system of linear equations is solved using SVD to give the
   * coefficients of the target transform Transform<T1, T2>.
   *
   * Options:
   * variables : Variable definitions for the parameters
   * external_coordinates (bool) : If the parameter values are given in external coordinates
   */
  template<class _TransLeft, class _TransRight>
  class CombinationTransformation : public Transformation<_TransLeft, _TransRight> {
  public:
    using This = CombinationTransformation<_TransLeft, _TransRight>;
    using Base = Transformation<_TransLeft, _TransRight>;
    using TransfLeft = _TransLeft;
    using TransfRight = _TransRight;
    using Transf = t_Transform<Base>;

    CombinationTransformation(Context& ctx, const std::string& name):Base(ctx, name), m_params_external_coordinates(false) { }

    CombinationTransformation(Context& ctx, const OptionMap& opts):Base(ctx, opts), m_params_external_coordinates(false) {
      if(opts.has_type<OptVec>("variables")) m_variables.init(opts.sub_vec("variables"));
      opts.get_if("external_coordinates", m_params_external_coordinates);
    }

    bool combine(const TransformSet<TransfLeft>& trans, const Matrix<double>& points, Transf& res, double tolerance = 0.0) const {
      if(trans.size() == 0) {
        do_log_error("combine() Number of points must be > 0");
        return false;
      }
      if(trans.size() != points.rows()) {
        do_log_error("combine() Number of points mismatch: "<<trans.size()<<" != "<<points.rows());
        return false;
      }
      if(!trans[0].variables()) {
        do_log_error("combine() No Variables");
        return false;
      }
      const unsigned npars1(res.eigenvectors().left().params());
      if(trans[0].nparams() != npars1) {
        do_log_error("combine() Number of parameters mismatch, got "<<trans[0].nparams()<<" expected "<<npars1);
        return false;
      }
      const unsigned npars2(res.eigenvectors().right().params());
      if(TransfRight::dims() != points.cols()) {
        do_log_error("combine() Number of values mismatch, got "<<points.cols()<<" expected "<<npars2);
        return false;
      }

      auto vars = std::make_shared<Variables>(m_variables);
      if(m_variables.nvariables() != res.dims()) vars->init(res.dims());
      for(unsigned i=0; i<TransfLeft::dims(); ++i) vars->at(i) = trans[0].variables()->at(i);
      if(!m_variables.nvariables()) {
        for(unsigned i=TransfLeft::dims(); i<res.dims(); ++i) vars->at(i).external_interval(-1,1);
      }
      vars->lock();

      // variables for the right trf
      auto rvars = std::make_shared<Variables>(TransfRight::dims());
      for(unsigned i=0; i<TransfRight::dims(); ++i) rvars->at(i) = vars->at(i+TransfLeft::dims());

      this->init_transform(res, this->m_name, vars);
      const bool _is_ampl = trans[0].is_amplitude();
      res.is_amplitude(_is_ampl);

      const unsigned npoints(trans.size());

      Matrix<double> bt(npoints, npars2);
      TransfRight trfr(res.eigenvectors().right(), "");
      trfr.variables(rvars);
      trfr.full_eval(points, bt, m_params_external_coordinates);
      // column-major form
      Matrix<double> x(npars1, npoints);
      for(unsigned n=0; n<npars1; ++n) {
        for(unsigned i=0; i<npoints; ++i) x(n,i) = trans[i].par(n);
      }
      auto sres = svd_solve(bt, x, tolerance);
      if(!sres) {
        do_log_error("combine() Failed to solve");
        return false;
      }
      if((x.rows() < npars1) || (x.cols() < npars2)) {
        do_log_error("combine() Failed to solve, expected solution of "<<npars1<<" x "<<npars2<<" got "<<x.rows()<<" x "<<x.cols());
        return false;
      }
      for(unsigned n=0; n<npars1; ++n) {
        for(unsigned i=0; i<npars2; ++i) res.par(i*npars1 + n) = x(n,i);
      }
      return true;
    }

    inline Variables& variables() { return m_variables; }
  protected:
    Variables m_variables;
    bool m_params_external_coordinates;
  };
}

#endif // sfi_CombinationTransformation_h
