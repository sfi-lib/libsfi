#ifndef sfi_square_system_h
#define sfi_square_system_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "TransformBase.h"
#include "EigenSystem.h"
#include "trans_tmp.h"

namespace sfi {
  struct EigenSquare {};
  template<> struct _t_is_eigentype<EigenSquare> : ttrue {};

  /** @return Number of parameters for a square eigensystem of degree n */
  constexpr size_t square_npars(size_t n) { return (n*(n+3) + 2)/2; }

  /** @return parameter index for square eigensystems */
  constexpr size_t square_paridx(size_t j, size_t k) { return j*(j+1)/2 + k; }

  template<unsigned _Dim, unsigned _Deg>
  struct _eigen_npars<_Dim, _Deg, EigenSquare> {
    static inline constexpr size_t value() { return powc(square_npars(_Deg), _Dim); }
  };

  template<unsigned _Dim>
  struct _eigen_npars<_Dim, 0U, EigenSquare> {
    static inline size_t value(unsigned _deg) { return powi(square_npars(_deg), _Dim); }
  };

  template<unsigned _Dim, unsigned _Deg, class _IdxType>
  struct EigenIndices<_Dim, _Deg, EigenSquare, _IdxType> : EigenIndicesBase<_Dim, _Deg, EigenSquare, _IdxType> {
    using This = EigenIndices<_Dim, _Deg, EigenSquare, _IdxType>;
    using Base = EigenIndicesBase<_Dim, _Deg, EigenSquare, _IdxType>;
    using Data = EigenVectorsData<_Dim, _Deg, EigenSquare>;

    EigenIndices(const Data& da):Base(da) { }

    EigenIndices(EigenIndices&& o):Base(std::forward<EigenIndices>(o)) {}

    EigenIndices(const EigenIndices& o):Base(o) {}

    inline This& operator=(const This& o) { Base::set(o); return *this; }
  };

  template<class _Transf, unsigned _Dim, unsigned _Deg, class _TType>
  struct TransformImpl<_Transf, EigenSystem<tuint<_Dim>, tuint<_Deg>, EigenSquare, _TType>> : DefaultTransformImpl<_Transf, EigenSystem<tuint<_Dim>, tuint<_Deg>, EigenSquare, _TType>> {
    using EigenSys = EigenSystem<tuint<_Dim>, tuint<_Deg>, EigenSquare, _TType>;

    static inline TransformVariableBase* create_linear_axis_transform(const _Transf&, unsigned, const std::string&, const EigenSys&, unsigned) {
      return 0;
    }

    static inline TransformVariableBase* create_amplitude_axis_transform(const _Transf&, unsigned, const std::string&, const EigenSys&, unsigned) {
      return 0;
    }

    static inline TransformVariableBase* create_transform_1d(unsigned, const std::string&, const EigenSys&) {
      return 0;
    }

    static inline TransformVariableBase* create_marginalized_2d(const _Transf&, unsigned, unsigned, const std::string&, const EigenSys&, unsigned, unsigned) {
      return 0;
    }

    static inline double linear_integral(const _Transf& trf) {
      double s=0;
      if(_Dim == 1) {
        for(unsigned i=0; i<=trf.var_degree(0); ++i) {
          s += coefficient_norm(false, trf.par(square_paridx(i,i)));
        }
      }
      else {
        const unsigned deg = trf.var_degree(0);
        const unsigned np1 = square_npars(deg);
        for(unsigned i=0; i<=deg; ++i) {
          for(unsigned j=0; j<=deg; ++j) {
            s += coefficient_norm(false, trf.par(square_paridx(i,i) + np1*square_paridx(j,j)));
          }
        }
      }
      return s;
    }
  };
  template<unsigned _Dim, unsigned _Deg, class _TType>
  struct SquareEigenSystemBase : StaticEigenSystem<tuint<_Dim>, tuint<_Deg>, EigenSquare, _TType, tuint<_Dim >> {
    using Base = StaticEigenSystem<tuint<_Dim>, tuint<_Deg>, EigenSquare, _TType, tuint<_Dim >>;

    SquareEigenSystemBase(const t_EigenVectors<Base>&):Base() {}
    SquareEigenSystemBase():Base() {}
  };

  template<unsigned _Dim, class _TType>
  struct SquareEigenSystemBase<_Dim, 0U, _TType> : DynamicEigenSystem<tuint<_Dim>, EigenSquare, _TType, tuint<_Dim >> {
    using Base = DynamicEigenSystem<tuint<_Dim>, EigenSquare, _TType, tuint<_Dim >>;

    SquareEigenSystemBase(const t_EigenVectors<Base>& ev):Base(ev) {}
    SquareEigenSystemBase():Base() {}
  };

  template<unsigned _Dim, unsigned _Deg, class _TType>
  struct EigenSystem<tuint<_Dim>, tuint<_Deg>, EigenSquare, _TType> : SquareEigenSystemBase<_Dim, _Deg, _TType> {
    using This = EigenSystem<tuint<_Dim>, tuint<_Deg>, EigenSquare, _TType>;
    using Base = SquareEigenSystemBase<_Dim, _Deg, _TType>;
    using Coeff = t_CoefficienctType<Base>;
    static_assert((_Dim <= 2), "Max dim is 2");

    EigenSystem(const OptionMap& om):Base(t_EigenVectors<Base>(om.sub("eigenv"))){}
    EigenSystem(const t_EigenVectors<Base>& ev):Base(ev) {}
    EigenSystem():Base() {}

    template<Basis::CompType _ttype, class _Func>
    inline _Func compute(const t_Var<Base>* x, t_EvalData<Base>& ed, const _Func& _f) const {
      _Func f(_f);
      const size_t _deg = Base::degree();
      Base::template precompute<_ttype>(x, ed);
      if(_Dim == 1) {
        unsigned i(0), j(0);
        for(i = 0; i<=_deg; ++i) {
          for(j = 0; j<i; ++j) {
            f(2.0*ed.ptr[0][i]*ed.ptr[0][j]);
          }
          f(square(ed.ptr[0][i]));
        }
      }
      else {
        Coeff phi(0.0);
        unsigned i1(0), i2(0), j1(0), j2(0);
        for(j1 = 0; j1<=_deg; ++j1) {
          for(j2 = 0; j2<=j1; ++j2) {
            phi = (j2 == j1) ? square(ed.ptr[1][j1]) : 2.0*ed.ptr[1][j1]*ed.ptr[1][j2];
            for(i1 = 0; i1<=_deg; ++i1) {
              for(i2 = 0; i2<i1; ++i2) {
                f(2.0*ed.ptr[0][i1]*ed.ptr[0][i2]*phi);
              }
              f(square(ed.ptr[0][i1])*phi);
            }
          }
        }
      }
      return f;
    }

    template<class _TIndices>
    static inline bool integral(const _TIndices& /*idx*/, double& /*I*/) {
      return true;
    }

    template<class _TIndices, class _Coeff>
    static inline bool integral(const _TIndices& /*idx*/, unsigned /*a*/, _Coeff& /*I*/) {
      return true;
    }

    template<class _TIndices, class _Coeff>
    static inline bool integral(const _TIndices& /*idx*/, unsigned /*a1*/, unsigned /*a2*/, _Coeff& /*I*/) {
      return true;
    }

    template<class _EvalData, class _EigenIter>
    static inline Coeff basis_product(const _EvalData& /*ed*/, unsigned /*ed_base_idx*/, const _EigenIter& /*eit*/, Coeff prod) {
      return prod;
    }
  };
}

#endif // sfi_square_system_h
