#ifndef sfi_sample_functions_h
#define sfi_sample_functions_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <Sample.h>
#include <Context.h>
#include <ITransform.h>

#include <iostream>
#include <random>

namespace sfi {
  void hit_and_miss_1d_from_function(const std::function<double(const double*)>& func, const std::function<void(unsigned,double)>& target, unsigned nevents, Context::RandomTy& rnd, double upper = 1.8, double xmin = -1.0, double xmax = 1.0, bool debug = false);

  void sample_random_1d_from_function(sfi::Sample<1>* res, const std::function<double(const double*)>& func, unsigned nevents, Context::RandomTy& rnd, double upper = 1.8, double xmin = -1.0, double xmax = 1.0, bool debug = false);

  void sample_random_2d_from_function(sfi::Sample<2>* res, const std::function<double(const double*)>& func, unsigned nevents, Context::RandomTy& rnd, double upper = 1.8,
                                      double xmin = -1.0, double xmax = 1.0, double ymin = -1.0, double ymax = 1.0, bool debug = false);
  /**
   * Construct a sample using hit-and-miss using a 1D transform.
   */
  void sample_random_1d(sfi::Sample<1>* res, const ITransform& trans, unsigned nevents, Context::RandomTy& rnd, double upper = 1.8);

  /**
   * Construct a sample using hit-and-miss using a 2D transform.
   */
  void sample_random_2d(sfi::Sample<2>* res, const ITransform& trans, unsigned nevents, Context::RandomTy& rnd, double upper = 1.8, bool debug = false);
}

#endif // sfi_sample_functions_h
