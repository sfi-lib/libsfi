#ifndef sfi_sparse_system_h
#define sfi_sparse_system_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "EigenSystem.h"

namespace sfi {
  struct EigenMonomicSparse {};
  template<> struct _t_is_eigentype<EigenMonomicSparse> : ttrue {};

  template<unsigned _Dims, unsigned _Deg>
  struct _eigen_npars<_Dims, _Deg, EigenMonomicSparse> {
    static inline constexpr size_t value() { return n_over_kc(_Deg + _Dims, _Dims); }
  };

  template<unsigned _Dims>
  struct _eigen_npars<_Dims, 0U, EigenMonomicSparse> {
    static inline constexpr size_t value(unsigned _deg) { return n_over_kc(_deg + _Dims, _Dims); }
  };

  template<>
  struct t_eigenvector_type_name<EigenMonomicSparse> {
    static const std::string& apply() {
      static const std::string _s = "sparse";
      return _s;
    }
  };

  template<unsigned _Dims, unsigned _Deg, class _IdxType>
  struct EigenIndices<_Dims, _Deg, EigenMonomicSparse, _IdxType> : public EigenIndicesBase<_Dims, _Deg, EigenMonomicSparse, _IdxType> {
    using This = EigenIndices<_Dims, _Deg, EigenMonomicSparse, _IdxType>;
    using Base = EigenIndicesBase<_Dims, _Deg, EigenMonomicSparse, _IdxType>;
    using Data = EigenVectorsData<_Dims, _Deg, EigenMonomicSparse>;

    EigenIndices(const Data& da):Base(da), m_sum(0) { }

    EigenIndices(EigenIndices&& o):Base(std::forward<EigenIndices>(o)), m_sum(o.m_sum) {}

    EigenIndices(const EigenIndices& o):Base(o), m_sum(o.m_sum) {}

    inline void clear() { Base::clear(); this->m_sum = 0; }

    inline bool next() {
      unsigned i = 0;
      do {
        ++m_sum;
        if(((++this->m_indices[i]) > Base::degree()) || (m_sum > Base::degree())) {
          m_sum -= this->m_indices[i];
          this->m_indices[i] = 0;
          ++i;
        }
        else i = Base::dims() + 1;
      }
      while(i < Base::dims());
      return (i != Base::dims());
    }

    /** Maximum allowed value in set */
    inline _IdxType max_idx(size_t i) { return Base::degree() - (m_sum -  this->m_indices[i]); }

    inline void set(size_t i, _IdxType val) {
      assert(i < this->dims() && "Invalid index");
      assert(val <= this->degree() && "Invalid value");
      assert((m_sum -  this->m_indices[i] + val) <= this->degree() && "Invalid value");
      m_sum = m_sum -  this->m_indices[i] + val;
      this->m_indices[i] = val;
    }

    inline uint64_t index() const {
      size_t sum(m_sum);
      uint64_t res = n_over_kc(Base::degree() + Base::dims(), Base::dims()) - (Base::degree() - sum + 1);
      for(unsigned d=0; d<(Base::dims() - 1); ++d) {
        sum -= this->m_indices[d];
        res -= (Base::degree() - sum)*n_over_kc(Base::degree() + d + 1 - sum, d + 1)/(d+2);
      }
      return res;
    }

    inline This& operator=(const This& o) { m_sum = o.m_sum; Base::set(o); return *this; }
  protected:
    size_t m_sum;
  };

  template<unsigned _Dims, unsigned _Deg, class _Trans>
  struct EigenSystem<tuint<_Dims>, tuint<_Deg>, EigenMonomicSparse, _Trans> :
  StaticEigenSystem<tuint<_Dims>, tuint<_Deg>, EigenMonomicSparse, _Trans, tuint<_Dims>> {
    using This = EigenSystem<tuint<_Dims>, tuint<_Deg>, EigenMonomicSparse, _Trans>;
    using Base = StaticEigenSystem<tuint<_Dims>, tuint<_Deg>, EigenMonomicSparse, _Trans, tuint<_Dims>>;

    EigenSystem(const OptionMap&):Base(){}
    EigenSystem(const t_EigenVectors<Base>&):Base() { }
    EigenSystem():Base() { }

    ~EigenSystem() { }

    template<Basis::CompType _ttype, class _Func>
    static inline _Func compute(const t_Var<Base>* x, t_EvalData<Base>& ed, const _Func& _f) {
      _Func f(_f);
      t_CoefficienctType<_Trans> phi(1.0);
      unsigned i(0), d(0), id(0), sum(0), g(0);
      Base::template precompute<_ttype>(x, ed);
      This::reset_eval(ed);
      for(d = 1; d<_Dims; ++d) phi *= ed[d][0];
      do {
        g = _Deg - sum;
        for(id=0; id <= g; ++id) f(ed.ptr[0][id]*phi);
        for(i = 1; (i != _Dims) && ((++ed.ptr[i],++sum) > _Deg); ++i) {
          sum -= ed.ptr[i] - ed[i];
          ed.ptr[i] = ed[i];
        }
        for(phi = 1.0, d = 1; d != _Dims; ++d) phi *= (*ed.ptr[d]);
      }
      while(i != _Dims);
      return f;
    }
  };

  template<unsigned _Dims, class _Trans>
  struct EigenSystem<tuint<_Dims>, tuint<0U>, EigenMonomicSparse, _Trans> :
  DynamicEigenSystem<tuint<_Dims>, EigenMonomicSparse, _Trans, tuint<_Dims>> {
    using This = EigenSystem<tuint<_Dims>, tuint<0U>, EigenMonomicSparse, _Trans>;
    using Base = DynamicEigenSystem<tuint<_Dims>, EigenMonomicSparse, _Trans, tuint<_Dims>>;

    EigenSystem(const OptionMap& om):Base(t_EigenVectors<Base>(om.sub("eigenv"))){}
    EigenSystem(const t_EigenVectors<Base>& eig):Base(eig) {}

    template<Basis::CompType _ttype, class _Func>
    inline _Func compute(const t_Var<Base>* x, t_EvalData<Base>& ed, const _Func& _f) const {
      _Func f(_f);
      const unsigned g = this->eigenvectors().degree();
      t_CoefficienctType<_Trans> phi(1.0);
      unsigned i(0), d(0), id(0), sum(0);
      Base::template precompute<_ttype>(x, ed);
      This::reset_eval(ed);
      for(d = 1; d<_Dims; ++d) phi *= ed[d][0];
      do {
        d = g - sum;
        for(id=0; id <= d; ++id) f(ed.ptr[0][id]*phi);
        for(i = 1; (i != _Dims) && ((++ed.ptr[i], ++sum) > g); ++i) {
          sum -= ed.ptr[i] - ed[i];
          ed.ptr[i] = ed[i];
        }
        phi = 1.0;
        for(d=1; d != _Dims; ++d) phi *= (*ed.ptr[d]);
      }
      while(i != _Dims);
      return f;
    }
  };
}

#endif // sfi_sparse_system_h
