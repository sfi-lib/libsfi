#ifndef sfi_AmplitudeTransformation_h
#define sfi_AmplitudeTransformation_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Transformation.h"
#include "TransformCall.h"
#include "timer.h"

namespace sfi {

  template<class _Transf>
  struct SqrtWeightFunc {
    using This = SqrtWeightFunc<_Transf>;

    SqrtWeightFunc(const _Transf& trf, double _regparam, unsigned& _ninvalid, unsigned& _nvalid, bool _zeros = true):
      ninvalid(_ninvalid), nvalid(_nvalid), pdfeval(&trf, 1.0, false), regparam(_regparam), zeros(_zeros) {
      this->ninvalid = 0;
      this->nvalid = 0;
    }

    SqrtWeightFunc(const SqrtWeightFunc& o):
      ninvalid(o.ninvalid), nvalid(o.nvalid), pdfeval(o.pdfeval), regparam(o.regparam), zeros(o.zeros) {}

    ~SqrtWeightFunc() {  }

    inline double eval(const double* x) const {
      const double pv(pdfeval(x));
      if(pv <= 0.0) {
        ++ninvalid;
        return zeros ? 0.0 : 1.0/(sqrt(-pv)  + regparam);
      }
      else {
        ++nvalid;
        const double w = 1.0/(sqrt(pv) + regparam);
        return w;
      }
    }
  protected:
    unsigned& ninvalid;
    unsigned& nvalid;
    mutable Evaluator<_Transf> pdfeval;
    double regparam;
    bool zeros;
  };

  template<class _Transf>
  struct WeightFunc {
    using This = WeightFunc<_Transf>;

    WeightFunc(const _Transf& trf, double _regparam, unsigned& _ninvalid):ninvalid(_ninvalid), pdfeval(&trf, 1.0, false), regparam(_regparam) {
      this->ninvalid = 0;
    }

    WeightFunc(const WeightFunc& o):ninvalid(o.ninvalid), pdfeval(o.pdfeval), regparam(o.regparam) {}

    inline double eval(const double* x) const {
      const double pv(pdfeval(x));
      if(pv <= 0.0) ++ninvalid;
      return 1.0/(pv + sgn(pv)*regparam);
    }
  protected:
    unsigned& ninvalid;
    mutable Evaluator<_Transf> pdfeval;
    double regparam;
  };

  template<class _Transf> struct tremap_if_single<sfi::SqrtWeightFunc<_Transf>> : public ttrue {};
  template<class _Transf> struct tremap_if_single<sfi::WeightFunc<_Transf>> : public ttrue {};

  template<class _AmpDebug = tfalse>
  struct amplitude_transform_result : public result {
    amplitude_transform_result(bool st = false, unsigned /*maxiters*/ = 0):result(st), niters(0), nevents(0), diff(0.0), ninvalid_events(0) {
    }
    unsigned niters;
    unsigned nevents;
    double diff;
    unsigned ninvalid_events;
    inline void fill(unsigned /*it*/, double /*res_I*/, double /*delta_I*/, double /*candidate_I*/, double /*nnegative*/) { }
  };

  template<>
  struct amplitude_transform_result<ttrue> : public result {
    amplitude_transform_result(bool st = false, unsigned maxiters = 0):result(st), niters(0), nevents(0), diff(0.0), ninvalid_events(0),
        res_I_v(maxiters, 0.0), delta_I_v(maxiters, 0.0),
        candidate_I_v(maxiters, 0.0), nnegative_I_v(maxiters, 0.0) {
    }
    static constexpr unsigned N_dev_vals = 100;
    unsigned niters;
    unsigned nevents;
    double diff;
    unsigned ninvalid_events;
    std::vector<double> res_I_v;
    std::vector<double> delta_I_v;
    std::vector<double> candidate_I_v;
    std::vector<double> nnegative_I_v;
    inline void fill(unsigned it, double res_I, double delta_I, double candidate_I, double nnegative) {
      res_I_v[it] = res_I;
      delta_I_v[it] = delta_I;
      candidate_I_v[it] = candidate_I;
      nnegative_I_v[it] = nnegative;
    }
  };

  /**
   * @class sfi::AmplitudeTransformation<_Trafo>
   *
   * Computes the square root of a pdf.
   * The parameter _Trafo is the base Transformation,
   * usually ProjectionTransformation.
   * The Transform integral (I) has been found to be
   * indicative of convergence.
   * The algorithm will refine the solution until
   * the difference between I(n) and I(n-1) is below 'max_diff',
   * or 'max_iterations' has been reached.
   * 'initial_linear' controls whether an initial linear
   * transform is performed or the initial guess should
   * be {1, 0, ..., 0}.
   * The parameter 'speed' controls speed (surprise).
   * If I diverges,'regparam' may be set to a value >0
   * to stabilize the process.
   */
  template<class _Trafo, class _AmpDebug = tfalse>
  class AmplitudeTransformation : public NestedTransformation<_Trafo> {
  public:
    using This = AmplitudeTransformation<_Trafo, _AmpDebug>;
    using Base = NestedTransformation<_Trafo>;

    AmplitudeTransformation(const This& o):
      Base(o), m_max_iterations(o.m_max_iterations), m_max_diff(o.m_max_diff),
      m_regparam(o.m_regparam), m_speed(o.m_speed), m_initial_linear(o.m_initial_linear) {
      bind_options();
    }

    AmplitudeTransformation(Context& ctx, const OptionMap& opts):
      Base(ctx, opts), m_max_iterations(10), m_max_diff(1e-4),
      m_regparam(0.0), m_speed(1.0), m_initial_linear(false) {
      bind_options();
      if(!opts.has("compute_uncertainty")) this->m_compute_uncertainty = false;
    }

    AmplitudeTransformation(Context& ctx, const std::string& name):
      AmplitudeTransformation(ctx, name, _Trafo(ctx, name)) {
      this->m_compute_uncertainty = false;
    }

    AmplitudeTransformation(Context& ctx, const std::string& name, const _Trafo& sub_transfo):
      Base(ctx, "AmplitudeTransformation{"+name+"}", sub_transfo),
      m_max_iterations(10),
      m_max_diff(1e-4),
      m_regparam(0.0),
      m_speed(1.0),
      m_initial_linear(false) {
      this->m_compute_uncertainty = false;
      bind_options();
    }

    /** Get/set if an initial linear transform should be used to get start guess */
    inline unsigned initial_linear() const { return m_initial_linear; }
    inline This& initial_linear(bool b) { m_initial_linear = b; return *this; }

    /** Get/set max iterations */
    inline unsigned max_iterations() const { return m_max_iterations; }
    inline This& max_iterations(unsigned b) { m_max_iterations = b; return *this; }

    /** Get/set maximum coefficient difference */
    inline double max_diff() const { return m_max_diff; }
    inline This& max_diff(double md) { m_max_diff = md; return *this; }

    /** Get/set regularization parameter */
    inline double regparam() const { return m_regparam; }
    inline This& regparam(double rp) { m_regparam = rp; return *this; }

    /** Get/set the speed factor, must be between 0 and 1 */
    inline double speed() const { return m_speed; }
    inline This& speed(double rp) { m_speed = rp; return *this; }

    using transform_result = amplitude_transform_result<_AmpDebug>;

    template<class ..._T, class _Transf>
    transform_result
    transform(Context& ctx, const TransformCall<_T...>& ctrf, _Transf& res) const {
      static_assert((c_is_valid_transform<_Trafo, _Transf>()), "Invalid transform");
      transform_result tres(false, m_max_iterations+2);

      using Transf = _Transf;
      double regparam = m_regparam;

      if(!this->init_transform(res, &ctrf.sample())) return tres;
      res.is_amplitude(false);
      double res_I(0.0), tmp_I(0.0);

      // Step 0: linear transformation
      auto ltrans_res = ctrf.transform(ctx, this->m_sub_transformation, res);
      res_I = res.integral_pdf();
      const double Nev(ltrans_res.sum_weights);
      tres.nevents = Nev;
      const double sqrtNev(sqrt(Nev));
      unsigned ninvalid, nvalid;

      if(teq<ttrue, _AmpDebug>::value()) tres.fill(0, res_I, res_I/Nev - 1, res_I, 0);

      do_log_verbose("pdf_transform() iter: 0 res_I: "<<res_I<<"  Nev: "<<Nev<<" sample: "<<ctrf.sample().name());

      if(m_max_iterations == 0) {
        tres.status = this->post_process_transform(res);
        tres.diff = 0;
        tres.niters = 1;
        return tres;
      }

      Transf tmp(res);
      double tmp_rdiff_I(0.0);
      if(m_initial_linear) {
        // Step 1: Weight positive events by 1.0/sqrt(c*P)
        SqrtWeightFunc<Transf> wf(res, regparam, ninvalid, nvalid);
        auto swct = ctrf.add_function(wf);
        swct.transform(ctx, this->m_sub_transformation, tmp);
        tmp_I = tmp.square_integral();
        tmp_rdiff_I = fabs(tmp_I/res_I - 1);

        do_log_verbose("pdf_transform() rdiff_I: "<<tmp_rdiff_I<<" t1: "<<tmp_I<<" ninvalid: "<<ninvalid<<" valid: "<<nvalid);

        if(teq<ttrue, _AmpDebug>::value()) {
          tres.fill(1, tmp_I, tmp_rdiff_I, tmp_I, ninvalid);
        }

        if(tmp_rdiff_I > 2.0) {
          do_log_warning("pdf_transform() Large deviation of step 2 integral: "<<tmp_I<<" expected "<<Nev);
        }

        if(m_max_iterations == 1) {
          res = tmp;
          tres.status = this->post_process_transform(res);
          tres.diff = 0;
          tres.niters = 2;
          tres.ninvalid_events = ninvalid;
          return tres;
        }
      }
      else {
        if(m_max_iterations < 2) {
          do_log_warning("pdf_transform() skipping initial linear transform, but max iterations="<<m_max_iterations);
        }
        tmp.clear();
        tmp[0] = sqrtNev;
        // set the start guess to the uniform distribution,
        // in this case the first step is always a linear transform scaled by 1/sqrt(Nev)
        res.scale(1.0/sqrtNev);
        compute_step(tmp, res, tmp, m_speed);
        tmp_I = tmp.square_integral();
        tmp_rdiff_I = fabs(tmp_I/res_I - 1);
        if(teq<ttrue, _AmpDebug>::value()) tres.fill(1, tmp_I, tmp_rdiff_I, res_I, 0);
        do_log_verbose("pdf_transform() iter: 1 tmp_I: "<<tmp_I<<" rdiff_I: "<<tmp_rdiff_I);
      }

      double speed(m_speed), tmp2_I(0.0), rdiff_I(0.0), ti;
      unsigned iter = 2;
      if(m_max_iterations > 1) {
        Timer<true> timer;
        do {
          timer.start();
          auto wct = ctrf.add_function(WeightFunc<Transf>(tmp, regparam, ninvalid));
          wct.transform(ctx, this->m_sub_transformation, res);
          ti = timer.stop();
          tmp2_I = res.square_integral();
          tres.ninvalid_events = ninvalid;

          compute_step(tmp, res, tmp, speed);
          res_I = tmp.square_integral();
          rdiff_I = fabs((res_I - tmp_I)/tmp_I);
          do_log_verbose("pdf_transform() iter: "<<iter<<" ** rdiff_I:"<<rdiff_I<<" speed: "<<speed<<" regp: "<<regparam<<" tmp2_I: "<<tmp2_I<<" time: "<<ti<<" ninvalid="<<tres.ninvalid_events<<" res_I: "<<res_I);

          tmp_I = res_I;
          tmp_rdiff_I = rdiff_I;
          // for monitoring
          if(teq<ttrue, _AmpDebug>::value()) tres.fill(iter, res_I, rdiff_I, tmp2_I, tres.ninvalid_events);
        }
        while((++iter < m_max_iterations) && (rdiff_I > m_max_diff));
      }
      bool all_ok = false;
      if(rdiff_I > m_max_diff) {
        do_log_warning("pdf_transform() >>> Failed to converge niter="<<iter<<" ("<<m_max_iterations<<") rdiff_I:"<<rdiff_I<<" ("<<m_max_diff<<") regparam: "<<regparam<<" integral: "<<res_I<<" Nevents: "<<Nev);
        all_ok = false;
      }
      else {
        do_log_debug("pdf_transform() >>> Converged niter: "<<iter<<" rdiff_I:"<<rdiff_I<<" ("<<m_max_diff<<") regparam: "<<regparam<<" integral: "<<res_I<<" sample: "<<ctrf.sample().name()<<" Nevents: "<<Nev);
        all_ok = true;
        /*if(this->m_compute_uncertainty) {
          tmp.uncertainties() = res.uncertainties();
          tmp.uncertainties() *= 0.5;
        }*/
      }
      res = tmp;
      res.is_amplitude(true);
      tres.status = this->post_process_transform(res) && all_ok;
      tres.diff = rdiff_I;
      tres.niters = iter - 1;
      return tres;
    }

  protected:
    template<class _Transf>
    inline void compute_step(const _Transf& tmp, const _Transf& tmp2, _Transf& res, double speed) const {
      const double half_speed = 0.5*speed;
      for(unsigned k=0; k<res.npar(); ++k) {
        res.par(k) = (1.0 - half_speed)*tmp.par(k) + half_speed*tmp2.par(k);
      }
      if(this->m_compute_uncertainty) {
        for(unsigned k=0; k<res.npar(); ++k) {
          res.par_unc(k) = sqrt(square((1.0 - half_speed)*tmp.par_unc(k)) + square(half_speed*tmp2.par_unc(k)));
        }
      }
    }

    void bind_options() {
      this->m_options.bind("max_iterations", m_max_iterations);
      this->m_options.bind("max_diff", m_max_diff);
      this->m_options.bind("regparam", m_regparam);
      this->m_options.bind("speed", m_speed);
      this->m_options.bind("initial_linear", m_initial_linear);
    }

    unsigned m_max_iterations;
    double m_max_diff;
    double m_regparam;
    double m_speed;
    bool m_initial_linear;

  };
}

#endif // sfi_AmplitudeTransformation_h
