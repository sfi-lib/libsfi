#ifndef sfi_MLTransformation_h
#define sfi_MLTransformation_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Sample.h"
#include "Transformation.h"
#include "TransformCall.h"
#include "Evaluator.h"
#include "ILikelihoodMaximizer.h"
#include "LikelihoodMaximizerFactory.h"
#include "ILikelihood.h"

namespace sfi {
  /**
   * @class MLTransformation
   *
   * Estimate the coefficient of a Transform by using maximum likelihood.
   * The likelihood is: I - sum ln(p(x))
   * In case of a linear transform the logarithm is protected against values <= 0.
   */
  template<class _Eigen, class _TType>
  class MLTransformation : public Transformation<_Eigen, _TType> {
  public:
    using This = MLTransformation<_Eigen, _TType>;
    using Base = Transformation<_Eigen, _TType>;
    using Transf = t_Transform<Base>;

    MLTransformation(Context& ctx, const OptionMap& opts);

    MLTransformation(const This& o) = default;

    ~MLTransformation() { }

    inline bool square_pdf() const { return m_square_pdf; }
    inline This& square_pdf(bool b) { m_square_pdf = b; return *this; }

    inline bool do_minos() const { return m_do_minos; }
    inline This& do_minos(bool b) { m_do_minos = b; return *this; }

    inline bool do_hesse() const { return m_do_hesse; }
    inline This& do_hesse(bool b) { m_do_hesse = b; return *this; }

    inline bool print_covariance() const { return m_print_covariance; }
    inline This& print_covariance(bool b) { m_print_covariance = b; return *this; }

    inline bool debug_minuit() const { return m_debug_minuit; }
    inline This& debug_minuit(bool b) { m_debug_minuit = b; return *this; }

    inline This& initial(const Transf* in) { m_initial = in; return *this; }

    template<class ..._T>
    bool
    transform(Context& ctx, const TransformCall<_T...>& ctrf, Transf& res) const {
      return fit_transform(ctx, ctrf.sample(), res, ctrf.event_selector());
    }
  protected:
    using SampleTy = Sample<_Eigen::dims()>;

    template<class _EventSel>
    bool fit_transform(Context& ctx, const SampleTy& _sample, Transf& res, const _EventSel& esel) const;

    class UnbinnedTrfLH : public ILikelihood {
    public:
      UnbinnedTrfLH(Evaluator<Transf>& ev, const SampleTy& spl, Transf& trf):m_spl(spl), m_ev(ev), m_trf(trf) {}

      virtual ~UnbinnedTrfLH() {}

      virtual double do_eval(Context&) {
        const unsigned Nev(m_spl.events());
        auto* pvals = m_pars.values();
        for(unsigned i=0; i<m_trf.nparams(); ++i) m_trf[i] = pvals[i];

        double f = m_trf.integral_pdf();
        double p(0.0);
        if(m_trf.is_amplitude()) for(unsigned i=0; i<Nev; ++i) f -= ln(m_ev(m_spl.x(i)));
        else {
          for(unsigned i=0; i<Nev; ++i) {
            p = m_ev(m_spl.x(i));
            if(p > 0.0) f -= ln(p);
          }
        }
        return 2.0*f;
      }

      virtual bool do_init_parameters(const OptionMap&, Parameters& pars) {
        m_pars = pars.par_array("a", m_trf.nparams());
        for(unsigned i=0; i<m_trf.nparams();++i) m_pars[i].initial(m_trf[i]).step_length(0.1);
        return true;
      }

      virtual void get_pdfs(std::vector<IPDF*>&) {}

      virtual void do_print(std::ostream&) const {};
    protected:
      const SampleTy& m_spl;
      Evaluator<Transf>& m_ev;
      Transf& m_trf;
      ParameterArray m_pars;
    };

    bool m_square_pdf;
    bool m_do_minos;
    bool m_do_hesse;
    bool m_print_covariance;
    bool m_debug_minuit;
    Transf* m_initial;
    OptionMap m_opts;
  };

  template<class _Eigen, class _TType>
  MLTransformation<_Eigen, _TType>::MLTransformation(Context& ctx, const OptionMap& opts):
    Base(ctx, opts),
    m_square_pdf(opts.get_as<bool>("square_pdf", false)), m_do_minos(opts.get_as<bool>("do_minos", true)),
    m_do_hesse(opts.get_as<bool>("do_hesse", true)), m_print_covariance(opts.get_as<bool>("print_covariance", false)),
    m_debug_minuit(opts.get_as<bool>("debug_minuit", false)), m_initial(0), m_opts(opts) {
  }

  template<class _Eigen, class _TType>
  template<class _EventSel>
  bool MLTransformation<_Eigen, _TType>::fit_transform(Context& ctx, const SampleTy& _sample, Transf& res, const _EventSel& esel) const {
    const unsigned np(res.npar());

    auto& sample = *ctx.memory_pool().pool<SampleTy>()->get(_sample.events(), _sample.name());
    _sample.select_events(sample, esel);

    this->init_transform(res, &sample);
    res.is_amplitude(m_square_pdf);

    OptionMap opts(m_opts);
    opts.set("do_minos", m_do_minos);
    opts.set("do_hesse", m_do_hesse);

    LikelihoodMaximizerFactory lhmf(opts, ctx);

    auto ev = res.evaluator();

    for(unsigned i=0; i<res.npar(); ++i) res.par(i) = i ? 0.0 : 1.0;
    res.normalize(sample.events());

    UnbinnedTrfLH lh(*ev, sample, res);

    auto* minuit = lhmf.create(Parameters(), ctx, opts, &lh, opts);

    if(m_debug_minuit) minuit->set_print_level(PrintLevel::Trace);
    else minuit->set_print_level(PrintLevel::Warning);

    minuit->set_strategy(2);

    bool error(false);
    bool repeat = true;
    unsigned repeats = 0;
    std::normal_distribution<> gaus(1.0, 0.05);
    while(repeat && (repeats < 10)) {
      // Make a new gausian guess
      if(repeats) {
        for(unsigned p=0; p<np; ++p) {
          double pval = m_initial ? m_initial->par(p)*gaus(ctx.rnd_gen()) : res.par(p);
          if(!minuit->set_parameter_value(p, pval)) {
            do_log_error("fit() error setting param "<<p<<" to "<<pval);
            error = true;
          }
        }
      }

      if (!minuit->do_run_migrad(5000, 1)) {
        do_log_error("fit() MIGRAD failed");
        error = true;
      }
      if(!m_square_pdf && error && m_initial) {
        repeat = true;
        error = false;
      }
      else repeat = false;
      ++repeats;
    }

    if(m_do_hesse && (!error)) {
      if (!minuit->do_run_hesse()) {
        do_log_error("fit() HESSE failed");
        error = true;
      }
    }
    if(m_do_minos && (!error)) {
      if(!minuit->do_run_minos()) {
        do_log_error("fit() MINOS failed");
        error = true;
      }
    }

    if(!error) {
      auto& pars = minuit->get_parameters();
      // res.parameters() = minuit->get_min_parameters();
      for(unsigned p=0; p<np; ++p) {
        res.par(p) = pars[p].value();
        res.par_unc(p) = pars[p].uncertainty();
      }
      if(res.compute_covariance()) minuit->get_covariance(res.covariance());
    }
    sample.release();
    return !error;
  }
}
#endif // sfi_MLTransformation_h
