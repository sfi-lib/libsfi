#ifndef sfi_ISample_h
#define sfi_ISample_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <type_traits>

#include "EventSelector.h"
#include "Variables.h"
#include "Histogram.h"
#include "DataSource.h"

namespace sfi {
  class ITransformation;
  template<class _T> class Matrix;
  class Log;

  /**
   * @class sfi::ISample
   *
   * Interface to Sample. A Sample stores data used as input to Transformations
   * in a Matrix. In addition to the contained Matrix, a Sample
   * points to its DataSource, has a name and also points to
   * its Variables.
   *
   * To be used by Transformations, a Sample needs to be
   * normalized, i.e. its values transformed to an internal
   * representation. The normalization step may include
   * a non linear transformation, see Variables for available
   * options.
   *
   * A Sample is capable of providing 1D or 2D histograms
   * of its data.
   *
   * Specific options used to build the sample are copied to the meta data OptionMap.
   */
  class ISample {
  public:
    virtual ~ISample() {}

    /** @return DataSource for this sample */
    virtual std::shared_ptr<const DataSource> get_data_source() const = 0;
    
    /** Set DataSource */
    virtual void set_data_source(const std::shared_ptr<const DataSource>& ds) = 0;

    /** Print the content of this Sample to stdout */
    virtual void do_dump() const = 0;

    /** Write the sample to named text file */
    virtual void do_write(const std::string& filename) = 0;

    /** set/get sample name */
    virtual const std::string& get_name() const = 0;
    virtual void set_name(const std::string& n) = 0;

    /** @return If this sample is normalized, i.e. its values are transformed to internal coordinates */
    virtual bool get_is_normalized() const = 0;

    /** @return The type name of the Sample */
    virtual const std::string& get_type_name() const = 0;

    /** @return If this sample is weighted */
    virtual bool get_use_weight() const = 0;

    /** get/set number of events */
    virtual unsigned get_events() const = 0;
    virtual void set_events(unsigned ev) = 0;

    /** @return Number of dimensions */
    virtual unsigned get_dim() const = 0;

    /** @return If data is owned by the Sample */
    virtual bool get_owns_data() const = 0;

    /**
     * @return Data for event ev
     * @throws SampleException If ev is not in the sample, i.e. out of bounds
     */
    virtual const double* get_x(unsigned ev) const  = 0;
    
    /**
     * @return Data for event ev and dimension d
     * @throws SampleException If ev is not in the sample, i.e. out of bounds
     */
    virtual double get_x(unsigned ev, unsigned d) const = 0;
    virtual double& get_x(unsigned ev, unsigned d) = 0;

    /**
     * @return Weight for event ev
     * @throws SampleException If ev is not in the sample, i.e. out of bounds
     */
    virtual double get_w(unsigned ev) const = 0;
    virtual void set_w(unsigned ev, double _w) = 0;

    /** @return Variables */
    virtual PVariables get_variables() = 0;
    virtual const PVariables get_variables() const = 0;

    /** Set Variables */
    virtual void set_variables(const PVariables&) = 0;

    /** Release this Sample instance, i.e. reuse the instance */
    virtual void do_release() = 0;

    /**
     * Normalize Sample, i.e. transform to internal coordinate system.
     * A Sample that should be used in a Transformation must be normalized.
     */
    virtual void do_normalize(Log& _log) = 0;

    /**
     * @param v The variable to get a historgram for
     * @param nbins Number of bins
     * @param external_coordinates Use external or internal coordinates
     * @param esel Event selector to use
     * @return 1D histogram for given variable
     */
    virtual Histogram<true, double> get_histogram_1D(size_t v, int nbins, bool external_coordinates = false, EventSelectorID esel = AllEventSelID) const = 0;

    /**
     * Get 1D histograms for each variable
     * @param nbins Number of bins
     * @param external_coordinates Use external or internal coordinates
     * @param esel Event selector to use
     * @return vector of 1D histograms
     */
    virtual std::vector<Histogram<true, double>> get_histograms_1D(int nbins, bool external_coordinates = false, EventSelectorID esel = AllEventSelID) const = 0;

    /**
     * @param v1 The first variable to get a 2D historgram for
     * @param v2 The second variable
     * @param nbins Number of bins
     * @param external_coordinates Use external or internal coordinates
     * @param esel Event selector to use
     * @return 2D histogram for given variables
     */
    virtual Histogram2D<true,double> get_histogram_2D(unsigned v1, unsigned v2, unsigned nbins, bool external_coordinates = false, EventSelectorID esel = AllEventSelID) const = 0;

    /** @return The underlying Matrix */
    virtual Matrix<double>& get_matrix() = 0;
    virtual const Matrix<double>& get_matrix() const = 0;

    /** @return Meta data for the sample */
    virtual OptionMap& get_meta_data() = 0;
    virtual const OptionMap& get_meta_data() const = 0;
    
    /**
     * Append a sample with the same dimensionality.
     * @throws SampleException
     */
    virtual bool do_append(const ISample& sb) = 0;

    /** Delete all data from this sample */
    virtual void do_clear() = 0;
  };

  /**
   * @class SampleMap
   *
   * A collection of named Samples. The sample map owns the instances.
   * A Sample instance can be detached by using the method get.
   */
  class SampleMap {
  public:
    SampleMap();
    SampleMap(SampleMap&& o);
    ~SampleMap();
    inline void put(const std::string& id, ISample*spl) {
      m_samples.insert(std::make_pair(id,spl));
    }
    inline ISample* operator[](const std::string& id) { return m_samples[id]; }
    inline ISample* get(const std::string& id) {
      auto* r = m_samples[id];
      m_samples.erase(id);
      return r;
    }
    inline bool contains(const std::string& id) const {
      return m_samples.find(id) != m_samples.end();
    }
    inline bool empty() const { return m_samples.empty(); }
  protected:
    std::map<std::string,ISample*> m_samples;
  };

  class SampleException : public Exception {
  public:
    SampleException(const std::string& msg):Exception(msg) {}
  };
}

#endif // sfi_ISample_h
