#ifndef sfi_DataSource_h
#define sfi_DataSource_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Options.h"

namespace sfi {
  /**
   * @class sfi::DataSource
   *
   * Identifies the source of the data used by a Sample.
   *
   * options:
   * name string      : Name of source
   * index unsigned   : Specifies order of samples
   * nevents unsigned : Requested number of events
   */
  class DataSource {
  public:
    inline const std::string& name() const { return m_name; }

    inline const std::string& variant() const { return m_variant; }

    inline unsigned index() const { return m_index; }

    inline OptionMap& options() { return m_src_opts; }

    inline const OptionMap& options() const { return m_src_opts; }

    inline unsigned nevents() const { return m_nevents; }

    inline DataSource& nevents(unsigned _nevents) { m_nevents = _nevents; return *this; }

    inline unsigned dims() const { return m_dims; }

    inline DataSource& dims(unsigned _dims) { m_dims = _dims; return *this; }

    virtual ~DataSource();

    virtual std::shared_ptr<DataSource> get_variant(const OptionMap& vopts, const std::string& variant) const = 0;

    virtual std::shared_ptr<DataSource> do_clone() const = 0;
  protected:
    DataSource(const OptionMap& src_opt);

    DataSource(const std::string& name, unsigned index = 0, unsigned nevents = 0, unsigned dims = 1);

    DataSource(const DataSource& nom, const OptionMap& vopts, const std::string& variant);

    void bind_options();

    OptionMap m_src_opts;
    std::string m_name;
    std::string m_variant;
    unsigned m_index;
    unsigned m_nevents;
    unsigned m_dims;
  };

  class DefaultDataSource : public DataSource {
  public:
    DefaultDataSource(const OptionMap& src_opt);

    DefaultDataSource(const std::string& name, unsigned index = 0, unsigned nevents = 0, unsigned dims = 1);

    DefaultDataSource(const DataSource& nom, const OptionMap& vopts, const std::string& variant);

    virtual ~DefaultDataSource();

    virtual std::shared_ptr<DataSource> get_variant(const OptionMap& vopts, const std::string& variant) const;

    virtual std::shared_ptr<DataSource> do_clone() const;
  };
}

#endif // sfi_DataSource_h
