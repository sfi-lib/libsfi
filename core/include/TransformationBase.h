#ifndef sfi_TransformationBase_h
#define sfi_TransformationBase_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string>

#include "Transform.h"
#include "Sample.h"
#include "Options.h"
#include "Context.h"

namespace sfi {
  class TransformationBase {
  public:
    inline const std::string& name() const { return m_name; }

    // ---- Exported interface ----

    inline bool normalize() const { return m_normalize; }
    inline TransformationBase& normalize(bool b) { m_normalize = b; return *this; }

    inline bool compute_covariance() const { return m_compute_covariance; }
    inline TransformationBase& compute_covariance(bool b) { m_compute_covariance = b; return *this; }

    inline bool compute_uncertainty() const { return m_compute_uncertainty; }
    inline TransformationBase& compute_uncertainty(bool b) { m_compute_uncertainty = b; return *this; }

    inline bool explicit_post_processing() const { return m_explicit_post_processing; }
    inline TransformationBase& explicit_post_processing(bool b) { m_explicit_post_processing = b; return *this; }

    inline double prune_significance() const { return m_prune_significance; }
    inline TransformationBase& prune_significance(double b) { m_prune_significance = b; return *this; }

    inline bool debug() const { return this->m_log.debug(); }
    inline bool verbose() const { return this->m_log.verbose(); }
    inline bool warning() const { return this->m_log.warning(); }
    inline bool error() const { return this->m_log.error(); }

    inline TransformationBase& print_level(PrintLevel pl) { this->m_log.print_level(pl); return *this; }

    inline OptionMap& options() { return this->m_options; }
    inline const OptionMap& options() const { return this->m_options; }

    using transform_result = bool;

    inline std::ostream& print(std::ostream& out) const {
      out<<this->name()<<std::endl;
      return out<<this->options();
    }
  protected:
    TransformationBase(Context& ctx, const std::string& name);

    TransformationBase(Context& ctx, const OptionMap& opts);

    TransformationBase(const TransformationBase& o);

    void bind_options();

    template<class _Transf>
    bool
    init_transform(_Transf& trf, const std::string& name, const PVariables& vars) const {
      trf.name(name);
      trf.compute_covariance(this->m_compute_covariance).compute_uncertainty(this->m_compute_uncertainty).is_amplitude(false);
      if(vars) trf.variables(vars);
      if(!trf.init_params()) trf.clear();
      if(!trf.nparams()) {
        do_log_error("init_transform() no params for Transform of type '"<<trf.type_name()<<"'");
        return false;
      }
      do_log_verbose("init_transform() name="<<trf.name()<<" type_name="<<type_name<_Transf>()<<" nparams="<<trf.nparams()<<" npars="<<trf.npar());
      return true;
    }

    template<class _Transf, unsigned _NDims, class _SplUseWeight = tfalse>
    bool
    init_transform(_Transf& trf, const Sample<_NDims, _SplUseWeight>* spl) const {
      if(spl) return init_transform(trf, this->name() + "_" + spl->name(), spl->variables());
      else return init_transform(trf, this->name(), 0);
      trf.set_sample(spl);
    }

    std::string m_name;
    bool m_normalize;
    bool m_compute_covariance;
    bool m_compute_uncertainty;
    bool m_explicit_post_processing;
    double m_prune_significance;
    Log m_log;
    OptionMap m_options;
  };


  // Determine Transform type
  // TODO: Something better for derived Transforms (such as ModelTransform)
  template<class _Eigen, class _TType>
  struct _transform_type : tvalued<Transform<_Eigen, _TType>> { };

  template<class _T1, class _T2>
  struct _transform_type<Transform<_T1, _T2>, void> : tvalued<Transform<_T1, _T2>> { };

  template<class _T1A, class _T1B, class _T2A, class _T2B>
  struct _transform_type<Transform<_T1A, _T1B>, Transform<_T2A, _T2B>> : tvalued<Transform<Transform<_T1A, _T1B>, Transform<_T2A, _T2B>>> { };

  template<class _EigLeft, class _EigRight, class _TType>
  struct _transform_type<BiEigenVectors<_EigLeft, _EigRight>, _TType> :
  tvalued<Transform<Transform<_EigLeft, _TType>, Transform<_EigRight, _TType>>> { };

  template<class _T1, class _T2, bool _is_dynamic = false>
  class TransformationEigenBase : public TransformationBase {
  public:
    using Transf = tvalue<_transform_type<_T1, _T2>>;
    using Eigenv = t_EigenVectors<Transf>;

    static constexpr inline unsigned dims() { return Transf::dims(); }
    
    /** @return New Transform instance */
    inline Transf* create_transform(Context&) const { return new Transf("T{"+this->name()+"}"); }

    /** @return Axis transform instance */
    inline TransformVariableBase* create_transform_1D(Context&, unsigned d) const {
      return TransformImpl<Transf, t_EigenSystem<Transf>>::create_transform_1d(d, "T{"+this->name()+"}:"+std::to_string(d), t_EigenSystem<Transf>(eigenvectors()));
    }

    inline Eigenv eigenvectors() const { return Eigenv(); }
  protected:
    TransformationEigenBase(Context& ctx, const std::string& name):TransformationBase(ctx, name) { }

    TransformationEigenBase(Context& ctx, const OptionMap& opts):TransformationBase(ctx, opts) { }

    TransformationEigenBase(const TransformationEigenBase& o):TransformationBase(o) { }
  };

  template<class _T1, class _T2>
  class TransformationEigenBase<_T1, _T2, true> : public TransformationBase {
  public:
    using This = TransformationEigenBase<_T1, _T2, true>;
    using Base = TransformationBase;
    using Transf = tvalue<_transform_type<_T1, _T2>>;
    using Eigenv = t_EigenVectors<Transf>;

    ~TransformationEigenBase() {
      if(m_eigenvectors) {
        delete m_eigenvectors;
        m_eigenvectors = 0;
      }
    }

    static constexpr inline unsigned dims() { return Transf::dims(); }
    
    /** @return New Transform instance */
    inline Transf* create_transform(Context&) const { return new Transf(This::eigenvectors(), "T{"+this->name()+"}"); }

    /** @return Axis transform instance */
    inline TransformVariableBase* create_transform_1D(Context&, unsigned d) const {
      return TransformImpl<Transf, t_EigenSystem<Transf>>::create_transform_1d(d, "T{"+this->name()+"}:"+std::to_string(d), t_EigenSystem<Transf>(eigenvectors()));
    }

    inline Eigenv eigenvectors() const { return m_eigenvectors ? *m_eigenvectors : Eigenv(this->options().sub("eigen")); }
  protected:
    TransformationEigenBase(Context& ctx, const std::string& name):Base(ctx, name), m_eigenvectors(0) { }

    TransformationEigenBase(Context& ctx, const std::string& name, const Eigenv& eig):Base(ctx, name), m_eigenvectors(new Eigenv(eig)) { }

    TransformationEigenBase(Context& ctx, const OptionMap& opts):Base(ctx, opts), m_eigenvectors(0) { }

    TransformationEigenBase(const This& o):Base(o), m_eigenvectors(o.m_eigenvectors ? new Eigenv(*o.m_eigenvectors) : 0) { }

    Eigenv* m_eigenvectors;
  };
}

#endif // sfi_TransformationBase_h
