#ifndef sfi_ParamIterator_h
#define sfi_ParamIterator_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ITransform.h"
#include "EigenVectors.h"

namespace sfi {
  template<class _Transf>
  class ParamIteratorBase : public IParamIterator {
  public:
    ParamIteratorBase(const t_EigenVectorsIterator<_Transf>& it):m_iter(it) { }

    virtual ~ParamIteratorBase() { }

    // ---- implementation of IParamIterator ----

    virtual bool do_next() { return this->next(); }

    virtual unsigned get_degree(unsigned d) {
      if(d >= _Transf::dims()) throw TransformException("Invalid dimension "+std::to_string(d));
      return m_iter[d];
    }

    // ---- public methods ----

    inline bool next() { return m_iter.next(); }

    inline const t_EigenVectorsIndices<_Transf>& eigen_indices() const { return m_iter.indices(); }

    inline unsigned degree(unsigned d) const { return m_iter[d]; }

    inline unsigned index() const { return m_iter.eigen_idx(); }
  protected:
    t_EigenVectorsIterator<_Transf> m_iter;
  };

  template<class _Transf>
  class ParamIteratorIndexBase : public ParamIteratorBase<_Transf> {
  public:
    ParamIteratorIndexBase(const t_EigenVectorsIterator<_Transf>& it):ParamIteratorBase<_Transf>(it) { }

    virtual ~ParamIteratorIndexBase() { }

    // ---- public methods ----

    inline const t_EigenVectorsData<_Transf>& indices() const { return this->m_iter.indices().data(); }
  };

  template<class _Transf> class ParamIterator;

  template<class _Trf> using t_ParamIteratorPtr = std::unique_ptr<ParamIterator<_Trf>>;
}

#endif // sfi_ParamIterator_h
