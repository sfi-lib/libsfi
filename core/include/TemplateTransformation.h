#ifndef sfi_TemplateTransformation_h
#define sfi_TemplateTransformation_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Sample.h"
#include "CombinationTransformation.h"

namespace sfi {

  template<class _LTransf, class _RTransf>
  struct template_result {
    using CTransf = Transform<_LTransf, _RTransf>;
    template_result(t_EigenVectors<CTransf> ev):transform(ev, "", true), points(0,0), status(false) {}
    SampleSet<1> samples;
    CTransf transform;
    Matrix<double> points;
    TransformSet<_LTransf> point_transforms;
    bool status;
    inline operator bool() const { return this->status; }
  };

  /**
   * @class sfi::TemplateTransformation
   *
   * Combine transforms to a conditional PDF.
   *
   * - Options:
   * channel: string
   * source : string
   * variant: string
   * npoints1: int
   * npoints2: int
   *
   * TODO:
   * - No need to pass Sample if Transform points to its sample
   * - Extend to several dims
   * - shape template
   */
  template<class _CTransf>
  class TemplateTransformation : public CombinationTransformation<t_LeftTransform<_CTransf>, t_RightTransform<_CTransf>> {
  public:
    using This = TemplateTransformation<_CTransf>;
    using TransfLeft = t_LeftTransform<_CTransf>;
    using TransfRight = t_RightTransform<_CTransf>;
    using Base = CombinationTransformation<TransfLeft, TransfRight>;

    static constexpr unsigned NPars = TransfRight::dims();

    TemplateTransformation(Context& ctx, const OptionMap& opts):Base(ctx, opts) {
      this->m_options.get_if("channel", m_channel).get_if("source", m_source).get_if("variant", m_variant);
    }

    auto transform(TransformSet<TransfLeft>& trfs, SampleSet<1>& spls) {
      const unsigned NDims = 1;
      std::string prefix = m_channel+"_"+m_source+"_"+m_variant;
      int npts1(this->m_options.template get_as<int>("npoints1"));
      int npts2((NPars > 1) ? this->m_options.template get_as<int>("npoints2") : 1);
      auto& vars = this->variables();
      do_log_info("transform() tag : "<<prefix<<" name: "<<this->name()<<" npts1: "<<npts1<<" npts2: "<<npts2<<" varables: "<<vars);
      for(unsigned p=0;p<NPars; ++p) vars.at(NDims + p).lock();

      template_result<TransfLeft, TransfRight> res(this->eigenvectors());
      res.points.size(npts1*npts2, NPars);

      unsigned r=0;
      for(auto& spl : spls) {
        if(r > res.points.rows()) {
          do_log_error("transform() too many transforms tag: "<<prefix);
          break;
        }
        else if(starts_with(spl.name(), prefix)) {
          auto* trf = transform_by_sample(trfs, spl);
          // Make sure the samples are not owned by the set
          if(r == 0) {
            res.samples = SampleSet<1>({&spl}, false);
            res.point_transforms = TransformSet<TransfLeft>({trf}, false);
          }
          else {
            res.samples.add(&spl);
            res.point_transforms.add(trf);
          }

          auto src = spl.data_source();
          do_log_info("transform() r: "<<r<<" spl: "<<spl.name()<<" trf name: "<<trf->name()<<" : ");
          for(unsigned p=0;p<NPars; ++p) {
            res.points(r,p) = src->options().get<double>(vars[NDims + p].name());
            do_log_info("transform() '"<<vars[NDims + p].name()<<"' : "<<res.points(r,p));
          }
          ++r;
        }
      }
      if(this->combine(res.point_transforms, res.points, res.transform)) {
        do_log_info("transform() result: '"<<res.transform.name()<<"' variables: "<<*res.transform.variables());
        res.status = true;
      }
      else {
        do_log_error("transform() failed!");
        res.status = false;
      }
      return res;
    }
  protected:
    std::string m_channel, m_source, m_variant;
  };
}

#endif // sfi_TemplateTransformation_h
