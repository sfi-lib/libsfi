#ifndef sfi_Evaluator_h
#define sfi_Evaluator_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "trans_tmp.h"
#include "ITransform.h"

namespace sfi {
  /**
   * @class sfi::Evaluator<T>
   *
   * The parameter T is a Transform.
   *
   * This class speeds up the evaluation of a Transform at multiple points.
   */
  template<class _Trans>
  class Evaluator : public IEvaluator {
  public:
    using EvalData = t_EvalData<_Trans>;

    Evaluator(const _Trans* _me, double scale, bool external_coordinates):m_me(_me), m_scale(scale),
        m_external_coordinates(external_coordinates) {
      m_me->init_eval(m_p);
    }

    Evaluator(Evaluator&& o):m_p(std::forward<EvalData>(o.m_p)),
        m_me(o.m_me), m_scale(o.m_scale),
        m_external_coordinates(o.m_external_coordinates) {
    }

    Evaluator(const Evaluator& o):m_p(),
        m_me(o.m_me), m_scale(o.m_scale),
        m_external_coordinates(o.m_external_coordinates) {
      m_me->init_eval(m_p);
    }

    Evaluator operator=(const Evaluator&) = delete;

    virtual ~Evaluator() { }

    // ---- implementation of IEvaluator ----

    virtual double get_value(const double* x) { return (*this)(x); }

    virtual const ITransform* get_transform() { return m_me; }

    virtual void set_scale(double scale) { m_scale = scale; }

    virtual void set_external_coordinates(bool ec) { m_external_coordinates = ec; }

    virtual unsigned get_dimensions() const { return _Trans::dims(); }

    virtual bool do_eval(unsigned npts, const double* xv, double* res) {
      return m_me->eval(m_p, npts, xv, res, m_scale, m_external_coordinates);
    }

    virtual const Variable& get_variable(unsigned d) const {
      if(d >= _Trans::dims()) throw TransformException("Invalid dimension "+std::to_string(d));
      return m_me->variables()->at(d);
    }

    virtual IEvaluator* get_clone(bool deep) const {
      return new Evaluator<_Trans>(deep ? m_me->clone() : m_me, m_scale, m_external_coordinates);
    }

    virtual unsigned get_nslice_parameters() const { return 0; }

    virtual void set_slice(const double *) { }

    virtual const ITransform* get_slice_transform() { return m_me; }

    // ---- public methods ----

    inline double integral() const { return m_me->integral_pdf(); }

    inline Evaluator<_Trans>& scale(double scale) { m_scale = scale; return *this; }

    inline Evaluator<_Trans>& external_coordinates(bool external_coordinates) {
      m_external_coordinates = external_coordinates; return *this;
    }

    inline const _Trans* transform() { return m_me; }

    inline double operator()(const double* x) {
      return m_scale*m_me->eval(x, m_p, m_external_coordinates);
    }

    inline const EvalData& eval_data() const { return m_p; }

    template<class _InMatrix, class _OutMatrix>
    void eval_all(const _InMatrix& in, _OutMatrix& out, size_t out_idx) {
      size_t N(in.size());
      for(size_t i=0; i<N; ++i) out[i][out_idx] = (*this)(in[i].data());
    }
  protected:
    EvalData m_p;
    const _Trans* m_me;
    double m_scale;
    bool m_external_coordinates;
  };

  template<class _Transf> struct _t_transform<Evaluator<_Transf>> : tvalued<_Transf> {};

  template<class _Trf> using t_EvaluatorPtr = std::unique_ptr<Evaluator<_Trf>>;
}

#endif // sfi_Evaluator_h
