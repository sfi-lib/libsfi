#ifndef sfi_EigenVectors_h
#define sfi_EigenVectors_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <inttypes.h>
#include <array>

#include "sfi_math.h"
#include "util.h"
#include "Options.h"
#include "trans_tmp.h"

namespace sfi {
  template<unsigned _Dims, unsigned _Deg, class _Type>
  struct _eigen_npars {
    static inline constexpr size_t value() { return 0; }
    static inline constexpr size_t value(unsigned) { return 0; }
  };

  template<class _Type>
  struct t_eigenvector_type_name {
    static const std::string& apply() {
      static const std::string _s = "unknonw";
      return _s;
    }
  };

  template<unsigned _Dims, unsigned _Deg, class _Type>
  class EigenVectorsData {
  public:
    using Type = _Type;

    EigenVectorsData() { }

    EigenVectorsData(const OptionMap&) { }

    EigenVectorsData(unsigned /*d*/) {
      assert(false && "EigenVectorsData() must not be called with degree");
    }

    static inline constexpr unsigned dims() { return _Dims; }

    static inline constexpr unsigned degree() { return _Deg; }

    static inline constexpr bool is_dynamic() { return false; }

    static inline constexpr size_t params() { return c_nparams; }
  protected:
    static constexpr const size_t c_nparams = _eigen_npars<_Dims, _Deg, _Type>::value();
  };

  template<unsigned _Dims, class _Type>
  class EigenVectorsData<_Dims, 0U, _Type> {
  public:
    using Type = _Type;

    EigenVectorsData():m_degree(0), m_params(0) {
      assert(false && "EigenVectorsData() must be called with degree");
    }

    EigenVectorsData(const OptionMap& opts):m_degree(opts.get_as<unsigned>("degree")), m_params(_eigen_npars<_Dims, 0U, _Type>::value(m_degree)) { }

    EigenVectorsData(unsigned d):m_degree(d), m_params(_eigen_npars<_Dims, 0U, _Type>::value(d)) {}

    static inline constexpr unsigned dims() { return _Dims; }

    inline unsigned degree() const { return m_degree; }

    static inline constexpr bool is_dynamic() { return true; }

    inline size_t params() const { return m_params; }
  protected:
    unsigned m_degree;
    size_t m_params;
  };

  /**
   * @class EigenIndices
   * To be implemented for each eigen vectors type.
   * Basically it provides functions for iterating through the eigen vectors.
   * Should intially have the 0:th value.
   *
   * The interface to be exported is:
   * void clear() - Reset iterator
   * bool next() - Advance iterator, post access
   * operator[] - Access elements
   * size_t index() - Compute global index from individual indices
   * void set(i,value) - Set value of index i
   */
  template<unsigned _Dims, unsigned _Deg, class _Type, class _IdxType = uint8_t>
  struct EigenIndices { };

  /**
   * @class EigenIndicesBase
   * Base class for EigenIndices.
   */
  template<unsigned _Dims, unsigned _Deg, class _Type, class _IdxType>
  struct EigenIndicesBase : public EigenVectorsData<_Dims, _Deg, _Type> {
    using IndexType = _IdxType;
    using DataType = std::array<_IdxType, _Dims>;
    using This = EigenIndicesBase<_Dims, _Deg, _Type, _IdxType>;
    using Data = EigenVectorsData<_Dims, _Deg, _Type>;

    inline void clear() { this->m_indices.fill(0); }

    inline const DataType& data() const { return this->m_indices; }

    inline _IdxType operator[](size_t i) const {
      assert(i < this->dims() && "Invalid index");
      return this->m_indices[i];
    }

    inline bool next() { return false; }

    inline _IdxType max_idx(size_t) { return This::degree(); }

    inline void set(size_t i, _IdxType val) {
      assert(i < this->dims() && "Invalid index");
      assert(val <= this->max_idx(i) && "Invalid value");
      this->m_indices[i] = val;
    }

    inline uint64_t index() const { return 0; }

    This& operator=(const This& o) = delete;
  protected:
    EigenIndicesBase(const Data& da):Data(da) { m_indices.fill(0); }

    EigenIndicesBase(const EigenIndicesBase& o):Data(o), m_indices(o.m_indices) {}

    EigenIndicesBase(EigenIndicesBase&& o):Data(o), m_indices(o.m_indices) {}

    inline void set(const This& o) { m_indices = o.m_indices; }

    inline const _IdxType* indices_ptr() const { return m_indices.data(); }
    inline _IdxType* indices_ptr() { return m_indices.data(); }

    DataType m_indices;
  };

  template<unsigned _Dims, unsigned _Deg, class _Type, class _IdxType = uint8_t>
  inline std::ostream& operator<<(std::ostream& out, const EigenIndices<_Dims, _Deg, _Type, _IdxType>& idx) {
    for(unsigned i=0; i<idx.dims(); ++i) out<<(int)idx[i]<<" ";
    return out;
  }

  /**
   * @class EigenVectorsIterator
   *
   * Class for iterating through all eigen vectors. Can be used in one of two ways:
   * EigenVectorsIterator it;
   *  do {
   *  } while(it.next());
   * or:
   * EigenVectorsIterator begin, end;
   *  for(; begin != end; ++begin) {
   *  }
   */
  template<unsigned _Dims, unsigned _Deg, class _Type, class _IdxType = uint8_t>
  struct EigenVectorsIterator {
    using Indices = EigenIndices<_Dims, _Deg, _Type, _IdxType>;
    using This = EigenVectorsIterator<_Dims, _Deg, _Type, _IdxType>;
    using Data = EigenVectorsData<_Dims, _Deg, _Type>;

    EigenVectorsIterator(const Data& d):m_index(0), m_indices(d) {}

    EigenVectorsIterator(EigenVectorsIterator&& o):m_index(o.m_index), m_indices(std::forward<Indices>(o.m_indices)) {}

    EigenVectorsIterator(const EigenVectorsIterator& o):m_index(o.m_index), m_indices(o.m_indices) {}

    /** Should only be used for the end index */
    EigenVectorsIterator(const Data& d, unsigned index):m_index(index), m_indices(d) {}

    inline void clear() { this->m_index = 0; this->m_indices.clear(); }

    inline bool next() { ++this->m_index; return this->m_indices.next(); }

    inline const Indices& indices() const { return this->m_indices; }

    inline _IdxType operator[](size_t i) const { return this->m_indices[i]; }

    inline unsigned par_idx() const { return this->m_index; }

    inline unsigned eigen_idx() const { return this->m_index; }

    // ---- iterator interface ----

    inline This& operator*() { return *this; }

    inline This& operator++() { next(); return *this; }

    inline bool operator!=(const This& o) { return m_index != o.m_index; }
  protected:
    unsigned m_index;
    Indices m_indices;
  };

  /**
   * @class EigenVectors
   *
   * Define eigen vectors for a simple system, i.e. a homogenous eigen system or
   * a part of a complex ES. The parameter _Type is a tag class. The details
   * of the implementation is given by specializations of EigenIndices that define
   * how the collection of EiegenVector is enumberated and how to iterate over them.
   *
   * @param _Dims Number of dimensions
   * @param _Deg Number of degrees, if 0 then this is dynamic
   * @param _Type Type of eigen vectors
   * @param _IdxType Storage type, will determine the maximum allowed degree
   */
  template<unsigned _Dims, unsigned _Deg, class _Type, class _IdxType = uint8_t>
  class EigenVectors : public EigenVectorsData<_Dims, _Deg, _Type> {
  public:
    using Base = EigenVectorsData<_Dims, _Deg, _Type>;
    using This = EigenVectors<_Dims, _Deg, _Type, _IdxType>;
    using Iterator = EigenVectorsIterator<_Dims, _Deg, _Type, _IdxType>;
    using IndicesType = typename Iterator::Indices::DataType;
    using Indices = typename Iterator::Indices;
    using IndexType = _IdxType;

    EigenVectors():Base() {}

    EigenVectors(const OptionMap& opts):Base(opts) {}

    EigenVectors(unsigned _deg):Base(_deg) {}

    EigenVectors(unsigned _dim, unsigned _deg):Base(_dim, _deg) {}

    inline Iterator iterator() const { return Iterator(*this); }

    inline Indices indices() const { return Indices(*this); }

    inline Iterator begin() const { return Iterator(*this); }

    inline Iterator end() const { return Iterator(*this, this->params()); }

    inline void write(OptionMap& om) const {
      om.set("dims", _Dims);
      om.set("degree", this->degree());
      om.set("type", t_eigenvector_type_name<_Type>::apply());
    }
  };

  template<class _IdxLeft, class _IdxRight, bool _is_dynamic = false>
  struct BiEigenIndicesBase {

    static inline constexpr bool is_dynamic() { return false; }

    inline static constexpr size_t params() { return c_nparams; }

    inline const _IdxLeft& left() const { return m_ileft; }

    inline const _IdxRight& right() const { return m_iright; }
  protected:
    BiEigenIndicesBase(const _IdxLeft& ileft, const _IdxRight& iright):m_ileft(ileft), m_iright(iright) { }

    BiEigenIndicesBase(const BiEigenIndicesBase& o):m_ileft(o.m_ileft), m_iright(o.m_iright) {}

    static constexpr const size_t c_nparams = _IdxLeft::params() * _IdxRight::params();

    _IdxLeft m_ileft;
    _IdxRight m_iright;
  };

  template<class _IdxLeft, class _IdxRight>
  struct BiEigenIndicesBase<_IdxLeft, _IdxRight, true> {

    static inline constexpr bool is_dynamic() { return false; }

    inline size_t params() const { return m_nparams; }

    inline const _IdxLeft& left() const { return m_ileft; }

    inline const _IdxRight& right() const { return m_iright; }
  protected:
    BiEigenIndicesBase(const _IdxLeft& ileft, const _IdxRight& iright):m_ileft(ileft), m_iright(iright),
    m_nparams(this->m_ileft.params() * this->m_iright.params()) { }

    BiEigenIndicesBase(const BiEigenIndicesBase& o):m_ileft(o.m_ileft), m_iright(o.m_iright), m_nparams(o.m_nparams) {}

    _IdxLeft m_ileft;
    _IdxRight m_iright;
    size_t m_nparams;
  };

  /**
   * @class BiEigenIndices
   *
   * Implementation of the exported EigenIndices interface for composite eigen vectors.
   */
  template<class _IdxLeft, class _IdxRight>
  struct BiEigenIndices : BiEigenIndicesBase<_IdxLeft, _IdxRight, (_IdxLeft::is_dynamic() || _IdxRight::is_dynamic())> {
    using This = BiEigenIndices<_IdxLeft, _IdxRight>;
    using Base = BiEigenIndicesBase<_IdxLeft, _IdxRight, (_IdxLeft::is_dynamic() || _IdxRight::is_dynamic())>;
    using IndexType = typename _IdxLeft::IndexType;
    using DataType = This;

    BiEigenIndices(const _IdxLeft& ileft, const _IdxRight& iright):Base(ileft, iright) { }

    BiEigenIndices(const This& o):Base(o) {}

    static inline constexpr unsigned dims() { return c_dims; }

    inline void clear() { this->m_ileft.clear(); this->m_iright.clear(); }

    inline const DataType& data() const { return *this; }

    inline bool next() {
      return this->m_ileft.next() ? true : (this->m_ileft.clear(), this->m_iright.next());
    }

    inline IndexType operator[](size_t i) const {
      return (i < _IdxLeft::dims()) ? this->m_ileft[i] : this->m_iright[i - _IdxLeft::dims()];
    }

    /** Maximum allowed value in set */
    inline IndexType max_idx(size_t i) {
      return (i < _IdxLeft::dims()) ? this->m_ileft.max_idx(i) : this->m_iright.max_idx(i - _IdxLeft::dims());
    }

    inline void set(size_t i, IndexType val) {
      if(i < _IdxLeft::dims()) this->m_ileft.set(i, val);
      else this->m_iright.set(i - _IdxLeft::dims(), val);
    }

    inline This& operator=(const This& o) {
      if(this != &o) {
        this->m_ileft = o.m_ileft;
        this->m_iright = o.m_iright;
      }
      return *this;
    }

    inline uint64_t index() const {
      return this->m_ileft.index() + this->m_ileft.params() * this->m_iright.index();
    }

    inline std::ostream& print(std::ostream& out) const {
      return out<<this->m_ileft<<" "<<this->m_iright;
    }
  protected:
    static constexpr const unsigned c_dims = _IdxLeft::dims() + _IdxRight::dims();
  };

  template<class _IdxLeft, class _IdxRight>
  inline std::ostream& operator<<(std::ostream& out, const BiEigenIndices<_IdxLeft, _IdxRight>& idx) {
    return idx.print(out);
  }

  template<class _EigLeft, class _EigRight, bool _is_dynamic = false>
  class BiEigenVectorsBase {
  public:
    BiEigenVectorsBase(){}

    BiEigenVectorsBase(const _EigLeft& el, const _EigRight& er):m_left(el), m_right(er) {}

    BiEigenVectorsBase(const OptionMap&):m_left(), m_right() {}

    static inline constexpr bool is_dynamic() { return false; }

    static inline constexpr unsigned degree() { return c_degree; }

    static inline constexpr size_t params() { return c_nparams; }
  protected:
    static constexpr unsigned const c_degree = sfi::maxc(_EigLeft::degree(), _EigRight::degree());
    static constexpr const size_t c_nparams = _EigLeft::params() * _EigRight::params();

    _EigLeft m_left;
    _EigRight m_right;
  };

  template<class _EigLeft, class _EigRight>
  class BiEigenVectorsBase<_EigLeft, _EigRight, true> {
  public:
    BiEigenVectorsBase():m_params(0){}

    BiEigenVectorsBase(const _EigLeft& el, const _EigRight& er):m_left(el), m_right(er), m_params(el.params() * er.params()) {}

    BiEigenVectorsBase(const OptionMap& opts):m_left(_EigLeft(opts.sub("left"))), m_right(_EigRight(opts.sub("right"))),
        m_params(m_left.params() * m_right.params()) {}

    static inline constexpr bool is_dynamic() { return true; }

    inline unsigned degree() const { return std::max(m_left.degree(), m_right.degree()); }

    inline size_t params() const { return m_params; }
  protected:
    _EigLeft m_left;
    _EigRight m_right;
    size_t m_params;
  };

  /**
   * @class BiEigenVectors
   *
   * Implementation of the EigenVector exported interface for composite eigen vectors.
   */
  template<class _EigLeft, class _EigRight>
  class BiEigenVectors : public BiEigenVectorsBase<_EigLeft, _EigRight, (_EigLeft::is_dynamic() || _EigRight::is_dynamic())> {
    using Base = BiEigenVectorsBase<_EigLeft, _EigRight, (_EigLeft::is_dynamic() || _EigRight::is_dynamic())>;
  public:
    using EigenvLeft = _EigLeft;
    using EigenvRight = _EigRight;

    static_assert(teq<typename _EigLeft::IndexType, typename _EigRight::IndexType>::value() == true, "Index types must match");

    struct Iterator {
      using IdxType = typename _EigLeft::IndexType;
      using Indices = BiEigenIndices<typename _EigLeft::Iterator::Indices, typename _EigRight::Iterator::Indices>;
      using This = Iterator;

      Iterator(const Indices& idx):m_index(0), m_indices(idx) {}

      Iterator(const Indices& idc, unsigned idx):m_index(idx), m_indices(idc) {}

      inline void clear() { this->m_index = 0; this->m_indices.clear(); }

      inline bool next() { ++this->m_index; return this->m_indices.next(); }

      inline const Indices& indices() const { return this->m_indices; }

      inline IdxType operator[](size_t i) const { return this->m_indices[i];  }

      inline unsigned par_idx() const { return this->m_index; }

      inline unsigned eigen_idx() const { return this->m_index; }

      // ---- iterator interface ----

      inline This& operator*() { return *this; }

      inline This& operator++() { next(); return *this; }

      inline bool operator!=(const This& o) { return m_index != o.m_index; }
    protected:
      unsigned m_index;
      Indices m_indices;
    };

    using IndexType = typename _EigLeft::IndexType;
    using IndicesType = typename _EigLeft::IndicesType;
    using Indices = typename Iterator::Indices;

    static inline constexpr unsigned dims() { return _EigLeft::dims() + _EigRight::dims(); }

    inline Iterator iterator() const { return Iterator(indices()); }

    inline Indices indices() const { return Indices(this->m_left.indices(), this->m_right.indices()); }

    inline Iterator begin() const { return Iterator(indices()); }

    inline Iterator end() const { return Iterator(indices(), this->params()); }

    inline const _EigLeft& left() const { return this->m_left; }

    inline const _EigRight& right() const { return this->m_right; }

    BiEigenVectors():Base(){}

    BiEigenVectors(const _EigLeft& el, const _EigRight& er):Base(el, er) {}

    BiEigenVectors(const OptionMap& opts):Base(opts) {}

    inline void write(OptionMap& om) const {
      this->m_left.write(om.sub("left", true));
      this->m_right.write(om.sub("right", true));
    }
  };

  /** extract single eigenvector type */
  template<unsigned _Dims, unsigned _Deg, class _Type, class _IdxType>
  struct _t_eigenvector<EigenVectors<_Dims, _Deg, _Type, _IdxType>> :
  tvalued<typename EigenVectors<_Dims, _Deg, _Type, _IdxType>::Iterator> {};

  template<class _EigLeft, class _EigRight>
  struct _t_eigenvector<BiEigenVectors<_EigLeft, _EigRight>> :
  tvalued<typename BiEigenVectors<_EigLeft, _EigRight>::Iterator> {};

  /** extract eigenvectors type */
  template<unsigned _Dims, unsigned _Deg, class _Type, class _IdxType>
  struct _t_eigenvectors<EigenVectors<_Dims, _Deg, _Type, _IdxType>> :
  tvalued<EigenVectors<_Dims, _Deg, _Type, _IdxType>> {};

  template<class _EigLeft, class _EigRight>
  struct _t_eigenvectors<BiEigenVectors<_EigLeft, _EigRight>> :
  tvalued<BiEigenVectors<_EigLeft, _EigRight>> {};

  /** extract iterator type */
  template<unsigned _Dims, unsigned _Deg, class _Type, class _IdxType>
  struct _t_eigenvectors_iterator<EigenVectors<_Dims, _Deg, _Type, _IdxType>> :
  tvalued<typename EigenVectors<_Dims, _Deg, _Type, _IdxType>::Iterator> {};

  template<class _EigLeft, class _EigRight>
  struct _t_eigenvectors_iterator<BiEigenVectors<_EigLeft, _EigRight>> :
  tvalued<typename BiEigenVectors<_EigLeft, _EigRight>::Iterator> {};

  /** extract indices type */
  template<class _T>
  using t_EigenVectorsIndices = typename t_EigenVectors<_T>::Indices;

  /** extract data type indices */
  template<class _T>
  using t_EigenVectorsData = typename t_EigenVectorsIndices<_T>::DataType;

  /** extract single degree */
  template<class _Eigen, unsigned _Dim> struct _t_c_eigen_degree {};

  template<unsigned _Dims, unsigned _Deg, class _Type, class _IdxType, unsigned _Dim>
  struct _t_c_eigen_degree<EigenVectors<_Dims, _Deg, _Type, _IdxType>, _Dim> : tvalued<tuint<_Deg>> {};

  template<class _EigLeft, class _EigRight, unsigned _Dim>
  struct _t_c_eigen_degree<BiEigenVectors<_EigLeft, _EigRight>, _Dim> :
  tcond<(_Dim < _EigLeft::dims()), _t_c_eigen_degree<_EigLeft, _Dim>, _t_c_eigen_degree<_EigRight, _Dim - _EigLeft::dims()>> {};

  template<class _Eigen, unsigned _Dim>
  static constexpr inline unsigned c_eigen_degree() { return tvalue<_t_c_eigen_degree<_Eigen, _Dim>>::value(); }

  /** extract eigen type */
  template<class _Eigen, unsigned _Dim> struct _t_c_eigen_type : tvalued<void> {};

  template<unsigned _Dims, unsigned _Deg, class _Type, class _IdxType, unsigned _Dim>
  struct _t_c_eigen_type<EigenVectors<_Dims, _Deg, _Type, _IdxType>, _Dim> : tvalued<_Type> {};

  template<class _EigLeft, class _EigRight, unsigned _Dim>
  struct _t_c_eigen_type<BiEigenVectors<_EigLeft, _EigRight>, _Dim> :
  tcond<(_Dim < _EigLeft::dims()), _t_c_eigen_type<_EigLeft, _Dim>, _t_c_eigen_type<_EigRight, _Dim - _EigLeft::dims()>> {};

  template<class _T, unsigned _Dim>
  using t_EigenType = tvalue<_t_c_eigen_type<_T, _Dim>>;

  /**
   * create eigenvectors
   * @TODO: Remove/replace!
   */
  template<unsigned _Dims, typename _Eigen>
  struct _t_make_eigenvectors : tvalued<void> {};

  template<unsigned _TDims, unsigned _Dims, unsigned _Deg, class _Type, class _IdxType>
  struct _t_make_eigenvectors<_TDims, EigenVectors<_Dims, _Deg, _Type, _IdxType>> :
  public tvalued<EigenVectors<_TDims, _Deg, _Type, _IdxType>> { };

  template<unsigned _TDims, class _EigLeft, class _EigRight>
  struct _t_make_eigenvectors<_TDims, BiEigenVectors<_EigLeft, _EigRight>> :
  public tvalued<EigenVectors<_TDims, 0, t_EigenType<_EigLeft,0>, typename _EigLeft::IndexType>> { };

  /**
   * TODO: Replace
   */
  template<unsigned _Dims, typename _Eigen>
  using t_MakeEigenVectors = tvalue<_t_make_eigenvectors<_Dims, _Eigen>>;
}

#endif // sfi_EigenVectors_h
