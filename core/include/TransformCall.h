#ifndef sfi_TransformCall_h
#define sfi_TransformCall_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Sample.h"
#include "functions.h"

#include <type_traits>

/**
 * @class sfi::TransformCall
 *
 * Wraps the call to a transformations transform method.
 * Use the functions 'create_transform_call' to automtically wrap the arguments.
 *
 * - create_transform_call(const _Sample& spl, const _EventSel& esel)
 * - create_transform_call(const _Sample& spl, const _EventSel& esel, const _Func& fun)
 *
 * To transform a single variable:
 * - create_transform_call(unsigned var, const _Sample& spl, const _EventSel& esel)
 * - create_transform_call(unsigned var, const _Sample& spl, const _EventSel& esel, const _Func& fun)
 */
namespace sfi {
  template<class _Sample, class _EventSel>
  struct TransformCallBase {
    static_assert(t_is_sample<_Sample>::value(), "Invalid type, must be Sample<...>");

    using SampleTy = _Sample;
    using EventSelTy = _EventSel;

    TransformCallBase(const SampleTy& spl, const _EventSel& esel):m_spl(spl), m_esel(esel) { }

    /** @return reference to sample */
    inline const SampleTy& sample() const { return m_spl; }

    /** @return The event selector to use */
    inline const _EventSel& event_selector() const { return m_esel; }
    inline _EventSel& event_selector() { return m_esel; }
  protected:
    const SampleTy& m_spl;
    _EventSel m_esel;
  };

  template<class _Sample, class _SingleVar = ttrue, class _EventSel = EvenEventSel, class _OFunc = NoopFunc>
  struct TransformCall {};

  template<class _Sample, class _EventSel>
  inline TransformCall<_Sample, tfalse, _EventSel, NoopFunc>
  create_transform_call(const _Sample& spl, const _EventSel& esel) {
    return TransformCall<_Sample, tfalse, _EventSel, NoopFunc>(spl, esel);
  }

  template<class _Sample, class _EventSel>
  inline TransformCall<_Sample, ttrue, _EventSel, NoopFunc>
  create_transform_call(unsigned var, const _Sample& spl, const _EventSel& esel) {
    return TransformCall<_Sample, ttrue, _EventSel, NoopFunc>(var, spl, esel);
  }

  template<class _Sample, class _EventSel, class _Func>
  inline TransformCall<_Sample, tfalse, _EventSel, _Func>
  create_transform_call(const _Sample& spl, const _EventSel& esel, const _Func& fun) {
    return TransformCall<_Sample, tfalse, _EventSel, _Func>(spl, esel, fun);
  }

  template<class _Sample, class _EventSel, class _Func>
  inline TransformCall<_Sample, ttrue, _EventSel, _Func>
  create_transform_call(unsigned var, const _Sample& spl, const _EventSel& esel, const _Func& fun) {
    return TransformCall<_Sample, ttrue, _EventSel, _Func>(var, spl, esel, fun);
  }
  template<class _Sample, class _EventSel>
  inline TransformCall<_Sample, ttrue, _EventSel, NoopFunc>
  create_transform_call(unsigned var, const _Sample& spl, const _EventSel& esel, const NoopFunc&) {
    return TransformCall<_Sample, ttrue, _EventSel, NoopFunc>(var, spl, esel);
  }

  template<class ..._T>
  struct _t_sample<TransformCall<_T...>> : tvalued<typename TransformCall<_T...>::SampleTy> {};

  template<class ..._T>
  struct _t_sample<TransformCallBase<_T...>> : tvalued<typename TransformCallBase<_T...>::SampleTy> {};

  template<class _Sample, class _EventSel>
  struct TransformCall<_Sample, ttrue, _EventSel, NoopFunc> : TransformCallBase<_Sample, _EventSel> {
    using Base = TransformCallBase<_Sample, _EventSel>;
    using SampleTy = _Sample;

    TransformCall(unsigned var, const SampleTy& spl, const _EventSel& esel):
      Base(spl, esel), m_var(var) {
    }

    TransformCall(const TransformCall& tc, const SampleTy& spl):
      Base(spl, tc.event_selector()), m_var(tc.m_var) {
    }

    inline static constexpr bool single_variable() { return true; }

    inline static constexpr bool has_function() { return false; }

    inline unsigned variable() const { return m_var; }

    inline NoopFunc function() const { return NoopFunc(); }

    template<class _Func>
    TransformCall<_Sample, ttrue, _EventSel, RemapFunc<_Func>> add_function(const _Func& fun, teif<tremap_if_single<_Func>::value()>* = 0) const {
      return create_transform_call(m_var, this->m_spl, this->m_esel, RemapFunc<_Func>(fun, this->m_var));
    }
    template<class _Func>
    TransformCall<_Sample, ttrue, _EventSel, _Func> add_function(const _Func& fun, teif<!tremap_if_single<_Func>::value()>* = 0) const {
      return create_transform_call(m_var, this->m_spl, this->m_esel, fun);
    }

    template<class _Transfo, class _Transf1D>
    inline t_transform_result<_Transfo> transform(Context& ctx, const _Transfo& transfo, _Transf1D& res) const {
      return transfo.transform(ctx, *this, res);
    }

    template<class _OEventSel>
    TransformCall<_Sample, ttrue, _OEventSel, NoopFunc> change_event_selector(const _OEventSel& oesel) const {
      return TransformCall<_Sample, ttrue, _OEventSel, NoopFunc>(this->m_var, this->m_spl, oesel);
    }
  protected:
    unsigned m_var;
  };

  template<class _Sample, class _EventSel>
  struct TransformCall<_Sample, tfalse, _EventSel, NoopFunc> : TransformCallBase<_Sample, _EventSel> {
    using Base = TransformCallBase<_Sample, _EventSel>;
    using SampleTy = _Sample;

    TransformCall(const SampleTy& spl, const _EventSel& esel):Base(spl, esel) { }

    TransformCall(const TransformCall& tc, const SampleTy& spl):Base(spl, tc.event_selector()) { }

    inline static constexpr bool single_variable() { return false; }

    inline static constexpr bool has_function() { return false; }

    inline NoopFunc function() const { return NoopFunc(); }

    template<class _Func>
    TransformCall<_Sample, tfalse, _EventSel, _Func> add_function(const _Func& fun) const {
      return create_transform_call(this->m_spl, this->m_esel, fun);
    }

    template<class _Transfo>
    inline t_transform_result<_Transfo> transform(Context& ctx, const _Transfo& transfo, t_Transform<_Transfo>& res) const {
      return transfo.transform(ctx, *this, res);
    }

    template<class _OEventSel>
    TransformCall<_Sample, tfalse, _OEventSel, NoopFunc> change_event_selector(const _OEventSel& oesel) const {
      return TransformCall<_Sample, tfalse, _OEventSel, NoopFunc>(this->m_spl, oesel);
    }
  };

  template<class _Sample, class _EventSel, class _OFunc>
  struct TransformCall<_Sample, ttrue, _EventSel, _OFunc> : TransformCallBase<_Sample, _EventSel> {
    using Base = TransformCallBase<_Sample, _EventSel>;
    using SampleTy = _Sample;

    TransformCall(unsigned var, const SampleTy& spl, const _EventSel& esel, const _OFunc& func):
      Base(spl, esel), m_var(var), m_func(func) {
    }

    TransformCall(const TransformCall& tc, const SampleTy& spl):
      Base(spl, tc.event_selector()), m_var(tc.m_var), m_func(tc.m_func) {
    }

    inline static constexpr bool single_variable() { return true; }

    inline static constexpr bool has_function() { return true; }

    inline unsigned variable() const { return m_var; }

    inline const _OFunc& function() const { return m_func; }

    template<class _Func>
    TransformCall<_Sample, ttrue, _EventSel, CombinedFunc<tcond<tremap_if_single<_OFunc>::value(), RemapFunc<_OFunc>, _OFunc>, tcond<tremap_if_single<_Func>::value(), RemapFunc<_Func>, _Func>>>
    add_function(const _Func& fun) const {
      return create_transform_call(m_var, this->m_spl, this->m_esel, create_combined_func(create_single_var_func(this->m_func, this->m_var), create_single_var_func(fun, this->m_var)));
    }

    template<class _Transfo, class _Transf1D>
    inline t_transform_result<_Transfo> transform(Context& ctx, const _Transfo& transfo, _Transf1D& res) const {
      return transfo.transform(ctx, *this, res);
    }

    template<class _OEventSel>
    TransformCall<_Sample, ttrue, _OEventSel, _OFunc> change_event_selector(const _OEventSel& oesel) const {
      return TransformCall<_Sample, ttrue, _OEventSel, _OFunc>(this->m_var, this->m_spl, oesel, this->m_func);
    }
  protected:
    unsigned m_var;
    _OFunc m_func;
  };

  template<class _Sample, class _EventSel, class _OFunc>
  struct TransformCall<_Sample, tfalse, _EventSel, _OFunc> : TransformCallBase<_Sample, _EventSel> {
    using Base = TransformCallBase<_Sample, _EventSel>;
    using SampleTy = _Sample;

    TransformCall(const SampleTy& spl, const _EventSel& esel, const _OFunc& func):
      Base(spl, esel), m_func(func) {
    }

    TransformCall(const TransformCall& tc, const SampleTy& spl):
      Base(spl, tc.event_selector()), m_func(tc.m_func) {
    }

    inline static constexpr bool single_variable() { return false; }

    inline static constexpr bool has_function() { return true; }

    inline const _OFunc& function() const { return m_func; }

    template<class _Func>
    TransformCall<_Sample, tfalse, _EventSel, CombinedFunc<_OFunc, _Func>> add_function(const _Func& fun) const {
      return create_transform_call(this->m_spl, this->m_esel, create_combined_func(this->m_func, fun));
    }

    template<class _Transfo>
    inline t_transform_result<_Transfo> transform(Context& ctx, const _Transfo& transfo, t_Transform<_Transfo>& res) const {
      return transfo.transform(ctx, *this, res);
    }

    template<class _OEventSel>
    TransformCall<_Sample, tfalse, _OEventSel, _OFunc> change_event_selector(const _OEventSel& oesel) const {
      return TransformCall<_Sample, tfalse, _OEventSel, _OFunc>(this->m_spl, oesel, this->m_func);
    }
  protected:
    _OFunc m_func;
  };
}

#endif // sfi_TransformCall_h
