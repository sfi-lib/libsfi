#ifndef sfi_SliceEvaluator_h
#define sfi_SliceEvaluator_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ITransform.h"
#include "EigenVectors.h"

namespace sfi {
  // determine slice outer eigenvectors
  // TODO: Handle splitting??
  template<unsigned d2, class _Eigen>
  struct _t_slice_outer_eigenvectors : tvalued<void> {};

  template<unsigned d2, class _Eigen>
  using t_SliceOuterEigenVectors = tvalue<_t_slice_outer_eigenvectors<d2, _Eigen>>;

  template<unsigned d2, class _EigLeft, class _EigRight>
  struct _t_slice_outer_eigenvectors<d2, BiEigenVectors<_EigLeft, _EigRight>> :
  tvalued<
    tcond<(d2 + 1 == _EigLeft::dims()), _EigRight,
      tcond<(d2 + 1 < _EigLeft::dims()),
        BiEigenVectors<t_SliceOuterEigenVectors<d2, _EigLeft>, _EigRight>,
        t_SliceOuterEigenVectors<d2 - _EigLeft::dims(), _EigRight>
      >
    >
  > {};

  template<class _TargetEigenv, class _Eigenv>
  struct _slice_eigenvectors {
    template<unsigned d2>
    static inline const _TargetEigenv& value(const _Eigenv& eigenv, teif<_Eigenv::dims() == (d2+1)>* = 0) { return eigenv(); }
  };

  template<class _TargetEigenv, class _EigenLeft, class _EigenRight>
  struct _slice_eigenvectors<_TargetEigenv, BiEigenVectors<_EigenLeft, _EigenRight>> {
    template<unsigned d2>
    static inline const _TargetEigenv& value(const BiEigenVectors<_EigenLeft, _EigenRight>& eigenv, teif<_EigenLeft::dims() == (d2+1)>* = 0) {
      return eigenv.left();
    }
    template<unsigned d2>
    static inline const _TargetEigenv& value(const BiEigenVectors<_EigenLeft, _EigenRight>& eigenv, teif<(_EigenLeft::dims() > (d2+1))>* = 0) {
      return _slice_eigenvectors<_TargetEigenv, _EigenLeft>::template value<d2>(eigenv.left());
    }
  };

  template<class _TargetEigenv, class _Eigenv, unsigned d2>
  inline const _TargetEigenv&
  slice_eigenvectors(const _Eigenv& eigenv) {
    return _slice_eigenvectors<_TargetEigenv, _Eigenv>::template value<d2>(eigenv);
  }

  template<class _Eigenv>
  struct _slice_outer_eigenvectors { };

  template<class _EigenLeft, class _EigenRight>
  struct _slice_outer_eigenvectors<BiEigenVectors<_EigenLeft, _EigenRight>> {
    template<unsigned d2>
    static inline _EigenRight value(const BiEigenVectors<_EigenLeft, _EigenRight>& eigenv, teif<_EigenLeft::dims() == (d2+1)>* = 0) {
      return eigenv.right();
    }
    template<unsigned d2>
    static inline BiEigenVectors<t_SliceOuterEigenVectors<d2,_EigenLeft>, _EigenRight>
    value(const BiEigenVectors<_EigenLeft, _EigenRight>& eigenv, teif<(_EigenLeft::dims() > (d2+1))>* = 0) {
      using Eigen = BiEigenVectors<t_SliceOuterEigenVectors<d2,_EigenLeft>, _EigenRight>;
      return Eigen(_slice_outer_eigenvectors<_EigenLeft>::template value<d2>(eigenv.left()), eigenv.right());
    }
  };

  template<class _Eigenv, unsigned d2>
  inline t_SliceOuterEigenVectors<d2,_Eigenv>
  slice_outer_eigenvectors(const _Eigenv& eigenv) {
    return _slice_outer_eigenvectors<_Eigenv>::template value<d2>(eigenv);
  }

  template<class _Transf, unsigned d2>
  class SliceEvaluator : public IEvaluator {
  public:
    using Transf = _Transf;
    using ATransf = t_SliceTransform<_Transf, d2>;
    using SlEvalData = typename ATransf::EvalData;
    using This = SliceEvaluator<_Transf, d2>;

    SliceEvaluator(const _Transf* trf, bool _do_normalize, double scale, bool external_coordinates = false):
      m_transform(trf),
      m_slice(trf),
      m_slice_eval_data(1, SlEvalData()),
      m_scale(scale),
      m_do_normalize(_do_normalize),
      m_external_coordinates(external_coordinates) {
      m_transform->init_eval(m_p);
      m_slice.trans.init_eval(m_slice_eval_data[0]);
      m_slice.trans.is_amplitude(trf->is_amplitude());
      auto vars = std::make_shared<Variables>(ATransf::dims());
      for(unsigned i=0; i<ATransf::dims(); ++i) {
        vars->at(i) = trf->variables()->at(i);
      }
      m_slice.trans.variables(vars);
    }

    SliceEvaluator(const SliceEvaluator& o, const _Transf* trf):
      m_transform(trf), m_slice(o.m_slice), m_slice_eval_data(1, SlEvalData()),
      m_scale(o.m_scale), m_do_normalize(o.m_do_normalize),
      m_external_coordinates(o.m_external_coordinates) {
        m_slice.trans.init_eval(m_slice_eval_data[0]);
    }

    virtual ~SliceEvaluator() { }

    // ---- implementation of IEvaluator ----

    virtual double get_value(const double* x) { return (*this)(x); }

    virtual const ITransform* get_transform() { return m_transform; }

    virtual void set_scale(double scale) { m_scale = scale; }

    virtual void set_external_coordinates(bool external_coordinates) {
      m_external_coordinates = external_coordinates;
    }

    virtual unsigned get_dimensions() const { return d2 + 1; }

    virtual bool do_eval(unsigned npts, const double* xv, double* res) {
      return m_slice.trans.eval(m_slice_eval_data[0], npts, xv, res, m_scale, m_external_coordinates);
    }

    virtual const Variable& get_variable(unsigned d) const {
      return m_transform->variables()->at(d);
    }

    virtual IEvaluator* get_clone(bool deep) const {
      return new SliceEvaluator<_Transf, d2>(*this, deep ? m_transform->clone() : m_transform);
    }

    virtual unsigned get_nslice_parameters() const { return _Transf::dims() - (d2+1); }

    virtual void set_slice(const double *point) { this->slice(point); }

    virtual const ITransform* get_slice_transform() { return &m_slice.trans; }

    // ---- public methods ----

    inline double integral() const { return m_slice.trans.integral_pdf(); }

    inline This& scale(double scale) { m_scale = scale; return *this; }

    inline This& external_coordinates(bool external_coordinates) {
      m_external_coordinates = external_coordinates; return *this;
    }

    inline const _Transf* transform() { return m_transform; }

    inline ATransf& slice() { return m_slice.trans; }

    inline void slice(const double* point) {
      m_transform->template slice<d2>(point, m_slice.trans, m_p, m_do_normalize, m_external_coordinates);
    }

    inline double operator()(const double *x) {
      return m_scale*m_slice.trans.eval(x, m_slice_eval_data[0], m_external_coordinates);
    }

    void init_multi_processing(size_t n) {
      m_slice_eval_data.resize(n, SlEvalData());
      for(auto& ed : m_slice_eval_data) m_slice.trans.init_eval(ed);
    }

    inline double operator()(size_t N, const double *x) {
      return m_scale*m_slice.trans.eval(x, m_slice_eval_data[N], m_external_coordinates);
    }
  protected:
    template<class __Trans, bool is_dynamic = true>
    struct transf_wrapper {
      transf_wrapper(const _Transf* trf):trans(slice_eigenvectors<t_EigenVectors<ATransf>, t_EigenVectors<Transf>, d2>(trf->eigenvectors()), trf->name()+":"+std::to_string(d2)) {
      }
      __Trans trans;
    };
    template<class __Trans>
    struct transf_wrapper<__Trans, false> {
      transf_wrapper(const _Transf* trf):trans(trf->name()+":"+std::to_string(d2)) {
      }
      __Trans trans;
    };
    
    const _Transf* m_transform;
    transf_wrapper<ATransf, t_EigenVectors<ATransf>::is_dynamic()> m_slice;
    std::vector<SlEvalData> m_slice_eval_data;
    t_EvalData<_Transf> m_p;
    double m_scale;
    bool m_do_normalize;
    bool m_external_coordinates;
  };

  template<class _T, unsigned d2>
  struct _t_pstransform<SliceEvaluator<_T, d2>> : tvalued<typename SliceEvaluator<_T, d2>::ATransf> {};

  class ISliceDerivEvaluator : public IEvaluator {
  public:
    virtual ~ISliceDerivEvaluator() {}
    
    virtual void set_parno(unsigned) = 0;
    
    /** @return Evaluate the first derivative */
    virtual double do_eval_derivative(const double* x, double& d) = 0;
    
    /** @return Evaluate the second derivative */
    virtual double do_eval_derivative2(const double* x, double& dr, double& dr2) = 0;
    
    /** @return The derivative of the transform */
    virtual const ITransform* get_dslice_transform() = 0;
    
    /** @return The 2:nd derivative of the transform */
    virtual const ITransform* get_d2slice_transform() = 0;
  };

  template<class _Transf, unsigned d2>
  class SliceDerivEvaluator : public ISliceDerivEvaluator {
  public:
    using Transf = _Transf;
    using ATransf = t_SliceTransform<_Transf, d2>;
    using SlEvalData = typename ATransf::EvalData;
    using This = SliceDerivEvaluator<_Transf, d2>;

    SliceDerivEvaluator(const _Transf* trf, bool _do_normalize, double scale, bool external_coordinates = false):
      m_transform(trf),
      m_slice(trf), m_dslice(trf), m_d2slice(trf),
      m_slice_eval_data(),
      m_parnr(d2+1),
      m_scale(scale),
      m_do_normalize(_do_normalize),
      m_external_coordinates(external_coordinates) {
      m_transform->init_eval(m_p);
      m_transform->init_eval(m_dp);
      m_transform->init_eval(m_d2p);
      m_transform->init_eval(m_work);
      m_slice.trans.init_eval(m_slice_eval_data);
      assert(trf->is_amplitude() && "Must be amplitude");
      m_slice.trans.is_amplitude(true);
      auto vars = std::make_shared<Variables>(ATransf::dims());
      for(unsigned i=0; i<ATransf::dims(); ++i) vars->at(i) = trf->variables()->at(i);
      m_slice.trans.variables(vars);
    }

    virtual ~SliceDerivEvaluator() { }

    // ---- implementation of IEvaluator ----

    virtual double get_value(const double* x) { return (*this)(x); }

    virtual const ITransform* get_transform() { return m_transform; }

    virtual void set_scale(double scale) { m_scale = scale; }

    virtual void set_external_coordinates(bool external_coordinates) {
      m_external_coordinates = external_coordinates;
    }

    virtual unsigned get_dimensions() const { return d2 + 1; }

    virtual bool do_eval(unsigned npts, const double* xv, double* res) {
      return m_slice.trans.eval(m_slice_eval_data, npts, xv, res, m_scale, m_external_coordinates);
    }

    virtual const Variable& get_variable(unsigned d) const {
      return m_transform->variables()->at(d);
    }

    virtual IEvaluator* get_clone(bool) const { return nullptr; }

    virtual unsigned get_nslice_parameters() const { return _Transf::dims() - (d2+1); }

    virtual void set_slice(const double *point) { this->slice(point); }

    virtual const ITransform* get_slice_transform() { return &m_slice.trans; }

    // ---- implementation of ISliceDerivEvaluator ----
    
    virtual void set_parno(unsigned parno) { m_parnr = parno; }
    
    virtual double do_eval_derivative(const double* x, double& d) { return eval_derivative(x, d); }
    
    virtual double do_eval_derivative2(const double* x, double& dr, double& dr2) { return eval_derivative2(x,dr,dr2); }
    
    virtual const ITransform* get_dslice_transform() { return &m_dslice.trans; }
    
    virtual const ITransform* get_d2slice_transform() { return &m_d2slice.trans; }
    
    // ---- public methods ----

    inline double integral() const { return m_slice.trans.integral_pdf(); }

    inline This& scale(double scale) { m_scale = scale; return *this; }

    inline This& external_coordinates(bool external_coordinates) {
      m_external_coordinates = external_coordinates; return *this;
    }

    inline const _Transf* transform() { return m_transform; }

    inline ATransf& slice() { return m_slice.trans; }
    inline ATransf& dslice() { return m_dslice.trans; }
    inline ATransf& d2slice() { return m_d2slice.trans; }
    
    inline void slice(const double* point) {
      m_transform->template eval_deriv<d2>(point, m_p, m_dp, m_d2p, m_external_coordinates);
      m_transform->template slice_derivative<d2>(m_parnr,m_slice.trans, m_dslice.trans, m_d2slice.trans, m_p, m_dp, m_d2p, m_work, m_do_normalize, m_external_coordinates);
    }

    inline double operator()(const double *x) {
      return m_scale*m_slice.trans.eval(x, m_slice_eval_data, m_external_coordinates);
    }
    
    double eval_derivative(const double* x, double& d) {
      double a = (*this)(x);
      double da = m_scale*m_dslice.trans.eval(x, m_slice_eval_data, m_external_coordinates);
      d = 2.0*a*da;
      return a;
    }
    
    double eval_derivative2(const double* x, double& dr, double& dr2) {
      double a = (*this)(x);
      double da = m_scale*m_dslice.trans.eval(x, m_slice_eval_data, m_external_coordinates);
      dr = 2.0*a*da;
      double d2a = m_scale*m_d2slice.trans.eval(x, m_slice_eval_data, m_external_coordinates);
      dr2 = 2.0*da*da + 2.0*a*d2a;
      return a;
    }
  protected:
    template<class __Trans, bool is_dynamic = true>
    struct transf_wrapper {
      transf_wrapper(const _Transf* trf):trans(slice_eigenvectors<t_EigenVectors<ATransf>, t_EigenVectors<Transf>, d2>(trf->eigenvectors()), trf->name()+":"+std::to_string(d2)) {
      }
      __Trans trans;
    };
    template<class __Trans>
    struct transf_wrapper<__Trans, false> {
      transf_wrapper(const _Transf* trf):trans(trf->name()+":"+std::to_string(d2)) {
      }
      __Trans trans;
    };
    
    const _Transf* m_transform;
    transf_wrapper<ATransf, t_EigenVectors<ATransf>::is_dynamic()> m_slice, m_dslice, m_d2slice;
    SlEvalData m_slice_eval_data;
    t_EvalData<_Transf> m_p, m_dp, m_d2p, m_work;
    unsigned m_parnr;
    double m_scale;
    bool m_do_normalize;
    bool m_external_coordinates;
  };

  template<class _T, unsigned d2>
  struct _t_pstransform<SliceDerivEvaluator<_T, d2>> : tvalued<typename SliceDerivEvaluator<_T, d2>::ATransf> {};
}

#endif // sfi_SliceEvaluator_h
