#ifndef sfi_Sample_h
#define sfi_Sample_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <type_traits>
#include <limits>

#include "SampleBase.h"
#include "MemoryPool.h"
#include "functions.h"
#include "ReferenceSet.h"
#include "trans_tmp.h"

namespace sfi {
  template<unsigned _NDims, class _UseWeight = tfalse>
  struct _SplEvDataBase {
    using Data = std::array<double, _NDims>;
    _SplEvDataBase() { m_data.fill(0); }

    inline static constexpr double weight() { return 1.0; }
    inline void weight(double) { }
  protected:
    Data m_data;
  };

  template<unsigned _NDims>
  struct _SplEvDataBase<_NDims, ttrue> {
    using Data = std::array<double, _NDims>;

    _SplEvDataBase():m_weight(0.0) { m_data.fill(0); }

    inline double weight() const { return this->m_weight; }
    inline void weight(double w) { this->m_weight = w; }
  protected:
    Data m_data;
    double m_weight;
  };

  template<unsigned _NDims, class _UseWeight = tfalse>
  struct _SplEvData : public _SplEvDataBase<_NDims, _UseWeight> {
    using Data = std::array<double, _NDims>;

    _SplEvData(){ }

    inline double& operator[](unsigned d) {
      assert((d < _NDims) && "Invalid index");
      return this->m_data[d];
    }
    inline double operator[](unsigned d) const {
      assert((d < _NDims) && "Invalid index");
      return this->m_data[d];
    }

    inline const double* data() const { return this->m_data.data(); }
    inline double* data() { return this->m_data.data(); }
  };

  template<unsigned _NDims, class _UseWeight = tfalse>
  class Sample : public SampleBase {
  public:
    using Base = SampleBase;
    using This = Sample<_NDims, _UseWeight>;
    using EvData = _SplEvData<_NDims, _UseWeight>;
    using EvDataRef = EvData&;
    using EvDataConstRef = const EvData&;
    static_assert((std::is_standard_layout<EvData>::value), "EvData not standard");

    template<class> friend class PoolEntry;

    // ---- implementation of ISample ----

    virtual const std::string& get_type_name() const {
      static std::string _type_name = type_name<This>();
      return _type_name;
    }

    virtual bool get_use_weight() const { return _UseWeight::value(); }

    virtual unsigned get_dim() const { return _NDims; }

    virtual const double* get_x(unsigned ev) const { return this->x(ev); }
    virtual double get_x(unsigned ev, unsigned d) const { return this->x(ev, d); }
    virtual double& get_x(unsigned ev, unsigned d) { return this->x(ev, d); }

    virtual double get_w(unsigned ev) const { return this->w(ev); }
    virtual void set_w(unsigned ev, double _w) { this->w(ev, _w); }

    virtual void do_release() { this->release(); }

    using HistogramTy = Histogram<true, double>;

    virtual HistogramTy get_histogram_1D(size_t v, int nbins, bool external_coordinates = false, EventSelectorID esel = AllEventSelID) const;

    virtual std::vector<HistogramTy> get_histograms_1D(int nbins, bool external_coordinates = false, EventSelectorID esel = AllEventSelID) const;

    virtual Histogram2D<true,double> get_histogram_2D(unsigned v1, unsigned v2, unsigned nbins, bool external_coordinates = false, EventSelectorID esel = AllEventSelID) const;

    // ---- exported vector interface ----

    inline size_t size() const { return this->events(); }

    inline EvDataRef operator[](size_t i) { return this->data(i); }

    inline EvDataConstRef operator[](size_t i) const { return this->data(i); }

    // ---- public methods ----

    inline static constexpr bool use_weight() { return _UseWeight::value(); }

    template<class EventSel, typename... _Funcs>
    unsigned apply(const EventSel& esel, _Funcs... funcs) const;

    inline static constexpr unsigned dim() { return _NDims; }

    inline unsigned events() const { return Base::events(); }

    /** Set number of events, if larger than capacity, increase capacity */
    inline This& events(unsigned ev) { this->resize(ev); return *this; }

    inline const EvData& data(unsigned ev) const {
      assert(ev < events() && "Invalid event number");
      return *reinterpret_cast<const EvData*>(this->m_data.row_data(ev));
    }
    inline EvData& data(unsigned ev) {
      assert(ev < events() && "Invalid event number");
      return *reinterpret_cast<EvData*>(this->m_data.row_data(ev));
    }

    /** @return data at event */
    inline double* x(unsigned ev) { return this->m_data.row_data(ev); }
    inline const double* x(unsigned ev) const { return this->m_data.row_data(ev); }

    /** @return data at event and dimension */
    inline double x(unsigned ev, unsigned d) const { return this->m_data(ev,d); }
    inline double& x(unsigned ev, unsigned d) { return this->m_data(ev,d); }

    /** @return weight at event */
    inline double w(unsigned ev) const { return this->data(ev).weight(); }

    /** Set weight for event */
    inline This& w(unsigned ev, double _w) {
      assert(_UseWeight::value() && "setting weight on a Sample that holds no weights");
      this->data(ev).weight(_w);
      return *this;
    }

    /** Find min and maxval for each dimension */
    inline bool find_limits(Log& log, std::array<double, _NDims>& minv, std::array<double, _NDims>& maxv) {
      return Base::find_limits(log, minv.data(), maxv.data());
    }

    /** Fill 1D histogram for given variable */
    template<class _EventSel = AllEventSel>
    HistogramTy histogram_1D(size_t v, int nbins, bool external_coordinates = false, const _EventSel& esel = AllEventSel()) const;

    template<class _EventSel = AllEventSel>
    std::vector<HistogramTy> histograms_1D(int nbins, bool external_coordinates = false, const _EventSel& esel = AllEventSel()) const;

    template<class EvSel = OddEventSel, class Func = NoopFunc>
    Histogram2D<true,double> histogram_2D(unsigned v1, unsigned v2, unsigned nbins, bool external_coordinates = false, const EvSel& esel = OddEventSel(), const Func& func = NoopFunc()) const;

    template<class _Rnd>
    inline void bootstrap(Sample& target, _Rnd& rnd, bool do_poisson = false) const {
      bootstrap(target, rnd, AllEventSel(), do_poisson);
    }

    template<class _Rnd, class _EventSel>
    void bootstrap(Sample& target, _Rnd& rnd, const _EventSel& esel, bool do_poisson = false) const {
      bootstrap_subset(target, rnd, esel, (unsigned)(double(do_poisson ? rnd.poisson(this->events()) : this->events())*esel.fraction()));
    }

    template<class _Rnd, class _EventSel>
    void bootstrap_subset(Sample& target, _Rnd& rnd, const _EventSel& esel, unsigned nevents) const;

    template<class _EventSel>
    void select_events(Sample& target, const _EventSel& esel) const;

    /** Create a clone of this sample, must be released with a call to release() */
    Sample* clone(bool deep = false, const std::string& name = "", unsigned nevents = 0) const;

    inline void release() { m_pool->put(this); }

    inline PoolEntry<This>* pool() const { return m_pool; }
  protected:
    Sample(PoolEntry<This>* pool, unsigned nevents, const std::string& name, const PVariables& vars = nullptr):
      Base(nevents, event_size(), name, vars), m_pool(pool) {
    }

    Sample(PoolEntry<This>* pool, unsigned nevents, double*data, const std::string& name, const PVariables& vars = nullptr):
      Base(nevents, event_size(), data, name, vars), m_pool(pool) {
    }

    using Base::reinitialize;
    void reinitialize(unsigned nevents, const std::string& name, const PVariables& vars = nullptr) {
      Base::reinitialize(nevents, name, vars, event_size());
    }

    This& operator=(const This&) = delete;

    inline static constexpr unsigned event_size() { return (_NDims + _UseWeight::value()); }

    virtual ~Sample() {}

    PoolEntry<This>* m_pool;
  };

  template<unsigned _NDims, class _UseWeight>
  Histogram<true, double>
  Sample<_NDims, _UseWeight>::get_histogram_1D(size_t v, int nbins, bool external_coordinates, EventSelectorID esel) const {
    if(esel == AllEventSelID) return this->histogram_1D(v, nbins, external_coordinates, AllEventSel());
    else if(esel == EvenEventSelID) return this->histogram_1D(v, nbins, external_coordinates, EvenEventSel());
    else return this->histogram_1D(v, nbins, external_coordinates, OddEventSel());
  }

  template<unsigned _NDims, class _UseWeight>
  std::vector<Histogram<true, double>>
  Sample<_NDims, _UseWeight>::get_histograms_1D(int nbins, bool external_coordinates, EventSelectorID esel) const {
    if(esel == AllEventSelID) return this->histograms_1D(nbins, external_coordinates, AllEventSel());
    else if(esel == EvenEventSelID) return this->histograms_1D(nbins, external_coordinates, EvenEventSel());
    else return this->histograms_1D(nbins, external_coordinates, OddEventSel());
  }

  template<unsigned _NDims, class _UseWeight>
  Histogram2D<true,double>
  Sample<_NDims, _UseWeight>::get_histogram_2D(unsigned v1, unsigned v2, unsigned nbins, bool external_coordinates, EventSelectorID esel) const {
    if(esel == AllEventSelID) return this->histogram_2D(v1, v2, nbins, external_coordinates, AllEventSel());
    else if(esel == EvenEventSelID) return this->histogram_2D(v1, v2, nbins, external_coordinates, EvenEventSel());
    else return this->histogram_2D(v1, v2, nbins, external_coordinates, OddEventSel());
  }

  template<unsigned _NDims, class _UseWeight>
  template<class EventSel, typename... _Funcs>
  unsigned Sample<_NDims, _UseWeight>::apply(const EventSel& esel, _Funcs... funcs) const {
    unsigned nev(0);
    for(auto& ev : esel.select(*this)) {
      ++nev;
      bool __[sizeof...(_Funcs)] = {( funcs(ev.data(), ev.weight()), false)...};
      static_cast<void>(__);
    }
    return nev;
  }

  template<unsigned _NDims, class _UseWeight>
  template<class _EventSel>
  Histogram<true,double> Sample<_NDims, _UseWeight>::histogram_1D(size_t v, int nbins, bool external_coordinates, const _EventSel& esel) const {
    assert(this->m_vars && "Must have variables");
    assert((v < _NDims) && "Invalid variable");
    auto& var(this->m_vars->at(v));
    HistogramTy res(nbins, external_coordinates ? var.minval(): -1, external_coordinates ? var.maxval(): 1);
    if(use_weight()) {
      // int -> ext
      if(this->is_normalized() && external_coordinates) {
        for(auto& ev : esel.select(*this)) res.fill(var.vitrans(ev[v]), ev.weight());
      }
      // ext -> int
      else if(!this->is_normalized() && !external_coordinates) {
        for(auto& ev : esel.select(*this)) res.fill(var.vtrans(ev[v]), ev.weight());
      }
      else {
        for(auto& ev : esel.select(*this)) res.fill(ev[v], ev.weight());
      }
    }
    else {
      // int -> ext
      if(this->is_normalized() && external_coordinates) {
        for(auto& ev : esel.select(*this)) res.fill(var.vitrans(ev[v]));
      }
      // ext -> int
      else if(!this->is_normalized() && !external_coordinates) {
        for(auto& ev : esel.select(*this)) res.fill(var.vtrans(ev[v]));
      }
      else {
        for(auto& ev : esel.select(*this)) res.fill(ev[v]);
      }
    }
    return res;
  }

  template<unsigned _NDims, class _UseWeight>
  template<class _EventSel>
  std::vector<Histogram<true,double>>
  Sample<_NDims, _UseWeight>::histograms_1D(int nbins, bool external_coordinates, const _EventSel& esel) const {
    std::vector<HistogramTy> res;
    res.reserve(_NDims);
    for(unsigned d=0; d<_NDims; ++d) res.emplace_back(histogram_1D(d, nbins, external_coordinates, esel));
    return res;
  }

  template<unsigned _NDims, class _UseWeight>
  template<class EvSel, class Func>
  Histogram2D<true,double>
  Sample<_NDims, _UseWeight>::histogram_2D(unsigned v1, unsigned v2, unsigned nbins, bool external_coordinates, const EvSel& esel, const Func& func) const {
    assert(this->variables() != 0 && "No Variables");
    assert(this->variables()->nvariables() > 1 && "Too few variables");
    assert(_NDims > 1 && "Too few variables");
    assert((v1 < _NDims) && (v2 < _NDims) && "Invalid indices");
    assert((v1  != v2) && "Must be different variables");
    auto& var1 = this->m_vars->at(v1);
    auto& var2 = this->m_vars->at(v2);
    Histogram2D<true,double> res(nbins, external_coordinates ? var1.minval(): -1, external_coordinates ? var1.maxval(): 1, nbins, external_coordinates ? var2.minval(): -1, external_coordinates ? var2.maxval(): 1);
    if(use_weight()) {
      if(this->is_normalized() && external_coordinates) {
        for(auto& ev : esel.select(*this)) res.fill(var1.vitrans(ev[v1]), var2.vitrans(ev[v2]), func.eval(ev.data())*ev.weight());
      }
      else if(!this->is_normalized() && !external_coordinates) {
        for(auto& ev : esel.select(*this)) res.fill(var1.vtrans(ev[v1]), var2.vtrans(ev[v2]), func.eval(ev.data())*ev.weight());
      }
      else {
        for(auto& ev : esel.select(*this)) res.fill(ev[v1], ev[v2], func.eval(ev.data())*ev.weight());
      }
    }
    else {
      if(this->is_normalized() && external_coordinates) {
        for(auto& ev : esel.select(*this)) res.fill(var1.vitrans(ev[v1]), var2.vitrans(ev[v2]), func.eval(ev.data()));
      }
      else if(!this->is_normalized() && !external_coordinates) {
        for(auto& ev : esel.select(*this)) res.fill(var1.vtrans(ev[v1]), var2.vtrans(ev[v2]), func.eval(ev.data()));
      }
      else {
        for(auto& ev : esel.select(*this)) res.fill(ev[v1], ev[v2], func.eval(ev.data()));
      }
    }
    return res;
  }

  template<unsigned _NDims, class _UseWeight>
  template<class _Rnd, class _EventSel>
  void Sample<_NDims, _UseWeight>::bootstrap_subset(Sample& target, _Rnd& rnd, const _EventSel& esel, unsigned nevents) const {
    target.m_vars = this->m_vars;
    target.events(nevents);
    for(unsigned i=0; i<nevents; ) {
      unsigned n = (unsigned)rnd.uniform(0, this->events());
      if(esel(n)) {
        target.data(i) = this->data(n);
        ++i;
      }
    }
  }

  template<unsigned _NDims, class _UseWeight>
  template<class _EventSel>
  void Sample<_NDims, _UseWeight>::select_events(Sample& target, const _EventSel& esel) const {
    target.m_vars = this->m_vars;
    const unsigned nevents = unsigned(1.1*double(this->events())*esel.fraction());
    target.events(nevents);
    unsigned i=0;
    for(unsigned n=0; n<this->events(); ++n) {
      if(esel(n)) {
        target.data(i) = this->data(n);
        ++i;
      }
    }
    target.events(i);
  }

  template<unsigned _NDims, class _UseWeight>
  Sample<_NDims, _UseWeight>* Sample<_NDims, _UseWeight>::clone(bool deep, const std::string& name, unsigned nevents) const {
    Sample<_NDims, _UseWeight> *res = pool()->get(!nevents ? this->events() : nevents, name.empty() ? this->m_name : name);
    copy_to(deep, res, nevents);
    return res;
  }

  template<class _T> struct t_is_sample : public tfalse {};

  template<unsigned _dims, class _SplUseWeight>
  struct t_is_sample<Sample<_dims, _SplUseWeight>> : public ttrue {};

  template<unsigned _NDims, class _SplUseWeight = tfalse>
  class SampleSet : public ReferenceSet<Sample<_NDims, _SplUseWeight>, ISample> {
  public:
    using This = SampleSet<_NDims, _SplUseWeight>;
    using SampleTy = Sample<_NDims,_SplUseWeight>;
    using Base = ReferenceSet<SampleTy, ISample>;

    SampleSet():Base() {}

    SampleSet(size_t size):Base(size) { }

    SampleSet(const std::initializer_list<SampleTy*>& samples, bool owns_objects = true):Base(samples, owns_objects) { }

    SampleSet(const std::vector<ISample*>& samples, bool owns_objects = true):Base(samples, owns_objects) { }

    SampleSet(This&& o):Base(std::forward<This>(o)) {}

    /** Sort the samples in this set by the source meta data index. */
    inline void sort_by_index() {
      std::sort(this->m_objects.begin(), this->m_objects.end(), [](SampleTy *a, SampleTy* b) {
        return (a->data_source()) && (b->data_source()) ? (a->data_source()->index() < b->data_source()->index()) : 0;
      });
    }

    inline bool normalize_variables(Log& log) {
      return _samples_normalize(log, _NDims, std::vector<SampleBase*>(this->m_objects.begin(), this->m_objects.end()));
    }

    /** Clone all samples in the set */
    inline This clone(bool deep = false) const { return This(Base::clone(deep)); }

    template<class _EventSel = AllEventSel>
    std::vector<std::vector<Histogram<true,double>>> histograms_1D(int nbins, bool external_coordinates = false, const _EventSel& esel = AllEventSel()) const {
      std::vector<std::vector<Histogram<true,double>>> res;
      res.reserve(this->size());
      for(auto& spl : *this) res.emplace_back(spl.histograms_1D(nbins, external_coordinates, esel));
      return res;
    }

    This& operator=(This&& o) {
      if(&o != this) Base::operator =(std::move(o));
      return *this;
    }

    /** Look up sample by name */
    inline SampleTy* operator[](const std::string& name) const {
      for(auto* spl : this->m_objects) {
        if(spl->name() == name) return spl;
      }
      return 0;
    }
    inline SampleTy* operator[](const char* name) const { return (*this)[std::string(name)]; }

    inline SampleTy& operator[](size_t i) { return Base::operator [](i); }
    inline const SampleTy& operator[](size_t i) const { return Base::operator [](i); }
    inline SampleTy& operator[](int i) { return Base::operator [](i); }
    inline const SampleTy& operator[](int i) const { return Base::operator [](i); }
    inline SampleTy& operator[](unsigned int i) { return Base::operator [](i); }
    inline const SampleTy& operator[](unsigned int i) const { return Base::operator [](i); }
  protected:
    SampleSet(Base&& o):Base(std::forward<Base>(o)) {}
  };

  template<unsigned _NDims, class _SplUseWeight>
  struct _t_sampleset<Sample<_NDims, _SplUseWeight>> : tvalued<SampleSet<_NDims,_SplUseWeight>> { };

  template<unsigned _NDims, class _SplUseWeight>
  struct _t_sample<SampleSet<_NDims,_SplUseWeight>> : tvalued<Sample<_NDims,_SplUseWeight>> {};

  template<unsigned _NDims, class _SplUseWeight = tfalse>
  inline SampleSet<_NDims, _SplUseWeight>
  sample_set(const std::initializer_list<Sample<_NDims, _SplUseWeight>*>& samples) {
    return SampleSet<_NDims, _SplUseWeight>(samples);
  }

  template<unsigned _NDims, class _SplUseWeight = tfalse>
  inline SampleSet<_NDims, _SplUseWeight>
  sample_set(std::vector<Sample<_NDims, _SplUseWeight>*>&& samples) {
    return SampleSet<_NDims, _SplUseWeight>(std::move(samples));
  }
}

#endif // sfi_Sample_h
