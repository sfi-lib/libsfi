#ifndef sfi_ProjectionTransformation_h
#define sfi_ProjectionTransformation_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Transformation.h"
#include "Context.h"
#include "TransformCall.h"

#include <random>

namespace sfi {

  /**
   * @class sfi::ProjectionTransformation
   *
   * This Transformation produces a Transform given a sample
   * by projecting the event weight (or 1) upon the basis vectors.
   * Given a Transform with k eigen-vectors and a Sample with n events
   * the computational complexity is O(kn).
   *
   * Overtraining can be reduced by oversampling or pruning.
   * The full covariance matrix between coefficients may be
   * computed.
   *
   * options:
   * do_oversampling bool        : If oversampling should be performed
   * oversampling_smear_all bool : If the 0:th oversampling should be smeared
   * oversampling unsigned       : Number of oversamplings
   * oversampling_width          : Width of Normal distr used to smear
   */
  template<class _T1, class _T2 = void>
  class ProjectionTransformation : public Transformation<_T1, _T2> {
  public:
    using This = ProjectionTransformation<_T1, _T2>;
    using Base = Transformation<_T1, _T2>;
    using Transf = t_Transform<Base>;
    using Coeff = t_CoefficienctType<Transf>;
    using Eigenv = t_EigenVectors<Transf>;

    ProjectionTransformation(Context& ctx, const std::string& name):
      Base(ctx, "ProjectionTransformation{"+name+"}"),
      m_do_oversampling(false),
      m_oversampling_smear_all(false),
      m_oversampling(0),
      m_oversampling_width(0.0) {
      bind_options();
    }

    template<class _EigenV>
    ProjectionTransformation(Context& ctx, const std::string& name, const _EigenV& eigen, teif<teq<_EigenV, Eigenv>::value() && Eigenv::is_dynamic()>* = 0):
    Base(ctx, "ProjectionTransformation{"+name+"}", eigen),
    m_do_oversampling(false),
    m_oversampling_smear_all(false),
    m_oversampling(0),
    m_oversampling_width(0.0) {
      bind_options();
    }

    ProjectionTransformation(Context& ctx, const OptionMap& opts):
      Base(ctx, opts),
      m_do_oversampling(false),
      m_oversampling_smear_all(false),
      m_oversampling(0),
      m_oversampling_width(0.0) {
      bind_options();
    }

    ProjectionTransformation(const ProjectionTransformation& o):
      Base(o),
      m_do_oversampling(o.m_do_oversampling),
      m_oversampling_smear_all(o.m_oversampling_smear_all),
      m_oversampling(o.m_oversampling),
      m_oversampling_width(o.m_oversampling_width) {
      bind_options();
    }

    /** @return oversampling */
    inline unsigned oversampling() const { return m_oversampling; }

    /** @return If oversampling should be applied */
    inline bool do_oversampling() const { return m_do_oversampling; }

    /** @return If all data should be smeared, i.e. if the original point should be discarded. */
    inline bool oversampling_smear_all() const { return m_oversampling_smear_all; }

    /** Set oversampling */
    inline This& oversampling(unsigned os, double osw, bool smear_all = false) {
      m_do_oversampling = true;
      m_oversampling_smear_all = smear_all;
      m_oversampling = os;
      m_oversampling_width = osw;
      return *this;
    }

    /** @return oversampling */
    inline double oversampling_width() const { return m_oversampling_width; }

    // ---- exported Transformation interface ----

    struct transform_result {
      transform_result():status(false), events(0), sum_weights(0.0) {}
      transform_result(bool s, size_t e, double sumw = 0.0):status(s), events(e), sum_weights(sumw) {}
      bool status;
      size_t events;
      double sum_weights;
      inline operator bool() const { return this->status; }
    };

    template<class _Sample, class _EventSel, class _Func>
    transform_result
    transform(Context& ctx, const TransformCall<_Sample, tfalse, _EventSel, _Func>& ctrf, Transf& res) const {
      if(this->m_compute_covariance) return projection_transform_covariance(ctx, ctrf, res);
      else if(!m_do_oversampling) {
        if(this->compute_uncertainty()) return projection_transform<ttrue>(ctx, ctrf, res);
        else return projection_transform<tfalse>(ctx, ctrf, res);
      }
      else {
        if(this->compute_uncertainty()) return projection_transform_oversampling<ttrue>(ctx, ctrf, res);
        else return projection_transform_oversampling<tfalse>(ctx, ctrf, res);
      }
    }

    template<class _Sample, class _EventSel, class _Func, class _Transf1D>
    transform_result
    transform(Context& ctx, const TransformCall<_Sample, ttrue, _EventSel, _Func>& ctrf, _Transf1D& res) const {
      static_assert((c_is_1D_transform<Transf, _Transf1D>()), "Invalid 1D transform");
      if(!m_do_oversampling) {
        if(this->compute_uncertainty()) return projection_transform_1D<ttrue>(ctx, ctrf, res);
        else return projection_transform_1D<tfalse>(ctx, ctrf, res);
      }
      else {
        if(this->compute_uncertainty()) return projection_transform_oversampling_1D<ttrue>(ctx, ctrf, res);
        else return projection_transform_oversampling_1D<tfalse>(ctx, ctrf, res);
      }
    }

    template<class _Transf>
    inline transform_result post_process_transform(_Transf& trf, size_t events, double sum_weights) const {
      const unsigned npar(trf.npar());
      // TODO: Scale by sum_weights instead of events
      if(this->m_compute_covariance) {
        auto& cov = trf.covariance();
        cov *= double(events)/double(events - 1);
        if(this->compute_uncertainty()) {
          for(unsigned k=0; k<npar; ++k) trf.par_unc(k) = sqrt(cov(k,k));
        }
      }
      else if(m_do_oversampling) {
        if(this->compute_uncertainty()) {
          for(unsigned k=0; k<npar; ++k) {
            trf.par(k) = trf.par(k)/double(events);
            trf.par_unc(k) = std::real(sqrt((trf.par_unc(k)/double(events) - square(trf.par(k)))));
            trf.par(k) *= double(events)/double(m_oversampling);
            trf.par_unc(k) *= sqrt(double(events)/double(m_oversampling));
          }
          // TODO: Check this calc
          trf.par_unc(0) = std::real(sqrt(trf.integral_pdf())*trf.eigen_system().norm(0, 0));
        }
        else {
          for(unsigned k=0; k<npar; ++k) {
            trf.par(k) = trf.par(k)/double(events);
            trf.par(k) *= double(events)/double(m_oversampling);
          }
        }
      }
      else {
        if(this->compute_uncertainty()) {
          for(unsigned k=0; k<npar; ++k) {
            trf.par_unc(k) = std::real(sqrt((trf.par_unc(k)*double(events) - square(trf.par(k)))/double(events - 1)));
            if(k == 0) {
              const double sqrtn(sqrt(trf.integral_pdf()));
              // TODO: Check this calc
              trf.par_unc(k) = sqrtn*trf.eigen_system().norm(0, 0);
            }
          }
        }
      }
      return transform_result{Base::post_process_transform(trf), events, sum_weights};
    }

    template<class _Transf, unsigned _SplDims = 0, class _SplUseWeight = tfalse>
    inline transform_result merge(const Sample<_SplDims, _SplUseWeight>& sample, const std::vector<std::pair<_Transf, transform_result>>& parts, _Transf& res) const {
      if(!this->init_transform(res, &sample)) return transform_result{false, 0};
      size_t events(0);
      double sum_weights(0.0);
      bool all_ok(true);
      auto& par = res.parameters();
      if(this->m_compute_covariance) {
        CovarianceSummation covsum(res.covariance());
        for(auto& p : parts) {
          all_ok = all_ok && p.second.status;
          events += p.second.events;
          sum_weights += p.second.sum_weights;
          auto& par_n = p.first.parameters();
          par += par_n;
          // TODO: Use sum_weight when needed
          covsum.add_part(p.first.covariance(), p.first.parameters(), p.second.events);
        }
        // TODO: Use sum_weight when needed
        covsum.finalize(par, events);
      }
      else {
        if(this->compute_uncertainty()) {
          auto& unc = res.uncertainties();
          for(auto& p : parts) {
            all_ok = all_ok && p.second.status;
            events += p.second.events;
            sum_weights += p.second.sum_weights;
            par += p.first.parameters();
            unc += p.first.uncertainties();
          }
        }
        else {
          for(auto& p : parts) {
            all_ok = all_ok && p.second.status;
            events += p.second.events;
            sum_weights += p.second.sum_weights;
            par += p.first.parameters();
          }
        }
      }
      if(!all_ok) {
        do_log_error("merge() failed");
      }
      return post_process_transform(res, events, sum_weights);
    }

  protected:
    template<class _ComputeUnc, class ..._T, class _Transf1D>
    transform_result projection_transform_1D(Context&, const TransformCall<_T...>& call, _Transf1D& trf) const {
      using TCoeff = t_CoefficienctType<_Transf1D>;
      if(!this->init_transform(trf, &call.sample())) return transform_result{false,0};
      const constexpr bool use_weight = t_Sample<TransformCall<_T...>>::use_weight();
      const constexpr bool has_function = TransformCall<_T...>::has_function();
      const constexpr bool compute_uncertainties = teq<_ComputeUnc, ttrue>::value();
      const unsigned var = call.variable();

      t_EvalData<_Transf1D> p;
      trf.init_eval(p);

      size_t events(0);
      double sum_weights(0.0);
      double xx[1];
      TCoeff w(0.0);
      TCoeff *par_ptr = trf.parameters().data_ptr();
      double *par_unc_ptr = trf.uncertainties().data_ptr();
      for(auto& ev : call.event_selector().select(call.sample())) {
        if(use_weight) sum_weights += ev.weight();
        w = (use_weight ? ev.weight() : 1.0) * (!has_function ? 1.0 : call.function().eval(ev.data()));
        xx[0] = ev[var];
        if(compute_uncertainties) trf.template compute<Basis::Project, _project_unc<TCoeff>>(xx, p, _project_unc<TCoeff>(w, par_ptr, par_unc_ptr));
        else trf.template compute<Basis::Project, _project<TCoeff>>(xx, p, _project<TCoeff>(w, par_ptr));
        events++;
      }
      if(!use_weight) sum_weights = (double)events;

      do_log_debug("projection() var: "<<var<<" type: "<<trf.type_name()<<" events: "<<events<<" sum_weights: "<<sum_weights);
      if(!this->explicit_post_processing()) return post_process_transform(trf, events, sum_weights);
      else return transform_result{true, events, sum_weights};
    }

    template<class _Coeff>
    struct _project {
      _project(_Coeff _w, _Coeff*_ptr):w(_w), ptr(_ptr) {}

      inline void operator()(const _Coeff& v) {
        *ptr += w*v;
        ++ptr;
      }
      _Coeff w;
      _Coeff* ptr;
    };

    template<class _Coeff>
    struct _project_unc {
      _project_unc(_Coeff _w, _Coeff*_ptr, double*_unc_ptr):w(_w), ptr(_ptr), unc_ptr(_unc_ptr) {}

      inline void operator()(const _Coeff& v) {
        const _Coeff y = w*v;
        *ptr += y;
        ++ptr;
        *unc_ptr += std::real(y*conjugate(y));
        ++unc_ptr;
      }

      _Coeff w;
      _Coeff* ptr;
      double* unc_ptr;
    };

    template<class _ComputeUnc, class ..._T>
    transform_result projection_transform(Context&, const TransformCall<_T...>& call, Transf& trf) const {
      if(!this->init_transform(trf, &call.sample())) return transform_result{false,0};
      const constexpr bool use_weight = t_Sample<TransformCall<_T...>>::use_weight();
      const constexpr bool has_function = TransformCall<_T...>::has_function();
      const constexpr bool compute_uncertainties = teq<_ComputeUnc, ttrue>::value();

      t_EvalData<Transf> p;
      trf.init_eval(p);

      size_t events(0);
      double sum_weights(0.0);
      Coeff w(0.0);
      Coeff *par_ptr = trf.parameters().data_ptr();
      double *par_unc_ptr = trf.uncertainties().data_ptr();
      for(auto& ev : call.event_selector().select(call.sample())) {
        if(use_weight) sum_weights += ev.weight();
        w = (use_weight ? ev.weight() : 1.0) * (!has_function ?  1.0 : call.function().eval(ev.data()));
        if(compute_uncertainties) trf.template compute<Basis::Project, _project_unc<Coeff>>(ev.data(), p, _project_unc<Coeff>(w, par_ptr, par_unc_ptr));
        else trf.template compute<Basis::Project, _project<Coeff>>(ev.data(), p, _project<Coeff>(w, par_ptr));
        events++;
      }
      if(!use_weight) sum_weights = (double)events;

      do_log_debug("projection_transform() events: "<<events<<" sum_weights: "<<sum_weights);
      if(!this->explicit_post_processing()) return post_process_transform(trf, events, sum_weights);
      else return transform_result{true, events, sum_weights};
    }

    template<class ..._T>
    transform_result projection_transform_covariance(Context&, const TransformCall<_T...>& call, Transf& trf) const {
      if(!this->init_transform(trf, &call.sample())) return transform_result{false,0};
      auto& cov = trf.covariance();
      auto& par = trf.parameters();
      const constexpr bool use_weight = t_Sample<TransformCall<_T...>>::use_weight();
      const constexpr bool has_function = TransformCall<_T...>::has_function();

      t_EvalData<Transf> p;
      trf.init_eval(p);

      Vector<Coeff> data(trf.npar()), work(trf.npar());
      size_t events(0);
      double sum_weights(0.0);
      Coeff t(0.0), w(0.0);
      auto eit = trf.eigenvectors().iterator();
      for(auto& ev : call.event_selector().select(call.sample())) {
        trf.full_project(ev.data(), p);
        if(use_weight) sum_weights += ev.weight();
        w = (use_weight ? ev.weight() : 1.0)*(!has_function ? 1.0 : call.function().eval(ev.data()));
        eit.clear();
        do {
          t = w;
          for (unsigned int j=0; j<trf.dims(); ++j) t *= p[j][eit[j]];
          data[eit.par_idx()] = t;
        } while(eit.next());
        // TODO: Include weight
        update_online_covariance(data, events, cov, par, work);
      }
      if(!use_weight) sum_weights = (double)events;
      // TODO: Scale by sum_weights instead of events
      par *= (double)events;
      do_log_debug("projection_transform_covariance() events: "<<events<<" sum_weights: "<<sum_weights);
      if(!this->explicit_post_processing()) return post_process_transform(trf, events, sum_weights);
      else return transform_result{true, events, sum_weights};
    }

    template<class _ComputeUnc, class ..._T>
    transform_result projection_transform_oversampling(Context& ctx, const TransformCall<_T...>& call, Transf& trf) const {
      if(!this->init_transform(trf, &call.sample())) return transform_result{false,0};
      const constexpr bool use_weight = t_Sample<TransformCall<_T...>>::use_weight();
      const constexpr bool has_function = TransformCall<_T...>::has_function();
      const constexpr bool compute_uncertainties = teq<_ComputeUnc, ttrue>::value();

      t_EvalData<Transf> p;
      trf.init_eval(p);

      size_t events(0);
      std::normal_distribution<> norm(0,0);
      double sum_weights(0.0), xv;
      Coeff w(0.0);
      std::vector<double> xp(this->dims(),0);
      Coeff *par_ptr = trf.parameters().data_ptr();
      double *par_unc_ptr = trf.uncertainties().data_ptr();
      for(auto& ev : call.event_selector().select(call.sample())) {
        for(unsigned l=0; l<m_oversampling; ++l) {
          if(!l && !m_oversampling_smear_all) {
            for (unsigned int j=0; j<This::dims(); ++j) xp[j] = ev[j];
          }
          else {
            for (unsigned int j=0; j<This::dims(); ++j) {
              do {
                xv = norm(ctx.rnd_gen(), std::normal_distribution<>::param_type(ev[j], m_oversampling_width));
              }
              while(fabs(xv) > 1);
              xp[j] = xv;
            }
          }
          if(use_weight) sum_weights += ev.weight();
          w = (use_weight ? ev.weight() : 1.0)*(!has_function ? 1.0 : call.function().eval(xp.data()));
          if(compute_uncertainties) trf.template compute<Basis::Project, _project_unc<Coeff>>(xp.data(), p, _project_unc<Coeff>(w, par_ptr, par_unc_ptr));
          else trf.template compute<Basis::Project, _project<Coeff>>(xp.data(), p, _project<Coeff>(w, par_ptr));
        }
        events++;
      }
      if(!use_weight) sum_weights = (double)events;

      do_log_debug("projection_transform_oversampling() events: "<<events<<" sum_weights: "<<sum_weights);
      if(!this->explicit_post_processing()) return post_process_transform(trf, events, sum_weights);
      else return transform_result{true, events, sum_weights};
    }

    template<class _ComputeUnc, class ..._T, class _Transf1D>
    transform_result projection_transform_oversampling_1D(Context& ctx, const TransformCall<_T...>& call, _Transf1D& trf) const {
      using TCoeff = t_CoefficienctType<_Transf1D>;
      if(!this->init_transform(trf, &call.sample())) return transform_result{false,0};
      const constexpr bool use_weight = t_Sample<TransformCall<_T...>>::use_weight();
      const constexpr bool has_function = TransformCall<_T...>::has_function();
      const constexpr bool compute_uncertainties = teq<_ComputeUnc, ttrue>::value();
      const unsigned v = call.variable();

      t_EvalData<_Transf1D> p;
      trf.init_eval(p);

      size_t events(0);
      double sum_weights(0.0);
      std::normal_distribution<> norm(0,0);
      double xv;
      TCoeff w(0.0);
      TCoeff *par_ptr = trf.parameters().data_ptr();
      double *par_unc_ptr = trf.uncertainties().data_ptr();
      for(auto& ev : call.event_selector().select(call.sample())) {
        for(unsigned l=0; l<m_oversampling; ++l) {
          if(!l && !m_oversampling_smear_all) xv = ev[v];
          else {
            do {
              xv = norm(ctx.rnd_gen(), std::normal_distribution<>::param_type(ev[v], m_oversampling_width));
            }
            while(fabs(xv) > 1);
          }
          if(use_weight) sum_weights += ev.weight();
          w = (use_weight ? ev.weight() : 1.0)*(!has_function ? 1.0 : call.function().eval(&xv));
          if(compute_uncertainties) trf.template compute<Basis::Project, _project_unc<TCoeff>>(&xv, p, _project_unc<TCoeff>(w, par_ptr, par_unc_ptr));
          else trf.template compute<Basis::Project, _project<TCoeff>>(&xv, p, _project<TCoeff>(w, par_ptr));
        }
        events++;
      }
      if(!use_weight) sum_weights = (double)events;

      do_log_debug("projection_transform_oversampling() var: "<<v<<" events: "<<events<<" sum_weights: "<<sum_weights);
      if(!this->explicit_post_processing()) return post_process_transform(trf, events, sum_weights);
      else return transform_result{true, events, sum_weights};
    }

    void bind_options() {
      this->m_options.bind("do_oversampling", m_do_oversampling);
      this->m_options.bind("oversampling_smear_all", m_oversampling_smear_all);
      this->m_options.bind("oversampling", m_oversampling);
      this->m_options.bind("oversampling_width", m_oversampling_width);
    }

    bool m_do_oversampling;
    bool m_oversampling_smear_all;
    unsigned m_oversampling;
    double m_oversampling_width;
  };
}

#endif // sfi_ProjectionTransformation_h
