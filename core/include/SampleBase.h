#ifndef sfi_SampleBase_h
#define sfi_SampleBase_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <iostream>

#include "defs.h"
#include "Variables.h"
#include "ISample.h"
#include "util.h"
#include "Matrix.h"
#include "Log.h"

namespace sfi {
  class SampleBase;
  bool _samples_normalize(Log& _log, unsigned dims, const std::vector<SampleBase*>& samples);

  class SampleBase : public ISample {
  public:
    // ---- implementation of ISample ----

    virtual std::shared_ptr<const DataSource> get_data_source() const;
    virtual void set_data_source(const std::shared_ptr<const DataSource>& ds);

    virtual void do_dump() const;

    virtual void do_write(const std::string& filename);

    virtual const std::string& get_name() const;
    virtual void set_name(const std::string& n);

    virtual bool get_is_normalized() const;

    virtual PVariables get_variables();
    virtual const PVariables get_variables() const;

    virtual void set_variables(const PVariables& vars);

    virtual void do_normalize(Log& _log) { _samples_normalize(_log, this->get_dim(), {this}); }
    
    virtual unsigned get_events() const;

    virtual void set_events(unsigned ne);

    virtual bool get_owns_data() const;

    virtual Matrix<double>& get_matrix();
    virtual const Matrix<double>& get_matrix() const;

    virtual OptionMap& get_meta_data();
    virtual const OptionMap& get_meta_data() const;
    
    virtual bool do_append(const ISample& sb);

    virtual void do_clear();

    // ---- public methods ----

    inline std::shared_ptr<const DataSource> data_source() const { return m_src; }
    inline void data_source(const std::shared_ptr<const DataSource>& ds) { m_src = ds; }

    inline const std::string& name() const { return m_name; }
    inline void name(const std::string& n) { m_name = n; }

    inline bool is_normalized() const { return m_is_normalized; }
    inline void is_normalized(bool b) { m_is_normalized = b; }

    inline PVariables variables() { return m_vars; }
    inline const PVariables variables() const { return m_vars; }

    inline void variables(const PVariables& vars) { m_vars = vars; }

    inline bool owns_data() const { return this->m_data.owns_memory(); }

    inline Matrix<double>& matrix() { return m_data; }
    inline const Matrix<double>& matrix() const { return m_data; }

    inline OptionMap& meta_data() { return m_meta_data; }
    inline const OptionMap& meta_data() const { return m_meta_data; }
    
    void dump() const;

    void write(const std::string& filename);

    bool normalize(Log& _log);

    bool find_limits(Log& _log, double* minv, double* maxv);

    /** @throws SampleException if appending failed */
    bool append(const SampleBase& sb);

    void clear();
  protected:
    SampleBase(unsigned nevents, unsigned evsize, const std::string& name, const PVariables& vars);

    SampleBase(unsigned nevents, unsigned evsize, double* data, const std::string& name, const PVariables& vars = nullptr);

    virtual ~SampleBase();

    bool resize(unsigned ev);

    void reinitialize(unsigned nevents, const std::string& name, const PVariables& vars, unsigned evsize);

    void reinitialize(unsigned nevents, double* data, const std::string& name, const PVariables& vars = nullptr);

    void copy_to(bool deep_copy, SampleBase* res, unsigned nevents) const;

    inline size_t events() const { return m_data.rows(); }

    std::string m_name;
    PVariables m_vars;
    std::shared_ptr<const DataSource> m_src;
    Matrix<double> m_data;
    OptionMap m_meta_data;
    bool m_is_normalized;
  };
}

#endif // sfi_SampleBase_h
