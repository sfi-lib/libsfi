#ifndef sfi_transform_functions_h
#define sfi_transform_functions_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * A collection of functions for performing transformations.
 */
#include <Sample.h>
#include <Transformation.h>
#include <functions.h>
#include <timer.h>

#ifdef SFI_ENABLE_MP
#include <mp/Dispatcher.h>
#endif

namespace sfi {
  /**
   * Run single Transformation on one Sample.
   * @return Transform instance
   */
  template<class _Transfo, class _EventSel = AllEventSel, bool _SplUseWeight = false>
  t_Transform<_Transfo>*
  perform_transformation(Context& ctx, const Sample<_Transfo::dims(), tbool<_SplUseWeight>>& sample, const _Transfo& trfo, const _EventSel& esel = _EventSel()) {
    auto* res = trfo.create_transform(ctx);
    trfo.transform(ctx, create_transform_call(sample, esel), *res);
    return res;
  }

  /**
   * Run single Transformation on one Sample, store result in provided Transform.
   * @return Result of Transformation
   */
  template<class _Transfo, class _EventSel, bool _SplUseWeight = false>
  t_transform_result<_Transfo>
  perform_transformation(Context& ctx, const Sample<_Transfo::dims(), tbool<_SplUseWeight>>& sample, const _Transfo& trfo, const _EventSel& esel, t_Transform<_Transfo>& res) {
    return trfo.transform(ctx, create_transform_call(sample, esel), res);
  }

  template<class _Trafo>
  struct transform_result : public result {
    transform_result():result(false), time(0) {}
    TransformSet<t_Transform<_Trafo>> transforms;
    std::vector<t_transform_result<_Trafo>> results;
    double time;
  };

  /**
   * Transform all Sample in SampleSet, using one Transformation.
   * @return transform_result with all Transform
   */
  template<class _Trafo, class _EventSel, bool _SplUseWeight = false>
  transform_result<_Trafo>
  perform_transformations(Context& ctx, Log& log, const SampleSet<_Trafo::dims(), tbool<_SplUseWeight>>& samples, const _Trafo& trfo, const _EventSel& esel) {
    Timer<true> ti;
    ti.start();
    transform_result<_Trafo> res;
    res.status = true;
    res.results.reserve(samples.size());
    
#ifdef SFI_ENABLE_MP
    using RType = std::pair<t_transform_result<_Trafo>,t_Transform<_Trafo>*>;
    if(ctx.dispatcher()->enable_multi_processing()) {
      auto js = mp::job_set_create<void, RType>();
      for(auto &spl : samples) {
        js += [&] (Context&) {
          auto* trf = trfo.create_transform(ctx);
          return std::make_pair(trfo.transform(ctx, create_transform_call(spl, esel), *trf), trf);
        };
      }
      auto jt = mp::job_seq_create(js)
      + [&] (Context& ctx, std::vector<RType>&& r) {
        for(auto& _res : r) {
          res.results.push_back(std::move(_res.first));
          res.transforms.add(_res.second);
        }
        (*ctx.dispatcher())<<mp::Dispatcher::done;
      };
      (*ctx.dispatcher())<<jt;
      ctx.dispatcher()->run();
    } else {
#endif
    for(auto &spl : samples) {
      auto* trf = trfo.create_transform(ctx);
      auto tres = trfo.transform(ctx, create_transform_call(spl, esel), *trf);
      res.results.push_back(tres);
      res.status = res.status && (bool)tres;
      res.transforms.add(trf);
    }
#ifdef SFI_ENABLE_MP
    }
#endif
    res.time = ti.stop();
    log_info(log, "perform_transformations() Transformed "<<samples.size()<<" samples in "<<res.time<<" s");
    return res;
  }

  /**
   * Transform all Sample in SampleSet, using one Transformation per Sample.
   * @return transform_result with all Transform
   */
  template<class _Trafo, class _EventSel, bool _SplUseWeight = false>
  transform_result<_Trafo>
  perform_transformations(Context& ctx, Log& log, const SampleSet<_Trafo::dims(), tbool<_SplUseWeight>>& samples, const std::vector<_Trafo>& trafos, const _EventSel& esel) {
    transform_result<_Trafo> res;
    const size_t N(samples.size());
    if(N != trafos.size()) return res;
    Timer<true> ti;
    ti.start();
    res.results.reserve(N);
    res.status = true;
    for(size_t i=0; i<N; ++i) {
      auto* trf = trafos[i].create_transform(ctx);
      log_debug(log, "perform_transformations() Transforming sample '"<<samples[i].name()<<"' using Transformation: "<<trafos[i]);
      auto tres = trafos[i].transform(ctx, create_transform_call(samples[i], esel), *trf);
      res.results.push_back(tres);
      res.status = res.status && (bool)tres;
      res.transforms.add(trf);
    }
    res.time = ti.stop();
    log_info(log, "perform_transformations() Transformed "<<N<<" samples in "<<res.time<<" s");
    return res;
  }

  /**
   * @see perform_transformations
   */
  template<class _Trafo, class _EventSel, bool _SplUseWeight = false>
  inline transform_result<_Trafo>
  perform_transformations(Context& ctx, Log& log, const SampleSet<_Trafo::dims(), tbool<_SplUseWeight>>& samples, const std::initializer_list<_Trafo>& trafos, const _EventSel& esel) {
    return perform_transformations(ctx, log, samples, std::vector<_Trafo>(trafos), esel);
  }

  /**
   * Helper functions for calling a Transformation with the correctly typed Transform
   * for a single variable when the Transform is heterogeneous.
   */
  template<unsigned _N, class _Trafo, class _EventSel, class _Func, bool _SplUseWeight, class _EigenSys>
  inline bool call_single_var_transform_N(const _Trafo& trfo, unsigned var0, unsigned, Context& ctx, const Sample<_Trafo::dims(), tbool<_SplUseWeight>>& spl, TransformVariableBase* _res, const _Func& fun, const _EventSel& esel, const _EigenSys&) {
    using Transf = t_Transform1D<t_Transform<_EigenSys>, _N>;
    auto* res = dynamic_cast<Transf*>(_res);
    if(res) {
      return create_transform_call(var0, spl, esel, fun).transform(ctx, trfo, *res);
    }
    else return false;
  }

  template<class _Trafo, class _EventSel, class _Func, bool _SplUseWeight, class _EigenSys>
  inline bool call_single_var_transform(const _Trafo& trfo, unsigned var0, unsigned var, Context& ctx, const Sample<_Trafo::dims(), tbool<_SplUseWeight>>& spl, TransformVariableBase* _res, const _Func& fun, const _EventSel& esel, const _EigenSys& eigens) {
    if(var < eigens.dims()) {
      switch(var) {
      case 0: return call_single_var_transform_N<0>(trfo, var0, var, ctx, spl, _res, fun, esel, eigens);
      case 1: return call_single_var_transform_N<1>(trfo, var0, var, ctx, spl, _res, fun, esel, eigens);
      case 2: return call_single_var_transform_N<2>(trfo, var0, var, ctx, spl, _res, fun, esel, eigens);
      case 3: return call_single_var_transform_N<3>(trfo, var0, var, ctx, spl, _res, fun, esel, eigens);
      case 4: return call_single_var_transform_N<4>(trfo, var0, var, ctx, spl, _res, fun, esel, eigens);
      case 5: return call_single_var_transform_N<5>(trfo, var0, var, ctx, spl, _res, fun, esel, eigens);
      case 6: return call_single_var_transform_N<6>(trfo, var0, var, ctx, spl, _res, fun, esel, eigens);
      case 7: return call_single_var_transform_N<7>(trfo, var0, var, ctx, spl, _res, fun, esel, eigens);
      case 8: return call_single_var_transform_N<8>(trfo, var0, var, ctx, spl, _res, fun, esel, eigens);
      case 9: return call_single_var_transform_N<9>(trfo, var0, var, ctx, spl, _res, fun, esel, eigens);
      }
    }
    return false;
  }

  template<class _Trafo, class _EventSel, class _Func, bool _SplUseWeight, class _EigenSysLeft, class _EigenSysRight>
  inline bool call_single_var_transform(const _Trafo& trfo, unsigned var0, unsigned var, Context& ctx, const Sample<_Trafo::dims(), tbool<_SplUseWeight>>& spl, TransformVariableBase* _res, const _Func& fun, const _EventSel& esel, const EigenSystem<_EigenSysLeft, _EigenSysRight>& eigens) {
    if(var < eigens.left_dims()) return call_single_var_transform(trfo, var0, var, ctx, spl, _res, fun, esel, eigens.left());
    else return call_single_var_transform(trfo, var0, var - eigens.left_dims(), ctx, spl, _res, fun, esel, eigens.right());
  }

  /**
   * Transform each variable separately in the Sample.
   * @return TransformSet of the individual Transforms
   */
  template<class _Trafo, class _EventSel, class _Func = NoopFunc, bool _SplUseWeight = false>
  TransformSet<ITransform>
  perform_single_transformations(Context& ctx, const Sample<_Trafo::dims(), tbool<_SplUseWeight>>& spl, const _Trafo& trfo, const _EventSel& esel, const _Func& fun = NoopFunc()) {
    TransformSet<ITransform> res;
    for(unsigned var = 0; var < _Trafo::dims(); ++var) {
      TransformVariableBase* trf = trfo.create_transform_1D(ctx, var);
      auto vars = std::make_shared<Variables>(1);
      vars->at(0) = spl.variables()->at(var);
      call_single_var_transform<_Trafo, _EventSel, _Func, _SplUseWeight>(trfo, var, var, ctx, spl, trf, fun, esel, t_EigenSystem<t_Transform<_Trafo>>(trfo.eigenvectors()));
      trf->variables(vars);
      res.add(trf);
    }
    return res;
  }

  /**
   * Transform each variable separately for each Sample.
   * The resulting TransformSet2D is first indexed by sample nr, then by variable nr.
   * @return TransformSet2D of the individual Transforms.
   */
  template<class _Trafo, class _EventSel, class _Func = NoopFunc, bool _SplUseWeight = false>
  TransformSet2D<ITransform>
  perform_single_transformations(Context& ctx, const SampleSet<_Trafo::dims(), tbool<_SplUseWeight>>& samples, const _Trafo& trfo, const _EventSel& esel, const _Func& fun = NoopFunc()) {
    const unsigned nsamples(samples.size());
    TransformSet2D<ITransform> res(nsamples);
    for(unsigned s=0; s<nsamples; ++s) res[s] = perform_single_transformations(ctx, samples[s], trfo, esel, fun);
    return res;
  }

  /** @return Transform with name matching the given suffix */
  template<class _Transf>
  const _Transf* transform_by_name_suffix(const TransformSet<_Transf>& tset, const std::string& suffix) {
    for(auto& trf : tset) {
      if(ends_with(trf.name(), suffix)) return &trf;
    }
    return 0;
  }
  template<class _Transf>
  _Transf* transform_by_name_suffix(TransformSet<_Transf>& tset, const std::string& suffix) {
    for(auto& trf : tset) {
      if(ends_with(trf.name(), suffix)) return &trf;
    }
    return 0;
  }

  template<class _Transf, unsigned _NDims, bool _SplUseWeight = false>
  const _Transf* transform_by_sample(const TransformSet<_Transf>& tset, const Sample<_NDims, tbool<_SplUseWeight>>& spl) {
    return transform_by_name_suffix(tset, spl.name());
  }
  template<class _Transf, unsigned _NDims, bool _SplUseWeight = false>
  _Transf* transform_by_sample(TransformSet<_Transf>& tset, const Sample<_NDims, tbool<_SplUseWeight>>& spl) {
    return transform_by_name_suffix(tset, spl.name());
  }

  template<class _Transf> inline bool write_transform(const _Transf& trf, const std::string& file_name) {
    Options opts;
    trf.write(opts);
    return opts.write(file_name);
  }

  template<class _Transf> inline _Transf read_transform(const std::string& file_name) {
    Options opts;
    opts.parse_opts_file(file_name);
    return _Transf(opts);
  }

  template<class _CTransf, class _Trf>
  struct param_template_result {
    param_template_result(t_EigenVectors<_CTransf> ev):transform(ev, "", true), points(0,0), status(false) {}
    SampleSet<1> samples;
    _CTransf transform;
    Matrix<double> points;
    TransformSet<_Trf> point_transforms;
    bool status;
    inline operator bool() const { return this->status; }
  };

  /**
   * Create 1D template with 1 or 2 parameters
   * @param trfo_name Name of transformation (in 'transformations')
   * TODO: Extend to N-D and allow arbitrary number of params
   */
  template<class _CTransfo, class _Transf>
  auto create_param_template(Context& ctx, OptionMap& opts, Log& log, TransformSet<_Transf>& trfs, SampleSet<1>& spls, const std::string& ch, const std::string& src, const std::string& trfo_name = "") {
    const unsigned NPars = t_RightTransform<t_Transform<_CTransfo>>::dims();
    const unsigned NDims = 1;
    auto& topts = opts.sub(trfo_name.empty() ? ("transformations.comb_"+ch+"_"+src) : ("transformations."+trfo_name));
    _CTransfo combt(ctx, topts);
    auto& var = topts.get<std::string>("variant");
    std::string prefix = ch+"_"+src+"_"+var;
    int npts1(topts.get<int>("npoints1"));
    int npts2((NPars > 1) ? topts.get<int>("npoints2") : 1);
    auto& vars = combt.variables();
    log_info(log, "create_param_template() channel: "<<ch<<" source: "<<src<<" name: "<<combt.name()<<" variant: "<<var<<" npts1: "<<npts1<<" npts2: "<<npts2<<" varables: "<<vars);
    for(unsigned p=0;p<NPars; ++p) vars.at(NDims + p).lock();

    param_template_result<t_Transform<_CTransfo>, _Transf> res(combt.eigenvectors());
    res.points.size(npts1*npts2, NPars);

    unsigned r=0;
    for(auto& spl : spls) {
      if(r > res.points.rows()) {
        log_error(log, "create_param_template() too many transforms ch:"<<ch<<" src: "<<src<<" variant: "<<var);
        break;
      }
      else if(starts_with(spl.name(), prefix)) {
        auto* trf = transform_by_sample(trfs, spl);
        // Make sure the samples are not owned by the set
        if(r == 0) {
          res.samples = SampleSet<1>({&spl}, false);
          res.point_transforms = TransformSet<_Transf>({trf}, false);
        }
        else {
          res.samples.add(&spl);
          res.point_transforms.add(trf);
        }

        auto src = spl.data_source();
        log_info(log, "create_param_template() r: "<<r<<" spl: "<<spl.name()<<" trf name: "<<trf->name()<<" : ");
        for(unsigned p=0;p<NPars; ++p) {
          res.points(r,p) = src->options().get<double>(vars[NDims + p].name());
          log_info(log, "create_param_template() '"<<vars[NDims + p].name()<<"' : "<<res.points(r,p));
        }
        ++r;
      }
    }
    if(combt.combine(res.point_transforms, res.points, res.transform)) {
      log_info(log, "create_param_template() result: '"<<res.transform.name()<<"' variables: "<<*res.transform.variables());
      res.status = true;
    }
    else {
      log_error(log, "create_param_template() failed!");
      res.status = false;
    }
    return res;
  }
}

#endif // sfi_transform_functions_h
