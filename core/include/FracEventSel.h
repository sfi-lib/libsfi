#ifndef sfi_FracEventSel_h
#define sfi_FracEventSel_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "EventSelector.h"

namespace sfi {
  /**
   * @class FracEventSel
   *
   * Split the sample in <nparts> parts, select all events in the part or
   * that are not in the part, depending on the template parameter _Include.
   * If _Include is true then select the events that balong to the part.
   */
  template<bool _Include>
  struct FracEventSel : public IEventSelector {
    FracEventSel(uint8_t nparts = 1, uint8_t partnr = 0):m_nparts(nparts), m_partnr(partnr) {}

    virtual ~FracEventSel() {}

    virtual bool do_select(unsigned e) const { return (*this)(e); };

    virtual double get_fraction() const { return fraction(); }

    inline bool operator()(unsigned e) const {
      if(_Include) return e % m_nparts == m_partnr;
      else return e % m_nparts != m_partnr;
    }

    inline double fraction() const { return _Include ? 1.0/double(m_nparts) : double(m_nparts - 1)/double(m_nparts); }

    template<unsigned _NDims, class _SplUseWeight>
    struct Selector {
      using SampleTy = Sample<_NDims, _SplUseWeight>;
      Selector(const SampleTy& sample, uint8_t nparts, uint8_t partnr):m_sample(sample), m_nparts(nparts), m_partnr(partnr) { }

      struct iterator {
        iterator(size_t index, uint8_t nparts, uint8_t partnr, const SampleTy& sample):
          m_index(index), m_nparts(nparts), m_partnr(partnr), m_sample(sample) { }

        inline typename SampleTy::EvDataConstRef operator*() { return m_sample[m_index]; }

        inline iterator& operator++() {
          if(_Include) m_index += m_nparts;
          else {
            ++m_index;
            if(m_index % m_nparts == m_partnr) ++m_index;
          }
          return *this;
        }

        inline bool operator!=(const iterator& o) const { return m_index != o.m_index; }

        inline size_t index() const { return this->m_index; }
      protected:
        size_t m_index;
        uint8_t m_nparts, m_partnr;
        const SampleTy& m_sample;
      };
      inline iterator begin() {
        if(_Include) return iterator(m_partnr, m_nparts, m_partnr, m_sample);
        else return iterator(m_partnr==0 ? 1 : 0, m_nparts, m_partnr, m_sample);
      }
      inline iterator end() {
        if(_Include) {
          if(m_sample.events() % m_nparts > m_partnr)
            return iterator((m_sample.events()/m_nparts+1)*m_nparts + m_partnr, m_nparts, m_partnr, m_sample);
          else return iterator((m_sample.events()/m_nparts)*m_nparts + m_partnr, m_nparts, m_partnr, m_sample);
        }
        else {
          if(m_sample.events() % m_nparts == m_partnr) return iterator(m_sample.events() + 1, m_nparts, m_partnr, m_sample);
          else return iterator(m_sample.events(), m_nparts, m_partnr, m_sample);
        }
      }
    protected:
      const SampleTy& m_sample;
      uint8_t m_nparts, m_partnr;
    };

    template<unsigned _NDims, class _SplUseWeight>
    inline Selector<_NDims, _SplUseWeight> select(const Sample<_NDims, _SplUseWeight>& spl) const {
      return Selector<_NDims, _SplUseWeight>(spl, m_nparts, m_partnr);
    }
  protected:
    uint8_t m_nparts, m_partnr;
  };

  using FracIncEventSel = FracEventSel<true>;
  using FracExcEventSel = FracEventSel<false>;
}
#endif // sfi_FracEventSel_h
