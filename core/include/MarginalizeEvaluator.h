#ifndef sfi_MarginalizeEvaluator_h
#define sfi_MarginalizeEvaluator_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ITransform.h"
#include "Variables.h"
#include "Basis.h"

namespace sfi {
  template<class _Transf, unsigned _Dims>
  class MarginalizeEvaluator {};

  template<class _Transf>
  class MarginalizeEvaluator<_Transf, 1> : public IEvaluator {
  public:
    MarginalizeEvaluator(const _Transf* trf, unsigned axis, bool external_coordinates = false, double scale = 1.0):
      m_trf(trf), m_var(trf->variables()->at(axis)), m_axis(axis), m_scale(scale), m_external_coordinates(external_coordinates),
      m_phi(trf->var_degree(axis) + 2, 0.0) {
    }

    virtual ~MarginalizeEvaluator() {}

    // ---- implementation of IEvaluator ----

    virtual double get_value(const double* x) { return eval_marginal(*x); }

    virtual const ITransform* get_transform() { return m_trf; }

    virtual void set_scale(double scale) { m_scale = scale; }

    virtual void set_external_coordinates(bool ec) { m_external_coordinates = ec; }

    virtual unsigned get_dimensions() const { return 1; }

    virtual bool do_eval(unsigned npts, const double* xv, double* res) {
      for(unsigned i=0; i<npts; ++i) res[i] = eval_marginal(xv[i]);
      return true;
    }

    virtual const Variable& get_variable(unsigned) const { return m_var; }

    virtual IEvaluator* get_clone(bool deep) const {
      return new MarginalizeEvaluator<_Transf, 1>(deep ? m_trf->clone() : m_trf, m_axis, m_external_coordinates, m_scale);
    }

    virtual unsigned get_nslice_parameters() const { return 0; }

    virtual void set_slice(const double *) {}

    virtual ITransform* get_slice_transform() { return nullptr; }

    // ---- public methods ----

    inline double eval_marginal(const double _x) {
      double x(_x);
      double w = m_external_coordinates ? m_var.vtrans(_x, x) : 1.0;
      m_trf->evaln(x, m_axis, m_phi.data());

      double res(0.0);
      t_CoefficienctType<_Transf> tmp(0.0);
      for(auto& ev : m_trf->eigenvectors()) {
        if(ev[m_axis] == 0) {
          tmp = m_phi[0]*m_trf->par(ev.par_idx());
          auto idx = ev.indices();
          const unsigned md = idx.max_idx(m_axis);
          for(unsigned g=1; g<=md; ++g) {
            idx.set(m_axis, g);
            tmp += m_phi[g]*m_trf->par(idx.index());
          }
          res += coefficient_norm(true, tmp);
        }
      }
      return res*w*m_scale;
    }

  protected:
    const _Transf* m_trf;
    const Variable& m_var;
    unsigned m_axis;
    double m_scale;
    bool m_external_coordinates;
    std::vector<t_CoefficienctType<_Transf>> m_phi;
  };

  template<class _Transf>
  class MarginalizeEvaluator<_Transf, 2> : public IEvaluator {
  public:
    MarginalizeEvaluator(const _Transf* trf, unsigned a1, unsigned a2, bool external_coordinates = false, double scale = 1.0):
      m_trf(trf), m_var1(trf->variables()->at(a1)), m_var2(trf->variables()->at(a2)),
      m_axis1(a1), m_axis2(a2), m_scale(scale), m_external_coordinates(external_coordinates),
      m_phi1(trf->var_degree(a1)+2, 0.0),
      m_phi2(trf->var_degree(a2)+2, 0.0){
    }

    virtual ~MarginalizeEvaluator() {}

    // ---- implementation of IEvaluator ----

    virtual double get_value(const double* x) { return eval_marginal(x); }

    virtual const ITransform* get_transform() { return m_trf; }

    virtual void set_scale(double scale) { m_scale = scale; }

    virtual void set_external_coordinates(bool ec) { m_external_coordinates = ec; }

    virtual unsigned get_dimensions() const { return 2; }

    virtual bool do_eval(unsigned npts, const double* xv, double* res) {
      const double* x = xv;
      for(unsigned i=0; i<npts; ++i) {
        res[i] = eval_marginal(x);
        x += 2;
      }
      return true;
    }

    virtual const Variable& get_variable(unsigned d) const { return (d==0) ? m_var1 : m_var2; }

    virtual IEvaluator* get_clone(bool deep) const {
      return new MarginalizeEvaluator<_Transf, 2>(deep ? m_trf->clone() : m_trf, m_axis1, m_axis2, m_external_coordinates, m_scale);
    }

    virtual unsigned get_nslice_parameters() const { return 0; }

    virtual void set_slice(const double *) {}

    virtual ITransform* get_slice_transform() { return nullptr; }

    // ---- public methods ----

    inline double eval_marginal(const double* _x) {
      double x[2];
      double w(1.0);
      if(m_external_coordinates) {
        w *= m_var1.vtrans(_x[0], x[0]);
        w *= m_var2.vtrans(_x[1], x[1]);
      }
      else {
        x[0] = _x[0];
        x[1] = _x[1];
      }
      m_trf->evaln(x[0], m_axis1, m_phi1.data());
      m_trf->evaln(x[1], m_axis2, m_phi2.data());

      double res(0.0);
      t_CoefficienctType<_Transf> tmp(0.0);
      for(auto& ev : m_trf->eigenvectors()) {
        if((ev[m_axis1] == 0) && (ev[m_axis2] == 0)) {
          auto idx = ev.indices();
          const unsigned md1 = idx.max_idx(m_axis1);
          tmp = m_phi1[0]*m_phi2[0]*m_trf->par(ev.par_idx());
          for(unsigned g1=0; g1<=md1; ++g1) {
            idx.set(m_axis1, g1);
            const unsigned md2 = idx.max_idx(m_axis2);
            for(unsigned g2=(g1==0 ? 1 : 0); g2<=md2; ++g2) {
              idx.set(m_axis2, g2);
              tmp += m_phi1[g1]*m_phi2[g2]*m_trf->par(idx.index());
            }
          }
          res += coefficient_norm(true, tmp);
        }
      }
      return res*w*m_scale;
    }

  protected:
    const _Transf* m_trf;
    const Variable& m_var1;
    const Variable& m_var2;
    unsigned m_axis1;
    unsigned m_axis2;
    double m_scale;
    bool m_external_coordinates;
    std::vector<t_CoefficienctType<_Transf>> m_phi1;
    std::vector<t_CoefficienctType<_Transf>> m_phi2;
  };
}

#endif // sfi_MarginalizeEvaluator_h
