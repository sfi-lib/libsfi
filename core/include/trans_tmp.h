#ifndef sfi_trans_h
#define sfi_trans_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "util.h"
#include <memory>

/**
 * Common functionality for transforms.
 */
namespace sfi {
  /** Given a Type, extract the Transform type */
  template<class ..._T> struct _t_transform : tvalued<typename t_get<0,_T...>::Transf> {};
  template<class _T> struct _t_transform<std::unique_ptr<_T>> : _t_transform<_T> {};
  template<class ..._T> using t_Transform = tvalue<_t_transform<_T...>>;

  /** Given a Type, typically a Slice Evaluator or Evaluator, extract the phase space transform */
  template<class ..._T> struct _t_pstransform : _t_transform<_T...> {};
  template<class _T> struct _t_pstransform<std::unique_ptr<_T>> : _t_pstransform<_T> {};
  template<class ..._T> using t_PSTransform = tvalue<_t_pstransform<_T...>>;

  /** Given a Type, extract the result type */
  template<class _T> struct _t_transform_result : tvalued<typename _T::transform_result> {};
  template<class _T> using t_transform_result = tvalue<_t_transform_result<_T>>;

  /** Given a Type, extract the eigen vectors type */
  template<class _T> struct _t_eigenvectors : tvalued<typename _T::Eigenv> {};
  template<class _T> using t_EigenVectors = tvalue<_t_eigenvectors<_T>>;

  /** Given a Type, extract the eigen vectors iterator type */
  template<class _T> struct _t_eigenvectors_iterator : tvalued<void> {};
  template<class _T> using t_EigenVectorsIterator = tvalue<_t_eigenvectors_iterator<_T>>;

  /** Given a Type, extract the eigen vectors type */
  template<class _T> struct _t_eigenvector : tvalued<typename _T::EigenVector> {};
  template<class _T> using t_EigenVector = tvalue<_t_eigenvector<_T>>;

  /** Given a Type, extract the transform type */
  template<class _T, unsigned _Dim> struct _t_basis : tvalued<void> {};
  template<class _T, unsigned _Dim = 0> using t_Basis = tvalue<_t_basis<_T, _Dim>>;

  /** Given a transform type, extract the type for coefficients */
  template<class _T> struct _t_coefficient_type : tvalued<typename _T::Coeff> {};
  template<class _T> using t_CoefficienctType = tvalue<_t_coefficient_type<_T>>;

  /** Given a Type, extract the transform type */
  template<class _T> struct _t_eval_data : tvalued<typename _T::EvalData> {};
  template<class _T> using t_EvalData = tvalue<_t_eval_data<_T>>;

  /** Given a Type, extract the 1D transform type */
  template<class _T, unsigned _Dim = 0> struct _t_transform_1D : tvalued<typename _T::_INVALID_> {};
  template<class _T, unsigned _Dim = 0> using t_Transform1D = tvalue<_t_transform_1D<_T, _Dim>>;

  /** Given a Type, extract the event selector */
  template<class _T> struct _t_event_selector : tvalued<typename _T::EventSelTy> {};
  template<class _T> using t_EventSelector = tvalue<_t_event_selector<_T>>;

  /** Given a Type, extract the Sample */
  template<class _T> struct _t_sample : tvalued<void> {};
  template<class _T> using t_Sample = tvalue<_t_sample<_T>>;

  /** Given a Type, extract the SampleSet */
  template<class _Sample> struct _t_sampleset : tvalued<void> { };
  template<class _Sample> using t_SampleSet = tvalue<_t_sampleset<_Sample>>;

  /** Given a nested Transformation, extract the sub transformation */
  template<class ..._T> struct _t_subtransformation : tvalued<typename t_get<0, _T...>::SubTransformation> {};
  template<class ..._T> using t_SubTransformation = tvalue<_t_subtransformation<_T...>>;

  /** Given a nested Transformation, extract the transform of the sub transformation */
  template<class ..._T> using t_SubTransform = t_Transform<t_SubTransformation<_T...>>;

  /** Given a nested Transformation, extract the 1D transform of the sub transformation */
  template<class ..._T> using t_SubTransform1D = t_Transform1D<t_Transform<t_SubTransformation<_T...>>>;

  /** Determine the slice transform type */
  template<class _T, unsigned d2>
  struct _t_slice_transform : tvalued<void> {};

  template<class _T, unsigned d2>
  using t_SliceTransform = tvalue<_t_slice_transform<_T, d2>>;

  /** Determine EigenSystem */
  template<class _Transf> struct _t_eigen_system : tvalued<void> {};
  template<class _Transf> using t_EigenSystem = tvalue<_t_eigen_system<_Transf>>;

  /** Determine if the transform type is compound */
  template<class _T> struct _t_is_compound_transform : tfalse {};

  /** @return If the transform type is compound */
  template<class _T>
  static constexpr bool c_is_compound_transform() { return _t_is_compound_transform<_T>::value(); }

  template<class _EigenType> struct _t_is_eigentype : tfalse {};
  template<class _Basis> struct _t_is_basis : tfalse {};

  template<class _EigenType>
  inline constexpr bool c_is_eigentype() { return _t_is_eigentype<_EigenType>::value(); }
  template<class _Basis>
  inline constexpr bool c_is_basis() { return _t_is_basis<_Basis>::value(); }

  template<unsigned _Dims> struct tdimensions : tuint<_Dims> {};
  template<unsigned _Deg> struct tdegree : tuint<_Deg> {};

  /** Extract left or right transform of composite transform */
  template<class _T> struct _t_left_transform : tvalued<void> {};
  template<class _T> struct _t_right_transform : tvalued<void> {};

  template<class _Transf> using t_LeftTransform = tvalue<_t_left_transform<_Transf>>;
  template<class _Transf> using t_RightTransform = tvalue<_t_right_transform<_Transf>>;
}

#endif // sfi_trans_h
