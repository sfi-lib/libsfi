#include "pseudo_experiment_plots.h"
#include "ParamEst.h"
#include "ExtendedLogLikelihood.h"
#include "gui.h"
#include <TGraph2D.h>
#include "TMinuitMaximizer.h"
#include "FSpaceROPELogLikelihood.h"
#include "LikelihoodPlot.h"
#include <TransformPDF.h>
#include <SlicePDF.h>
#include <AnalyticPDF.h>
#include <CompositePDF.h>

// Transform to use for SFI-linear
using LTransf = t_Transform<t_Transform<tdimensions<2>, tdegree<4>, FullTensor, Chebyshev>>;

using PSTransfo = ProjectionTransformation<t_Transform<tdimensions<1>, Sparse, Cosine>>;
using ParamTransf = t_Transform<tdimensions<1>, tdegree<8>, FullTensor, Chebyshev>;
using CTransfo = CombinationTransformation<t_Transform<PSTransfo>, ParamTransf>;
using Transf = t_Transform<CTransfo>;

// Ordinary (non amplitude) transformations
using PSTransf = t_Transform<PSTransfo>;

// Amplitude transform for the template : SFI
using AmpPSTransfo = AmplitudeTransformation<PSTransfo>;

using Maximizer = TMinuitMaximizer;

struct options {
  bool plot_pdf = true;
  bool plot_slices = false;
  bool plot_pe_details = true;
  bool enable_overtraining_check = true;
};

void
param_1d(Options& op) {
  options opts;
  op.bind("plot_pdf",opts.plot_pdf).bind("plot_slices",opts.plot_slices);
  op.bind("plot_pe_details",opts.plot_pe_details);

  double bg_mean = op.get_as<double>("bg_mean");
  //double bg_spread = op.get_as<double>("bg_spread");
  double bg_offset = op.get_as<double>("bg_offset");

  bool init_true = op.get_as<bool>("init_with_true", false);
    
  Analysis ana(op);
  Log log(Log::get(ana.main_context(), ana.name()));
  ParamEst<AmpPSTransfo, CTransfo> pe("Amp", ana, op);

  auto samples = ana.channel_samples<1U>("s", true, true);
  for(auto& spl : samples) {
    log_info(log, "'"<<spl.name()<<"' events: "<<spl.events());
  }

  // Template for unbinned amplitude
  auto  amp_tpl_res = pe.create_template_res(samples, op, opts.plot_pdf, opts.plot_slices, opts.enable_overtraining_check);
  Transf& tpl = amp_tpl_res.transform;
  assert(tpl.is_amplitude());

  auto* dsrc2 = dynamic_cast<ParamDataSource*>(ana.channel("d2").source("d20").get());

  PSTransfo mctrfo(ana.main_context(), op.sub("transformations.model_ps")), datatrfo(ana.main_context(), op.sub("transformations.data_ps"));
  PSTransfo lin_mod_trfo(ana.main_context(), op.sub("transformations.tpl_ps_lin"));

  AmpPSTransfo amp_datatrfo(ana.main_context(), op.sub("transformations.amp_data_ps"));
  
  // template for lin sfi
  auto tres = perform_transformations(ana.main_context(), log, samples, lin_mod_trfo, EvenEventSel());
  auto lin_tpl_res = create_param_template<CTransfo>(ana.main_context(), op, log, tres.transforms, samples, "s", "s0", "tpl_lin");
  if(!lin_tpl_res) {
    log_error(log, "Failed to generate linear template");
    return;
  }
  if(op.get<bool>("plot_pdf")) {
    create_canvas("P lin MC");
    TransformPlot2D(ana.main_context(), lin_tpl_res.transform).npointsx(40).npointsy(40).title("P").color(1).plot("SURF");
  }
  if(op.get<bool>("plot_slices")) plot_slices(ana.main_context(), "lin tpl", lin_tpl_res);

  PSTransfo bmctrfo(ana.main_context(), op.sub("transformations.b_model_ps"));
  AmpPSTransfo bamp_mctrfo(ana.main_context(), op.sub("transformations.b_amp_model_ps"));
  auto* bsample = samples["s_b0"];
  auto* btrf = perform_transformation(ana.main_context(), *bsample, bmctrfo, AllEventSel());
  auto* btrf_amp = perform_transformation(ana.main_context(), *bsample, bamp_mctrfo, AllEventSel());
  log_info(log, "B trf: "<<btrf_amp->parameters());
  
  if(op.get<bool>("plot_pdf")) {
    DataTransformPlot1D(ana.main_context(), *btrf).title("B pdf").scale_to_data(true).sample(*bsample, AllEventSel()).plot();
    TransformPlot1D(ana.main_context(), *btrf_amp).scale(bsample->events()).superimpose(true).color(2).plot();
  }
  
  auto& mini_opts = op.sub("lhmini");
  auto& lh_ana_opts = op.sub("likelihoods.lh_ana");
  auto& lh_sfi_opts = op.sub("likelihoods.lh_sfi");
  auto& lh_sfi_lin_opts = op.sub("likelihoods.lh_sfi_lin");
  std::string paramset_name = "pset1";
  const double sigma = op.get_as<double>("sigma");

  if(op.get<bool>("plot_linear_likelihood")) {
    PSTransf* datatr = datatrfo.create_transform(ana.main_context());
    dsrc2->nevents(1000);
    dsrc2->mean(0.1);
    auto dspls = ana.channel_samples<PSTransf::dims()>("d2", false, true);

    datatrfo.transform(ana.main_context(), create_transform_call(dspls[0], AllEventSel()), *datatr);

    Maximizer tfit(ana, mini_opts, new FSpaceROPELogLikelihood(1,lin_tpl_res.transform, *datatr), lh_sfi_lin_opts);
    log_info(log,"params: "<<tfit.get_parameters());
    LikelihoodPlot lp(ana.main_context(), tfit, {1000.0, 0.1, -3.0});
    create_canvas("LH");
    lp.plot2D(0,40,500,2000,1,40,-0.2,0.2)->Draw("SURF");

    tfit.do_maximize();

    create_canvas("LH contour");
    lp.plot_contour(0,1,100)->Draw("ALP");

    delete datatr;
  }

  if(op.get_as<int>("plot_coefficients")) {
    const unsigned npts = 100;
    int ncoeff = op.get_as<int>("plot_coefficients");
    plot_coefficients_1D("P", lin_tpl_res, ncoeff, npts);
    plot_coefficients_1D("A", amp_tpl_res, ncoeff, npts);
  }

  if(op.get<bool>("enable_psexps")) {
    // Run pseudo experiments
    create_canvas("X");
    using PExp = PseudoExperiment<PSTransf>;
    PExp expr(ana, op.sub("psexps"));

    unsigned nexp3=0;
    if(op.get<bool>("enable_reference")) {
      // [&bg_mean,&bg_offset](const double*p,const double*x) { return bg_mean*std::exp(-(x[0]-bg_offset)*bg_mean); }
      expr.add_case("reference", [&](Context&, const Sample<1>& ds) {
        Maximizer tfit(ana, mini_opts, extended_log_likelihood("s", &ds, composite_pdf(
          analytic_pdf([&sigma](const double*p,const double*x) { return gaus(x[0], 5.0*p[0], sigma); }, "reference", NormAbsolute),
          analytic_pdf([&](const double*,const double*x) { return bg_mean*exp(-bg_mean*(x[0] - bg_offset)); }, "reference_b", NormAbsolute))), lh_ana_opts, init_true ? &ds.get_meta_data() : nullptr);
                                                                                              
        experiment_result res;
        res.status = tfit.do_maximize();
        copy_parameters(tfit.get_parameters(), res.params);
        ++nexp3;
        return res;
      });
    }

    if(op.get_as<bool>("enable_sfi_lin")) {
      expr.add_case("sfi_lin", [&](Context&, const Sample<1>& ds) -> experiment_result {
        PSTransf* datatr = datatrfo.create_transform(ana.main_context());
        datatrfo.transform(ana.main_context(), create_transform_call(ds, AllEventSel()), *datatr);
        auto* lh = new FSpaceROPELogLikelihood(1,lin_tpl_res.transform, *datatr, btrf);
        Maximizer tfit(ana, mini_opts, lh, lh_sfi_lin_opts, init_true ? &ds.get_meta_data() : nullptr);
        experiment_result res;
        res.status = tfit.do_maximize();
        copy_parameters(tfit.get_parameters(), res.params);
        res.aux_params["nvalues"] = lh->nvalues().result();
        res.aux_params["TrS_orig"] = lh->trS_orig().result();
        res.aux_params["TrS_reg"] = lh->trS_reg().result();
        delete datatr;
        return res;
      });
    }
    unsigned nexp2(0);
    if(op.get_as<bool>("enable_sfi"))
      // pass SampleMap
      expr.add_case("sfi", [&](Context&, const Sample<1>& ds) {
        Maximizer tfit(ana, mini_opts, extended_log_likelihood("s", &ds, composite_pdf(slice_pdf(tpl, 0, NormAbsolute),transform_pdf(*btrf_amp, NormAbsolute))), lh_sfi_opts, init_true ? &ds.get_meta_data() : nullptr);
      experiment_result res;
      res.status = tfit.do_maximize();
      copy_parameters(tfit.get_parameters(), res.params);
      if(!nexp2) {
        LikelihoodPlot(ana.main_context(), tfit).data_pdf_comparison(op.sub("plots.sfi_comp"),ds);
      }
      ++nexp2;
      return res;
    });
    
    expr.run();
    plot_pseudo_experiment_results(expr, opts.plot_pe_details);
  }
}

DefineGeneralExample(param_1d, paperexamples);
