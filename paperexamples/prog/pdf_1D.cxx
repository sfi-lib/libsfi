#include "fourier.h"
#include "legendre.h"
#include "TransformImpl.h"
#include "example_util.h"
#include "PDFAnalysis.h"
#include "MLTransformation.h"
#include "cross_entropy.h"

using namespace sfi;
using namespace sfi::bases;

using Eig1 = EigenVectors<1, 0U, EigenFullTensor>;

using Trafo_cos = ProjectionTransformation<Eig1, Cosine>;
using ATrafo_cos = AmplitudeTransformation<Trafo_cos, ttrue>;

using Trafo_legendre = ProjectionTransformation<Eig1, Legendre>;
using ATrafo_legendre = AmplitudeTransformation<Trafo_legendre, ttrue>;

using Trafo_fourier = ProjectionTransformation<Eig1, Fourier>;
using ATrafo_fourier = AmplitudeTransformation<Trafo_fourier, ttrue>;

template<class Trafo, class ATrafo, class Transf>
void
transform_sample_ext(const std::string&, PDFAnalysis& ana, const Sample<1U>& spl, const OptionMap& opts, ATrafo& atfo, const Transf& at0, Canvas& can, teif<!teq<t_CoefficienctType<Transf>, std::complex<double>>::value()>* = 0) {
  using ATrafoO = AmplitudeTransformation<Trafo>;
  using MLTransfo = MLTransformation<Eig1, t_Basis<Transf>>;

  if(opts.get_as<bool>("do_amp") && opts.get_as<bool>("print_transform_stats")) {
    transform_stats("pdf", at0, spl, AllEventSel(), true);
  }
  if(opts.get_as<bool>("do_ml")) {
    Transf a_trans(t_EigenVectors<Transf>(opts.get_as<bool>("degrees")), "a", true);
    a_trans.is_amplitude(true);
    a_trans.compute_covariance(true);
    OptionMap opts;
    MLTransfo mltrfo(ana.main_context(), opts);
    mltrfo.square_pdf(a_trans.is_amplitude());
    mltrfo.debug_minuit(false).compute_covariance(true);
    mltrfo.do_minos(false).do_hesse(true);
    mltrfo.transform(ana.main_context(), create_transform_call(spl, AllEventSel()), a_trans);
    can.draw(new TransformPlot1D(ana.main_context(), a_trans)).title("c").npoints(opts.get_as<bool>("npdfbins")).color(kGreen).superimpose(true);

    a_trans.dump();
    std::cout<<a_trans.covariance()<<std::endl;
    a_trans.normalize(1.0);
    std::cout<<"stat_invp_sum="<<stat_invp_sum(spl, a_trans, AllEventSel())<<std::endl;
  }

  if(opts.get_as<bool>("do_pe")) {
    ATrafoO atfo0(ana.main_context(), "at", Trafo(ana.main_context(), "mc_t"));
    atfo0.speed(atfo.speed()).max_iterations(atfo.max_iterations()).print_level(sfi::Warning);
    auto* at1 = atfo0.create_transform(ana.main_context());
    SampleCovariance cov_mc(at0.npar());
    std::poisson_distribution<> po(opts.get_as<bool>("nevents"));
    for(unsigned i=0; i<1000; ++i) {
      ana.maxevt(po(ana.main_context().rnd_gen()));
      auto samples0 = ana.samples<Transf>();
      if(atfo0.transform(ana.main_context(), create_transform_call(samples0[0], AllEventSel()), *at1)) cov_mc += at1->parameters();
    }
    cov_mc.compute();
    std::cout<<"Amplitude MC covariance:"<<std::endl;
    std::cout<<cov_mc.covariance()<<std::endl;
  }
}

template<class Trafo, class ATrafo, class Transf>
void
transform_sample_ext(const std::string&, PDFAnalysis&, const Sample<1U>&, const OptionMap&, ATrafo&, const Transf&, Canvas&, teif<teq<t_CoefficienctType<Transf>, std::complex<double>>::value()>* = 0) { }

template<class Trafo, class ATrafo>
void
transform_sample(const std::string& prefix, PDFAnalysis& ana, Log log, const Sample<1U>& spl, const OptionMap& opts) {
  log_info(log, "*** "<<prefix);
  using Transf = t_Transform<Trafo>;
  auto npdfbins = opts.get_as<unsigned>("npdfbins");

  Trafo trfo(ana.main_context(), opts.sub("transformations.ps_lin"));
  Transf* t1 = trfo.create_transform(ana.main_context());
  trfo.transform(ana.main_context(), create_transform_call(spl, AllEventSel()), *t1);
  auto can = compare_data_and_transform(ana.main_context(), prefix, " pdf", false, *t1, spl, npdfbins);

  if(opts.get_as<bool>("do_plot_single")) {
    Transf* t2 = trfo.create_transform(ana.main_context());
    trfo.transform(ana.main_context(), create_transform_call(0U, spl, AllEventSel()), *t2);
    can.draw(new TransformPlot1D(ana.main_context(), t2)).title("a2").npoints(npdfbins).color(kGreen);
    delete t2;
  }
  if(opts.get_as<bool>("do_scan_ce")) {
    unsigned Ndeg = opts.get_as<unsigned>("max_degrees") - opts.get_as<unsigned>("min_degrees")+1;
    plot_cross_entropy(ana, log, spl, "Lin "+prefix, Ndeg, opts.get_as<unsigned>("ce_parts"),opts.get_as<unsigned>("min_degrees"),trfo);
  }
  
  // ---- Amplitude ----
  ATrafo atfo(ana.main_context(), opts.sub("transformations.ps_amp"));
  Transf* at0 = atfo.create_transform(ana.main_context());
  if(opts.get_as<bool>("do_amp")) {
    auto res_s = atfo.transform(ana.main_context(), create_transform_call(spl, AllEventSel()), *at0);
    std::cout<<"at0 I="<<at0->integral_pdf()<<" params : "<<at0->parameters()<<std::endl;
    can.draw(new TransformPlot1D(ana.main_context(), at0)).title("b").npoints(npdfbins).color(kRed);

    if(opts.get_as<bool>("do_plot_single")) {
      Transf* at1 = atfo.create_transform(ana.main_context());
      atfo.transform(ana.main_context(), create_transform_call(0U, spl, AllEventSel()), *at1);
      std::cout<<"at1 I="<<at1->integral_pdf()<<" params : "<<at1->parameters()<<std::endl;
      can.draw(new TransformPlot1D(ana.main_context(), at1)).title("b2").npoints(npdfbins).color(kBlue);
      delete at1;
    }
    if(opts.get_as<bool>("do_plot_axis")) {
      auto* axist = at0->axis_transform(0);
      can.draw(new TransformPlot1D(ana.main_context(), axist)).title("A b2").npoints(npdfbins).color(kYellow);
    }
    if(opts.get_as<bool>("do_plot_margin")) {
      auto mtrf = at0->get_marginalize_evaluator_1D(0);
      can.draw(new TransformPlot1D(ana.main_context(), mtrf.release())).title("M2D").npoints(npdfbins).color(kCyan);
    }
    if(opts.get_as<bool>("do_plot_details")) {
      plot_amp_transform_details(prefix+"S", res_s, "plots/");
    }
    if(opts.get_as<bool>("do_scan_ce")) {
      unsigned Ndeg = opts.get_as<unsigned>("max_degrees") - opts.get_as<unsigned>("min_degrees")+1;
      plot_cross_entropy(ana, log, spl, prefix,Ndeg, opts.get_as<unsigned>("ce_parts"),opts.get_as<unsigned>("min_degrees"),atfo);
    }
  }

  transform_sample_ext<Trafo, ATrafo, Transf>(prefix, ana, spl, opts, atfo, *at0, can);

  can.plot();
  delete at0;
  delete t1;
  log_info(log, "***");
}

template<class Trafo, class ATrafo>
void
plot_1d_transforms(OptionMap& opts) {
  using Transf = t_Transform<Trafo>;

  PDFAnalysis ana(opts);
  Log log = Log::get(ana.main_context(), "pdf_1D");

  if(opts.get_as("do_gaus_005", false)) {
    ana.set_gaus_uniform(0.05);
    transform_sample<Trafo, ATrafo>("Gaus-Uniform 0.05", ana, log, ana.samples<Transf>()[0], opts);
  }
  
  if(opts.get_as("do_gaus_05", false)) {
    ana.main_context().rnd_gen().seed(14);
    ana.set_gaus_uniform(0.5);
    transform_sample<Trafo, ATrafo>("Gaus-Uniform 0.5", ana, log, ana.samples<Transf>()[0], opts);
  }
  
  if(opts.get_as("do_gaus", false)) {
    ana.main_context().rnd_gen().seed(15);
    ana.set_gaus();
    transform_sample<Trafo, ATrafo>("Gaus", ana, log, ana.samples<Transf>()[0], opts);
  }
  
  if(opts.get_as("do_step", false)) {
    ana.main_context().rnd_gen().seed(16);
    ana.set_step(0.05);
    transform_sample<Trafo, ATrafo>("Step", ana, log, ana.samples<Transf>()[0], opts);
  }
  
  if(opts.get_as("do_breit_wigner", false)) {
    ana.main_context().rnd_gen().seed(17);
    ana.set_breit_wigner();
    transform_sample<Trafo, ATrafo>("Breit-Wigner", ana, log, ana.samples<Transf>()[0], opts);
  }
  
  if(opts.get_as("do_gaus_expo", false)) {
    ana.main_context().rnd_gen().seed(18);
    ana.set_gaus_expo(0.5);
    transform_sample<Trafo, ATrafo>("Gaus-Expo", ana, log, ana.samples<Transf>()[0], opts);
  }
  
  if(opts.get_as("do_sine", false)) {
    ana.main_context().rnd_gen().seed(19);
    ana.set_sine();
    transform_sample<Trafo, ATrafo>("Sin-uniform", ana, log, ana.samples<Transf>()[0], opts);
  }
  
  if(opts.get_as("do_expo", false)) {
    ana.main_context().rnd_gen().seed(20);
    transform_sample<Trafo, ATrafo>("Expo", ana, log, ana.samples<Transf>(true, "s2")[0], opts);
  }
}

void
pdf_1D(OptionMap& opts) {
  auto& trans_type = opts.get<std::string>("trans_type");
  if(trans_type == "cos") plot_1d_transforms<Trafo_cos, ATrafo_cos>(opts);
  else if(trans_type == "legendre") plot_1d_transforms<Trafo_legendre, ATrafo_legendre>(opts);
  else if(trans_type == "fourier") plot_1d_transforms<Trafo_fourier, ATrafo_fourier>(opts);
}

DefineGeneralExample(pdf_1D, paperexamples);
