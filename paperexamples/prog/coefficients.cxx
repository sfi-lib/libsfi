#include "example_util.h"
#include "MLTransformation.h"
#include "BootstrapTransformation.h"
#include "AggregateTransformation.h"
#include "sparse_system.h"

using namespace sfi;
using namespace sfi::bases;

using Eig1 = EigenVectors<1, 4, EigenMonomicSparse>;
using PSTransfo = ProjectionTransformation<Eig1, Cosine>;
using MLTransfo = MLTransformation<Eig1, Cosine>;
using PSTransf = t_Transform<PSTransfo>;

using APSTransfo = AmplitudeTransformation<PSTransfo>;

template<class _Transfo, class _Transf>
void
do_pseudo_experiments(Analysis& ana, OptionMap& opts, const std::string& prefix, Sample<1>* spl, unsigned nexperiments, const _Transfo& trfo, const _Transf& _true_trf,unsigned nevents, unsigned nbsevents) {
  const bool do_poisson = true;
  const unsigned npar = _true_trf.npar();
  const bool plot_coeff = opts.get_as("plot_coefficients", false);
  const bool is_amp = _true_trf.is_amplitude();
  
  std::poisson_distribution<> po(nevents);
  _Transf true_trf(_true_trf);
  true_trf.normalize(nevents);

  MLTransfo mltrfo(ana.main_context(), opts.sub("transformations.ml_trfo"));
  mltrfo.square_pdf(true_trf.is_amplitude());

  AggregateTransformation<BootstrapTransformation<_Transfo>> bstrfo(ana.main_context(), "bstrfo", BootstrapTransformation<_Transfo>(ana.main_context(), "bstrf", trfo, nbsevents));
  bstrfo.compute_covariance(false);
  bstrfo.sub_transformation().poisson(do_poisson);

  SampleStats pull_ml(npar), pull_mc(npar), mean_ml(npar), mean_mc(npar), e_ml(npar), e_mc(npar), true_par(npar);
  SampleCovariance cov_ml(npar), cov_mc(npar);
  _Transf mc_trf, mc_trf_base, ml_trf;
  
  std::vector<TH1D*> parhi(npar, nullptr);
  if(plot_coeff) {
    for(unsigned p=0; p<npar; ++p) parhi[p] = new TH1D(Form("par%d%s",p,prefix.c_str()), Form("par%d%s",p,prefix.c_str()), 2000, is_amp ? -100 : -nevents, is_amp ? 100 : nevents);
  }
  for(unsigned i=0; i<nexperiments; ++i) {
    sample_random_1d(spl, _true_trf, do_poisson ? po(ana.main_context().rnd_gen()) : nevents, ana.main_context().rnd_gen());
    trfo.transform(ana.main_context(), create_transform_call(*spl, AllEventSel()), mc_trf_base);

    // ML
    ml_trf.clear();
    mltrfo.transform(ana.main_context(), create_transform_call(*spl, AllEventSel()), ml_trf);
    mean_ml += ml_trf.parameters();
    cov_ml += ml_trf.parameters();
    e_ml += ml_trf.uncertainties();
    for(unsigned p=0; p<npar; ++p) pull_ml[p] += (ml_trf.par(p) - true_trf.par(p))/ml_trf.par_unc(p);

    // MC
    bstrfo.transform(ana.main_context(), create_transform_call(*spl, AllEventSel()), mc_trf, &mc_trf_base);
    mean_mc += mc_trf.parameters();
    cov_mc += mc_trf.parameters();
    e_mc += mc_trf.uncertainties();
    for(unsigned p=0; p<npar; ++p) pull_mc[p] += (mc_trf.par(p) - true_trf.par(p))/mc_trf.par_unc(p);
    if(plot_coeff) {
      for(unsigned p=0; p<npar; ++p) parhi[p]->Fill(mc_trf.par(p));
    }
  }

  for(unsigned p=0; p<npar; ++p) true_par[p] += true_trf.par(p);

  std::cout<<"**** Cov ML ****"<<std::endl;
  cov_ml.compute();
  std::cout<<cov_ml.mean()<<std::endl;
  std::cout<<cov_ml.covariance()<<std::endl;

  std::cout<<"**** Cov MC ****"<<std::endl;
  cov_mc.compute();
  std::cout<<cov_mc.mean()<<std::endl;
  std::cout<<cov_mc.covariance()<<std::endl;

  {
    auto* can = create_canvas("PULL "+prefix, 500, 500);
    TH1D* hi = draw_hi(create_hi(pull_ml, "pull_ml_"+prefix, true), kBlack, 2.0, 20, "E1");
    hi->GetXaxis()->SetNdivisions(npar, 0, 0, true);
    hi->GetXaxis()->SetTitle("Eigenvector");
    hi->GetYaxis()->SetTitle("Coefficient pull");
    TH1D* himc = create_hi(pull_mc, "pull_mc_"+prefix, true);
    draw_hi(himc, kRed, 2.0, 22, "E1 SAME");
    set_visible_range_y(hi, -2, 2);
    draw_horizontal_line(hi, 1, 14);
    draw_horizontal_line(hi, 0, 14);
    draw_horizontal_line(hi, -1, 14);
    TLegend *leg = new TLegend(0.60,0.77, 0.93,0.93);
    leg->AddEntry(hi, "ML");
    leg->AddEntry(himc, "projection");
    leg->Draw();
    can->Update();
    can->Print(("plots/coeff_"+prefix+"_pull.pdf").c_str());
    if(plot_coeff) {
      for(unsigned p=0; p<npar; ++p) {
        auto* can = create_canvas("PAR "+std::to_string(p)+" "+prefix, 500, 500);
        parhi[p]->Draw("");
        parhi[p]->Fit("gaus");
      }
    }
  }
  {
    auto* can = create_canvas("MEAN "+prefix, 500, 500);
    TH1D* hi = draw_hi(create_hi(mean_ml, "mean_ml_"+prefix, true, true), kBlack, 2.0, 20, "E1");
    hi->GetXaxis()->SetNdivisions(npar, 0, 0, true);
    hi->GetXaxis()->SetTitle("Eigenvector");
    hi->GetYaxis()->SetTitle("Coefficient mean");
    TH1D* himc = create_hi(mean_mc, "mean_mc_"+prefix, true, true);
    draw_hi(himc, kRed, 2.0, 22, "E1 SAME");
    TH1D* hitrue = create_hi(true_par, "true_"+prefix, true, true);
    draw_hi(hitrue, kBlue, 2.0, 23, "E1 SAME");
    TLegend *leg = new TLegend(0.60,0.77, 0.93,0.93);
    leg->AddEntry(hi, "ML");
    leg->AddEntry(himc, "projection");
    leg->AddEntry(hitrue, "true");
    leg->Draw();
    can->Print(("plots/coeff_"+prefix+"_mean.pdf").c_str());
  }

  {
    auto* can = create_canvas("UNC "+prefix, 500, 500);
    can->SetLeftMargin(0.22);
    TH1D* hi = draw_hi(create_hi(e_ml, "e_ml_"+prefix, true, true), kBlack, 2.0, 20, "E1");
    hi->GetXaxis()->SetNdivisions(npar, 0, 0, true);
    hi->GetXaxis()->SetTitle("Eigenvector");
    hi->GetYaxis()->SetTitle("Coefficient std.dev.");
    hi->GetYaxis()->SetTitleOffset(1.8);
    TH1D* himc = create_hi(e_mc, "e_mc_"+prefix, true, true);
    draw_hi(himc, kRed, 2.0, 22, "E1 SAME");
    if(true_trf.is_amplitude()) set_visible_range_y(hi, 0.48, 0.52);
    else set_visible_range_y(hi, 60, 100.0);
    TLegend *leg = new TLegend(0.60,0.77, 0.93,0.93);
    leg->AddEntry(hi, "ML");
    leg->AddEntry(himc, "projection");
    leg->Draw();
    can->Print(("plots/coeff_"+prefix+"_unc.pdf").c_str());
  }
}

template<class _Transf, class _Transfo>
void
example_plot(Analysis& ana, Log& log, const std::string& prefix, Sample<1>* spl, const _Transf& trf, unsigned nevents, unsigned npdfbins, const _Transfo& trfo) {
  log_info(log, "example_plot() "<<prefix);
  sample_random_1d(spl, trf, nevents, ana.main_context().rnd_gen());
  log_info(log, "example_plot() I="<<trf.integral_pdf()<<" spl nevents="<<spl->events());
  compare_data_and_transform(ana.main_context(), prefix, " pdf", false, trf, *spl, npdfbins);

  _Transf trf2;
  trfo.transform(ana.main_context(), create_transform_call(*spl, AllEventSel()), trf2);
  log_info(log, "example_plot() trf2 I="<<trf2.integral_pdf());
  trf2.dump();
  TransformPlot1D(ana.main_context(), trf2).title("a2").npoints(npdfbins).color(kGreen).superimpose(true).plot();
}

void
coefficients(Options& opts) {
  const unsigned npdfbins = opts.get_as<unsigned>("npdfbins");
  unsigned nevents = opts.get_as<unsigned>("nevents");
  unsigned nexperiments = opts.get_as<unsigned>("nexperiments");
  unsigned nbsevents = opts.get_as<unsigned>("nbsevents");
  bool do_run_pseudo_experiments = opts.get_as<bool>("run_pseudo_experiments");
  bool do_lin = opts.get_as<bool>("do_lin");
  bool do_amp = opts.get_as<bool>("do_amp");

  Analysis ana("coeff");
  Log log(Log::get(ana.main_context(), "coefficiencts"));
  Sample<1>* s1 = ana.main_context().memory_pool().pool<Sample<1>>()->get(nevents, "x");
  auto vars =  std::make_shared<Variables>(1);
  vars->at(0).external_interval(-1, 1);
  s1->variables(vars);
  vars->lock();

  if(do_lin) {
    std::cout<<" ----- Linear transform -----"<<std::endl;
    PSTransf p_trans("p", true);
    p_trans.parameters() = {7.0, -2, 1.25, -2.77, 0.7};
    p_trans.normalize(1.0);
    p_trans.dump();
    p_trans.variables(s1->variables());
    example_plot(ana, log, "P", s1, p_trans, nevents, npdfbins, PSTransfo(ana.main_context(), "ps_p"));

    if(do_run_pseudo_experiments) {
      PSTransfo pstrfo(ana.main_context(), "ps_p");
      do_pseudo_experiments(ana, opts, "P", s1, nexperiments, pstrfo, p_trans, nevents, nbsevents);
    }
  }
  if(do_amp) {
    std::cout<<" ----- Sqrt transform -----"<<std::endl;
    PSTransf a_trans("a", true);
    a_trans.is_amplitude(true);
    a_trans.parameters() = {9.0, -1.0, -2.25, 2.1, 0.7};
    a_trans.normalize(nevents);
    a_trans.dump();
    a_trans.normalize(1.0);
    a_trans.dump();
    a_trans.variables(s1->variables());
    {
      APSTransfo apstrfo(ana.main_context(), "ps_a");
      example_plot(ana, log, "A", s1, a_trans, nevents, npdfbins, apstrfo);
    }

    if(do_run_pseudo_experiments) {
      APSTransfo apstrfo(ana.main_context(), "ps_a");
      do_pseudo_experiments(ana, opts, "A", s1, nexperiments, apstrfo, a_trans, nevents, nbsevents);
    }
  }
  s1->release();
}

DefineGeneralExample(coefficients, paperexamples);
