#include "example_util.h"
#include "tmva_util.h"
#include "MLTransformation.h"
#include "LatexTable.h"
#include "BinaryClassifier.h"
#include "full_system.h"
#include "sparse_system.h"
#include "PDF2DComparison.h"
#include "cross_entropy.h"
#include "sample_tools.h"
#include "transform_plots.h"

using namespace sfi;
using namespace sfi::bases;

const unsigned dims = 2;

using Eig1 = EigenVectors<dims, 0, EigenMonomicSparse>;
using PSTrafo = AmplitudeTransformation<ProjectionTransformation<Eig1, Cosine>, ttrue>;

using Transf = t_Transform<PSTrafo>;

struct options {
  bool run_tmva = false;
  unsigned nrocbins = 1000;
  bool compare_1d = true;
  bool do_plot_details = true;
  std::string file_prefix = "plots/";
};

void spiral_2D(OptionMap& op) {
  options opts;
  op.bind("run_tmva", opts.run_tmva).bind("nrocbins", opts.nrocbins);
  op.bind("compare_1d", opts.compare_1d).bind("do_plot_details", opts.do_plot_details);
  op.bind("file_prefix", opts.file_prefix);

  Analysis ana(op);
  Log log = Log::get(ana.main_context(), "spiral2d");
  
  auto samples = ana.channel_samples<2U, false>("spiral", true, false);

  PSTrafo pstr(ana.main_context(), op.sub("transformations.amp"));
  auto tres = perform_transformations(ana.main_context(), log, samples, pstr, EvenEventSel());
  Transf& str(tres.transforms[0]), &btr(tres.transforms[1]);

  if(opts.do_plot_details) {
    plot_amp_transform_details("Spiral 2D S", tres.results[0], op.get<std::string>("file_prefix"));
    plot_amp_transform_details("Spiral 2D B", tres.results[1], op.get<std::string>("file_prefix"));
  }

  // plot PDF:s for signal and background
  auto& s(samples[0]), &b(samples[1]);
  PDF2DComparison(ana.main_context(), op.sub("plots.cmp2d_s")).make_histogram(s, EvenEventSel()).transform(str).plot();
  PDF2DComparison(ana.main_context(), op.sub("plots.cmp2d_b")).make_histogram(b, EvenEventSel()).transform(btr).plot();

  if(op.get<bool>("show_transform_stats")) {
    transform_stats("S train", str, s, EvenEventSel());
    transform_stats("B train", btr, b, EvenEventSel());
    transform_stats("S test", str, s, OddEventSel());
    transform_stats("B test", btr, b, OddEventSel());
  }

  if(op.get<bool>("do_scan_ce")) {
    unsigned min_degrees = op.get_as<unsigned>("min_degrees");
    unsigned max_degrees = op.get_as<unsigned>("max_degrees");
    unsigned ce_parts = op.get_as<unsigned>("ce_parts");
    unsigned Ndeg = max_degrees - min_degrees;
    plot_cross_entropy(ana,log,s,"S",Ndeg,ce_parts,min_degrees,pstr);
  }
  if(op.get<bool>("run_classifier")) {
    auto classifier = make_binary_classifier(ana, str, btr, samples, EvenEventSel(), OddEventSel());
    classifier.n_roc_bins(opts.nrocbins).training_time(tres.time);
    if(opts.compare_1d) classifier.make_1D_transforms(PSTrafo(ana.main_context(), op.sub("transformations.amp")).speed(1.0).max_diff(0.1), samples);
    classifier.compute();

    auto bin_cls_plots = make_binary_classifier_plots(classifier, op.sub("plots.bc"));
    bin_cls_plots.make_plots();

    LatexTable& table = classifier.summary_table();
    table.rows(5).row_title(1, "MLP").row_title(2, "BDT").row_title(3, "LD").row_title(4, "LDD");

    LatexTable& eff_table = classifier.efficiency_table();
    eff_table.rows(5).row_title(1, "MLP").row_title(2, "BDT").row_title(3, "LD").row_title(4, "LDD");

    // TMVA
    if(opts.run_tmva) {
      // ***********
      // Stolen from TMVA example!
      // ***********
      TFile* outputFile = TFile::Open("tmva_cmcclass.root", "RECREATE");
      TTree* sigtree = sample_2_tree(s);
      TTree* bgtree = sample_2_tree(b);

      TMVA::Tools::Instance();
      TMVA::Factory *factory = new TMVA::Factory("TMVAClassification", outputFile, "!V:!Silent:Color:DrawProgressBar:Transformations=I;D;P;G,D:AnalysisType=Classification" );
      TMVA::DataLoader* loader = new TMVA::DataLoader("data");

      loader->AddVariable( "x", 'D' );
      if(dims > 1) loader->AddVariable( "y", 'D' );

      loader->AddSignalTree(sigtree, 1.0);
      loader->AddBackgroundTree(bgtree, 1.0);

      loader->PrepareTrainingAndTestTree("", "", "nTrain_Signal=0:nTrain_Background=0:SplitMode=Random:NormMode=NumEvents:!V" );

      loader->Print();

      factory->BookMethod(loader, TMVA::Types::kLD, "LD", "H:!V:VarTransform=None:CreateMVAPdfs:PDFInterpolMVAPdf=Spline2:NbinsMVAPdf=50:NsmoothMVAPdf=10" );
      factory->BookMethod(loader, TMVA::Types::kLD, "LDD", "H:!V:VarTransform=Decorrelate:CreateMVAPdfs:PDFInterpolMVAPdf=Spline2:NbinsMVAPdf=50:NsmoothMVAPdf=10" );
      factory->BookMethod(loader, TMVA::Types::kBDT, "BDT", "!H:!V:NTrees=200:MinNodeSize=2.5%:MaxDepth=4:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=20" );
      factory->BookMethod(loader, TMVA::Types::kMLP, "MLP", "H:!V:NeuronType=tanh:VarTransform=N:NCycles=250:HiddenLayers=N+5,N+3:TestRate=5:!UseRegulator" );

      factory->TrainAllMethods();
      factory->TestAllMethods();
      factory->EvaluateAllMethods();

      TMVA::MethodBase* mld = dynamic_cast<TMVA::MethodBase*>(factory->GetMethod("data", "LD"));
      TMVA::MethodBase* mldd = dynamic_cast<TMVA::MethodBase*>(factory->GetMethod("data", "LDD"));
      TMVA::MethodBase* mbdt = dynamic_cast<TMVA::MethodBase*>(factory->GetMethod("data", "BDT"));
      TMVA::MethodBase* mmlp = dynamic_cast<TMVA::MethodBase*>(factory->GetMethod("data", "MLP"));

      copy_tmva_efficiencies(eff_table, mmlp, 1);
      copy_tmva_efficiencies(eff_table, mbdt, 2);
      copy_tmva_efficiencies(eff_table, mld, 3);
      copy_tmva_efficiencies(eff_table, mldd, 4);

      copy_tmva_summary(table, 1, mmlp, factory, "MLP");
      copy_tmva_summary(table, 2, mbdt, factory, "BDT");
      copy_tmva_summary(table, 3, mld, factory, "LD");
      copy_tmva_summary(table, 4, mldd, factory, "LDD");

      delete factory;
      delete loader;

      outputFile->Close();
    }
    TFile* inFile = TFile::Open("tmva_cmcclass.root");

    if(inFile) {
      plot_tmva_results(bin_cls_plots, inFile, false);
      plot_tmva_results(bin_cls_plots, inFile, true);
      inFile->Close();
    }
    else {
      log_warning(log, "No TMVA input file found!");
    }
    std::cout<<table<<std::endl;
    std::cout<<eff_table<<std::endl;
  }
}

DefineGeneralExample(spiral_2D,paperexamples)
