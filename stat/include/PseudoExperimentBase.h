#ifndef sfi_PseudoExperimentBase_h
#define sfi_PseudoExperimentBase_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Sample.h"
#include "Variables.h"
#include "Analysis.h"
#include "stat.h"
#include "ITransform.h"
#include "ILikelihoodMaximizer.h"
#include "timer.h"
#include "ISampleBuilder.h"

namespace sfi {
  struct experiment_result {
    bool status;
    TransformSet<ITransform> data_transform;
    Quantities params, aux_params;
  };

  class PseudoExperimentBase {
  public:
    using This = PseudoExperimentBase;

    virtual ~PseudoExperimentBase() {}

    struct CaseStat {
      CaseStat(unsigned nparams);

      ~CaseStat();

      std::vector<SampleStat> param;
      std::vector<SampleStat> param_reldiff;
      std::vector<SampleStat> param_e;
      std::vector<SampleStat> pull_param;
      std::map<std::string, SampleStat> aux_param;

      std::vector<Histogram<true, double>> param_hi;
      std::vector<Histogram<true, double>> param_reldiff_hi;
      std::vector<Histogram<true, double>> param_e_hi;
      std::vector<Histogram<true, double>> param_pull_hi;

      SampleStat rtime;
      Histogram<true, double> rtime_hi;
      unsigned nerrors;
      unsigned noutliers;
      bool m_initialized;
    };

    struct CaseResult {
      CaseResult(unsigned nparams);
      std::vector<Qty> param;
      std::vector<Qty> param_reldiff;
      std::vector<Qty> param_e;
      std::vector<Qty> param_pull;
      Quantities aux_param;
      Qty rtime;
    };

    struct PECaseBase {
      PECaseBase(const std::string& n, size_t nparams, int mcol = 1, int mstyle = 20);

      virtual ~PECaseBase() {}

      // ---- interface ----

      virtual experiment_result run(Context& ctx, SampleMap) = 0;

      // ---- public methods ----

      inline bool debug() const { return this->m_debug; }
      inline PECaseBase& debug(bool b) { this->m_debug = b; return *this; }

      inline const std::string& title() const { return this->m_title; }
      inline PECaseBase& title(const std::string& ti) { this->m_title = ti; return *this; }

      inline PECaseBase& par_limits(unsigned np, double min, double max) {
        this->m_par_limits[np] = std::make_pair(min, max); return *this;
      }

      inline double par_max(unsigned p) { return m_par_limits[p].second; }

      inline double par_min(unsigned p) { return m_par_limits[p].first; }

      void init(size_t n_param_points, size_t nparams);

      inline const std::string& name() const { return m_name; }

      inline CaseStat& statistics(size_t param_point) { return m_statistics[param_point]; }

      inline CaseResult& result(size_t param_point) { return m_result[param_point]; }

      inline int marker_color() const { return m_marker_color; }
      inline void marker_color(int col) { m_marker_color = col; }

      inline int marker_style() const { return m_marker_style; }
      inline void marker_style(int st) { m_marker_style = st; }
    protected:
      std::string m_name, m_title;
      std::vector<CaseStat> m_statistics;
      std::vector<CaseResult> m_result;
      std::vector<std::pair<double, double>> m_par_limits;
      // plot options
      int m_marker_color;
      int m_marker_style;
      bool m_debug;
    };

    /** Set/get number of experiments per point */
    inline This& nexperiments(unsigned ne) { m_nexperiments = ne; return *this; }
    inline unsigned nexperiments() const { return m_nexperiments; }

    /** @return number of experiment setups */
    inline unsigned nexperiment_setups() const { return m_experiments.size(); }

    /** @return Name of experiment setup */
    inline const std::string& experiment_setup_name(unsigned i) const { return m_experiments[i]; }

    /** @return Number of params */
    inline unsigned nparams() const { return m_param_values.cols(); }

    /** @return Number of param values */
    inline unsigned nparam_values() const { return m_param_values.rows(); }

    /** @return parameter values for given index */
    inline const double* param_value(unsigned idx) const { return m_param_values.row_data(idx); }

    /** @return Param name */
    inline const std::string& param_name(unsigned idx) const { return m_parameter_names[idx]; }

    /** If histograms or simple mean/rms should be used */
    inline bool use_histograms() const { return m_use_histograms; }
    inline This& use_histograms(bool b) { m_use_histograms = b; return *this; }

    /** If time statistics should be gathered */
    inline bool time_statistics() const { return m_time_statistics; }
    inline This& time_statistics(bool b) { m_time_statistics = b; return *this; }

    /** If general statistics should be collected */
    inline bool collect_statistics() const { return m_collect_statistics; }
    inline This& collect_statistics(bool b) { m_collect_statistics = b; return *this; }

    /** If mean unc or sigma should be reported */
    inline bool mean_unc() const { return m_mean_unc; }
    inline This& mean_unc(bool b) { m_mean_unc = b; return *this; }

    inline This& file_name_prefix(const std::string& fname_prefix) { m_file_name_prefix = fname_prefix; return *this; }
    inline const std::string& file_name_prefix() const { return m_file_name_prefix; }

    /** @return Number of cases */
    inline unsigned ncases() const { return m_cases.size(); }

    /** @return Numbered test case */
    inline PECaseBase& test_case(unsigned idx) { return *m_cases[idx]; }
    inline const PECaseBase& test_case(unsigned idx) const { return *m_cases[idx]; }

    /** Max value for parameter unertainty, i.e. upper limit in histogram */
    inline This& par_unc_max(double m) { m_par_unc_max = m; return *this; }
    inline double par_unc_max() const { return m_par_unc_max; }

    /** Run the experiments */
    void run();
  protected:
    PseudoExperimentBase(Analysis& ana, const OptionMap& oppts);

    void init_parameters();

    void add_case(PECaseBase* _case);

    inline unsigned get_poisson_events(unsigned ne) {
      std::poisson_distribution<> po(ne);
      return po(m_analysis.main_context().rnd_gen());
    }

    SampleMap create_sample(const std::string& exname);

    void create_histograms(const std::string& suf, CaseStat& stat, PECaseBase& cas);

    bool collect_statistics(const experiment_result& eres, unsigned k, const std::string& suf,  PECaseBase& cas);

    void compute_results(CaseStat& stat, CaseResult& res, PECaseBase& cas);

    Log m_log;
    unsigned m_nexperiments;
    Matrix<double> m_param_values;
    Analysis& m_analysis;
    OptionMap m_options;
    std::string m_channel;
    double m_par_unc_max;
    double m_outlier_limit;
    std::string m_file_name_prefix;
    std::vector<PECaseBase*> m_cases;
    std::string m_experiment_set;
    std::vector<std::string> m_experiments, m_parameter_names, m_aux_params;
    bool m_use_histograms;
    bool m_time_statistics;
    bool m_collect_statistics;
    bool m_mean_unc; // if mean unc or sigma should be reported
  };
}

#endif // sfi_PseudoExperimentBase_h
