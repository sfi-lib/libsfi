#ifndef sfi_SlicePDF_h
#define sfi_SlicePDF_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <ParametrizedPDF.h>

namespace sfi {
  /**
   * @class SlicePDF
   *
   * This is a pdf based on a Transform that is sliced in two parts, with the parameter d2. The left slice is the phase space part, and
   * the right slice is the parametrized part.
   * An option is expected for each parametrized dimension, which is the variable name mapped to a parameter value.
   */
  class SlicePDF : public ParametrizedPDF {
  public:
    SlicePDF(Normalization norm, unsigned d2, const ITransform& trf);

    SlicePDF(unsigned d2, const ITransform& trf, const OptionMap& opts);

    virtual ~SlicePDF();

    // ---- Implementation of IPDF ----

    virtual IEvaluator* get_evaluator();

    virtual const ITransform* get_transform();

    virtual void do_update_parameters(EvalPoint ep);

    virtual double do_eval(const double* x, const std::string& var, bool shift_down);

    virtual bool do_init_parameters(const OptionMap& opts, Parameters& pars);
  protected:
    std::unique_ptr<IEvaluator> m_eval;
  };

  inline auto slice_pdf(const ITransform& trf, unsigned d2, const OptionMap& opts) {
    return new SlicePDF(d2, trf, opts);
  }
  inline auto slice_pdf(const ITransform& trf, unsigned d2, Normalization norm) {
    return new SlicePDF(norm, d2, trf);
  }
}

#endif /* sfi_SlicePDF_h */
