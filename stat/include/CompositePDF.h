#ifndef sfi_pdf_h
#define sfi_pdf_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <PDFBase.h>

namespace sfi {
  /**
   * @class CompositePDF
   * Create a marked Poisson model
   * (sum N_i p_i(x) ) / sum N_i
   */
  class CompositePDF : public PDFBase {
  public:
    CompositePDF();

    CompositePDF(const std::initializer_list<PDFBase*>& pdfs);

    virtual ~CompositePDF();

    void add(PDFBase* pdf);

    inline size_t size() const { return m_pdfs.size(); }

    inline PDFBase* at(size_t n) const { return m_pdfs[n]; }

    // ---- Implementation of IPDF ----

    virtual double get_N(EvalPoint ep, const std::string& var, bool shift_down);

    virtual IEvaluator* get_evaluator();

    virtual const ITransform* get_transform();

    virtual void do_update_parameters(EvalPoint ep);

    virtual void do_print(std::ostream&);

    // ---- Implementation of PDFBase ----

    virtual bool is_dependent_on_variant(const std::string&) const;

    virtual double do_eval(const double*x, const std::string& var, bool shift_down);

    virtual bool do_init_parameters(const OptionMap& opts, Parameters&);

    virtual void get_pdfs(std::vector<IPDF*>& pd);
  protected:
    std::vector<PDFBase*> m_pdfs;
  };

  template<class ... _PDFs>
  inline auto* composite_pdf(_PDFs*... pdfs) { return new CompositePDF({pdfs...}); }
}

#endif // sfi_pdf_h
