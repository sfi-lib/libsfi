#ifndef sfi_BootstrapTransformation_h
#define sfi_BootstrapTransformation_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Transformation.h"
#include "MultiTransform.h"
#include "TransformCall.h"

namespace sfi {
  template<class _Trafo>
  class BootstrapTransformation : public NestedTransformation<_Trafo> {
  public:
    using This = BootstrapTransformation<_Trafo>;
    using Base = NestedTransformation<_Trafo>;
    using SubTransf = t_Transform<_Trafo>;
    using Transf = MultiTransform<SubTransf>;
    using Eigenv = t_EigenVectors<Transf>;

    BootstrapTransformation(Context& ctx, const std::string& name, unsigned nrepeats = 0, unsigned nevents = 0):
      Base(ctx, name, _Trafo(name)), m_nrepeats(nrepeats), m_poisson(false), m_ignore_fails(false), m_nevents(nevents) {
      bind_options();
    }

    BootstrapTransformation(Context& ctx, const std::string& name, const _Trafo& trafo, unsigned nrepeats = 0, unsigned nevents = 0):
      Base(ctx, name, trafo), m_nrepeats(nrepeats), m_poisson(false), m_ignore_fails(false), m_nevents(nevents) {
      bind_options();
    }

    BootstrapTransformation(const BootstrapTransformation& o):
      Base(o), m_nrepeats(o.m_nrepeats), m_poisson(o.m_poisson), m_ignore_fails(o.m_ignore_fails), m_nevents(o.m_nevents) {
      bind_options();
    }

    BootstrapTransformation(Context& ctx, OptionMap& opts):
      Base(ctx, opts), m_nrepeats(0), m_poisson(false), m_ignore_fails(false), m_nevents(0) {
      bind_options();
    }

    inline unsigned nrepeats() const { return m_nrepeats; }
    inline This& nrepeats(unsigned r) { m_nrepeats = r; return *this; }

    inline unsigned nevents() const { return m_nevents; }
    inline This& nevents(unsigned r) { m_nevents = r; return *this; }

    inline This& poisson(bool b) { m_poisson = b; return *this; }
    inline bool poisson() const { return m_poisson; }

    inline This& ignore_fails(bool b) { m_ignore_fails = b; return *this; }
    inline bool ignore_fails() const { return m_ignore_fails; }

    template<class ..._T, class _Transf>
    bool transform(Context& ctx, const TransformCall<_T...>& ctrf, _Transf& res) const {
      static_assert((c_is_valid_transform<This, _Transf>()), "Invalid Transform type");
      using SampleTy = t_Sample<TransformCall<_T...>>;
      res.clear();

      const SampleTy& spl = ctrf.sample();
      auto* spl_pool = ctx.memory_pool().pool<SampleTy>();

      // TODO: the MultiTransform needs to be initialized
      // this->init_transform(res, &ctrf.sample());
      unsigned nerr(0);
      auto* s0 = spl_pool->get(spl.events(), spl.name());
      spl.select_events(*s0, ctrf.event_selector());

      auto* s1 = spl_pool->get(m_nevents ? m_nevents : s0->events(), spl.name());

      TransformCall<_T...> _ctrf_bs(ctrf, *s1);
      auto ctrf_bs = _ctrf_bs.change_event_selector(AllEventSel());
      Random<t_ContextRandom<Context>> rnd(ctx.rnd_gen());
      for(unsigned i=0; i<m_nrepeats; ) {
        if(m_nevents) {
          if(m_poisson) s0->bootstrap_subset(*s1, rnd, AllEventSel(), rnd.poisson(m_nevents));
          else s0->bootstrap_subset(*s1, rnd, AllEventSel(), m_nevents);
        }
        else s0->bootstrap(*s1, rnd, m_poisson);

        auto* trf = this->m_sub_transformation.create_transform(ctx);
        this->init_transform(*trf, s1);
        trf->variables(spl.variables());
        if(ctrf_bs.transform(ctx, this->m_sub_transformation, *trf) || m_ignore_fails) {
          res.transforms().add(trf);
          ++i;
        }
        else {
          nerr++;
          // TODO: reuse instance
          delete trf;
        }
      }
      s1->release();
      s0->release();
      // TODO: post processing
      if(nerr) {
        do_log_warning("bootstrap_transform() nerrors: "<<nerr);
      }
      return true;
    }
  protected:
    void bind_options() {
      this->m_options.bind("nrepeats", m_nrepeats);
      this->m_options.bind("poisson", m_poisson);
      this->m_options.bind("ignore_fails", m_ignore_fails);
      this->m_options.bind("nevents", m_nevents);
    }
    unsigned m_nrepeats;
    bool m_poisson;
    bool m_ignore_fails;
    unsigned m_nevents;
  };
}
#endif // sfi_BootstrapTransformation_h
