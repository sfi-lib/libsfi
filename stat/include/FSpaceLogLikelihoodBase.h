#ifndef sfi_FSpaceLogLikelihoodBase_h
#define sfi_FSpaceLogLikelihoodBase_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ExtendedLogLikelihood.h"

namespace sfi {
  class FSpaceLogLikelihoodBase : public LogLikelihoodBase {
  public:
    FSpaceLogLikelihoodBase(const ITransform& trf_model, const ITransform& trf_data, std::unique_ptr<IEvaluator> eval);
    
    virtual ~FSpaceLogLikelihoodBase();
    
    // ---- implementation of ILikelihood ----
        
    virtual void get_pdfs(std::vector<IPDF*>&);
    
    virtual void do_print(std::ostream&) const;

    // ---- implementation if LogLikelihoodBase ----
    
    virtual bool is_dependent_on_variant(const std::string&) const;
    
    virtual void do_update_parameters(EvalPoint);
    
    virtual double do_evaluate(Context&, const std::string&, bool);
    
    virtual bool do_init_parameters(const OptionMap& opts, Parameters& pars);
  protected:
    /** Set slice param values, return the transform or null */
    ITransform* set_slice_params();
    
    const ITransform& m_trf_model;
    const ITransform& m_trf_data;
    Parameter* m_p_N;
    std::vector<Parameter*> m_params;
    std::vector<double> m_values;
    std::unique_ptr<IEvaluator> m_eval_model;
  };
}

#endif /* sfi_FSpaceLogLikelihoodBase_h */
