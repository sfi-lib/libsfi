#ifndef sfi_FSpaceROPELogLikelihood_h
#define sfi_FSpaceROPELogLikelihood_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <ExtendedLogLikelihood.h>
#include <MatrixOp.h>

namespace sfi {
  /**
   * @TODO: This can be split in several parts and made into PDF:s
   * @TODO: Code stolen from ParamtrizedPDF - Make base of FSpacePDF
   */
  class FSpaceROPELogLikelihood : public LogLikelihoodBase {
  public:
    /**
     * @param nd Number of phase space dims
     * @param trf_model The transform for the model, must have
     */
    FSpaceROPELogLikelihood(unsigned nd, const ITransform& trf_model, const ITransform& trf_data, ITransform* trf_bg = 0);
    
    inline const SampleStat& nvalues() const { return m_s_nvalues; }
    inline const SampleStat& trS_orig() const { return m_s_Tr_orig; }
    inline const SampleStat& trS_reg() const { return m_s_Tr_reg; }
    
    // ---- implementation of ILikelihood ----
    
    virtual double do_eval(Context&);
    
    virtual bool do_init_parameters(const OptionMap& opts, Parameters& pars);
    
    virtual void get_pdfs(std::vector<IPDF*>&);
    
    virtual void do_print(std::ostream&) const;
    
    // ---- implementation if LogLikelihoodBase ----
    
    virtual bool is_dependent_on_variant(const std::string&) const;
    
    virtual void do_update_parameters(EvalPoint);
    
    virtual double do_evaluate(Context&, const std::string&, bool);
  protected:
    std::unique_ptr<IEvaluator> m_eval_model;
    const ITransform& m_trf_model;
    const ITransform& m_trf_data;
    ITransform* m_trf_bg;
    
    Parameter* m_p_rho;
    Parameter* m_p_N;
    Parameter* m_p_Nb;
    std::vector<Parameter*> m_params;
    std::vector<double> m_values;
    MatrixSym<double> m_cov;
    Vector<double> m_cov_eigenv;
    MatrixSymEigenDecomposition m_eigen_decomp;
    MatrixSym<double> m_inv_cov;
    SampleStat m_s_nvalues;
    SampleStat m_s_Tr_orig;
    SampleStat m_s_Tr_reg;
  };
}
#endif /* sfi_FSpaceROPELogLikelihood_h */
