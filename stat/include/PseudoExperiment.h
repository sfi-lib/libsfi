#ifndef sfi_PseudoExperiment_h
#define sfi_PseudoExperiment_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "PseudoExperimentBase.h"

namespace sfi {
  template<class _PSTrans>
  class PseudoExperiment : public PseudoExperimentBase {
  public:
    using This = PseudoExperiment<_PSTrans>;
    using Base = PseudoExperimentBase;
    using SampleTy = Sample<_PSTrans::dims()>;

    using RunMethod = experiment_result(Context&, const SampleTy&);

    using RunMultiMethod = experiment_result(Context&, SampleMap);

    struct PECase : public PECaseBase {
      PECase(const std::function<RunMethod>& m, const std::string& n, unsigned nparams, int mcol = 1, int mstyle = 20):
        PECaseBase(n, nparams, mcol, mstyle), method(m) {
      }

      virtual ~PECase() {}

      virtual experiment_result run(Context& ctx, SampleMap spls) {
        experiment_result eres;
        eres.status = false;
        SampleTy* spl = dynamic_cast<SampleTy*>(spls[channel]);
        if(!spl) return eres;
        eres = this->method(ctx, *spl);
        return eres;
      }

      std::function<RunMethod> method;
      std::string channel;
    };

    struct PEMultiCase : public PECaseBase {
      PEMultiCase(const std::function<RunMultiMethod>& m, const std::string& n, unsigned nparams, int mcol = 1, int mstyle = 20):
        PECaseBase(n, nparams, mcol, mstyle), method(m) {
      }

      virtual ~PEMultiCase() {}

      virtual experiment_result run(Context& ctx, SampleMap spls) {
        return this->method(ctx, std::move(spls));
      }

      std::function<RunMultiMethod> method;
    };

    PseudoExperiment(Analysis& ana, const OptionMap& opts):Base(ana, opts) { }

    virtual ~PseudoExperiment() {}

    /**
     * Add a pseudo experiment test case.
     * @param name The name of the case
     * @param f What to to for each experiment
     */
    inline PECase& add_case(const std::string& name, const std::function<RunMethod>& f, int mcol = 1, int mstyle = 20) {
      PECase *c =  new PECase(f, name, nparams(), mcol, mstyle);
      c->channel = m_channel;
      Base::add_case(c);
      return *c;
    }
    inline PEMultiCase& add_case(const std::string& name, const std::function<RunMultiMethod>& f, int mcol = 1, int mstyle = 20) {
      PEMultiCase *c =  new PEMultiCase(f, name, nparams(), mcol, mstyle);
      Base::add_case(c);
      return *c;
    }
  };
}

#endif // sfi_PseudoExperiment_h
