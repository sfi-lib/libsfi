#ifndef sfi_SystExtrapolation_h
#define sfi_SystExtrapolation_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Matrix.h"

namespace sfi {
  /**
   * Interpolation/extrapolation of normalization systematics
   * implemented as in CERN-OPEN-2012-016.
   * @param Im Relative normalization at alpha=-1
   * @param Ip Relative normalization at alpha=1
   * @return lambda that takes alpha as const double* and returns extra/interpolated normalization
   */
  std::function<double(double)> norm_syst_extrap(double Im, double Ip);

  class SystExtrapolation {
  public:
    SystExtrapolation();
    void update(double Im, double Ip);
    double operator()(double z) const;
  protected:
    double m_Im, m_Ip;
    Vector<double> m_x, m_v;
  };

  /**
   * Inter- and extrapolation, typically used on a log likelihood:
   *
   * Let I_m = L_m/L_0 and I_p = L_p/L_0
   *
   * L = L_0 * N(Im,alpha,Ip)
   *
   * N (alpha<-1): (L_0 + (L_0 - L_m) alpha)/L_0 = (L_0(1+alpha) - L_m alpha)/L_0 = 1+alpha - L_m/L_0 alpha = 1 + alpha(1-I_m)
   * N (alpha>1) : (L_0 + (L_p - L_0) alpha)/L_0 = (L_0(1-alpha) + L_p alpha)/L_0 = 1-alpha + L_p/L0 alpha = 1 + alpha(I_p - 1)
   * N (-1 -> 1) : 1 + sum(i=1->4) a_i alpha^i
   *
   * The coefficients are given by the four conditions at -1 and 1, for N and dN/d alpha.
   *
   * N(-1) = L_m/L0 = I_m
   * N(1)  = L_p/L0 = I_p
   * N'(-1)= 1 - L_m/L_0 = 1 - I_m
   * N'(1) = -1 + L_p/L_0 = I_p - 1
   *
   */
  class LogSystExtrapolation {
  public:
    LogSystExtrapolation();
    void update(double Im, double Ip);
    double operator()(double z) const;
  protected:
    double m_Im, m_Ip;
    Vector<double> m_x, m_v;
  };
}

#endif // sfi_SystExtrapolation_h
