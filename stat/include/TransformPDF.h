#ifndef sfi_TransformPDF_h
#define sfi_TransformPDF_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <PDFBase.h>

namespace sfi {
  /**
   * @class TransformPDF
   *
   * Pdf that doesn't depend on any parameter. May have a floting normalization.
   */
  class TransformPDF : public PDFBase {
  public:
    TransformPDF(const ITransform& trf, Normalization norm);

    TransformPDF(const ITransform& trf, const OptionMap& topts);

    virtual ~TransformPDF();

    // ---- Implementation of IPDF ----

    virtual IEvaluator* get_evaluator();

    virtual const ITransform* get_transform();

    virtual double do_eval(const double* x, const std::string& var, bool shift_down);

    virtual void get_pdfs(std::vector<IPDF*>& pd);
  protected:
    std::unique_ptr<IEvaluator> m_eval;
  };

  inline auto transform_pdf(const ITransform& trf, Normalization norm) { return new TransformPDF(trf, norm); }
}

#endif /* sfi_TransformPDF_h */
