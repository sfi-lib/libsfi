#ifndef sfi_ModelPDF_h
#define sfi_ModelPDF_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <ParametrizedPDF.h>
#include <IModelTransform.h>

namespace sfi {
  /**
   * @class ModelPDF
   *
   * Using a IModelTransform a pdf can be defined that
   * may be parametrized and depend on any number of
   * systematic uncertainties/variants.
   *
   * Instances are expected to be used in an extended ML,
   * so the normalization is handled separately,
   * the details is left to the using class
   * (like CompositePDF or ExtendedLogLikelihood).
   *
   * Expects a parameter "nu_<VARIANT>" for each variant.
   */
  class ModelPDF : public ParametrizedPDF {
  public:
    ModelPDF(const IModelTransform& trf, const OptionMap& opts);

    virtual ~ModelPDF();

    // ---- Implementation of IPDF ----

    virtual IEvaluator* get_evaluator();

    virtual const ITransform* get_transform();

    virtual void do_update_parameters(EvalPoint ep);

    // ---- Implementation of PDFBase ----

    virtual double do_eval(const double* x, const std::string& var, bool shift_down);

    virtual bool do_init_parameters(const OptionMap& opts, Parameters& pars);
  protected:
    std::unique_ptr<IModelEvaluator> m_eval;
  };

  /**
   * Construct a pdf from a ModelTransform
   */
  inline ModelPDF* model_pdf(const IModelTransform& ss, const OptionMap& opts) {
    return new ModelPDF(ss, opts);
  }
}

#endif /* sfi_ModelPDF_h */
