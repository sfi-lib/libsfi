#ifndef sfi_SparseTransform_h
#define sfi_SparseTransform_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ITransform.h"
#include "Transform.h"
#include "util.h"
#include "defs.h"

#include <unordered_map>
#include <tuple>

namespace sfi {

  template<class _Eigen, class _TType> class SparseTransform;

  template<class _Eigen, class _TType>
  class Evaluator<SparseTransform<_Eigen, _TType>> : public IEvaluator {
  public:
    using Transf = SparseTransform<_Eigen, _TType>;

    Evaluator(const Transf* _me, double scale, bool external_coordinates = false):
      m_me(_me), m_scale(scale), m_external_coordinates(external_coordinates) { }

    virtual ~Evaluator() { }

    // ---- implementation of IEvaluator ----

    virtual double get_value(const double* x) { return (*this)(x); }

    virtual const ITransform* get_transform() { return m_me; }

    virtual void set_scale(double scale) { m_scale = scale; }

    virtual void set_external_coordinates(bool ec) { m_external_coordinates = ec; }

    virtual unsigned get_dimensions() const { return _Eigen::dims(); }

    virtual bool do_eval(unsigned npts, const double* xv, double* res) {
      const double* x = xv;
      for(unsigned i=0; i<npts; ++i) {
        res[i] = (*this)(x);
        x += _Eigen::dims();
      }
      return true;
    }

    virtual const Variable& get_variable(unsigned d) const {
      // TODO : protect
      return m_me->variables()->at(d);
    }

    virtual IEvaluator* get_clone(bool) const { return 0; }

    virtual unsigned get_nslice_parameters() const { return 0; }

    virtual void set_slice(const double *) {}

    virtual const ITransform* get_slice_transform() { return m_me; }

    // ---- public methods ----

    inline const Transf* transform() { return m_me; }

    inline double operator()(const double* x) {
      if(m_external_coordinates) {
            double xi[_Eigen::dims()];
            double w = m_me->variables()->transform(x, xi);
            return w*m_scale*m_me->pdf(xi);
          }
      else {
        return m_scale*m_me->pdf(x);
      }
    }

    template<class _InMatrix, class _OutMatrix>
    void eval_all(const _InMatrix& in, _OutMatrix& out, size_t out_idx) {
      size_t N(in.size());
      for(size_t i=0; i<N; ++i) out[i][out_idx] = (*this)(in[i].data());
    }
  protected:
    const Transf* m_me;
    double m_scale;
    bool m_external_coordinates;
  };

  template<class _Eigen, class _TType>
  class ParamIterator<SparseTransform<_Eigen, _TType>> : public IParamIterator {
  public:
    using Transf = SparseTransform<_Eigen, _TType>;
    ParamIterator(const Transf* tr):
      m_trans(tr), m_i_b(tr->m_par.begin()), m_i_e(tr->m_par.end()) {
    }

    virtual ~ParamIterator() { }

    // ---- implementation of IParamIterator ----

    virtual bool do_next() { return this->next(); }

    virtual unsigned get_degree(unsigned d) { return this->m_i_b->second.idx[d]; }

    virtual Qty get_value() { return Qty(this->m_i_b->second.c, this->m_i_b->second.c_unc); }

    // ---- public methods ----

    inline bool next() {
      m_i_b++;
      return m_i_b != m_i_e;
    }

    inline double par() { return this->m_i_b->second.c; }

    inline double par_unc() { return this->m_i_b->second.c_unc; }

    inline const typename Transf::Eigenv::Iterator::Indices::DataType& indices() const {
      return this->m_i_b->second.idx;
    }

    inline unsigned index(unsigned d) const { return this->m_i_b->second.idx[d]; }

    inline unsigned number() const { return this->m_i_b->first; }
  protected:
    const Transf* m_trans;
    typename Transf::CoefficientSet::const_iterator m_i_b, m_i_e;
  };

  template<class _T1, class _T2>
  class SparseTransform : public TransformVariableBase {
  protected:
    template<class _T> friend class ParamIterator;

    using BaseTransf = Transform<_T1, _T2>;
    using EigenSys = t_EigenSystem<BaseTransf>;
    using Indices = t_EigenVectorsData<BaseTransf>;
    using Impl = t_Basis<BaseTransf>;
    struct CoefficientData {
      CoefficientData(const Indices& _idx, double _c, double _c_unc):
        idx(_idx), c(_c), c_unc(_c_unc) {
      }
      CoefficientData():c(0.0), c_unc(0.0) {
        idx.fill(0);
      }
      Indices idx;
      double c;
      double c_unc;
    };
    using This = SparseTransform<_T1, _T2>;
    using Base = TransformVariableBase;
    using CoefficientSet = std::unordered_map<unsigned, CoefficientData>;
  public:
    using Eigenv = t_EigenVectors<BaseTransf>;

    SparseTransform():Base("") {}

    SparseTransform(const std::string& name):Base(name) {}

    SparseTransform(const Eigenv& eigen):Base(""), m_eigenvectors(eigen) { }

    SparseTransform(const This& o):Base(o), m_eigenvectors(o.m_eigenvectors), m_par(o.m_par) { }

    SparseTransform(This&& o):Base(std::forward<This>(o)), m_eigenvectors(o.m_eigenvectors), m_par(std::move(o.m_par)) { }

    virtual ~SparseTransform() { }

    // ---- public methods ----

    inline void set(unsigned k, double c, double c_unc, const Indices& idx) {
      m_par.emplace(std::piecewise_construct, std::forward_as_tuple(k), std::forward_as_tuple(idx, c, c_unc));
    }

    // ---- implementation of Transform ----

    inline size_t nparams() { return m_eigenvectors.params(); }

    // TODO:
    static constexpr inline unsigned dims() { return Eigenv::dims(); }

    inline unsigned npar() { return m_eigenvectors.params(); }

    inline const Eigenv& eigenvectors() const { return m_eigenvectors; }

    inline This& compute_covariance(bool b) { m_compute_covariance = b; return *this; }

    inline This& compute_uncertainty(bool b) { m_compute_uncertainty = b; return *this; }

    inline double& par(size_t i) {
      assert(i < npar() && "Invalid parameter id");
      return m_par[i].c;
    }

    inline double& par_unc(size_t i) {
      assert(i < npar() && "Invalid parameter id");
      return m_par[i].c_unc;
    }

    inline double par_sign(size_t i) {
      assert(i < npar() && "Invalid parameter id");
      auto& p = m_par[i];
      return sfi::abs(p.c/p.c_unc);
    }

    inline double par_cov(size_t , size_t ) const { return 0.0; }

    /** Multiply all parameter by s */
    void scale(double s) {
      for(auto& p : m_par) {
        p.second.c *= s;
        p.second.c_unc *= s;
      }
    }

    inline void clear() {}

    inline const std::string& type_name() const {
      static std::string s_type_name(sfi::type_name<This>());
      return s_type_name;
    }

    std::unique_ptr<ParamIterator<This>> param_iterator() const {
      return std::unique_ptr<ParamIterator<This>>(new ParamIterator<This>(this));
    }

    inline double pdf(const double *_x) const {
      double p(0.0);
      for(auto& par : m_par) {
        double term = par.second.c;
        for (unsigned int j=0; j<dims(); ++j) {
          term *= eval(j, _x[j], par.second.idx[j]);
        }
        p += term;
      }
      return this->m_is_amplitude ? p*p : p;
    }

    inline unsigned max_degree() { return m_eigenvectors.degree(); }

    static double eval(unsigned /*d*/, double x, unsigned deg) { return Impl::eval(x, deg); }

    static inline bool zero_integral(unsigned /*d*/, unsigned g) {
      return Impl::zero_integral(g);
    }

    static inline double integral(unsigned /*d*/, unsigned g) {
      return Impl::integral(g);
    }

    static inline double inverse_single_norm_square(unsigned /*d*/, unsigned g) {
      return Impl::inverse_single_norm_square(g);
    }

    inline This* clone() const { return new This(*this); }

    inline std::unique_ptr<Evaluator<This>> evaluator(double scale = 1.0, bool external_coordinates = false) const {
      return std::unique_ptr<Evaluator<This>>(new Evaluator<This>(this, scale, external_coordinates));
    }

    double square_integral() const {
      double s=0;
      for(auto& par : m_par) {
        double term=1.0;
        for (unsigned int j=0; j<dims(); ++j) term *= inverse_single_norm_square(j, par.second.idx[j]);
        s += square(par.second.c)/term;
      }
      return s;
    }

    // Integral of PDF using \int^{1}_{-1} T_{2n}(x)dx=\frac{2}{1-4n^2}
    double integral_pdf() const {
      if(this->m_is_amplitude) return this->square_integral();
      double s=0;
      for(auto& par : m_par) {
        double term=1.0;
        for(unsigned int j=0; j<dims(); ++j) {
          if(!zero_integral(j, par.second.idx[j])) term *= integral(j, par.second.idx[j]);
          else goto next;
        }
        s += term*par.second.c;
        next:
        continue;
      }
      return s;
    }

    inline void normalize(double norm) {
      if(this->is_amplitude()) this->scale(sqrt(norm/this->integral_pdf()));
      else this->scale(norm/this->integral_pdf());
    }

    inline bool init_params() { return true; }

    void prune(unsigned /*dof*/) {}

    void prune_significance(double /*sign*/) {}

    This& operator=(const This&) = delete;
    This& operator=(This&&) = delete;

    // ---- implementation of ITransform ----

    virtual const std::string& get_type_name() const { return type_name(); }

    virtual const std::string& get_basis_name() const { return Impl::name(); }

    virtual unsigned get_dimensions() const { return m_eigenvectors.dims(); }

    // TODO: Not true for BiEigenVectors
    virtual unsigned get_var_degree(unsigned) const { return m_eigenvectors.degree(); }

    virtual unsigned get_max_degree() const { return m_eigenvectors.degree(); }

    virtual unsigned get_nparams() const { return 0; }

    virtual Qty get_param(unsigned /*i*/) const { return Qty(); }

    virtual void set_param(unsigned /*i*/, double /*val*/) {
      // TODO: Implement
    }

    virtual std::unique_ptr<IParamIterator> get_param_iterator() const {
      return std::unique_ptr<IParamIterator>(new ParamIterator<This>(this));
    }

    virtual std::unique_ptr<IEvaluator> get_evaluator(double scale = 1.0, bool external_coordinates = false) const {
      return std::unique_ptr<IEvaluator>(new Evaluator<This>(this, scale, external_coordinates));
    }

    virtual std::unique_ptr<IEvaluator> get_marginalize_evaluator_1D(unsigned, double = 1.0, bool = false) const {
      return std::unique_ptr<IEvaluator>(nullptr);
    }

    virtual std::unique_ptr<IEvaluator> get_marginalize_evaluator_2D(unsigned, unsigned, double = 1.0, bool = false) const {
      return std::unique_ptr<IEvaluator>(nullptr);
    }

    virtual std::unique_ptr<IEvaluator> get_slice_evaluator(unsigned, bool , double = 1.0, bool = false) const {
      return 0;
    }

    virtual std::unique_ptr<IEvaluator> get_slice_derivative_evaluator(unsigned /*d2*/, bool /*_do_normalize*/, double /*scale*/ = 1.0, bool /*external_coordinates*/ = false) const  { return nullptr; }
    
    virtual double get_value(const double* x) const { return pdf(x); }

    virtual double get_integral() const { return integral_pdf(); }

    virtual ITransform* get_axis_transform(unsigned /*d*/) const { return 0; }

    virtual ITransform* get_clone() const { return new This(*this); }

    virtual void do_dump(bool /*unc*/ = false) const {}

    virtual void do_normalize(double norm) { normalize(norm); }

    virtual bool do_eval(unsigned /*npts*/, const double* /*xv*/, double* /*res*/, double /*scale*/ = 1.0, bool /*external_coordinates*/ = false) const {
      // TODO: implement
      return false;
    }

    virtual bool do_full_eval(const Matrix<double>&, Matrix<double>&, bool) const { return false; }

    virtual bool do_write(OptionMap&) const { return false; }

    virtual const MatrixSym<double>* do_create_covariance(bool /*poisson*/) { return nullptr; }

    virtual const Vector<double>* get_parameters() const { return nullptr; }

    virtual const Vector<double>* get_uncertainties() const { return nullptr; }
    
    virtual const MatrixSym<double>* get_covariance() const { return nullptr; }

    virtual bool do_copy(const ITransform*) { return false; }
  protected:
    Eigenv m_eigenvectors;
    CoefficientSet m_par;
  };

  template<class _Eigen, class _TType>
  struct _t_transform<SparseTransform<_Eigen, _TType>> : tvalued<SparseTransform<_Eigen, _TType>> {};

  template<class _Eigen, class _TType, unsigned _Dim>
  struct _t_basis<SparseTransform<_Eigen, _TType>, _Dim> : tvalued<_TType> {};

  template<class _Eigen, class _TType>
  struct _t_eigenvectors<SparseTransform<_Eigen, _TType>> : tvalued<_Eigen> {};

  template<class _Eigen, class _TType>
  struct _t_eigenvector<SparseTransform<_Eigen, _TType>> : _t_eigenvector<_Eigen> {};

  template<class _Eigen, class _TType>
  struct _t_transform_1D<SparseTransform<_Eigen, _TType>> : tvalued<SparseTransform<t_MakeEigenVectors<1, _Eigen>, _TType>> {};
}
#endif // sfi_SparseTransform_h
