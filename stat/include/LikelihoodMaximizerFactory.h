#ifndef sfi_stat_LikelihoodMaximizerFactory_h
#define sfi_stat_LikelihoodMaximizerFactory_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "IComponent.h"
#include "Context.h"
#include "Analysis.h"

namespace sfi {
  class ILikelihood;
  class ILikelihoodMaximizer;
  class Parameters;

  class ILikelihoodMaximizerFactory : public IComponent {
  public:
    virtual ~ILikelihoodMaximizerFactory() {};

    virtual ILikelihoodMaximizer* do_create(Parameters&&, Context&, const OptionMap& opts, ILikelihood* lh, const OptionMap& lhopts) = 0;
  };

  class LikelihoodMaximizerFactoryBase : public ILikelihoodMaximizerFactory {
  public:
    virtual ~LikelihoodMaximizerFactoryBase();
  };

  class LikelihoodMaximizerFactory {
    public:
    LikelihoodMaximizerFactory(OptionMap&, Context&);

    ILikelihoodMaximizer* create(Parameters&&, Context&, const OptionMap& opts, ILikelihood* lh, const OptionMap& lhopts);
    protected:
    ILikelihoodMaximizerFactory* m_factory;
  };

  ILikelihoodMaximizer* likelihood_maximizer(Analysis& ana, OptionMap& opts, ILikelihood* lh, const OptionMap& lhopts);
}
#endif // sfi_stat_LikelihoodMaximizerFactory_h
