#ifndef sfi_OckhamTransformation_h
#define sfi_OckhamTransformation_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "SparseTransform.h"
#include "Transformation.h"
#include "TransformCall.h"
#include "util.h"
#include "pruned_system.h"

namespace sfi {
  struct PruneSignificance {
    template<class _Transf>
    struct apply {
      using SparseTransf = SparseTransform<t_EigenVectors<_Transf>, t_Basis<_Transf>>;
      apply(Context& ctx, const OptionMap& opts):m_log(Log::get(ctx, "PruneSignificance")),m_prune_significance(1e10) {
        m_prune_significance = opts.get_as<double>("prune_significance");
      }

      template<class ... _T>
      bool transform(Context&, _Transf& src, SparseTransf& res) const {
        res.is_amplitude(src.is_amplitude());
        auto pit = src.param_iterator();
        unsigned npruned(0);
        do {
          if(sfi::abs(pit->par()/pit->par_unc()) < this->m_prune_significance) {
            ++npruned;
          }
          else {
            res.set(pit->index(), pit->par(), pit->par_unc(), pit->indices());
          }
        } while(pit->next());
        do_log_info("transform() type: '"<<res.type_name()<<"' pruned "<<npruned<<" elements, frac: "<<double(npruned)/double(src.nparams()));
        return true;
      }

      template<class ..._T>
      bool transform(Context&, _Transf& res) const {
        unsigned npruned(0);
        for(unsigned k=0; k<res.npar(); ++k) {
          if(res.par_sign(k) < this->m_prune_significance) {
            ++npruned;
            res.par(k) = 0.0;
          }
        }
        do_log_info("transform() type: '"<<res.type_name()<<"' pruned "<<npruned<<" elements, frac: "<<double(npruned)/double(res.nparams()));
        return true;
      }
    protected:
      Log m_log;
      double m_prune_significance;
    };
  };

  struct PruneMagnitude {
    template<class _Transf>
    struct apply {
      using SparseTransf = SparseTransform<t_EigenVectors<_Transf>, t_Basis<_Transf>>;
      apply(Context& ctx, const OptionMap& opts):m_log(Log::get(ctx, "PruneMagnitude")),m_fraction(1.0) {
        m_fraction = opts.get_as<double>("fraction");
      }

      template<class ... _T>
      bool transform(Context&, _Transf&, SparseTransf&) const {
        // TODO: Implement
        return false;
      }

      struct _par_val {
        inline bool operator<(const _par_val& o) const { return value < o.value; }
        double value; unsigned index;
      };

      template<class ..._T>
      bool transform(Context&, _Transf& res) const {
        unsigned npruned(((double)res.npar())*m_fraction);
        if(npruned >= res.npar()) return false;
        std::vector<_par_val> values(res.npar(), _par_val{0,0});
        for(unsigned k=0; k<res.npar(); ++k) values[k] = _par_val{std::abs(res.par(k)), k};
        std::sort(values.begin(), values.end());
        for(unsigned k=npruned; k<res.npar(); ++k) res.par(values[k].index) = 0.0;
        do_log_info("transform() type: '"<<res.type_name()<<"' pruned "<<npruned<<" elements, frac: "<<m_fraction);
        return true;
      }
    protected:
      Log m_log;
      double m_fraction;
    };
  };

  template<class _SubTransfo, class _UseSparse = ttrue, class _PruneAlg = PruneSignificance>
  class OckhamTransformation : public NestedTransformation<_SubTransfo> {
    using SubTransf = t_Transform<_SubTransfo>;
    using SparseTransf = SparseTransform<t_EigenVectors<SubTransf>, t_Basis<SubTransf>>;
  public:
    using This = OckhamTransformation<_SubTransfo, _UseSparse>;
    using Base = NestedTransformation<_SubTransfo>;
    using Transf = tcond<teq<_UseSparse, ttrue>::value(), SparseTransf, SubTransf>;
    using Alg = tapply<_PruneAlg, SubTransf>;

    OckhamTransformation(const This& o):Base(o) { }

    OckhamTransformation(Context& ctx, OptionMap& opts):Base(ctx, opts), m_alg(ctx, opts) {}

    OckhamTransformation(Context& ctx, const std::string& name):OckhamTransformation(ctx, name, _SubTransfo(ctx, name)) { }

    OckhamTransformation(Context& ctx, const std::string& name, const _SubTransfo& sub_transfo):Base(ctx, name, sub_transfo), m_alg(ctx, this->m_options) { }

    // ---- exported Transformation interface ----

    /** @return New Transform instance */
    inline Transf* create_transform(Context&) const { return new Transf(this->eigenvectors()); }

    template<class ..._T>
    bool transform(Context& ctx, const TransformCall<_T...>& ctrf, SparseTransf& res) const {
      // TODO: Reuse instance??
      // t_Transform<TransformCall<_T...>> sres(ctrf.eigenvectors());
      SubTransf sres(this->m_sub_transformation.eigenvectors(), "T{"+this->name()+"}");
      ctrf.transform(ctx, this->m_sub_transformation, sres);
      this->init_transform(res, &ctrf.sample());
      m_alg.transform(ctx, sres, res);
      // TODO: This is a workaround because post_process_transform prunes
      if(this->m_normalize) {
        res.scale(res.is_amplitude() ? sqrt(1.0/res.integral_pdf()) : 1.0/res.integral_pdf());
      }
      // TODO: pass nevents
      // this->post_process_transform(res, 0);
      return true;
    }

    template<class ..._T>
    bool transform(Context& ctx, const TransformCall<_T...>& ctrf, SubTransf& res) const {
      ctrf.transform(ctx, res);
      return m_alg.transform(ctx, res);
    }
  protected:
    Alg m_alg;
  };

  template<class _Transf, class _RTransf>
  void copy_to_pruned(const _Transf& trf, const std::vector<bool>& keep_eigen, _RTransf& res) {
    res.compute_covariance(trf.compute_covariance()).compute_uncertainty(trf.compute_uncertainty()).name(trf.name());
    res.is_amplitude(trf.is_amplitude());
    res.init_params();
    res.parameters().copy_subset(trf.parameters(), keep_eigen);
    if(trf.compute_uncertainty()) res.uncertainties().copy_subset(trf.uncertainties(), keep_eigen);
    res.variables(trf.variables());
    if(trf.compute_covariance()) res.covariance().copy_subset(trf.covariance(), keep_eigen);
  }

  template<class _Transf>
  t_PrunedTransform<_Transf>*
  create_pruned_transform(const _Transf& trf, const t_PrunedEigenSystem<_Transf>* esys, const std::vector<bool>& keep_eigen) {
    t_PrunedTransform<_Transf>* res = new t_PrunedTransform<_Transf>;
    res->eigen_system(esys);
    copy_to_pruned(trf, keep_eigen, *res);
    return res;
  }

  template<class _Transf>
  t_PrunedTransform<_Transf>*
  prune_transform(const _Transf& trf, const std::function<bool(const t_EigenVector<_Transf>&, const _Transf&)>& sfunc, std::vector<bool>& keep_eigen) {
    using EigenSys = t_PrunedEigenSystem<_Transf>;
    using Eigenv = t_EigenVectors<EigenSys>;
    EigenSys* eigens = new EigenSys(Eigenv());
    Eigenv& eigenv = eigens->eigenvectors();
    t_PrunedTransform<_Transf>* res = new t_PrunedTransform<_Transf>;
    res->eigen_system(eigens);
    keep_eigen.resize(trf.npar(), true);
    for(auto& ei : trf.eigenvectors()) {
      keep_eigen[ei.par_idx()] = sfunc(ei, trf);
      if(keep_eigen[ei.par_idx()]) eigenv.add(ei.indices().data(), ei.eigen_idx());
    }
    copy_to_pruned(trf, keep_eigen, *res);
    return res;
  }

  template<class _Transf>
  t_PrunedTransform<_Transf>*
  prune_transform(const _Transf& trf, const std::function<bool(const t_EigenVector<_Transf>&, const _Transf&)>& sfunc) {
    std::vector<bool> keep_eigen;
    return prune_transform<_Transf>(trf, sfunc, keep_eigen);
  }

  template<class _Transf>
  void prune_transform(const _Transf& trf, const std::function<bool(const t_EigenVector<_Transf>&, const _Transf&)>& sfunc, t_PrunedTransform<_Transf>& res) {
    using EigenSys = t_PrunedEigenSystem<_Transf>;
    using Eigenv = t_EigenVectors<EigenSys>;
    EigenSys* eigens = new EigenSys(Eigenv());
    Eigenv& eigenv = eigens->eigenvectors();
    res.eigen_system(eigens);
    std::vector<bool> keep_eigen(trf.npar(), true);
    for(auto& ei : trf.eigenvectors()) {
      keep_eigen[ei.par_idx()] = sfunc(ei, trf);
      if(keep_eigen[ei.par_idx()]) eigenv.add(ei.indices().data(), ei.eigen_idx());
    }
    copy_to_pruned(trf, keep_eigen, res);
  }

  template<class _Transf>
  void create_pruned(const _Transf& trf, double sign_limit, t_PrunedTransform<_Transf>& res) {
    prune_transform<_Transf>(trf, [sign_limit](const t_EigenVector<_Transf>& ei, const _Transf& trf) -> bool {
      return fabs(trf.par(ei.par_idx()))/trf.par_unc(ei.par_idx()) > sign_limit;
    }, res);
  }
}

#endif // sfi_OckhamTransformation_h
