#ifndef sfi_PDFBase_h
#define sfi_PDFBase_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <Parameters.h>
#include <IPDF.h>

namespace sfi {
  /**
   * @class PDFBase
   *
   * Base class of the pdf:s. Responsible for normalization.
   * The class contains the normalizations for each variant, in terms
   * of relative shifts.
   * This is the interface used by the likelihood classes.
   */
  class PDFBase : public IPDF {
  public:
    virtual ~PDFBase();

    // ---- implementation of IPDF ----

    virtual const std::string& get_name() const { return m_name; }
    
    virtual double get_N(EvalPoint ep, const std::string& var, bool shift_down);

    virtual IEvaluator* get_evaluator() { return 0; }

    virtual const ITransform* get_transform() { return 0; }

    virtual void do_update_parameters(EvalPoint) { }

    virtual void do_print(std::ostream&);

    // ---- interface ----

    /** @return If this pdf is dependent on named variant */
    virtual bool is_dependent_on_variant(const std::string& var) const { return has_norm_variant(var); }

    /** @return Evaluate this pdf for given point and variant*/
    virtual double do_eval(const double* x, const std::string& var = "", bool shift_down = false) = 0;

    /** @return Contained pdf:s */
    virtual void get_pdfs(std::vector<IPDF*>& pd) = 0;

    /** Initlaize the parameters calls init_norm_parameter */
    virtual bool do_init_parameters(const OptionMap& opts, Parameters& pars);

    // ---- exported interface ----

    inline const std::string& name() const { return m_name; }

    inline Normalization norm_type() const { return m_norm_type; }

    inline bool has_norm_variant(const std::string& name) const {
      return m_norm_variants.find(name) != m_norm_variants.end();
    }

    inline std::pair<double,double> norm_variant(const std::string& name) const {
      auto it = m_norm_variants.find(name);
      if(it != m_norm_variants.end()) return it->second;
      else return std::make_pair(0.0,0.0);
    }
  protected:
    PDFBase(Normalization norm, const std::string& name);

    PDFBase(const std::string& name, const OptionMap& topts);

    /** @return Shift of normalization for given varaint */
    double normalization_shift(EvalPoint ep = EvalCurrent, const std::string& var = "", bool shift_down = false) const;

    /** Add norm variant/systematic shift in terms of relative normalization */
    void add_norm_systematics(const std::string& name, double mu_down, double mu_up);

    /**
     * Look for option "norm" with is the name of the normaliation parameter.
     * Sets the Normalization type.
     * In case the parameter is relative norm, an option "norm_expected" must be provided.
     */
    void init_norm_parameter(const OptionMap& opts, Parameters& pars);

    Normalization m_norm_type;
    std::string m_name;
    double m_N;
    Parameter* m_p_N;
    std::map<std::string, std::pair<double,double>> m_norm_variants;
  };
}

#endif /* sfi_PDFBase_h */
