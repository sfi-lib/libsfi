#ifndef sfi_stat_functions_h
#define sfi_stat_functions_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Histogram.h"
#include "Sample.h"

namespace sfi {

  template<class _Transf, class _EventSel, class _Sample>
  double
  stat_invp_sum(const _Sample& sample, const _Transf& tr, const _EventSel& esel, bool sq = false) {
    double res(0.0);
    auto ev = tr.evaluator();
    unsigned nev(0);
    if(sq) nev = sample.apply(esel, [&res, &ev] (const double* x, double) { res += 1.0/square((*ev)(x)); });
    else nev = sample.apply(esel, [&res, &ev] (const double* x, double) { res += 1.0/(*ev)(x); });
    return res*(sq ? tr.square_integral()  : tr.integral_pdf())/(pow(2.0, _Transf::dims())*double(nev));
  }

  template<class _Transf, class _EventSel, class _Sample>
  double
  stat_lhood(const _Sample& sample, const _Transf& tr, const _EventSel& esel, bool _square = false, bool _normalize = true) {
    double res(0.0);
    auto ev = tr.evaluator();
    if(_square) sample.apply(esel, [&res, &ev] (const double* x, double) { res += -2.0*ln(square((*ev)(x))); });
    else sample.apply(esel, [&res, &ev] (const double* x, double) { res += -2.0*ln((*ev)(x)); });
    if(_normalize) {
      const double _w = 1.0/(_square ? tr.square_integral() : tr.integral_pdf());
      res += -2.0*double(sample.events())*ln(_w);
    }
    return res;
  }

  template<class _Transf, class _Esel, class _Sample>
  Vector<double>
  compute_hessian_diagonal(const _Sample& s1, const _Transf& ttrans, const _Esel& esel) {
    const unsigned npar(ttrans.npar());
    Vector<double> H(npar);
    if(!ttrans.is_amplitude()) {
      std::cout<<"compute_hessian_diagonal() Error: Not implemented for linear transform"<<std::endl;
      return H;
    }

    auto pdf_ev = ttrans.evaluator();
    unsigned nevents = s1.apply(esel, [&H, &pdf_ev, &ttrans](const double* x, double) {
      double y = 1.0/((*pdf_ev)(x)), t;
      auto& ed = pdf_ev->eval_data();
      for(auto& eit : ttrans.eigenvectors()) {
        t = y;
        for(unsigned d=0; d<_Transf::dims(); ++d) t *= square(ed[d][eit[d]]);
        H(eit.par_idx()) += t;
      }
    });
    const double sc = 4.0*ttrans.integral_pdf()/double(nevents);
    H *= sc;
    for(unsigned r=0; r<npar; ++r) H(r) += 4.0;
    return H;
  }

  template<class _Transf, class _Esel, class _Sample>
  Vector<double>
  compute_grad(const _Sample& s1, const _Transf& ttrans, const _Esel& esel) {
    const unsigned npar(ttrans.npar());
    Vector<double> g(npar);
    auto pdf_ev = ttrans.evaluator();
    auto eit = ttrans.eigenvectors().iterator();
    auto& ed = pdf_ev->eval_data();
    unsigned nevents = s1.apply(esel, [&g, &pdf_ev, &eit, &ed](const double* x, double) {
      // TODO: only compute A (not A^2)
      double inv_A = 1.0/sqrt((*pdf_ev)(x)), t;
      eit.clear();
      do {
        t = inv_A;
        for(unsigned d=0; d<_Transf::dims(); ++d) t *= ed[d][eit[d]];
        g[eit.par_idx()] -= t;
      } while(eit.next());
    });
    g += ttrans.parameters();
    g *= 4.0*ttrans.integral_pdf()/double(nevents);
    return g;
  }
}

#endif // sfi_stat_functions_h
