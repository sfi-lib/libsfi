#ifndef sfi_AggregateTransformation_h
#define sfi_AggregateTransformation_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Transformation.h"
#include "MultiTransform.h"
#include "timer.h"

namespace sfi {
  template<class _Trafo>
  class AggregateTransformation : public NestedTransformation<_Trafo> {
  public:
    using This = AggregateTransformation<_Trafo>;
    using Base = NestedTransformation<_Trafo>;
    using Transf = t_SubTransform<_Trafo>;
    using SubTrafo = t_SubTransformation<_Trafo>;

    AggregateTransformation(Context& ctx, OptionMap& opts):
      Base(ctx, opts), m_normalize_to_samplen(false) {
      this->m_options.bind("normalize_to_samplen", m_normalize_to_samplen);
    }

    AggregateTransformation(Context& ctx, const std::string& name):
      Base(ctx, name, _Trafo(name)), m_normalize_to_samplen(false) {
      this->m_options.bind("normalize_to_samplen", m_normalize_to_samplen);
    }

    AggregateTransformation(Context& ctx, const std::string& name, const _Trafo& trafo):
      Base(ctx, name, trafo), m_normalize_to_samplen(false) {
      this->m_options.bind("normalize_to_samplen", m_normalize_to_samplen);
    }

    AggregateTransformation(const This& o):
      Base(o), m_normalize_to_samplen(o.m_normalize_to_samplen) {
      this->m_options.bind("normalize_to_samplen", m_normalize_to_samplen);
    }

    /** @return New Transform instance */
    inline Transf* create_transform(Context& ctx) const { return Base::sub_transformation().sub_transformation().create_transform(ctx); }

    inline bool normalize_to_samplen() const { return m_normalize_to_samplen; }
    inline This& normalize_to_samplen(bool b) { m_normalize_to_samplen = b; return *this; }

    struct transform_result {
      bool status;
      double time;
      unsigned ntransforms;
      inline operator bool() const { return this->status; }
    };

    template<class _Transf>
    transform_result transform(Context& ctx, const MultiTransform<_Transf>& strf, _Transf& res, const _Transf* base = 0) const {
      return aggregate_transform<_Transf, Sample<2U>>(ctx, strf, res, 0, base);
    }

    template<class ..._T, class _Transf>
    transform_result
    transform(Context& ctx, const TransformCall<_T...>& ctrf, _Transf& res, const _Transf* base = 0) const {
      static_assert((c_is_valid_transform<SubTrafo, _Transf>()), "Invalid transform type");
      MultiTransform<_Transf> strf;
      if(!ctrf.transform(ctx, this->m_sub_transformation, strf) || (strf.size() == 0)) {
        do_log_error("transform() Error in sub transform");
        transform_result tres;
        tres.status = false;
        return tres;
      }
      return aggregate_transform(ctx, strf, res, &ctrf.sample(), base);
    }

  protected:
    template<class _Transf, class _Sample>
    transform_result aggregate_transform(Context&, const MultiTransform<_Transf>& strf, _Transf& res, const _Sample* spl = 0, const _Transf* base = 0) const {
      transform_result tres;
      tres.status = false;
      if(strf.size() == 0) {
        do_log_error("aggregate_transform() Error in sub transform");
        return tres;
      }

      Timer<true> timer;
      timer.start();

      const bool compute_cov(this->compute_covariance());
      this->init_transform(res, spl);
      if(!spl) res.variables(strf.get_variables());
      res.compute_covariance(compute_cov);
      res.is_amplitude(strf.transforms()[0].is_amplitude());

      const unsigned npar(res.npar());
      SampleCovariance cov(npar);
      SampleStats stats(npar);

      Vector<double> pars(npar);
      for(auto& trf : strf.transforms()) {
        pars = trf.parameters();
        if(base) {
          for(unsigned i=0; i<npar; ++i) pars[i] -= base->par(i);
        }
        if(compute_cov) cov += pars;
        stats += pars;
      }

      if(compute_cov) {
        cov.compute();
        res.covariance() = cov.covariance();
      }
      for(unsigned n=0; n<npar; ++n) {
        res.par(n) = stats[n].mean();
        res.par_unc(n) = stats[n].stddev();
      }
      if(base) {
        res.parameters() = base->parameters();
      }
      if(m_normalize_to_samplen && spl) res.normalize(spl->events());
      tres.status = this->post_process_transform(res);
      tres.time = timer.stop();
      tres.ntransforms = strf.size();
      return tres;
    }
  protected:
    bool m_normalize_to_samplen;
  };
}

#endif // sfi_AggregateTransformation_h
