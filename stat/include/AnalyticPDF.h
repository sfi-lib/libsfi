#ifndef sfi_AnalyticPDF_h
#define sfi_AnalyticPDF_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "util.h"
#include "Parameters.h"
#include "Evaluator.h"
#include "SliceEvaluator.h"
#include "IModelTransform.h"
#include <PDFBase.h>
#include <ParametrizedPDF.h>

#include <memory>

namespace sfi {
  /**
   * @class AnalyticPDF
   *
   * A pdf which is based on an analytic function, possibly parametrized. The options must contain
   * a vector of strings, called "parameters" with parameter names.
   */
  class AnalyticPDF : public ParametrizedPDF {
  public:
    using Function = std::function<double(const double*, const double*)>;

    AnalyticPDF(Normalization norm, Function fun, const std::string& name);

    AnalyticPDF(Function fun, const std::string& name, const OptionMap& opts);

    virtual ~AnalyticPDF();

    // ---- Implementation of IPDF ----

    virtual double do_eval(const double* x, const std::string& var, bool shift_down);

    virtual bool do_init_parameters(const OptionMap& opts, Parameters& pars);
  protected:
    Function m_function;
  };

  /**
   * Construct an AnalyticalPDF from a lambda : double(const double*,const double*)
   */
  template<class _Lambda>
  inline auto analytic_pdf(_Lambda fun, const std::string& name, const OptionMap& opts) {
    return new AnalyticPDF(std::move(fun), name, opts);
  }
  template<class _Lambda>
  inline auto analytic_pdf(_Lambda fun, const std::string& name, Normalization norm) {
    return new AnalyticPDF(norm, std::move(fun), name);
  }
}

#endif /* sfi_AnalyticPDF_h */
