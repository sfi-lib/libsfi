#ifndef sfi_ILikelihood_h
#define sfi_ILikelihood_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stat.h>
#include <Parameters.h>
#include <IPDF.h>
#include <Context.h>

namespace sfi {
  class ILikelihood {
  public:
    virtual ~ILikelihood() {}

    /**
     * @param ctx Computation context
     * @return The value of the evaluated likelihood
     */
    virtual double do_eval(Context& ctx) = 0;

    /**
     * Set up parameters using options
     * @param opts The options that configure the parameters
     * @param pars The Parameters instance
     * @return If the initialization went well
     */
    virtual bool do_init_parameters(const OptionMap& opts, Parameters& pars) = 0;

    /** Get all contained pdf:s */
    virtual void get_pdfs(std::vector<IPDF*>&) = 0;

    /** Debug print to given stream*/
    virtual void do_print(std::ostream&) const = 0;
  };
}

#endif // sfi_ILikelihood_h
