#ifndef sfi_LogLikelihoodBase_h
#define sfi_LogLikelihoodBase_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <ILikelihood.h>
#include <SystExtrapolation.h>

namespace sfi {
  /**
   * @class LogLikelihoodBase
   * Base class of all log likelihoods.
   *
   * Systematic uncertainties are modelled as "variants", with an associated
   * nuisance parameter, prefixed by "nu_".
   *
   * The likelihood is Taylor expanded for each nuisance parameter, and
   * inter- and extrapolated using LogSystExtrapolation, i.e. a degree
   * 4 polynomial for interpolation and linear extrapolation. This means that
   * the ikelihood is evaluated nominally to get a value L0, then it is
   * evaluated for each variant up and down (Lp, Lm) and the final value is
   * then: L0 * extrap(nu,Lp/L0,Lm/L0).
   *
   * A constraint term is added for each nuisance parameter: nu^2.
   */
  class LogLikelihoodBase : public ILikelihood {
  public:
    virtual ~LogLikelihoodBase();

    // ---- Implementation of ILikelihood ----
    
    virtual double do_eval(Context&);

    virtual bool do_init_parameters(const OptionMap& opts, Parameters& pars);

    // ---- interface ----

    /**
     * Recursively check if this likelihood or one of its constituents is
     * dependent on the given variant.
     * @return True if the likelihood depends on the varaiant
     */
    virtual bool is_dependent_on_variant(const std::string&) const = 0;

    /**
     * Called whenever the parameters have changed. Typically inside the minimizer.
     * @param ep The evaluation point
     */
    virtual void do_update_parameters(EvalPoint ep) = 0;

    /**
     * Evaluate for given variant.
     * @param ctx Context
     * @param var Variant, "" means nominal
     * @param shift_down If the variant should be shifted down, or up
     */
    virtual double do_evaluate(Context& ctx, const std::string& var, bool shift_down) = 0;

    // ---- Exported interface ----

    void variants(const std::vector<std::string>& var);

    void add_variant(const std::string& n);
  protected:
    double eval_constraints();

    std::vector<std::string> m_variant_names;
    std::vector<Parameter*> m_nuisance_params;
    LogSystExtrapolation m_syst_extrap;
  };
}

#endif /* sfi_LogLikelihoodBase_h */
