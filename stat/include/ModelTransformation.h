#ifndef sfi_ModelTransformation_h
#define sfi_ModelTransformation_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Sample.h"
#include "Transformation.h"
#include "ModelTransform.h"

namespace sfi {
  /**
   * @class sfi::ModelTransformation
   *
   * Combine transforms to a conditional PDF.
   *
   * - Options:
   * channel: string
   * source : string
   * variant: string
   *
   * TODO:
   */
  template<class _Transf> class ModelTransformation;

  template<class _T1, unsigned _NDims>
  struct _transform_type<ModelTransform<_T1, _NDims>, void> : tvalued<ModelTransform<_T1, _NDims>> { };

  template<class _Transf, unsigned _NDims>
  class ModelTransformation<ModelTransform<_Transf, _NDims>> : public Transformation<ModelTransform<_Transf, _NDims>, void> {
  public:
    using MTransf = ModelTransform<_Transf, _NDims>;
    using This = ModelTransformation<MTransf>;
    using Base = Transformation<MTransf, void>;
    using TransfLeft = t_LeftTransform<_Transf>;

    ModelTransformation(Context& ctx, const OptionMap& opts):Base(ctx, opts) {
      this->m_options.get_if("channel", m_channel).get_if("source", m_source).get_if("variant", m_variant).get_if("name", m_trfname);
    }

    auto transform(TransformSet<TransfLeft>& trfs, SampleSet<1>& spls) {
      model_transform_result<_Transf, TransfLeft::dims()> res;

      if(m_channel.empty() || m_source.empty()) {
        do_log_error("transform() Source or channel not specified");
        return res;
      }

      auto& trfo = this->m_options;
      std::string srcid = m_channel+"_"+m_source;

      auto* spl_nom = spls[srcid];
      if(!spl_nom) {
        do_log_error("transform() Nominal sample not found '"<<srcid<<"'");
        return res;
      }

      auto* trf_nom = transform_by_name_suffix(trfs, srcid);
      if(!trf_nom) {
        do_log_error("transform() Nominal transform not found with suffix '"<<srcid<<"'");
        return res;
      }
      trf_nom = trf_nom->clone();
      res.shift = MTransf(trf_nom, m_trfname);

      if(trfo.template has_type<std::vector<std::string>>("variants")) {
        auto variants = trfo.template get<std::vector<std::string>>("variants");
        for(auto& sysid : variants) {
          std::string downid = srcid+"_"+sysid+"_down";
          auto *spl_down = spls[downid];
          if(!spl_down) {
            do_log_error("transform() Down sample not found '"<<downid<<"'");
            return res;
          }
          std::string upid = srcid+"_"+sysid+"_up";
          auto *spl_up = spls[upid];
          if(!spl_up) {
            do_log_error("transform() Up sample not found '"<<upid<<"'");
            return res;
          }
          double mu_down = double(spl_down->events())/double(spl_nom->events());
          double mu_up = double(spl_up->events())/double(spl_nom->events());
          do_log_info("transform() Norm shifts for systematics '"<<(srcid+"_"+sysid)<<"' trf name: '"<<m_trfname<<"' down: "<<mu_down<<" up: "<<mu_up);

          auto* trf_down = transform_by_sample(trfs, *spl_down);
          if(!trf_down) {
            do_log_error("transform() Down transform not found for sample '"<<downid<<"'");
            return res;
          }
          auto* trf_up = transform_by_sample(trfs, *spl_up);
          if(!trf_up) {
            do_log_error("transform() Up transform not found for sample '"<<upid<<"'");
            return res;
          }

          trf_down = trf_down->clone();
          trf_up = trf_up->clone();
          res.shift.add_variant(sysid, trf_down, mu_down, trf_up, mu_up);

          // for debugging mainly
          res.samples[sysid] = SampleSet<_NDims>({spl_down, spl_nom, spl_up}, false);
          res.transforms[sysid] = TransformSet<_Transf>({trf_down, trf_nom, trf_up}, false);
        }
      }

      // shape shifts
      auto* shape_down = transform_by_name_suffix(trfs, srcid+"_shape_down");
      do_log_info("transform() shape down transform: "<<shape_down);
      if(shape_down) {
        auto* shape_up = transform_by_name_suffix(trfs, srcid+"_shape_up");
        shape_down = shape_down->clone();
        shape_up = shape_up->clone();

        std::string varid = res.shift.name();
        res.shift.add_variant(varid, shape_down, 1.0, shape_up, 1.0);

        // for debugging mainly
        res.samples[varid] = SampleSet<_NDims>({spl_nom, spl_nom, spl_nom}, false);
        res.transforms[varid] = TransformSet<_Transf>({shape_down, trf_nom, shape_up}, false);

        do_log_info("transform() added shape transform, src '"<<srcid<<"'");
      }

      res.status = true;
      return res;
    }
  protected:
    std::string m_channel, m_source, m_variant, m_trfname;
  };
}

#endif // sfi_ModelTransformation_h
