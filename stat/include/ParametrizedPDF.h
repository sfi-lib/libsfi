#ifndef sfi_ParametrizedPDF_h
#define sfi_ParametrizedPDF_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "PDFBase.h"

namespace sfi {
  /**
   * Base class of parametrized pdf:s, i.e. pdf:s that depend on at least one parameter.
   * Contains an ordered set (vector) of parameters, and a corresponding vector of
   * parameter values.
   * Contains a map of all variant parameters.
   * Parameters can be initialized in one of two ways (init_parameters).
   * Either using Parameters and OptionMap, used for AnalyticPDF, in which case
   * an option vector called "parameters" is expected, containing the names of the
   * parameters.
   * If the parameters are initialized using an Evaluator the variables of the underlying
   * Transform will be used, and one option per variable is expected, that name the
   * corresponding parameter.
   */
  class ParametrizedPDF : public PDFBase {
  public:
    virtual ~ParametrizedPDF();

    /** Pick up the current parameter values and store them in vector */
    virtual void do_update_parameters(EvalPoint ep);

    /** This is a simple pdf, just add it to the list */
    virtual void get_pdfs(std::vector<IPDF*>& pd) { pd.push_back(this); }

    virtual bool is_dependent_on_variant(const std::string& var) const;
  protected:
    ParametrizedPDF(Normalization norm, const std::string& name);

    /**
     * A shape variant/systematics can be added by giving the option "shape".
     */
    ParametrizedPDF(const std::string& name, const OptionMap& topts);

    bool init_parameters(const OptionMap& opts, Parameters& pars, IEvaluator& eval);

    void init_parameters(Parameters& pars, const OptionMap& vars);

    std::vector<Parameter*> m_params;
    std::vector<double> m_values;
    std::map<std::string,Parameter*> m_variants;
    std::string m_shape_variant;
  };
}

#endif /* sfi_ParametrizedPDF_h */
