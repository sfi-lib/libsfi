#ifndef sfi_IModelTransform_h
#define sfi_IModelTransform_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ITransform.h"

namespace sfi {
  class IModelEvaluator;

  /**
   * @class IModelTransform
   *
   * Class representing a complex model of a single contribution.
   * The nominal transform has a number of variant transforms,
   * corresponding to systematic shifts.
   *
   * All transforms are normalized to integral 1.0.
   */
  class IModelTransform : public ITransform {
  public:
    virtual ~IModelTransform() {}

    /** @return The name */
    virtual const std::string& get_name() const = 0;

    /**
     * Get the number of dimensions, may be less than the dimensionality of a contained transform
     * in case the transforms are parametrized.
     */
    virtual unsigned get_dimensions() const = 0;

    /** @return The nominal transform */
    virtual const ITransform* get_nominal() const = 0;

    /** @return The nominal integral */
    virtual double get_nominal_N() const = 0;

    /** @return Names of all variants supported by this model */
    virtual std::vector<std::string> get_variants() const = 0;

    /** @return If this model has the named variant */
    virtual bool get_has_variant(const std::string& var) const = 0;

    /** @return The named variant transform */
    virtual const ITransform* get_variant(const std::string& var, bool shift_down) const = 0;

    /** @return The variant scale, relative to the nominal integral */
    virtual double get_variant_scale(const std::string& var, bool shift_down) const = 0;

    /** @return Number of parameters to the slice */
    virtual unsigned get_nslice_parameters() const = 0;
  };

  class IModelEvaluator : public IEvaluator {
  public:
    virtual const IModelTransform* get_transform() = 0;

    virtual double do_evaluate(const double*x, const std::string& var = "", bool shift_down = false) = 0;
  };
}

#endif // sfi_IModelTransform_h
