#ifndef sfi_IPDF_h
#define sfi_IPDF_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <Exception.h>
#include <ITransform.h>
#include <iostream>

namespace sfi {
  class PDFException : public Exception {
  public:
    PDFException(const std::string& n):Exception(n) {}
  };

  enum Normalization { NormAbsolute, NormRelative };
  enum EvalPoint { EvalInitial = 1, EvalCurrent = 2 , EvalML = 3 };

  /**
   * Interface to all pdf:s.
   * Each pdf is required to have a name. The instances also keep track of their normalization,
   * which can be dependent on variant. Most pdf:s have an underlying Transform, which can be
   * accessed using the interface. The pdf:s are usually contained in a likelihood.
   */
  class IPDF {
  public:
    virtual ~IPDF() {}
    
    /** @return The name of the pdf */
    virtual const std::string& get_name() const = 0;
    
    /** @return Normalization for given eval point, variant and shift */
    virtual double get_N(EvalPoint ep, const std::string& var, bool shift_down) = 0;
    
    /** @return Evaluator if or nullptr if there is none */
    virtual IEvaluator* get_evaluator() = 0;
    
    /** @return Underlying Transform or nullptr if there is none */
    virtual const ITransform* get_transform() = 0;
    
    /** Called when the parameters have been updated for instance in the minimization step */
    virtual void do_update_parameters(EvalPoint ep) = 0;
    
    /** Debug print to given stream */
    virtual void do_print(std::ostream&) = 0;
  };
}

#endif /* sfi_IPDF_h */
