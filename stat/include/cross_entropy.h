#ifndef sfi_cross_entropy_h
#define sfi_cross_entropy_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Analysis.h"
#include "stat.h"
#include "gui.h"

namespace sfi {
  template<class _Trafo, class _Sample, class _Esel>
  double cross_entropy(const _Trafo& at1, const _Sample& spl, const _Esel& esel) {
    auto at1_ev = at1.evaluator(1.0, false);
    double ce = 0;
    unsigned Nev = 0;
    if(at1.is_amplitude()) {
      spl.apply(esel, [&Nev, &ce, &at1_ev](const double* x, double) {
        ce -= ln((*at1_ev)(x));
        ++Nev;
      });
    }
    else {
      spl.apply(esel, [&Nev, &ce, &at1_ev](const double* x, double) {
        double f = (*at1_ev)(x);
        if(f > 0) {
          ce -= ln(f);
          ++Nev;
        }
      });
    }
    ce = ce / double(Nev);
    return ce;
  }

  template<class _T>
  unsigned niters(const _T&) { return 0; }

  template<class _AD>
  unsigned niters(const amplitude_transform_result<_AD>& at) {
    return at.niters;
  }

  template<class _Trafo, class _Sample>
  void plot_cross_entropy(Analysis& ana, Log log, const _Sample& spl, const std::string& prefix, unsigned Ndeg, unsigned nfold, unsigned min_degrees, _Trafo& atfo) {
    using Transf = t_Transform<_Trafo>;
    Vector<double> xv(Ndeg), yv_a(Ndeg), yv_b(Ndeg), niter_a(Ndeg), niter_b(Ndeg);

    SampleStats yvs(Ndeg), niter_s(Ndeg);
    yvs.mean_and_stddev(false);
    for(unsigned deg=0; deg<Ndeg; ++deg) {
      {
        Transf at1(t_EigenVectors<Transf>(deg+min_degrees), "a");
        auto res = atfo.transform(ana.main_context(), create_transform_call(spl, EvenEventSel()), at1);
        if(!res) {
          log_error(log, "plot_cross_entropy() failed for even deg: "<<(deg+min_degrees));
        }
        niter_a[deg] = niters(res);
        at1.normalize(1.0);
        yv_a[deg] = cross_entropy(at1, spl, OddEventSel());
        xv[deg] = deg+min_degrees;
      }
      {
        Transf at1(t_EigenVectors<Transf>(deg+min_degrees), "a");
        auto res = atfo.transform(ana.main_context(), create_transform_call(spl, OddEventSel()), at1);
        if(!res) {
          log_error(log, "plot_cross_entropy() failed for odd deg: "<<(deg+min_degrees));
        }
        niter_b[deg] = niters(res);
        at1.normalize(1.0);
        yv_b[deg] = cross_entropy(at1, spl, EvenEventSel());
      }
      for(unsigned p=0;p<nfold; ++p) {
        Transf at1(t_EigenVectors<Transf>(deg+min_degrees), "a");
        auto res = atfo.transform(ana.main_context(), create_transform_call(spl, FracExcEventSel(nfold,p)), at1);
        if(!res) {
          log_error(log, "plot_cross_entropy() failed for "<<nfold<<"-fold "<<p<<" deg: "<<(deg+min_degrees));
        }
        niter_s[deg] += niters(res);
        at1.normalize(1.0);
        yvs[deg] += cross_entropy(at1, spl, FracIncEventSel(nfold, p));
      }
    }

    // Make the plots
    {
      auto& can = Canvas::get(prefix+" CE");
      can.legend(true).axis_title(0, "degree").axis_title(1,"H");
      can.draw(xv,yv_a).title("Even CE").color(kGreen);
      can.draw(xv,yv_b).title("Odd CE").color(kRed);
      can.draw(xv,yvs).title(std::to_string(nfold)+"-fold CE").color(kBlack);
      can.plot();
    }
    {
      auto& can = Canvas::get(prefix+" N iters");
      can.legend(true).axis_title(0, "degree").axis_title(1,"H");
      can.draw(xv,niter_a).title("Even N iters").color(kGreen);
      can.draw(xv,niter_b).title("Odd N iters").color(kRed);
      can.draw(xv,niter_s).title(std::to_string(nfold)+"-fold N iters").color(kBlack);
      can.plot();
    }

  }
}

#endif // sfi_cross_entropy_h
