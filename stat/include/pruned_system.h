#ifndef sfi_pruned_system_h
#define sfi_pruned_system_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "EigenSystem.h"

namespace sfi {
  struct EigenSparse {};
  template<> struct _t_is_eigentype<EigenSparse> : ttrue {};

  template<unsigned _Dims, unsigned _Deg, class _IdxType>
  struct EigenIndices<_Dims, _Deg, EigenSparse, _IdxType> : public EigenIndicesBase<_Dims, _Deg, EigenSparse, _IdxType> {
    using This = EigenIndices<_Dims, _Deg, EigenSparse, _IdxType>;
    using Base = EigenIndicesBase<_Dims, _Deg, EigenSparse, _IdxType>;
    using Data = EigenVectorsData<_Dims, _Deg, EigenSparse>;

    EigenIndices(const Data& da):Base(da) { }

    EigenIndices(EigenIndices&& o):Base(std::forward<EigenIndices>(o)) {}

    EigenIndices(const EigenIndices& o):Base(o) {}

    /** Maximum allowed value in set */
    inline _IdxType max_idx(size_t) { return this->degree(); }

    inline void set(size_t i, _IdxType val) {
      assert(i < _Dims && "Invalid index");
      assert(val <= _Deg && "Invalid value");
      unused_parameters(i, val);
    }

    inline This& operator=(const This& o) { Base::set(o); return *this; }
  };

  template<unsigned _Dims, unsigned _Deg, class _Type, class _IdxType = uint8_t>
  class PrunedEigenVector {
  public:
    using Base = EigenIndices<_Dims, _Deg, _Type, _IdxType>;
    using DataType = typename Base::DataType;

    PrunedEigenVector(const DataType& b, unsigned _par_idx, unsigned _eigen_idx):m_indices(b), m_par_idx(_par_idx), m_eigen_idx(_eigen_idx) {
    }

    inline const DataType& data() const { return this->m_indices; }

    inline _IdxType operator[](size_t i) const {
      assert(i < _Dims && "Invalid index");
      return this->m_indices[i];
    }

    inline unsigned dims() const { return _Dims; }

    inline unsigned par_idx() const { return this->m_par_idx; }

    inline unsigned eigen_idx() const { return this->m_eigen_idx; }

    /** Maximum allowed value in set */
    inline _IdxType max_idx(size_t) { return _Deg; }

    inline void set(size_t /*i*/, _IdxType /*val*/) { }

    inline uint64_t index() const { return m_eigen_idx; }
  protected:
    DataType m_indices;
    unsigned m_par_idx;
    unsigned m_eigen_idx;
  };

  template<unsigned _Dims, unsigned _Deg, class _Type, class _IdxType = uint8_t>
  inline std::ostream& operator<<(std::ostream& out, const PrunedEigenVector<_Dims, _Deg, _Type, _IdxType>& idx) {
    for(unsigned i=0; i<idx.dims(); ++i) out<<(int)idx[i]<<" ";
    return out;
  }

  template<unsigned _Dims, unsigned _Deg, class _IdxType>
  class EigenVectors<_Dims, _Deg, EigenSparse, _IdxType> : public EigenVectorsData<_Dims, _Deg, EigenSparse> {
    using EigenV = PrunedEigenVector<_Dims, _Deg, EigenSparse, _IdxType>;
    using PrunedEigenVectorVec = std::vector<EigenV>;
  public:
    static_assert(std::numeric_limits<_IdxType>::max() > _Deg, "Index type not large enough");

    using Indices = EigenIndices<_Dims, _Deg, EigenSparse, _IdxType>;
    using This = EigenVectors<_Dims, _Deg, EigenSparse, _IdxType>;
    using IndexType = _IdxType;
    using Base = EigenVectorsData<_Dims, _Deg, EigenSparse>;

    class Iterator {
    public:
      using Indices = EigenIndices<_Dims, _Deg, EigenSparse, _IdxType>;

      Iterator(const PrunedEigenVectorVec& vec):m_vec(vec), m_begin(vec.begin()), m_end(vec.end()) { }

      inline void clear() { this->m_begin = m_vec.begin(); }

      inline bool next() { return ++m_begin != m_end; }

      inline const EigenV& indices() const { return *m_begin; }

      inline _IdxType operator[](size_t i) const { return (*m_begin)[i]; }

      inline unsigned par_idx() const { return m_begin->par_idx(); }

      inline unsigned eigen_idx() const { return m_begin->eigen_idx(); }

      // ---- iterator interface ----

      inline Iterator& operator*() { return *this; }

      inline Iterator& operator++() { ++m_begin; return *this; }

      inline bool operator!=(const Iterator& o) { return m_begin != o.m_end; }
    protected:
      const PrunedEigenVectorVec& m_vec;
      typename PrunedEigenVectorVec::const_iterator m_begin, m_end;
    };

    inline Iterator iterator() const { return Iterator(m_eigenvectors); }

    inline size_t params() const { return m_eigenvectors.size(); }

    inline Iterator begin() const { return Iterator(m_eigenvectors); }

    inline Iterator end() const { return Iterator(m_eigenvectors); }

    inline void add(const typename Indices::DataType& ind, unsigned _eigen_idx) {
      m_eigenvectors.emplace_back(ind, m_eigenvectors.size(), _eigen_idx);
    }

    inline const EigenV& operator[](size_t i) const {
      assert(i < m_eigenvectors.size() && "Invalid eigenvector index");
      return m_eigenvectors[i];
    }

    EigenVectors():Base() {}

    EigenVectors(unsigned _deg):Base(_deg) {}

    EigenVectors(unsigned _dim, unsigned _deg):Base(_dim, _deg) {}
  protected:
    PrunedEigenVectorVec m_eigenvectors;
  };

  template<class _Eigen> struct _t_pruned_eigensystem : tvalued<void> { };

  template<class _Dims, class _Deg, class _EType, class _TType>
  struct _t_pruned_eigensystem<EigenSystem<_Dims, _Deg, _EType , _TType>> :
  tvalued<EigenSystem<_Dims, _Deg, EigenSparse , _TType>> { };

  template<class _T>
  using t_PrunedEigenSystem = tvalue<_t_pruned_eigensystem<t_EigenSystem<_T>>>;

  template<class _Transf>
  using t_PrunedTransform = Transform<t_EigenVectors<t_PrunedEigenSystem<_Transf>>, t_Basis<_Transf>>;

  /**
   * Specialization of EigenSystem for sparse
   */
  template<unsigned _Dims, unsigned _Deg, class _Trans>
  struct EigenSystem<tuint<_Dims>, tuint<_Deg>, EigenSparse, _Trans> :
  EigenSystemImpl<tuint<_Dims>, tuint<_Deg>, EigenSparse, _Trans, tuint<_Dims>> {
    using This = EigenSystem<tuint<_Dims>, tuint<_Deg>, EigenSparse, _Trans>;
    using Base = EigenSystemImpl<tuint<_Dims>, tuint<_Deg>, EigenSparse, _Trans, tuint<_Dims>>;
    using Coeff = t_CoefficienctType<_Trans>;

    EigenSystem(const t_EigenVectors<Base>& eig):Base(eig) {}

    template<Basis::CompType _ttype, class _Func>
    inline _Func compute(const t_Var<Base>* x, t_EvalData<Base>& ed, const _Func& _f) const {
      _Func f(_f);
      Base::template precompute<_ttype>(x, ed);
      Coeff term(0);
      auto eit = Base::eigenvectors().iterator();
      do {
        term = 1.0;
        for (unsigned int j=0; j<This::dims(); ++j) term *= ed[j][eit[j]];
        f(term);
      } while(eit.next());
      return f;
    }
  };

  /**
   * Specialization of TransformEigenBase for sparse
   */
  template<unsigned _Dims, unsigned _Deg, class _Trans>
  class TransformEigenBase<EigenSystem<tuint<_Dims>, tuint<_Deg>, EigenSparse, _Trans>> :
  public TransformParamBase<t_CoefficienctType<EigenSystem<tuint<_Dims>, tuint<_Deg>, EigenSparse, _Trans>>> {
  public:
    using EigenSys = EigenSystem<tuint<_Dims>, tuint<_Deg>, EigenSparse, _Trans>;
    using Eigenv = t_EigenVectors<EigenSys>;
    using This = TransformEigenBase<EigenSys>;
    using Base = TransformParamBase<t_CoefficienctType<EigenSys>>;
    using EigenVector = t_EigenVector<Eigenv>;

    virtual ~TransformEigenBase() {}

    virtual bool do_write(OptionMap&) const { return false; }

    inline This& eigensystem(const EigenSys* eigens) {
      // TODO: manage memory!
      m_eigen_system = eigens;
      return *this;
    }

    static constexpr inline unsigned dims() { return EigenSys::dims(); }

    static constexpr inline unsigned indices() { return EigenSys::indices(); }

    inline unsigned npar() const { return eigenvectors().params(); }

    inline const Eigenv& eigenvectors() const { return this->m_eigen_system->eigenvectors(); }

    inline void eigen_system(const EigenSys* eigens) { this->m_eigen_system = eigens; }

    inline const EigenSys& eigen_system() const { return *this->m_eigen_system; }

    /** Print all params to stdout */
    void dump(bool unc = false, bool details = false) const {
      if(details) {
        for(auto& e : eigenvectors()) {
          std::cout<<e.par_idx()<<" "<<e.eigen_idx()<<" "<<e.indices()<<" = "<<this->par(e.par_idx());
          if(unc) std::cout<<" +- "<<this->par_unc(e.par_idx());
          std::cout<<std::endl;
        }
      }
      else this->dump_impl(unc, dims());
    }

    /**
     * Create param vector, by asking Eigenvectors, if Eigenvectors is not materialized, start by creating them.
     * @return True if the parameters were created and set to zero, false if the parameters are already defined (they may be non-zero)
     */
    bool init_params() { return this->do_init_params(npar()); }

    inline const EigenSys* eigen_system_ptr() const { return m_eigen_system; }
  protected:
    TransformEigenBase():Base(""), m_eigen_system(0) { }

    TransformEigenBase(const std::string& name):Base(name), m_eigen_system(0) { }

    TransformEigenBase(const Eigenv&, const std::string& name):Base(name), m_eigen_system(0) { }

    TransformEigenBase(const TransformEigenBase& o):Base(o), m_eigen_system(o.m_eigen_system) { }

    TransformEigenBase(TransformEigenBase&& o):Base(std::forward<TransformEigenBase>(o)), m_eigen_system(o.m_eigen_system) {}

    const EigenSys* m_eigen_system;
  };

  /**
   * Specialization of TransformationEigenBase for sparse
   */
  template<unsigned _Dims, unsigned _Deg, class _IdxType, class _TType>
  class TransformationEigenBase<EigenVectors<_Dims, _Deg, EigenSparse, _IdxType>, _TType> : public TransformationBase {
  public:
    using This = TransformationEigenBase<EigenVectors<_Dims, _Deg, EigenSparse, _IdxType>, _TType>;
    using Base = TransformationBase;
    using Eigenv = EigenVectors<_Dims, _Deg, EigenSparse, _IdxType>;
    using Transf = tvalue<_transform_type<Eigenv, _TType>>;
    using EigenSys = t_EigenSystem<Transf>;

    TransformationEigenBase(Context& ctx, const std::string& name, const EigenSys* _eigen_sys = 0):Base(ctx, name), m_eigen_system(_eigen_sys) { }

    TransformationEigenBase(Context& ctx, OptionMap& opts):Base(ctx, opts), m_eigen_system(0) { }

    TransformationEigenBase(const This& o):Base(o), m_eigen_system(o.m_eigen_system) { }

    ~TransformationEigenBase() {
      // TODO: Manage EigenVectors instance
    }

    static constexpr inline unsigned dims() { return EigenSys::dims(); }
    
    inline void eigen_system(const EigenSys* _eigenvectors) { m_eigen_system = _eigenvectors; }
    inline const EigenSys* eigen_system() const { return m_eigen_system; }

    /** @return New Transform instance */
    inline Transf* create_transform(Context&) const { return new Transf("T{"+this->name()+"}"); }
  protected:
    template<class _Transf, unsigned _NDims, class _SplUseWeight = tfalse>
    inline bool init_transform(_Transf& trf, const Sample<_NDims, _SplUseWeight>* spl) const {
      return trf_eigenvectors(trf) && Base::init_transform(trf, spl);
    }

    template<class _Transf>
    inline bool init_transform(_Transf& trf, const std::string& name, const Variables* vars) const {
      return trf_eigenvectors(trf) && Base::init_transform(trf, name, vars);
    }

    template<class _Transf>
    inline bool trf_eigenvectors(_Transf& trf) const {
      if(!m_eigen_system && !trf.eigen_system_ptr()) {
        do_log_error("trf_eigenvectors() No eigen system!");
        return false;
      }
      else if((m_eigen_system && !trf.eigen_system_ptr()) || (m_eigen_system == trf.eigen_system_ptr())) {
        trf.eigen_system(m_eigen_system);
      }
      else if(m_eigen_system) {
        do_log_warning("trf_eigenvectors() Replacing eigenvectors!");
        trf.eigen_system(m_eigen_system);
      }
      return true;
    }

    const EigenSys* m_eigen_system;
  };
}

#endif // sfi_pruned_system_h
