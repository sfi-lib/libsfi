#ifndef sfi_ModelTransform_h
#define sfi_ModelTransform_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Evaluator.h"

#include <map>
#include "IModelTransform.h"
#include "SliceEvaluator.h"
#include "SystExtrapolation.h"
#include "Sample.h"
#include "Analysis.h"
#include "Transform.h"

namespace sfi {
  /**
   * @class ModelTransform
   *
   * Acts like a Transform, i.e. imports ITransform and exports a subset
   * of the Transform methods.
   */
  template<class _Transf, unsigned _NDims>
  class ModelTransform : public IModelTransform {
  public:
    using This = ModelTransform<_Transf, _NDims>;

    virtual ~ModelTransform() {}

    // ---- Implementation of IModelTransform ----

    virtual const std::string& get_name() const { return this->name(); }

    virtual unsigned get_dimensions() const { return this->dims(); }

    virtual const ITransform* get_nominal() const { return this->m_nominal; }

    virtual double get_nominal_N() const { return this->m_N; }

    virtual std::vector<std::string> get_variants() const { return variants(); }

    virtual bool get_has_variant(const std::string& var) const { return has_variant(var); }

    virtual const ITransform* get_variant(const std::string& var, bool shift_down) const {
      return variant(var, shift_down);
    }

    virtual double get_variant_scale(const std::string& var, bool shift_down) const {
      return variant_scale(var, shift_down);
    }

    virtual unsigned get_nslice_parameters() const { return this->nslice_parameters(); }

    // ---- Implementation of ITransform ----

    virtual const std::string& get_type_name() const;
    virtual const std::string& get_basis_name() const;

    virtual unsigned get_var_degree(unsigned /*i*/) const { return 0; }
    virtual unsigned get_max_degree() const { return 0; }
    virtual unsigned get_nparams() const { return 0; }
    virtual bool get_is_amplitude() const { return m_nominal->get_is_amplitude(); }
    virtual void set_is_amplitude(bool) {}
    virtual Qty get_param(unsigned) const { return Qty(0,0); }
    virtual void set_param(unsigned, double) {}
    virtual std::unique_ptr<IParamIterator> get_param_iterator() const { return nullptr; }
    virtual std::unique_ptr<IEvaluator> get_evaluator(double scale = 1.0, bool external_coordinates = false) const {
      return std::unique_ptr<IEvaluator>(new Evaluator<This>(this, scale, external_coordinates));
    }
    virtual std::unique_ptr<IEvaluator> get_marginalize_evaluator_1D(unsigned /*axis*/, double /*scale*/ = 1.0, bool /*external_coordinates*/ = false) const { return nullptr; }
    virtual std::unique_ptr<IEvaluator> get_marginalize_evaluator_2D(unsigned /*a1*/, unsigned /*a2*/, double /*scale*/ = 1.0, bool /*external_coordinates*/ = false) const { return nullptr; }
    virtual std::unique_ptr<IEvaluator> get_slice_evaluator(unsigned /*d2*/, bool /*_do_normalize*/, double /*scale*/ = 1.0, bool /*external_coordinates*/ = false) const { return nullptr; }
    virtual std::unique_ptr<IEvaluator> get_slice_derivative_evaluator(unsigned /*d2*/, bool /*_do_normalize*/, double /*scale*/ = 1.0, bool /*external_coordinates*/ = false) const  { return nullptr; }

    virtual double get_value(const double*) const { return 0; }
    virtual double get_integral() const { return 0; }
    virtual ITransform* get_axis_transform(unsigned /*d*/) const { return nullptr; }
    virtual ITransform* get_clone() const { return new ModelTransform(*this); }
    virtual void do_dump(bool /*unc*/ = false) const {};
    virtual void do_normalize(double) {}
    virtual const PVariables& get_variables() const { return m_nominal->get_variables(); }
    virtual void set_variables(const PVariables&) {}
    virtual bool do_eval(unsigned, const double*, double*, double /*scale*/ = 1.0, bool /*external_coordinates*/ = false) const {
      return 0;
    }

    virtual bool get_compute_uncertainties() const { return m_nominal->get_compute_uncertainties(); }
    virtual bool get_compute_covariance() const { return m_nominal->get_compute_covariance(); }
    virtual bool do_full_eval(const Matrix<double>&, Matrix<double>&, bool /*external_coordinates*/ = false) const {
      return 0;
    }
    virtual bool do_write(OptionMap& o) const { return write(o); }
    virtual const MatrixSym<double>* do_create_covariance(bool /*poisson*/) { return nullptr; }
    virtual const Vector<double>* get_parameters() const { return nullptr; }
    virtual const Vector<double>* get_uncertainties() const { return nullptr; }
    virtual const MatrixSym<double>* get_covariance() const { return nullptr; }

    virtual bool do_copy(const ITransform* trf) {
      auto* ttrf = dynamic_cast<const This*>(trf);
      if(ttrf && (ttrf != this)) {
        *this = *ttrf;
        return true;
      }
      else return false;
    }

    virtual const PSample& get_sample() const { return m_nominal->get_sample(); }

    virtual void set_sample(const PSample&) { }

    // ---- Exported interface ----

    ModelTransform();

    ModelTransform(_Transf* nom, const std::string& name);

    ModelTransform(const OptionMap& o);

    ModelTransform(const This& o);

    inline const std::string& name() const { return m_name; }

    inline static constexpr unsigned dims() { return _Transf::dims(); }

    inline static constexpr unsigned nslice_parameters() { return _Transf::dims() - _NDims; }

    inline _Transf* nominal() { return m_nominal; }
    inline const _Transf* nominal() const { return m_nominal; }

    inline double nominal_N() const { return m_N; }

    inline bool has_variant(const std::string& var) const {
      return m_variants.find(var) != m_variants.end();
    }

    const _Transf* variant(const std::string& var, bool down) const;
    _Transf* variant(const std::string& var, bool down);

    double variant_scale(const std::string& var, bool down) const;

    void add_variant(const std::string& var, _Transf* vt_down, _Transf* vt_up);

    void add_variant(const std::string& var, _Transf* vt_down, double scale_down, _Transf* vt_up, double scale_up);

    std::vector<std::string> variants() const;

    inline std::unique_ptr<Evaluator<This>> evaluator(double scale = 1.0, bool external_coordinates = false) const {
      return std::unique_ptr<Evaluator<This>>(new Evaluator<This>(this, scale, external_coordinates));
    }

    inline This* clone(bool) const { return new This(*this); }

    bool write(OptionMap& o) const;
  protected:
    std::string m_name;
    double m_N;
    _Transf* m_nominal;
    struct variant_data {
      _Transf* trf_down;
      _Transf* trf_up;
      double mu_down, mu_up;
    };
    std::map<std::string,variant_data> m_variants;
  };

  template<class _SampleSet, class _Trafo, class _Transf>
  void make_shape_shifts(Analysis& ana, const std::string& ch_name, Log log, _SampleSet& spls, _Trafo& pstrf, TransformSet<_Transf>& trfs);

  template<class _Transf, unsigned _NDims>
  struct model_transform_result : result {
    model_transform_result():result(false) {}
    ModelTransform<_Transf, _NDims> shift;
    std::map<std::string, TransformSet<_Transf>> transforms;
    std::map<std::string, SampleSet<_Transf::dims()>> samples;
  };

  template<class _Transf, unsigned _NDims> struct _t_eigen_system<ModelTransform<_Transf, _NDims>> : _t_eigen_system<_Transf> {};
  template<class _Transf, unsigned _NDims> struct _t_eigenvectors<ModelTransform<_Transf, _NDims>> : _t_eigenvectors<_Transf> {};

  template<class _Transf, unsigned _NDims>
  class Evaluator<ModelTransform<_Transf, _NDims>> : public IModelEvaluator {
  public:
    using Transf = ModelTransform<_Transf, _NDims>;
    using EvalTy = tcond<_NDims == _Transf::dims(), Evaluator<_Transf>, SliceEvaluator<_Transf,_NDims-1>>;
    using SliceTy = tcond<_NDims == _Transf::dims(), _Transf, t_SliceTransform<_Transf,_NDims-1>>;

    Evaluator(const Transf* _me, double scale , bool external_coordinates):m_me(_me),
        m_nominal_eval(evaluator<_NDims>(m_me->nominal())), m_scale(scale), m_external_coordinates(external_coordinates) {
      init_eval<_NDims>();
    }

    Evaluator(Evaluator&& o):m_me(o.m_me), m_nominal_eval(std::move(o.m_nominal_eval)), m_variants(std::move(o.m_variants)),
        m_scale(o.m_scale), m_external_coordinates(o.m_external_coordinates) {
    }

    Evaluator(const Evaluator& o):m_me(o.m_me), m_scale(o.m_scale), m_external_coordinates(o.m_external_coordinates) {
      init_eval<_NDims>();
    }

    Evaluator operator=(const Evaluator&) = delete;

    virtual ~Evaluator() { }

    // ---- implementation of IEvaluator ----

    virtual double get_value(const double* x) { return eval(x); }

    virtual const IModelTransform* get_transform() { return m_me; }

    virtual void set_scale(double scale) { m_scale = scale; }

    virtual void set_external_coordinates(bool b) { m_external_coordinates = b; }

    virtual unsigned get_dimensions() const { return _Transf::dims() - m_me->nslice_parameters(); }

    virtual bool do_eval(unsigned npts, const double* xv, double* res);

    virtual const Variable& get_variable(unsigned d) const;

    virtual IEvaluator* get_clone(bool deep) const;

    virtual unsigned get_nslice_parameters() const { return m_me->nslice_parameters(); }

    virtual void set_slice(const double *c) { this->slice(c); }

    virtual const ITransform* get_slice_transform() { return slice_transform(); }

    // ---- implementation of IModelEvaluator ----
    
    virtual double do_evaluate(const double*x, const std::string& var, bool shift_down);

    // ---- public methods ----

    inline const Transf* transform() { return m_me; }

    inline double operator()(const double* x) { return eval(x); }

    inline void slice(const double* point) { slice_impl<_NDims>(point); }

    inline const SliceTy* slice_transform() { return slice_transform_impl<_NDims>(); }
  protected:
    double eval(const double* x);

    template<unsigned _dims>
    void init_eval();

    template<unsigned _dims>
    static inline std::unique_ptr<EvalTy> evaluator(const _Transf* trf, teif<_dims == _Transf::dims()>* = 0) {
      return trf->evaluator(1.0,true);
    }
    template<unsigned _dims>
    static inline std::unique_ptr<EvalTy> evaluator(const _Transf* trf, teif<_dims < _Transf::dims()>* = 0) {
      return trf->template slice_evaluator<(_dims-1)>(true, 1.0, true);
    }

    template<unsigned _dims>
    void slice_impl(const double*, teif<_dims == _Transf::dims()>* = 0) {}

    template<unsigned _dims>
    void slice_impl(const double*, teif<_dims < _Transf::dims()>* = 0);

    template<unsigned _dims>
    const SliceTy* slice_transform_impl(teif<_dims == _Transf::dims()>* = 0) { return m_nominal_eval->transform(); }

    template<unsigned _dims>
    const SliceTy* slice_transform_impl(teif<_dims < _Transf::dims()>* = 0) { return &m_nominal_eval->slice(); }

    const Transf* m_me;
    std::unique_ptr<EvalTy> m_nominal_eval;
    struct variant_data {
      std::unique_ptr<EvalTy> eval_down, eval_up;
    };
    std::map<std::string,variant_data> m_variants;
    double m_scale;
    bool m_external_coordinates;
  };

  // ---- ModelTransform ----

  template<class _Transf, unsigned _NDims>
  const std::string& ModelTransform<_Transf, _NDims>::get_type_name() const {
    static std::string s_type_name = "ModelTransform";
    return s_type_name;
  }

  template<class _Transf, unsigned _NDims>
  const std::string& ModelTransform<_Transf, _NDims>::get_basis_name() const {
    static std::string s_basis_name = "";
    return s_basis_name;
  }

  template<class _Transf, unsigned _NDims>
  ModelTransform<_Transf, _NDims>::ModelTransform():m_name(), m_N(0.0), m_nominal(0) {}

  template<class _Transf, unsigned _NDims>
  ModelTransform<_Transf, _NDims>::ModelTransform(_Transf* nom, const std::string& name):
  m_name(name), m_N(nom->get_integral()), m_nominal(nom) {
  }

  // TODO: Check that the number of dims match
  template<class _Transf, unsigned _NDims>
  ModelTransform<_Transf, _NDims>::ModelTransform(const OptionMap& o):
  m_name(o.get_as<std::string>("name")), m_N(o.get_as<unsigned>("N")), m_nominal(0) {
    if(o.has_sub("nominal")) m_nominal = new _Transf(o.sub("nominal"));
    auto& vo = o.sub("variants");
    for(auto& varo : vo) {
      auto* varom = dynamic_cast<const OptionMap*>(varo.second);
      if(varom) {
        add_variant(varo.first,new _Transf(varom->sub("trf_down")), varom->get_as<double>("mu_down"), new _Transf(varom->sub("trf_up")), varom->get_as<double>("mu_up"));
      }
    }
  }

  // TODO: deep clone
  template<class _Transf, unsigned _NDims>
  ModelTransform<_Transf, _NDims>::ModelTransform(const This& o):m_name(o.m_name), m_N(o.m_N), m_nominal(o.m_nominal), m_variants(o.m_variants) {}

  template<class _Transf, unsigned _NDims>
  const _Transf* ModelTransform<_Transf, _NDims>::variant(const std::string& var, bool shift_down) const {
    auto it = m_variants.find(var);
    if(it != m_variants.end()) return shift_down ? it->second.trf_down : it->second.trf_up;
    else return 0;
  }

  template<class _Transf, unsigned _NDims>
  _Transf* ModelTransform<_Transf, _NDims>::variant(const std::string& var, bool shift_down) {
    auto it = m_variants.find(var);
    if(it != m_variants.end()) return shift_down ? it->second.trf_down : it->second.trf_up;
    else return 0;
  }

  template<class _Transf, unsigned _NDims>
  double ModelTransform<_Transf, _NDims>::variant_scale(const std::string& var, bool shift_down) const {
    auto it = m_variants.find(var);
    if(it != m_variants.end()) return shift_down ? it->second.mu_down : it->second.mu_up;
    else return 0.0;
  }

  template<class _Transf, unsigned _NDims>
  void ModelTransform<_Transf, _NDims>::add_variant(const std::string& var, _Transf* vt_down, _Transf* vt_up) {
    m_variants[var] = variant_data{vt_down,vt_up,vt_down->get_integral()/m_N,vt_up->get_integral()/m_N};
  }

  template<class _Transf, unsigned _NDims>
  void ModelTransform<_Transf, _NDims>::add_variant(const std::string& var, _Transf* vt_down, double scale_down, _Transf* vt_up, double scale_up) {
    m_variants[var] = variant_data{vt_down,vt_up,scale_down,scale_up};
  }

  template<class _Transf, unsigned _NDims>
  std::vector<std::string> ModelTransform<_Transf, _NDims>::variants() const {
    std::vector<std::string> res;
    res.reserve(m_variants.size());
    for(auto& v : m_variants) res.push_back(v.first);
    return res;
  }

  template<class _Transf, unsigned _NDims>
  bool ModelTransform<_Transf, _NDims>::write(OptionMap& o) const {
    o.set("name",m_name);
    o.set("dims",_NDims);
    o.set("N",m_N);
    if(m_nominal) {
      m_nominal->write(o.sub("nominal",true));
    }
    auto& vo = o.sub("variants", true);
    for(auto& var : m_variants) {
      auto& varo = vo.sub(var.first, true);
      var.second.trf_down->write(varo.sub("trf_down", true));
      var.second.trf_up->write(varo.sub("trf_up", true));
      varo.set("mu_down", var.second.mu_down);
      varo.set("mu_up", var.second.mu_up);
    }
    return true;
  }

  // ---- Evaluator<ModelTransform> ----

  template<class _Transf, unsigned _NDims>
  bool Evaluator<ModelTransform<_Transf, _NDims>>::do_eval(unsigned np, const double* xv, double* yv) {
    const double* xp = xv;
    for(unsigned i=0; i<np; ++i) {
      yv[i] = eval(xp);
      xp += _NDims;
    }
    return true;
  }

  template<class _Transf, unsigned _NDims>
  const Variable& Evaluator<ModelTransform<_Transf, _NDims>>::get_variable(unsigned d) const {
    if(d >= _Transf::dims()) throw TransformException("Invalid dimension "+std::to_string(d));
    return m_me->nominal()->variables()->at(d);
  }

  template<class _Transf, unsigned _NDims>
  IEvaluator* Evaluator<ModelTransform<_Transf, _NDims>>::get_clone(bool deep) const {
    return new Evaluator<ModelTransform<_Transf, _NDims>>(deep ? const_cast<const ModelTransform<_Transf, _NDims>*>(m_me->clone(deep)) : m_me, m_scale, m_external_coordinates);
  }

  template<class _Transf, unsigned _NDims>
  double Evaluator<ModelTransform<_Transf, _NDims>>::do_evaluate(const double*x, const std::string& var, bool shift_down) {
    if(var.empty()) return (*m_nominal_eval)(x);
    auto it = m_variants.find(var);
    if(it != m_variants.end()) return (*(shift_down ? it->second.eval_down : it->second.eval_up))(x);
    return 0;
  }

  template<class _Transf, unsigned _NDims>
  template<unsigned _dims>
  void Evaluator<ModelTransform<_Transf, _NDims>>::slice_impl(const double* c, teif<_dims < _Transf::dims()>*) {
    m_nominal_eval->slice(c);
    for(auto& v : m_variants) {
      v.second.eval_down->slice(c);
      v.second.eval_up->slice(c);
    }
  }

  template<class _Transf, unsigned _NDims>
  double Evaluator<ModelTransform<_Transf, _NDims>>::eval(const double* x) { return m_scale*(*m_nominal_eval)(x); }

  template<class _Transf, unsigned _NDims>
  template<unsigned _dims>
  void Evaluator<ModelTransform<_Transf, _NDims>>::init_eval() {
    auto vars = m_me->variants();
    for(auto& v : vars) {
      m_variants[v] = variant_data{evaluator<_dims>(m_me->variant(v, true)),evaluator<_dims>(m_me->variant(v, true))};
    }
  }

  // ---- make_shape_shifts ----

  template<class _SampleSet, class _Trafo, class _Transf>
  void make_shape_shifts(Context& ctx, const std::string& spln, Log log, _SampleSet& spls, _Trafo& pstrf, TransformSet<_Transf>& trfs) {
    auto* ss = spls[spln];
    if(!ss) {
      log_error(log, "make_shape_shifts() No sample '"<<spln<<"' found");
      return;
    }
    log_info(log, "make_shape_shifts() adding shape shift to '"<<spln<<"'");
    auto* down = perform_transformation(ctx, *ss, pstrf, OddEventSel());
    down->name(spln+"_shape_down");
    trfs.add(down);
    auto* up = perform_transformation(ctx, *ss, pstrf, EvenEventSel());
    up->name(spln+"_shape_up");
    trfs.add(up);
  }

  template<class _SampleSet, class _Trafo, class _Transf>
  void make_shape_shifts(Analysis& ana, const std::string& ch_name, Log log, _SampleSet& spls, _Trafo& pstrf, TransformSet<_Transf>& trfs) {
    auto& ch = ana.channel(ch_name);
    for(auto& src : ch.sources()) {
      std::string spln(ch_name+"_"+src.first);
      make_shape_shifts(ana.main_context(), spln, log, spls, pstrf, trfs);
      for(auto& var : src.second.variants()) {
        std::string varn(ch_name+"_"+var.first);
        make_shape_shifts(ana.main_context(), varn, log, spls, pstrf, trfs);
      }
    }
  }
}

#endif // sfi_ModelTransform_h
