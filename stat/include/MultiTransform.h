#ifndef sfi_MultiTransform_h
#define sfi_MultiTransform_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Transform.h"

namespace sfi {
  template<class _Transf> class MultiTransform;

  template<class _Transf>
  class Evaluator<MultiTransform<_Transf>> : public IEvaluator {
  public:
    Evaluator(const MultiTransform<_Transf>* par, double scale = 1.0, bool external_coordinates = false):
      m_parent(par), m_scale(scale/double(par->size())) {
      for(auto& tr : par->transforms()) m_evals.push_back(tr.evaluator(scale, external_coordinates));
    }

    // ---- implementation of IEvaluator ----

    virtual ~Evaluator() { }

    virtual double get_value(const double* x) {
      return (*this)(x);
    }

    virtual const ITransform* get_transform() { return m_parent; }

    virtual void set_scale(double scale) { m_scale = scale/double(m_evals.size()); }

    virtual void set_external_coordinates(bool /*ec*/) { }

    virtual unsigned get_dimensions() const { return _Transf::dims(); }

    virtual bool do_eval(unsigned npts, const double* xv, double* res) {
      const double* xp = xv;
      const unsigned NDims = _Transf::dims();
      for(unsigned i=0; i<npts; ++i) {
        res[i] = (*this)(xp);
        xp += NDims;
      }
      return true;
    }

    virtual const Variable& get_variable(unsigned d) const {
      return (*m_parent)[0].variables()->at(d);
    }

    virtual IEvaluator* get_clone(bool) const { return 0; }

    virtual unsigned get_nslice_parameters() const { return 0; }

    virtual void set_slice(const double *) { }

    virtual const ITransform* get_slice_transform() { return nullptr; }

    // ---- public methods ----

    inline const MultiTransform<_Transf>* transform() { return m_parent; }

    inline double operator()(const double* x) {
      double res(0.0);
      for(auto& ev : m_evals) res += (*ev)(x);
      return m_scale*res;
    }
  protected:
    const MultiTransform<_Transf>* m_parent;
    double m_scale;
    std::vector<t_EvaluatorPtr<_Transf>> m_evals;
  };

  template<class _Transf>
  class MultiTransform : public ITransform {
  public:
    using This = MultiTransform<_Transf>;
    using SubTransf = _Transf;

    MultiTransform() { }

    MultiTransform(const TransformSet<_Transf>& trfs) {
      for(auto& t : trfs) m_transforms.add(t.clone());
    }

    MultiTransform(const This& o):m_name(o.m_name) {
      for(auto& t : o.transforms()) m_transforms.add(t.clone());
    }

    MultiTransform(This&& o):m_name(std::move(o.m_name)), m_transforms(std::move(o.m_transforms)) {
      o.m_transforms.clear();
    }

    MultiTransform(const OptionMap& o) {
      o.get_if("name", m_name);
      if(o.has_sub_vec("transforms")) {
        auto& ov = o.sub_vec("transforms");
        for(unsigned i=0; i<ov.size(); ++i) {
          auto* tom = dynamic_cast<const OptionMap*>(ov[i]);
          if(tom) add(new _Transf(*tom));
        }
      }
    }

    virtual ~MultiTransform() { }

    inline const std::string& name() const { return m_name; }

    inline This& name(const std::string& n) { m_name = n; return *this; }

    inline size_t size() const { return m_transforms.size(); }

    inline _Transf& operator[](size_t i) { return m_transforms[i]; }

    inline const _Transf& operator[](size_t i) const { return m_transforms[i]; }

    inline TransformSet<_Transf>& transforms() { return m_transforms; }

    inline const TransformSet<_Transf>& transforms() const { return m_transforms; }

    inline This& add(_Transf* trf) { m_transforms.add(trf); return *this; }

    inline void clear() { m_transforms.clear(); }

    static constexpr inline unsigned dims() { return _Transf::dims(); }

    inline This& operator=(This&& o) {
      if(this != &o) {
        m_transforms = std::move(o.m_transforms);
        o.m_transforms.clear();
      }
      return *this;
    }

    void write(OptionMap& o) const {
      o.set("name", m_name);
      auto& ov = o.sub_vec("transforms", true);
      for(auto& t : m_transforms) {
        auto* om = new OptionMap;
        t.write(*om);
        ov.add(om);
      }
    }

    // ---- implementation of Transform ----

    inline std::unique_ptr<Evaluator<MultiTransform<_Transf>>> evaluator(double scale = 1.0, bool external_coordinates = false) const {
      return std::unique_ptr<Evaluator<MultiTransform<_Transf>>>(new Evaluator<MultiTransform<_Transf>>(this, scale, external_coordinates));
    }

    // ---- implementation of ITransform ----

    virtual const std::string& get_name() const { return m_name; }

    virtual const std::string& get_type_name() const {
      static const std::string s_type_name = type_name<This>();
      return s_type_name;
    }

    virtual const std::string& get_basis_name() const {
      assert(size() && "Empty MultiTransform");
      return m_transforms[0].basis_name();
    }

    virtual unsigned get_dimensions() const { return _Transf::dims(); }

    virtual unsigned get_var_degree(unsigned) const { return 0; }

    virtual unsigned get_max_degree() const { return 0; }

    virtual unsigned get_nparams() const { return 0; }

    virtual bool get_is_amplitude() const { return false; }

    virtual void set_is_amplitude(bool) {}

    virtual Qty get_param(unsigned /*i*/) const { return Qty(); }

    virtual void set_param(unsigned, double) {}

    virtual std::unique_ptr<IParamIterator> get_param_iterator() const {
      return std::unique_ptr<IParamIterator>(nullptr);
    }

    virtual std::unique_ptr<IEvaluator> get_evaluator(double scale = 1.0, bool external_coordinates = false) const {
      return std::unique_ptr<IEvaluator>(new Evaluator<MultiTransform<_Transf>>(this, scale, external_coordinates));
    }

    virtual std::unique_ptr<IEvaluator> get_marginalize_evaluator_1D(unsigned, double = 1.0, bool = false) const {
      return std::unique_ptr<IEvaluator>(nullptr);
    }

    virtual std::unique_ptr<IEvaluator> get_marginalize_evaluator_2D(unsigned, unsigned, double = 1.0, bool = false) const {
      return std::unique_ptr<IEvaluator>(nullptr);
    }

    virtual std::unique_ptr<IEvaluator> get_slice_evaluator(unsigned, bool , double = 1.0, bool = false) const {
      return 0;
    }

    virtual std::unique_ptr<IEvaluator> get_slice_derivative_evaluator(unsigned /*d2*/, bool /*_do_normalize*/, double /*scale*/ = 1.0, bool /*external_coordinates*/ = false) const  { return nullptr; }
    
    virtual double get_value(const double*) const { return 0.0; }

    virtual double get_integral() const {
      double res(0.0);
      for(auto& tr : m_transforms) res += tr.integral_pdf();
      return res/double(m_transforms.size());
    }

    virtual ITransform* get_axis_transform(unsigned /*d*/) const { return 0; }

    virtual ITransform* get_clone() const { return new This(*this); }

    virtual void do_dump(bool /*unc*/ = false) const {}

    virtual void do_normalize(double norm) {
      for(auto& tr : m_transforms) tr.normalize(norm);
    }

    virtual const PVariables& get_variables() const { return m_transforms[0].variables(); }

    virtual void set_variables(const PVariables&) { };

    virtual bool do_eval(unsigned /*npts*/, const double* /*xv*/, double* /*res*/, double /*scale*/ = 1.0, bool /*external_coordinates*/ = false) const {
      return false;
    }

    virtual bool get_compute_uncertainties() const {
      assert(size() && "Empty MultiTransform");
      return m_transforms[0].compute_uncertainty();
    }

    virtual bool get_compute_covariance() const {
      assert(size() && "Empty MultiTransform");
      return m_transforms[0].compute_covariance();
    }

    virtual bool do_full_eval(const Matrix<double>&, Matrix<double>&, bool) const { return false; }

    virtual bool do_write(OptionMap& o) const { write(o); return true; }

    virtual const MatrixSym<double>* do_create_covariance(bool /*poisson*/) { return nullptr; }

    virtual const Vector<double>* get_parameters() const { return nullptr; }

    virtual const Vector<double>* get_uncertainties() const { return nullptr; }
    
    virtual const MatrixSym<double>* get_covariance() const { return nullptr; }

    virtual bool do_copy(const ITransform*) { return false; }

    virtual const PSample& get_sample() const { return m_transforms[0].get_sample(); }

    virtual void set_sample(const PSample&) { }
  protected:
    std::string m_name;
    TransformSet<_Transf> m_transforms;
  };

  template<class _Transf>
  struct _t_transform<MultiTransform<_Transf>> : tvalued<MultiTransform<_Transf>> {};

  template<class _Transf, unsigned _Dim>
  struct _t_basis<MultiTransform<_Transf>, _Dim> : _t_basis<_Transf, _Dim> {};

  template<class _Transf>
  struct _t_eigenvectors<MultiTransform<_Transf>> : _t_eigenvectors<_Transf> {};

  template<class _Transf>
  struct _t_eigenvector<MultiTransform<_Transf>> : _t_eigenvector<_Transf> {};

  template<class _Transf>
  struct _t_transform_1D<MultiTransform<_Transf>> : tvalued<MultiTransform<t_Transform1D<_Transf>>> {};

  template<class _T1>
  struct _transform_type<MultiTransform<_T1>, void> : tvalued<MultiTransform<_T1>> { };

  template<class _T1>
  struct _t_eigen_system<MultiTransform<_T1>> : tvalued<t_EigenSystem<_T1>> {};

  template<class _Transf, class _Transf1D>
  struct _t_is_1D_transform<MultiTransform<_Transf>, MultiTransform<_Transf1D>> : _t_is_1D_transform<_Transf, _Transf1D> {};
}

#endif // sfi_MultiTransform_h
