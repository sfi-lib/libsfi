#ifndef sfi_stat_ILikelihoodMaximizer_h
#define sfi_stat_ILikelihoodMaximizer_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Parameters.h"
#include "Matrix.h"
#include "Log.h"
#include "stat.h"

namespace sfi {
  class IPDF;
  class ILikelihood;

  class ILikelihoodMaximizer {
  public:
    virtual ~ILikelihoodMaximizer() {}

    virtual void do_fix_parameter(unsigned par, double val) = 0;

    virtual Parameters& get_parameters() = 0;
    virtual const Parameters& get_parameters() const = 0;

    virtual Parameter& get_parameter(const std::string& name) = 0;

    /** Maximize the likelihood */
    virtual bool do_maximize() = 0;

    virtual bool get_profile(unsigned par, unsigned npts, Vector<double>& xv, Vector<double>& fv, double sigmas_range = 3.0) = 0;

    virtual bool get_contour(unsigned par1, unsigned par2, unsigned npts, Vector<double>& v1, Vector<double>& v2) = 0;

    /** Evaluate the likelihood at point given by the working values of Parameters */
    virtual double do_eval() = 0;

    virtual void get_pdfs(std::vector<IPDF*>&) = 0;

    /** @return The minimum value of the likelihood */
    virtual double get_minval() const = 0;

    virtual bool get_has_asymetrical_uncertainties() const = 0;

    virtual const ILikelihood* get_likelihood() const = 0;

    /** Set strategy, 0: speed, 1:balanced, 2:accuracy */
    virtual ILikelihoodMaximizer& set_strategy(int i) = 0;

    virtual void set_print_level(PrintLevel) = 0;

    /** Set valuye of given parameter */
    virtual bool set_parameter_value(unsigned p, double v) = 0;

    virtual bool get_covariance(MatrixSym<double>&) = 0;

    virtual bool do_run_migrad(unsigned maxcalls, double tole) = 0;
    virtual bool do_run_minos() = 0;
    virtual bool do_run_hesse() = 0;

    virtual void set_enable_minos(bool b) = 0;
    virtual void set_enable_hesse(bool b) = 0;
  };

  using Quantities = std::map<std::string, Qty>;
  inline void copy_parameters(const Parameters& pars, Quantities& qtys) {
    for(auto* pa : pars) qtys[pa->name()] = Qty(pa->value(), pa->uncertainty());
  }
  inline std::ostream& operator<<(std::ostream& out, const Quantities& qtys) {
    for(auto& q : qtys) out<<"'"<<q.first<<"' = "<<q.second<<" ";
    return out;
  }
}

#endif // sfi_stat_ILikelihoodMaximizer_h
