#ifndef sfi_CombinedLogLikelihood_h
#define sfi_CombinedLogLikelihood_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <LogLikelihoodBase.h>

namespace sfi {
  /**
   * @class CombinedLogLikelihood
   * Manage a set of likelihoods. The result of evaluation will be the sum of the
   * contained likelihoods.
   */
  class CombinedLogLikelihood : public LogLikelihoodBase {
  public:
    CombinedLogLikelihood();

    CombinedLogLikelihood(const std::initializer_list<LogLikelihoodBase*>& lh);

    virtual ~CombinedLogLikelihood();

    void add(LogLikelihoodBase*);

    inline size_t size() const { return this->m_lhoods.size(); }

    inline LogLikelihoodBase* at(size_t i) const { return this->m_lhoods[i]; }

    // ---- implementation if ILikelihood ----

    virtual bool do_init_parameters(const OptionMap& opts, Parameters& pars);

    virtual void get_pdfs(std::vector<IPDF*>& pd);

    virtual void do_print(std::ostream&) const;

    // ---- implementation if LogLikelihoodBase ----

    virtual bool is_dependent_on_variant(const std::string&) const;

    virtual void do_update_parameters(EvalPoint);

    virtual double do_evaluate(Context&, const std::string& var, bool shift_down);
  protected:
    double eval(Context& ctx, const std::string& var, bool shift_down);

    std::vector<LogLikelihoodBase*> m_lhoods;
  };
}

#endif /* sfi_CombinedLogLikelihood_h */
