#ifndef sfi_ExtendedLogLikelihood_h
#define sfi_ExtendedLogLikelihood_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <LogLikelihoodBase.h>
#include <IModelTransform.h>
#include <Analysis.h>
#include <PDFBase.h>

namespace sfi {
  class ISample;

  /**
   * @class ExtendedLogLikelihood
   *
   * Evaluate -2ln(L) of an extended likelihood, i.e.
   * 2.0*(n - Nln(n) - sum_i ln(pdf(x_i)) )
   * Where n is the floating normalization and N is the number of events.
   * The contained pdf is evaluated for each event x_i.
   */
  class ExtendedLogLikelihood : public LogLikelihoodBase {
  public:
    ExtendedLogLikelihood(const std::string& id, const ISample* sample, PDFBase* pdf);

    virtual ~ExtendedLogLikelihood();

    // ---- implementation if ILikelihood ----

    virtual bool do_init_parameters(const OptionMap& opts, Parameters& pars);

    virtual void get_pdfs(std::vector<IPDF*>& pd);

    virtual void do_print(std::ostream&) const;

    // ---- implementation if LogLikelihoodBase ----

    virtual bool is_dependent_on_variant(const std::string&) const;

    virtual void do_update_parameters(EvalPoint);

    virtual double do_evaluate(Context&, const std::string& var, bool shift_down);

    // ---- exported interface ----

    inline const std::string& id() const { return m_id; }

    inline const ISample* sample() const { return m_sample; }

    inline PDFBase* pdf() const { return m_pdf; }
  protected:
    double eval(const std::string& var, bool shift_down);

    std::string m_id;
    const ISample* m_sample;
    PDFBase* m_pdf;
  };

  inline ILikelihood* extended_log_likelihood(const std::string& id, const ISample* sample, PDFBase* pdf) {
    return new ExtendedLogLikelihood(id, sample, pdf);
  }

  /**
   * Construct a likelihood based on options.
   *  The options should be on the form:
   *    channels {
   *      <CH>: {
   *        <TRANSFORM> : { norm: <PARAM>, norm_expected:<VALUE>, <VARIABLE>:<PARAM> ... }
   *      }
   *    }
   * The transforms must be registed in the Analysis.
   * In case <TRANSFORM> is a ModelTransform a ModelPDF is created,
   * if the number of dimensions in the sample and the Transform agree,
   * a TransformPDF is created, otherwise a SlicePDF.
   * For ModelPDF and SlicePDF an option is required for each variable, with the name of the parameter
   * to map the variable to.
   * Norm must always be a parameter name. If the corresponding
   * parameter is of type relative norm, the option "norm_expected" must be
   * set to the nominal normalization.
   *
   * A top level CombinedLogLikelihood will be created. For each channel an ExtendedLogLikelihood
   * is created. For the channels with multiple transforms (i.e. multiple signal sources) a CompositePDF
   * is created.
   * 
   * @param ana Reference to the Analysis containing the parameter set and the transforms
   * @param spls Samples
   * @param param_set Name of parameter set in Analysis
   * @param opts Options defining the likelihood
   * @return ILikelihood or nullptr if something went wrong
   */
  ILikelihood* extended_log_likelihood(Analysis& ana, SampleMap& spls, const std::string& param_set, const OptionMap& opts);
}

#endif // sfi_ExtendedLogLikelihood_h
