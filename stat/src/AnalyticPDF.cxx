/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <AnalyticPDF.h>

using namespace sfi;

AnalyticPDF::AnalyticPDF(Normalization norm, Function fun, const std::string& name):
              ParametrizedPDF(norm, name), m_function(std::move(fun)) {
}

AnalyticPDF::AnalyticPDF(Function fun, const std::string& name, const OptionMap& opts):
              ParametrizedPDF(name, opts), m_function(std::move(fun)) {
}

AnalyticPDF::~AnalyticPDF() {}

double AnalyticPDF::do_eval(const double* x, const std::string&, bool)  { return m_function(m_values.data(), x); }

bool AnalyticPDF::do_init_parameters(const OptionMap& opts, Parameters& pars) {
  PDFBase::do_init_parameters(opts, pars);
  init_parameters(pars, opts);
  return true;
}
