/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <ModelPDF.h>

using namespace sfi;

constexpr bool _Debug = false;

ModelPDF::ModelPDF(const IModelTransform& trf, const OptionMap& opts):ParametrizedPDF(trf.get_name(), opts),
    m_eval(dynamic_cast<IModelEvaluator*>(trf.get_evaluator(1.0,true).release())) {
}

ModelPDF::~ModelPDF() {}

IEvaluator* ModelPDF::get_evaluator() { return m_eval.get(); }

const ITransform* ModelPDF::get_transform() { return m_eval->get_transform(); }

void ModelPDF::do_update_parameters(EvalPoint ep) {
  ParametrizedPDF::do_update_parameters(ep);
  m_eval->set_slice(m_values.data());
}

double ModelPDF::do_eval(const double* x, const std::string& var, bool shift_down) {
  return m_eval->get_transform()->get_has_variant(var) ? m_eval->do_evaluate(x, var, shift_down) : m_eval->do_evaluate(x, "", false);
}

bool ModelPDF::do_init_parameters(const OptionMap& opts, Parameters& pars) {
  if(!m_eval) throw PDFException("Invalid evaluator");
  auto* trf = m_eval->get_transform();
  bool all_ok = init_parameters(opts, pars, *m_eval);
  auto vars = trf->get_variants();
  for(auto& v : vars) {
    double mu_down = trf->get_variant_scale(v,true);
    double mu_up = trf->get_variant_scale(v,false);
    PDFBase::add_norm_systematics(v,mu_down,mu_up);
    std::string parname = "nu_"+v;
    if(pars.has(parname)) {
      m_variants[v] = &pars[parname];
      if(_Debug) {
        std::cout<<"ModelPDF::do_init_parameters() Adding variant '"<<v<<"' parameter '"<<parname<<"'"<<std::endl;
      }
    }
    else throw PDFException("Undefined parameter '"+parname+"'");
  }
  return all_ok;
}
