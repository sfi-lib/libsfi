/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <TransformPDF.h>

using namespace sfi;

TransformPDF::TransformPDF(const ITransform& trf, Normalization norm):
    PDFBase(norm, trf.get_name()), m_eval(trf.get_evaluator(1.0,true)) {
}

TransformPDF::TransformPDF(const ITransform& trf, const OptionMap& topts):PDFBase(trf.get_name(), topts),
    m_eval(trf.get_evaluator(1.0,true)) {
}

TransformPDF::~TransformPDF() {}

IEvaluator* TransformPDF::get_evaluator() { return m_eval.get(); }

const ITransform* TransformPDF::get_transform() { return m_eval->get_transform(); }

double TransformPDF::do_eval(const double* x, const std::string&, bool) { return m_eval->get_value(x); }

void TransformPDF::get_pdfs(std::vector<IPDF*>& pd) { pd.push_back(this); }
