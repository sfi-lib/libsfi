/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "FSpaceLogLikelihoodBase.h"

using namespace sfi;

FSpaceLogLikelihoodBase::FSpaceLogLikelihoodBase(const ITransform& trf_model, const ITransform& trf_data, std::unique_ptr<IEvaluator> eval):
    m_trf_model(trf_model), m_trf_data(trf_data), m_p_N(nullptr), m_eval_model(std::move(eval)) {
}

FSpaceLogLikelihoodBase::~FSpaceLogLikelihoodBase() {}

// TODO: Shared code, make common base
bool FSpaceLogLikelihoodBase::do_init_parameters(const OptionMap& opts, Parameters& pars) {
  bool all_ok = LogLikelihoodBase::do_init_parameters(opts,pars);
  m_p_N = &pars["N"];
  if(m_eval_model) {
    unsigned sd = m_eval_model->get_dimensions();
    unsigned nparams = m_trf_model.get_dimensions() - sd;
    auto tvars = m_trf_model.get_variables();
    m_params.resize(nparams,0);
    m_values.resize(nparams,0);
    for(size_t i=0; i<nparams; ++i) {
      auto& var = tvars->at(sd+i);
      auto& vname = opts.get<std::string>(m_trf_model.get_name()+"."+var.name());
      if(pars.has(vname)) m_params[i] = &pars[vname];
      else all_ok = false;
    }
  }
  return all_ok;
}

void FSpaceLogLikelihoodBase::get_pdfs(std::vector<IPDF*>&) {}

void FSpaceLogLikelihoodBase::do_print(std::ostream&) const { }

bool FSpaceLogLikelihoodBase::is_dependent_on_variant(const std::string&) const { return false; }

void FSpaceLogLikelihoodBase::do_update_parameters(EvalPoint) {}

double FSpaceLogLikelihoodBase::do_evaluate(Context&, const std::string&, bool) { return 0; }

ITransform* FSpaceLogLikelihoodBase::set_slice_params() {
  if(m_eval_model) {
    bool has_nan = false;
    for(unsigned i=0; i<m_params.size(); ++i) {
      m_values[i] = m_params[i]->work_value();
      if(m_values[i] != m_values[i]) has_nan = true;
    }
    if(!has_nan) m_eval_model->set_slice(m_values.data());
    else return nullptr;
  }
  auto* trf_model = const_cast<ITransform*>(m_eval_model->get_slice_transform());
  if(std::abs(trf_model->get_integral() - 1.0) > 1e-12) {
    std::cout<<"FSpaceLogLikelihoodBase::set_slice_params() Model not normalized : "<<trf_model->get_integral()<<std::endl;
  }
  if(!trf_model->get_is_amplitude()) throw PDFException("Transform '"+trf_model->get_name()+"' is -not- an amplitude");
  if(trf_model->get_nparams() != m_trf_data.get_nparams()) throw PDFException("Invalid number of coefficients");
  return trf_model;
}
