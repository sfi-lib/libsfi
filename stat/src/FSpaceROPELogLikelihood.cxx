/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "FSpaceROPELogLikelihood.h"

using namespace sfi;

FSpaceROPELogLikelihood::FSpaceROPELogLikelihood(unsigned nd, const ITransform& trf_model, const ITransform& trf_data, ITransform* trf_bg):
    m_trf_model(trf_model), m_trf_data(trf_data), m_trf_bg(trf_bg), m_p_rho(nullptr), m_p_N(nullptr), m_p_Nb(nullptr),
    m_cov(trf_model.get_nparams()), m_eigen_decomp(true, true), m_inv_cov(m_cov.rows()) {
  if(trf_model.get_is_amplitude()) throw PDFException("Transform '"+trf_model.get_name()+"' is an amplitude");
  if(trf_bg && trf_bg->get_is_amplitude()) throw PDFException("Transform '"+trf_model.get_name()+"' is an amplitude");
  if(nd < trf_model.get_dimensions()) {
    m_eval_model = trf_model.get_slice_evaluator(nd-1,true,1.0,true);
  }
}

double FSpaceROPELogLikelihood::do_eval(Context&) {
  const double rho = exp10(m_p_rho->work_value());
  const double N = m_p_N->work_value();
  const bool prune_eigen = true;
  double Nb(0.0), Ntot(N);
  
  if(m_eval_model) {
    bool has_nan(false);
    for(unsigned i=0; i<m_params.size(); ++i) {
      m_values[i] = m_params[i]->work_value();
      if(m_values[i] != m_values[i]) has_nan = true;
    }
    if(has_nan) return std::nan("");
    m_eval_model->set_slice(m_values.data());
  }
  auto* trf_model = const_cast<ITransform*>(m_eval_model->get_slice_transform());
  if(std::abs(trf_model->get_integral() - 1.0) > 1e-15) {
    std::cout<<"FSpaceROPELogLikelihood::do_eval() Model not normalized : "<<trf_model->get_integral()<<std::endl;
  }
  auto* cov = trf_model->do_create_covariance(true);

  assert((trf_model->get_nparams() == m_trf_data.get_nparams()) && "Invalid number of coefficients");

  if(!cov) {
    std::cout<<"FSpaceROPELogLikelihood::do_eval() No covariance matrix"<<std::endl;
    return std::nan("");
  }
  
  m_cov = *cov;
  if(m_trf_bg) {
    Nb = m_p_Nb->work_value();
    Ntot = N + Nb;
    m_cov *= N;
    auto* cov_bg = m_trf_bg->do_create_covariance(true);
    m_cov.add(*cov_bg, Nb);
    m_cov *= 1.0/Ntot;
  }
  if(!m_eigen_decomp.decompose(m_cov)) {
    std::cout<<"FSpaceROPELogLikelihood::do_eval() Decomposition failed N: "<<N<<" rho: "<<rho<<std::endl;
    return std::nan("");
  }
  auto res = compute_eigen_inverse_rope(m_eigen_decomp, m_cov_eigenv, m_inv_cov, rho, prune_eigen);

  m_s_Tr_orig += res.Tr_orig;
  m_s_Tr_reg += res.Tr_reg;

  m_s_nvalues += res.nvalues;
  const auto npar = trf_model->get_nparams();
  const double lndet = - ln(res.det) + ln(Ntot)*double(npar);
  
  // reuse
  m_cov_eigenv.rows(npar);
  auto& r = m_cov_eigenv;
  
  m_trf_data.get_parameters()->add(*trf_model->get_parameters(), r, -N);
  if(m_trf_bg) r.add(*m_trf_bg->get_parameters(), -Nb);
    
  return lndet + m_inv_cov.lrmult(r)/Ntot;
}

bool FSpaceROPELogLikelihood::do_init_parameters(const OptionMap& opts, Parameters& pars) {
  bool all_ok = LogLikelihoodBase::do_init_parameters(opts,pars);
  m_p_rho = &pars["rho"];
  m_p_N = &pars["N"];
  if(m_trf_bg) m_p_Nb = &pars["Nb"];
  if(m_eval_model) {
    unsigned sd = m_eval_model->get_dimensions();
    unsigned nparams = m_trf_model.get_dimensions() - sd;
    auto tvars = m_trf_model.get_variables();
    m_params.resize(nparams,0);
    m_values.resize(nparams,0);
    for(size_t i=0; i<nparams; ++i) {
      auto& var = tvars->at(sd+i);
      auto& vname = opts.get<std::string>(m_trf_model.get_name()+"."+var.name());
      if(pars.has(vname)) m_params[i] = &pars[vname];
      else all_ok = false;
    }
  }
  return all_ok;
}

void FSpaceROPELogLikelihood::get_pdfs(std::vector<IPDF*>&) {}

void FSpaceROPELogLikelihood::do_print(std::ostream&) const { }

bool FSpaceROPELogLikelihood::is_dependent_on_variant(const std::string&) const { return false; }

void FSpaceROPELogLikelihood::do_update_parameters(EvalPoint) {}

double FSpaceROPELogLikelihood::do_evaluate(Context&, const std::string&, bool) { return 0; }

