/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <SlicePDF.h>

using namespace sfi;

SlicePDF::SlicePDF(Normalization norm, unsigned d2, const ITransform& trf):
    ParametrizedPDF(norm, trf.get_name()), m_eval(trf.get_slice_evaluator(d2, true, 1.0, true)) {
}

SlicePDF::SlicePDF(unsigned d2, const ITransform& trf, const OptionMap& opts):
    ParametrizedPDF(trf.get_name(), opts), m_eval(trf.get_slice_evaluator(d2, true, 1.0, true)) {
}

SlicePDF::~SlicePDF() {}

IEvaluator* SlicePDF::get_evaluator() { return m_eval.get(); }

const ITransform* SlicePDF::get_transform() { return m_eval->get_slice_transform(); }

void SlicePDF::do_update_parameters(EvalPoint ep) {
  ParametrizedPDF::do_update_parameters(ep);
  m_eval->set_slice(m_values.data());
}

double SlicePDF::do_eval(const double* x, const std::string&, bool)  { return m_eval->get_value(x); }

bool SlicePDF::do_init_parameters(const OptionMap& opts, Parameters& pars) {
  if(!m_eval) throw PDFException("Invalid evaluator");
  return init_parameters(opts, pars, *m_eval);
}
