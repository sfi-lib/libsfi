/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "PseudoExperiment.h"

using namespace sfi;

PseudoExperimentBase::PseudoExperimentBase(Analysis& ana, const OptionMap& opts):
    m_log(Log::get(ana.main_context(), "PseudoExperiment")),
    m_nexperiments(100), m_param_values(0,0), m_analysis(ana), m_options(opts),
    m_channel(""), m_par_unc_max(0.05), m_outlier_limit(5.0), m_use_histograms(false), m_time_statistics(true), m_collect_statistics(true),
    m_mean_unc(true) {
  if(m_use_histograms) {
    do_log_error("use_histograms does not work!");
    m_use_histograms = false;
  }
  opts.get_if<std::string>("channel", m_channel);
  opts.get_if("nexperiments", m_nexperiments).get_if("par_unc_max", m_par_unc_max);
  opts.get_if("use_histograms", m_use_histograms).get_if("time_statistics", m_time_statistics);
  opts.get_if("collect_statistics", m_collect_statistics).get_if("outlier_limit", m_outlier_limit);
  opts.get_if("mean_unc", m_mean_unc).get_if("experiment_set", m_experiment_set);
  init_parameters();
}

void PseudoExperimentBase::init_parameters() {
  std::vector<std::string> exnames;
  std::vector<const Analysis::ParameterSet::Param*> aparams;
  if(m_options.has_type<std::string>("parameter_set")) {
    auto psetname = m_options.get_as<std::string>("parameter_set");
    if(m_analysis.has_parameter_set(psetname)) {
      auto& pset = m_analysis.parameter_set(psetname);
      for(auto& _par : pset) {
        auto& par = _par.second;
        if(!par.fixed()) {
          if(par.source().empty()) {
            m_aux_params.push_back(par.name());
          }
          else {
            m_parameter_names.push_back(par.name());
            exnames.push_back(par.source());
            aparams.push_back(&par);
          }
        }
      }
    }
    else {
      do_log_error("Unknown parameter set '"<<psetname<<"'");
    }
  }
  else {
    do_log_error("No parameter set defined");
    return;
  }
  m_experiments = m_analysis.experiments_in_set(m_experiment_set);
  m_param_values.size(m_experiments.size(), m_parameter_names.size());
  unsigned ne = 0;
  for(auto& ex : m_experiments) {
    if(m_analysis.has_experiment(m_experiment_set,ex)) {
      auto& exprops = m_analysis.experiment(m_experiment_set,ex).options();
      for(unsigned np=0; np<m_parameter_names.size(); ++np) {
        try {
          m_param_values(ne, np) = exprops.get_as<double>(exnames[np]);
        }
        catch(const OptionException&) {
          m_param_values(ne, np) = exprops.get_as<double>(exnames[np]+".value");
        }

        /*if(!aparams.empty()) {
          auto& par = *aparams[np];
          if(par.type() == Parameter::RelativeNorm) {
            m_param_values(ne, np) = m_param_values(ne, np)/par.norm();
          }
        }*/
        do_log_info("experiment '"<<ex<<"' opt: '"<<m_parameter_names[np]<<"' '"<<exnames[np]<<"' : "<<m_param_values(ne, np));
      }
    }
    else {
      do_log_error("Unknown experiment '"<<ex<<"'");
    }
    ++ne;
  }
}

PseudoExperimentBase::CaseStat::CaseStat(unsigned nparams):
    param(nparams, SampleStat()), param_reldiff(nparams, SampleStat()), param_e(nparams, SampleStat()), pull_param(nparams, SampleStat()),
    param_hi(nparams, Histogram<true,double>()), param_reldiff_hi(nparams, Histogram<true,double>()),
    param_e_hi(nparams, Histogram<true,double>()), param_pull_hi(nparams, Histogram<true,double>()),
    rtime_hi(), nerrors(0), noutliers(0), m_initialized(false) {
}

PseudoExperimentBase::CaseStat::~CaseStat() {}

PseudoExperimentBase::CaseResult::CaseResult(unsigned nparams):
    param(nparams, Qty()), param_reldiff(nparams, Qty()),
    param_e(nparams, Qty()), param_pull(nparams, Qty()) {
}

PseudoExperimentBase::PECaseBase::PECaseBase(const std::string& n, size_t nparams, int mcol, int mstyle):
    m_name(n), m_par_limits(nparams, std::make_pair(-1.0, 1.0)),
    m_marker_color(mcol), m_marker_style(mstyle), m_debug(false) {
}

void PseudoExperimentBase::PECaseBase::init(size_t n_param_points, size_t nparams) {
  if(m_statistics.empty()) {
    m_statistics.resize(n_param_points, CaseStat(nparams));
    m_result.resize(n_param_points, CaseResult(nparams));
  }
}

void
PseudoExperimentBase::run() {
  const unsigned npv(m_param_values.rows());
  double rtime;
  Timer<true> timer;
  for(auto* _cas : m_cases) {
    PECaseBase& cas = *_cas;
    // TODO: Remove one dim
    if(m_collect_statistics) cas.init(npv, nparams());

    const std::string suf0 = cas.name();

    for(unsigned k=0; k<npv; ++k) {
      auto& exname = m_experiments[k];
      const std::string suf = suf0 + " " + exname;
      do_log_info("++++++++++++++++++++++++++++++++++++++++++");
      do_log_info(suf);
      do_log_info("++++++++++++++++++++++++++++++++++++++++++");

      unsigned attempts(0);
      const unsigned max_attempts = 2 * m_nexperiments;
      unsigned i=0;
      for(; (i<m_nexperiments) && (attempts < max_attempts); ++attempts) {
        if(cas.debug()) std::cout<<"--- "<<i<<std::endl;
        timer.start();

        experiment_result eres = cas.run(m_analysis.main_context(), create_sample(exname));

        bool stat_ok = collect_statistics(eres, k, suf, cas);

        rtime = timer.stop();
        if(stat_ok && m_time_statistics && m_collect_statistics) {
          CaseStat &stat = cas.statistics(k);
          if(m_use_histograms) stat.rtime_hi.fill(rtime);
          stat.rtime += rtime;
        }
        // only count successful runs
        if(stat_ok && eres.status) ++i;
      } // end loop pseudo exps
      if(attempts != m_nexperiments) {
        do_log_info("Done "<<i<<" experiments in "<<attempts<<" attempts");
      }
    } // end loop over params
    // compute results
    if(m_collect_statistics) {
      do_log_info(" **** "<<suf0<<" ****");
      for(unsigned k=0; k<npv; ++k) {
        CaseStat &stat = cas.statistics(k);
        CaseResult &res = cas.result(k);
        if(stat.nerrors) {
          do_log_warning("run() "<<stat.nerrors<<" fit errors in experiment '"<<m_experiments[k]<<"'");
        }
        if(stat.noutliers) {
          do_log_warning("run() "<<stat.noutliers<<" outliers in experiment '"<<m_experiments[k]<<"'");
        }
        compute_results(stat, res, cas);
      }
      for(unsigned k=0; k<npv; ++k) {
        CaseResult &res = cas.result(k);
        std::ostringstream sout;
        sout<<m_experiments[k]<<" t: {";
        for(unsigned p=0; p<nparams(); ++p) {
          if(p) sout<<" ";
          sout<<m_parameter_names[p]<<"="<<m_param_values(k,p);
        }
        sout<<"}";
        for(unsigned p=0; p<nparams(); ++p) {
          sout<<" < "<<m_parameter_names[p]<<" : "<<res.param[p]<<" e "<<res.param_e[p]<<" p "<<res.param_pull[p]<<" > ";
        }
        if(m_time_statistics) sout<<" T: "<<res.rtime;
        do_log_info(sout.str());
      }
    }
    // XXX: cas->_statistics[ne].clear();
    // XXX: cas->_statistics.clear();
  }
}

void PseudoExperimentBase::create_histograms(const std::string&, CaseStat& stat, PECaseBase& cas) {
  for(unsigned p = 0; p < nparams(); ++p) {
    stat.param_hi[p] = Histogram<true,double>(200, cas.par_min(p), cas.par_max(p));
    stat.param_reldiff_hi[p] = Histogram<true,double>(200, -3, 3);
    stat.param_e_hi[p] = Histogram<true,double>(200, 0, m_par_unc_max);
    stat.param_pull_hi[p] = Histogram<true,double>(200, -5, 5);
  }
  if (m_time_statistics) stat.rtime_hi = Histogram<true,double>(1000, 0, 1.0);
  stat.m_initialized = true;
}

bool PseudoExperimentBase::collect_statistics(const experiment_result& eres, unsigned k, const std::string& suf, PECaseBase& cas) {
  const ITransform* data_trf(0);
  size_t ncoeff(0);
  if(eres.data_transform.size() > 0) {
    data_trf = &eres.data_transform[0];
    ncoeff = eres.data_transform[0].get_nparams();
  }
  bool all_ok = true;
  CaseStat& stat = cas.statistics(k);
  if(m_use_histograms && !stat.m_initialized) create_histograms(suf, stat, cas);
  if(eres.status) {
    if(cas.debug()) {
      do_log_info(""<<eres.params);
    }
    if(m_collect_statistics) {
      // collect statistics
      for (unsigned p = 0; p < nparams(); ++p) {
        auto pit = eres.params.find(m_parameter_names[p]);
        if(pit != eres.params.end()) {
          const Qty& pv = pit->second;
          if(m_use_histograms) {
            stat.param_hi[p].fill(pv.value());
            stat.param_reldiff_hi[p].fill((pv.value() - m_param_values(k,p)) / pv.value());
            stat.param_e_hi[p].fill(pv.uncertainty());
            stat.param_pull_hi[p].fill((pv.value() - m_param_values(k,p)) / pv.uncertainty());
          } else {
            double pull = (pv.value() - m_param_values(k,p)) / pv.uncertainty();
            if(std::abs(pull) > m_outlier_limit) {
              do_log_warning("collect_statistics() Outliter '"<<m_parameter_names[p]<<"' true: "<<m_param_values(k,p)<<" est: "<<pv<<" nsigmas: "<<pull);
              ++stat.noutliers;
              all_ok = false;
            } else {
              stat.param[p] += pv.value();
              stat.param_reldiff[p] += (pv.value() - m_param_values(k,p)) / pv.value();
              stat.param_e[p] += pv.uncertainty();
              stat.pull_param[p] += pull;
              if(cas.debug()) {
                do_log_info(""<<m_parameter_names[p]<<" value: "<<pv<<" : "<<stat.param[p].mean_and_spread()<<" unc: "<<stat.param_e[p].mean_and_spread()<<" pull: "<<pull<<" : "<<stat.pull_param[p].mean_and_spread());
              }
            }
          }
        } else {
          do_log_error("collect_statistics() can't find parameter '"<<m_parameter_names[p]<<"'");
        }
      }
      // auxiliary parameters
      for(auto& ap : m_aux_params) {
        auto pit = eres.params.find(ap);
        if(pit != eres.params.end()) stat.aux_param[ap] += pit->second.value();
      }
      for (auto& ap : eres.aux_params) stat.aux_param[ap.first] += ap.second;
    }
  }
  else {
    if (m_collect_statistics) {
      CaseStat& stat = cas.statistics(k);
      stat.nerrors++;
    }
    all_ok = false;
  }
  return all_ok;
}

void PseudoExperimentBase::compute_results(CaseStat& stat, CaseResult& res, PECaseBase&) {
  if (m_use_histograms) {
    // TODO: Use limits from cas?
    // TODO: Reimplement fit!
    double mu(0), mu_e(0), sig(0);//, sig_e(0);
    for (unsigned p = 0; p < nparams(); ++p) {
      //sfi::fit_gaus(stat.param_hi[p], -1, 1, mu, mu_e, sig, sig_e, true);
      res.param[p] = Qty(mu, m_mean_unc ? mu_e : sig);
      //sfi::fit_gaus(stat.param_reldiff_hi[p], -3, 3, mu, mu_e, sig, sig_e, true);
      res.param_reldiff[p] = Qty(mu, m_mean_unc ? mu_e : sig);
      //sfi::fit_gaus(stat.param_e_hi[p], 0, 0.4, mu, mu_e, sig, sig_e, true);
      res.param_e[p] = Qty(mu, m_mean_unc ? mu_e : sig);
      //stat.param_pull_hi[p], -5, 5, mu, mu_e, sig, sig_e, true);
      res.param_pull[p] = Qty(mu, sig);
    }
  }
  else {
    for (unsigned p = 0; p < nparams(); ++p) {
      res.param[p] = m_mean_unc ? stat.param[p].result() : stat.param[p].mean_and_spread();
      res.param_reldiff[p] = m_mean_unc ? stat.param_reldiff[p].result() : stat.param_reldiff[p].mean_and_spread();
      res.param_e[p] = m_mean_unc ? stat.param_e[p].result() : stat.param_e[p].mean_and_spread();
      res.param_pull[p] = stat.pull_param[p].mean_and_spread();
    }
  }
  if (m_time_statistics) res.rtime = stat.rtime.result();
  for (auto& ap : stat.aux_param)
    res.aux_param[ap.first] = m_mean_unc ? ap.second.result() : ap.second.mean_and_spread();
}

void PseudoExperimentBase::add_case(PECaseBase* _case) {
  if(m_options.has_sub("cases."+_case->name())) {
    auto& op = m_options.sub("cases."+_case->name());
    _case->debug(op.get_as<bool>("debug"));
    _case->marker_color(op.get_as<int>("marker_color"));
    _case->marker_style(op.get_as<int>("marker_style"));
    _case->title(op.get_as<std::string>("title"));
  }
  else {
    do_log_warning("add_case() No config found for case '"<<_case->name()<<"'");
  }
  m_cases.push_back(_case);
}

SampleMap PseudoExperimentBase::create_sample(const std::string& exname) {
  auto espl = m_analysis.experiment_samples(m_experiment_set, exname, false);
  if(!m_channel.empty() && !espl.contains(m_channel)) {
    do_log_error("create_sample() No sample for channel '"<<m_channel<<"' experiment '"<<exname<<"'");
  }
  return espl;
}
