/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "SystExtrapolation.h"

using namespace sfi;

MatrixSqr<double> _norm_syst_matrix() {
  MatrixSqr<double> c(6);
  c.row(0,{0.9375, -0.9375, -0.4375, -0.4375, 0.0625, -0.0625});
  c.row(1,{1.5, 1.5, -0.5625, 0.5625, 0.0625, 0.0625});
  c.row(2,{-0.625, 0.625, 0.625, 0.625, -0.125, 0.125});
  c.row(3,{-1.5, -1.5, 0.875, -0.875, -0.125, -0.125});
  c.row(4,{0.1875, -0.1875, -0.187, -0.1875, 0.0625, -0.0625});
  c.row(5,{0.5, 0.5, -0.3125, 0.3125, 0.0625, 0.0625});
  return c;
}

MatrixSqr<double> _norm_syst_matrix4() {
  MatrixSqr<double> c(4);
  c.row(0,{-0.75, 0.75, -0.25, -0.25});
  c.row(1,{1, 1, 0.25, -0.25});
  c.row(2,{0.25, -0.25, 0.25, 0.25});
  c.row(3,{-0.5, -0.5, -0.25, 0.25});
  return c;
}

std::function<double(double)> sfi::norm_syst_extrap(double Im, double Ip) {
  const static MatrixSqr<double> c(_norm_syst_matrix());
  Vector<double> v{Ip-1,Im-1,Ip*ln(Ip),-Im*ln(Im),Ip*square(ln(Ip)),Im*square(ln(Im))};
  Vector<double> x(6);
  c.mult(v,x);
  return [x,Im,Ip](double z) {
    if(z > 1) return pow(Ip,z);
    else if(z < -1) return pow(Im,-z);
    double r = 1.0;
    double alpha=1.0;
    for(unsigned i=0;i<6;++i) {
      alpha *= z;
      r += x[i]*alpha;
    }
    return r;
  };
}

SystExtrapolation::SystExtrapolation():m_Im(0.0), m_Ip(0.0), m_x(6), m_v(6) { }

void SystExtrapolation::update(double Im, double Ip) {
  const static MatrixSqr<double> c(_norm_syst_matrix());
  m_Im = Im; m_Ip = Ip;
  m_v[0] = Ip-1; m_v[1] = Im-1; m_v[2] = Ip*ln(Ip); m_v[3] = -Im*ln(Im); m_v[4] = Ip*square(ln(Ip)); m_v[5] = Im*square(ln(Im));
  c.mult(m_v,m_x);
}

double SystExtrapolation::operator()(double z) const {
  if(z > 1) return pow(m_Ip,z);
  else if(z < -1) return pow(m_Im,-z);
  double r = 1.0;
  double alpha=1.0;
  for(unsigned i=0;i<6;++i) {
    alpha *= z;
    r += m_x[i]*alpha;
  }
  return r;
}

LogSystExtrapolation::LogSystExtrapolation():m_Im(0.0), m_Ip(0.0), m_x(4), m_v(4) { }

void LogSystExtrapolation::update(double Im, double Ip) {
  const static MatrixSqr<double> c(_norm_syst_matrix4());
  m_Im = Im; m_Ip = Ip;
  m_v[0] = Im - 1; m_v[1] = Ip - 1; m_v[2] = 1.0 - Im; m_v[3] = Ip - 1.0;
  c.mult(m_v,m_x);
}

double LogSystExtrapolation::operator()(double z) const {
  if(z > 1) return 1.0 + z * (m_Ip - 1.0);
  else if(z < -1) return 1.0 + z * (1.0 - m_Im);
  double r = 1.0;
  double alpha=1.0;
  for(unsigned i=0;i<4;++i) {
    alpha *= z;
    r += m_x[i]*alpha;
  }
  return r;
}
