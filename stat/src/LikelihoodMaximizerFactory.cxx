/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "LikelihoodMaximizerFactory.h"

#include "Context.h"
#include "ComponentHandler.h"

using namespace sfi;

LikelihoodMaximizerFactory::LikelihoodMaximizerFactory(OptionMap& opts, Context& ctx):m_factory(0) {
  m_factory = dynamic_cast<ILikelihoodMaximizerFactory*>(ctx.component_handler()->component(ctx,opts.get_as<std::string>("type"), opts.get_as<std::string>("library",""), opts));
}

ILikelihoodMaximizer* LikelihoodMaximizerFactory::create(Parameters&& pars, Context& ctx, const OptionMap& opts, ILikelihood* lh, const OptionMap& lhopts) {
  return m_factory->do_create(std::move(pars), ctx, opts, lh, lhopts);
}

ILikelihoodMaximizer* sfi::likelihood_maximizer(Analysis& ana, OptionMap& opts, ILikelihood* lh, const OptionMap& lhopts) {
  auto& ctx = ana.main_context();
  auto pars = ana.build_parameters(opts.get_as<std::string>("parameter_set"));
  return LikelihoodMaximizerFactory(opts, ctx).create(std::move(pars), ctx, opts, lh, lhopts);
}
