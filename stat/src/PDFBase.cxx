/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <PDFBase.h>

using namespace sfi;

constexpr bool _Debug = false;

PDFBase::~PDFBase() {}

double PDFBase::get_N(EvalPoint ep, const std::string& var, bool shift_down) {
  switch(ep) {
    case EvalInitial: return m_p_N->initial()*m_N*this->normalization_shift(ep, var, shift_down);
    case EvalCurrent: return m_p_N->work_value()*m_N*this->normalization_shift(ep, var, shift_down);
    case EvalML: return m_p_N->value()*m_N*this->normalization_shift(ep, var, shift_down);
    default: return 0.0;
  }
}

void PDFBase::do_print(std::ostream& out) {
  out<<m_name<<"(N[";
  for(auto& v : m_norm_variants) {
    out<<v.first<<" -"<<v.second.first<<" +"<<v.second.second<<" ";
  }
  out<<"])";
}

bool PDFBase::do_init_parameters(const OptionMap& opts, Parameters& pars) {
  for(auto& v : m_norm_variants) {
    std::string pname = "nu_"+v.first;
    if(!pars.has(pname) || (pars[pname].type() != Parameter::Type::Nuisance)) {
      throw PDFException("Undefined norm nuisance parameter '"+pname+"' for PDF '"+m_name+"'");
    }
    else {
      if(_Debug) {
        std::cout<<"PDFBase::do_init_parameters() name="<<m_name<<" norm variant '"<<pname<<"'"<<std::endl;
      }
    }
  }
  init_norm_parameter(opts, pars);
  return true;
}

PDFBase::PDFBase(Normalization norm, const std::string& name):m_norm_type(norm), m_name(name), m_N(1.0), m_p_N(0) {}

PDFBase::PDFBase(const std::string& name, const OptionMap& topts):m_norm_type(NormAbsolute), m_name(name), m_N(1.0), m_p_N(0) {
  if(topts.has_sub_vec(name+".norm_systematics")) {
    auto& ns = topts.sub_vec(name+".norm_systematics");
    for(unsigned i=0; i<ns.size(); ++i) {
      auto* nsys = dynamic_cast<const OptionMap*>(ns[i]);
      if(nsys) {
        if(nsys->has_type<double>("shift") && nsys->has_type<std::string>("name")) {
          double shift = nsys->get_as<double>("shift");
          auto sname = nsys->get<std::string>("name");
          if(starts_with(sname, "nu_")) {
            std::string varn = sname.substr(3);
            add_norm_systematics(varn, 1.0 - shift, 1.0 + shift);
            std::cout<<"PDFBase::init_parameters() Adding norm syst '"<<varn<<"' "<<shift<<std::endl;
          } else {
            add_norm_systematics(sname, 1.0 - shift, 1.0 + shift);
            std::cout<<"PDFBase::init_parameters() Adding norm syst '"<<sname<<"' "<<shift<<std::endl;
          }
        }
      }
      else {
        std::cout<<"PDFBase::init_parameters() Error: Norm systematics must be OptionMap {}"<<std::endl;
      }
    }
  }
}

double PDFBase::normalization_shift(EvalPoint, const std::string& var, bool shift_down) const {
  if(var.empty()) return 1.0;
  double s = 1.0;
  auto it = m_norm_variants.find(var);
  if(it != m_norm_variants.end()) s *= shift_down ? it->second.first : it->second.second;
  return s;
}

void PDFBase::add_norm_systematics(const std::string& var_name, double mu_down, double mu_up) {
  if(_Debug) {
    std::cout<<"PDFBase::add_norm_systematics() '"<<var_name<<"' -"<<mu_down<<" +"<<mu_up<<std::endl;
  }
  m_norm_variants[var_name] = std::make_pair(mu_down, mu_up);
}

void PDFBase::init_norm_parameter(const OptionMap& opts, Parameters& pars) {
  auto pname = opts.get<std::string>(m_name+".norm");
  if(!pars.has(pname)) throw PDFException("Undefined abs. norm '"+pname+"'");
  m_p_N = &pars[pname];

  if(m_p_N->type() == Parameter::Type::AbsoluteNorm) m_norm_type = Normalization::NormAbsolute;
  else if(m_p_N->type() == Parameter::Type::RelativeNorm) m_norm_type = Normalization::NormRelative;
  else throw PDFException("Undefined norm type '"+std::to_string((int)m_p_N->type())+"'");

  if(m_norm_type == NormAbsolute) m_N = 1.0;
  else {
    if(opts.has(m_name+".norm_expected"))  m_N = opts.get_as<double>(m_name+".norm_expected");
    else throw PDFException("Undefined 'norm_expected' for pdf '"+m_name+"'");
  }
  if(_Debug) {
    std::cout<<"PDFBase::init_norm_parameter() name="<<m_name<<" N="<<m_N<<std::endl;
  }
}

