/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <ExtendedLogLikelihood.h>
#include <CombinedLogLikelihood.h>
#include <ISample.h>
#include <TransformPDF.h>
#include <SlicePDF.h>
#include <ModelPDF.h>
#include <CompositePDF.h>

using namespace sfi;

static constexpr bool _Debug = false;

// ---- ExtendedLogLikelihood ----

ExtendedLogLikelihood::ExtendedLogLikelihood(const std::string& id, const ISample* sample, PDFBase* pdf):m_id(id), m_sample(sample), m_pdf(pdf) { }

ExtendedLogLikelihood::~ExtendedLogLikelihood() {
  delete m_pdf;
  m_pdf = nullptr;
}

bool ExtendedLogLikelihood::do_init_parameters(const OptionMap& opts, Parameters& pars) {
  bool all_ok = LogLikelihoodBase::do_init_parameters(opts, pars);
  return m_pdf->do_init_parameters(opts.sub("channels."+m_id), pars) && all_ok;
}

void ExtendedLogLikelihood::get_pdfs(std::vector<IPDF*>& pd) { m_pdf->get_pdfs(pd); }

void ExtendedLogLikelihood::do_print(std::ostream& out) const {
  out<<"-2ln({id: '"<<m_id<<"', sample: '"<<m_sample->get_name()<<"'} ";
  m_pdf->do_print(out);
  out<<" )";
  for(auto* np : m_nuisance_params) out<<" + "<<np->name()<<"^2";
}

bool ExtendedLogLikelihood::is_dependent_on_variant(const std::string& var) const {
  return m_pdf->is_dependent_on_variant(var);
}

void ExtendedLogLikelihood::do_update_parameters(EvalPoint) {
  m_pdf->do_update_parameters(EvalPoint::EvalCurrent);
}

double ExtendedLogLikelihood::do_evaluate(Context&, const std::string& var, bool shift_down) {
  return eval(var, shift_down);
}

double ExtendedLogLikelihood::eval(const std::string& var, bool shift_down) {
  auto& splm = m_sample->get_matrix();
  const unsigned Nev(splm.rows());
  double N = m_pdf->get_N(EvalPoint::EvalCurrent, var, shift_down);
  double f = N - double(Nev)*ln(N);
  for(unsigned i=0; i<Nev; ++i) f -= ln(m_pdf->do_eval(splm.row_data(i), var, shift_down));
  if(_Debug) {
    std::cout<<"ExtendedLogLikelihood::eval() var: '"<<var<<"' down: "<<shift_down<<" N: "<<N<<" => "<<(2.0*f)<<std::endl;
  }
  return 2.0*f;
}

namespace sfi {
  PDFBase* create_pdf(Analysis& ana, const Analysis::ParameterSet& pset, const OptionMap& popts, const OptionMap& opts, ISample& spl) {
    auto normn = opts.get<std::string>("norm");
    auto* par = pset[normn];
    if(!par) {
      log_error(ana.log(), "create_pdf() No parameter '"<<normn<<"' found");
      return nullptr;
    }
    auto* trf = ana.transform(opts.name());
    if(!trf) {
      log_error(ana.log(), "create_pdf() No transform '"<<opts.name()<<"' found");
      return nullptr;
    }
    PDFBase* pdf = 0;
    auto* mtrf = dynamic_cast<IModelTransform*>(trf);
    if(mtrf) pdf = new ModelPDF(*mtrf, popts);
    else {
      if(trf->get_dimensions() == spl.get_dim()) pdf = new TransformPDF(*trf, popts);
      else pdf = new SlicePDF(spl.get_dim()-1, *trf, popts);
    }
    return pdf;
  }

  LogLikelihoodBase* create_likelihood(Analysis& ana, const Analysis::ParameterSet& pset, SampleMap& spls, const OptionMap& opts) {
    auto* res = new CombinedLogLikelihood();
    if(opts.has_sub("channels")) {
      auto& cho = opts.sub("channels");
      for(auto& ch : cho) {
        auto* cho = dynamic_cast<OptionMap*>(ch.second);
        if(cho) {
          auto& chname = ch.first;
          if(spls.contains(chname)) {
            auto* spl = spls[chname];

            if(cho->size() == 1) {
              auto* pdfo = dynamic_cast<OptionMap*>(cho->begin()->second);
              if(pdfo) {
                res->add(new ExtendedLogLikelihood(chname, spl, create_pdf(ana, pset, *cho, *pdfo, *spl)));
              } else {
                log_error(ana.log(), "create_likelihood() channel '"<<ch.first<<"' pdf '"<<cho->begin()->first<<"' no OptionMap {}");
              }
            }
            else {
              auto* cpdf = new CompositePDF();
              for(auto& pdfo : *cho) {
                auto* pdfom = dynamic_cast<OptionMap*>(pdfo.second);
                if(pdfom) {
                  cpdf->add(create_pdf(ana, pset, *cho, *pdfom, *spl));
                } else {
                  log_error(ana.log(), "create_likelihood() channel '"<<ch.first<<"' pdf '"<<pdfo.first<<"' no OptionMap {}");
                }
              }
              res->add(new ExtendedLogLikelihood(chname, spl, cpdf));
            }
          } else {
            log_error(ana.log(), "create_likelihood() No sample for channel '"<<chname<<"'");
          }
        } else {
          log_error(ana.log(), "create_likelihood() channel '"<<ch.first<<"' no OptionMap {}");
        }
      }
    }
    for(auto& pp : pset) {
      auto& p = pp.second;
      if((p.type() == Parameter::Nuisance) && !p.fixed()) {
        if(starts_with(p.name(), "nu_")) {
          std::string varn = p.name().substr(3);
          log_debug(ana.log(), "create_likelihood() var: '"<<varn<<"' dep: "<<res->is_dependent_on_variant(varn));
          if(res->is_dependent_on_variant(varn)) res->add_variant(varn);
        } else {
          log_debug(ana.log(), "create_likelihood() var: '"<<p.name()<<"' dep: "<<res->is_dependent_on_variant(p.name()));
          if(res->is_dependent_on_variant(p.name())) res->add_variant(p.name());
        }
      }
    }
    return res;
  }

  ILikelihood* extended_log_likelihood(Analysis& ana, SampleMap& spls, const std::string& param_set, const OptionMap& opts) {
    auto& pset = ana.parameter_set(param_set);
    return create_likelihood(ana,pset,spls,opts);
  }
}
