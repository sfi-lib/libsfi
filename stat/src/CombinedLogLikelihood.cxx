/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <CombinedLogLikelihood.h>

using namespace sfi;

CombinedLogLikelihood::CombinedLogLikelihood():m_lhoods() {}

CombinedLogLikelihood::CombinedLogLikelihood(const std::initializer_list<LogLikelihoodBase*>& lh):m_lhoods(lh) {}

CombinedLogLikelihood::~CombinedLogLikelihood() {
  for(auto* lh : m_lhoods) delete lh;
  m_lhoods.clear();
}

void CombinedLogLikelihood::add(LogLikelihoodBase* lh) { m_lhoods.push_back(lh); }

bool CombinedLogLikelihood::do_init_parameters(const OptionMap& opts, Parameters& pars) {
  bool all_ok = LogLikelihoodBase::do_init_parameters(opts, pars);
  for(auto* lh : m_lhoods) all_ok = lh->do_init_parameters(opts, pars) && all_ok;
  return all_ok;
}

void CombinedLogLikelihood::get_pdfs(std::vector<IPDF*>& pd) {
  for(auto* lh : m_lhoods) lh->get_pdfs(pd);
}

void CombinedLogLikelihood::do_print(std::ostream& out) const {
  bool first = true;
  for(auto* lh : m_lhoods) {
    if(!first) out<<" + ";
    else first = false;
    lh->do_print(out);
  }
  for(auto* np : m_nuisance_params) out<<" + "<<np->name()<<"^2";
}

void CombinedLogLikelihood::do_update_parameters(EvalPoint ep) {
  for(auto* lh : m_lhoods) lh->do_update_parameters(ep);
}

bool CombinedLogLikelihood::is_dependent_on_variant(const std::string& var) const {
  for(auto* lh : m_lhoods) if(lh->is_dependent_on_variant(var)) return true;
  return false;
}

double CombinedLogLikelihood::do_evaluate(Context& ctx, const std::string& var, bool shift_down) {
  return eval(ctx, var, shift_down);
}

double CombinedLogLikelihood::eval(Context& ctx, const std::string& var, bool shift_down) {
  double lnL = 0.0;
  for(auto* lh : m_lhoods) lnL += lh->do_evaluate(ctx, var, shift_down);
  return lnL;
}
