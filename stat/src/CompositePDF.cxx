/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <CompositePDF.h>

using namespace sfi;

CompositePDF::CompositePDF():PDFBase(NormAbsolute, "{}") {}

CompositePDF::CompositePDF(const std::initializer_list<PDFBase*>& pdfs):PDFBase(NormAbsolute, "{}"), m_pdfs(pdfs){}

CompositePDF::~CompositePDF() {
  for(auto* pd : m_pdfs) delete pd;
  m_pdfs.clear();
}

void CompositePDF::add(PDFBase* pdf) { m_pdfs.push_back(pdf); }

double CompositePDF::get_N(EvalPoint ep, const std::string& var, bool shift_down) {
  double res = 0;
  for(auto* pdf : m_pdfs) res += pdf->get_N(ep, var, shift_down);
  return res;
}

IEvaluator* CompositePDF::get_evaluator() { return 0; }

const ITransform* CompositePDF::get_transform() { return 0; }

void CompositePDF::do_update_parameters(EvalPoint ep) {
  for(auto* pdf : m_pdfs) pdf->do_update_parameters(ep);
}

void CompositePDF::do_print(std::ostream& out) {
  for(auto* pdf : m_pdfs) pdf->do_print(out);
}

bool CompositePDF::is_dependent_on_variant(const std::string& var) const {
  for(auto* pdf : m_pdfs) {
    if(pdf->is_dependent_on_variant(var)) return true;
  }
  return false;
}

double CompositePDF::do_eval(const double*x, const std::string& var, bool shift_down) {
  double p(0), Ntot(0);
  for(auto* pdf : m_pdfs) {
    double N = pdf->get_N(EvalPoint::EvalCurrent, var, shift_down);
    Ntot += N;
    p += N*pdf->do_eval(x, var, shift_down);
  }
  return p/Ntot;
}

bool CompositePDF::do_init_parameters(const OptionMap& opts, Parameters& pars) {
  for(auto* pdf : m_pdfs) pdf->do_init_parameters(opts, pars);
  return false;
}

void CompositePDF::get_pdfs(std::vector<IPDF*>& pd) {
  for(auto* pdf : m_pdfs) pdf->get_pdfs(pd);
}
