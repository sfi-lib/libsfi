/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <LogLikelihoodBase.h>

using namespace sfi;

static constexpr bool _Debug = false;

LogLikelihoodBase::~LogLikelihoodBase() {}

double LogLikelihoodBase::eval_constraints() {
  double lnL = 0;
  for(auto* np : m_nuisance_params) {
    double alpha = np->work_value();
    //lnL += alpha*alpha*0.5;
    lnL += alpha*alpha;
  }
  return lnL;
}

double LogLikelihoodBase::do_eval(Context& ctx) {
  do_update_parameters(EvalPoint::EvalCurrent);
  const double L0 = do_evaluate(ctx, "",false);
  double L = L0;
  if(_Debug) {
    std::cout<<"LogLikelihoodBase::do_eval() L0: "<<L0<<std::endl;
  }
  for(unsigned i=0;i<m_variant_names.size(); ++i) {
    auto& np = m_variant_names[i];
    if(is_dependent_on_variant(np)) {
      const double Lm = do_evaluate(ctx, np,true);
      const double Lp = do_evaluate(ctx, np,false);
      m_syst_extrap.update(Lm/L0, Lp/L0);
      L *= m_syst_extrap(m_nuisance_params[i]->work_value());
      if(_Debug) {
        std::cout<<"LogLikelihoodBase::do_eval() var: "<<np<<" val: "<<m_nuisance_params[i]->work_value()<<" Lm: "<<Lm<<" Lp:"<<Lp<<" -> L="<<L<<std::endl;
      }
    }
  }
  double co = eval_constraints();
  L += co;
  if(_Debug) {
    std::cout<<"LogLikelihoodBase::do_eval() constraints: "<<co<<" L="<<L<<std::endl;
  }

  return L;
}

bool LogLikelihoodBase::do_init_parameters(const OptionMap&, Parameters& pars) {
  if(_Debug) {
    std::cout<<"LogLikelihoodBase::do_init_parameters() adding "<<m_variant_names.size()<<" nuisance params!"<<std::endl;
  }
  for(auto& v : m_variant_names) {
    std::string pname = "nu_"+v;
    if(pars.has(pname)) {
      m_nuisance_params.push_back(&pars[pname]);
      if(_Debug) {
        std::cout<<"LogLikelihoodBase::do_init_parameters() Adding nuisance parameter '"<<pname<<"'"<<std::endl;
      }
    }
    else throw PDFException("Undefined nuisance parameter '"+pname+"'");
  }
  return true;
}

void LogLikelihoodBase::variants(const std::vector<std::string>& var)  {
  for(auto& v : var) m_variant_names.push_back(v);
}

void LogLikelihoodBase::add_variant(const std::string& n) {
  m_variant_names.push_back(n);
}
