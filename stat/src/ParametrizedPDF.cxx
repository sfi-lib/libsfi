/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <ParametrizedPDF.h>
#include <Variables.h>

using namespace sfi;

constexpr bool _Debug = false;

ParametrizedPDF::~ParametrizedPDF() {}

void ParametrizedPDF::do_update_parameters(EvalPoint ep) {
  PDFBase::do_update_parameters(ep);
  const size_t npar = m_params.size();
  if(ep == EvalInitial) for(size_t i=0; i<npar; ++i) m_values[i] = m_params[i]->initial();
  else if(ep == EvalCurrent) for(size_t i=0; i<npar; ++i) m_values[i] = m_params[i]->work_value();
  else if(ep == EvalML) for(size_t i=0; i<npar; ++i) m_values[i] = m_params[i]->value();
  // TODO: Error
  else return;
}

ParametrizedPDF::ParametrizedPDF(Normalization norm, const std::string& name):PDFBase(norm, name) { }

ParametrizedPDF::ParametrizedPDF(const std::string& name, const OptionMap& topts):PDFBase(name, topts) {
  if(topts.has_type<std::string>(name+".shape")) {
    m_shape_variant = topts.get_as<std::string>(name+".shape");
    m_variants[m_shape_variant] = 0;
    if(_Debug) {
      std::cout<<"ParametrizedPDF() adding shape param: '"<<m_shape_variant<<"'"<<std::endl;
    }
  }
}

bool ParametrizedPDF::is_dependent_on_variant(const std::string& var) const {
  return PDFBase::is_dependent_on_variant(var) || (m_variants.find(var) != m_variants.end());
}

bool ParametrizedPDF::init_parameters(const OptionMap& opts, Parameters& pars, IEvaluator& eval) {
  PDFBase::do_init_parameters(opts, pars);
  auto& topts = opts.sub(m_name);

  auto* trf = eval.get_transform();
  unsigned sd = eval.get_dimensions();
  if(sd > trf->get_dimensions()) throw PDFException("Evaluator for transform '"+m_name+"' has "+std::to_string(sd)+" dims while the transform has only "+std::to_string(trf->get_dimensions()));

  unsigned nparams = eval.get_nslice_parameters();
  auto tvars = trf->get_variables();
  m_params.resize(nparams,0);
  m_values.resize(nparams,0);

  for(size_t i=0; i<nparams; ++i) {
    auto& var = tvars->at(sd+i);
    auto& vname = topts.get<std::string>(var.name());
    if(pars.has(vname)) {
      m_params[i] = &pars[vname];
      if(_Debug) {
        std::cout<<"ParametrizedPDF::init_parameters() Variable '"<<m_name<<"."<<var.name()<<"' to parameter '"<<vname<<"'"<<std::endl;
      }
    }
    else throw PDFException("Undefined slice parameter '"+vname+"' for transform '"+m_name+"'");
  }
  return true;
}

void ParametrizedPDF::init_parameters(Parameters& pars, const OptionMap& vars) {
  PDFBase::do_init_parameters(vars, pars);
  if(vars.has_type<std::vector<std::string>>(m_name+".parameters")) {
    auto& parv = vars.get<std::vector<std::string>>(m_name+".parameters");
    for(auto vi : parv) {
      if(pars.has(vi)) m_params.push_back(&pars[vi]);
      else throw PDFException("Undefined parameter '"+vi+"' for PDF '"+m_name+"'");
    }
    m_values.resize(m_params.size(), 0);
  }
}
