#include <Transform.h>
#include <ProjectionTransformation.h>
#include <cosine.h>
#include <sparse_system.h>
#include <AmplitudeTransformation.h>
#include <transform_functions.h>
#include <Analysis.h>
#include <PDF1DComparison.h>
#include <TransformExportCpp.h>

#include <TApplication.h>

using namespace sfi;

constexpr const unsigned NDims = 2;

void exttest(OptionMap& opts) {
  Analysis ana(opts);
  using Transf = t_Transform<tdimensions<NDims>, EigenMonomicSparse, bases::Cosine>;
  AmplitudeTransformation<ProjectionTransformation<Transf>> ptrfo(opts.sub("transformations.ps"));
  auto* spl = ana.channel_sample<NDims, true>("in", "S", true);
  auto* trf = perform_transformation(ana.main_context(), *spl, ptrfo, AllEventSel());

  std::cout<<"I:"<<trf->integral_pdf()<<std::endl;

  auto trf1D = transform_set_2D(perform_single_transformations(ana.main_context(), *spl, ptrfo, AllEventSel()));

  PDF1DComparison pdfs("pdf");
  pdfs.normalize(false).plot_external_coordinates(false).n_pdf_bins(100).transforms({trf}).samples(sample_set({spl}), AllEventSel());
  pdfs.file_prefix("pdf_").transforms_1D(trf1D.generic()).plot();

  TransformExportCpp expo(opts.get_as<std::string>("output_prefix"));
  expo.do_export(*trf);
}

int
main(int argc,char** argv) {
  Options opts;
  opts.parse_options(argc, argv);
  if(opts.parse_opts_file("config/exttest.opts")) {
    TApplication app(argv[0], &argc, argv);
    exttest(opts);
    app.Run();
    return 0;
  }
  else return 1;
}
