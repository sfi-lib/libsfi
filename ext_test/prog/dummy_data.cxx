#include <Analysis.h>

#include <TRandom3.h>

using namespace sfi;

using SampleTy = Sample<2,ttrue>;

SampleTy* create_sample(Analysis& ana, TRandom3& rnd, unsigned Nevents) {
  SampleTy* res = ana.memory_pool().pool<SampleTy>()->get(Nevents,"");
  for(size_t e = 0; e<Nevents; ++e) {
    auto& data = res->data(e);
    double x = rnd.Uniform(-1,1);
    double y = rnd.Uniform(-1,1);
    data.weight(gaus(x, 1, 0.1)*gaus(y, -2, 0.2));
    data[0] = x;
    data[1] = y;
  }
  auto vars = std::make_shared<Variables>(2);
  vars->at(0).external_interval(-1, 1);
  vars->at(1).external_interval(-1, 1);
  res->variables(vars);
  res->is_normalized(true);
  return res;
}

int
main(int argc,char** argv) {
  Options opts;
  std::string filen = "dummy_data.txt";
  unsigned Nevents = 10000;
  opts.bind("file_name", filen).bind("Nevents", Nevents);
  opts.parse_options(argc, argv);
  Analysis ana("a");
  TRandom3 rnd;
  auto* spl = create_sample(ana, rnd, Nevents);
  spl->do_write(filen);
  spl->release();
  std::cout<<"Written "<<Nevents<<" events to file '"<<filen<<"'"<<std::endl;
  return 0;
}
