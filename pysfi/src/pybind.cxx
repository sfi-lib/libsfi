/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string.h>

#include "DTransform.h"
#include "ProjectionTransformation.h"
#include "AmplitudeTransformation.h"
#include "ITransformation.h"
#include "WTransformation.h"
#include "Sample.h"
#include "Analysis.h"
#include "sparse_system.h"
#include "full_system.h"
#include "ISampleBuilder.h"

using namespace sfi;
using namespace sfi::bases;

const size_t MaxDims = 6;
using HistogramTy = Histogram<true,double>;

ITransformation* _create_transformation(Context& ctx, const std::string&name_suffix, OptionMap* opts, int trans_type, int dims, bool sparse, int basis_type) {
  ITransformation* res = nullptr;
  if(trans_type == 0) {
    if(opts) res = new WTransformation<ProjectionTransformation<DTransform>>(ctx, *opts);
    else res = new WTransformation<ProjectionTransformation<DTransform>>(ctx, name_suffix);
  }
  else {
    if(opts) res = new WTransformation<AmplitudeTransformation<ProjectionTransformation<DTransform>>>(ctx, *opts);
    else res = new WTransformation<AmplitudeTransformation<ProjectionTransformation<DTransform>>>(ctx, name_suffix);
  }
  std::string ttype = sparse ? "sparse" : "full";
  std::string tbasis;
  switch(basis_type) {
    case 0 : { tbasis = "cos_on"; break; }
    case 1 : { tbasis = "legendre_on"; break; }
    case 2 : { tbasis = "fourier_on"; break; }
    case 3 : { tbasis = "ylm_on"; break; }
  }
  auto* topts = res->get_sub_transformation() ?  res->get_sub_transformation()->get_options() : res->get_options();

  DTransform trf(name_suffix, DTransform::Eig(dims,0,tbasis, ttype));
  trf.write(*topts);
  return res;
}

using _SampleSet_ = std::vector<ISample*>;
template<bool _is_weighted, unsigned _dims>
_SampleSet_* analysis_channel_samples_impl2(Analysis* ana, const std::string& cname, bool sort_by_index = false, bool normalize_vars = true) {
  auto spls = ana->channel_samples<_dims, _is_weighted>(cname, sort_by_index, normalize_vars);
  auto* res = new std::vector<ISample*>(spls.size(),0);
  for(unsigned i=0; i<spls.size(); ++i) (*res)[i] = &spls[i];
  spls.detach_objects();
  return res;
}

template<bool _is_weighted>
_SampleSet_* analysis_channel_samples_impl(int dims, Analysis* ana, const std::string& cname, bool sort_by_index = false, bool normalize_vars = true) {
  if((dims < 1) || (((size_t)dims) > MaxDims)) return 0;
  switch(dims) {
  case 1: return analysis_channel_samples_impl2<_is_weighted, 1>(ana, cname, sort_by_index, normalize_vars);
  case 2: return analysis_channel_samples_impl2<_is_weighted, 2>(ana, cname, sort_by_index, normalize_vars);
  case 3: return analysis_channel_samples_impl2<_is_weighted, 3>(ana, cname, sort_by_index, normalize_vars);
  case 4: return analysis_channel_samples_impl2<_is_weighted, 4>(ana, cname, sort_by_index, normalize_vars);
  case 5: return analysis_channel_samples_impl2<_is_weighted, 5>(ana, cname, sort_by_index, normalize_vars);
  default: return analysis_channel_samples_impl2<_is_weighted, 6>(ana, cname, sort_by_index, normalize_vars);
  }
}

extern "C" {
  // ----- functions on Sample -----

  void sample_release(ISample* spl) { spl->do_release(); }

  ISample* sample_create(Context* ctx, Variables* vars, int nevents, double* data) {
    const int dims = vars->nvariables();
    if((dims < 1) || (((size_t)dims) > MaxDims)) return 0;
    switch(dims) {
    case 1: return ctx->memory_pool().pool<Sample<1, tfalse>>()->get(nevents, data, "", std::shared_ptr<Variables>(vars));
    case 2: return ctx->memory_pool().pool<Sample<2, tfalse>>()->get(nevents, data, "", std::shared_ptr<Variables>(vars));
    case 3: return ctx->memory_pool().pool<Sample<3, tfalse>>()->get(nevents, data, "", std::shared_ptr<Variables>(vars));
    case 4: return ctx->memory_pool().pool<Sample<4, tfalse>>()->get(nevents, data, "", std::shared_ptr<Variables>(vars));
    case 5: return ctx->memory_pool().pool<Sample<5, tfalse>>()->get(nevents, data, "", std::shared_ptr<Variables>(vars));
    case 6: return ctx->memory_pool().pool<Sample<6, tfalse>>()->get(nevents, data, "", std::shared_ptr<Variables>(vars));
    }
    std::cout<<"pybind::sample_create() Invalid dimensions "<<dims<<std::endl;
    return 0;
  }

  const char* sample_get_name(ISample* spl) { return spl->get_name().c_str(); }

  void sample_set_name(ISample* spl, const char* name) { spl->set_name(std::string(name)); }

  bool sample_get_use_weight(ISample* spl) { return spl->get_use_weight(); }

  int sample_get_events(ISample* spl) { return spl->get_events(); }

  bool sample_get_is_normalized(ISample* spl) { return spl->get_is_normalized(); }

  int sample_get_dimensions(ISample* spl) { return spl->get_dim(); }

  bool sample_get_owns_data(ISample* spl) { return spl->get_owns_data(); }

  Variables* sample_get_variables(ISample* spl) { return spl->get_variables().get(); }

  void sample_normalize_variables(Analysis* ana, ISample* spl) {
    spl->do_normalize(ana->log());
  }

  HistogramTy* sample_get_histogram_1D(ISample* spl, int var, int nbins, bool external_coordinates, int esel) {
    if((var >= 0) && (var < (int)spl->get_dim())) {
      return new HistogramTy(spl->get_histogram_1D(var, nbins, external_coordinates, (EventSelectorID)esel));
    }
    else return 0;
  }

/*
      virtual const double* get_x(unsigned ev) const  = 0;
      virtual double get_x(unsigned ev, unsigned d) const = 0;
      virtual double& get_x(unsigned ev, unsigned d) = 0;

      virtual double get_w(unsigned ev) const = 0;
      virtual double& get_w(unsigned ev) = 0;
*/

  // ----- functions on Transform -----

  void transform_release(ITransform* tr) { delete tr; }

  const char* transform_get_name(ITransform* trf) { return trf->get_name().c_str(); }

  const char* transform_get_type_name(ITransform* trf) { return trf->get_type_name().c_str(); }

  const char* transform_get_basis_name(ITransform* trf) { return trf->get_basis_name().c_str(); }

  int transform_get_dimensions(ITransform* trf) { return trf->get_dimensions(); }

  int transform_get_max_degree(ITransform* trf) { return trf->get_max_degree(); }

  int transform_get_nparams(ITransform* trf) { return trf->get_nparams(); }

  bool transform_get_is_amplitude(ITransform* trf) { return trf->get_is_amplitude(); }

  void transform_set_is_amplitude(ITransform* trf, bool isa) { trf->set_is_amplitude(isa); }

  double transform_get_param(ITransform* trf, int i) { return trf->get_param(i).value(); }

  void transform_set_param(ITransform* trf, int i, double p) { trf->set_param(i, p); }

  IEvaluator* transform_get_evaluator(ITransform* trf, double scale, bool external_coordinates) {
    return trf->get_evaluator(scale, external_coordinates).release();
  }

  double transform_get_integral(ITransform* trf) { return trf->get_integral(); }

  ITransform* transform_get_axis_transform(ITransform* trf, int d) { return trf->get_axis_transform(d); }

  void transform_do_normalize(ITransform* trf, double n) { trf->do_normalize(n); }

  const Variables* transform_get_variables(ITransform* trf) { return trf->get_variables().get(); }

  void transform_set_variables(ITransform* trf, Variables* v) { trf->set_variables(std::shared_ptr<Variables>(v)); }

  bool transform_do_eval(ITransform* trf, int npts, const double *xv, double* res, double scale, bool external_coordinates) {
    return trf->do_eval(npts, xv, res, scale, external_coordinates);
  }

  // ----- functions on Transformation -----

  void transformation_release(ITransformation* tr) { delete tr; }

  /**
   * @param trans_type : 0 linear, 1 amplitude
   * @param dims
   * @params sparse
   * @param basis_type : 0 cos, 1 legendre, 3 fourier, 4 Ylm
   */
  ITransformation* transformation_create(Analysis* ana, int trans_type, int dims, bool sparse, int basis_type) {
    if((trans_type < 0) || (trans_type > 1)) return 0;
    if((dims < 1) || (((size_t)dims) > MaxDims)) return 0;
    std::string name_suffix = "_"+std::to_string(dims)+"D";
    return _create_transformation(ana->main_context(), name_suffix, 0, trans_type, dims, sparse, basis_type);
  }

  ITransformation* transformation_create_from_options(Analysis* ana, OptionMap* opts, int trans_type, int dims, bool sparse, int basis_type) {
      if((trans_type < 0) || (trans_type > 1)) return 0;
      if((dims < 1) || (((size_t)dims) > MaxDims)) return 0;
      return _create_transformation(ana->main_context(), "", opts, trans_type, dims, sparse, basis_type);
    }

  const char* transformation_get_name(ITransformation* tr) { return tr->get_name().c_str(); }

  const char* transformation_get_type_name(ITransformation* tr) { return tr->get_type_name().c_str(); }

  OptionMap* transformation_get_options(ITransformation* tr) { return tr->get_options(); }

  void transformation_set_print_level(ITransformation* tr, int lvl) { tr->set_print_level(lvl); }

  bool transformation_do_transform(ITransformation* tr, Context* ctx, ISample* spl, int esel, ITransform* res) {
    return tr->do_transform(ctx, spl, esel, res);
  }

  ITransform* transformation_create_transform(ITransformation* tr, Context* ctx, int degrees) {
    auto* opts = tr->get_sub_transformation() ?  tr->get_sub_transformation()->get_options() : tr->get_options();
    auto& ev = opts->sub("eigensystem.eigenv");
    ev.set("degree", degrees);
    return tr->do_create_transform(ctx, degrees);
  }

  // ----- functions on Evaluator -----

  void evaluator_release(IEvaluator* ev) { delete ev; }

  // ----- functions on Variables -----

  Variables* variables_create(int vars) { return new Variables(vars); }

  void variables_release(Variables* vars) { delete vars; }

  int variables_nvariables(Variables* vars) { return vars->nvariables(); }

  const char* variables_get_name(Variables* vars, int vn) {
    return (vn < (int)vars->nvariables()) ? vars->name(vn).c_str() : 0;
  }

  void variables_set_name(Variables* vars, int vn, const char* name) {
    if(vn < (int)vars->nvariables()) vars->at(vn).name(name);
  }

  const char* variables_get_title(Variables* vars, int vn) {
    return (vn < (int)vars->nvariables()) ? vars->at(vn).title().c_str() : 0;
  }

  void variables_set_title(Variables* vars, int vn, const char* name) {
    if(vn < (int)vars->nvariables()) vars->at(vn).title(name);
  }

  void variables_set_external_interval(Variables* vars, int vn, double _min, double _max) {
    if(vn < (int)vars->nvariables()) vars->at(vn).external_interval(_min, _max);
  }

  double variables_get_external_min(Variables* vars, int vn) {
    if(vn < (int)vars->nvariables()) return vars->at(vn).minval();
    else return 0.0;
  }

  double variables_get_external_max(Variables* vars, int vn) {
    if(vn < (int)vars->nvariables()) return vars->at(vn).maxval();
    else return 0.0;
  }

  void variables_set_intermediary_interval(Variables* vars, int vn, double _min, double _max) {
    if(vn < (int)vars->nvariables()) vars->at(vn).intermediary_interval(_min, _max);
  }

  int variables_get_trans_type(Variables* vars, int vn) {
    return (vn < (int)vars->nvariables()) ? vars->at(vn).trans_type() : -1;
  }

  void variables_set_trans_type(Variables* vars, int vn, int tr) {
    if(vn < (int)vars->nvariables()) vars->at(vn).trans_type((Variable::TransType)tr);
  }

  void variables_lock(Variables* vars) {
    for(int v=0; v<(int)vars->nvariables(); ++v) vars->at(v).lock();
  }

  char* variables_print(Variables* vars) {
    std::ostringstream sout;
    sout<<(*vars);
    return ::strdup(sout.str().c_str());
  }

  // ----- functions on Options -----

  OptionMap* options_create() { return new OptionMap; }

  OptionMap* options_read(const char* filen) {
    auto* opts = new Options;
    opts->parse_opts_file(filen);
    return opts;
  }

  void options_release(OptionMap* opts) { delete opts; }

  int options_get_size(OptionMap* opts) { return opts->size(); }

  void options_get_names(OptionMap* opts, const char** opn, int nnames) {
    if(((size_t)nnames) == opts->size()) {
      unsigned n(0);
      for(auto& op : (*opts)) {
        opn[n] = op.first.c_str();
        ++n;
      }
    }
    else {
      std::cout<<"pybind::options_get_names() invalid array size"<<std::endl;
    }
  }

  void options_set_int(OptionMap* opts, const char* opn, int value) { opts->set(opn, value); }

  void options_set_bool(OptionMap* opts, const char* opn, bool value) { opts->set(opn, value); }

  void options_set_double(OptionMap* opts, const char* opn, double value) { opts->set(opn, value); }

  void options_set_string(OptionMap* opts, const char* opn, const char* value) { opts->set(opn, std::string(value)); }

  Option* options_get(OptionMap* opts, const char* opn) {
    if(opts->has(opn)) return &(*opts)[opn];
    else return 0;
  }

  OptionMap* options_sub(OptionMap* opts, const char* opn) { return &opts->sub(opn, true); }

  char* options_print(OptionMap* opts) {
    std::ostringstream sout;
    opts->do_print(sout);
    return ::strdup(sout.str().c_str());
  }

  /**
   * Determine the type of the Option
   * 0 : Unsupported
   * 1 : int
   * 2 : bool
   * 3 : double
   * 4 : string
   * 5 : OptionMap
   *
   * TODO: support vectors
   */
  enum OptType {
    Unsupported=0, Int=1, Bool=2, Double=3, String=4, Object=5
  };
  OptType option_type(Option* opt) {
    auto ot = opt->get_type();
    if(ot == Option::Type::Object) return OptType::Object;
    else if(ot == Option::Type::Value) {
      auto& ti = opt->get_type_info();
      if(ti == typeid(int)) return OptType::Int;
      else if(ti == typeid(bool)) return OptType::Bool;
      else if(ti == typeid(double)) return OptType::Double;
      else if(ti == typeid(std::string)) return OptType::String;
    }
    return OptType::Unsupported;
  }

  int option_value_int(Option* _opt) {
    auto* opt = dynamic_cast<OptionBase<int>*>(_opt);
    if(opt) return opt->get_value();
    else {
      std::cout<<"Option '"<<*_opt<<"' not of type int"<<std::endl;
      return 0;
    }
  }

  bool option_value_bool(Option* _opt) {
    auto* opt = dynamic_cast<OptionBase<bool>*>(_opt);
    if(opt) return opt->get_value();
    else {
      std::cout<<"Option '"<<*_opt<<"' not of type bool"<<std::endl;
      return 0;
    }
  }

  double option_value_double(Option* _opt) {
    auto* opt = dynamic_cast<OptionBase<double>*>(_opt);
    if(opt) return opt->get_value();
    else {
      std::cout<<"Option '"<<*_opt<<"' not of type double"<<std::endl;
      return 0;
    }
  }

  const char* option_value_string(Option* _opt) {
    auto* opt = dynamic_cast<OptionBase<std::string>*>(_opt);
    if(opt) return opt->get_value().c_str();
    else {
      std::cout<<"Option '"<<*_opt<<"' not of type string"<<std::endl;
      return 0;
    }
  }

  OptionMap* option_value_object(Option* _opt) { return dynamic_cast<OptionMap*>(_opt); }

  // ----- functions on Analysis -----

  Analysis* analysis_create_named(const char* name) { return new Analysis(name); }

  Analysis* analysis_create_initialized(OptionMap* opts) { return new Analysis(*opts); }

  const char* analysis_name(Analysis* ana) { return ana->name().c_str(); }

  const OptionMap* analysis_options(Analysis* ana) { return &ana->options(); }

  Context* analysis_context(Analysis* ana) { return &ana->main_context(); }

  AnalysisChannel* analysis_channel(Analysis* ana, const char* cname) {
    if(ana->has_channel(cname)) return &ana->channel(cname);
    else return 0;
  }

  _SampleSet_* analysis_channel_samples(Analysis* ana, const char* cname, bool sort_by_index = false, bool normalize_vars = true) {
    if(ana->has_channel(cname)) {
      auto& ch = ana->channel(cname);
      ISampleBuilder* sb = ch.sample_builder();
      if(sb) {
        std::cout<<"analysis_channel_samples() "<<std::string(cname)<<" name="<<sb->get_name()<<" dims="<<sb->get_dims()<<" weight="<<sb->get_is_weighted()<<std::endl;
        if(sb->get_is_weighted()) return analysis_channel_samples_impl<true>(sb->get_dims(), ana, cname, sort_by_index, normalize_vars);
        else return analysis_channel_samples_impl<false>(sb->get_dims(), ana, cname, sort_by_index, normalize_vars);
      }
    }
    std::cout<<"Error: Can't get samples for channel '"<<cname<<"'"<<std::endl;
    return 0;
  }

  void analysis_release(Analysis* ana) { delete ana; }

  // ----- functions on AnalysisChannel -----

  const char* analysis_channel_name(AnalysisChannel* ach) { return ach->name().c_str(); }

  Variables* analysis_channel_variables(AnalysisChannel* ach) { return ach->variables().get(); }

  // ----- functions on SampleSet (i.e. vector<ISample>) -----

  void sample_set_release(_SampleSet_* spls) { delete spls; }

  int sample_set_size(_SampleSet_* spls) { return spls->size(); }

  ISample* sample_set_get(_SampleSet_* spls, int idx) { return (*spls)[idx]; }

  // ----- functions on Histogram -----

  double histogram_min_x(HistogramTy* hi) { return hi->min_x(); }

  double histogram_max_x(HistogramTy* hi) { return hi->max_x(); }

  int histogram_nbins(HistogramTy* hi) { return hi->nbins(); }

  int histogram_entries(HistogramTy* hi) { return hi->entries(); }

  void histogram_fill(HistogramTy* hi, double x, double w) { hi->fill(x,w); }

  double histogram_bin_content(HistogramTy* hi, int i) { return hi->bin_content(i); }

  double histogram_bin_uncertainty(HistogramTy* hi, int i) { return hi->bin_uncertainty(i); }

  void histogram_release(HistogramTy* hi) { delete hi; }

  // ----- generic functions -----

  void deallocate_string(char* str) { ::free(str); }
}
