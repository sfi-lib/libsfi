#ifndef sfi_WTransformation_h
#define sfi_WTransformation_h

/*
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string>

#include "ITransformation.h"

namespace sfi {
  template<class _Transfo>
  class WTransformation : public ITransformation {
  public:
    using Transf = t_Transform<_Transfo>;

    template<class..._T>
    WTransformation(Context& ctx, _T&&... args):
      m_transformation(new _Transfo(ctx, std::forward<_T>(args)...)), m_context(ctx),
      m_log(Log::get(ctx, "WTransformation{"+m_transformation->name()+"}")),
      m_owned_instance(true) {
    }

    WTransformation(Context& ctx, const OptionMap& opts):
      m_transformation(new _Transfo(ctx, opts)), m_context(ctx),
      m_log(Log::get(ctx, "WTransformation{"+m_transformation->name()+"}")),
      m_owned_instance(true) {
    }

    WTransformation(Context& ctx, _Transfo* trf):
      m_transformation(trf), m_context(ctx),
      m_log(Log::get(ctx, "WTransformation{"+m_transformation->name()+"}")),
      m_owned_instance(false) {
    }

    virtual ~WTransformation() {
      if(m_owned_instance) delete m_transformation;
    }

    // ---- implementation of ITransformation ----

    virtual const std::string& get_name() const { return m_transformation->name(); }

    virtual const std::string& get_type_name() const {
      static std::string _type_name = type_name<_Transfo>();
      return _type_name;
    }

    virtual OptionMap* get_options() { return &m_transformation->options(); }

    virtual void set_print_level(int lvl) { m_transformation->print_level((PrintLevel)lvl); }

    virtual bool do_transform(Context* ctx, const ISample* _spl, int esel, ITransform* _res) const {
      /*if(_spl->get_dim() !=  m_transformation->dims()) {
        do_log_error("do_transform() invalid Sample dimensions "<<_spl->get_dim()<<" expected "<<m_transformation->dims());
        return false;
      }*/
      const bool _use_weight(_spl->get_use_weight());
      if(_use_weight) {
        switch(_spl->get_dim()) {
          case 1: return _transform_impl<1,ttrue>(ctx, _spl, esel, _res);
          case 2: return _transform_impl<2,ttrue>(ctx, _spl, esel, _res);
          case 3: return _transform_impl<3,ttrue>(ctx, _spl, esel, _res);
          case 4: return _transform_impl<4,ttrue>(ctx, _spl, esel, _res);
          case 5: return _transform_impl<5,ttrue>(ctx, _spl, esel, _res);
          case 6: return _transform_impl<6,ttrue>(ctx, _spl, esel, _res);
        }
      }
      else {
        switch(_spl->get_dim()) {
          case 1: return _transform_impl<1,tfalse>(ctx, _spl, esel, _res);
          case 2: return _transform_impl<2,tfalse>(ctx, _spl, esel, _res);
          case 3: return _transform_impl<3,tfalse>(ctx, _spl, esel, _res);
          case 4: return _transform_impl<4,tfalse>(ctx, _spl, esel, _res);
          case 5: return _transform_impl<5,tfalse>(ctx, _spl, esel, _res);
          case 6: return _transform_impl<6,tfalse>(ctx, _spl, esel, _res);
        }
      }
      return false;
    }
    
    virtual ITransform* do_create_transform(Context* ctx, int degree) const {
      if(degree > 0) set_degree(degree, m_transformation);
      auto* res = m_transformation->create_transform(*ctx);
      res->init_params();
      return res;
    }

    template<class _T>
    struct _sub_transformation {
      static ITransformation* value(Context&, _T*) { return 0; }
    };

    template<class _T>
    struct _sub_transformation<AmplitudeTransformation<_T>> {
      static ITransformation* value(Context& ctx, AmplitudeTransformation<_T>* trf) { return new WTransformation<_T>(ctx, &trf->sub_transformation()); }
    };

    virtual ITransformation* get_sub_transformation() {
      return _sub_transformation<_Transfo>::value(m_context, this->m_transformation);
    }
  protected:
    template<class ... _T>
    static void set_degree(unsigned degree, AmplitudeTransformation<_T...>* tr) {
      tr->sub_transformation().options().sub("eigen", true).set("degree", degree);
    }

    template<class ... _T>
    static void set_degree(unsigned degree, ProjectionTransformation<_T...>* tr) {
      tr->options().sub("eigen", true).set("degree", degree);
    }

    template<class _Sample>
    inline bool _transform(Context& ctx, const _Sample& spl, EventSelectorID esel, ITransform* _res) const {
      Transf* res = dynamic_cast<Transf*>(_res);
      if(!res) {
        do_log_error("_transform() invalid transform provided "<<_res->get_type_name()<<" expected "<<(type_name<Transf>()));
        return false;
      }
      if(esel == AllEventSelID) {
        return m_transformation->transform(ctx, create_transform_call(spl, AllEventSel()), *res);
      }
      else if(esel == EvenEventSelID) {
        return m_transformation->transform(ctx, create_transform_call(spl, EvenEventSel()), *res);
      }
      else if(esel == OddEventSelID) {
        return m_transformation->transform(ctx, create_transform_call(spl, OddEventSel()), *res);
      }
      else {
        do_log_error("_transform() invalid event selection "<<esel);
        return false;
      }
    }

    template<unsigned _Dims, class _UseW>
    bool _transform_impl(Context* ctx, const ISample* _spl, int esel, ITransform* _res) const {
      auto* spl = dynamic_cast<const Sample<_Dims, _UseW>*>(_spl);
      if(spl) return _transform(*ctx, *spl, (EventSelectorID)esel, _res);
      else {
        do_log_error("do_transform() invalid Sample "<<_spl->get_type_name()<<" expected "<<(type_name<Sample<_Dims, _UseW>>()));
        return false;
      }
    }

    _Transfo* m_transformation;
    Context& m_context;
    Log m_log;
    bool m_owned_instance;
  };
}

#endif // sfi_WTransformation_h
