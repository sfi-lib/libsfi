# coding: utf-8

"""
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
"""

import ctypes as ct
from .sfi import sfilib, handle_cstr, handle_obj
from .Options import Options

class Analysis(ct.Structure):
    def __init__(self, arg):
        self.m_name = None
        self.m_options = None
        self.m_context = None
        
    def __new__(cls, arg):
        if type(arg) == Options:
            return handle_obj(sfilib.analysis_create_initialized(arg))
        else:
            return handle_obj(sfilib.analysis_create_named(arg.encode('UTF-8')))
    
    def __del__(self):
        if self.m_context:
            self.m_context.clear()
        sfilib.analysis_release(self)
            
    def name(self):
        if not self.m_name:
            self.m_name = handle_cstr(sfilib.analysis_name(self))
        return self.m_name
    
    def options(self):
        if not self.m_options:
            self.m_options = handle_obj(sfilib.analysis_options(self))
        return self.m_options
    
    def context(self):
        if not self.m_context:
            self.m_context = handle_obj(sfilib.analysis_context(self), True)
            self.m_context._ana = self
        return self.m_context
        
    def channel(self, chn):
        return handle_obj(sfilib.analysis_channel(self, chn.encode('UTF-8')))
        
    def channel_samples(self, channel, sort=False, normalize=True):
        return handle_obj(sfilib.analysis_channel_samples(self, channel.encode('UTF-8'), sort, normalize), True)

class AnalysisChannel(ct.Structure):
    def name(self):
        return handle_cstr(sfilib.analysis_channel_name(self))
    
    def variables(self):
        return handle_obj(sfilib.analysis_channel_variables(self))

""" Resource handling in SFI """
class Context(ct.Structure):
    def __init__(self):
        self.m_instances = []
        
    def add_instance(self, obj):
        self.m_instances.append(obj)
        obj._ctx = self
        
    def clear(self):
        for o in self.m_instances:
            del o
        self.m_instances = None
    
