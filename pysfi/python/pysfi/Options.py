# coding: utf-8

"""
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
"""

import ctypes as ct
from .sfi import sfilib, handle_cstr, handle_obj

""" A single option, for internal use only """
class Option(ct.Structure):
    pass

class Options(ct.Structure):
    def __init__(self, filename=None):
        if not hasattr(self, 'm_own_ptr'):
            self.m_own_ptr = False
        
    def __new__(cls, filename=None):
        if filename is None:
            inst = sfilib.options_create()
        else:
            inst = sfilib.options_read(filename.encode('UTF-8'))
            
        if inst:
            oinst = inst.contents
            oinst.m_own_ptr = True
            return oinst
        else:
            return None
        
    def __del__(self):
        if hasattr(self, 'm_own_ptr') and (self.m_own_ptr == True):
            sfilib.options_release(self)
    
    def __iter__(self):
        for n in self.names():
            yield n
    
    def __len__(self):
        return sfilib.options_get_size(self)
    
    def __setitem__(self, key, value):
        self.set(key, value)
    
    def __getitem__(self, key):
        return self.get(key)
    
    def __getattr__(self, name):
        return self.get(name)
    
    def __setattr__(self, name, value):
        return self.set(name, value)
    
    def __str__(self):
        return handle_cstr(sfilib.options_print(self))
    
    def get(self, key):
        o = sfilib.options_get(self, key.encode('UTF-8'))
        if not o:
            return None
        t = sfilib.option_type(o)
        # Int=1, Bool=2, Double=3, String=4, Object=5
        if t == 0:
            print("Unsupported opt type for '%s'"%key)
            return None
        elif t == 1:
            return sfilib.option_value_int(o)
        elif t == 2:
            return sfilib.option_value_bool(o)
        elif t == 3:
            return sfilib.option_value_double(o)
        elif t == 4:
            v = sfilib.option_value_string(o)
            return handle_cstr(v) if v else None
        elif t == 5:
            v = sfilib.option_value_object(o)
            return v.contents if v else None
        else:
           print("Unsupported opt type for '%s' : %d"%(key,t))
           return None 
        
    def names(self):
        _names = (ct.c_char_p*(len(self)))()
        sfilib.options_get_names(self, ct.cast(_names, ct.POINTER(ct.c_char_p)), len(self))
        return [handle_cstr(n) for n in _names]
    
    def set(self, name, value):
        if type(value) == float:
            sfilib.options_set_double(self, name.encode('UTF-8'), value)
        elif type(value) == int:
            sfilib.options_set_int(self, name.encode('UTF-8'), value)
        elif type(value) == bool:
            sfilib.options_set_bool(self, name.encode('UTF-8'), value)
        elif type(value) == str:
            sfilib.options_set_string(self, name.encode('UTF-8'), value.encode('UTF-8'))
        elif type(value) == dict:
            so = sfilib.options_sub(self, name.encode('UTF-8'))
            if so:
                om = so.contents
                for k in value:
                    om.set(k, value[k])
