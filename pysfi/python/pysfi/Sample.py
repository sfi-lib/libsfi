# coding: utf-8

"""
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
"""

import ctypes as ct
from .sfi import *
from array import array
from _ctypes import addressof

class Sample(ct.Structure):
    def __init__(self, ctx, vars, data):
        pass
    
    def __new__(cls, ctx, vars, data):
        o = handle_obj(sfilib.sample_create(ctx, vars, len(data), data.ctypes.data_as(ct.POINTER(ct.c_double))))
        ctx.add_instance(o)
        return o
        
    def __del__(self):
        if self._b_base_:
            sfilib.sample_release(self)
    
    def __len__(self):
        return self.events()
    
    def name(self, _name=None):
        if _name:
            sfilib.sample_set_name(_name.encode('utf-8'))
        else:
            res = sfilib.sample_get_name(self)
            return handle_cstr(res)

    def use_weight(self):
        return sfilib.sample_get_use_weight(self)
    
    def events(self):
        return sfilib.sample_get_events(self)

    def is_normalized(self):
        return sfilib.sample_get_is_normalized(self)
    
    def dimensions(self):
        return sfilib.sample_get_dimensions(self)

    def variables(self):
        return handle_obj(sfilib.sample_get_variables(self))
    
    def normalize_variables(self,analysis):
        sfilib.sample_normalize_variables(analysis, self)
    
    def histogram_1D(self, var=0, nbins=100, external_coordinates=True, event_selection=EventSelection.All):
        return handle_obj(sfilib.sample_get_histogram_1D(self, var, nbins, external_coordinates, event_selection.value), True)

class SampleSet(ct.Structure):
    def __init__(self, *arg, **args):
        super().__init__(*arg, **args)
        self.m_samples = []
        for s in range(sfilib.sample_set_size(self)):
            self.m_samples.append(handle_obj(sfilib.sample_set_get(self, s)))
            
    def __del__(self):
        sfilib.sample_set_release(self)
            
    def __len__(self):
        return len(self.m_samples)
    
    def __getitem__(self, key): 
        return self.m_samples[key]
