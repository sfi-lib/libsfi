# coding: utf-8

"""
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
"""

from matplotlib import pyplot
#from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from pysfi import *

def plot_histogram(hi):
    x = hi.xvalues()
    y = hi.yvalues()
    yerr = hi.uncertainties()
    pyplot.errorbar(x=x, y=y, yerr=yerr, elinewidth=1.0, linewidth=0.0, capsize=1.5)
        
def plot_transform_1D(trf, data, npoints=100, external_coordinates=False):
    # Determine x-values
    if external_coordinates:
        iv = trf.variables().external_interval(0)
        xp = np.linspace(iv[0], iv[1],npoints)
    else:
        iv = (-1,1)
        xp = np.linspace(-1,1,npoints)
    _scale = (iv[1] - iv[0])/float(npoints)
    yp = np.ndarray(shape=[npoints])
    
    # plot 'data'
    if type(data) == Histogram:
        plot_histogram(hi=data)
        # assume histogram is un-weighted
        _scale *= data.entries()/trf.integral()
        print("entries: %f integral: %f" % (data.entries(),trf.integral()))
    elif data is not None:
        pyplot.hist(data, npoints, [iv[0],iv[1]])
    
    trf.eval(xp, yp, scale=_scale, external_coordinates=external_coordinates)
    pyplot.plot(xp, yp, 'r')
    pyplot.show()

def eval_transform_2D(trf, npoints=40, external_coordinates=False):
    if external_coordinates:
        vars = trf.variables()
        xiv = vars.external_interval(0)
        yiv = vars.external_interval(1)
        x = np.linspace(xiv[0], xiv[1], npoints)
        y = np.linspace(yiv[0], yiv[1], npoints)
        vars.lock()
    else:
        x = np.linspace(-1, 1, npoints)
        y = np.linspace(-1, 1, npoints)
    xp, yp = np.meshgrid(x, y, sparse=False)

    pts = np.zeros([len(xp)*len(yp), 2])
    pts[:,0] = xp.flatten()
    pts[:,1] = yp.flatten()

    z = np.zeros(len(xp)*len(yp))
    trf.eval(pts, z, 1.0, external_coordinates)
    z = np.reshape(z, (len(xp),len(yp)))
    return xp, yp, z
    
def plot_transform_2D(trf, npoints=40):
    xp, yp, z = eval_transform_2D(trf, npoints)
    fig = pyplot.figure()
    ax = fig.gca(projection='3d')
    surf = ax.plot_surface(xp, yp, z)
    pyplot.show()

def plot_transform_2D_spherical(trf, npoints=40):
    xp, yp, zp = eval_transform_2D(trf, npoints)
    theta = 0.5*np.pi*(xp + 1)
    phi = np.pi*yp
    
    x = zp*np.sin(theta)*np.cos(phi)
    y = zp*np.sin(theta)*np.sin(phi)
    z = zp*np.cos(theta)
    fig = pyplot.figure()
    ax = fig.gca(projection='3d')
    surf = ax.plot_surface(x, y, z)
    pyplot.show()

def plot_axis_transform(trf, axis, data, external_coordinates=False, npoints=100):
    at = trf.axis_transform(axis=axis)
    d = None
    if type(data) == Histogram:
        d = data
    elif data is not None:
        d = data[:,axis]
    plot_transform_1D(trf=at, data=d, npoints=npoints, external_coordinates=external_coordinates)
