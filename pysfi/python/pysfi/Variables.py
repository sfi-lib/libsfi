# coding: utf-8

"""
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
"""

import ctypes as ct
from enum import Enum
from .sfi import *

class Variables(ct.Structure):
    class Transform(Enum):
        Linear = 0 
        Ln = 1
        Acos = 2 
        Atan = 3 
        Cos = 4
    
    def __init__(self, nvars = None):
        pass
    
    def __new__(cls, nvars):
        if (nvars < 1) or (nvars > 10):
            raise ValueError("nvars must be and int between 1 and 10")
        inst = sfilib.variables_create(nvars)
        return inst.contents if inst else None
    
    def __len__(self):
        return self.nvariables()

    def __str__(self):
        return handle_cstr(sfilib.variables_print(self))
    
    def nvariables(self):
        return sfilib.variables_nvariables(self)
    
    def name(self, nr, _name=None):
        if _name:
            sfilib.variables_set_name(self, nr, _name.encode('utf-8'))
        else:
            return handle_cstr(sfilib.variables_get_name(self, nr))
    
    def title(self, nr, _title=None):
        if _title:
            sfilib.variables_set_title(self, nr, _title.encode('utf-8'))
        else:
            return handle_cstr(sfilib.variables_get_title(self, nr))
    
    def trans_type(self, nr, _ttype=None):
        if _ttype:
            sfilib.variables_set_trans_type(self, nr, _ttype.value)
        else:
            return sfilib.variables_get_trans_type(self, nr)
    
    def external_interval(self, nr, _min=None, _max=None):
        if not _min is None:
            sfilib.variables_set_external_interval(self, nr, _min, _max)
        else:
            return (sfilib.variables_get_external_min(self, nr), sfilib.variables_get_external_max(self, nr))
    
    def intermediary_interval(self, nr, _min, _max):
        sfilib.variables_set_intermediary_interval(self, nr, _min, _max)
    
    """ Prepare the variables for transforming data """
    def lock(self):
        sfilib.variables_lock(self)
