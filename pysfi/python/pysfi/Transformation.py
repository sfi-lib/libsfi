# coding: utf-8

"""
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
"""

import ctypes as ct
from enum import Enum
from .sfi import sfilib, handle_obj

class Transformation(ct.Structure):
    class Type(Enum):
        Linear=0
        Amplitude=1
    
    class Basis(Enum):
        Cosine=0
        Legendre=1
        Fourier=2,
        Ylm=3
    
    def __init__(self, ana, trans_type, dims, sparse=False, basis_type=Basis.Cosine, opts=None):
        self.m_trans_type = trans_type
        self.m_dims = dims
        self.m_sparse = sparse
        self.m_basis_type = basis_type
        self.m_opts = self._options()
        for _n in self.m_opts.names():
            name = str(_n)
            setattr(self.__class__, name, lambda s,v,name=name : s.m_opts.set(name, v))
    
    def __new__(cls, ana, trans_type, dims, sparse=False, basis_type=Basis.Cosine, opts=None):
        if not type(trans_type) == Transformation.Type:
            raise TypeError("trans_type must be of type Transformation.Type")
        if not type(basis_type) == Transformation.Basis:
            raise TypeError("basis_type must be of type Transformation.Basis")
        if (dims < 1) or (dims > 10):
            raise ValueError("dims must be and int between 1 and 10")
        if not opts:
            return handle_obj(sfilib.transformation_create(ana, trans_type.value, dims, sparse, basis_type.value))
        else:
            return handle_obj(sfilib.transformation_create_from_options(ana, opts, trans_type.value, dims, sparse, basis_type.value))
    
    def __del__(self):
        if self._b_base_:
            sfilib.transformation_release(self)
    
    def name(self):
        return handle_cstr(sfilib.transformation_get_name(self)) 
    
    def type_name(self):
        return handle_cstr(sfilib.transformation_get_type_name(self))  
    
    def options(self):
        return self.m_opts
    
    def _options(self):
        return handle_obj(sfilib.transformation_get_options(self))
        
    def print_level(self, pl):
        return sfilib.transformation_set_print_level(self, pl.value)
            
    def transform(self, context, sample, esel, transform):
        return sfilib.transformation_do_transform(self, context, sample, esel.value, transform)
    
    def create_transform(self, ctx, degrees=-1):
        o = handle_obj(sfilib.transformation_create_transform(self, ctx, degrees))
        ctx.add_instance(o)
        return o
    
    def type(self):
        return self.m_trans_type
    
    def dimensions(self):
        return self.m_dims
    
    def is_sparse(self):
        return self.m_sparse
    
    def basis_type(self):
        return self.m_basis_type 
