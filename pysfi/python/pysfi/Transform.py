# coding: utf-8

"""
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
"""

import ctypes as ct
from .sfi import sfilib, handle_obj

class Transform(ct.Structure):
    def __init__(self):
        pass
    
    def __del__(self):
        if self._b_base_:
            sfilib.transform_release(self)
        
    def name(self):
        return handle_cstr(sfilib.transform_get_name(self)) 
    
    def type_name(self):
        return handle_cstr(pysfisfilib.transform_get_type_name(self)) 
    
    def basis_name(self):
        return handle_cstr(sfilib.transform_get_basis_name(self)) 
        
    def dimensions(self):
        return sfilib.transform_get_dimensions(self)

    def max_degree(self):
        return sfilib.transform_get_max_degree(self)

    def nparams(self):
        return sfilib.transform_get_nparams(self)

    def is_amplitude(self, _is_amp = None):
        if _is_amp:
            sfilib.transform_get_is_amplitude(self, _is_amp)
        else:
            return sfilib.transform_get_is_amplitude(self)

    def param(self, idx, _val=None):
        if _val:
            return sfilib.transform_set_param(self, idx, _val)
        else:
            return sfilib.transform_get_param(self, idx)
    
    def evaluator(self, scale=1.0, external_coordiantes=False):
        return handle_obj(sfilib.transform_get_evaluator(self, scale, external_coordiantes))
  
    def integral(self):
        return sfilib.transform_get_integral(self)

    def axis_transform(self, axis):
        return handle_obj(sfilib.transform_get_axis_transform(self, axis))

    def normalize(self, scale):
        sfilib.transform_do_normalize(self, scale)
    
    def variables(self, _vars = None):
        if _vars:
            sfilib.transform_set_variables(self, _vars)
        else:
            return handle_obj(sfilib.transform_get_variables(self))
    
    def eval(self, x, y, scale = 1.0, external_coordinates=False):
        return sfilib.transform_do_eval(self, len(x), x.ctypes.data_as(ct.POINTER(ct.c_double)), y.ctypes.data_as(ct.POINTER(ct.c_double)), scale, external_coordinates)
