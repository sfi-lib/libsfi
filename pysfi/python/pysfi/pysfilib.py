# coding: utf-8

"""
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
"""

import ctypes as ct
from .Transform import Transform
from .Variables import Variables
from .Sample import Sample, SampleSet
from .Transformation import Transformation
from .Options import Options, Option
from .Analysis import Analysis, AnalysisChannel, Context
from .Histogram import Histogram
from .sfi import *

def def_function(func, res, args):
	func.restype = res
	func.argtypes = args

def bind_functions():
	# ---- Sample functions ----
	
	def_function(sfilib.sample_release, None, [ct.POINTER(Sample)])
	def_function(sfilib.sample_create, ct.POINTER(Sample), [ct.POINTER(Context), ct.POINTER(Variables), ct.c_int, ct.POINTER(ct.c_double)])
	def_function(sfilib.sample_get_name, ct.c_char_p, [ct.POINTER(Sample)])
	def_function(sfilib.sample_set_name, None, [ct.POINTER(Sample), ct.c_char_p])
	def_function(sfilib.sample_get_use_weight, ct.c_bool, [ct.POINTER(Sample)])
	def_function(sfilib.sample_get_events, ct.c_int, [ct.POINTER(Sample)])
	def_function(sfilib.sample_get_is_normalized, ct.c_bool, [ct.POINTER(Sample)])
	def_function(sfilib.sample_get_dimensions, ct.c_int, [ct.POINTER(Sample)])
	def_function(sfilib.sample_get_owns_data, ct.c_bool, [ct.POINTER(Sample)])
	def_function(sfilib.sample_get_variables, ct.POINTER(Variables), [ct.POINTER(Sample)])
	def_function(sfilib.sample_normalize_variables, None, [ct.POINTER(Analysis),ct.POINTER(Sample)])
	def_function(sfilib.sample_get_histogram_1D, ct.POINTER(Histogram), [ct.POINTER(Sample), ct.c_int, ct.c_int, ct.c_bool, ct.c_int])
	
	# ---- SampleSet functions ----
	
	def_function(sfilib.sample_set_release, None, [ct.POINTER(SampleSet)])
	def_function(sfilib.sample_set_size, ct.c_int, [ct.POINTER(SampleSet)])
	def_function(sfilib.sample_set_get, ct.POINTER(Sample), [ct.POINTER(SampleSet), ct.c_int])

	# ---- Analysis functions ----
	
	def_function(sfilib.analysis_create_named, ct.POINTER(Analysis), [ct.c_char_p])
	def_function(sfilib.analysis_create_initialized, ct.POINTER(Analysis), [ct.POINTER(Options)])
	def_function(sfilib.analysis_release, None, [ct.POINTER(Analysis)])
	def_function(sfilib.analysis_name, ct.c_char_p, [ct.POINTER(Analysis)])
	def_function(sfilib.analysis_options, ct.POINTER(Options), [ct.POINTER(Analysis)])
	def_function(sfilib.analysis_context, ct.POINTER(Context), [ct.POINTER(Analysis)])
	def_function(sfilib.analysis_channel, ct.POINTER(AnalysisChannel), [ct.POINTER(Analysis), ct.c_char_p])
	def_function(sfilib.analysis_channel_samples, ct.POINTER(SampleSet), [ct.POINTER(Analysis), ct.c_char_p, ct.c_bool, ct.c_bool])
  
    # ---- AnalysisChannel functions ----
      
	def_function(sfilib.analysis_channel_name, ct.c_char_p, [ct.POINTER(AnalysisChannel)])
	def_function(sfilib.analysis_channel_variables, ct.POINTER(Variables), [ct.POINTER(AnalysisChannel)])
      
	# ---- Transform functions ----
	
	def_function(sfilib.transform_release, None, [ct.POINTER(Transform)])
	def_function(sfilib.transform_get_name, ct.c_char_p, [ct.POINTER(Transform)])
	def_function(sfilib.transform_get_type_name, ct.c_char_p, [ct.POINTER(Transform)])
	def_function(sfilib.transform_get_basis_name, ct.c_char_p, [ct.POINTER(Transform)])
	def_function(sfilib.transform_get_dimensions, ct.c_int, [ct.POINTER(Transform)])
	def_function(sfilib.transform_get_max_degree, ct.c_int, [ct.POINTER(Transform)])
	def_function(sfilib.transform_get_nparams, ct.c_int, [ct.POINTER(Transform)])
	def_function(sfilib.transform_get_is_amplitude, ct.c_bool, [ct.POINTER(Transform)])
	def_function(sfilib.transform_set_is_amplitude, None, [ct.POINTER(Transform), ct.c_bool])
	def_function(sfilib.transform_get_param, ct.c_double, [ct.POINTER(Transform), ct.c_int])
	def_function(sfilib.transform_set_param, None, [ct.POINTER(Transform), ct.c_int, ct.c_double])
	def_function(sfilib.transform_get_evaluator, ct.POINTER(Evaluator), [ct.POINTER(Transform), ct.c_double])
	def_function(sfilib.transform_get_integral, ct.c_double, [ct.POINTER(Transform)])
	def_function(sfilib.transform_get_axis_transform, ct.POINTER(Transform), [ct.POINTER(Transform), ct.c_int])
	def_function(sfilib.transform_do_normalize, None, [ct.POINTER(Transform), ct.c_double])
	def_function(sfilib.transform_get_variables, ct.POINTER(Variables), [ct.POINTER(Transform)])
	def_function(sfilib.transform_set_variables, None, [ct.POINTER(Transform), ct.POINTER(Variables)])
	def_function(sfilib.transform_do_eval, ct.c_bool, [ct.POINTER(Transform), ct.c_int, ct.POINTER(ct.c_double), ct.POINTER(ct.c_double), ct.c_double, ct.c_bool])
	
	# ---- Transformation functions ----
	
	def_function(sfilib.transformation_release, None, [ct.POINTER(Transformation)])
	def_function(sfilib.transformation_create, ct.POINTER(Transformation), [ct.POINTER(Analysis), ct.c_int, ct.c_int, ct.c_bool, ct.c_int])
	def_function(sfilib.transformation_create_from_options, ct.POINTER(Transformation), [ct.POINTER(Analysis), ct.POINTER(Options), ct.c_int, ct.c_int, ct.c_bool, ct.c_int])
	def_function(sfilib.transformation_get_name, ct.c_char_p, [ct.POINTER(Transformation)])
	def_function(sfilib.transformation_get_type_name, ct.c_char_p, [ct.POINTER(Transformation)])
	def_function(sfilib.transformation_get_options, ct.POINTER(Options), [ct.POINTER(Transformation)])
	def_function(sfilib.transformation_set_print_level, None, [ct.POINTER(Transformation), ct.c_int])
	def_function(sfilib.transformation_do_transform, ct.c_bool, [ct.POINTER(Transformation), ct.POINTER(Context), ct.POINTER(Sample), ct.c_int, ct.POINTER(Transform)])
	def_function(sfilib.transformation_create_transform, ct.POINTER(Transform), [ct.POINTER(Transformation), ct.POINTER(Context), ct.c_int])
	
	# ---- Evaluator functions ----
	
	def_function(sfilib.evaluator_release, None, [ct.POINTER(Evaluator)])
	
	# ---- Variables functions ----
	
	def_function(sfilib.variables_create, ct.POINTER(Variables), [ct.c_int])
	def_function(sfilib.variables_release, None, [ct.POINTER(Variables)])
	def_function(sfilib.variables_nvariables, ct.c_int, [ct.POINTER(Variables)])
	def_function(sfilib.variables_get_name, ct.c_char_p, [ct.POINTER(Variables), ct.c_int])
	def_function(sfilib.variables_set_name, None, [ct.POINTER(Variables), ct.c_int, ct.c_char_p])
	def_function(sfilib.variables_get_title, ct.c_char_p, [ct.POINTER(Variables), ct.c_int])
	def_function(sfilib.variables_set_title, None, [ct.POINTER(Variables), ct.c_int, ct.c_char_p])
	def_function(sfilib.variables_set_external_interval, None, [ct.POINTER(Variables), ct.c_int, ct.c_double, ct.c_double])
	def_function(sfilib.variables_get_external_min, ct.c_double, [ct.POINTER(Variables), ct.c_int])
	def_function(sfilib.variables_get_external_max, ct.c_double, [ct.POINTER(Variables), ct.c_int])
	def_function(sfilib.variables_set_intermediary_interval, None, [ct.POINTER(Variables), ct.c_int, ct.c_double, ct.c_double])
	def_function(sfilib.variables_get_trans_type, ct.c_int, [ct.POINTER(Variables), ct.c_int])
	def_function(sfilib.variables_set_trans_type, None, [ct.POINTER(Variables), ct.c_int, ct.c_int])
	def_function(sfilib.variables_lock, None, [ct.POINTER(Variables)])
	def_function(sfilib.variables_print, ct.c_void_p, [ct.POINTER(Variables)])
	
	# ---- Options functions ----
	
	def_function(sfilib.options_create, ct.POINTER(Options), [])
	def_function(sfilib.options_read, ct.POINTER(Options), [ct.c_char_p])
	def_function(sfilib.options_release, None, [ct.POINTER(Options)])
	def_function(sfilib.options_get_size, ct.c_int, [ct.POINTER(Options)])
	def_function(sfilib.options_get_names, None, [ct.POINTER(Options), ct.POINTER(ct.c_char_p), ct.c_int])
	def_function(sfilib.options_set_int, None, [ct.POINTER(Options), ct.c_char_p, ct.c_int])
	def_function(sfilib.options_set_bool, None, [ct.POINTER(Options), ct.c_char_p, ct.c_bool])
	def_function(sfilib.options_set_double, None, [ct.POINTER(Options), ct.c_char_p, ct.c_double])
	def_function(sfilib.options_set_string, None, [ct.POINTER(Options), ct.c_char_p, ct.c_char_p])
	def_function(sfilib.options_get, ct.POINTER(Option), [ct.POINTER(Options), ct.c_char_p])
	def_function(sfilib.options_sub, ct.POINTER(Options), [ct.POINTER(Options), ct.c_char_p])
	def_function(sfilib.options_print, ct.c_void_p, [ct.POINTER(Options)])
	
	def_function(sfilib.option_type, ct.c_int, [ct.POINTER(Option)])
	def_function(sfilib.option_value_int, ct.c_int, [ct.POINTER(Option)])
	def_function(sfilib.option_value_bool, ct.c_bool, [ct.POINTER(Option)])
	def_function(sfilib.option_value_double, ct.c_double, [ct.POINTER(Option)])
	def_function(sfilib.option_value_string, ct.c_char_p, [ct.POINTER(Option)])
	def_function(sfilib.option_value_object, ct.POINTER(Options), [ct.POINTER(Option)])
	
	# ---- Histogram functions ----
	
	def_function(sfilib.histogram_min_x, ct.c_double, [ct.POINTER(Histogram)])
	def_function(sfilib.histogram_max_x, ct.c_double, [ct.POINTER(Histogram)])
	def_function(sfilib.histogram_nbins, ct.c_int, [ct.POINTER(Histogram)])
	def_function(sfilib.histogram_fill, None, [ct.POINTER(Histogram), ct.c_double, ct.c_double])
	def_function(sfilib.histogram_bin_content, ct.c_double, [ct.POINTER(Histogram), ct.c_int])
	def_function(sfilib.histogram_bin_uncertainty, ct.c_double, [ct.POINTER(Histogram), ct.c_int])
	def_function(sfilib.histogram_release, None, [ct.POINTER(Histogram)])
	def_function(sfilib.histogram_entries, ct.c_int, [ct.POINTER(Histogram)])
	
	# ---- generic functions ----
	
	def_function(sfilib.deallocate_string, None, [ct.c_void_p])
