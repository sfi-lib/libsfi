# coding: utf-8

"""
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
"""

import ctypes as ct
from enum import Enum
import os
import numpy as np

sfi_pylib_path = os.environ.get("SFI_PYLIB_PATH")
sfilib = np.ctypeslib.load_library("libSFIPy", sfi_pylib_path)

""" 
Convert c-string to utf-8.
If the string should be deallocated in c,
return char* from c and assign type c_void_p.
If the string is a reference, just use c_char_p. 
"""
def handle_cstr(_str):
    if not _str:
        return None
    res = None
    if type(_str) == bytes:
        return _str.decode('utf-8')
    else:
        res = ct.cast(_str, ct.c_char_p).value.decode('utf-8')
        sfilib.deallocate_string(_str)
        return res

def handle_obj(_inst, run_init=False):
    if run_init:
        i = _inst.contents if _inst else None
        if i is not None:
            i.__init__()
        return  i
    return _inst.contents if _inst else None

class Evaluator(ct.Structure):
    def __init__(self):
        pass
    
    def __del__(self):
        if self._b_base_:
            sfilib.evaluator_release(self)

class PrintLevel(Enum):
    Verbose = 0
    Debug = 1
    Warning = 2
    Error = 3
    Info = 4

class EventSelection(Enum):
    All = 0
    Even = 1
    Odd = 2
 
