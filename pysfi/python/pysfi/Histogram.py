# coding: utf-8

"""
    Copyright (C) 2020, Jörgen Sjölin and Karl Gellerstedt.

    This file is part of SFI.

    SFI is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SFI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SFI.  If not, see <https://www.gnu.org/licenses/>.
"""

import ctypes as ct
from .sfi import *

class Histogram(ct.Structure):
    def __init__(self):
        self.m_min_x = None
        self.m_max_x = None
        self.m_nbins = None
        
    def __del__(self):    
        sfilib.histogram_release(self)
    
    def min_x(self):
        if self.m_min_x is None:
            self.m_min_x = sfilib.histogram_min_x(self)
        return self.m_min_x
    
    def max_x(self):
        if self.m_max_x is None:
            self.m_max_x = sfilib.histogram_max_x(self)
        return self.m_max_x
    
    def nbins(self):
        if self.m_nbins is None:
            self.m_nbins = sfilib.histogram_nbins(self)
        return self.m_nbins
    
    def entries(self):
        return sfilib.histogram_entries(self)
    
    def fill(self, x, w = 1.0):
        sfilib.histogram_fill(self, x, w)
    
    def bin_content(self, i):
        return sfilib.histogram_bin_content(self, i)
    
    def bin_uncertainty(self, i):
        return sfilib.histogram_bin_uncertainty(self, i)

    def xvalues(self):
        xv = np.zeros(self.nbins())
        dx = (self.max_x() - self.min_x())/self.nbins()
        x = self.min_x() + 0.5*dx
        for b in range(0, self.nbins()):
            xv[b-1] = x
            x += dx
        return xv
    
    def yvalues(self):
        yv = np.zeros(self.nbins())
        for b in range(1, self.nbins()+1):
            yv[b-1] = self.bin_content(b)
        return yv
    
    def uncertainties(self):
        uv = np.zeros(self.nbins())
        for b in range(1, self.nbins()+1):
            uv[b-1] = self.bin_uncertainty(b)
        return uv
