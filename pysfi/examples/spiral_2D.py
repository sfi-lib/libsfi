from pysfi import *
import math

nevents = 10000
a = np.random.normal(0.1, 0.07,[nevents, 2])
v = 1.5  + 2.0*math.pi*np.random.uniform(0, 1, nevents)    
b = 1.0/(4.0*math.pi)
a[:,0] -= b*v*np.cos(v)
a[:,1] -= b*v*np.sin(v)

ana = Analysis('Spiral 2D')
s = Sample(ana.context(), Variables(2), a)
tr = Transformation(ana, Transformation.Type.Amplitude, dims=2, sparse=True, basis_type=Transformation.Basis.Cosine)
    
trf = tr.create_transform(ana.context(), degrees=30)
tr.transform(context=ana.context(), sample=s, esel=EventSelection.All, transform=trf)

plot_transform_2D(trf, npoints=40)
    
