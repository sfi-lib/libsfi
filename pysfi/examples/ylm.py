from pysfi import *
import numpy as np
from matplotlib import pyplot

def ylm_index(l, m):
     return int((l*(l+1))/2 + m)
 
def plot_ylm(trf, npoints=40):
    xp, yp, zp = eval_transform_2D(trf, npoints, external_coordinates=True)
    x = zp*np.sin(xp)*np.cos(yp)
    y = zp*np.sin(xp)*np.sin(yp)
    z = zp*np.cos(xp)
    fig = pyplot.figure()
    ax = fig.gca(projection='3d')
    surf = ax.plot_surface(x, y, z, cmap=pyplot.get_cmap('jet'))
    pyplot.show()

ana = Analysis('Ylm')

# In this case the Transformation acts as a factory for Transforms
tr = Transformation(ana, Transformation.Type.Amplitude, dims=2, sparse=False, basis_type=Transformation.Basis.Ylm)    
trf = tr.create_transform(ana.context(), degrees=6)

# Ylm needs the first variable (theta) to be transformed by cos
vars = Variables(2)
vars.external_interval(0, 0.0, np.pi)
vars.trans_type(0, Variables.Transform.Cos)
vars.external_interval(1, -np.pi, np.pi)

trf.variables(vars)
trf.param(0, 20)
trf.param(ylm_index(4, 4), 5)
trf.param(ylm_index(6, 0), 5)
plot_ylm(trf, npoints=100)

plot_axis_transform(trf, 0, data=None, external_coordinates=True)
plot_axis_transform(trf, 1, data=None, external_coordinates=True)
