from pysfi import *

x = np.random.randn(1000)
x = x*0.1 + 1

ana = Analysis('Gaus 1D')
# Define the external interval for the variable
vars = Variables(1)
vars.external_interval(0, 0.0, 2.0)

# Create a sample and normalize it, 
# i.e. transform to the internal space (-1, 1)  
s = Sample(ana.context(), vars, x)
s.normalize_variables(ana)

tr = Transformation(ana, Transformation.Type.Amplitude, dims=1, sparse=False, basis_type=Transformation.Basis.Cosine)
trf = tr.create_transform(ana.context(), degrees=10)
tr.print_level(PrintLevel.Verbose)
tr.transform(context=ana.context(), sample=s, esel=EventSelection.All, transform=trf)
plot_transform_1D(trf, data=x, npoints=200)
